<?php
/************************************************/
/* Author Barry Wood & John Horton              */
/* Date   Jul 05 2005               		*/
/* Filename = scripmoderate.php	     		*/
/* This program moderates the scripware system	*/
/************************************************/
session_set_cookie_params (60 * 60);
session_start();
//session_register("case");

// Read in class to communicate with database
require 'dbabstraction.php';
class scripclass {
    // Student details 
    var $studentID, $firstname, $lastname, $initials, $studentrecordID,
    $authority, $seat, $realStudent,

    // Moderator details 
    $moderatorID,$mfirstname, $mlastname, $musername, $mpassword, $MorS,

    // Usage
    $whence, $whither, $lastTime, $nResults,

    // Colours
    $bgcolor, $bgcolor1, $bgcolor2,     

    // Generic code
    $weeksGC, $DEFGC, $sizeGC, $numberGC, $multiple,

    // Display details
    $OODNoProd,$Fatal,$lostFatal,$lostOODNoProd,
    $lostClassify,$lostVerify,$lostRegisters,$lostLabel,
    $lostAncillaries,$lostEndorse,$lostFate,$lostTabs,
    $lostCD,$lostPOR,$finalScore, $group, $lostPMR,
    $pageShowing, $textMain, $textChanges, $textCD, $textPOR, $textOTC,
    $OTCScore,$OTCTotal,$fullAuthority,
    $lostOTCNegative,$lostOTCNegative2,$lostOTCOOD,$lostOTCOOD2,$lostOTCCDSig,
    $lostOTCFatal, $seriousErrorType,
    $correctP,$OTCNegative,$OTCNegative2,$OTCOOD,$OTCOOD2,$OTCCDSig,
    $OTCFatal,$itemsOfInfo,$OTCQ1,$OTCQ2,
    $style,$information,$structure,$response,$understanding,
    $communication,$OTCcomments,$lostNA,$lostAAI,$lostAAI2,
    $PageToShow, $variety,$commentsAudit,
    $OTCWrongQtyLegal, $OTCWrongQtyLegal2, $lostOTCWrongQtyLegal, $lostOTCWrongQtyLegal2,

    // Case details
    $caseID, $formID, $itemScore, $categoryToMod, $mode, $submode, $mark, 

    // Drug details
    $patientproblemID, $prescriberproblemID, $drugproblemID,
    $miscproblemID,

    // Patient details
    $P,

    // Miscellaneous details
    $theSeriousPenalty,

    // Configuration details
    $byes, $resultsTable,

    // Student record details ...
    $FinishedYN, $ClassifyID, $VerifyID,
    $RegisterID, $LabelID, $LabelID2, $EndorseID, $ScoreID, $VerifyDrugID,
    $DateDone, $ProbeID, $PMRViewedID, $InteractionID,
    $theSRecord,

    $lastModID,
    $maxStyle, $maxInformation, $maxStructure, $maxResponse, $maxUnderstanding,
    $maxCommTotal, $maxscore,
    $finished, $moderated, $released, $location;

    // Constructor function 
    function scripclass() {
    } 
}  

/************************/
/* program starts here 	*/
/************************/
// Set the "fatal penalty" - by now (October 2009), this is old code but we can leave it as a default ...
//$_SESSION['theSeriousPenalty'] = 35;

    // How do we want to run the serious error type? $_SESSION['multiple'] = 0 => as a single box; $_SESSION['multiple'] = 1 => as a multiple box; 
    $_SESSION['multiple'] = 1;

// In PHP5 must explicitly set $tab ...
if (isset($_GET['tab'])){ $tab = $_GET['tab']; };
if (isset($_POST['tab'])){ $tab = $_POST['tab']; };

if ($tab != 200 && $tab != 300) {
    A_html_begin("Scripware Results");
};

if (! isset($tab)) {
    // New session variable ...
//    $case=new scripclass();

    // Have the various GET variables been set? If not, that is because we've just arrived from logon ...
    if (!isset($_SESSION['variety'])) {
        // Clear the session variable ...
        $_SESSION = array(); 
 
        // Collect information from logging in:
        $_SESSION['table']=$_GET['t'];

        // The ID is used in different ways according to whether user is a student or an administrator (i.e. moderator) ...
        if ($_SESSION['table']=="Student"){
            $_SESSION['ID']=$_GET['i'];
        } else {
            $_SESSION['adminID']=$_GET['i'];
        };

//        $_SESSION['ID']=$_GET['i'];

        $_SESSION['adminID']=$_GET['i'];
        $_SESSION['authority']=$_GET['a'];
        $_SESSION['inTheDispensary']=$_GET['d'];
        $_SESSION['dispensaryMode']=$_GET['m'];
        $_SESSION['variety']=$_GET['v'];
    };

// Commentary: 
//    table: Which table does the user appear in?
//    ID: What is the username of the user in that table (via ID)?
//    authority: What authority does the user have? "A" is adminstrator; "M" is moderator; "S" is student. Note that !users in table "Student" have "M" authority
//    inTheDispensary is a number: If 0, false; if <> 0, true
//    dispensaryMode is a letter: If "U" unsupervised; if "P" practice, if "E" examination

//    B_startprog();



//  Here we need the code that lets the user choose time and place ...

/*
    if (!isset($_POST['combination'])){
        Z_TimesAndPlaces();
    } else {
        $chosenElement = $_POST['combination'];
        $temp = explode("|", $_POST['chosentime']);
        $_SESSION['chosenTime'] = $temp[$chosenElement];
        $temp = explode("|", $_POST['chosenplace']);
        $_SESSION['chosenPlace'] = $temp[$chosenElement];
        C_checkUserAllVersions($theUsername,$thePassword);
    };
*/

    C_checkUserAllVersions($theUsername,$thePassword);


//    C_checkUserAllVersions($theUsername,$thePassword);



/*
    if ($_SESSION['variety'] == 1){
        C_checkuser($theUsername,$thePassword);
    };

    if ($_SESSION['variety'] == 2){
        // Now run the code for generic comments ...
        C_checkuserGC($theUsername,$thePassword);
    };

    if ($_SESSION['variety'] == 3){
        // Now run the code for calculations ...
        C_checkuserCalculation($theUsername,$thePassword);
    };
*/





} else {
    switch ($tab) {
      	 case 0: // user has just entered log on data
        $theUsername=$_POST['username'];
        $thePassword=$_POST['password'];
//        C_checkuser($theUsername,$thePassword);break;

//        case 5:
//
//        break;

        case 10: // Main tab clicked

            // Record usage here ...
            $nameOfThisTab = "Main";
            if ($fromtab){

                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // USAGE section (to ScripUsageSM_ITabs):
                // Where we're just about to go to ...	
                $_SESSION['whither'] = $nameOfThisTab;

                // Read the time and date ...
                $time = time ();
                if ($_SESSION['lastTime']){
                    $timeSpent = $time - $_SESSION['lastTime'];
                };
                $_SESSION['lastTime'] = $time;

                $time = date('G:i:s', $time);
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'CaseID'=>$_SESSION['caseID'], 'RealStudent'=>$_SESSION['realStudent'], 
                              'TimeSpent'=>$timeSpent,
                              'Whence'=>$_SESSION['whence'], 'Whither'=>$_SESSION['whither']);

                $db->insert("ScripUsageSM_ITabs", $text);

                // ... and finally store the old whither as the new whence
                $_SESSION['whence'] = $_SESSION['whither'];
            };

            D_DisplayHeaderTable();
            $_SESSION['pageShowing']="Main";
            E_Main();
            break;
	case 11: // main form submitted 
	    D_DisplayHeaderTable();
	    EA_MainUpdate();
	    break;
	case 12: // main form - new data
            $_SESSION['OODNoProd']=$_POST['OODNoProd'];
            $_SESSION['Fatal']=$_POST['Fatal'];
            $_SESSION['NACheck']=$_POST['NACheck'];
            $_SESSION['textMain']=$_POST['marking'];
            $_SESSION['textChanges']=$_POST['changes'];
	    D_DisplayHeaderTable();
	    E_Main();
	    break;



        case 15:
        C_checkuserCalculation($theUsername,$thePassword);
        break;



	case 20: // CD Register tab clicked

            // Record usage here ...
            // Record usage here ...
            $nameOfThisTab = "CD";
            if ($fromtab){


                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // USAGE section (to ScripUsageSM_ITabs):
                // Where we're just about to go to ...	
                $_SESSION['whither'] = $nameOfThisTab;

                // Read the time and date ...
                $time = time ();
                if ($_SESSION['lastTime']){
                    $timeSpent = $time - $_SESSION['lastTime'];
                };
                $_SESSION['lastTime'] = $time;

                $time = date('G:i:s', $time);
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'CaseID'=>$_SESSION['caseID'], 'RealStudent'=>$_SESSION['realStudent'], 
                              'TimeSpent'=>$timeSpent,
                              'Whence'=>$_SESSION['whence'], 'Whither'=>$_SESSION['whither']);

                $db->insert("ScripUsageSM_ITabs", $text);

                // ... and finally store the old whither as the new whence
                $_SESSION['whence'] = $_SESSION['whither'];
            };



	    D_DisplayHeaderTable();
            $_SESSION['pageShowing'] = "CD";
	    F_CD();
	    break;
	case 21: // CD form submitted
	    D_DisplayHeaderTable();
	    FA_CDUpdate();
	    break;
	case 22: // CD form - new data
            $_SESSION['textCD']=$_POST['markingCD'];
	    D_DisplayHeaderTable();
	    F_CD();
	    break;
	case 30: // POR tab clicked 

            // Record usage here ...

            // Record usage here ...
            $nameOfThisTab = "POR";
            if ($fromtab){

                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // USAGE section (to ScripUsageSM_ITabs):
                // Where we're just about to go to ...	
                $_SESSION['whither'] = $nameOfThisTab;

                // Read the time and date ...
                $time = time ();
                if ($_SESSION['lastTime']){
                    $timeSpent = $time - $_SESSION['lastTime'];
                };
                $_SESSION['lastTime'] = $time;

                $time = date('G:i:s', $time);
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'CaseID'=>$_SESSION['caseID'], 'RealStudent'=>$_SESSION['realStudent'], 
                              'TimeSpent'=>$timeSpent,
                              'Whence'=>$_SESSION['whence'], 'Whither'=>$_SESSION['whither']);

                $db->insert("ScripUsageSM_ITabs", $text);

                // ... and finally store the old whither as the new whence
                $_SESSION['whence'] = $_SESSION['whither'];
            };


	    D_DisplayHeaderTable();
            $_SESSION['pageShowing'] = "POR";
	    G_POR();
	    break;
	case 32: // POR form - new data
            $_SESSION['textPOR']=$_POST['markingPOR'];
	    D_DisplayHeaderTable();
            I_StudentCase();
	    G_POR();
	    break;
	case 40: // Comms and OTC tab clicked 


            // Record usage here ...
            // Record usage here ...
            $nameOfThisTab = "Comms";
            if ($fromtab){


                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // USAGE section (to ScripUsageSM_ITabs):
                // Where we're just about to go to ...	
                $_SESSION['whither'] = $nameOfThisTab;

                // Read the time and date ...
                $time = time ();
                if ($_SESSION['lastTime']){
                    $timeSpent = $time - $_SESSION['lastTime'];
                };
                $_SESSION['lastTime'] = $time;

                $time = date('G:i:s', $time);
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'CaseID'=>$_SESSION['caseID'], 'RealStudent'=>$_SESSION['realStudent'], 
                              'TimeSpent'=>$timeSpent,
                              'Whence'=>$_SESSION['whence'], 'Whither'=>$_SESSION['whither']);

                $db->insert("ScripUsageSM_ITabs", $text);

                // ... and finally store the old whither as the new whence
                $_SESSION['whence'] = $_SESSION['whither'];
            };


	    D_DisplayHeaderTable();
            $_SESSION['pageShowing'] = "Comms";
	    H_Comms();
	    break;

	case 42: // OTC and Comms - new data
            // Either a single variable or an array ...
            if ($_SESSION['multiple']) {
                unset($_SESSION['seriousErrorType']);
                $_SESSION['seriousErrorType']=$_POST['seriouserrortype'];
            };

            $_SESSION['moderatorHasPressedIncludeData'] = 1;

            if (!$_SESSION['multiple']) { $_SESSION['seriousErrorType']=$_POST['seriouserrortype']; };
            $_SESSION['OTCOODbox']=$_POST['OTCOODbox'];
            $_SESSION['OTCOODbox2']=$_POST['OTCOODbox2'];
            $_SESSION['OTCNegative']=$_POST['OTCNegative'];
            $_SESSION['OTCCDSig']=$_POST['OTCCDSigbox'];
            $_SESSION['OTCNegative2']=$_POST['OTCNegative2'];
            $_SESSION['OTCWrongQtyLegal']=$_POST['OTCWrongQtyLegal'];
            $_SESSION['OTCWrongQtyLegal2']=$_POST['OTCWrongQtyLegal2'];
            $_SESSION['OTCFatal']=$_POST['OTCFatal'];
            $_SESSION['failedNA']=$_POST['failedNA'];
            $_SESSION['AAI']=$_POST['AAI'];
            $_SESSION['AAI2']=$_POST['AAI2'];
            $_SESSION['textOTC']=$_POST['textOTC'];
            $_SESSION['correctP']=$_POST['correctP'];
            $_SESSION['itemsOfInfo']=$_POST['itemsOfInfo'];
            $_SESSION['OTCQ1']=$_POST['OTCQ1'];
            $_SESSION['OTCQ2']=$_POST['OTCQ2'];

            // From the marksheet ...
            $_SESSION['initiate']=$_POST['initiate'];
            $_SESSION['gather']=$_POST['gather'];
            $_SESSION['explain']=$_POST['explain'];
            $_SESSION['conclude']=$_POST['conclude'];
            $_SESSION['answer']=$_POST['answer'];
            $_SESSION['communicate']=$_POST['communicate'];
            $_SESSION['bonus']=$_POST['bonus'];
            $_SESSION['competent']=$_POST['competent'];

            $_SESSION['OTCcomments']=$_POST['textOTC'];
            $_SESSION['commentsAudit']=$_POST['textAudit'];
            $_SESSION['seriousErrorCounselling']=$_POST['seriouserrorcounselling'];
            D_DisplayHeaderTable();

            H_Comms();
	    break;

        case 46:
            D_DisplayHeaderTable();
            CH_ShowChecking();
            break;

	case 50: // Choose user or finished 

            $db=new dbabstraction();
            $db->connect() or die ($db->getError());

            // Which scrips to offer to marker? Called by D_
            $_SESSION['finished']=$_POST['finished'];
            $_SESSION['moderated']=$_POST['moderated'];
            $_SESSION['released']=$_POST['released'];
            $_SESSION['location']=$_POST['location'];
            $_SESSION['studentrecordID']=$_POST['theSRecord'];
            $_SESSION['selectYearGroup'] = $_POST['selectYearGroup'];
            $_SESSION['selectLocation'] = $_POST['selectLocation'];

            // Can now look the record ID up in the correct record table to find the letter ...

            $condition = array('RecordID'=>$_SESSION['studentrecordID']);
            switch ($_SESSION['categoryToMod']) {
                case 1: $table=$db->select("ScripStudentRecord",$condition); break;
                case 2: $table=$db->select("ScripStudentRecord",$condition); break;
                case 3: $table=$db->select("ScripStudentRecord",$condition); break;
                case 4: $table=$db->select("ScripStudentRecordExam",$condition); break;
                case 5: $table=$db->select("ScripStudentRecordExam",$condition); break;
                case 6: $table=$db->select("ScripStudentRecordExam",$condition); break;
                case 7: $table=$db->select("ScripStudentRecord",$condition); break;
                case 8: $table=$db->select("ScripStudentRecordHome",$condition); break;
            };

            // Array $table should now contain the case ID (e.g. DE5) for the record number (primary key from the relevant ScripStudentRecord file); read it ...
            if (count($table)) {
                foreach ($table as $row){
                    $_SESSION['caseID'] = $row['CaseID'];
                    $_SESSION['theSeriousPenalty'] = $row['SeriousPenalty'];
                };
            } else {
                // Null table - this should only happen when $_SESSION['studentrecordID'] is not set (which happens e.g. when
                // we have chosen a student but not yet chosen a case for that student). Once it 
                // is set, then there should be a caseID (and we should be in the IF part); set and no ID => error message ...
                if ($_SESSION['studentrecordID']) {
                    print("Error - record number ".$_SESSION['studentrecordID']." does not exist.<br><br>");
                    print("Do you have more than one ScripWare window open? If so, close all but one.");
                };
            };

//print("<br>_SESSION['theSeriousPenalty'] read from SSR ($_SESSION['studentrecordID']) is ".$_SESSION['theSeriousPenalty']);

	    $_SESSION['categoryToMod']=$_POST['categoryToMod'];
        // Clear variables 
            unset ($_SESSION['textMain'], $_SESSION['textChanges'], $_SESSION['textCD'],
                   $_SESSION['textPOR'], $_SESSION['textOTC'], $_SESSION['finalScore'],
                   $_SESSION['Fatal'],$_SESSION['OTCFatal']);
            unset($_SESSION['OTCOODbox'],$_SESSION['OTCOODbox2']);
            unset($_SESSION['OTCNegative'],$_SESSION['OTCNegative2']);
            unset($_SESSION['OTCWrongQtyLegal'],$_SESSION['OTCWrongQtyLegal2']);
            unset($_SESSION['OTCFatal']);
            unset($_SESSION['failedNA']);
            unset($_SESSION['AAI'], $_SESSION['AAI2']);
            unset($_SESSION['OTCCDSig']);
            unset($_SESSION['textOTC']);
            unset($_SESSION['seriousErrorType']);
            unset($_SESSION['correctP'], $_SESSION['itemsOfInfo'], $_SESSION['OTCQ1'], $_SESSION['OTCQ2']);

            // From the marksheet ...
            unset($_SESSION['initiate'], $_SESSION['gather'], $_SESSION['explain'], $_SESSION['conclude'], $_SESSION['answer'], $_SESSION['communicate'], $_SESSION['bonus'], $_SESSION['competent']);

            unset($_SESSION['OTCcomments']);
            unset($_SESSION['seriousErrorCounselling']);
//   	    J_Finished($finished);

            if ($_SESSION['MorS']!="S") { // Do NOT do this for students!!
                I_StudentCase();
            };

            ZB_MaxScore();
            D_DisplayHeaderTable();
//            if ($_SESSION['pageShowing']=="Main") E_Main();
//            if ($_SESSION['pageShowing']=="CD") F_CD();
//            if ($_SESSION['pageShowing']=="POR") G_POR();
//            if ($_SESSION['pageShowing']=="Comms") H_Comms();


            $db=new dbabstraction();
            $db->connect() or die ($db->getError());

            // This is what is fired when the student has chosen a case; it is therefore here that the usage is written ...

            // USAGE section:
            // Read the time and date ...
            $time = date('G:i:s');
            $date = date('Y-n-j');
            $browser = $_SERVER['HTTP_USER_AGENT'];
            $ipAddress = $_SERVER['REMOTE_ADDR'];
            $os = ""; // ... set blank until we discover where it is

            // New script has been chosen ...
            $action = "Mode";
            // ... also, re-set these two variables ...
            $_SESSION['whence'] = "";
            $_SESSION['whither'] = "";

            // Write usage to ScripUsage ...
            $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                          'Mode'=>$_SESSION['mode'],
                          'CaseID'=>$_SESSION['caseID'], 'RealStudent'=>$_SESSION['realStudent'], 
                          'Action'=>$action,
                          'Browser'=>$browser, 'IPAddress'=>$ipAddress, 'OS'=>$os);
            $db->insert("ScripUsageSM_I", $text);

	    break;
            
        case 51: // This is the generic code version ...
//print($_SESSION['mode']);
            // $_SESSION['mode'] can be one of P, E or U
            // For P: There is a variable week (form not yet determined)
            // For E: There is a variable DEF which can have values D, E or F
            // For U: There is a variable size and a variable number

            // Write Usage if in mode U and the Choose button has fired (don't do it for other reasons e.g. pressing F5) ...
            if ($_SESSION['mode'] == "U" && $ChooseGC == "Choose"){
                // Write usage for unsupervised mode ...
                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // Read the time and date ...
                $time = date('G:i:s');
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'RealStudent'=>$_SESSION['realStudent'], 
                              'NExercises'=>$_SESSION['nResults'], 
                              'NGroups'=>$_POST['number'], 'GroupsOf'=>$_POST['size']);

                $db->insert("ScripUsageSM_GUnsupervised", $text);
            };

            if ($_SESSION['mode'] == "E"){
                // This is what we want to store in USAGE ... $_SESSION['DEFGC'] (which is one of D, E or F)
                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // Read the time and date ...
                $time = date('G:i:s');
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'RealStudent'=>$_SESSION['realStudent'], 
                              'ExamType'=>$_POST['DEF'], 'ExamCode'=>$_SESSION['examcode']);

                $db->insert("ScripUsageSM_GExam", $text);
            };


            D_DisplayHeaderTableGC();

            // In order to jump to the next section (whcih is showing some summaries, we need three things to be set:
            // 1. The mode (to either PEU);
            // 2. The sub-mode which differs according to what the mode is;
            // 3. The student ID.
//            logic1 = $_SESSION['mode'];
//print($_SESSION['mode']);
            switch ($_SESSION['mode']){
                case "P":
//                logic2 = XXX;
                break;

                case "E":
//                logic2 = XXX;
                break;

                case "U":
//                logic2 = XXX;
                break;
            };
//            logic3 = XXX;
//            if (logic1 && logic2 && logic3){
                // Jump to the function for the summaries  
//            };


            break;

        case 54: // This is the calculations version ...

            // $_SESSION['mode'] can be one of P, E or U
            // For P: There is a variable week (form not yet determined)
            // For E: There is a variable DEF which can have values D, E or F
            // For U: There is a variable size and a variable number

            // Write Usage if in mode U and the Choose button has fired (don't do it for other reasons e.g. pressing F5) ...
            if ($_SESSION['mode'] == "U" && $ChooseGC == "Choose"){
                // Write usage for unsupervised mode ...
                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // Read the time and date ...
                $time = date('G:i:s');
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'RealStudent'=>$_SESSION['realStudent'], 
                              'NExercises'=>$_SESSION['nResults'], 
                              'NGroups'=>$_POST['number'], 'GroupsOf'=>$_POST['size']);

                $db->insert("ScripUsageSM_GUnsupervised", $text);
            };

            if ($_SESSION['mode'] == "E"){
                // This is what we want to store in USAGE ... $_SESSION['DEFGC'] (which is one of D, E or F)
                $db=new dbabstraction();
                $db->connect() or die ($db->getError());
 
                // Read the time and date ...
                $time = date('G:i:s');
                $date = date('Y-n-j');

                // Write tab information ...
                $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                              'RealStudent'=>$_SESSION['realStudent'], 
                              'ExamType'=>$_POST['DEF'], 'ExamCode'=>$_SESSION['examcode']);

                $db->insert("ScripUsageSM_GExam", $text);
            };

            if (isset($_POST['caseID']) && $_SESSION ['caseIDinSelect'] != $_POST['caseID']){
                // Change the two-letter code ...
                $_SESSION ['caseIDinSelect'] = $_POST['caseID'];
                // ... and re-set the question number ...
                $_SESSION ['questionIDinSelect'] = "";
            } else if (isset($_POST['questionID'])){
                // If the two-letter code hasn't been changed, check whether the question number has been changed ...
                $_SESSION ['questionIDinSelect'] = $_POST['questionID'];
            };
  
            D_DisplayHeaderTableCalculations();

            // In order to jump to the next section (which is showing some summaries, we need three things to be set:
            // 1. The mode (to either PEU);
            // 2. The sub-mode which differs according to what the mode is;
            // 3. The student ID.
//            logic1 = $_SESSION['mode'];
            switch ($_SESSION['mode']){
                case "P":
//                logic2 = XXX;
                break;

                case "E":
//                logic2 = XXX;
                break;

                case "U":
//                logic2 = XXX;
                break;
            };
//            logic3 = XXX;
//            if (logic1 && logic2 && logic3){
                // Jump to the function for the summaries  
//            };


            break;


        case 254:
        // Write the comments to the appropriate table (ScripCalculationsAnswersModerated if in P mode or ScripCalculationsAnswersExamModerated if in E mode):
        // Look at $_SESSION['comment']
//print("POST");
//print_r($_POST);
//print("end POST<br>");

        $newComments = $_POST['comment'];
        $newPenalties = $_POST['penalty'];
        $theQuestionID = $_POST['thequestionid'];
        $theCaseID = $_POST['thecaseid'];

//print_r($newComments); print("<br>");print_r($newPenalties);print("<br>");

        D_WriteCalculationsComments($theCaseID, $theQuestionID, $newComments, $newPenalties);

        break;


        case 52: // Write to file from hypertext link
        J_Finished($finished);
        break;

        case 57: // Show the correction in a window of its own
        $rectification = $_GET['rectification']; // ... error number that the user has chosen
        F_DisplayGenericCodeRectification($rectification);
        break;

        case 60: /* choose user or finished */
        $_SESSION['finished']=$_POST['finished'];
	    $_SESSION['moderated']=$_POST['moderated'];
	    $_SESSION['released']=$_POST['released'];
	    $_SESSION['studentrecordID']=$_POST['theSRecord'];
        /* Clear variables */
            unset ($_SESSION['textMain'], $_SESSION['textChanges'], $_SESSION['textCD'],
                   $_SESSION['textPOR'], $_SESSION['textOTC']);
//   	    J_Finished($finished);
            I_StudentCase();
	    D_DisplayHeaderTable();
            ZB_MaxScore();
            if ($_SESSION['pageShowing']=="Main") E_Main();
            if ($_SESSION['pageShowing']=="CD") F_CD();
            if ($_SESSION['pageShowing']=="POR") G_POR();
            if ($_SESSION['pageShowing']=="Comms") H_Comms();
	    break;

        case 200:
            $_SESSION['PageToShow'] = $side;
            Y_Show("Script");
            break;

        case 300:
            Y_Show("Label");
            break;

	default: print("<P>Error in tab number</P>");
    };
};
Q_html_end();

/****************************************************************/
/* logical end of program - function declarations follow 	*/
/* in alphabetical order 					*/
/****************************************************************/

// Write the HTML for the Head of the Page

function A_html_begin($title) {
    global $tab;
//print("Penalty is ".$_SESSION['theSeriousPenalty']); 
    print ("<HTML>\n");
    print ("<HEAD>\n");
    if ($title) { print ("<TITLE>$title</TITLE>\n"); };
    print('<link href="pharmacy_css.css" rel="stylesheet"');
    print(' type="text/css">');
    print('<meta http-equiv="Content-Type"');
    print(' content="text/html; charset=iso-8859-1">');
    print ("</HEAD>\n");
    print ("<BODY>\n");
    print("<TABLE ALIGN='CENTER' BGCOLOR='#003163'>\n");

//    print("<TR>\n<TD><IMG SRC=images/keep.jpg></TD>\n");
//    print("<TD><IMG SRC=images/school.jpg></TD>\n</TR>\n");
//    print("<TR><TD colspan=2><TABLE ALIGN=CENTER>\n");

        print("<tr><td>");
        print("<h1 align='center'>School of Pharmacy");
        print("</h1>");
        print("<a href=javascript:void(0)\n");

//        print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=70\'');
        print('onClick="open(\'ScripWare.php?tab=70\'');
        print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,width=700,height=400')\">");
        print("<div id='noimagebackground'>");
        print("<img src=images/help.gif width='30' height='30' border='0'");
        print("align='middle'>");
        print("</div>");

        print("</a>");
        print("<hr width='500' size='1'>");
        print("</TD></TR>\n");
 	print("<TR><TD align='center'>\n\n<TABLE border=1>\n");     
// print("<tr>");
};


// Display the title and logon screen 
// Called if this is the first run of the program 
function B_startprog() {
    global $case;

    print("<TR>\n<TD>A&nbsp;</TD>\n\n");
    print("<TD>");
    print("<IMG SRC=images/Titlescreen.jpg></TD></TR>");
    print("<TR><TD colspan=2><FONT COLOR=#FFFFFF SIZE=4>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=0>");
    print("&nbsp;&nbsp;&nbsp;Please log on:"); 
    print("&nbsp;&nbsp;&nbsp;username: ");
    print("<INPUT TYPE=TEXT SIZE=10 MAXCHAR=10 NAME=username>");
    print("&nbsp;&nbsp;&nbsp;password: ");
    print("<INPUT TYPE=PASSWORD SIZE=10 MAXCHAR=10 NAME=password>");
    print("&nbsp;&nbsp;&nbsp;");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR>");
};


function C_checkUserAllVersions($theUsername,$thePassword){
    // There are three versions of checkuser - go to the correct version:

    if ($_SESSION['variety'] == 1){
        C_checkuser($theUsername,$thePassword);
    };

    if ($_SESSION['variety'] == 2){
        // Now run the code for generic comments ...
        C_checkuserGC($theUsername,$thePassword);
    };

    if ($_SESSION['variety'] == 3){
        // Now run the code for calculations ...
        C_checkuserCalculation($theUsername,$thePassword);
    };

};


function C_checkuser($theusername, $thepassword) {
    // 



    // IMPORTANT NOTE: The code here should be amended to only show students in year group $_SESSION['chosenTime'] and at location  $_SESSION['chosenPlace']

    global $case;
    $tab=90;

// Formerly checked the passwords in this section
//   - now receiving this information from logon.php
//   // Look up username and password - Admin 
//    $db=new dbabstraction();
//    $db->connect() or die ($db->getError());
//    $table=$db->select("ScripConfiguration","AdminUserName='$theusername'");
//    foreach ($table as $row) {
//	$_SESSION['apassword']=$row['AdminPassword'];
//    };

   // Look up username and password - Moderators
//    $table=$db->select("ScripModerator","UserName='$theusername'");
//    foreach ($table as $row)
//    {   $_SESSION['moderatorID']=$row['ModeratorID'];
//	$_SESSION['mfirstname']=$row['FirstName'];
//	$_SESSION['mlastname']=$row['LastName'];
//	$_SESSION['musername']=$row['UserName'];
//	$_SESSION['mpassword']=$row['Password'];
//    };

   // Look up username and password - Students
//    $table=$db->select("ScripStudent","UserName='$theusername'");
//    foreach ($table as $row)
//    {   $_SESSION['studentID']=$row['StudentID'];
//	$_SESSION['firstname']=$row['FirstName'];
//	$_SESSION['lastname']=$row['LastName'];
//	$_SESSION['username']=$row['UserName'];
//	$_SESSION['spassword']=$row['Password'];
//	$_SESSION['practicecode']=$row['PracticeCode'];
//	$_SESSION['examcode']=$row['ExamCode'];
//	$_SESSION['initials']=$row['initials'];
//    }


    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    if ($_SESSION['table']=="Student") {
        $condition=array("StudentID"=>$_SESSION['ID']);
        $table=$db->select("ScripStudent",$condition);
        foreach ($table as $row) {
            $_SESSION['studentID']=$row['StudentID'];
            $_SESSION['firstname']=$row['FirstName'];
            $_SESSION['lastname']=$row['LastName'];
            $_SESSION['username']=$row['UserName'];
            $_SESSION['spassword']=$row['Password'];
            $_SESSION['practicecode']=$row['PracticeCode'];
            $_SESSION['examcode']=$row['ExamCode'];
            $_SESSION['initials']=$row['initials'];
            $_SESSION['seat']=$row['Seat'];
            $_SESSION['realStudent']=$row['RealStudent'];
            // Put location and yeargroup into the same variable that would be used were location and yeargroup being selected ...
            $_SESSION['selectLocation']=$row['Location']; 
            $_SESSION['selectYearGroup']=$row['YearGroup'];
        };

        $_SESSION['MorS'] = $_SESSION['authority'];
        if ($_SESSION['MorS']=="S") {
            $_SESSION['fullAuthority'] = 0;
        } else {
            $_SESSION['fullAuthority'] = 1;
        };
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
        D_DisplayHeaderTable(); // For current student only
    } else if ($_SESSION['table']=="Moderator") {
        $condition=array("ModeratorID"=>$_SESSION['ID']);
        $table=$db->select("ScripModerator",$condition);
        foreach ($table as $row) {
            $_SESSION['moderatorID']=$row['ModeratorID'];
            $_SESSION['mfirstname']=$row['FirstName'];
            $_SESSION['mlastname']=$row['LastName'];
            $_SESSION['musername']=$row['UserName'];
            $_SESSION['mpassword']=$row['Password'];
        };
        $_SESSION['MorS'] = $_SESSION['authority'];
        $_SESSION['fullAuthority'] = 1;
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
        D_DisplayHeaderTable(); // Of all students
    } else if ($_SESSION['table']=="(NUL)") {
       $table=$db->select("ScripConfiguration");    
        foreach ($table as $row) {
            $_SESSION['apassword']=$row['AdminPassword'];
        };
        $_SESSION['MorS'] = $_SESSION['authority'];
        $_SESSION['fullAuthority'] = 1;
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
        D_DisplayHeaderTable(); // Of all students
    } else {
        print("Shouldn't be here!");
    }; 






//    if ($thepassword == $_SESSION['apassword']) {
//        $_SESSION['MorS'] = "A"; $_SESSION['authority'] = 1;
//        ZJ_correctResultsTable();
//        ZA_byesAndCommsMax();
//        D_DisplayHeaderTable(); // Of all students
//    } else if ($thepassword == $_SESSION['mpassword']) {
//        $_SESSION['MorS'] = "M"; $_SESSION['authority'] = 1;
//        ZJ_correctResultsTable();
//        ZA_byesAndCommsMax();
//        D_DisplayHeaderTable(); // Of all students
//    } else if ($thepassword == $_SESSION['spassword']) {
//        $_SESSION['MorS'] = "S"; $_SESSION['authority'] = 0;
//        ZJ_correctResultsTable();
//        ZA_byesAndCommsMax();
//        D_DisplayHeaderTable(); // For current student only
//    } else { // Total failure to find password
//     	print("<TABLE BGCOLOR=#FFFFFF ");
//  	print("ALIGN=CENTER><TR><TD>");
//	print("<FONT color=red ><H2>Incorrect password or username");
//	print("</H2><FONT><P>");
//	print("Username and password are case sensitive.");
//	print("Check the caps lock and try again</P>");
//	print("<P><A HREF='ScripModerate.php'>Try Again</A></P>");
//        print("</TD></TR></TABLE>");
//    }

    $db->close();
};



function C_checkuserGC($theusername, $thepassword) {



    // IMPORTANT NOTE: The code here should be amended to only show studentsin year group $_SESSION['chosenTime'] and at location  $_SESSION['chosenPlace']


    // This is the code that starts the generic part of the program ...

    // THIS IS WHERE THE GENERIC CODE (BASED ON SCRIPMODERATE) BEGINS ...
    global $case;
    $tab=90;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    if ($_SESSION['table']=="Student") {
        $condition=array("StudentID"=>$_SESSION['ID']);
        $table=$db->select("ScripStudent",$condition);
        foreach ($table as $row) {
            $_SESSION['studentID']=$row['StudentID'];
            $_SESSION['firstname']=$row['FirstName'];
            $_SESSION['lastname']=$row['LastName'];
            $_SESSION['username']=$row['UserName'];
            $_SESSION['location']=$row['Location'];
            $_SESSION['yearGroup']=$row['YearGroup'];
            $_SESSION['spassword']=$row['Password'];
            $_SESSION['practicecode']=$row['PracticeCode'];
            $_SESSION['examcode']=$row['ExamCode'];
            $_SESSION['realStudent'] = $row['RealStudent'];
            $_SESSION['initials']=$row['initials'];
            $_SESSION['seat']=$row['Seat'];
        };


        // Now have all information: write usage for ScripUsageSM_G here 
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());

        // Read the time and date ...
        $time = date('G:i:s');
        $date = date('Y-n-j');
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $os = ""; // ... set blank until we discover where it is

        // New script has been chosen ...
        $action = "Entered";

        // Write usage to ScripUsage ...
        $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                      'RealStudent'=>$_SESSION['realStudent'], 
                      'Action'=>$action,
                      'Browser'=>$browser, 'IPAddress'=>$ipAddress, 'OS'=>$os);

        $db->insert("ScripUsageSM_G", $text);

        // Return to code ...
        $_SESSION['MorS'] = $_SESSION['authority'];
        if ($_SESSION['MorS']=="S") {
            $_SESSION['fullAuthority'] = 0;
        } else {
            $_SESSION['fullAuthority'] = 1;
        };
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
        D_DisplayHeaderTableGC(); // For current student only
    } else if ($_SESSION['table']=="Moderator") {
        $condition=array("ModeratorID"=>$_SESSION['ID']);
        $table=$db->select("ScripModerator",$condition);
        foreach ($table as $row) {
            $_SESSION['moderatorID']=$row['ModeratorID'];
            $_SESSION['mfirstname']=$row['FirstName'];
            $_SESSION['mlastname']=$row['LastName'];
            $_SESSION['musername']=$row['UserName'];
            $_SESSION['mpassword']=$row['Password'];
        };
        $_SESSION['MorS'] = $_SESSION['authority'];
        $_SESSION['fullAuthority'] = 1;
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
        D_DisplayHeaderTableGC(); // Of all students
    } else if ($_SESSION['table']=="(NUL)") {
       $table=$db->select("ScripConfiguration");    
        foreach ($table as $row) {
            $_SESSION['apassword']=$row['AdminPassword'];
        };
        $_SESSION['MorS'] = $_SESSION['authority'];
        $_SESSION['fullAuthority'] = 1;
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
        D_DisplayHeaderTableGC(); // Of all students
    } else {
        print("Shouldn't be here!");
    }; 

    $db->close();
};



function C_checkuserCalculation($theusername, $thepassword) {

    // This is the code that starts the calculations part of the program ...



    // IMPORTANT NOTE: The code here should be amended to only show studentsin year group $_SESSION['chosenTime'] and at location  $_SESSION['chosenPlace']



    // THIS IS WHERE THE CALCULATION CODE (BASED ON SCRIPMODERATE) BEGINS ...
    global $case;
    $tab=90;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    if ($_SESSION['table']=="Student") {
print("Student view");
        $condition=array("StudentID"=>$_SESSION['ID']);
        $table=$db->select("ScripStudent",$condition);
        foreach ($table as $row) {
            $_SESSION['studentID']=$row['StudentID'];
            $_SESSION['firstname']=$row['FirstName'];
            $_SESSION['lastname']=$row['LastName'];
            $_SESSION['username']=$row['UserName'];
            $_SESSION['spassword']=$row['Password'];
            $_SESSION['practicecode']=$row['PracticeCode'];
            $_SESSION['examcode']=$row['ExamCode'];
            $_SESSION['realStudent'] = $row['RealStudent'];
            $_SESSION['initials']=$row['initials'];
            $_SESSION['seat']=$row['Seat'];
        };

/*
        // Now have all information: write usage for ScripUsageSM_G here 
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());

        // Read the time and date ...
        $time = date('G:i:s');
        $date = date('Y-n-j');
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $os = ""; // ... set blank until we discover where it is

        // New script has been chosen ...
        $action = "Entered";

        // Write usage to ScripUsage ...
        $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                      'RealStudent'=>$_SESSION['realStudent'], 
                      'Action'=>$action,
                      'Browser'=>$browser, 'IPAddress'=>$ipAddress, 'OS'=>$os);

        $db->insert("ScripUsageSM_G", $text);
*/



/*
        // Return to code ...
        $_SESSION['MorS'] = $_SESSION['authority'];
        if ($_SESSION['MorS']=="S") {
            $_SESSION['fullAuthority'] = 0;
        } else {
            $_SESSION['fullAuthority'] = 1;
        };
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
*/
        D_DisplayHeaderTableCalculations(); // For current student only

    } else if ($_SESSION['table']=="Moderator") {
print("Moderator view");


        $condition=array("ModeratorID"=>$_SESSION['ID']);
        $table=$db->select("ScripModerator",$condition);
        foreach ($table as $row) {
            $_SESSION['moderatorID']=$row['ModeratorID'];
            $_SESSION['mfirstname']=$row['FirstName'];
            $_SESSION['mlastname']=$row['LastName'];
            $_SESSION['musername']=$row['UserName'];
            $_SESSION['mpassword']=$row['Password'];
        };
        $_SESSION['MorS'] = $_SESSION['authority'];
        $_SESSION['fullAuthority'] = 1;


        // Look for all the questions and then the students who have done each question ...








/*
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
*/
        D_DisplayHeaderTableCalculations(); // Of all students

    } else if ($_SESSION['table']=="(NUL)") {
/*
       $table=$db->select("ScripConfiguration");    
        foreach ($table as $row) {
            $_SESSION['apassword']=$row['AdminPassword'];
        };
        $_SESSION['MorS'] = $_SESSION['authority'];
        $_SESSION['fullAuthority'] = 1;
        ZJ_correctResultsTable();
        ZA_byesAndCommsMax();
        D_DisplayHeaderTableCalculations(); // Of all students
*/
    } else {
        print("Shouldn't be here!");
    }; 


    $db->close();
};










function D_DisplayHeaderTableCalculations() {
    global $case;
    global $_POST;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

//    ZB_MaxScore();

    // Read the time and date ...
    $time = date('G:i:s');
    $date = date('Y-n-j');
    $browser = $_SERVER['HTTP_USER_AGENT'];
    $ipAddress = $_SERVER['REMOTE_ADDR'];
    $os = ""; // ... set black until we discover where it is

    // Write usage to ScripUsage ...
    $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                  'StudentRecord'=>$_SESSION['studentrecordID'], 'CaseID'=>$_SESSION['currentscrip'],
                  'Browser'=>$browser, 'IPAddress'=>$ipAddress, 'OS'=>$os);
//    $db->insert("ScripUsageSM", $text);

    print("<tr><td valign='top' align='center'>\n");
    print("<form action='ScripModerate.php' method='post'>\n");
    print("<input type='hidden' name=tab value='54'>");
//    print("<FORM ACTION=ScripModerate.php?tab=50 METHOD=POST>\n");
    print("<div class=tablegrey>");
    print("\n\n");
    print("<table bgcolor='#eeeeee' border='1' width='100%' align='center'>");
    print("<tr>\n");

    // Set default state for following set of radio buttons ...
    if (!isset($_SESSION['categoryToMod'])) { // Not set?
        if ($_SESSION['MorS'] == "S"){
            $_SESSION['categoryToMod'] = 7; // 7 for students
        } else {
            $_SESSION['categoryToMod'] = 1; // 1 for others
        };
    };

/*
    // Read THE VALUE of the serious penalty from the ScripStudentRecord or ScriptStudentRecordExam table for this case ...
    $condition = array('RecordID'=>$_SESSION['studentrecordID']);
    switch($_SESSION['mode']){
        case "P":
        $table=$db->select("ScripStudentRecord", $condition);
        break;

        case "E":
        $table=$db->select("ScripStudentRecordExam", $condition);
        break;
    };

    if ($_SESSION['mode'] == "P" || $_SESSION['mode'] == "E"){
        foreach ($table as $row){
            $_SESSION['theSeriousPenalty'] = $row['SeriousPenalty'];
        };
    };
*/
    // Values: 1 - Practice - Unmarked
    //         2 - Practice - Mark originals
    //         3 - Practice - Mark moderated
    //         4 - Examination - Unmarked
    //         5 - Examination - Mark originals
    //         6 - Examination - Marked moderated
    //
    //         7 - Practice - Marked and released
    //         8 - Unsupervised

    $defaultMode = "P";
    $defaultMark = "U";

    // Overall logic: If we change the value of a parameter (i.e. the POST data is different to the SESSION data), we set the
    // the SESSION data to the POST data and then unset all the lower-ranking data ...

    // Logic: If a student record has been set in the POST data, copy it to the SESSION data ...
    if ($_POST['theSRecord']) {
        $_SESSION['theSRecord'] = $_POST['theSRecord'];
    };

    // Logic: If a student ID has been set in the POST data, copy it to the SESSION data and unset the SRecord
    if ($_POST['studentID']) {
        if ($_POST['studentID'] != $_SESSION['studentID']) {
            $_SESSION['theSRecord'] = "";
        };
        $_SESSION['studentID'] = $_POST['studentID'];
    };

    // Logic: If we change mark, then we need to re-set studentID and theSRecord ...
    if ($_POST['mark']) {
        if ($_POST['mark'] != $_SESSION['mark']) {
            $_SESSION['studentID'] = "";
            $_SESSION['theSRecord'] = "";
        };
        $_SESSION['mark'] = $_POST['mark'];
    };

    // Logic: If we change mode, then we need to re-set all the other options ...
    if ($_POST['mode']) { // Set?
        if ($_POST['mode'] != $_SESSION['mode']) { // Changed?
            $_SESSION['mark'] = $defaultMark;
            $_SESSION['studentID'] = "";
            $_SESSION['theSRecord'] = "";
        };
        $_SESSION['mode'] = $_POST['mode'];
    };

    // Set defaults if variables are nul ...
    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
        if (!$_SESSION['mode']) { $_SESSION['mode'] = $defaultMode; };
        if (!$_SESSION['mark']) { $_SESSION['mark'] = $defaultMark; };
    } else {
        if (!$_SESSION['mode']) { $_SESSION['mode'] = "P"; };
        $_SESSION['mark'] = "";
    };

    // Define the various combinations of mode and mark with one variable (categoryToMod) ... 
    if ($_SESSION['mode'] == "P" && $_SESSION['mark'] == "U") { $_SESSION['categoryToMod'] = 1; };
    if ($_SESSION['mode'] == "P" && $_SESSION['mark'] == "O") { $_SESSION['categoryToMod'] = 2; };
    if ($_SESSION['mode'] == "P" && $_SESSION['mark'] == "M") { $_SESSION['categoryToMod'] = 3; };
    if ($_SESSION['mode'] == "E" && $_SESSION['mark'] == "U") { $_SESSION['categoryToMod'] = 4; };
    if ($_SESSION['mode'] == "E" && $_SESSION['mark'] == "O") { $_SESSION['categoryToMod'] = 5; };
    if ($_SESSION['mode'] == "E" && $_SESSION['mark'] == "M") { $_SESSION['categoryToMod'] = 6; };

    if ($_SESSION['mode'] == "P" && !$_SESSION['mark']) { $_SESSION['categoryToMod'] = 7; }; // Practice (moderated)
    if ($_SESSION['mode'] == "U" && !$_SESSION['mark']) { $_SESSION['categoryToMod'] = 8; }; // Home

/*
    $condition=array("FormID"=>$_SESSION['formID']);
    $table=$db->select("ScripForms",$condition);
    foreach ($table as $row) {
        $_SESSION['group']=$row['GroupID'];
    };
*/

    // NEW CELL 1 - Choose Practice, Examination or Unsupervised ...
    // Open cell ...
    print("<td valign='top' width='25%'>");
    // ... radio button ...
    print("<input type='radio' name='mode' value='P' ");
    // ... already set? ...
    if ($_SESSION['mode'] == "P") { print("checked "); };
    // ... onclick ...
    print("onClick='javascript:document.forms[0].submit()'>");
    print("<font color=ffff00>Practice</font><br>");

    // Moderator is allowed to see exams, students home ...   
    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
        print("<input type='radio' name='mode' value='E' ");
        if ($_SESSION['mode'] == "E") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Examination</font><br>");
    } else {
        print("<input type='radio' name='mode' value='U' ");
        if ($_SESSION['mode'] == "U") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Unsupervised</font><br>");
    };
    // N.B. Students can see generic comments on exams but not precise details ...

//    print("<input type='radio' name='mode' value='C' ");
//    if ($_SESSION['mode'] == "C") { print("checked "); };
//    print("onClick='javascript:document.forms[0].submit()'>");
//    print("<font color=ffff00>Checking</font><br>");

    print("</td>");

    // NEW CELL 2 - Choose Unmarked, Mark originals or Mark moderated ...
    print("<td valign='top' width='25%'>");
    
    // If either administrator or moderator, set mark to one of U, O or M ...
    if (($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M")) {
        print("<input type='radio' name='mark' value='U' ");
        if ($_SESSION['mark'] == "U") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Unmarked</font><br>");

        print("<input type='radio' name='mark' value='O' ");
        if ($_SESSION['mark'] == "O") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Marked - Original results</font><br>");

        print("<input type='radio' name='mark' value='M' ");
        if ($_SESSION['mark'] == "M") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Marked - Moderated</font><br>");
    };

    // Message to student about moderation ...
    if (!($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M")) {
        print("<font color=ffff00>");
        if ($_SESSION['mode'] == "P") {
            print("These marks HAVE been moderated by an academic.");
        };
        if ($_SESSION['mode'] == "U") {
            print("These marks have NOT been moderated by an academic.");
        };
        print("</font>");
    };

    print("</td>");

    // CALCULATIONS: STUDENT is offered two-letter codes, chooses one and is then shown all questions in that two-letter code;
    //               MODERATOR is offered two-letter codes, chooses one, is offered a question number (from 1 to 10), chooses one 
    //               and is shown all student attempts at that one particular question (e.g. Question 7 in DC)








/*
    // NEW CELL 2A
    print("<td width='25%' align='center'>");

    if (($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") && $_SESSION['mode'] != "C") {

        // Need a set of all the different locations in ScripStudent ...
        $allLocations = array();
        $table=$db->select("ScripStudent");
        foreach ($table as $row){
            // Read an entry from the table ...
            $thisLocation = $row['Location'];
            $alreadyNoted = false;

            // Loop over contents of $allLocations ...
            for ($j = 0; $j < count($allLocations); $j++){
                if ($allLocations[$j] == $thisLocation){ $alreadyNoted = true; };
            };

            if (!$alreadyNoted) {
                // If not previously in $allLocations, add it now ...
                array_push($allLocations, $thisLocation);
            };
        };
       // ... all the locations are now in $allLocations

        // Drop-down menu for YEAR GROUP ...
        print("<select name='selectYearGroup' onChange='javascript:document.forms[0].submit()'>");
        print("<option value='0'>Select year group ...</option>");
        for ($i = 0; $i < 4; $i++){
            $ip1 = $i + 1;
            print("<option value=$ip1");
            if ($ip1 == $_SESSION['selectYearGroup']){ print(" selected"); };
            print(">Year $ip1</option>");
        };
        print("</select>");

        print("<br><br>");

        // Drop-down menu for LOCATION ...
        print("<select name='selectLocation' onChange='javascript:document.forms[0].submit()'>");
        print("<option value=''>Select location ...</option>");
        for ($i = 0; $i < count($allLocations); $i++){
            $ip1 = $i + 1;
            print("<option value=$allLocations[$i]");
            if ($allLocations[$i] == $_SESSION['selectLocation']) { print(" selected"); };
            print(">$allLocations[$i]</option>");
        };
        print("</select>");

    } else {

        print("&nbsp;");

    };

    print("</td>\n");
*/






    // NEW CELL 3 - Choose a two-letter code from the relevant ScripStudentRecord (i.e. ScripStudentRecordCalculations,
    // ScripStudentRecordCalculationsExam or ScripStudentRecordCalculationsHome) using a drop-down list ...

    print("<td valign='middle' width='25%'>");

    // If a moderator and categoryToMod is < 7 then we choose the correct table and read from it according to the 
    // parameters set by the radio buttons above ...

//print("categoryToMod is " . $_SESSION['categoryToMod'] . "<br><br>");

    switch ($_SESSION['categoryToMod']) {
 
        // For moderator: practice results that have not been moderated ...
        case 1:
        // Look in ScripStudentRecordCalculations for calculation scripts that have been finished ...            
        $condition=array("FinishedYN"=>1, "ModeratedYN"=>0);
        $xTable=$db->select("ScripStudentRecordCalculations", $condition);
        break;

        // For moderator: practice results that HAVE been moderated ...
        case 2: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
        $xTable=$db->select("ScripStudentRecordCalculations",$condition, "CaseID");
        break;

        // For moderator: moderated results that HAVE been moderated ...
        case 3: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
        $xTable=$db->select("ScripStudentRecordCalculations",$condition, "CaseID");
        break;

        // As case 1 but exam results ...
        case 4: $condition=array("FinishedYN"=>1, "ModeratedYN"=>0);
        $xTable=$db->select("ScripStudentRecordCalculationsExam",$condition, "CaseID");
        break;

        // As case 2 but exam results ...
        case 5: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
// WARNING - Wrong way round
//        $xTable=$db->select("ScripStudentRecordExamCalculations",$condition, "CaseID");
        $xTable=$db->select("ScripStudentRecordCalculationsExam",$condition, "CaseID");
        break;

        // As case 3 but exam results ...
        case 6: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
        $xTable=$db->select("ScripStudentRecordCalculationsExam",$condition, "CaseID");
        break;

        // For student: moderated practice ...
        case 7:
        $_SESSION['studentID'] = $_SESSION['ID'];
        $condition=array("ModeratedYN"=>1, "FinishedYN"=>1, "ReleasedYN"=>1, "StudentID"=>$_SESSION['studentID']);    
        $xTable=$db->select("ScripStudentRecordCalculations",$condition);
        break;

        // For student: home ...
        case 8:
// March 2012
        $_SESSION['studentID'] = $_SESSION['ID'];
        $condition=array("FinishedYN"=>1, "StudentID"=>$_SESSION['studentID']);
        $xTable=$db->select("ScripStudentRecordCalculationsHome",$condition);
//print_r($condition);
//print_r($xTable);
        break;
    };

    // List all the two letter codes that occur in the relevant table: we want one listing of each calculation script (only) ... 

//print_r($xTable);

//print("Details of calculations that fulfil requirements:<br><br>"); print("<pre>"); print_r($xTable); print("</pre>"); 

    for ($i = 0; $i < count($xTable); $i++){
        $notFound = true;
        for ($j = 0; $j < count($tableCalculationsOnly); $j++){
            // If already in there, we don't want to add it and we don't even want to go any further down $tableCalculationsOnly (hence break) ...
            if ($xTable[$i]['CaseID'] == $tableCalculationsOnly[$j]){
                $notFound = false; break;
            };
        };
        // Not in table so far - add it ...
        if ($notFound) {
            $tableCalculationsOnly[] = $xTable[$i]['CaseID'];
        };
    };

    // Found any cases? Select
    if (count($tableCalculationsOnly)) {
        print("<select name='caseID' onchange='javascript:document.forms[0].submit()'>");
        print("<option value=''>Select from ...");
        for ($i = 0; $i < count($tableCalculationsOnly); $i++){
            print("<option value=".$tableCalculationsOnly[$i]);
            if ($_SESSION['caseIDinSelect'] == $tableCalculationsOnly[$i]) { print(" selected"); };
            print(">".$tableCalculationsOnly[$i]);
        };
        print("</select>");
    } else {
        print("<p align='center'>");
        print("<font color='#ffffff'>No calculation scripts found</font>");
        print("</p>");
    };

    print("</td>");

    // NEW CELL 4 - CHOOSE QUESTION NUMBER ...

    // If the user is a moderator, we offer all the questions in the chosen two-letter code (1-10).
    // If a student, this cell is empty.

    // Ideally, we do not offer a number if there are no questions left with that number
    // Look down ScripStudentRecordCalculations (or equivalent) and see if there are any questions left e.g. that have not been moderated
    // Only applies to original (can always re-moderate a moderated calculation) ...

    // We have an array called answerToBeModerated. If element i is 1 then there are some questions of that number to be moderated ...
    for ($i = 0; $i < 10; $i++){ $answerToBeModerated[$i + 1] = 0; }; // ... assume there are none in the first instance

    // Look for the numbers in the chosen two-letter code ...

    switch ($_SESSION['categoryToMod']) {
        case 1:
        // Practice - original: Finished, but NOT moderated
        $condition = array("CaseID"=>$_SESSION['caseIDinSelect'], "FinishedYN"=>1, "ModeratedYN"=>0);
        $ntable = $db->select("ScripStudentRecordCalculations", $condition);
        break;

        case 2:
        // Practice - original: Finished AND moderated
        $condition = array("CaseID"=>$_SESSION['caseIDinSelect'], "FinishedYN"=>1, "ModeratedYN"=>1);
        $ntable = $db->select("ScripStudentRecordCalculations", $condition);
        break;

        case 3:
        // Practice - moderated: Finished AND moderated
        $condition = array("CaseID"=>$_SESSION['caseIDinSelect'], "FinishedYN"=>1, "ModeratedYN"=>1);
        $ntable = $db->select("ScripStudentRecordCalculations", $condition);
        break;

        case 4:
        // Exam - original: Finished, but NOT moderated
        $condition = array("CaseID"=>$_SESSION['caseIDinSelect'], "FinishedYN"=>1, "ModeratedYN"=>0);
        $ntable = $db->select("ScripStudentRecordCalculationsExam", $condition);
        break;

        case 5:
        // Exam - original: Finished AND moderated
        $condition = array("CaseID"=>$_SESSION['caseIDinSelect'], "FinishedYN"=>1, "ModeratedYN"=>1);
        $ntable = $db->select("ScripStudentRecordCalculationsExam", $condition);
        break;

        case 6:
        // Exam - moderated: Finished AND moderated
        $condition = array("CaseID"=>$_SESSION['caseIDinSelect'], "FinishedYN"=>1, "ModeratedYN"=>1);
        $ntable = $db->select("ScripStudentRecordCalculationsExam", $condition);
        break;

        case 7:
        // Student - practice - don't offer question numbers
        break;

        case 8:
        // Student - home - don't offer question numbers
        break;
    };

    // 
    foreach ($ntable as $nrow) {
        // Take the questionID in each entry, look up its number in its exercise ...
        $theQuestionID = $nrow['QuestionID']; // ... Question ID (unique)

        $condition = array ("ID"=>$theQuestionID);
        $mtable = $db->select("ScripCalculationsQuestions", $condition);
        foreach ($mtable as $mrow){
            $questionNumber = $mrow['QuestionNumber']; // ... local number of question within exercise (not unique outside the exercise)
            // Tell the array (by setting the relevant element to 1) that we have a question with this (local) number ...
            $answerToBeModerated[$questionNumber] = 1;
        };
    };


    print("<td valign='middle' width='25%'>\n");

    // Cell not available to students (only moderators and, in principle, administrator) ...
    if (($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M")){
        print("<select name='questionID' onchange='javascript:document.forms[0].submit()'>");
        print("<option value=''>Select from ...");
        for ($i = 0; $i < 10; $i++){
            $iPlusOne = $i + 1;
            if ($answerToBeModerated[$iPlusOne]){
                print("<option value='$iPlusOne'");
                if ($_SESSION['questionIDinSelect'] == $iPlusOne) { print(" selected"); };
                print(">$iPlusOne");
            };
        };
        print("</select>");  
    };

    print("</td>\n");


    // NEW CELL 5 ...
    print("<td width='25%' align='center'>");
    print("<input type='submit' name='ChooseButton' value='Choose'>");
    print("</td>\n");

    // End the row ...
    print("</tr>\n");

    print("</form>\n");





    // If user has sufficient authority (moderator), then show questions and all answers ... but only if a question number (in $_SESSION['questionIDinSelect']) has been selected ... 
    if (($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") && $_SESSION['questionIDinSelect']){
        // Show all the student answers to the current two-letter code and question:
        // First find which students have finished the two-letter code that the user has chosen ...

        // We know the two-letter code and the question number - this can give us a question ID (which is unique across all
        // questions in all exercises) ...

        // Need to convert e.g. DC9 into DC ...
        $allDigits = "0123456789";
        $twoLetterCodeCharacters = str_split ($_SESSION['caseIDinSelect']);
        for ($j = 0; $j < count($twoLetterCodeCharacters); $j++){
            if (strpos($allDigits, $twoLetterCodeCharacters[$j]) === false) {
                // Character is not a digit, therefore add it to the array
                $letters[] = $twoLetterCodeCharacters[$j];
            };
        };
        // Concatenate the array ...
        $twoLetterCode = implode ("", $letters);

        // Find the ID ...
        $condition = array("ExerciseSet"=>$twoLetterCode, "QuestionNumber"=>$_SESSION['questionIDinSelect']);
        $table=$db->select("ScripCalculationsQuestions", $condition);
        foreach ($table as $row){
            $theQuestionID = $row['ID'];
        };
        // ... now have the unique ID of the question

        // Finds entries for chosen case that have been finished ...
        $condition = array("FinishedYN"=>1, "CaseID"=>$_SESSION['caseIDinSelect']);
        switch($_SESSION['mode']){
            case "P":
            $table=$db->select("ScripStudentRecordCalculations", $condition);
            break;

            case "E":
            $table=$db->select("ScripStudentRecordCalculationsExam", $condition);
            break;

            case "U":
            $table=$db->select("ScripStudentRecordCalculationsHome", $condition);
            break;
        };
        // ... $table has all the entries for a given case that have been finished

        // Make a list of the students in $table (i.e. those who have finished the chosen two-letter code ... and chosen from the right table) 
        foreach ($table as $row){
            $studentIDFinishedThisCalculation[] = $row['StudentID']; // ... add into the array
        };


        // Now need to read the correct one of ScripCalculationsAnswers, ScripCalculationsAnswersExam, ScripCalculationsAnswersExamModerated, 
        // ScripCalculationsAnswersHome, ScripCalculationsAnswersHomeModerated ...

        $condition = array("QuestionID"=>$theQuestionID);

        // Get the answers (correct choice of QuestionID and from correct table):
        switch ($_SESSION['categoryToMod']) {

            // For moderator: practice results that have not been moderated ...
            case 1:
            // Look in ScripStudentRecordCalculations ...
            $condition=array("QuestionID"=>$theQuestionID);
            $xTable=$db->select("ScripCalculationsAnswers", $condition);
            // ... $xTable shows all answers to the chosen question
            break;

            case 2:
            // Look in ScripStudentRecordCalculations ...            
            $condition=array("QuestionID"=>$theQuestionID);
            $xTable=$db->select("ScripCalculationsAnswers",$condition, "CaseID");
            break;

            case 3:
            // Look in ScripStudentRecordCalculationsModerated ...            
            $condition=array("QuestionID"=>$theQuestionID);
            $xTable=$db->select("ScripCalculationsAnswersModerated",$condition, "CaseID");
            break;

            case 4:
            // Look in ScripStudentRecordCalculationsExam ...            
            $condition=array("QuestionID"=>$theQuestionID);
            $xTable=$db->select("ScripCalculationsAnswersExam",$condition, "CaseID");
            break;

            case 5:
            // Look in ScripStudentRecordCalculationsExam ...            
            $condition=array("QuestionID"=>$theQuestionID);
            $xTable=$db->select("ScripCalculationsAnswersExam",$condition, "CaseID");
            break;

            case 6:
            // Look in ScripStudentRecordCalculationsExamModerated ...            
            $condition=array("QuestionID"=>$theQuestionID);
            $xTable=$db->select("ScripCalculationsAnswersExamModerated",$condition, "CaseID");
            break;

            // For student - moderated practice: choose from current student's answers ...
            case 7:
            $_SESSION['studentID'] = $_SESSION['ID'];
            $condition=array("StudentID"=>$_SESSION['studentID']);    
            $xTable=$db->select("ScripCalculationsAnswers", $condition);
            break;

            // For student - home: choose from current student's answers ...
            case 8:
            $_SESSION['studentID'] = $_SESSION['ID'];
            $condition=array("StudentID"=>$_SESSION['studentID']);
            $xTable=$db->select("ScripCalculationsAnswersHome",$condition);
            break;
        };
        // ... will have to remove any questions not finished

        // Put in information about the question ($theQuestionID) here: the text and the correct answer ...
        $condition = array ("ID"=>$theQuestionID);
        $table = $db->select("ScripCalculationsQuestions", $condition);

        // Read the question text ...
        foreach ($table as $row){
            $questionText = $row['QuestionText'];
        };




        // Now read the model answers ...
        $condition = array ("QuestionID"=>$theQuestionID);
        $table = $db->select("ScripCalculationsMA", $condition, "ID");

        // This was used when all model answers were shown ...

        foreach ($table as $row){
            $MA[] = $row['MA'];
        };
        // ... and combine the model answers into one string ...
        $allMA = implode(" -- ", $MA);


        // Each answer given by any student is stored in ScripCalculationsAnswers with the model answers that were current when the answer was 
        // given. These are for reference only.
        // The answer that is shown now is the "correct" answer (the first of the model answers) now. 

//print("%%%%%%%%%%%%%%");

        // Start the table ...
        print("<table width='100%'>");

        // Display question and model answer(s):
        // One row: QUESTION
        print("<tr><td colspan='4'><font color='#ffff00'><i>Question [$theQuestionID]:</i> $questionText</font></td></tr>");
        // One row: MODEL ANSWER(S)
        print("<tr><td colspan='4'><font color='#ffff00'><i>Answer(s):</i> $allMA</font></td></tr>");

        // Begin the form ...
        print("<form action='ScripModerate.php' method='post'>\n");
        print("<input type='hidden' name='tab' value='254'>");
        print("<input type='hidden' name='thequestionid' value='$theQuestionID'>");
        print("<input type='hidden' name='thecaseid' value='" . $_SESSION['caseIDinSelect'] . "'>");

        print("<tr>");
        print("<td width='30%' align='center'><font color='#ffff00'><b>Student Number</b></font></td>");
        print("<td width='30%' align='center'><font color='#ffff00'><b>Student's Answer</b><font></td>");
        print("<td width='10%' align='center'><font color='#ffff00'><b>Penalty</b><font></td>");
        print("<td width='30%' align='center'><font color='#ffff00'><b>Comment</b><font></td>");
        print("</tr>");

        // The table $xTable has all the results relating to the question that the user has requested. We only want those done by students who
        // have finished that question and the correct moderation condition (accorindg to categoryToMod) ... 

        // Note that the contents of $xTable all relate to either one question (defined by QuestionID) or to one student (defined by StudentID) ...

        $yTable = array(); // ... modified version of $xTable

        // Look at each row in xTable and either keep it (in yTable) or not ...
        switch ($_SESSION['categoryToMod']) {
            // Practice ...
            case 1:
            // Look through $xTable:
            foreach ($xTable as $xRow){
                $theStudent = $xRow['StudentID']; $theQuestion = $xRow['QuestionID'];

                // Now go to the ScripStudentRecord to see the status of this record (as characterised by StudentID and QuestionID) ..
                $condition = array ('StudentID'=>$theStudent, 'QuestionID'=>$theQuestion);
                $finishedAnswersTable = $db->select("ScripStudentRecordCalculations", $condition);
                foreach ($finishedAnswersTable as $finishedAnswersRow){
                    $theFinishedYN = $finishedAnswersRow['FinishedYN']; $theModeratedYN = $finishedAnswersRow['ModeratedYN'];

                    if ($theFinishedYN && !$theModeratedYN){
                        // Finished but not moderated - add to table ...

                        $yTable[] = $xRow;
                    };
                };
            };
            break;

            case 2: case 3:
            // Look through $xTable:
            foreach ($xTable as $xRow){
                $theStudent = $xRow['StudentID']; $theQuestion = $xRow['QuestionID'];

                // Now go to the ScripStudentRecord to see the status of this record (as characterised by StudentID and QuestionID) ..
                $condition = array ('StudentID'=>$theStudent, 'QuestionID'=>$theQuestion);
                $finishedAnswersTable = $db->select("ScripStudentRecordCalculations", $condition);

                foreach ($finishedAnswersTable as $finishedAnswersRow){
                    $theFinishedYN = $finishedAnswersRow['FinishedYN']; $theModeratedYN = $finishedAnswersRow['ModeratedYN'];
                    if ($theFinishedYN && $theModeratedYN){
                        // Finished AND moderated - add to table ...
                        $yTable[] = $xRow;
                    };
                };
            };
            break;

            case 4:
            // Look through $xTable:
            foreach ($xTable as $xRow){
                $theStudent = $xRow['StudentID']; $theQuestion = $xRow['QuestionID'];

                // Now go to the ScripStudentRecord to see the status of this record (as characterised by StudentID and QuestionID) ..
                $condition = array ('StudentID'=>$theStudent, 'QuestionID'=>$theQuestion);
                $finishedAnswersTable = $db->select("ScripStudentRecordCalculationsExam", $condition);

                foreach ($finishedAnswersTable as $finishedAnswersRow){
                    $theFinishedYN = $finishedAnswersRow['FinishedYN']; $theModeratedYN = $finishedAnswersRow['ModeratedYN'];
                    if ($theFinishedYN && !$theModeratedYN){
                        // Finished but not moderated - add to table ...
                        $yTable[] = $xRow;
                    };
                };
            };
            break;

            case 5: case 6:
            // Look through $xTable:
            foreach ($xTable as $xRow){
                $theStudent = $xRow['StudentID']; $theQuestion = $xRow['QuestionID'];

                // Now go to the ScripStudentRecord to see the status of this record (as characterised by StudentID and QuestionID) ..
                $condition = array ('StudentID'=>$theStudent, 'QuestionID'=>$theQuestion);
                $finishedAnswersTable = $db->select("ScripStudentRecordCalculationsExam", $condition);

                foreach ($finishedAnswersTable as $finishedAnswersRow){
                    $theFinishedYN = $finishedAnswersRow['FinishedYN']; $theModeratedYN = $finishedAnswersRow['ModeratedYN'];
                    if ($theFinishedYN && $theModeratedYN){
                        // Finished AND moderated - add to table ...
                        $yTable[] = $xRow;
                    };
                };
            };
            break;

            // Student: Practice ...
            case 7:
            break;

            // Student: Home ...
            case 8:
            break;
        };
        // ... now have $yTable which is a modified version of $xTable



        // Now write out the contents of $yTable to the screen ...
        foreach ($yTable as $yRow){
            $theStudentID = $yRow['StudentID'];

            // One row in the table [on the screen] for each row in the table [in the database] ...
            print("<tr>");

            // Two fixed entries (cells): STUDENT NUMBER and STUDENT'S ANSWER ...
            print("<td><font color='#ffff00'>" . $theStudentID . "<font></td>");
            print("<td><font color='#ffff00'>" . $yRow['StudentAnswer'] . "<font></td>");

            // Two entries that can be overwritten (cells): PENALTY and COMMENT ...
            print("<td><input type='text' name='penalty[$theStudentID]' value='".$yRow['Penalty']."'></td>");
            print("<td><input type='text' name='comment[$theStudentID]' value='".$yRow['Comment']."' size='100'></td>");
            // ... note that we link the comment to the student ID (more robust than the loop parameter?)

            print("</tr>");
        };

        // Write to the moderated version of the table (ScripCalculationsAnswersModerated or ScripCalculationsAnswersExamModerated) ...
        print("<tr><td><input type='submit' value='Submit'></td></tr>");

        print("</form>");
    } else {
        // HOME
        // For the student, show all the questions belonging to the current two-letter code $_SESSION['caseIDinSelect'] and student $_SESSION['ID']:

        // There are two categories for student: released practice calculations ($_SESSION['categoryToMod'] = 7) and home (unsupervised)
        // calculations ($_SESSION['categoryToMod'] = 8) ...
        $yTable = array (); // ... as above set this to blank and then store suitable rows of $xTable in it as they are found.
        switch ($_SESSION['categoryToMod']) {

            // For student: moderated practice ...
            case 7:
            $_SESSION['studentID'] = $_SESSION['ID'];
            $condition=array("CaseID"=>$_SESSION['caseIDinSelect'], "StudentID"=>$_SESSION['studentID']);
            $xTable=$db->select("ScripCalculationsAnswersModerated", $condition);

            // Now find what in $xTable has been finished, moderated and released ...
            foreach ($xTable as $xRow){
                $theStudent = $xRow['StudentID']; $theQuestion = $xRow['QuestionID'];

                // Now go to the ScripStudentRecord to see the status of this record (as characterised by StudentID and QuestionID) ..
                $condition = array ('StudentID'=>$theStudent, 'QuestionID'=>$theQuestion);
                $finishedAnswersTable = $db->select("ScripStudentRecordCalculations", $condition);

                foreach ($finishedAnswersTable as $finishedAnswersRow){
                    $theFinishedYN = $finishedAnswersRow['FinishedYN']; $theModeratedYN = $finishedAnswersRow['ModeratedYN']; $theReleasedYN = $finishedAnswersRow['ReleasedYN'];
                    if ($theFinishedYN && $theModeratedYN && $theReleasedYN){
                        // Finished AND moderated AND released - add to table ...
                        $yTable[] = $xRow;
                    };
                };
            };
            break;
            // ... may want to order this in question number order

            // For student: home ...
            case 8:
            $_SESSION['studentID'] = $_SESSION['ID'];
            $condition=array("CaseID"=>$_SESSION['caseIDinSelect'], "StudentID"=>$_SESSION['studentID']);    
            $xTable=$db->select("ScripCalculationsAnswersHome",$condition);

//print_r($condition);
//print_r($xTable);

            // Now find what in $xTable has been finished (there being no moderation or release for unsupervised) ...
            $_SESSION['studentID'] = $_SESSION['ID'];
            $condition=array("CaseID"=>$_SESSION['caseIDinSelect'], "StudentID"=>$_SESSION['studentID']);    
            $xTable=$db->select("ScripCalculationsAnswersHome", $condition);

            // Now find what in $xTable has been finished, moderated and released ...
            foreach ($xTable as $xRow){
                $theStudent = $xRow['StudentID']; $theQuestion = $xRow['QuestionID'];

                // Now go to the ScripStudentRecord to see the status of this record (as characterised by StudentID and QuestionID) ..
                $condition = array ('StudentID'=>$theStudent, 'QuestionID'=>$theQuestion);
                $finishedAnswersTable = $db->select("ScripStudentRecordCalculationsHome", $condition);

                foreach ($finishedAnswersTable as $finishedAnswersRow){
                    $theFinishedYN = $finishedAnswersRow['FinishedYN'];
                    if ($theFinishedYN){
                        // Finished AND moderated AND released - add to table ...
                        $yTable[] = $xRow;
                    };
                };
            };

            break;
        };

        print("<table width='100%'>");


        // Sort on QuestionNumber field so that the questions are displayed in the order, 1, 2, 3, ..., 10 ...
        $qN = array();
        foreach ($yTable as $yRow){

            // Look up the question text
            $condition = array ("ID" => $yRow['QuestionID']);
            $qTable = $db->select("ScripCalculationsQuestions", $condition);
            foreach ($qTable as $qRow){
                $qN[] = $qRow['QuestionNumber'];
            };
        };
        array_multisort ($qN, SORT_ASC, SORT_NUMERIC, $yTable);

        // Now read $yTable and display the contents ...
        $firstEntry = true;
        foreach ($yTable as $yRow){

            // Look up the question text
            $condition = array ("ID" => $yRow['QuestionID']);
            $qTable = $db->select("ScripCalculationsQuestions", $condition);
            foreach ($qTable as $qRow){
                $questionText = $qRow['QuestionText'];
                $questionNumber = $qRow['QuestionNumber'];
            };

            // Want to blank lines between different entries. Is this the first entry?            
            if ($firstEntry){
                // It is ... so later entries will definitely not be the first. Indicate this with the variable ...
                $firstEntry = false;
            } else {
                // Not the first ... add a blank line ...
                print("<tr><td colspan='2'>&nbsp;</td></tr>");
            };

            // Table for student: QUESTION ...
            print("<tr>");
            print("<td colspan='2'><font color='#ffff00'><b>($questionNumber)</b> $questionText</font></td>");
            print("</tr>");

            // Table for student: STUDENT'S ANSWER and PENALTY ...
            print("<tr>");
            print("<td><font color='#ffff00'>Your answer: " . $yRow['StudentAnswer'] . "</font></td>");
            print("<td><font color='#ffff00'>Penalty: " . $yRow['Penalty'] . "</font></td>");
            print("</tr>");

            // Table for student: COMMENT (but no comments for unsupervised use) ...
            if ($_SESSION['categoryToMod'] != 8){
                print("<tr>");
                print("<td colspan='2'><font color='#ffff00'><i>Comment:</i> " . $yRow['Comment'] . "</font></td>");
                print("</tr>");
            };

            // Extract the first model answer - should be the model answer with the lowest primary key (because it should have been written away that way) ...
            // (Could improve this offered radio buttons to administrator to select one model answer as the "Correct answer".)
            $theCorrectAnswer = explode ("--", $yRow['MA']); // ... explode the string of model answers on "--" (demarker)

            // Table for student: MODEL ANSWER(S) ...
            print("<tr>");
            print("<td colspan='2'><font color='#ffff00'><i>Correct answer:</i> " . $theCorrectAnswer[0] . "</font></td>");
            print("</tr>");
        };

    };

    print("</table>");


//    print("</form>\n");


//    print("<p><a href='ScripModerate.php?tab=10'>Back</a></p>");



    print("</table></div>");
    if ($showCheckingNow) { CH_ShowChecking(); };
    print("</td></tr>");
    print("<tr><td>$else</td></tr>");
    print("\n");
};






function D_WriteCalculationsComments($theCaseID, $theQuestionID, $newComments, $newPenalties){

    // This function takes comments and penalties relating to one question (but multiple students) and writes them to a table:

    $db=new dbabstraction(); $db->connect() or die ($db->getError());

    // Loop over all the students who have been marked. Specifically, loop through the array $newComments which has one
    // element for each user who has finished the question (itself identified by $theCaseID and $theQuestionID) ... 

    foreach ($newPenalties as $elementStudentID => $valuePenalty) {

        // Get the matching commment ...
        $valueComment = $newComments[$elementStudentID];

        // Read the original entry (from either ScripCalculationsAnswers or ScripCalculationsAnswersExam) ...
        $condition = array("StudentID"=>$elementStudentID, "CaseID"=>$theCaseID, "QuestionID"=>$theQuestionID);
        switch($_SESSION['mode']){
            // Switch between Practice and Examination ...
            case "P": $table=$db->select("ScripCalculationsAnswers", $condition); break;
            case "E": $table=$db->select("ScripCalculationsAnswersExam", $condition); break;
        };

        switch ($_SESSION['mark']){
            // Switch between Unmarked, Marked Original and Marked Moderated ...
            case "U": break;
            case "O": break;
            case "M": break;
        };

        // Get the original data ...
        foreach ($table as $row){ $data = $row; };

        // ... add on the comment field (i.e. concatenate) ...
        $data = array_merge($data, array("Comment"=>$valueComment));

        // ... and amend the penalty field ...
        $data['Penalty'] = $valuePenalty;

        // Amend equivalent of ScripStudentRecord (i.e. ScripStudentRecordCalculations or ScripStudentRecordCalculations)
        // but note that this is now done on a per question basis ...
        $text = array("ModeratedYN"=>1);
        $condition = array("StudentID"=>$elementStudentID, "CaseID"=>$theCaseID, "QuestionID"=>$theQuestionID);

        // Delete any existing entries in equivalent of ScripResultsModerated for this student and this case ...
//        $condition = array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);
        switch ($_SESSION['categoryToMod']) {
            case 1: case 2: case 3:
//print("123 Del ");
            $db->delete("ScripCalculationsAnswersModerated", $condition);
            break;
            case 4: case 5: case 6:
//print("456 Del ");
            $db->delete("ScripCalculationsAnswersExamModerated", $condition);
            break;
            // 7 and 8 refer to student options - updates not available
        };

        // ... and write to the correct table using either insert or update as appropriate ...
        switch ($_SESSION['categoryToMod']) {
            case 1: case 2: case 3:
//print("123 Ins ");
            $db->insert("ScripCalculationsAnswersModerated", $data);
            break;

            case 4: case 5: case 6:
//print("456 Ins ");
            $db->insert("ScripCalculationsAnswersExamModerated", $data);
            break;
        };

        // Set ModeratedYN to 1  ...
        $condition = array('CaseID'=>$_SESSION['caseIDinSelect'], 'StudentID'=>$elementStudentID, 'QuestionID'=>$theQuestionID);
        $text = array('ModeratedYN'=>1);

//print("<pre>");
//print_r($condition); 
//print_r($text); 
//print("</pre>");

        // JANUARY 2012 - Now also need to amend ScripStudentRecordCalculations too
        $db->update("ScripStudentRecordCalculations", $text, $condition);

        // For each student, attempt to read from 1 to 10 in the moderated table - when can do so for all questions the whole exercise is available to that student 

        switch ($_SESSION['categoryToMod']) {
            case 1: case 2: case 3:
//print("123 SSR ");
            $db->update("ScripStudentRecordCalculations", $text, $condition);
            break;

            case 4: case 5: case 6:
//print("456 SSR ");
            $db->update("ScripStudentRecordCalculationsExam", $text, $condition);
            break;
            // 7 and 8 refer to student options - updates not available
        };

    };

    // Now show the "Back" hyperlink so the user can go back ...
    print("<p><a href='ScripModerate.php?tab=15'>Back</a></p>");

};  


   









function D_DisplayHeaderTable() {
    global $case;
    global $_POST;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    ZB_MaxScore();

    // Read the time and date ...
    $time = date('G:i:s');
    $date = date('Y-n-j');
    $browser = $_SERVER['HTTP_USER_AGENT'];
    $ipAddress = $_SERVER['REMOTE_ADDR'];
    $os = ""; // ... set black until we discover where it is

    // Write usage to ScripUsage ...
    $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                  'StudentRecord'=>$_SESSION['studentrecordID'], 'CaseID'=>$_SESSION['currentscrip'],
                  'Browser'=>$browser, 'IPAddress'=>$ipAddress, 'OS'=>$os);
//    $db->insert("ScripUsageSM", $text);

    print("<tr><td valign='top' align='center'>\n");
    print("<form action='ScripModerate.php' method='post'>\n");
    print("<input type='hidden' name=tab value='50'>");
//    print("<FORM ACTION=ScripModerate.php?tab=50 METHOD=POST>\n");
    print("<div class=tablegrey>");
    print("\n\n");
    print("<table bgcolor='#eeeeee' border='1' width='100%' align='center'>");
    print("<tr>\n");

    // Set default state for following set of radio buttons ...
    if (!isset($_SESSION['categoryToMod'])) { // Not set?
        if ($_SESSION['MorS'] == "S"){
            $_SESSION['categoryToMod'] = 7; // 7 for students
        } else {
            $_SESSION['categoryToMod'] = 1; // 1 for others
        };
    };

/*
    // Read THE VALUE of the serious penalty from the ScripStudentRecord or ScriptStudentRecordExam table for this case ...
    $condition = array('RecordID'=>$_SESSION['studentrecordID']);
    switch($_SESSION['mode']){
        case "P":
        $table=$db->select("ScripStudentRecord", $condition);
        break;

        case "E":
        $table=$db->select("ScripStudentRecordExam", $condition);
        break;
    };

    if ($_SESSION['mode'] == "P" || $_SESSION['mode'] == "E"){
        foreach ($table as $row){
            $_SESSION['theSeriousPenalty'] = $row['SeriousPenalty'];
        };
    };
*/
    // Values: 1 - Practice - Unmarked
    //         2 - Practice - Mark originals
    //         3 - Practice - Mark moderated
    //         4 - Examination - Unmarked
    //         5 - Examination - Mark originals
    //         6 - Examination - Marked moderated
    //
    //         7 - Practice - Marked and released
    //         8 - Unsupervised

    $defaultMode = "P";
    $defaultMark = "U";

    if ($_POST['theSRecord']) {
        $_SESSION['theSRecord'] = $_POST['theSRecord'];
    };

    if ($_POST['studentID']) {
        if ($_POST['studentID'] != $_SESSION['studentID']) {
            $_SESSION['theSRecord'] = "";
        };
        $_SESSION['studentID'] = $_POST['studentID'];
    };

    // Logic: If we change mark, then we need to re-set studentID
    // and theSRecord ...
    if ($_POST['mark']) {
        if ($_POST['mark'] != $_SESSION['mark']) {
            $_SESSION['studentID'] = "";
            $_SESSION['theSRecord'] = "";
        };
        $_SESSION['mark'] = $_POST['mark'];
    };

    // Logic: If we change mode, then we need to re-set all the
    // other options ...
    if ($_POST['mode']) { // Set?
        if ($_POST['mode'] != $_SESSION['mode']) { // Changed?
            $_SESSION['mark'] = $defaultMark;
            $_SESSION['studentID'] = "";
            $_SESSION['theSRecord'] = "";
        };
        $_SESSION['mode'] = $_POST['mode'];
    };


    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
        if (!$_SESSION['mode']) { $_SESSION['mode'] = $defaultMode; };
        if (!$_SESSION['mark']) { $_SESSION['mark'] = $defaultMark; };
    } else {
        if (!$_SESSION['mode']) { $_SESSION['mode'] = "P"; };
        $_SESSION['mark'] = "";
    };

    if ($_SESSION['mode'] == "P" && $_SESSION['mark'] == "U") { $_SESSION['categoryToMod'] = 1; };
    if ($_SESSION['mode'] == "P" && $_SESSION['mark'] == "O") { $_SESSION['categoryToMod'] = 2; };
    if ($_SESSION['mode'] == "P" && $_SESSION['mark'] == "M") { $_SESSION['categoryToMod'] = 3;};
    if ($_SESSION['mode'] == "E" && $_SESSION['mark'] == "U") { $_SESSION['categoryToMod'] = 4;};
    if ($_SESSION['mode'] == "E" && $_SESSION['mark'] == "O") { $_SESSION['categoryToMod'] = 5;};
    if ($_SESSION['mode'] == "E" && $_SESSION['mark'] == "M") { $_SESSION['categoryToMod'] = 6; };

    if ($_SESSION['mode'] == "P" && !$_SESSION['mark']) { $_SESSION['categoryToMod'] = 7; };
    if ($_SESSION['mode'] == "U" && !$_SESSION['mark']) { $_SESSION['categoryToMod'] = 8; };

    $condition=array("FormID"=>$_SESSION['formID']);
    $table=$db->select("ScripForms",$condition);
    foreach ($table as $row) {
        $_SESSION['group']=$row['GroupID'];
    };

    $widthOfCell="20%";

    // NEW CELL 1 - Choose Practice, Examination or Unsupervised ...
//    print("<td valign='top' width='25%'>");
    print("<td valign='top' width=$widthOfCell>");
    print("<input type='radio' name='mode' value='P' ");
    if ($_SESSION['mode'] == "P") { print("checked "); };
    print("onClick='javascript:document.forms[0].submit()'>");
    print("<font color=ffff00>Practice</font><br>");

    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
        print("<input type='radio' name='mode' value='E' ");
        if ($_SESSION['mode'] == "E") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Examination</font><br>");
    } else {
        print("<input type='radio' name='mode' value='U' ");
        if ($_SESSION['mode'] == "U") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Unsupervised</font><br>");
    };

    print("<input type='radio' name='mode' value='C' ");
    if ($_SESSION['mode'] == "C") { print("checked "); };
    print("onClick='javascript:document.forms[0].submit()'>");
    print("<font color=ffff00>Checking</font><br>");

    print("</td>");

    // NEW CELL 2 - Choose Unmarked, Mark originals or Mark moderated ...
//    print("<td valign='top' width='25%'>");
    print("<td valign='top' width=$widthOfCell>");
    
    // If either administrator or moderator and not in checking mode ...
    if (($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") && $_SESSION['mode'] != "C") {
        print("<input type='radio' name='mark' value='U' ");
        if ($_SESSION['mark'] == "U") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Unmarked</font><br>");

        print("<input type='radio' name='mark' value='O' ");
        if ($_SESSION['mark'] == "O") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Marked - Original results</font><br>");

        print("<input type='radio' name='mark' value='M' ");
        if ($_SESSION['mark'] == "M") { print("checked "); };
        print("onClick='javascript:document.forms[0].submit()'>");
        print("<font color=ffff00>Marked - Moderated</font><br>");
    };

    // Message to student about moderation ...
    if (!($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M")) {
        print("<font color=ffff00>");
        if ($_SESSION['mode'] == "P") {
            print("These marks HAVE been moderated by an academic.");
        };
        if ($_SESSION['mode'] == "U") {
            print("These marks have NOT been moderated by an academic.");
        };
        print("</font>");
    };

    print("</td>");

    // NEW CELL 2A
//    print("<td width='25%' align='center'>");
    print("<td width=$widthOfCell align='center'>");

    if (($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") && $_SESSION['mode'] != "C") {

        // Need a set of all the different locations in ScripStudent ...
        $allLocations = array();
        $table=$db->select("ScripStudent");
        foreach ($table as $row){
            // Read an entry from the table ...
            $thisLocation = $row['Location'];
            $alreadyNoted = false;

            // Loop over contents of $allLocations ...
            for ($j = 0; $j < count($allLocations); $j++){
                if ($allLocations[$j] == $thisLocation){ $alreadyNoted = true; };
            };

            if (!$alreadyNoted) {
                // If not previously in $allLocations, add it now ...
                array_push($allLocations, $thisLocation);
            };
        };
       // ... all the locations are now in $allLocations

        // Drop-down menu for YEAR GROUP ...
        print("<select name='selectYearGroup' onChange='javascript:document.forms[0].submit()'>");
        print("<option value='0'>Select year group ...</option>");
        for ($i = 0; $i < 4; $i++){
            $ip1 = $i + 1;
            print("<option value=$ip1");
            if ($ip1 == $_SESSION['selectYearGroup']){ print(" selected"); };
            print(">Year $ip1</option>");
        };
        print("</select>");

        print("<br><br>");

        // Drop-down menu for LOCATION ...
        print("<select name='selectLocation' onChange='javascript:document.forms[0].submit()'>");
        print("<option value=''>Select location ...</option>");
        for ($i = 0; $i < count($allLocations); $i++){
            $ip1 = $i + 1;
            print("<option value=$allLocations[$i]");
            if ($allLocations[$i] == $_SESSION['selectLocation']) { print(" selected"); };
            print(">$allLocations[$i]</option>");
        };
        print("</select>");

    } else {

        print("&nbsp;");

    };

    print("</td>\n");


    // NEW CELL 3 - Choose a student from a drop-down list ...
//    print("<td valign='middle' width='25%'>");
    print("<td valign='middle' width=$widthOfCell>");

    if ($_SESSION['mode'] == "C") {
        // Find checking names in ScripCheckingResults ...
        if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
            $rTable=$db->select("ScripCheckingResults");
        } else {
            $condition = array("StudentID"=>$_SESSION['studentID'], "Release"=>1);
            $rTable=$db->select("ScripCheckingResults", $condition);
        };
    } else {
//print("XX ". $_SESSION['categoryToMod']);
        // If a moderator and categoryToMod is < 7 then we choose the correct table and read from it according to the 
        // parameters set by the radio buttons above ...
        // With year group and location added, we now include that in the condition used in the $db->select. This is fixed for the
        // first six cases (and not used for the last two); merge it with the special condition within each case ...
        $conditionYL = array("YearGroup"=>$_SESSION['selectYearGroup'], "Location"=>$_SESSION['selectLocation']);
        switch ($_SESSION['categoryToMod']) { 
            case 1:
            // Results that have NOT been moderated ...
            $condition=array("FinishedYN"=>1, "ModeratedYN"=>0);
            $condition = array_merge($condition, $conditionYL);
            $rTable=$db->select("ScripStudentRecord",$condition, "CaseID");  
            break;

            // Results that HAVE been moderated, but get originals ...
            case 2: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
            $condition = array_merge($condition, $conditionYL);
            $rTable=$db->select("ScripStudentRecord",$condition, "CaseID");
            break;

            // Results that HAVE been moderated and get moderated versions ...
            case 3: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
            $condition = array_merge($condition, $conditionYL);
            $rTable=$db->select("ScripStudentRecord",$condition, "CaseID");
            break;

            case 4: $condition=array("FinishedYN"=>1, "ModeratedYN"=>0);
            $condition = array_merge($condition, $conditionYL);
            $rTable=$db->select("ScripStudentRecordExam",$condition, "CaseID");
            break;

            case 5: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
            $condition = array_merge($condition, $conditionYL);
            $rTable=$db->select("ScripStudentRecordExam",$condition, "CaseID");
            break;

            case 6: $condition=array("FinishedYN"=>1, "ModeratedYN"=>1);
            $condition = array_merge($condition, $conditionYL);
            $rTable=$db->select("ScripStudentRecordExam",$condition, "CaseID");
            break;

            case 7:
            // No need to specify yeargroup and location for student options (student number is tight enough a specification) ...
            $_SESSION['studentID'] = $_SESSION['ID'];
            $condition=array("ModeratedYN"=>1, "FinishedYN"=>1, "ReleasedYN"=>1, "StudentID"=>$_SESSION['studentID']);    
            $rTable=$db->select("ScripStudentRecord",$condition);
//HERE
//print("<pre>");
//print_r($condition);print_r($rTable);
//print_r($_SESSION);


            break;

            case 8:
            $_SESSION['studentID'] = $_SESSION['ID'];
            $condition=array("FinishedYN"=>1, "StudentID"=>$_SESSION['studentID']);
            $rTable=$db->select("ScripStudentRecordHome",$condition);
            break;
        };
    };

    if (($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") && ($_SESSION['categoryToMod'] || $_SESSION['mode'] == "C")) {
        // List each student (e.g. 99, 113) ONCE only ...
        for ($i = 0; $i < count($rTable); $i++){
            $notFound = true;
            for ($j = 0; $j < count($tableStudentsOnly); $j++){
                // If already in there, we don't want to add it and we don't even want to go any further down $tableStudentsOnly (hence break) ...
                if ($rTable[$i]['StudentID'] == $tableStudentsOnly[$j]['ID']){
                    $notFound = false; break;
                };
            };
            // Not in table so far - add it ...
            if ($notFound) {
                $tableStudentsOnly[]['ID'] = $rTable[$i]['StudentID'];
            };
        };

        // Now we want the names matching these ID numbers ...
        $table = $db->select("ScripStudent");
        foreach ($table as $row) {
            for ($i = 0; $i < count($tableStudentsOnly); $i++) {
                if ($tableStudentsOnly[$i]['ID'] == $row['StudentID']) {
                    $tableStudentsOnly[$i]['FirstName'] = $row['FirstName'];
                    $tableStudentsOnly[$i]['LastName'] = $row['LastName'];
                    break; 
                };
            };
        };

        // Found any students?
        if (count($tableStudentsOnly)) {
            // Q: How do we want to sort it? A: In order of surname.
            for ($i = 0; $i < count($tableStudentsOnly); $i++) {
                $guide[] = $tableStudentsOnly[$i]['LastName'];
            };
            array_multisort($guide, SORT_ASC, SORT_STRING, $tableStudentsOnly);

            print("<select name='studentID' ");
            print("onchange='javascript:document.forms[0].submit()'");
            print(">");
            print("<option value=''>Select from ...");
            foreach ($tableStudentsOnly as $row) {
                $name = $row['LastName'].", ".$row['FirstName'];
                $ID = $row['ID'];
                print("<option value=$ID");
                if ($_SESSION['studentID'] == $ID) { print(" selected"); };
                print(">$name");
            };
            print("</select>");
        } else {
            print("<p align='center'>");
            print("<font color='#ffffff'>No students found</font>");
            print("</p>");
        };
    } else {
        // Student can only look at own results ...
        print("<input type='hidden' name='studentID' value='".$_SESSION['ID']."'>");
        $_SESSION['studentID'] = $_SESSION['ID'];
    };

    print("</td>");

    // NEW CELL 4 - CHOOSE CASE ...
    // Offer the records that include only the specified studentID
    // If have studentID can search for it in the record table ...
    // March 2014: Need to offer only the RECENT record (in case of ! users)
//    print("<td valign='middle' width='25%'>\n");
    print("<td valign='middle' width=$widthOfCell>\n");


    if ($_SESSION['studentID']) {
        if ($_SESSION['mode'] == "C") {
            // Show all checking ...
            $showCheckingNow = 1;
        } else { // Get a case ...
            $showCheckingNow = 0;
            foreach ($rTable as $row) {
                if ($_SESSION['studentID'] == $row['StudentID']) {
                    $cases[] = $row;
                };
            };

            if (count($cases) && $_SESSION['mode'] != "C") {
                print("<select name='theSRecord' ");
                print("onchange='javascript:document.forms[0].submit()'");
                print(">\n");
// THIS IS WHERE THE STUDENT'S CASES APPEAR
                print("<option value=''>Select from ...\n");
                foreach ($cases as $row) {
                    $aRecordID = $row['RecordID'];
                    $aCaseID = $row['CaseID'];
                    print("<option value=$aRecordID");
                    if ($aRecordID == $_SESSION['theSRecord']) {
                        print(" selected");
                    };
                    print(">$aCaseID\n");
                };
                print("</select>\n");
            } else {
                print("<font color='#ffffff'>No cases to choose from yet.</font>");
            };
        };
    } else {
        print("<font color='#ffffff'>No student selected yet.</font>");
    };
    print("</td>\n");


    // NEW CELL 5 ...
//    print("<td width='25%' align='center'>");
//    print("<input type='submit' name='ChooseButton' value='Choose'>");
//    print("</td>\n");
//    print("</tr>\n");

    // SECOND ROW ...
    // Row listing four options: Main, two registers and input ...
    print("<tr>\n");
    print("<td width=150 pixels  align='center'>");
    if ($_SESSION['theSRecord'] && $_SESSION['mode'] != "C") {
        print("<a href='ScripModerate.php?tab=10&fromtab=1'>Main</a>");
    } else {
        print("&nbsp;");
    };
    print("</td>\n");

    print("<TD width=150 pixels  ALIGN=CENTER>");
    if ($_SESSION['theSRecord'] && $_SESSION['mode'] != "C") {
        print("<A HREF='ScripModerate.php?tab=20&fromtab=1'>CD Register</A>");
    } else {
        print("&nbsp;");
    };
    print("</TD>\n");

    print("<TD width=150 pixels align='center'>");
    if ($_SESSION['theSRecord'] && $_SESSION['mode'] != "C") {
        print("<A HREF='ScripModerate.php?tab=30&fromtab=1'>P.O.R.</A>");
    } else {
        print("&nbsp;");
    };
    print("</TD>\n");

    print("<td width=150 pixels align='center'>");
    if ($_SESSION['theSRecord'] && $_SESSION['mode'] != "C") {
        print("<a href='ScripModerate.php?tab=40&fromtab=1'>Demonstrator Input</a>");
    } else {
        print("&nbsp;");
    };
    print("</td>\n");

    print("</form>\n");


    print("</table></div>");
    if ($showCheckingNow) { CH_ShowChecking(); };
    print("</td></tr>");
    print("<tr><td>$else</td></tr>");
    print("\n");
};

function D_DisplayHeaderTableGC() {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    global $_POST;

    $condition=array("FormID"=>$_SESSION['formID']);
    $table=$db->select("ScripForms",$condition);
    foreach ($table as $row) {
        $_SESSION['group']=$row['GroupID'];
    };

    // This is the GC [generic code] version of the function that asks for details to be displayed.

    // In the first cell, there are three types of exercise: Practice (P); Examination (D, E and F); and Unsupervised (U).

    // The second cell requests certain information according to whether practice, examination or unsupervised has been chosen from the first cell:
    // PRACTICE - Last week only; All weeks (reverse chronological) - labelled with exercise letter: Last week;
    // Week before; Week before that; ...
    // EXAMINATION - By assessment either D (i.e. concatenation of D1 and D2) or E or F.
    // UNSUPERVISED - By last 5 (concatenated), 10 (concatenated) or 20 (concatenated) exercises completed; By last day
    // (concatenated), three (concatenated), seven (concatenated) or fourteen days (concatenated); By individual exercise. Sample size and number of samples

    // The third cell contains choice of student (for moderators only) 

    global $case;
    global $_POST;

    print("<tr>");
    print("<td valign='top' align='center'>\n");
    print("<form action='ScripModerate.php' name='ChooseType' method='post'>\n");
    print("<input type='hidden' name=tab value='51'>");
    print("<div class=tablegrey>");
    print("\n\n");
    print("<table bgcolor='#EEEEEE' border='1' align='center'>");
    print("<tr>\n");

    if ($_POST['theSRecord']) {
        $_SESSION['theSRecord'] = $_POST['theSRecord'];
    };

    if ($_POST['studentID']) {
        if ($_POST['studentID'] != $_SESSION['studentID']) {
            $_SESSION['theSRecord'] = "";
        };
        $_SESSION['studentID'] = $_POST['studentID'];
    };

    // The data returned here are:
    //     $_POST['mode'] one of either P, E or U
    //         For P:
    //         $_POST['week'] - some numbers - $_SESSION['weeksGC'] (array)

    //         For E:
    //         $_POST['DEF'] - one of either D, E or F - $_SESSION['DEFGC']

    //         For U:
    //         $_POST['size'] a number - size of the sample - $_SESSION['sizeGC']
    //         $_POST['number'] a number - number of samples - $_SESSION['numberGC']



//print("HTTP_POST_VARS['DEF'] ".$_POST['DEF']."<br>");


    // Need to FIND the correct settings, SET them and, if everything needed is set, then to FIND AND SHOW the data ...

    // If the user is a student, two things have to be set: mode and sub-mode; if a moderator, three: these two plus the student.
    // When the correct number has been set the form will automatically jump to the code that sets the information
    // Status: $_SESSION['MorS'] can be "M" (moderator), "A" (adminstrator) or "S" (student)

    // All the above needs to be done together ...

    // Has the mark been changed?
    //     If so, implement this change and choose a mode (or none)
    //     If not, has the mode been changed?
    //         If so, implement
    //         If not, check the student (if user is a moderator)

    // Is there a mode set?

    if (!$_SESSION['studentID']) { $submodeIsSet = 0; };

    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
    } else {
        // Students can only ever see themselves ...
        $_SESSION['studentID'] = $_SESSION['ID'];
    };

    if ($_POST['mode']) {

        // A mode is set ...
        if ($_POST['mode'] != $_SESSION['mode']) {

            // The mode has been changed: implement the change and note that sub-mode is not set ...
            $_SESSION['mode'] = $_POST['mode'];
//            $submodeIsSet = 0;
            if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") { $_SESSION['studentID'] = 0; };
            // ... re-set student ID to zero (if moderator - for a student, student ID is permanently set to student's own ID) 

        } else {

            // The mode hasn't changed; check the sub-mode (which check is mode-dependent) ...
            if ($_POST['mode'] == "P"){
                // Practice mode - has elements $_POST['week1'], $_POST['week2'], ... 
                $weeksMaximum = 10;
                for ($i = 1; $i < $weeksMaximum; $i++){
                    if ($_POST['week'.$i] != $_SESSION['weeksGC'][$i]) {
                        $_SESSION['weeksGC'][$i] = $_POST['week'.$i];
                        if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") { $_SESSION['studentID'] = 0; };
                        // ... re-set student ID to zero (if moderator - for a student, student ID is permanently set to student's own ID) 
                    };

                    // The sub-mode for this mode is set when at least one week has been chosen ...
                    if ($_SESSION['weeksGC'][$i]) { $submodeIsSet = 1; }; // ... note that sub-mode has been set if any week has been chosen
                };

            } else if ($_POST['mode'] == "E"){

//print("_SESSION['DEFGC'] ".$_SESSION['DEFGC']."<br>");
//print("HTTP_POST_VARS['DEF'] ".$_POST['DEF']."<br>");

                // Examination ...
                if ($_POST['DEF'] != $_SESSION['DEFGC']) {
                    $_SESSION['DEFGC'] = $_POST['DEF'];
//print("_SESSION['DEFGC'] ".$_SESSION['DEFGC']."<br>");
                    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") { $_SESSION['studentID'] = 0; };
                    // ... re-set student ID to zero (if moderator - for a student, student ID is permanently set to student's own ID) 
                };

                // The sub-mode for this mode is set when one of D, E or F has been chosen ...
                if ($_SESSION['DEFGC']) { $submodeIsSet = 1; };

//print("submodeIsSet ".$submodeIsSet."<br>");




            } else if ($_POST['mode'] == "U") {
                // Unsupervised mode ...
                if ($_POST['size'] != $_SESSION['sizeGC'] || $_POST['number'] != $_SESSION['numberGC']) {
                    $_SESSION['sizeGC'] = $_POST['size'];
                    $_SESSION['numberGC'] = $_POST['number'];
                    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") { $_SESSION['studentID'] = 0; };
                    // ... re-set student ID to zero (if moderator - for a student, student ID is permanently set to student's own ID) 
                };

                // The sub-mode for this mode is set when these two variables are non-zero ...
                if ($_SESSION['sizeGC'] && $_SESSION['numberGC']){ $submodeIsSet = 1; };

            };
        };
    };

    // CELL 1 - MODE: Choose Practice, Examination or Unsupervised ...

    print("<td valign='top'>");
    print("<input type='radio' name='mode' value='P'");
    if ($_SESSION['mode'] == "P") { print("checked "); };
    print("onClick='javascript:document.forms[0].submit()'>");
    print("<font color='ffff00'>Practice");
    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") { print(" (P)"); };
    print("</font><br>");

    print("<input type='radio' name='mode' value='E' ");
    if ($_SESSION['mode'] == "E") { print("checked "); };
    print("onClick='javascript:document.forms[0].submit()'>");
    print("<font color='ffff00'>Examination");
    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {     print(" (D, E, F)"); };
    print("</font><br>");

    print("<input type='radio' name='mode' value='U'");
    if ($_SESSION['mode'] == "U") { print("checked "); };
    print("onClick='javascript:document.forms[0].submit()'>");
    print("<font color='ffff00'>Unsupervised");
    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") { print(" (U)"); };
    print("</font><br>");
    print("</td>");

    // CELL 2 - SUB-MODE: Choose sub-options according to choice of option ...

    print("<td valign='top'>");

    switch ($_SESSION['mode']){
        case "P":

        $inPractice = 0;
        $maxNumberWeeks = 0;
       // We need to find the maximum number of weeks. Do this by going to ScripWeeksRelesed and looking for the biggest value of WeekNumber.
       // We also need to know which weeks have been released and show check boxes for these) ...
       // $condition=array("FormID"=>$_SESSION['formID']);
        $table=$db->select("ScripWeeksReleased");
        foreach ($table as $row) {
            $weekNumber = $row['WeekNumber'];
            if ($weekNumber > $maxNumberWeeks) { $maxNumberWeeks = $weekNumber; }; 
            $wR[$weekNumber] = $row['RR'];
             // Note need to look at RR which is results released as opposed to WR which is week available for student to practise unsupervised
            if ($wR[$weekNumber]){ $inPractice = 1; }; // ... if any week has been relased, set $inPractice to TRUE
        };

        // Knowing the student's ID, find the matching practice code ...
        $condition = array('StudentID'=>$_SESSION['studentID']);
        $table=$db->select("ScripStudent", $condition);
        foreach ($table as $row){
            $practiceCode = $row['PracticeCode'];
        };

        // Knowing the practice code, find the letter codes that correspond to the weeks selected in $_SESSION['weeksGC'] 
        $condition = array('PracticeCode'=>$practiceCode);
        $table=$db->select("ScripTermPattern", $condition);
        $counter = 0;
        for ($i = 1; $i < $maxNumberWeeks + 1; $i++){
            foreach ($table as $row){
                $letterCode[$counter] = $row['wk'.$i];
            };
            $counter++;
        };
        // ... the user has thus asked for the generic comments on the letter-codes stored in $letterCode (though we haven't yet
        // determined whether this is allowed or not i.e. whether the relevant week has been released)


//print_r($letterCode);

        // Offer only weeks that have been released ...
        // We also want to know what letters belong to which week ...
        $atLeastOneWeekShowing = 0;

        // Count backwards from week with highest number to 1 ...
        for ($weekCounter = $maxNumberWeeks; $weekCounter > 0; $weekCounter--){

            // Only show a week if it has been released ... except that ? users can see everything out of term
           
            // A simple variable to indicate that current week (in terms of for loop) is disabled ...
            if ($wR[$weekCounter] == 1) { $disable = 0; } else { $disable = 1; };

            if ($_SESSION['fullAuthority'] && !$inpractice) { $disable = 0; };

            // Week 1 not shown (at present - November 2009), even if released ... 
            if (!$disable && $weekCounter != 1){
                $atLeastOneWeekShowing = 1;
                print("<input type='checkbox' name='week$weekCounter' value='1'");
                // If this has already been chosen, tick the box ...
                if ($_SESSION['weeksGC'][$weekCounter]) { print("checked='checked' "); };

                // If the week hasn't been released, disable the box ...
                // Normally follow the week unless nothing has been released and user is a ?user (e.g. out of term) ...
                if ($disable) { print("disabled='disabled' "); };

                print("onClick='javascript:document.forms[0].submit()'>\n");
                print("<font color=ffff00>Week $weekCounter (Practice week ".($weekCounter-1).")"); 
        
                // If the week has been released and we know who the student is, show the letter ...
                // (Meaningless unless we DO know the student)
                if ($wR[$weekCounter] && $_SESSION['studentID']) { print(" � ".$letterCode[$weekCounter-1]); };

                print("</font><br>\n");
            };

        };

        // Choose button or message ...
        if ($atLeastOneWeekShowing){
            print("<input type='submit' name='ChooseButton' value='Choose'>");
        } else {
            print("<font color='#ffffff'>No weeks released yet</font>");
        };
        break;

        case "E":
// Print statements were used in April 2014
//print("In case E");

        // Find out which exams have been released (D1 and D2 count as just one) ...
//        $condition=array("ExamType"=>"D");
        $condition=array("WeekType"=>"D");
        // This is where the wrong table had been included ...
//        $table=$db->select("ScripExamResultsReleased", $condition);
        $table=$db->select("ScripWeeksGCExamReleased", $condition);
        foreach ($table as $row) {
            $dReleased = $row['RR'];
        };

//        $condition=array("ExamType"=>"E");
// May 2014
        $condition=array("WeekType"=>"E", "Location"=>$_SESSION['location'], "YearGroup"=>$_SESSION['yearGroup']);
//print_r($condition);
//        $condition=array("WeekType"=>"E");
//        $table=$db->select("ScripExamResultsReleased", $condition);
        $table=$db->select("ScripWeeksGCExamReleased", $condition);
        foreach ($table as $row) {
            $eReleased = $row['RR'];
        };
//print_r($condition);
//print_r($table);
//print("<br />eReleased is $eReleased");


// Next line shows wrong field name ...
//        $condition=array("ExamType"=>"F");
// Next line shows wrong variable ...
//        $condition=array("WeekType"=>"F");
// Next line shows correct variable but without location or yeargroup ...
//        $condition=array("WeekType"=>"R");

        // Current version: correct field name and value is set to R ...
        $condition=array("WeekType"=>"R", "Location"=>$_SESSION['location'], "YearGroup"=>$_SESSION['yearGroup']);

//        $table=$db->select("ScripExamResultsReleased", $condition);
        $table=$db->select("ScripWeeksGCExamReleased", $condition);
        foreach ($table as $row) {
            $rReleased = $row['RR'];
        };

        // We need to know how many exams have been sat ...

        // Show the radio buttons ...
        // In this section, we only show the radio buttons if the exam in question has been released ...
        if ($dReleased){
            print("<input type='radio' name='DEF' value='D'"); // ... pre-examination
            if ($_SESSION['DEFGC'] == 'D') { print(" checked='checked'"); };
            print(" onClick='javascript:document.forms[0].submit()'>");
            print("<font color=ffff00>First Sitting");
// July 2014 - Next line remmed out in favour of the one that follows - typographical error: Autority -> Authority
//            if ($_SESSION['fullAutority']) { print(" (D)"); };
            if ($_SESSION['fullAuthority']) { print(" (D)"); };
// all the above line does is to show a code letter in brackets to someone with authority
            print("</font><br>");
        };

        if ($eReleased){
            print("<input type='radio' name='DEF' value='E'"); // ... examination
            if ($_SESSION['DEFGC'] == 'E') { print(" checked='checked'"); };
            print(" onClick='javascript:document.forms[0].submit()'>");
            print("<font color=ffff00>Examination");
// July 2014 - Next line remmed out in favour of the one that follows - typographical error: Autority -> Authority
//            if ($_SESSION['fullAutority']) { print(" (E)"); };
            if ($_SESSION['fullAuthority']) { print(" (E)"); };
// all the above line does is to show a code letter in brackets to someone with authority
            print("</font><br>");
        };

        if ($rReleased){
            print("<input type='radio' name='DEF' value='F'"); // ... re-sit
            if ($_SESSION['DEFGC'] == 'F') { print(" checked='checked'"); };
            print(" onClick='javascript:document.forms[0].submit()'>");
            print("<font color=ffff00>Re-sit");
// July 2014 - Next line remmed out in favour of the one that follows - typographical error: Autority -> Authority
//            if ($_SESSION['fullAutority']) { print(" (F)"); };
            if ($_SESSION['fullAuthority']) { print(" (R)"); };
            // (... all the above line does is to show a code letter in brackets to someone with authority)
            print("</font><br>");
        };


        if (!$dReleased && !$eReleased && !$rReleased){
            // Nothing has been released - tell the user this ...
            print("<font color='#ffff00'>No generic results for examinations have been released yet</font>");
        };
        break;

        case "U":
        print("<font color=ffff00></font><br>");
        print("<table border='0'><tr>");
        print("<td><font color=ffff00>Group the exercises in groups of:</font></td>");
        print("<td><input type='text' size='5' maxchar='5' name='size' value='".$_SESSION['sizeGC']."'></td>");
        print("</tr><tr>");
        print("<td><font color=ffff00>Number of groups to display:</font></td>");
        print("<td><input type='text' size='5' maxchar='5' name='number' value='".$_SESSION['numberGC']."'></td>");
        print("</tr></table>");
        break;

    };
    print("</td>");

    // CELL 3 - STUDENT: Only allowed to moderators ... 
    print("<td valign='middle'>");

    // If user is a student, nothing goes here - the student can see his own results only.

    // If user is a moderator, then any user can be chosen. Depending on what has been chosen in the earlier boxes, we offer
    // different lists of students. Each list generated from ScripResultsModerated ($_SESSION['mode'] = P),
    // ScripResultsExamModerated ($_SESSION['mode'] = E) or ScripResultsHomeGC ($_SESSION['mode'] = U). Each of these will require a little
    // extra processing e.g. if we want the latest unsupervised 50 exercises, then some students in ScripResultsHomeGC
    // may not have yet done 50 so they, though in ScripResultsHomeGC, will have to be removed from the list ... perhaps.

    // We only show a list of students if
    // do this if the relevant sub-mode has been set or {if the user is a moderator and a student has been set} ...


    // This is a shortcut way of setting $submodeIsSet if a student is still chosen ...

    if ($_SESSION['studentID']){ $submodeIsSet = 1; };



    if ($submodeIsSet){
        // Logically correct to enter here ... is current user ALLOWED to enter here?


        if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
            // For administrators and moderators only (i.e. not students), list each student (e.g. 99, 113) ONCE only (in $tableStudentsOnly) ...
            // This generates the drop-down list (select) of students from the relvant table ... 

            switch ($_SESSION['mode']){
                case "P": $rTable=$db->select("ScripResultsModerated"); break;
                case "E": $rTable=$db->select("ScripResultsExamModerated"); break;
                case "U": $rTable=$db->select("ScripResultsHomeGC"); break;
            };

            for ($i = 0; $i < count($rTable); $i++){
                $notFound = true;
                for ($j = 0; $j < count($tableStudentsOnly); $j++){
                    // If already in there, we don't want to add it and we don't even want to go any further down $tableStudentsOnly (hence break) ...
                    if ($rTable[$i]['StudentID'] == $tableStudentsOnly[$j]['ID']){
                        $notFound = false; break;
                    };
                };
                // Not in table so far - add it ...
                if ($notFound) {
                    $tableStudentsOnly[]['ID'] = $rTable[$i]['StudentID'];
                };
            };

            // How many students are there?
            $nStudents = count($tableStudentsOnly);
            // If there are more than zero ...
            if ($nStudents){
                for ($i = 0; $i < $nStudents; $i++){
                    // Look up the number in the ScripStudents table ...
                    $condition = array('StudentID'=>$tableStudentsOnly[$i]['ID']);
                    unset ($sTable); // ... this seems to be necessary to stop warnings on some FOREACH
                    $sTable = $db->select("ScripStudent", $condition);

                    foreach ($sTable as $row){
                        $tableStudentsOnly[$i]['FirstName'] = $row['FirstName'];
                        $tableStudentsOnly[$i]['LastName'] = $row['LastName'];
                    };
                };

                // If there are any students in the table, show them in the list ...

                // ... our list ($tableStudentsOnly) now contains the names as well as numbers

                // Q: How do we want to sort it? A: In order of surname.
                for ($i = 0; $i < count($tableStudentsOnly); $i++) {
                    $guide[] = $tableStudentsOnly[$i]['LastName'];
                };
                array_multisort($guide, SORT_ASC, SORT_STRING, $tableStudentsOnly);

                print("<select name='studentID' ");
                print("onchange='javascript:document.forms[0].submit()'");
                print(">");
                print("<option value=''>Select from ...");
                foreach ($tableStudentsOnly as $row) {
                    $name = $row['LastName'].", ".$row['FirstName'];
                    $ID = $row['ID'];
                    print("<option value=$ID");
                    if ($_SESSION['studentID'] == $ID) { print(" selected"); };
                    print(">$name");
                };
                print("</select>");
            } else {
                print("<p align='center'>");
                print("<font color='#ffffff'>No students found</font>");
                print("</p>");
            };
        } else {
            // User is a student and can therefore only look at own results ...





            $_SESSION['studentID'] = $_SESSION['ID'];

            $condition = array('StudentID'=>$_SESSION['studentID']);
            $table = $db->select("ScripStudent", $condition);
            foreach ($table as $row){
                $name = $row['FirstName']." ".$row['LastName'];
            };
//print ("Student! ".$name."<br>");

//            print("<font color=ffff00>You are ".$name." ".$_SESSION['MorS']." ".$_SESSION['authority']."</font>");
            print("<input type='hidden' name='studentID' value='".$_SESSION['ID']."'>");
        };
    }; // ... the IF fails if not enough information is available yet
    print("</td>");

    // NEW CELL 4 ... (Generic Code)
    print("<td align='center'>");
    print("<input type='submit' name='ChooseGC' value='Choose'>");
    print("</td>\n");
    print("</tr>\n");

    print("</form>\n");

    print("</table></div>\n");
    print("</td></tr>\n");
    print("<tr><td>$else</td></tr>\n");


    // We may jump when the last thing has been set; this is either the sub-mode (if a student) or a student ID (if a moderator) ...
    if ((!$_SESSION['fullAuthority'] && $submodeIsSet) || ($_SESSION['fullAuthority'] && $_SESSION['studentID'])){
        E_DisplayGenericCodeStatistics();
    };

    print("\n");
};

function E_DisplayGenericCodeStatistics(){

    // This function takes a student and some time specification and finds the student's generic results over that time period ... 

    global $case;
    global $_POST;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array('StudentID'=>$_SESSION['studentID']);
    $table = $db->select("ScripStudent", $condition);
    foreach ($table as $row){
        $name = $row['FirstName']." ".$row['LastName'];
    };

    // If in unsupervised mode, show the total number of excercises this user has done: each exercise wites one - and only one - zero to ScripResultsHomeGC ...
    if ($_SESSION['mode'] == "U"){
        $condition = array('StudentID'=>$_SESSION['studentID'], 'GenericComment'=>0);
        $table = $db->select("ScripResultsHomeGC", $condition);
        $nResults = count($table);
    };

    // Store it as a session variable as well ...
    $_SESSION['nResults'] = $nResults;

    // Read the text of each of the generic errors and store in an associative array with three parts ...
    $gCTable=$db->select("ScripGenericComments");
    foreach ($gCTable as $gCRow){
// Old version relied on things being in the right order in the table hence allowing the [] notation to be used
//        $allGenericComments['comment'][] = $gCRow['GenericComment'];
//        $allGenericComments['rectification'][] = $gCRow['GenericFeedback'];
//        $allGenericComments['ID'][] = $gCRow['ID'];

        // New version explicitly uses ID  as its main index ...
        $ID = $gCRow['ID'] - 1;
        // ... a temporary variable; note that we are using this in a zero-based array (and not the unity-based version found in the table) so it has to be adjusted

        // Now use this to fill up the main array ...
        $allGenericComments['ID'][$ID] = $gCRow['ID'];
        $allGenericComments['comment'][$ID] = $gCRow['GenericComment'];
        $allGenericComments['rectification'][$ID] = $gCRow['GenericFeedback'];
    };

    print("<table>");
    print("<tr><td>&nbsp;</td></tr>"); // ... blank line

    print("<tr><td colspan='2'>");
    print("<b><Font size=+1><center>Details for $name");
    if ($_SESSION['fullAuthority']) { print(" (".$_SESSION['studentID'].")"); };
    print("</center></font>");
    if ($_SESSION['mode'] == "U") {
        print("<Font size=+1><center> � $nResults exercises completed </center></font><br>");
        print("<center>Note: This is the total number of cases completed, including exercises attempted more than once.</center>");
    };
    print("</b>");
    print("</td></tr>");
   
    $_SESSION['bgcolor1'] = '#0058B0';
    $_SESSION['bgcolor2'] = '#003366';
    $_SESSION['bgcolor'] = $_SESSION['bgcolor1'];

    switch($_SESSION['mode']){
        case "P":

        // Knowing the student's ID, find the matching practice code ...
        $condition = array('StudentID'=>$_SESSION['studentID']);
        $table=$db->select("ScripStudent", $condition);
        foreach ($table as $row){
            $practiceCode = $row['PracticeCode'];
        };

        // Knowing the practice code, find the letter codes that correspond to the weeks selected in $_SESSION['weeksGC'] 
        $condition = array('PracticeCode'=>$practiceCode);
        $table=$db->select("ScripTermPattern", $condition);
        $counter = 0;
        // The reference to weeksGC is to the WEEKS (and not Practice Weeks) ...
        for ($i = 1; $i < 10 + 1; $i++){
            if ($_SESSION['weeksGC'][$i]){
                foreach ($table as $row){
                    $letterCode[$counter] = $row['wk'.$i];
                };
                $counter++;
            };
        };
        // ... the user has thus asked for the generic comments on the letter-codes stored in $letterCode

        // USAGE:
        // This is what we want to store in USAGE ... weeksGC[$i] and $letterCode[$i]
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
 
        // Read the time and date ...
        $time = date('G:i:s');
        $date = date('Y-n-j');

        for ($i = 0; $i < count($letterCode); $i++){

            // Write tab information ...
            $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
                          'RealStudent'=>$_SESSION['realStudent'], 
                          'WeekNumber'=>$weeksGC[$i], 'WeekCode'=>$letterCode[$i]);

            $db->insert("ScripUsageSM_GPractice", $text);

            // Find the ID of the number we've just inserted ...
            $ID = $db->getID();

            // We now want a "set number" that groups together all these entries. It is going to be the primary key of the
            // first set entered (i.e. when $i == 0). Therefore read it ...
            if ($i == 0) { $set = $ID; };

            // Set up $text and $condition ...
            $text = array('Set'=>$set);
            $condition = array('ID'=>$ID);
            // ... and update the line that we've just written ...
            $db->update("ScripUsageSM_GPractice", $text, $condition);
        };
        // ...end USAGE code 


        // Reverse the order so have most recent first ...
        if (count($letterCode) > 1) {
            $letterCode = array_reverse($letterCode);
        };

//print("Letters: ");print_r($letterCode);

        // Find the results from the correct table ...
        $condition = array('StudentID'=>$_SESSION['studentID']);
        $rTable=$db->select("ScripResultsModerated", $condition);
        // ... we're now looking at all the moderated results for the student in question

        // Each two-letter code (e.g. LD) will form a block of results: set the highest comment for each black at zero ...
        for ($i = 0; $i < count($letterCode); $i++){
            $highestComment[$i] = 0;
        };

        foreach ($rTable as $row){
            // Read the two letter code (by reading the case ID) ...
            $aCaseID = $row['CaseID'];
            $firstTwoLetters = substr($aCaseID, 0, 2);

            for ($i = 0; $i < count($letterCode); $i++){

                // Are the first two letters of $aCaseID in $letterCode? 
                if ($firstTwoLetters == $letterCode[$i]){
                    // Have found a caseID that fits: add in the generic comment  ...
                    $gC = $row['GenericComment'];
                    $genericCommentTotal[$i][$gC]++;

                    if ($gC > $highestComment[$i]) { $highestComment[$i] = $gC; }
                };
            };
            // ... finishing checking all required letters
        }; // ... finished checking all the possibilities for the student

        // We also need to find any generic comments to OTCs in ScripOTCModerated:
        $OTCcondition = array('StudentID'=>$_SESSION['studentID']);
        $OTCtable=$db->select("ScripOTCModerated", $condition);

        unset($timesCommitted);
        unset($genericCommentsModerator); // Do we need this??
        foreach($OTCtable as $OTCrow){ // [Very similar to above ...]
            // Read the two letter code (by reading the case ID) ...
            $aCaseID = $OTCrow['CaseID'];
            $firstTwoLetters = substr($aCaseID, 0, 2);

            for ($i = 0; $i < count($letterCode); $i++){
                if ($firstTwoLetters == $letterCode[$i]){
                    // Have found a caseID that fits: add in the generic comment  ...
                    $OTCcomments = $OTCrow['Comments'];

                    // If not nul ...
                    if ($OTCcomments){

//print("<br>Comments are $OTCcomments ");

                        // Add the comments in the i-th element of $genericCommentsModerator (possibly with a linebreak) ...
                        // Add a line break if necessary ...
                        if ($genericCommentsModerator[$i]) { $genericCommentsModerator[$i] .= "<br>"; };
                        $genericCommentsModerator[$i] .= $OTCcomments; // Add in the comments

                    };

                    // Also, count how often each of the different OTC faults has been committed ...
                    if ($OTCrow['OOD'] > 0) { $timesCommitted[$i]['OOD'] += 1; };
                    if ($OTCrow['OOD2'] > 0) { $timesCommitted[$i]['OOD'] += 1; };
                    if ($OTCrow['Negative'] > 0) { $timesCommitted[$i]['NoProduct'] += 1; };
                    if ($OTCrow['Negative2'] > 0) { $timesCommitted[$i]['NoProduct'] += 1; };
                    if ($OTCrow['WrongQtyLegal'] > 0) { $timesCommitted[$i]['WQL'] += 1; };
                    if ($OTCrow['WrongQtyLegal2'] > 0) { $timesCommitted[$i]['WQL'] += 1; };
                    if ($OTCrow['NA'] > 0) { $timesCommitted[$i]['FailedNA'] += 1; };
                    if ($OTCrow['AAI'] > 0) { $timesCommitted[$i]['AAI'] += 1; };
                    if ($OTCrow['AAI2'] > 0) { $timesCommitted[$i]['AAI'] += 1; };
                    if ($OTCrow['CDSig'] > 0) { $timesCommitted[$i]['SigCheck'] += 1; };

                    // Explode serious error type and count the number of elements ...
                    $explode = explode(" ", $OTCrow['SeriousErrorType']);

                    // Remove any spurious nul elements that the explode operation has produced ...
                    // [e.g. in a ten-element array, need to loop from 9 down to 0]
                    for ($k = count($explode); $k > 0; $k--){
                        $km1 = $k - 1;
                        if ($explode[$km1] == ""){ array_splice($explode, $km1, 1); }; // ... remove 1 element starting from element being considered (which means this element only)
                    };
                 
                    // The array $explode now contains the elements so we can loop safely over the array ...
                    for ($k = 0; $k < count($explode); $k++){
                        // Add each type into its own total ...
                        $timesCommitted[$i]['SeriousError'][$explode[$k]]++;
                    };



  //                  $nSeriousErrorType = count($explode);
                    // Add into the total for this set ...
//                    if ($nSeriousErrorType) { $timesCommitted[$i]['SeriousError'] += $nSeriousErrorType; };
                };
            };
        };

        // Now show it ...
        print("<table width='70%'><tr>");
        // ... headings ...
        print("<th>Times<br>Committed</th><th>Description</th>");
        print("</tr>");

        // Show result on the screen: loop over the blocks (i.e. the two-letter codes) ...
        for ($j = 0; $j < count($letterCode); $j++){
// Show in reverse order ...
//        for ($j = count($letterCode) - 1; $j > -1; $j--){
            print("<tr><td align='center'><br><font size='+1'>Set: ".$letterCode[$j]."</font></td></tr>");
            H_ShowSetOfData($j, $allGenericComments, $genericCommentTotal, $genericCommentsModerator, $timesCommitted);
        };

        break;


        case "E":
        // The sub-modes (in $_SESSION['DEFGC']) here are: D (D1 and D2); E; or F. We
        // can identify them thus: D will come from ScripExamPatterns (because the two D are two of the three old exams)  
        // and E and F from ScripResits (because E and F are the two of the six old re-sits) ... 

        // Now - Summer 2014 - E is examination (with dates from ScripDates); D is not used; and F comes from re-sits (ScripResits) ...

        switch($_SESSION['DEFGC']){

            case "D":
// Case D no longer used April 2014
                // Knowing the student's ID, find the matching exam code ...
                $condition = array('StudentID'=>$_SESSION['studentID']);
                $table=$db->select("ScripStudent", $condition);
                foreach ($table as $row){
                    $examCode = $row['ExamCode'];
                };

                // Knowing the exam code, find the letter codes that correspond to the weeks selected in $_SESSION['weeksGC'] 
                $condition = array('ExamCode'=>$examCode);
                $table=$db->select("ScripExamPatterns", $condition);
                foreach ($table as $row){
                    $letterCode[0] = $row['wk1'];
                    $letterCode[1] = $row['wk2'];
                };
            break;


            case "E":
// April 2014
                // Knowing the student's ID, look in ScripResit ...
                $condition = array('StudentID'=>$_SESSION['studentID']);
                $table=$db->select("ScripStudent", $condition);
//                $table=$db->select("ScripResit", $condition);
//                foreach ($table as $row){
//                    $letterCode[0] = $row['R1'];
//                };

                // Find the student's exam pattern ...
                foreach ($table as $row){
                    $examCode = $row['ExamCode'];
                };

                // Knowing the exam code, find the letter codes that correspond to the weeks selected in $_SESSION['weeksGC'] 
                $condition = array('ExamCode'=>$examCode);
                $table=$db->select("ScripExamPatterns", $condition);
                foreach ($table as $row){
                    $letterCode[0] = $row['wk1'];
// November 2014: Next line remmed out until E2 is sat in Semester 1 exams in January 2015 - so students don't see extraneous exam title for second set
//                    $letterCode[1] = $row['wk2'];


                    // Special code for exam failure on November 20th, 2014 ...
                    $examRerun = $row['wk2'];
                    // ... most students will have done Exam 1 and not Exam 2; some will have started Exam 1 (crashed part of the way through) and all Exam 2
                };

                // Before we leave the switch statement, we must have the correct letter (Week 1 or Week 2) stored in $letterCode[0] - we can then carry on with
                // the previous code. So -

                // Read the exam results for the student from the moderated exam table ...
                $condition = array('StudentID'=>$_SESSION['studentID']);
                $rTable=$db->select("ScripResultsExamModerated", $condition);

                // Loop over all the lines of moderated exam results one by one ...
                foreach ($rTable as $rRow){
                    // Get the two-letter code for the current line ...
                    $twoLetterCode = $rRow['CaseID'][0] . $rRow['CaseID'][1];

                    // If this two-letter code is the code of the re-run ($examRerun), then we set $letterCode[0] to $examRerun ...
                    if ($twoLetterCode == $examRerun){
                        $letterCode[0] = $examRerun;
                        // ... and we can leave the foreach loop ...
                        break;
                    };
                    // ... if we reach the end of loop and have not encountered an re-run lines, the student will be shown comments frome the first exam.
                };

            break;


            case "F": case "R":
// should this F be an R too? Let's be cautious!
                // Knowing the student's ID, look in ScripResit ...
                $condition = array('StudentID'=>$_SESSION['studentID']);
                $table=$db->select("ScripResit", $condition);
                foreach ($table as $row){
                    // Should only be one matching row - because students will have one entry only (or, to be prescise, none if not doing re-sit) ...
                    // Formerly when Week 1 corresponded to E; now E is the standard code for Examination
//                    $letterCode[0] = $row['R2'];
                    $letterCode[0] = $row['R1'];
                    // Only set next element of $letterCode if cell is not null ...
                    if ($row['R2'] != "") { $letterCode[1] = $row['R2']; };
                };
            break;
        };

        // Reverse the order so have most recent first ...
        if (count($letterCode) > 1) {
            $letterCode = array_reverse($letterCode);
        };

        // Read the exam results from the moderated table ...
        $condition = array('StudentID'=>$_SESSION['studentID']);
        $rTable=$db->select("ScripResultsExamModerated", $condition);

//print_r($condition);

//print_r($rTable);

        // Now carry out the usual procedure
        // Each two-letter code (e.g. LD) will form a block of results: set the highest comment for each block at zero ...
        // ["highest comment" means most frequently occurring code for the current student]
        for ($i = 0; $i < count($letterCode); $i++){
            $highestComment[$i] = 0;
        };

        foreach ($rTable as $row){
            // Look at the cases that are available in ScripResultsExamModerated ...
            $aCaseID = $row['CaseID'];
            $firstTwoLetters = substr($aCaseID, 0, 2); // ... first two letters of the case

            // Loop over the letter codes we're looking for ...
            for ($i = 0; $i < count($letterCode); $i++){
                // Are the first two letters of $aCaseID in $letterCode? 
                if ($firstTwoLetters == $letterCode[$i]){
                    // Have found a caseID that fits: add in the generic comment  ...
                    $gC = $row['GenericComment'];

                    // If the penalty is zero, ignore this line; otherwise, add it to the relevant total ... 
                    if ($row['Penalty']) {
                        $genericCommentTotal[$i][$gC]++;
//print("<br>Found an error ($gC) with $aCaseID");
                    };

                    if ($gC > $highestComment[$i]) { $highestComment[$i] = $gC; };
                };
                // Reminder: $genericCommentTotal[$i][$gC] where $i is the first two-letter code, second two-letter code and so on; $gC is the serial number of the generic code
//print_r($genericCommentTotal[$i][$gC]);
            };
            // ... finishing checking all required letters

        }; // ... finished checking all the possibilities for the student

        // We also need to find any generic comments to OTCs in ScripOTCExamModerated:
        $OTCcondition = array('StudentID'=>$_SESSION['studentID']);
        $OTCtable=$db->select("ScripOTCExamModerated", $condition);

        foreach($OTCtable as $OTCrow){ // [Very similar to above ...]

            // Read the two letter code (by reading the case ID) ...
            $aCaseID = $OTCrow['CaseID'];
            $firstTwoLetters = substr($aCaseID, 0, 2);

            for ($i = 0; $i < count($letterCode); $i++){ // i is the week number ...
                if ($firstTwoLetters == $letterCode[$i]){
                    // Have found a caseID that fits: add in the generic comment  ...
                    $OTCcomments = $OTCrow['Comments'];

                    if ($OTCcomments){
                        // Add the comments in the i-th element of $genericCommentsModerator (possibly with a linebreak) ...
                        if ($genericCommentsModerator[$i]) { $genericCommentsModerator[$i] .= "<br>"; };
                        $genericCommentsModerator[$i] .= $OTCcomments; 
//print("<br>Found an OTC error with $aCaseID");
                    };

                    // Also, count how often each of the different OTC faults has been committed ...
                    if ($OTCrow['OOD'] > 0) { $timesCommitted[$i]['OOD'] += 1; };
                    if ($OTCrow['OOD2'] > 0) { $timesCommitted[$i]['OOD'] += 1; };
                    if ($OTCrow['Negative'] > 0) { $timesCommitted[$i]['NoProduct'] += 1; };
                    if ($OTCrow['Negative2'] > 0) { $timesCommitted[$i]['NoProduct'] += 1; };
                    if ($OTCrow['WrongQtyLegal'] > 0) { $timesCommitted[$i]['WQL'] += 1; };
                    if ($OTCrow['WrongQtyLegal2'] > 0) { $timesCommitted[$i]['WQL'] += 1; };
                    if ($OTCrow['NA'] > 0) { $timesCommitted[$i]['FailedNA'] += 1; };
                    if ($OTCrow['AAI'] > 0) { $timesCommitted[$i]['AAI'] += 1; };
                    if ($OTCrow['AAI2'] > 0) { $timesCommitted[$i]['AAI'] += 1; };
                    if ($OTCrow['CDSig'] > 0) { $timesCommitted[$i]['SigCheck'] += 1; };

                    // Explode serious error type and count the number of elements ...
                    $explode = explode(" ", $OTCrow['SeriousErrorType']);


//print($OTCrow['CaseID']." ".$OTCrow['SeriousErrorType']."<br />");

                    // Remove any spurious nul elements that the explode operation has produced ...
                    // [e.g. in a ten-element array, need to loop from 9 down to 0]
                    // $k is the type of serious error number
                    for ($k = count($explode); $k > 0; $k--){
                        $km1 = $k - 1;
                        if ($explode[$km1] == "" || $explode[$km1] == "0" ){
                            array_splice($explode, $km1, 1);
                        }; // ... remove 1 element starting from element being considered (which means this element only)
                    };

                    // The array $explode now contains the elements so we can loop safely over the array ...
                    for ($k = 0; $k < count($explode); $k++){
                        // Add each type into its own total ...
//                        $timesCommitted[$i]['SeriousError'][$k]++;
                        $timesCommitted[$i]['SeriousError'][$explode[$k]]++;
                    };

                    // $timesCommitted is a three-dimensional array: i is the week number; [] is the error; k is the type of serious error ...

//print("Now add in ".$OTCrow['CaseID']."<br />");

//print("<pre>");
//print_r($timesCommitted);
//print("</pre>");


  //                  $nSeriousErrorType = count($explode);
                    // Add into the total for this set ...
//                    if ($nSeriousErrorType) { $timesCommitted[$i]['SeriousError'] += $nSeriousErrorType; };
                };
            };

        };

        // Now show it:
        print("<table>");
        // Heading ...
        print("<tr><th>Type</th><th>Description</th><th>Number</th></tr>");

        // Show result on the screen: loop over the blocks (i.e. the two-letter codes) ...

        // The sub-routine is designed to contain blocks (in P, the blocks are weeks; in U, the blocks are specified by the user).
        // The exams are to be shown all in one group.
        // At present, E and F are in one group in any case i.e. $letterCode has one element only. D, however, has D1 and D2. Add these together
        // and store them in element 0 ...

//print("<br>SESSION['DEFGC'] is " . $_SESSION['DEFGC'] . "<br>");

        if ($_SESSION['DEFGC'] == "|E"){
// Don't know why there's a vertical bar before the D !!!
//        if ($_SESSION['DEFGC'] == "|D"){
//print("<pre>");
//print_r($genericCommentTotal);
            // Lots of concatenation - since weeks are to be added:
            // Note: Very hard-wired - only works for two weeks. Would have to be amended if e.g. added thee weeks together
            // (1) Add $genericCommentTotal[1][$gC] to $genericCommentTotal[0][$gC] for all $gC ...
//print_r($allGenericComments);
            // How many types of generic comments are there (consult the relevant table - or, easier, the variable that it has been read into)?
            $nComments = count($allGenericComments[ID]);
//print("Comments: $nComments<br />");

            // Use that in the loop ...
            for ($k = 0; $k < $nComments; $k++){
//print("<br />$k: ".$genericCommentTotal[0][$k]." + ".$genericCommentTotal[1][$k]);
                $genericCommentTotal[0][$k] += $genericCommentTotal[1][$k];
            };

            // (2) Concatenate the generic comments from the moderator ...
            $genericCommentsModerator[0] .= "<br />".$genericCommentsModerator[1];

            // (3a) The same thing for the two-dimensional parts of $timesCommitted ...
            $timesCommitted[0]['OOD'] += $timesCommitted[1]['OOD'];
            $timesCommitted[0]['No Product'] += $timesCommitted[1]['No Product'];
            $timesCommitted[0]['WQL'] += $timesCommitted[1]['WQL'];
            $timesCommitted[0]['FailedNA'] += $timesCommitted[1]['FailedNA'];
            $timesCommitted[0]['AAI'] += $timesCommitted[1]['AAI'];
            $timesCommitted[0]['SigCheck'] += $timesCommitted[1]['SigCheck'];

            // (3b) Something similar for the three-dimensional part of $timesCommitted ...
            // What's the biggest number of ID in serious errors? (Allows us to know how far to take the loop below.)
            $table = $db->select("ScripSeriousErrorTypes");
            $maxID = 0;
            foreach ($table as $row){
                if ($row['ID'] > $maxID) { $maxID = $row['ID'];  };
            };

            // Again add all from "1" to "0" ...
            for ($k = 0; $k < $maxID + 1; $k++){
                $timesCommitted[0]['SeriousError'][$k] += $timesCommitted[1]['SeriousError'][$k];
            };
        };

//print("XX<pre>");
//print($timesCommitted[0]['SeriousError']);


        // Now run the function ...
//        H_ShowSetOfData(0, $allGenericComments, $genericCommentTotal, $genericCommentsModerator);

//print("<pre>");
//print_r($timesCommitted);


//print("<pre>");
//print("<br>allGenericComments");
//print_r($allGenericComments);

//print("<br>genericCommentTotal");
//print_r($genericCommentTotal);


        // The first parameter is the block of data (e.g. weeks for P, chosen by student for U)

        // The array $allGenericComments itself contains these arrays:
        //     (1) comment - example entry "You incorrectly classified a type of form."
        //     (2) rectification - example entry "Refer to the lecture ..."
        //     (3) pointers

        // The array $genericCommentTotal is the total times each fault was committed

        // The remaining two parameters are the same thing but for the faults that the demonstrators looks after
 
//        H_ShowSetOfData(0, $allGenericComments, $genericCommentTotal, $genericCommentsModerator, $timesCommitted);

//        for ($kk = 0; $kk++; $kk < count($genericCommentTotal)){
//            H_ShowSetOfData($kk, $allGenericComments, $genericCommentTotal, $genericCommentsModerator, $timesCommitted);
//        };


// Taken from the "P" code
        for ($j = 0; $j < count($letterCode); $j++){
//print($j);
            print("<tr><td align='center'><font size='+1'>Assessment: " . (count($letterCode) - $j) . "</font></td></tr>");
            H_ShowSetOfData($j, $allGenericComments, $genericCommentTotal, $genericCommentsModerator, $timesCommitted);
        };




        break;


        case "U":

        // Write usage for unsupervised mode ...
//        $db=new dbabstraction();
//        $db->connect() or die ($db->getError());
 
        // Read the time and date ...
//        $time = date('G:i:s');
//        $date = date('Y-n-j');

        // Write tab information ...
//        $text = array('UserID'=>$_SESSION['studentID'], 'Date'=>$date, 'Time'=>$time,
//                      'RealStudent'=>$_SESSION['realStudent'], 
//                      'NGroups'=>$_SESSION['numberGC'], 'GroupsOf'=>$_SESSION['sizeGC']);

//        $db->insert("ScripUsageSM_GUnsupervised", $text);


        // Back to code ...
        if ($_SESSION['sizeGC'] && $_SESSION['numberGC']) {

            print("<tr><td colspan='2'>Each group contains ".$_SESSION['sizeGC']." case");
            if ($_SESSION['sizeGC'] > 1) { print("s"); };
            print(" with the most recent group first.</td></tr>");

            // Get the results from the appropriate results table ...
            $condition = array('StudentID'=>$_SESSION['studentID']);
            $rTable=$db->select("ScripResultsHomeGC", $condition);

            if (count($rTable) > 1) {
                $reversedTable = array_reverse ($rTable);
            } else {
                $reversedTable = $rTable;
            };

            // Descending numerical order ...
            for ($i = 0; $i < count($rTable); $i++) {
               $guide[] = $rTable[$i]['ResultID'];
            };
            array_multisort($guide, SORT_DESC, SORT_NUMERIC, $rTable);

            $reversedTable = $rTable;

            $finished = 0; // ... has the program done as many as the user has asked for?
            // There are two counters:
            // (A) $sizeCounter counts the size of the current bundle. If a bundle consists of 5 scripts, then the counter
            // runs from 0 to 4, being incremented for each new script. When it reaches 5, it is reset to zero and $numberCounter
            // (v.i.) is incremented ...
            // (B) $numberCounter counts the number of bundles. If we want 3 bundles, it runs from 0 to 2
            $sizeCounter = 0;
            $numberCounter = 0;
            foreach ($reversedTable as $row){

                // Read the GenericComment and check to see whether it is zero ...
                $gC = $row['GenericComment'];

                if ($gC == 0){
                    // Yes, it is zero - increment the size counter
                    $sizeCounter++;
                    // Has the sizecounter now reached its maximum?
                    if ($sizeCounter == $_SESSION['sizeGC']){
                       if ($numberCounter == $_SESSION['numberGC'] - 1){
                            // ... ended - record this ...
                            $finished = 1;
                        } else {
                            // ... not ended - set the new higestComment ...
                            $sizeCounter = 0; // ... re-set the size counter
                            $numberCounter++; // ... increment the number counter
                            $highestComment[$numberCounter] = 0;
                        };
                    };
                } else {
                    // No, it isn't zero - add into the correct total ...
                    $genericCommentTotal[$numberCounter][$gC]++;
                    if ($gC > $highestComment[$numberCounter]){ $highestComment[$numberCounter] = $gC; };
                };

                if ($finished) { break; }; // ... finished - don't do any more!

            };

            if (!$finished){
                // There were few entries than the user wanted - note where the above code finished ...
                $lastNumber = $numberCounter;
                $lastSize = $sizeCounter;
            };

            // Show result on the screen: loop over the blocks ...
            for ($j = 0; $j < count($genericCommentTotal); $j++){
                H_ShowSetOfData($j, $allGenericComments, $genericCommentTotal, "");
            };

            // It's useful to know whether the last grouping was incomplete (e.g. user asked for 10 groups of 10 but has only done 99 cases):
            // If it wasn't finished and $lastSize was non-zero ...
            if (!$finished && $lastSize){
                // Comment about last group being incomplete ...
                print("<tr><td colspan='2'>LAST GROUP CONTAINED ".$lastSize." CASE");
                if ($lastSize > 1) { print("S"); }; // ... plural "S"
                print(" ONLY (and not ".$_SESSION['sizeGC'].")</td><tr>");
            };

        };

        break;
    };

    print("</td>");

    print("</tr>");

    print("</table>");

};


function H_ShowSetOfData($j, $allGenericComments, $genericCommentTotal, $gCD, $tC){

//print("X");
//print("<pre>");
//print_r($tC);

//print_r($allGenericComments['comment']);


//print("<pre>");
//print_r($genericCommentTotal);

    global $case;

    // We would like to ORDER $allGenericComments in DECREASING NUMBER OF ERRORS (part of $genericCommentTotal) ...

    $nGC = count($allGenericComments['comment']);

    // The guide array is the number of times each fault has been committed and we shall order the pointer:
    // The array $genericCommentTotal is not zero-based - beware of this when creating $guide ...
    for ($i = 1; $i < $nGC + 1; $i++){
        $guide[$i-1] = $genericCommentTotal[$j][$i];
        // ... note that $j has been passed into this array.
    };
//print("<pre>");
//print_r($guide);
    // A fault that has not been committed has a nul entry; replace it by zero. Also, create an array that is simply the pointer (0, 1, 2, ...) 
    for ($i = 0; $i < count($allGenericComments['comment']); $i++){
        if ($guide[$i] == ""){ $guide[$i] = 0; };
        $gCID[$i] = $i;
    };
//print("<pre>");
//print_r($guide);

    // Sort the pointer acording to the number of faults committed (most first; least last) ...
    array_multisort($guide, SORT_DESC, SORT_NUMERIC, $gCID);
//print("<pre>");
//print_r($guide);
//print_r($gCID);


    // Change the colour ...
    if ($_SESSION['bgcolor'] == $_SESSION['bgcolor1']) {
        $_SESSION['bgcolor'] = $_SESSION['bgcolor2'];
    } else if ($_SESSION['bgcolor'] == $_SESSION['bgcolor2']) {
        $_SESSION['bgcolor'] = $_SESSION['bgcolor1'];
    };

    print("<table border ='1' align='center' bgcolor=".$_SESSION['bgcolor']." width=500>");

    if ($j == 0) {
        print("<tr><td colspan='2'><br><b>MOST RECENT</b></td></tr>");
    } else if ($j == 1) {
        print("<tr><td colspan='2'><br><b>NEXT MOST RECENT</b></td></tr>");
    } else {
        print("<tr><td colspan='2'><br><b>MOST RECENT AFTER THAT</b></td></tr>");
    };

    print("<tr>");
    print("<td align='center'>Number<br> of Errors<br>Committed</td>");
    print("<td align='center'>Description of Error</td>");
    print("</tr>");

    for ($i = 0; $i < count($guide); $i++){
        // Only display if number of errors in non-zero ...
        if ($guide[$i]){
//print("<br>i is $i and guide(i) is ".$guide[$i]);
            // We have an error to show ...
            print("<tr>");
            print("<td><center>".$guide[$i]."<center></td>");

            // The description as a hyperlink ...
            print("<td><a href=javascript:void(0)\n");
            print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$allGenericComments['ID'][$gCID[$i]].'\'');
            print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
            print($allGenericComments['comment'][$gCID[$i]]."</a></td>\n");

            print("</tr>");
        };
    };

    // Now list the OTC faults; here is a list ...
    //     Out of date: $tC[$i]['OOD']
    //     No product: $tC[$i]['NoProduct']
    //     Wrong Quantity Legal: $tC[$i]['WQL']
    //     Failed name and address: $tC[$i]['FailedNA']
    //     AAI: $tC[$i]['AAI']
    //     CD signature: $tC[$i]['SigCheck']

    $number = $tC[$j]['OOD'];
    if ($number > 0){
        print("<tr>");
        print("<td><center>$number</center></td>");

        $text = "Out of date";
        $destination = 43;
        print("<td><a href=javascript:void(0)\n");
        print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$destination.'\'');
        print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
        print($text."</a></td>\n");

        print("</tr>");
    };

    $number = $tC[$j]['NoProduct'];
    if ($number > 0){
        print("<tr>");
//        $number = $tC[$j]['OOD'];
        print("<td><center>$number</center></td>");

        $text = "No product supplied";
        $destination = 49;
        print("<td><a href=javascript:void(0)\n");
        print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$destination.'\'');
        print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
        print($text."</a></td>\n");

        print("</tr>");
    };

    $number = $tC[$j]['WQL'];
    if ($number > 0){
        print("<tr>");
        print("<td><center>$number</center></td>");

        $text = "Wrong quantity supplied - legal error";
        $destination = 54;
        print("<td><a href=javascript:void(0)\n");
        print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$destination.'\'');
        print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
        print($text."</a></td>\n");

        print("</tr>");
    };

    $number = $tC[$j]['FailedNA'];
    if ($number > 0){
        print("<tr>");
        print("<td><center>$number</center></td>");

        $text = "Failed to do name and address check";
        $destination = 57;
        print("<td><a href=javascript:void(0)\n");
        print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$destination.'\'');
        print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
        print($text."</a></td>\n");

        print("</tr>");
    };

    $number = $tC[$j]['AAI'];
    if ($number > 0){
        print("<tr>");
        print("<td><center>$number</center></td>");

        $text = "Additional associated item either not supplied or incorrectly supplied";
        $destination = 55;
        print("<td><a href=javascript:void(0)\n");
        print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$destination.'\'');
        print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
        print($text."</a></td>\n");

        print("</tr>");
    };

    $number = $tC[$j]['SigCheck'];
    if ($number > 0){
        print("<tr>");
        print("<td><center>$number</center></td>");

        $text = "CD signature check not done or unnecessarily done";
        $destination = 56;
        print("<td><a href=javascript:void(0)\n");
        print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$destination.'\'');
        print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
        print($text."</a></td>\n");

        print("</tr>");
    };

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Whereas the variable $number is just a number in the above examples, it is an array here (otherwise the
    // code is very similar) ...
    $number = $tC[$j]['SeriousError'];
    // ... e.g. $number[5]=2 means error 5 has been committed twice. 
    // N.B. As in the above example, the array will probably not be complete - just looping over the NUMBER of elements
    // may not be enough to reach all the elements that actually exist. Use the largest value of ID in ScripSeriousErrorTypes as an upper limit. 

    $nTypesOfError = 0;
    $table=$db->select("ScripSeriousErrorTypes");
    foreach ($table as $row){
        if ($row['ID'] > $nTypesOfError){ $nTypesOfError = $row['ID']; };
    };

    // We now loop over the elements: each element being a type of serious error committed ...
//    $nTypesOfError = 10;
//    for ($k = 0; $k < count($tC[$j]['SeriousError']); $k++){
    for ($k = 0; $k < $nTypesOfError + 1; $k++){
    // (Need "+ 1" because error types start at 1 (and not 0).) 
//print($k);
//print($number[$k]);
        if ($number[$k] > 0){ // There is no serious error zero ...

            $condition = array("ID"=>$k);
            $table = $db->select("ScripSeriousErrorTypes", $condition);
            foreach ($table as $row){
                $destination = $row['GenericComment'];
            };

            $condition = array("ID"=>$destination);
            $table = $db->select("ScripGenericComments", $condition);
            foreach ($table as $row){
                $text = $row['GenericComment'];
            };
            print("<tr>");

            print("<td><center>".$number[$k]."</center></td>");
            print("<td><a href=javascript:void(0)\n");
            print('onClick="open(\'ScripModerate.php?tab=57&rectification='.$destination.'\'');
            print(",'miniwin','toolbar=0,menubar=1,scrollbars=1,width=1150,height=600')\">");
            print($text."</a></td>\n");  
            print("</tr>");
        };
    };

    // Are there any comments from the demonstrator - if so, show them (they are the $j-th element of the array $gCD) ...
    if (count($gCD[$j])){
//        for ($kk = 0; $kk < count($gCD[$j]); $kk++){
            print("<tr>");
            print("<td align='center'>From Demonstrator</td>");
//            print("<td>".$gCD[$kk]."</td>");
            print("<td>".$gCD[$j]."</td>");
            print("<tr>");
//        };
    };

    print("</table>");
};



function F_DisplayGenericCodeRectification($rectification){

    global $case;
    global $_POST;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    print("<table width='60%'><tr>");
    print("<td>");

    print("Feedback comment:<br><br>");

    $condition = array('ID'=>$rectification);



    // Read the correction ...
    $gCTable=$db->select("ScripGenericComments", $condition);
    foreach ($gCTable as $gCRow){
        print($gCRow['GenericFeedback']);
    };

    print("</td>");
    print("</tr></table>");

};

function CH_ShowChecking(){
    global $case;
  
    // Read ScripCheckingResults ...

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $checkingText = "";
    $cumulative = 0;
    $cumulativeMaximum = 0;


    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
        $condition = array ('StudentID'=>$_SESSION['studentID']);
    } else {
        $condition = array ('StudentID'=>$_SESSION['studentID'], 'Release'=>1);
    };
    $table = $db->select("ScripCheckingResults", $condition);
    foreach ($table as $row) {
        $penalty = $row['Penalty'];
        $cumulative += (10 - $penalty);
        $cumulativeMaximum += 10;
        $checkingText.= $penalty." - ";
        $checkingText.= $row['Tab'].": ";
        $checkingText.= $row['Comment']."\n";
    };    

    print("<div class=tablecream>");
    print("<table bgcolor='#0000EE' border=1 width=100% align='center'>");

    print("<tr><td colspan='4'>");
    print("NOT PART OF THE DISPENSING MARK - The numbers in the box are the penalties incurred in each of the exercises. "); 
    print("The number to the side of the box is the total.<br>"); 
    print("</td></tr>");

    print("<tr><td align='center'>Total Marks<br>");
    print("(out of $cumulativeMaximum):<br>");
    print("<br><b>$cumulative</b>");
    print("</td>");
    print("<td align='center' colspan='3'>");
    print("<textarea name=marking rows=10 COLS=80");
    if (!$_SESSION['fullAuthority']) { print(" readonly"); };
    print(">");
    print("$checkingText");
    print("</textarea></td>");
    print("</tr>");

    print("</table></div>\n");
  
};


function E_Main() {
    global $case;

    I_StudentCase();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition=array("StudentID"=>$_SESSION['studentID'],
	 "CaseID"=>$_SESSION['caseID']);
    $table=$db->select("ScripScore",$condition);
    foreach ($table as $row) {
        $Classify=$row['Classify'];
        $Verify=$row['Verify'];
        $Label=$row['Label'];
        $Endorse=$row['Endorse'];
        $File=$row['File'];
        $Probes=$row['Probes'];
        $Fatal=$row['Fatal'];
    };


    ZZ_DoAllMarkCalculations();

//    EA_MainResults();
//    FA_CDResults();
//    GA_PORResults();
//    HA_CommsResults();

    // Calculate the sub-totals 
//    ZH_subtotalsOTC();
//    ZG_subtotalsPOR();
//    ZF_subtotalsCD();
//    ZD_subtotalsChanges();
//    ZE_subtotalsMain();
//    ZI_subtotalsCombine();

    print("<tr><td VALIGN=TOP align=center>\n");
    print("<form ACTION=ScripModerate.php method=post>\n");
    print("<INPUT TYPE=HIDDEN name=tab value=12>");

    print("<div class=tablecream>");
    print("<TABLE BGCOLOR='#EEEEEE' BORDER=1 WIDTH=100% ALIGN=CENTER>");
    print("<TR>");

    print("<TD ALIGN=CENTER COLSPAN='2'>Student (with initials): ");
    print("<b>".$_SESSION['firstname']." ".$_SESSION['lastname']." - ".$_SESSION['initials']."</b></TD>\n");

    // Next cell ...
    print("<td align='center'>Case: ");

    // Double-sided forms are identified in the IF part of the IF-THEN-ELSE
    // In case form number is needed, it is $_SESSION['formID']

    if ($_SESSION['group'] == 9 || $_SESSION['group'] == 10) {
        // Double-sided form - front ...
        print("<a href=javascript:void(0)\n");
        print('onClick="open(\'');
        print('ScripModerate.php?tab=200&side=0\'');
        print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
        print(",width=500,height=700");
        print("')\">");


        // Old version - hard-wired ...
//        print("<a href=javascript:void(0)\n");
//        print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/');
//        print('scripware/ScripModerate.php?tab=200&side=0\'');
//        print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
//        print(",width=500,height=700");
//        print("')\">");
        print("<b>".$_SESSION['caseID']." front</b></a>");

        // Double-sided form - back ...
        print("; ");
        print("<a href=javascript:void(0)\n");
        print('onClick="open(\'');
        print('ScripModerate.php?tab=200&side=1\'');
        print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
        print(",width=500,height=700");
        print("')\">");

        // Old version (hard-wired) ...
//        print("<a href=javascript:void(0)\n");
//        print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/');
//        print('scripware/ScripModerate.php?tab=200&side=1\'');
//        print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
//        print(",width=500,height=700");
//        print("')\">");
        print("<b>".$_SESSION['caseID']." back</b></a>");
    } else {
        // Single-sided form ...
        print("<a href=javascript:void(0)\n");
        print('onClick="open(\'');
        print('ScripModerate.php?tab=200\'');
        print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
        print(",width=500,height=700");
        print("')\">");

        // Old version (hard-wired) ...
//        print("<a href=javascript:void(0)\n");
//        print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/');
//        print('scripware/ScripModerate.php?tab=200\'');
//        print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
//        print(",width=500,height=700");
//        print("')\">");

        print("<b>".$_SESSION['caseID']."</b></a>");
    };

    print("&nbsp;&nbsp;&nbsp;&nbsp;Seat: <b>".$_SESSION['seat']."</b>");
    if ($_SESSION['lastModID']) { print("  (Moderator: ".$_SESSION['lastModID'].")"); };
    print("</td>\n");

    print("<TD ALIGN=CENTER>Script Score (".$_SESSION['maxscore']."): <b>".$_SESSION['finalScore']."</b></TD>");
    print("</TR>\n");

//    print("<TR><TD COLSPAN='7'>\n\n");
//    print("<TABLE>");

//    print("<TR>");

//    print("<TD ALIGN='CENTER' WIDTH='33%'>Out of date ");
//    print("<i>or</i> No Product");
//    print("<INPUT TYPE='CHECKBOX' NAME='OODNoProd' ");
//    if (!$_SESSION['fullAuthority']){print("DISABLED ");}
//    if ($_SESSION['OODNoProd']==on) {
//        print ("CHECKED ");
//    } 
//    print("><br>");
//    print("<INPUT TYPE=TEXT NAME=lostOODNoProdName VALUE = $_SESSION['lostOODNoProd']
//         READONLY SIZE=3>");
//    print("</TD>");

//    print("<TD ALIGN='CENTER' WIDTH='33%'>Serious Error (from below)");
//    print("<INPUT TYPE='CHECKBOX' NAME='Fatal' ");
// //    if (!$_SESSION['fullAuthority']){print("DISABLED ");}
//    print("DISABLED ");
//    if ($_SESSION['Fatal']==on) {
//        print ("CHECKED ");
//    }

//    print("><br>");
//    print("<INPUT TYPE=TEXT NAME=lostFatal VALUE = ".$_SESSION['lostFatal']." READONLY SIZE=3>");
//    print("</TD>");

//    print("<TD ALIGN='CENTER' WIDTH='33%'>N/A Check Failed");
//    print("<INPUT TYPE='CHECKBOX' NAME='NACheck' ");
//    if (!$_SESSION['fullAuthority']) { print("DISABLED "); }
//    if ($_SESSION['NACheck']==on) { print ("CHECKED "); };
//    print("><br>");
//    print("<INPUT TYPE=TEXT NAME=lostNACheck 
//         VALUE = '".$_SESSION['lostNACheck']."' READONLY SIZE=3>");
//    print("</TD>");

//    print("</TR></TABLE>");
//    print("</TD></TR>");    


    print("<TR><TD COLSPAN='5'><TABLE><TR>");
    print ("<TD ALIGN=CENTER WIDTH='14%'>Classify<br>");
    print("<INPUT TYPE=TEXT NAME=classify READONLY VALUE='".$_SESSION['lostClassify']."' SIZE=3>");
    print("</TD>"); 

    print ("<TD ALIGN=CENTER WIDTH='14%'>Verify<br>");
    print("<INPUT TYPE=TEXT NAME=verify READONLY VALUE='".$_SESSION['lostVerify']."' SIZE=3>");
    print("</TD>"); 

    $_SESSION['lostRegisters']=$_SESSION['lostCD']+$_SESSION['lostPOR'];
    print ("<TD ALIGN=CENTER WIDTH='14%'>Registers</BR>");
    print("<INPUT TYPE=TEXT NAME=registers READONLY VALUE='".$_SESSION['lostRegisters']."' SIZE=3>");
    print("</TD>"); 

    print ("<TD ALIGN=CENTER WIDTH='14%'>");
    print("<a href=javascript:void(0)\n");
    print('onClick="open(\'');
    print('ScripModerate.php?tab=300\'');
    print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
    print(",width=500,height=700");
    print("')\">");


    // This was the old (hard-wired) code ...
//    print("<a href=javascript:void(0)\n");
//    print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/');
//    print('scripware/ScripModerate.php?tab=300\'');
//    print(",'miniwin','toolbar=0,menubar=0,scrollbars=1");
//    print(",width=500,height=700");
//    print("')\">");

    print("Label");
    print("</a>");
    print("</BR>");
    print("<INPUT TYPE=TEXT NAME=label READONLY VALUE='".$_SESSION['lostLabel']."' SIZE=3>");
    print("</TD>"); 








    print ("<TD ALIGN=CENTER WIDTH='14%'>Ancillaries</BR>");
    print("<INPUT TYPE=TEXT NAME=ancillaries READONLY VALUE='".$_SESSION['lostAncillaries']."' SIZE=3>");
    print("</TD>"); 

    print ("<TD ALIGN=CENTER WIDTH='14%'>Endorse</BR>");
    print("<INPUT TYPE=TEXT NAME=endorse READONLY VALUE='".$_SESSION['lostEndorse']."' SIZE=3>");
    print("</TD>"); 

    print ("<TD ALIGN=CENTER WIDTH='14%'>File & PMR</BR>");
    print("<INPUT TYPE=TEXT NAME=fate READONLY VALUE='".$_SESSION['lostFile']."' SIZE=3>");
    print("&nbsp;");
    print("<INPUT TYPE=TEXT NAME=pmr READONLY VALUE='".$_SESSION['lostPMR']."' SIZE=3>");
    print("</TD>"); 

    print ("<TD ALIGN=CENTER WIDTH='14%'><b>Total</b></BR>");
    print("<INPUT TYPE=TEXT NAME=lostTabs READONLY VALUE='".$_SESSION['lostTabs']."' SIZE=3>");
    print("</TD>"); 

    print("</TR></TABLE></div></TD></TR>");    

    // Next row: PENALTIES ...
    print("<tr>");
    print("<td align='center'>Penalties<br>(excluding registers)<br>");
    print("Total <input type='text' name=lostMain READONLY VALUE=".$_SESSION['lostMain']." SIZE=4></td>");
    print("<td align='center' colspan='3'>");
    print("<textarea name=marking rows='5' cols='80'");
    if (!$_SESSION['fullAuthority']) { print(" readonly"); };
    print(">");
    print($_SESSION['textMain']);
    print("</textarea></td>");
    print("</tr>");

    print("<tr>");
    print("<td align='center'>Unnecessary queries ");
    if ($_SESSION['fullAuthority']) {
        print("(allowing for ".$_SESSION['byes']." 'm' byes) -<br>");
    };
    print("included in <b>Total</b> above<br>");
    print("Total <input type='text' name=changes readonly value=".$_SESSION['lostChanges']." size='4'></td>\n");
    print("<td align='center' colspan='3'>\n");
    print("<textarea name=changes rows='5' cols='80'");
    if (!$_SESSION['fullAuthority']) { print(" readonly"); };
    print(">");
    if ($_SESSION['fullAuthority']) { print($_SESSION['textChanges']); };
    print("</textarea></td>\n");
    print("</tr>");

    QA_buttons();

    print("</table></div>\n");
    print("</form>");

};

function EA_MainResults() {
    global $case;
    if ( !isset($_SESSION['textMain']) ) {
        $_SESSION['textMain'] = "";
        $_SESSION['OODNoProd'] = off;
        $_SESSION['Fatal'] = off;
        $_SESSION['NACheck'] = off;

        $db=new dbabstraction();
        $db->connect() or die ($db->getError());

        $db->connect() or die ($db->getError());
        $condition=array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);

      switch ($_SESSION['categoryToMod']) {
            case 1: case 2:
            $table=$db->select("ScripResults",$condition);
            break;

            case 3:
            $table=$db->select("ScripResultsModerated",$condition);
            // Where marks have been moderated, look for the ID of the last moderator (in marks file) ...
            $mtable=$db->select("ScripMarksModerated",$condition);
            break;

            case 4: case 5:
            $table=$db->select("ScripResultsExam",$condition);
            break;

            case 6:
            $table=$db->select("ScripResultsExamModerated",$condition);
            // Where marks have been moderated, look for the ID of the last moderator (in marks file) ...
            $mtable=$db->select("ScripMarksExamModerated",$condition);
            break;

            case 7:
            $table=$db->select("ScripResultsModerated",$condition);
            break;

            case 8:
            $table=$db->select("ScripResultsHome",$condition);
            break;
        };

        foreach ($table as $row) {
            // Use TRIM to remove any extraneous characters e.g. return
            $penalty = trim($row['Penalty']);
            $tab = trim($row['Tab']); 
            $comment = trim($row['Comment']); 
            $gC = trim($row['GenericComment']);

            switch (trim(strtoupper($tab))){
                case strtoupper('Classify'): case strtoupper('Verify'): 
                case strtoupper('Label'): case strtoupper('Ancillaries'): 
                case strtoupper('Endorse'): case strtoupper('File'): 
                case strtoupper('PMR'):

                // GENERIC COMMENT CODE (October 2009): We need to add the generic comment number if moderation (~) is
                // taking place (i.e. if the user is a moderator) (since otherwise we can't write the
                // the generic code number to the relevant output file) but not otherwise ...

                if ($_SESSION['fullAuthority']) {
                    $_SESSION['textMain'] .= $penalty." - ".$tab." ~ ".$gC.": ".$comment."\n";
                } else {
                    $_SESSION['textMain'] .= $penalty." - ".$tab.": ".$comment."\n";
                };

// Originally read
//                if ($penalty=='$_SESSION['theSeriousPenalty']') { $_SESSION['Fatal'] = on; };
                if ($penalty==$_SESSION['theSeriousPenalty']) { $_SESSION['Fatal'] = on; };
            };
        };

        // Look for moderator ID if results have already been moderated 
        // (3 or 6) ...
        if ($_SESSION['categoryToMod']=='3' || $_SESSION['categoryToMod']=='6') {
            foreach ($mtable as $mrow) {
                $_SESSION['lastModID']=$mrow['ModeratorID'];
            };        
        } else {
            unset ($_SESSION['lastModID']);
        };

  //  };
    };


    if ( !isset($_SESSION['textChanges']) ){
        $_SESSION['textChanges'] = "";
        $db->connect() or die ($db->getError());
        $condition=array("StudentID"=>$_SESSION['studentID'], 
        	 "CaseID"=>$_SESSION['caseID']);

        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
        switch ($_SESSION['categoryToMod']) {
            case 1:
            $table=$db->select("ScripResults",$condition);
            $pable=$db->select("ScripProbes",$condition);
            break;

            case 2:
            $table=$db->select("ScripResults",$condition);
            $pable=$db->select("ScripProbes",$condition);
            break;

            case 3:
            $table=$db->select("ScripResultsModerated",$condition);
            $pable=$db->select("ScripProbesModerated",$condition);
            break;

            case 4:
            $table=$db->select("ScripResultsExam",$condition);
            $pable=$db->select("ScripProbesExam",$condition);
            break;

            case 5:
            $table=$db->select("ScripResultsExam",$condition);
            $pable=$db->select("ScripProbesExam",$condition);
            break;

            case 6:
            $table=$db->select("ScripResultsExamModerated",$condition);
            $pable=$db->select("ScripProbesExamModerated",$condition);
            break;

            case 7:
            $table=$db->select("ScripResultsModerated",$condition);
            $pable=$db->select("ScripProbesModerated",$condition);
            break;

            case 8:
            $table=$db->select("ScripResultsHome",$condition);
            $pable=$db->select("ScripProbesHome",$condition);
            break;
        };

        foreach ($pable as $pow) {
            $penalty=trim($pow['Marks']);
            $tab=trim($pow['mM']);
            $comment=trim($pow['Probe']);
// No checking need be made - only changes appear in this table
            $_SESSION['textChanges'] .= $penalty." - ".$tab.": ".$comment."\n";
        };

    };
};

function F_CD() {
    global $case;
    global $_GET;
    global $_POST;
    I_StudentCase();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    unset ($RegisterID, $dateDone);
    // Read the student's entry in correct record table ...
    $condition = array('RecordID'=>$_SESSION['studentrecordID']);
    switch ($_SESSION['categoryToMod']){
        case 8:
        $table=$db->select("ScripStudentRecordHome",$condition);
        break;

        case 1: case 2: case 3: case 7:
        $table=$db->select("ScripStudentRecord",$condition);
        break;

        case 4: case 5: case 6:
        $table=$db->select("ScripStudentRecordExam",$condition);
        break;
    };

    foreach ($table as $row){
        $RegisterID=$row['RegisterID']; 
    };
    // N.B. There is an implicit assumption here that the CD entry applies to the first drug on the script only.

    $condition = array('RegisterID'=>$RegisterID);
    $table = $db->select("ScripRegister", $condition);
    foreach ($table as $row) {
        $CDRegisterID = $row['CDRegisterID'];
    };

    // We need to know the type of form ...
    $condition=array ('FormID'=>$_SESSION['formID']);
    $table=$db->select("ScripForms",$condition);
    foreach ($table as $row) {
        $formtype=$row['FormName2'];
        $originator=$row['FormName1'];
        $formname2 = $row['FormName2']; // Form type
        $formname1 = $row['FormName1']; // Prescriber type
        $group = $row['GroupID'];
    };
    // Is the form in question not a receipt?
    $notAReceipt = strpos(strtolower($formtype), "receipt") === false;

    // Read the MODEL ANSWER for this case ...
    $condition = array ('CaseID'=>$_SESSION['caseID']);
    $table=$db->select("ScripCDRegisterMA", $condition);

    if (count($table)) { // A model answer exists ...
        foreach ($table as $row) {
            $collectorRelation = $row['CollectorRelation'];
            $collectorIDMA = $row['CollectorIDMA'];
            $CDMA_PageID = $row['PageID'];
            if ($notAReceipt) {
                $CDMA_Collector = $row['CollectorName']." ".$row['CollectorRelation']." ".$row['CollectorIDMA'];
                // Full prose ...
                $CDMA_CollectorIDFull = $row['CollectorIDFull'];
                // ... CollectorIDMA contains the keywords
            } else {
                $quantity = $row['AmountSupplied'];
                $CDMA_Collector = "";
                $CDMAFull_Collector = "";
            };
        };

        // What is the name of the model answer page?
        $condition = array('ID'=>$CDMA_PageID);
        $tableMA=$db->select("ScripCDPageTitleAndLabel", $condition);
        foreach ($tableMA as $row) {
            $pagetitle = $row['Brand'];
            $strength = $row['Strength'];  
            $form = $row['Form'];
        };
        if ($strength) { $pagetitle.= " ".$strength; };
        if ($form) { $pagetitle.= " ".$form; };

        // The rest of the MODEL ANSWER has to be BUILT ...
        $condition = array ('CDRegisterID'=>$CDRegisterID);
        $table=$db->select("ScripCDRegister", $condition);
        foreach ($table as $row) {
            $CDMA_Date = $row['DateMA'];
            // If we're looking at the correct page then the opening balance is the correct opening balance ...
            if ($row['PageID'] == $CDMA_PageID) {
                $opening = $row['Opening'];
            };
        };

        // We need some basic information about this case ...
        $condition = array ('Letter'=>$_SESSION['caseID']);
        $table=$db->select("ScripsScrips", $condition);
        foreach ($table as $row) {
            $_SESSION['drugproblemID'][0] = $row['DrugProblemID'];
            $_SESSION['drugproblemID'][1] = $row['DrugProblemID2'];
            $formID = $row['FormID'];
            $patientproblemID = $row['PatientProblemID'];
            $prescriberproblemID = $row['PrescriberProblemID'];
            $miscproblemID = $row['MiscProblemID'];
        };

        $condition = array ('MiscProblemID'=>$miscproblemID);
        $table=$db->select("ScripMiscProblem", $condition);
        foreach ($table as $row) {
            $purpose = $row['Purpose'];
        };

        // Find the quantity. There are three places it might be - search each one (in decreasing order of precedence) stopping when something has been found ...
        // N.B. Quantity should be WITHOUT units here (v.i.)
        if ($notAReceipt) { // ... already read above if a receipt
            $condition = array ('DrugProblemID'=>$_SESSION['drugproblemID'][0]);
            $table=$db->select("ScripDoseCodesMA", $condition);
            foreach ($table as $row) {
                $quantity = $row['LabelQuantity'];
            };
            if (!$quantity){
                $condition = array ('DrugProblemID'=>$_SESSION['drugproblemID'][0]);
                $table=$db->select("ScripDrugProblem", $condition);
                foreach ($table as $row) {
                    $quantity = $row['QuantityCorrect'];
                    if (!$quantity) {
                        $quantity = $row['Quantity'];
                    };
                };
            };
        };

        // Strip quantity of units ...
        // Regular expression [0-9]*\.?[0-9]+ should match an unsigned number ...
        preg_match('{[0-9]*\.?[0-9]+}', $quantity, $m);
        $quantity = $m[0];

        // Field 3: AMOUNT (IN) or Field 8: AMOUNT (OUT); Field 9: BALANCE (i.e. $CDMA_Closing)
        if ($notAReceipt) {
            // Not a receipt - items going out - SUBTRACT
            if ($opening) { $CDMA_Closing = $opening - $quantity; };
            $CDMA_AmountObtained = "";
            $CDMA_AmountSupplied = $quantity;
        } else {
            // A receipt - items coming in - ADD
            if ($opening) { $CDMA_Closing = $opening + $quantity; };
            $CDMA_AmountObtained = $quantity;
            $CDMA_AmountSupplied = "";
        };

        $condition = array ('PrescriberProblemID'=>$prescriberproblemID);
        $table = $db->select ("ScripPrescriberProblem", $condition);
        foreach ($table as $row) {
            $prescriberID = $row['PrescriberID'];
            $authorisingID = $row['AuthorisingID'];
        };

        $condition = array ('PrescriberID'=>$prescriberID);
        $table = $db->select ("ScripPrescriber", $condition);
        foreach ($table as $row) {
            $prescribername = $row['Name'];
            $nhsnumber = $row['NHSNumber'];
            $practiceID = $row['PracticeID'];
            $prescriberPrivateCD = $row['PrivateCD'];
        };

        $condition = array ('PracticeID'=>$practiceID);
        $table = $db->select ("ScripPractice", $condition);
        foreach ($table as $row) {
            $practiceAddress1 = $row['Address1'];
            $practiceAddress2 = $row['Address2'];
            $practiceAddress3 = $row['Address3'];
            $practiceAddress4 = $row['Address4'];
            $practicePostCode = $row['PostCode'];
            $practiceTelNo = $row['TelephoneNumber'];
        };

        // Field 4: NAME AND ADDRESS ...
        // For requisitions and receipts, this is the prescriber ...
        if (strpos(strtolower($formtype), "receipt") !== false || strtolower($formtype) == "requisition" || strtolower($formtype) == "fp10cdf") {
            $name = $prescribername;

            if (strtolower($formtype) == "fp10cdf") {
                // FP10CDFs only have enough space for two lines of address ...
                $address = $practiceAddress1." ".$practiceAddress2;
            } else {
                $address = $practiceAddress1." ".$practiceAddress2." ".$practiceAddress3." ".$practiceAddress4." ".$practicePostCode;
            }; 

            // Vets need the telephone number too ...
            if ($_SESSION['practiceTelNo'] == 13) {
                $address.= " ".$practiceTelNo;
            };

            if (strpos(strtolower($formtype), "receipt") !== false){
                // For a receipt, this is the supplier ...
                $CDMA_Supplier = $name." ".$address;
                $CDMA_Supplied = "";
            } else {
                // For a requisition, this is the supplied ...
                $CDMA_Supplied = $name." ".$address;
                $CDMA_Supplier = "";
            };

        } else { // Otherwise the patient ...
            $condition = array ('PatientProblemID'=>$patientproblemID);
            $table=$db->select("ScripPatientProblem", $condition);
            foreach ($table as $row) {
                $patientID = $row['PatientID'];
            };

            $condition = array ('PatientID'=>$patientID);
            $table=$db->select("ScripPatient", $condition);
            foreach ($table as $row) {
                $patientfname = $row['FirstName'];
                $patientlname = $row['LastName'];
                $address1 = $row['Address1'];
                $address2 = $row['Address2'];
                $address3 = $row['Address3'];
                $address4 = $row['Address4'];
                $postCode = $row['PostCode'];
            };

            $name = $patientfname." ".$patientlname;
            $address = $address1." ".$address2." ".$address3." ".$address4." ".$postCode;
            $CDMA_Supplied = $name." ".$address;
            $CDMA_Supplier = "";
        };

        // Field 5: AUTHORITY depends on form type
        if ($notAReceipt) {
            switch ($group) {
                case 1: case 2: case 10:
                $CDMA_Authority = "Signed order";
                $CDMA_Authority.= " ".$formname1;
//                $CDMA_Authority.= " ".$purpose;

                // Is the form a CD requisition? If so, the model answer for the
                // purpose comes from elsewhere ...
                if ($group == 10) {

                    // Yes - the model answer is given by PurposeCategory from ScripMiscProblem. This gives a number for which the matching
                    // text can be found in ScripPurposeText ... except that if the number is 6 then the purpose is Purpose in ScripMiscProblem.  
                    $condition=array ('MiscProblemID'=>$miscproblemID);
                    $table=$db->select("ScripMiscProblem", $condition);
                    foreach ($table as $row) {
                        $purposeCategory = $row['PurposeCategory'];
                        $purpose = $row['Purpose'];
                        $penalty = $row['PurposeMarks'];
                    };     
                    if ($purposeCategory == 6){
                        // This is "6. Other" - read the "Other" box ...
                        $CDMA_Authority.= " ".$purpose;
                        $CDMA_Authority = trim($CDMA_Authority);
                    } else {
                        // Read the printed text on the form ...
                        $condition=array ('ID'=>$purposeCategory);
                        $table=$db->select("ScripPurposeText", $condition);
                        foreach ($table as $row) {
                            $CDMA_Authority.= " ".$row['PurposeText'];
                        };
                    };
                    $CDMA_Authority.= " ".$prescriberPrivateCD;

                } else {
                    // For non-CD requisitions add purpose from ScripMiscProblem
                    $CDMA_Authority.= " ".$purpose;
                };

                if ($group == '2') {
                    $condition = array("PrescriberID"=>$authorisingID);
                    $table=$db->select("ScripPrescriber",$condition);
                    foreach ($table as $row) {
                        $AOName = $row['Name'];
                        $AOQual = $row['Qual'];
                    };
                    $CDMA_Authority.= " ".$AOName." ".$AOQual;
                };
                break;

                case 3: case 4: case 5: case 6: case 7:
                $CDMA_Authority = $prescribername." ".$nhsnumber." ".$formname2;
                break;

                case 8: // Private CD has a special number (to replace NHS number)
                $CDMA_Authority = $prescribername; 
                $CDMA_Authority.= " ".$prescriberPrivateCD." ".$formname2;
                break;

                case 9: // Prescriber has no number ...
                $CDMA_Authority = $prescribername." ".$formname2;
                break;
            };
        } else {
            $CDMA_Authority = "";
        };

        // Field 6: PERSONCOLLECTING is one of "patient" or "patient's rep" or
        // "healthcare professional" followed by name and address ...
        if ($notAReceipt) {
            if (strtolower($collectorRelation) == strtolower("patient")){
                // Patient ...
                $CDMA_PersonCollecting = "Patient";
            } else if ($collectorRelation == ""){
                // Now have to check the type of form ...
                $CDMA_PersonCollecting = $prescribername." Healthcare professional ";
                switch ($group) {
                    case 1: case 2: case 10: // All requisitions ...
                    // Healthcare professional EITHER name and address OR name and "as supplied" ...
                    // OR name and "see supplied" ...
                    $address = $practiceAddress1." ".$practiceAddress2." ";

                    // For other than FP10CDFs (i.e. 10), add remainder of address ... 
                    if ($_SESSION['group'] != 10) {
                        $address.= $practiceAddress3." ".$practiceAddress4." ".$practicePostCode." ";
                    };
                    $CDMA_PersonCollecting = $CDMA_PersonCollecting." ".$address;
                    $CDMA_PersonCollecting.= " (<i>or</i> as supplied <i>or</i> see supplied <i>instead of address</i>)";
                    break;

                    case 3: case 4: case 8: case 9:
                    // All scripts including TTOs ...
                    // Healthcare professional EITHER name and address OR name and "as supplied" ...
                    $address = $practiceAddress1." ".$practiceAddress2." ".$practiceAddress3." ".
                                            $practiceAddress4." ".$practicePostCode." ";
                    $CDMA_PersonCollecting = $CDMA_PersonCollecting." ".$address;
                    break;
                };
            } else {
                // Patient's representative ... with relationship
                $CDMA_PersonCollecting = "Representative ".$collectorRelation;
            };
        } else {
            $CDMA_PersonCollecting = "";
        };

        // Field 7A: PROOFREQUESTED is always "Yes" in Scripware ...
        if ($notAReceipt) {
            $CDMA_ProofRequested = "Yes";
        } else {
            $CDMA_ProofRequested = "";
        };

        // Field 7B: PROOFPROVIDED depends on CollectorIDMA ...
        if ($notAReceipt) {
            if (strtolower($collectorIDMA) == strtolower("no id")){
                // No proof provided, so user should enter "No" ...
                $CDMA_ProofProvided = "No";
            } else {
                // ... otherwise, user should enter "Yes" ...    
                $CDMA_ProofProvided = "Yes";
            };
        } else {
            $CDMA_ProofProvided = "";
        };

    } else {
        $pagetitle = "[There is no entry for this script]";
    };

    // The code below is sophisticated - read the comments well!    

    $pageNumberOfCorrectPage = 0;
    // There may now (2007) be more than one entry in the CD table for this student-case combination - all have the
    // the same $CDRegisterID. We can characterise them with either the ID field only or the PageID field and the CDRegisterID together ...
    // Remember: $CDRegisterID refers to entries made by THIS student for THIS case and $PageID is the number of the page in the
    // (imaginary) CD register where the entry has been made ...
    if ($CDRegisterID) {
        $condition = array('CDRegisterID'=>$CDRegisterID);
        $table = $db->select("ScripCDRegister", $condition);
        foreach ($table as $row) {
            $pageID = $row['PageID'];
            $ID = $row['ID'];
            // Loop over all the CD register pages chosen by the user ... is the current page in the array pageChosenByUser?
            // N.B. $pageChosenByUser is an array which begins empty so the first comparison always fails; should the second page
            // page be identical to the first, the comparison will succeed ...
            $pageAlreadyNoted = false;
            $thisIsThePage = 0;
            if (count($pageChosenByUser)) {
                for ($ii = 0; $ii < count($pageChosenByUser); $ii++) {
                    if ($pageChosenByUser[$ii] == $pageID) {
                        $pageAlreadyNoted = true;
                        $thisIsThePage = $ii; // i.e. the page that the user has written on (e.g. 806) is e.g. the second page he used
                    };
                };
            };

            // Where we come across a new page, we note it - this section is therefore a list of the pages on which the student has made one or more entries:
            if (!$pageAlreadyNoted) {
                // This pageID (from CD register) is not in the array pageChosenByUser yet; it is news that the student wrote here! Note it now i.e. enter the page ...
                // It goes in as the first element by default
                $nextEntry = 0; // ... this is serial number

                // ... but not if something already in there ...
                if (count($pageChosenByUser)) {
                    // ... put in at end ...
                    $nextEntry = count($pageChosenByUser);
                };
                // Write it to the place we've decided upon ...
                $pageChosenByUser[$nextEntry] = $pageID;

                // ... and is it the correct answer?
                if ($pageID == $CDMA_PageID) {
                    // If so, note it
                    $pageNumberOfCorrectPage = $nextEntry;
                // Example: $pageChosenByUser may be 987, 123, 765, 890 - the student has written on these four pages of the (imaginary) CD register  
                // Example: $pageNumberOfCorrectPage = 2; this means 765 is the correct page (since 987 is 0; 123 is 1; 765 is 2 and 890 is 3)  
                };
            };

            // We now know where in our list this page occurs e.g. the user might so far have used three pages (0 is No. 4, 1 is No. 9 and 2 is No. 11).
            // We have now found another reference to No. 9. We shall therefore be referring to our 1. We want to write our ID to somewhere in the array $UA[1]
            // $ID is just the serial number in the table

//if ($_SESSION['moderatorID'] == 10) {
            if ($pageAlreadyNoted){
                // $UA already has some of these (stored in the array $UA[$thisIsThePage]) ... add this as another ...
                $nextEntryThisPage = count($UA[$thisIsThePage]);
                $UA[$thisIsThePage][$nextEntryThisPage] = $ID;
            } else {
                  // $UA doesn't have any of these - put this one into $UA[$nextEntry] at entry 0 ...
                $UA[$nextEntry][0] = $ID;
            };
//};

//if ($_SESSION['moderatorID'] != 10) {
//            $nextEntryThisPage = count($UA[$thisIsThePage]);
//            $UA[$thisIsThePage][$nextEntryThisPage] = $ID;
//};

if ($_SESSION['moderatorID'] == 10) {
//print("$ID -> $nextEntryThisPage of $thisIsThePage<br>");
//print("Entering $ID into the ".$nextEntryThisPage."-th element of $UA[".$thisIsThePage."]");
};

            // Thus ths first dimension of the array is a serial number applied to UNIQUE pages, the second dimension is the attempt at that page; Example:
            //            $UA[0][0] = 121; $UA[0][1] = 499; $UA[0][2] = 921; $UA[0][3] = 89;  
            //            $UA[1][0] = 746; $UA[1][1] = 930; $UA[0][2] = 215;  
            // means the user has written on two pages (we know this since $UA has two elements). Four things have been written on one page (we know this
            // since $US[0] has four elements) and the details can be found at entries 121, 499, 921 and 89 in ScripCDRegister. These four will be shown
            // as four rows in the table.
            // Three things have been 
            // written on the other page and the details are entries 746, 930 and 215. These will be shown as successive rows in another table. The
            // user will move between tables via hyperlinks from "1" and "2".
        };
    };

if ($_SESSION['moderatorID'] == 10) {
print("<br>");
print_r($UA);
};

    // The correct page is element number $pageNumberOfCorrectPage of array $pageChosenByUser ...


    ZZ_DoAllMarkCalculations();


//    EA_MainResults();
//    FA_CDResults();
//    GA_PORResults();
//    HA_CommsResults();

// Calculate the sub-totals
//    ZH_subtotalsOTC();
//    ZG_subtotalsPOR();

// N.B. Need to loop over CD pages now (2007) 

//    ZF_subtotalsCD();
//    ZD_subtotalsChanges();
//    ZE_subtotalsMain();
//    ZI_subtotalsCombine();

// Where are this student's registers stored for this case?
    $condition=array("StudentID"=>$_SESSION['studentID'],
	"CaseID"=>$_SESSION['caseID']);
        switch ($_SESSION['categoryToMod']) {
            case 1: case 2: case 3: 
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 4: case 5: case 6: 
            $table=$db->select("ScripStudentRecordExam",$condition);
            break;

            case 7:
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 8:
            $table=$db->select("ScripStudentRecordHome",$condition);
            break;
        };
    unset ($RegisterID, $dateDone);
    foreach ($table as $row) {
        $RegisterID=$row['RegisterID'];
        $dateDone=$row['DateDone'];
    };
    // If no MA, set dateDone to nul ...
    if ($Particulars=="&nbsp;") { $dateDone="&nbsp;"; };

// Where is this student's CD register stored (for this case)? 
    $condition=array("RegisterID"=>$RegisterID);
    $table=$db->select("ScripRegister",$condition);
    foreach ($table as $row) {
        $CDRegisterID=$row['CDRegisterID'];
    };

    // Show top line (name, case and score)
    print("<tr><td valign='top' align=center>\n");

    print("<div class=tablecream>");
    print("<table bgcolor='#EEEEEE' border=1 width=100% align='center'>");

    print("<tr>");
    print("<td align='center'>Student (with initials): ");
    print("<b>".$_SESSION['firstname']." ".$_SESSION['lastname']." - ".$_SESSION['initials']."</b></TD>\n");
    print("<div class=tablecream>");

    print("<td align='center'>Case: <b>".$_SESSION['caseID']."</b>");
    print("  Seat: <b>".$_SESSION['seat']."</b>");
    print("</td>\n");

    print("<td align='center'>Script Score (".$_SESSION['maxscore']."): <b>".$_SESSION['finalScore']."</b></TD>");
    print("</tr>");

    print("</table></div>\n");

    // Show MODEL ANSWER ...
    print("<tr><td valign='top' align='center'>\n");

    print("<div class=tablecream>");
    if ($_SESSION['MorS']!="S") { // Model answer not available to students ...
        print("<table bgcolor='#EEEEEE' border=1 width=100% align='center'>");
        print("<tr>\n");
        print("<td align='center' colspan='9'>");
        print("<b>Model Answer:</b> $pagetitle");
        if ($Particulars!="&nbsp;") {
            print(" $Drug &nbsp;&nbsp;--&nbsp;&nbsp;");
            print("for student with initials: ".$_SESSION['initials']);
        };
        print("</td></tr>\n");

        print("<tr>");
        print("<td align='center' width='10%'>$dateDone</td>\n");
        print("<td align='center' width='10%'>$CDMA_Supplier</td>\n");
        print("<td align='center' width='10%'>$CDMA_AmountObtained</td>\n");
        print("<td align='center' width='10%'>$CDMA_Supplied</td>\n");
        print("<td align='center' width='10%'>$CDMA_Authority</td>\n");
//        print("<td align='center' width='11%'>$CDMA_Pharmacist</td>\n");
//        print("<td align='center' width='11%'>$CDMA_Collector</td>\n");
        print("<td align='center' width='10%'>$CDMA_PersonCollecting</td>\n");
        print("<td align='center' width='10%'>$CDMA_ProofRequested</td>\n");
        print("<td align='center' width='10%'>$CDMA_ProofProvided</td>\n");
        print("<td align='center' width='10%'>$CDMA_AmountSupplied</td>\n");
        print("<td align='center' width='10%'>$CDMA_Closing</td>");
        print("</tr>");
        print("</table></div>\n");
    };

    // READ the entries on the PAGE SELECTED ...
    // e.g. User clicks on "2" has to give second page i.e.
    // $_GET['page'] = 1 (since zero-based: 0 is first page, 1 is second) and then read ScripCDRegister at ID $UA[1][0], $UA[1][1], $UA[1][2] and $UA[1][3] (if the
    // user has had four goes at this page).

    // If no page chosen, default to the correct page (if possible) ... 
    $pageNumber = $_GET['page']; // e.g. 0, 1, 2, ...
    if (is_null($pageNumber) && $pageNumberOfCorrectPage) {
        $pageNumber = $pageNumberOfCorrectPage;
    };

    // Finally, default to the first CD page ...
    if (count($UA) && $pageNumber == "") { $pageNumber = '0'; };

    // Show STUDENT'S ANSWER ...
    // LINKS to all the pages entered by the user 
    print("<br>Choose a page: ");
    if (!count($UA)) { print("[No CD entries made by pharmacist]"); };
    for ($k = 0; $k < count($UA); $k++){
        // Construct a hyperlink where a serial number is the clickable text and the destination is the page ...
        $kp1 = $k + 1; // ... we prefer a system that is NOT zero-based in our display ...
        print("<a href='ScripModerate.php?tab=20&page=$k'>$kp1</a>");
        print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
    };
    print("<br><br>");

    // What is the title of the page that we're about to show?
    $condition = array ('ID'=>$pageChosenByUser[$pageNumber]);
    $tableTitle = $db->select("ScripCDPageTitleAndLabel", $condition);
    foreach ($tableTitle as $row) {
        $UA_PageTitle = $row['Brand'];
        $strengthUA = $row['Strength'];
        $formUA = $row['Form'];
    };
    if ($strengthUA) { $UA_PageTitle.=" ".$strengthUA; };
    if ($formUA) { $UA_PageTitle.=" ".$formUA; };

    // Show student's answers ...
    print("<div class=tablecream>");
    print("<table bgcolor='#EEEEEE' border=1 width=100% align=center>");
    print("<tr>");
    print("<td align='center' colspan='9'>");
    print("<b>Page ");
    if ($UA_PageID) print($pageNumber+1); // A page read?
    print(":</b> ");
    print("$UA_PageTitle&nbsp;");
    // If user's page is the correct page ID (and this is non-zero) ...

    if ($pageNumber != "" && $pageNumber == $pageNumberOfCorrectPage 
                  && $CDMA_PageID) {
        print(" -- <b>Correct page</b>");
    };
    print("</td></tr>\n");


    for ($ik = 0; $ik < count($UA[$pageNumber]); $ik++) {
        $condition = array ('ID'=>$UA[$pageNumber][$ik]);
        $table = $db->select("ScripCDRegister", $condition);
        foreach ($table as $row) {
            $UA_PageID = $row['PageID'];
            $UA_Date = $row['Date'];
            $UA_Supplier = $row['Supplier'];
            $UA_AmountObtained = $row['AmountObtained'];
            $UA_Supplied = $row['Supplied'];
            $UA_Authority = $row['Authority'];
//            $UA_Pharmacist = $row['Pharmacist'];
//            $UA_Collector = $row['Collector'];
            $UA_PersonCollecting = $row['PersonCollecting'];
            $UA_ProofRequested = $row['ProofRequested'];
            $UA_ProofProvided = $row['ProofProvided'];
            $UA_AmountSupplied = $row['AmountSupplied'];
            $UA_Closing = $row['Closing'];
        };

        print("<tr>");
        print("<td align='center' width='10%'>$UA_Date&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_Supplier&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_AmountObtained&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_Supplied&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_Authority&nbsp;</td>\n");
//        print("<td align='center' width='11%'>$UA_Pharmacist&nbsp;</td>\n");
//        print("<td align='center' width='11%'>$UA_Collector&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_PersonCollecting&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_ProofRequested&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_ProofProvided&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_AmountSupplied&nbsp;</td>\n");
        print("<td align='center' width='10%'>$UA_Closing&nbsp;</td>\n");
        print("</tr>\n");
    };

    // Penalties CD: Show MARKING COMMENTS ...
    print("<form action=ScripModerate.php method='post'>\n");
    print("<input type='hidden' name=tab value=22>");

    // Start the TR ...
    print("<tr><td align='center'>Penalties<br>Total ");
    print("<input type='text' name=totalpenalty VALUE=".$_SESSION['lostCD']." size='4'></td>");
    print("<td align='center' colspan='8'>");
    print("<textarea name=markingCD rows=5 cols=90");
    if (!$_SESSION['fullAuthority']) print(" readonly");
    print(">".$_SESSION['textCD']."</textarea></td>");
    print("</tr>");
    // ... end the TR

    print("</table></div>");

    print("<div class=tablecream>");
    print("<table>");
    QA_buttons();
    print("</table></div>");

    print("</table></div>\n");
    print("</form>");

};

function FA_CDResults() {
    global $case;

// Construct textCD if it doesn't already exist
    if ( !isset($_SESSION['textCD']) ){
        $_SESSION['textCD'] = "";
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
        $condition=array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);
        switch ($_SESSION['categoryToMod']) {
            case 1: case 2:
            $table=$db->select("ScripResults",$condition);
            break;

            case 3:
            $table=$db->select("ScripResultsModerated",$condition);
            break;

            case 4: case 5:
            $table=$db->select("ScripResultsExam",$condition);
            break;

            case 6:
            $table=$db->select("ScripResultsExamModerated",$condition);
            break;

            case 7:
            $table=$db->select("ScripResultsModerated",$condition);
            break;

            case 8:
            $table=$db->select("ScripResultsHome",$condition);
            break;
        };
        foreach ($table as $row) {
            $penalty=trim($row['Penalty']);
            $tab=trim($row['Tab']);
            $comment=trim($row['Comment']);
            $gC = trim($row['GenericComment']);
            switch (trim(strtoupper($tab))){
                case strtoupper('CD'):

                // Show the comment with a ~ if modereation is taking place (~); without otherwise ...
                if ($_SESSION['fullAuthority']) {
                    $_SESSION['textCD'] .= $penalty." - ".$tab." ~ ".$gC.": ".$comment."\n";
                } else {
                    $_SESSION['textCD'] .= $penalty." - ".$tab.": ".$comment."\n";
                };

            };
        };
    };
};

function G_POR() {
    global $case;
    I_StudentCase();

// Read the model answer (mainly single-letter codes)
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition=array("CaseID"=>$_SESSION['caseID']);
    $table=$db->select("ScripPORMA",$condition);
    unset($col2,$col3,$col4a,$col4b,$col5a,$col5b,$col6);
    foreach ($table as $row) {
        $col2=$row['col2'];
        $col3=$row['col3'];
        $col4a=$row['col4a'];
        $col4b=$row['col4b'];
        $col5a=$row['col5a'];
        $col5b=$row['col5b'];
        $col6=$row['col6'];
    };

// Read the things that the model answer needs

// Find name and address of PRESCRIBER or REQUESTER (R)
    // Set $R to nul string to prevent carry-over from previous results ...
    $R="";
    if ($col2) {
        $condition=array("Letter"=>$_SESSION['caseID']);
        $table=$db->select("ScripsScrips",$condition);
        foreach ($table as $row) {
            $PrescriberProblemID=$row['PrescriberProblemID'];
        };

        $condition=array("PrescriberProblemID"=>$PrescriberProblemID);
        $table=$db->select("ScripPrescriberProblem",$condition);
        foreach ($table as $row) {
            $PrescriberID=$row['PrescriberID'];
            $Address1=$row['UKAddress1'];
            if ($Address1 == "0") {$Address1 = "";}
            $Address2=$row['UKAddress2'];
            if ($Address2 == "0") {$Address2 = "";}
            $Address3=$row['UKAddress3'];
            if ($Address3 == "0") {$Address3 = "";}
            $Address4=$row['UKAddress4'];
            if ($Address4 == "0") {$Address4 = "";}
            $postCode=$row['UKPostCode'];
            if ($postCode == "0") {$postCode = "";}
         };

        $condition=array("PrescriberID"=>$PrescriberID);
        $table=$db->select("ScripPrescriber",$condition);
        foreach ($table as $row) {
            $RName=$row['Name'];
            $PracticeID=$row['PracticeID'];
        };

        if (!$Address1) { // If no specific UK address, use ordinary
            $condition=array("PracticeID"=>$PracticeID);
            $table=$db->select("ScripPractice",$condition);
            foreach ($table as $row) {
                $Address1=$row['Address1'];
                if ($Address1 == "0") {$Address1 = "";}
                $Address2=$row['Address2'];
                if ($Address2 == "0") {$Address2 = "";}
                $Address3=$row['Address3'];
                if ($Address3 == "0") {$Address3 = "";}
                $Address4=$row['Address4'];
                if ($Address4 == "0") {$Address4 = "";}
                $postCode=$row['PostCode'];
                if ($postCode == "0") {$postCode = "";}
            };
        };
        $R=$RName." ".$Address1." ".$Address2." ".$Address3." ".$Address4." ".$postCode;
    } else {
        $R="&nbsp;";
    };
    $p=$R; // Two names have been used for this variable!


// Find name and address of PATIENT (P)
    $P="";
    if ($col2) {
        $condition=array("Letter"=>$_SESSION['caseID']);
        $table=$db->select("ScripsScrips",$condition);
        foreach ($table as $row) {
            $PatientProblemID=$row['PatientProblemID'];
        };

        $condition=array("PatientProblemID"=>$PatientProblemID);
        $table=$db->select("ScripPatientProblem",$condition);
        foreach ($table as $row) {
            $PatientID=$row['PatientID'];
        };

        $condition=array("PatientID"=>$PatientID);
        $table=$db->select("ScripPatient",$condition);
        foreach ($table as $row) {
            $PName=$row['FirstName']." ".$row['LastName'];
            $Address1=$row['Address1'];
            if ($Address1 == "0") {$Address1 = "";}
            $Address2=$row['Address2'];
            if ($Address2 == "0") {$Address2 = "";}
            $Address3=$row['Address3'];
            if ($Address3 == "0") {$Address3 = "";}
            $Address4=$row['Address4'];
            if ($Address4 == "0") {$Address4 = "";}
            $postCode=$row['PostCode'];
            if ($postCode == "0") {$postCode = "";}
        };

        $P=$PName." ".$Address1." ".$Address2." ".$Address3." ".$Address4;
    } else {
        $P="&nbsp;";
    };

// Find the NFSQ of the drug (name, form, strength, quantity)
// FRom 2007 this includes dose
    $N="";
    $N2="";
    if ($col2) {
        $condition=array("Letter"=>$_SESSION['caseID']);
        $table=$db->select("ScripsScrips",$condition);
        foreach ($table as $row) {
            $drugProblemID=$row['DrugProblemID'];
            $drugProblemID2=$row['DrugProblemID2'];
        };

        $condition=array("DrugProblemID"=>$drugProblemID);
        $table=$db->select("ScripDoseCodesMA",$condition);
        foreach ($table as $row) {
            $labelQuantity=$row['LabelQuantity'];
            $labelNSF=$row['LabelNSF'];
        };

        $condition=array("DrugProblemID"=>$drugProblemID);
        $table=$db->select("ScripDrugProblem",$condition);
        foreach ($table as $row) {
            if ($row['DrugMispelling']) {
                $drugName=$row['DrugMispelling'];
            } else {
                $drugName=$row['DrugName'];
            };
            if ($row['StrengthCorrect']) {
                $drugStrength=$row['StrengthCorrect'];
            } else {
                $drugStrength=$row['Strength'];
            };
            if ($row['FormCorrect']) {
                $drugForm=$row['FormCorrect'];
            } else {
                $drugForm=$row['DeliveryForm'];
            };
            if ($row['DoseCorrect']) {
                $drugDose=$row['DoseCorrect'];
            } else {
                $drugDose=$row['Dose'];
            };
            if ($labelQuantity) {
                $drugQuantity=$labelQuantity;
            } elseif ($row['QuantityCorrect']) {
                $drugQuantity=$row['QuantityCorrect'];
            } else {
                $drugQuantity=$row['Quantity'];
            };
        };
        if ($labelNSF) {
            $N = $labelNSF." ".$drugQuantity;
        } else {
            $N = $drugName." ".$drugStrength." ".
                $drugForm." ".$drugDose." ".$drugQuantity;
        };

        if (ZRE_CorrectRetainDrug(2)) {
            $condition=array("DrugProblemID"=>$drugProblemID2);
            $table=$db->select("ScripDoseCodesMA",$condition);
            foreach ($table as $row) {
                $labelQuantity2=$row['LabelQuantity'];
                $labelNSF2=$row['LabelNSF'];
            };

            $condition=array("DrugProblemID"=>$drugProblemID2);
            $table=$db->select("ScripDrugProblem",$condition);
            foreach ($table as $row) {
                if ($row['DrugMispelling']) {
                    $drugName2=$row['DrugMispelling'];
                } else {
                    $drugName2=$row['DrugName'];
                };
                if ($row['StrengthCorrect']) {
                    $drugStrength2=$row['StrengthCorrect'];
                } else {
                    $drugStrength2=$row['Strength'];
                };
                if ($row['FormCorrect']) {
                    $drugForm2=$row['FormCorrect'];
                } else {
                    $drugForm2=$row['DeliveryForm'];
                };
                if ($row['DoseCorrect']) {
                    $drugDose2=$row['DoseCorrect'];
                } else {
                    $drugDose2=$row['Dose'];
                };
                if ($labelQuantity2) {
                    $drugQuantity2=$labelQuantity2;
                } elseif ($row['QuantityCorrect']) {
                    $drugQuantity2=$row['QuantityCorrect'];
                } else {
                    $drugQuantity2=$row['Quantity'];
                };
            };
            if ($labelNSF2) {
                $N2 = $labelNSF2." ".$drugQuantity2;
            } else {
                $N2 = $drugName2." ".$drugStrength2." ".
                    $drugForm2." ".$drugDose2." ".$drugQuantity2;
            };
        }; // end if second drug
    } else {
        $N="&nbsp;";
    };

    $condition=array("FormID"=>$_SESSION['formID']);
    $table=$db->select("ScripForms",$condition);
    foreach ($table as $row) {
        $formname2 = $row['FormName2'];
        $formname1 = $row['FormName1'];
    };
    // Is this an emergency supply?
    if (strpos(strtolower($formname2), "emergency") !== false) {
        // Yes - find the type of prescriber
        $originator = strtolower($formname1);
        $originator = trim(str_replace("request", "", $originator));

        // Construct the model answer for the description ...
        $emergencySupply = "Emergency supply at the request of the ";
        $emergencySupply.= "$originator.";
    };

// Where are this student's registers stored for this case?
    $condition=array("StudentID"=>$_SESSION['studentID'],
	"CaseID"=>$_SESSION['caseID']);
        switch ($_SESSION['categoryToMod']) {
            case 1: case 2: case 3:
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 4: case 5: case 6:
            $table=$db->select("ScripStudentRecordExam",$condition);
            break;

            case 7:
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 8:
            $table=$db->select("ScripStudentRecordHome",$condition);
            break;
        };
    unset($RegisterID, $dateDone);
    foreach ($table as $row) {
        $RegisterID=$row['RegisterID'];
        $dateDone=$row['DateDone'];
    };

// Where is this student's POR register stored (for this case)?
    $condition=array("RegisterID"=>$RegisterID);
    $table=$db->select("ScripRegister",$condition);
    foreach ($table as $row) {
        $PORID=$row['PORID'];
        $PORID2=$row['PORID2'];
    };

// What is in the POR register for this student and case?
// Note: Only SOME of these will have been set
    unset($refNo,$dateDispensed,$patientName,$patientAddress,
      $prescriberName,$prescriberAddress,$drugDetails,
      $prescriptionDate,$price,$receivedDate,
      $natureEmergency,$purpose);

    $condition=array("PORID"=>$PORID);
    $table=$db->select("ScripPOR",$condition);
    foreach ($table as $row) {
        $refNo=$row['RefNo'];
        $refNoMA=$row['RefNoMA'];
        $dateDispensed=$row['DateDispensed']; // Student's entry
        $dateDispensedMA=$row['DateDispensedMA']; // MA
        $patientName=$row['PatientName'];
        $patientAddress=$row['PatientAddress'];
        $prescriberName=$row['PrescriberName'];
        $prescriberAddress=$row['PrescriberAddress'];
        $drugDetails=$row['Drugdetails'];
        $prescriptionDate=$row['PrescriptionDate'];
        $prescriptionDateMA=$row['PrescriptionDateMA'];
        $price=$row['Price'];
        $receivedDate=$row['ReceivedDate'];
        $natureEmergency=$row['NatureEmergency'];
        $purpose=$row['Purpose'];
    // ... and two concatenations
        $sP=$patientName." ".$patientAddress;
        $sp=$prescriberName." ".$prescriberAddress; // Practitioner
        $sR=$sp; // Requester
    };

    ZZ_DoAllMarkCalculations();

//    EA_MainResults();
//    FA_CDResults();
//    GA_PORResults();
//    HA_CommsResults();

// Calculate the sub-totals
//    ZH_subtotalsOTC();
//    ZG_subtotalsPOR();
//    ZF_subtotalsCD();
//    ZD_subtotalsChanges();
//    ZE_subtotalsMain();
//    ZI_subtotalsCombine();

    print("<div class=tablecream>");
    print("<table>");
    print("</table>");
    print("</div>");

    print("<div class=tablecream>");
    print("<table>");
    print("<tr>");
    print("<TD ALIGN=CENTER>Student (with initials): ");
    print("<b>".$_SESSION['firstname']." ".$_SESSION['lastname']." - ".$_SESSION['initials']."</b></TD>\n");
    print("<div class=tablecream>");

    print("<td align='center'>Case: <b>".$_SESSION['caseID']."</b>");
    print("  Seat: <b>".$_SESSION['seat']."</b>");
    print("</td>\n");

    print("<TD ALIGN=CENTER>Script Score (".$_SESSION['maxscore']."): <b>".$_SESSION['finalScore']."</b></TD>");
    print("</tr>");

    print("</table>");
    print("</div>");


    $datePrescriptionMA="Date [Rx]";
    $receivedDate="Date";

// Choose the correct function according to form in use

    $condition = array('FormID'=>$_SESSION['formID']);
    $table = $db->select("ScripForms", $condition);
    foreach ($table as $row) {
        $_SESSION['group'] = $row['GroupID']; // Read the group NOW!
    };

    switch ($_SESSION['group']) {
        case 7: // Emergency
        switch ($_SESSION['formID']) {
            case 1: // Doctor
            GB_POR_Dr($_SESSION['MorS'], $P,$p,$N,$N2,
              $emergencySupply,$dateDone,$refNo,
              $refNoMA, $sP,$sp,$drugDetails,$price,$dateDispensed,
              $prescriptionDate,
              $receivedDate,$datePrescriptionMA); //Emergency Dr
            break;

            case 19: // Patient
            GB_POR_Pat($_SESSION['MorS'], $P,$N,$N2,$col4a,$dateDone,$refNo,
              $refNoMA, $sP,$drugDetails,$natureEmergency,$price,
              $dateDispensed); // Emergency Patient
            break;
        };
        break;

	case 1: case 2: case 10: // Requisition
        GB_POR_Req($_SESSION['MorS'],$N,$N2,$R,$col4a,$dateDone,$refNo,
          $refNoMA, $drugDetails,$sR,$purpose,$price,$dateDispensed);
	// Requisition: Dr, Dentist, Vet, Optician, Midwife, 
	// Chiropodist
        break;

	case 3: case 8: // Private
        GB_POR_Private($_SESSION['MorS'], $P,$R,$N,$N2,$theDate,$sP,$sR,
          $dateDone,$refNo,$refNoMA, $drugDetails,$price,
          $prescriptionDate,$prescriptionDateMA,$dateDispensed);
        break;

        // Should be no POR for other types of form, but students may
        // not realise this ...!
        default: 
        GB_POR_Private($_SESSION['MorS'], $P,$R,$N,$N2,$theDate,$sP,$sR,
          $dateDone,$refNo,$refNoMA, $drugDetails,$price,
          $prescriptionDate,$prescriptionDateMA,$dateDispensed);
    }; 

    print("<form action=ScripModerate.php method = post>\n");
    print("<input type=hidden name=tab value=32>");
    print("<tr><td align='center'>Penalties<br>Total ");
    print("<input type=text name=totalpenalty value=".$_SESSION['lostPOR']." size=4></td>");
    print("<td align='center' colspan='3'>");
    print("<textarea name=markingPOR rows=5 cols=80");
    if (!$_SESSION['fullAuthority']) { print(" readonly"); };
    print(">".$_SESSION['textPOR']."</textarea></td>");
    print("</tr>");

    print("<div class=tablecream>");
    print("<table>");
    QA_buttons();
    print("</table></div>");

    print("</table></div>\n");
    print("</form>");
};

function GA_PORResults(){
    global $case;
// Construct textPOR if not aleady available
    if ( !isset($_SESSION['textPOR']) ){
        $_SESSION['textPOR'] = "";
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
        $condition = array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);
        switch ($_SESSION['categoryToMod']) {
            case 1: case 2:
            $table=$db->select("ScripResults",$condition);
            break;

            case 3:
            $table=$db->select("ScripResultsModerated",$condition);
            break;

            case 4: case 5:
            $table=$db->select("ScripResultsExam",$condition);
            break;

            case 6:
            $table=$db->select("ScripResultsExamModerated",$condition);
            break;

            case 7:
            $table=$db->select("ScripResultsModerated",$condition);
            break;

            case 8:
            $table=$db->select("ScripResultsHome",$condition);
            break;
        };
        foreach ($table as $row) {
            $penalty=trim($row['Penalty']);
            $tab=trim($row['Tab']);
            $comment=trim($row['Comment']);
            $gC = trim($row['GenericComment']);

            switch (trim(strtoupper($tab))){
               // Only show ~ if moderation {~} is taking place ...
               case strtoupper('POR'):
                if ($_SESSION['fullAuthority']) {
                   $_SESSION['textPOR'] .= $penalty." - ".$tab." ~ ".$gC.": ".$comment."\n";
                } else {
                   $_SESSION['textPOR'] .= $penalty." - ".$tab.": ".$comment."\n";
                };
            };
        };
    };
};

function GB_POR_Private($ModorStu,$P,$R,$N,$N2,$theDate,$sP,$sR,
       $dateDone,$refNo,$refNoMA, $drugDetails,$price,
       $prescriptionDate,$prescriptionDateMA,$dateDispensed) {

    print("<div class=tablecream>");
    print("<table>");
    print("</table>");
    print("</div>");

    // If there is no MA, $P will have been set to &nbsp;
    //  - use this as a criterion for setting the other items
    // to &nbsp; too. (This will pad the table out correctly.)
    if ($P=="&nbsp;") {
        $refNoMA="&nbsp;";
        $dateDone="&nbsp;";
        $prescriptionDateMA="&nbsp;";
        $priceMA="&nbsp;";
    } else {
        $priceMA="Price";
    };

    if ($ModorStu!="S") {
// Show the model answer (in correct format)
        print("<div class=tablecream>");
        print("<table width='90%' bgcolor='#EEEEEE' ");
        print("align='center' border='1'>");
        print("<tr>");
        print("<td width='10%'>$refNoMA</td>");
        print("<td width='20%' rowspan='2'>$P</td>");
        print("<td width='20%' rowspan='2'>$R</td>");
        print("<td width='25%'>$N");
        if ($N2) { print("<br>$N2"); };
        print("</td>");
        print("<td width='10%' rowspan='2'>$priceMA</td>");
        print("</tr>");

        print("<tr>");
        print("<td>$dateDone</td>"); // Date exercise was done
        print("<td>$prescriptionDateMA</td>");
        print("</tr>");
        print("</table></div>");
    };

    print("&nbsp;");

    // Pad out the cells if contents are nul ...
    if (!$refNo) { $refNo="&nbsp;"; };
    if (!$sP) { $sP="&nbsp;"; };
    if (!$sR) { $sR="&nbsp;"; };
    if (!$drugDetails) { $drugDetails="&nbsp;"; };
    if (!$price) { $price="&nbsp;"; };
    if (!$dateDispensed) { $dateDispensed="&nbsp;"; };
    if (!$prescriptionDate) { $prescriptionDate="&nbsp;"; };

// Show the student's answer (in correct format)
    print("<div class=tablecream>");
    print("<table width='90%' bgcolor='#EEEEEE' ");
    print("align='center' border='1'>");
    print("<tr>");
    print("<td width='10%'>");
    print(trim(str_replace("|", "<br>",$refNo)));
    print("</td>");
    print("<td width='20%' rowspan='2'>$sP</td>");
    print("<td width='20%' rowspan='2'>$sR</td>");
    print("<td width='25%'>");
    print(trim(str_replace("|", "<br>",$drugDetails)));
    print("</td>");
    print("<td width='10%' rowspan='2'>$price</td>");
    print("</tr>");

    print("<tr>");
    print("<td>$dateDispensed</td>");
    print("<td>$prescriptionDate</td>");
    print("</tr>");
};

function GB_POR_Dr($ModorStu, $P,$p,$N,$N2,
             $emergencySupply,$dateDone,$refNo,
             $refNoMA,$sP,$sp,$drugDetails,$price,$dateDispensed,
             $prescriptionDate,
             $receivedDate,$datePrescriptionMA) {
    print("<div class=tablecream>");
    print("<table>");
    print("</table>");
    print("</div>");

// Show the model answer (in correct format)
    if ($ModorStu!="S") {
        print("<div class=tablecream>");
        print("<table width='700' bgcolor='#EEEEEE' ");
        print("align='center' border='1'>");
        print("<tr>");
        print("<td width='15%'>$refNoMA</td>");
        print("<td width='20%' rowspan='2'>$P</td>");
        print("<td width='20%' rowspan='2'>$p</td>");
        print("<td width='25%' rowspan='2'>$emergencySupply<br>$N");
        if ($N2) { print("<br>$N2"); };
        print("</td>");
        print("<td width='10%'>$datePrescriptionMA &nbsp;</td>");
        print("<td width='10%' rowspan='2'>Price</td>");
        print("</tr>");
        print("<tr>");
        print("<td>$dateDone</td>");
        print("<td>Date [rec&#39d]</td>");
        print("</tr></table></div>");
    };

    print("&nbsp;");

// Show the student's answer (in correct format)
    print("<div class=tablecream>");
    print("<table width='700' bgcolor='#EEEEEE' ");
    print("align='center' border='1'>");
    print("<tr>");
    print("<td width='15%'>");
    print(trim(str_replace("|", "<br>",$refNo)));
    print("</td>");
    print("<td rowspan='2' width='20%'>$sP</td>");
    print("<td rowspan='2' width='20%'>$sp</td>");
    print("<td rowspan='2' width='20%'>");
    print(trim(str_replace("|", "<br>",$drugDetails)));
    print("</td>");
    print("<td width='10%'>$prescriptionDate</td>");
    print("<td rowspan='2' width='10%'>$price &nbsp;</td>");
    print("</tr>");

    print("<tr>");
    print("<td>$dateDispensed</td>");
    print("<td>$receivedDate &nbsp;</td>");
    print("</tr>");
};

function GB_POR_Pat($ModorStu, $P,$N,$N2,$col4a,$dateDone,$refNo,
               $refNoMA,$sP,$drugDetails,$natureEmergency,$price,
               $dateDispensed) {
    print("<div class=tablecream>");
    print("<table>");
    print("</table>");
    print("</div>");

    if ($ModorStu!="S") {
// Show the model answer (in correct format)
        print("<div class=tablecream>");
        print("<table width='700' bgcolor='#EEEEEE' ");
        print("align='center' border='1'>");
        print("<tr>");
        print("<td width='15%'>$refNoMA</td>");
        print("<td width='20%' rowspan='2'>$P</td>");
        print("<td width='25%'>$N");
        if ($N2) { print("<br>$N2"); };
        print("</td>");
        print("<td width='20%' rowspan='2'>$col4a</td>");
        print("<td width='10%' rowspan='2'>Price</td>");
        print("</tr>");
        print("<tr>");
        print("<td>$dateDone</td>");
        print("</tr></table></div>");
    };

    print("&nbsp;");

// Show the student's answer (in correct format)
    print("<div class=tablecream>");
    print("<table width='700' bgcolor='#EEEEEE' ");
    print("align='center' border='1'>");
    print("<tr>");
    print("<td width='15%'>");
    print(trim(str_replace("|", "<br>",$refNo)));
    print("</td>");
    print("<td rowspan='2' width='20%'>$sP</td>");
    print("<td rowspan='2' width='20%'>");
    print(trim(str_replace("|", "<br>",$drugDetails)));
    print("</td>");
    print("<td rowspan='2' width='20%'>$natureEmergency</td>");
    print("<td rowspan='2' width='10%'>$price</td>");
    print("</tr>");
    print("<tr>");
    print("<td>$dateDispensed</td>");
    print("</tr>");
};

function GB_POR_Req($ModorStu, $N,$N2,$R,$col4a,$dateDone,$refNo,
        $refNoMA,$drugDetails,$sR,$purpose,$price,$dateDispensed) {

    print("<div class=tablecream><table>");
    print("</table></div>");


    if ($ModorStu!="S") {
// Show the model answer (in correct format)
        print("<div class=tablecream>");
        print("<table width='100%' bgcolor='#EEEEEE' ");
        print("align='center' border='1'>");
        print("<tr>");
        print("<td width='15%'>$refNoMA</td>");
        print("<td width='20%'>$N");
        if ($N2) { print("<br>$N2"); };
        print("</td>");
        print("<td width='20%' rowspan='2'>$R</td>");
        print("<td width='25%' rowspan='2'>$col4a</td>");
        print("<td width='10%' rowspan='2'>Price</td>");
        print("</tr>");
        print("<tr>");
        print("<td>$dateDone</td>");
        print("</tr>");
        print("</table></div>");
    };

    print("&nbsp;");

// Show the student's answer (in correct format)
    print("<div class=tablecream>");
    print("<table width='100%' bgcolor='#EEEEEE' align='center' border='1'>");
    print("<tr>");

    print("<td width='15%'>");
    print(trim(str_replace("|", "<br>",$refNo)));
    print("</td>");

    print("<td rowspan='2' width='20%'>");
    print(trim(str_replace("|", "<br>",$drugDetails)));
    print("</td>");
    print("<td rowspan='2' width='20%'>$sR</td>");
    print("<td rowspan='2' width='25%'>$purpose</td>");
    print("<td rowspan='2' width='10%'>$price</td>");
    print("</tr>");
    print("<tr>");
    print("<td>$dateDispensed</td>");
    print("</tr>");
};

function H_Comms() {
    global $case;
    I_StudentCase();

    ZZ_DoAllMarkCalculations();

    // Find type of mark sheet ...
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition=array('Letter'=>$_SESSION['caseID']);
    $table=$db->select("ScripsScrips",$condition);
    foreach ($table as $row) {
        $counsel=$row['Counsel'];
    };

    // Top line - name, initials, case
    print("<tr><td valign='top' align='center'>\n");
    print("<form action='ScripModerate.php' method='post'>\n");
    print("<input type='hidden' name='tab' value='42'>");

    print("<div class=tablecream>");

    print("<table bgcolor='#eeeeee' width='100%' align='center'>");
    print("<tr>");
    print("<td align='center' width='40%'>Student (with initials): ");
    print("<b>".$_SESSION['firstname']." ".$_SESSION['lastname']." - ".$_SESSION['initials']."</b>");
    print("</td>\n");

    print("<td align='center' width='20%'>Case: <b>".$_SESSION['caseID']."</b>");
    print("  Seat: <b>".$_SESSION['seat']."</b></td>\n");

    print("<td align='center' width='20%'>Marksheet: ");
    print("<b>$counsel</b></td>\n");

    print("<td align='center' width='20%'>Item Score ");
    print("<input type='text' name='caseScore' value='".$_SESSION['finalScore']."' readonly size='3'>");
    print("</td>");
    print("</tr>\n");

//    print("<tr><td colspan='4'><hr></td></tr>");

    // The code will now set up the boxes (row by row):

    // Row for PRODUCT penalties ...
    print("<tr><td colspan='4'><table>");

    // OUT OF DATE (1) ...
    print("<td align='center' width='15%'>");
    print("Out of Date<br>");
    print("1 <input type='checkbox' name='OTCOODbox' ");
    // Disabled for (a) Those without full authority (e.g. students) and (b) Marksheet 3
//    if (!$_SESSION['fullAuthority'] || $counsel=='3') { print("disabled "); };
    // Disabled for those without full authority (e.g. students)
    if (!$_SESSION['fullAuthority']) { print("disabled "); };
    if ($_SESSION['OTCOODbox']==on) {print ("checked ");}
    print(">");
    print("<input type=text name='OTCOOD' value='".$_SESSION['lostOTCOOD']."' readonly size='3'>");
    print("<br>");

    // OUT OF DATE (2) ...
    print("2 <INPUT TYPE='CHECKBOX' NAME='OTCOODbox2' ");
    // Disabled for (a) Those without full authority (e.g. students), (b) Marksheet 3 and (c) Only one drug
    if (!$_SESSION['fullAuthority'] || $counsel=='3' || !ZRE_CorrectRetainDrug(2)){ print("DISABLED "); };
    if ($_SESSION['OTCOODbox2']==on) {print ("CHECKED ");}
    print(">");
    print("<input type='text' name='OTCOOD2' value='".$_SESSION['lostOTCOOD2']."' READONLY size='3'>");
    print("</TD>"); 

    // NO PRODUCT (1) ...
    print("<td align='center' width='10%'>");
    print("No Product Dispensed<br>");
    print("1 <input type='checkbox' name='OTCNegative' ");
    // Disabled for (a) Those without full authority (e.g. students) and (b) Marksheet 3
//    if (!$_SESSION['fullAuthority'] || $counsel=='3') { print("disabled "); };
    if (!$_SESSION['fullAuthority']) { print("disabled "); };
    if ($_SESSION['OTCNegative']==on) {print ("checked ");}
    print(">");
    print("<input type=text name='OTCNeg' value='".$_SESSION['lostOTCNegative']."' readonly size='3'>");
    print("<br>");

    // NO PRODUCT (2) ...
    print("2 <INPUT TYPE='CHECKBOX' NAME='OTCNegative2' ");
    // Disabled for (a) Those without full authority (e.g. students), (b) Marksheet 3 and (c) Only one drug
//    if (!$_SESSION['fullAuthority'] || $counsel=='3' || !ZRE_CorrectRetainDrug(2)){ print("DISABLED "); };
    if (!$_SESSION['fullAuthority'] || !ZRE_CorrectRetainDrug(2)){ print("DISABLED "); };
    if ($_SESSION['OTCNegative2']==on) {print ("CHECKED ");}
    print(">");
    print("<INPUT TYPE=TEXT NAME=OTCNeg2 VALUE = '".$_SESSION['lostOTCNegative2']."' READONLY SIZE=3>");
    print("</TD>"); 

    // WRONG QUANTITY LEGAL (1) ...
    print("<td align='center' width='10%'>");
    print("Wrong Quantity Legal<br>");
    print("1 <input type='checkbox' name='OTCWrongQtyLegal' ");
    // Disabled for (a) Those without full authority (e.g. students) and (b) Marksheet 3
    if (!$_SESSION['fullAuthority'] || $counsel=='3') { print("disabled "); };
    if ($_SESSION['OTCWrongQtyLegal']==on) {print ("checked ");}
    print(">");
    print("<input type=text name='OTCWQL' value='".$_SESSION['lostOTCWrongQtyLegal']."' readonly size='3'>");
    print("<br>");

    // WRONG QUANTITY LEGAL (2) ...
    print("2 <input type='CHECKBOX' name='OTCWrongQtyLegal2' ");
    // Disabled for (a) Those without full authority (e.g. students), (b) Marksheet 3 and (c) Only one drug
    if (!$_SESSION['fullAuthority'] || $counsel=='3' || !ZRE_CorrectRetainDrug(2)){ print("DISABLED "); };
    if ($_SESSION['OTCWrongQtyLegal2']==on) {print ("CHECKED ");}
    print(">");
    print("<input type='text' name='OTCWQL2' VALUE='".$_SESSION['lostOTCWrongQtyLegal2']."' READONLY size='3'>");
    print("</td>"); 

    // SERIOUS ERROR ...
    // If there is an error, set the penalty to $_SESSION['theSeriousPenalty']; if not, zero ...
    // NOTE: Serious error type is an array

    // If there are no serious errors then the array seriousErrorType will be set to one element with value
    // 0 when read fom the database; if so, need to change it to a zero-element array. Do this very formally ...
    // [e.g. in a ten-element array, need to loop from 9 down to 0]
    for ($k = count($_SESSION['seriousErrorType']); $k > 0; $k--){
        $km1 = $k - 1;
        if ($_SESSION['seriousErrorType'][$km1] == "" || $_SESSION['seriousErrorType'][$km1] == 0){
            array_splice($_SESSION['seriousErrorType'], $km1, 1);
        }; // ... remove 1 element starting from element being considered (which means this element only)
    };

    if (count($_SESSION['seriousErrorType'])) {
        $_SESSION['lostOTCFatal']=$_SESSION['theSeriousPenalty'];
    } else {
        $_SESSION['lostOTCFatal']=0;
    };

    print("<td width='15%'>");
    // Title ...
    print("<center>Serious Error in Product");

    // Serious error is different to the others; in the other, the checkbox controls the number of marks in the text box; in serious error, the drop-down box
    // controls not only the text but the tick box ... 
    // SERIOUS ERROR: Checkbox ...
    print("<br>");

    // SERIOUS ERROR: Text box for penalty [marks] ...
    print("<input type='text' name='OTCFatalText' value='".$_SESSION['lostOTCFatal']."' READONLY size='3'></center>");
    print("<br>");

    // SERIOUS ERROR: Drop-down box [text of problem] ...
    $table=$db->select("ScripSeriousErrorTypes");

    // Re-arrange the table into numerical order according to the "Order" field ...
    foreach ($table as $row){
        $guide[] = $row['Order'];
    };
    array_multisort($guide, SORT_ASC, SORT_NUMERIC, $table);

    // Now display the checkboxes ...
    foreach ($table as $row){
        $id = $row['ID'];
        $text = $row['Text'];

        print("<input type='checkbox' name='seriouserrortype[]' value='$id'");

        // Box is disabled for students; also for $counsel = 3 ...
//        if (!$_SESSION['fullAuthority'] || $counsel=='3') { print(" disabled='disabled'"); };
        if (!$_SESSION['fullAuthority']) { print(" disabled='disabled'"); };

        // Is this number error one that has already been selected? If so, show a tick (by adding an extra parameter to the input box):
        // Loop over the array $_SESSION['seriousErrorType']; does the array contain the ID $id?
        for ($k = 0; $k < count($_SESSION['seriousErrorType']); $k++){
            if ($id == $_SESSION['seriousErrorType'][$k]) {
                print(" checked='checked'");
            };
        };

        // Close the input box ...
        print(">$text<br>\n");
    };
    // The old select box was originally a dop-down box; it was once turned to a multiple box but this was thought unsuitable ...
//    if (!$_SESSION['multiple']) { print("<select name='seriouserrortype'"); };
//    if ($_SESSION['multiple']) { print("<select name='seriouserrortype[]'"); };
//    if (!($_SESSION['MorS']=="M" || $_SESSION['MorS']=="A")) { print(" disabled='disabled'"); };
//    if ($_SESSION['multiple']) { print(" multiple='multiple'"); };
//    print(">");
//    print("<option value='0'>None</option>");
//    foreach ($table as $row) {
//        $id = $row['ID'];
//        $text = $row['Text'];
//        print("<option value='$id'");
//        if (!$_SESSION['multiple']) { if ($id == $_SESSION['seriousErrorType']) { print(" selected"); }; };

//        if ($_SESSION['multiple']) {  
//            for ($k = 0; $k < count($_SESSION['seriousErrorType']); $k++){
//                if ($id == $_SESSION['seriousErrorType'][$k]) { print(" selected"); };
//            };
//        };
//        print(">$text</option>");
//    };
//    print("</select>");
    print("</td>");

    // FAILED NAME AND ADDRESS ...
    print("<td align='center' width='15%'>");
    print("Failed N/A");
    print("<input type='checkbox' name='failedNA' ");
    if (!$_SESSION['fullAuthority'] || $counsel=='3'){ print("disabled "); }
    if ($_SESSION['failedNA']==on) { print ("checked"); }
    print("><br>");
    print("<input type='text' name='scoreNAFailed' value='".$_SESSION['lostNA']."' readonly size='3'>");
    print("</td>");

    // AAI (added late 2008) - actually need TWO of these (added late 2009) ...
    print("<td align='center' width='15%'>AAI<br>");
    print("1 <input type='checkbox' name='AAI' ");
    if (!$_SESSION['fullAuthority'] || $counsel=='3'){print("disabled ");}
    if ($_SESSION['AAI']==on) {print ("checked");}
    print(">");
    print("<input type='text' name='scoreAAI' value='".$_SESSION['lostAAI']."' readonly size='3'>");
    print("<br>");
    print("2 <input type='checkbox' name='AAI2' ");
    if (!$_SESSION['fullAuthority'] || $counsel=='3' || !ZRE_CorrectRetainDrug(2)){ print("disabled "); };
    if ($_SESSION['AAI2']==on) { print ("checked"); };
    print(">");
    print("<input type='text' name='scoreAAI2' value='".$_SESSION['lostAAI2']."' readonly size='3'>");
    print("</td>");

    // CD SIGNATURE CHECK ...
    print("<td align='center' width='15%'>CD Sig. Check");
    print("<input type='checkbox' name='OTCCDSigbox' ");
    if (!$_SESSION['fullAuthority'] || $counsel=='3'){print("disabled ");}
    if ($_SESSION['OTCCDSig']==on) {print ("checked");}
    print("><br>");
    print("<input type='text' name='OTCCDSigText' value='".$_SESSION['lostOTCCDSig']."' readonly size='3'>");
    print("</td>");

    // The total of all the penalties ...
    print("<td align='center' width='10%'><b>Product Penalties</b><br>");
    print("<input type='text' name='OTCTotal' value=".$_SESSION['OTCTotal']." readonly size='3'>");
    print("</td>");
    print("</tr>");

    // Next row: OTC ...
    print("<tr>");
    print("<td colspan='7'>");

    print("<table>");

    // OCTOBER 2013: All the cells in this row have been REMmed out (and replaced by &nbnsp;) except for the last. This
    // is to be connected to the (new) Competent: Yes/No radio button option
    print("<tr>");
    print("<td align='center' width='25%'>");
print("&nbsp;");
//    print("Correct Product<br><input type='TEXT' NAME='correctP' VALUE = '".$_SESSION['correctP']."' ");
    // Readonly if (a) Not have full authority or (b) Marksheet != 3
//    if (!$_SESSION['fullAuthority'] || $counsel!='3') { print("readonly "); };
//    print ("size='3'>");
    print("</td>");

    print("<td align='center' width='16%'>");
print("&nbsp;");
//    print("Items of Information<br><input type='text' name='itemsOfInfo' value='".$_SESSION['itemsOfInfo']."' ");
    // Readonly if (a) Not have full authority or (b) Marksheet != 3
//    if (!$_SESSION['fullAuthority'] || $counsel!='3') { print("readonly "); };
//    print ("size='3'>");
    print("</td>");

    print("<td align='center' width='16%'>");
print("&nbsp;");
//    print("OTC Question 1<br><input type='text' name=OTCQ1 value = '".$_SESSION['OTCQ1']."' ");
    // Readonly if (a) Not have full authority or (b) Marksheet != 3
//    if (!$_SESSION['fullAuthority'] || $counsel!='3') { print("readonly "); };
//    print ("size='3'>");
    print("</td>");

    print("<td align='center' width='16%'>");
print("&nbsp;");
//    print("OTC Question 2<br><input type='text' name=OTCQ2 value = '".$_SESSION['OTCQ2']."' ");
    // Readonly if (a) Not have full authority or (b) Marksheet != 3
//    if (!$_SESSION['fullAuthority'] || $counsel!='3') { print("readonly "); };
//    print ("size='3'>");
    print("</td>");

    // This was the original form ...
//    print("<td align='center' width='25%'><b>OTC Score</b><br><input type=text name='OTCScore' value = ".$_SESSION['OTCScore']." readonly size='3'></td>");


    // This is the NEW version (October 2013) ...
//    print("<td align='center' width='25%'><b>Competent</b><br><input type=text name='ComptenceScore' value = ".$_SESSION['competent']." readonly size='3'></td>");
    print("<td align='center' width='25%'><b>Competent (10)</b><br><input type='text' name='CompetenceScore' value = '".$_SESSION['competent']."' readonly='readonly' size='3'></td>");

    print("</tr></table></td></tr>");

    // Next row - COMMUNICATION:
    print("<tr><td colspan='7'><table>");
    print("<tr>");

    // Communication cell: INITIATING THE CONSULTATION
    print("<td align='center' width='10%'>Initiating the consultation (".$_SESSION['maxInitiate'].")<br>");
    print("<input type='number' name='initiate' value='".$_SESSION['initiate']."'  min='0' max='" . $_SESSION['maxInitiate'] . "'");
    if (!$_SESSION['fullAuthority']) {print("readonly ");}
    print("size=3>");
    print("</td>");

    // Communication cell: GATHERING THE INFORMATION
    print("<td align='center' width='10%'>Gathering the information (".$_SESSION['maxGather'].")<br>");
    print("<input type='text' name='gather' value='".$_SESSION['gather']."' ");
    if (!$_SESSION['fullAuthority']) {print("readonly ");}
    print ("size='3'>");  
    print("</td>");

    // Communication cell: EXPLANATION AND PLANNING
    print("<TD ALIGN='CENTER' WIDTH='10%'>Explanation and planning (".$_SESSION['maxExplain'].")<br>");
//    print("<INPUT TYPE=TEXT NAME='explain' VALUE='".$_SESSION['explain']."' ");
    print("<input type='text' name='explain' value='".$_SESSION['explain']."' ");
    if (!$_SESSION['fullAuthority']) {print("readonly ");}
    print("SIZE=3>");
    print("</TD>");

    // Communication cell: CONCLUDING THE CONSULTATION
    print("<td align='center' width='10%'>Concluding the consultation (".$_SESSION['maxConclude'].")<br>");
    print("<input type='text' name='conclude' value='".$_SESSION['conclude']."' ");
    if (!$_SESSION['fullAuthority']) { print("readonly "); };
    print("size='3'>");
    print("</td>");

    // Communication cell: ANSWERING THE PATIENT'S QUESTIONS
    print("<td align='center' width='10%'>Answering the patient's questions (".$_SESSION['maxAnswer'].")<br>");
    print("<input type='text' name='answer' value='".$_SESSION['answer']."' ");
    if (!$_SESSION['fullAuthority']) { print("readonly "); }
    print("size='3'>");
    print("</td>");

    // Communication cell: COMMUNICATION SKILLS (VERBAL AND NON-VERBAL)
    print("<td align='center' width='10%'>Communication skills (verbal and non-verbal) (".$_SESSION['maxCommunicate'].")<br>");
    print("<input type=text name='communicate' value='".$_SESSION['communicate']."' ");
    if (!$_SESSION['fullAuthority']) {print("readonly ");}
    print("size=3>");
    print("</td>");

    // Communication cell: BONUS MARKS
    print("<td align='center' width='10%'>Bonus marks (".$_SESSION['maxBonus'].")<br>");
    print("<input type=text name='bonus' value='".$_SESSION['bonus']."' ");
    if (!$_SESSION['fullAuthority']) {print("readonly ");}
    print("size=3>");
    print("</td>");

    // Communication cell: COMMUNICATION (total of previous cells in row)
    print("<td align='center' width='10%'>");
    print("<b>Calgary Cambridge Communication Score (".$_SESSION['maxCommTotal'].")</b><br><input type='text' name='communication' value='".$_SESSION['communication']."' readonly size='3'>");
    print("</td>");

    // Communication cell: COMPETENT [radio button]
    print("<td align='center' width='10%'>Competent<br>");
    print("<input type='radio' name='competent' value='10'");
    if (!$_SESSION['fullAuthority']) { print(" disabled='disabled'"); };
    if ($_SESSION['competent'] === '10') { print(" checked='checked'"); };
    print(">Yes<br>");
    print("<input type='radio' name='competent' value='0'");
    if (!$_SESSION['fullAuthority']) { print(" disabled='disabled'"); };
    if ($_SESSION['competent'] === '0') { print(" checked='checked'"); };
    print(">No<br>");
    // N.B. Have used === (equivalent) and not == (equals) since we want to discriminate between 0 and which causes the "Competent" radio button to be set to "No" and
    // "(NULL)" (the default value in the database) which causes neither Yes nor No in Competent to be set.  
    print("</td>");

    // Communication cell: DANGEROUS ADVICE GIVEN - ticking thos forces the total to zero (summer 2012)
    print("<td width='10%'>");
    // Title ...
    print("<center>Serious Error in Counselling - Automatic zero</center><br><br>");

    print("\n\n<input type='checkbox' name='seriouserrorcounselling' value='1'");
    if (!$_SESSION['fullAuthority']) { print(" disabled='disabled'"); };
    if ($_SESSION['seriousErrorCounselling']) { print (" checked='checked'"); }
    print(">Dangerous advice given<br>\n");
    print("\n\n</td>");

    print("</tr>");

    print("</table></tr>");

    // Comments for students ...
    print("<tr>");
    print("<td colspan='2'>Enter Comments");
    if ($_SESSION['fullAuthority']) { print("<br>(NOTE: Students will be able to see any comments written in this box both in PRACTICE and EXAM modes � KEEP IT GENERIC!)"); };
    print ("</td>");
    print("<td colspan='5'><textarea name='textOTC' cols='100' rows='4'");
    if (!$_SESSION['fullAuthority']) { print(" readonly"); };
    print(">".$_SESSION['OTCcomments']."</textarea></td>");
    print("</tr>");

    // Audit trail box - moderators only ...
    if ($_SESSION['fullAuthority']) {
        print("<tr>");
        print("<td colspan='2'>Enter Comments for Audit Trail Only</td>");
        print("<td colspan='5'><textarea name='textAudit' cols='100' rows='4'>".$_SESSION['commentsAudit']."</textarea></td>");
        print("</tr>");
    };

    QA_buttons();

// Include button not for students
//    print("<TR><TD COLSPAN=4 ALIGN=CENTER>");
//    if ($_SESSION['fullAuthority']) {print("<INPUT TYPE=SUBMIT
//        VALUE='Include data from this page and re-calculate'>");}
//    print("</TD></TR>");

// Write to file link not for students
//    print("<TR><TD align='center' colspan='5'><font size='+1'>");
//    if ($_SESSION['fullAuthority']) {print("<a
//          href='ScripModerate.php?tab=52'>Write all included data to
//          file</a>");}
//    print ("</font></TD></TR>");
//
    print("</table></div>\n");
    print("</form>");
};

function HA_CommsResults(){

    global $case;

// Debugging code in next line
//if (isset($_SESSION['textOTC']) ) { print ("<br>is set<br>"); } else { print ("<br>IS NOT SET<br>"); };

// Why was this check ever made?
//    if (!isset($_SESSION['textOTC']) ) {

// A cheat way to force our way into this IF block in all circumstances
//if (1){


    // A sort of fix - the original condition OR categoryToMod is either 3 or 6 ...
    if ((!isset($_SESSION['textOTC']) || $_SESSION['categoryToMod'] == 6 || $_SESSION['categoryToMod'] == 3) && !$_SESSION['moderatorHasPressedIncludeData']) {


print ("<br>In the IF statement<br>");

        // Get the data ...

        $_SESSION['textOTC'] = "";
        $OTCOnPaper='0';

        $db=new dbabstraction();
        $db->connect() or die ($db->getError());

        $condition=array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);
        switch ($_SESSION['categoryToMod']) {
            case 1: case 2:
            $OTCOnPaper='1';
            break;

            case 3:
            $table=$db->select("ScripOTCModerated",$condition);
            break;

            case 4: case 5:
            $OTCOnPaper='1';
            break;

            case 6:
            $table=$db->select("ScripOTCExamModerated",$condition);
            break;

            case 7:
            $table=$db->select("ScripOTCModerated",$condition);
            break;

            case 8:
            $OTCOnPaper='1';
            break;
        };

        // Is the OTC information on paper (never marked) or in database (already moderated)?
        if (!$OTCOnPaper) {
            // In database ...
            foreach ($table as $row) {
                // Text only (mainly numbers) ...
                $_SESSION['initiate'] = $row['Initiate'];
                $_SESSION['gather'] = $row['Gather'];
                $_SESSION['explain'] = $row['Plan'];
                $_SESSION['conclude'] = $row['Conclude'];
                $_SESSION['answer'] = $row['Answer'];
                $_SESSION['communicate'] = $row['Communicate'];
                $_SESSION['bonus'] = $row['Bonus'];
                $_SESSION['competent'] = $row['Competent'];

                $_SESSION['seriousErrorCounselling'] = $row['FatalCounselling'];

                $_SESSION['correctP'] = $row['CorrectP'];
                $_SESSION['itemsOfInfo'] = $row['ItemsOfInfo'];
                $_SESSION['OTCQ1'] = $row['Q1'];
                $_SESSION['OTCQ2'] = $row['Q2'];

                $_SESSION['lostOTCOOD'] = $row['OOD'];
                $_SESSION['lostOTCOOD2'] = $row['OOD2'];
                $_SESSION['lostOTCNegative'] = $row['Negative'];
                $_SESSION['lostOTCNegative2'] = $row['Negative2'];
                $_SESSION['lostOTCWrongQtyLegal'] = $row['WrongQuantityLegal'];
                $_SESSION['lostOTCNegative2'] = $row['WrongQuantityLegal2'];
                $_SESSION['lostOTCFatal']=$row['Fatal'];

                // Original was a single number ...
                if (!$_SESSION['multiple']) { $_SESSION['seriousErrorType'] = $row['SeriousErrorType']; };
                if ($_SESSION['multiple']) {
                    unset ($_SESSION['seriousErrorType']);

                    // Exploding a nul string seems to produce a one-element array, where that one element is itself
                    // nul ... nevertheless the array is deemed to have one element ...
                    $toBeExploded = trim($row['SeriousErrorType']);
                    if ($toBeExploded != ""){
                        $_SESSION['seriousErrorType'] = explode(" ", trim($row['SeriousErrorType']));
                    };


//                    $_SESSION['seriousErrorType'] = explode(" ", trim($row['SeriousErrorType']));
                };

                $_SESSION['lostNA']=$row['NA'];
                $_SESSION['lostAAI']=$row['AAI'];
                $_SESSION['lostAAI2']=$row['AAI2'];
                $_SESSION['lostOTCCDSig']=$row['CDSig'];

                $_SESSION['OTCcomments'] = $row['Comments'];
                $_SESSION['commentsAudit'] = $row['CommentsAudit'];

                // Matching tickboxes ...
                if ($_SESSION['lostOTCOOD']) { $_SESSION['OTCOOD']=on; };
                if ($_SESSION['lostOTCOOD2']) { $_SESSION['OTCOOD2']=on; };
                if ($_SESSION['lostOTCNegative']) { $_SESSION['OTCNegative']=on; };
                if ($_SESSION['lostOTCNegative2']) { $_SESSION['OTCNegative2']=on; };
                if ($_SESSION['lostOTCWrongQtyLegal']) { $_SESSION['OTCWrongQtyLegal']=on; };
                if ($_SESSION['lostOTCWrongQtyLegal2']) { $_SESSION['OTCWrongQtyLegal2']=on; };
                if ($_SESSION['lostOTCFatal']) { $_SESSION['OTCFatal']=on; }; 
                if ($_SESSION['lostNA']) { $_SESSION['failedNA']=on; }; 
                if ($_SESSION['lostAAI']) { $_SESSION['AAI']=on; }; 
                if ($_SESSION['lostAAI2']) { $_SESSION['AAI2']=on; }; 
                if ($_SESSION['lostOTCCDSig']) { $_SESSION['OTCCDSig']=on; }; 

                // Now set the variables with box in their name - we believe these variables supersede the variables without box in their name
                if ($_SESSION['lostOTCOOD']) { $_SESSION['OTCOODbox']=on; };
                if ($_SESSION['lostOTCOOD2']) { $_SESSION['OTCOODbox2']=on; };

            };
        } else {
            // On paper ...
            $_SESSION['initiate']=0;
            $_SESSION['gather']=0;
            $_SESSION['explain']=0;
            $_SESSION['conclude']=0;
            $_SESSION['answer']=0;
            $_SESSION['communicate']=0;
            $_SESSION['bonus']=0;
            $_SESSION['competent']=0;

            $_SESSION['seriousErrorCounselling']=0;

            $_SESSION['correctP']=0;
            $_SESSION['itemsOfInfo'] = 0;
            $_SESSION['OTCQ1'] = 0;
            $_SESSION['OTCQ2'] = 0; 

            $_SESSION['OTCOOD']=off; $_SESSION['lostOTCOOD']=0;
            $_SESSION['OTCOOD2']=off; $_SESSION['lostOTCOOD2']=0;
            $_SESSION['OTCNegative']=off; $_SESSION['lostOTCNegative']=0;
            $_SESSION['OTCNegative2']=off; $_SESSION['lostOTCNegative2']=0;
            $_SESSION['OTCWrongQtyLegal']=off; $_SESSION['lostOTCWrongQtyLegal']=0;
            $_SESSION['OTCWrongQtyLegal2']=off; $_SESSION['lostOTCWrongQtyLegal2']=0;
            $_SESSION['OTCFatal']=off; $_SESSION['lostOTCFatal']=0;
            $_SESSION['failedNA']=off; $_SESSION['lostNA']=0;
            $_SESSION['AAI']=off; $_SESSION['lostAAI']=0;
            $_SESSION['AAI2']=off; $_SESSION['lostAAI2']=0;
            $_SESSION['OTCCDSig']=off; $_SESSION['lostOTCCDSig']=0; 
//            $_SESSION['seriousErrorType'] = 0;
            unset($_SESSION['seriousErrorType']);
            $_SESSION['OTCcomments']="";
            $_SESSION['commentsAudit']="";
        };
    } else {
        $_SESSION['moderatorHasPressedIncludeData'] = 0;
    };
};

function I_StudentCase() {
// Looks up student and case for a given entry in ScripStudentRecord
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition=array("RecordID"=>$_SESSION['studentrecordID']);
//    $table=$db->select("ScripStudentRecord",$condition);

    if ($_SESSION['categoryToMod'] && $_SESSION['studentrecordID']) {
        // Read the relevant StudentRecord file ...
        switch ($_SESSION['categoryToMod']) { 
            case 1: 
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 2: 
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 3: 
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 4: 
            $table=$db->select("ScripStudentRecordExam",$condition);
            break;

            case 5: 
            $table=$db->select("ScripStudentRecordExam",$condition);
            break;

            case 6: 
            $table=$db->select("ScripStudentRecordExam",$condition);
            break;

            case 7: 
            $table=$db->select("ScripStudentRecord",$condition);
            break;

            case 8: 
            $table=$db->select("ScripStudentRecordHome",$condition);
            break;
        };

        foreach ($table as $row) {
            $_SESSION['studentID']=$row['StudentID'];
            $_SESSION['caseID']=$row['CaseID'];
        };

        $condition=array("StudentID"=>$_SESSION['studentID']);
        $table=$db->select("ScripStudent",$condition);
        foreach ($table as $row) {
            $_SESSION['firstname']=$row['FirstName'];
            $_SESSION['lastname']=$row['LastName'];
            $_SESSION['initials']=$row['initials'];
            $_SESSION['seat']=$row['Seat'];
        };
    };
};

function J_Finished() {
    // This writes away the moderated results (clearing any earlier such result):

    global $case;

    // Connect ...
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Set moderated field in ScripStudentRecord to TRUE ... 
    $text=array("ModeratedYN"=>1);
    $condition=array("RecordID"=>$_SESSION['studentrecordID']);
// print("<br>The categoryToMod variable is " . $_SESSION['categoryToMod']);
    switch ($_SESSION['categoryToMod']) {
        case 1: $db->update("ScripStudentRecord", $text, $condition); break;
        case 2: $db->update("ScripStudentRecord", $text, $condition); break;
        case 3: $db->update("ScripStudentRecord", $text, $condition); break;
        case 4: $db->update("ScripStudentRecordExam", $text, $condition); break;
        case 5: $db->update("ScripStudentRecordExam", $text, $condition); break;
        case 6: $db->update("ScripStudentRecordExam", $text, $condition); break;

        // 7 and 8 refer to student options - updates not available
    };

    // Delete any existing entries in ScripResultsModerated for this student and this case ...
    $condition = array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);
    switch ($_SESSION['categoryToMod']) {
        case 1: case 2: case 3:
//print("<br>In 1, 2 or 3 to delete existing");
        $db->delete("ScripResultsModerated", $condition);
        break;

        case 4: case 5: case 6:
//print("<br>In 4, 5 or 6 to delete existing");
        $db->delete("ScripResultsExamModerated", $condition);
        break;

        // 7 and 8 refer to student options - updates not available
    };

    // Write the revised comments to ResultsModerated ... 
    $pages = array($_SESSION['textMain'], $_SESSION['textCD'], $_SESSION['textPOR']);

//print("<pre>");
//print_r($pages);

    foreach($pages as $page){ // Only do this if not null string ...
        if (trim($page) != "") {
            $explode = explode ("\n", $page); // ... separate into lines

            foreach ($explode as $row){
                list ($penalty, $remainder) = split ( '-', $row, 2);
//                list ($tab, $comment) = split ( ':', $remainder, 2); // ... pre-generic code

                // Split further ...
                list ($tab, $subremainder) = split ( '~', $remainder, 2);
                list ($gC, $comment) = split(':', $subremainder, 2);

                $penalty = trim($penalty); // ... tidy up
                $tab = trim($tab);
                $comment = trim($comment);
                $gC = trim($gC);

                if ($penalty == 0){
                    $gC = 0;
                };

                // Try to cut out debris - zero penalty and no tab name
                if (!(($penalty=="0" || $penalty==0) && $tab=="")) {
                    //  GENERIC COMMENT CODE (October 2009) - (1) If the moderator has removed the comment, then the associated generic
                    // comment will also disappear; (2) It is practice in exams for the moderator not to remove incorrect marking but
                    // merely to set the penalty ro zero; when this is met in the code for the generic comments, the code must ignore the
                    // the generic comment when adding the totals ...

                    $newline = array ("StudentID"=>$_SESSION['studentID'],"CaseID"=>$_SESSION['caseID'],"Penalty"=>$penalty, "Tab"=>$tab, "Comment"=>$comment, "GenericComment"=>$gC);
//print("<pre>");
//print_r($newline);
                    switch ($_SESSION['categoryToMod']) {
                        case 1: case 2: case 3:
                        $db->insert("ScripResultsModerated", $newline);
                        break;

                        case 4: case 5: case 6:
                        $db->insert("ScripResultsExamModerated", $newline);
                        break;

                        // 7 and 8 refer to student options - updates not available
                   };
                }; // ... end the IF block
            }; // ... end the FOREACH block
        };
    };

    // Construct the generic comment: 43 - out of date; 49 - no product; failed n/a - 51; AAI - 52; CD sig - 53 ...
    $genericComment = "";
    if ($_SESSION['lostOTCOOD']) { $genericComment .= " 43"; }; 
    if ($_SESSION['lostOTCOOD2']) { $genericComment .= " 43"; }; 
    if ($_SESSION['lostOTCNegative']) { $genericComment .= " 49"; }; 
    if ($_SESSION['lostOTCNegative2']) { $genericComment .= " 49"; }; 
    if ($_SESSION['lostOTCWrongQtyLegal']) { $genericComment .= " 54"; }; 
    if ($_SESSION['lostOTCWrongQtyLegal2']) { $genericComment .= " 54"; }; 
    if ($_SESSION['lostNA']) { $genericComment .= " 51"; };

    if (!$_SESSION['multiple']){ // For single ...
        // This one is special - the generic comment is read from a table depending on the precise nature of the serious error ...
        if ($_SESSION['seriousErrorType']) {
            $condition = array("ID"=>$_SESSION['seriousErrorType']);
            $table=$db->select("ScripSeriousErrorTypes",$condition);
            foreach ($table as $row){
                $seriousErrorGC = $row['GenericComment'];
            };
            $genericComment .= " ".$seriousErrorGC;
        };
    };

    if ($_SESSION['multiple']){ // For multiple ...
        // This one is special - the generic comment is read from a table depending on the precise nature of the seriuos error ...  
        // $_SESSION['seriousErrorTypes'] is an array so we execute the "single" code for each element in turn (inside a loop) ...
        for ($k = 0; $k < count($_SESSION['seriousErrorType']); $k++){
            $temp = $_SESSION['seriousErrorType'][$k];
            if ($temp) {
                $condition = array("ID"=>$temp);
                $table=$db->select("ScripSeriousErrorTypes", $condition);
                foreach ($table as $row){
                    $seriousErrorGC = $row['GenericComment'];
                };
                $genericComment .= " ".$seriousErrorGC;
            };
        };
    };

    if ($_SESSION['lostAAI']) { $genericComment .= " 52"; }; 
    if ($_SESSION['lostAAI2']) { $genericComment .= " 52"; }; 
    if ($_SESSION['lostOTCCDSig']) { $genericComment .= " 53"; };

    if ($_SESSION['seriousErrorCounselling']) { $genericComment .= " 59"; };

    // What is stored in the SeriousErrorType field depends on multiple or single ...
    if (!$_SESSION['multiple']){ $temp = $_SESSION['seriousErrorType']; };
    if ($_SESSION['multiple']){
        $temp = "";
        for ($k = 0; $k < count($_SESSION['seriousErrorType']); $k++){
            $temp.= " ".$_SESSION['seriousErrorType'][$k];
        };
    };

    // Delete any existing OTC results for this case and write the new ones
    $newline = array ("StudentID"=>$_SESSION['studentID'],
                      "CaseID"=>$_SESSION['caseID'],
                      "Initiate"=>$_SESSION['initiate'],
                      "Gather"=>$_SESSION['gather'],
                      "Plan"=>$_SESSION['explain'],
                      "Conclude"=>$_SESSION['conclude'],
                      "Answer"=>$_SESSION['answer'],
                      "Communicate"=>$_SESSION['communicate'],
                      "Bonus"=>$_SESSION['bonus'],
                      "FatalCounselling"=>$_SESSION['seriousErrorCounselling'],
                      "Q1"=>$_SESSION['OTCQ1'],
                      "Q2"=>$_SESSION['OTCQ2'], 
                      "ItemsOfInfo"=>$_SESSION['itemsOfInfo'],
                      "CorrectP"=>$_SESSION['correctP'],

                      "OOD"=>$_SESSION['lostOTCOOD'],
                      "OOD2"=>$_SESSION['lostOTCOOD2'],
                      "Negative"=>$_SESSION['lostOTCNegative'],
                      "Negative2"=>$_SESSION['lostOTCNegative2'],
                      "WrongQtyLegal"=>$_SESSION['lostOTCWrongQtyLegal'],
                      "WrongQtyLegal2"=>$_SESSION['lostOTCWrongQtyLegal2'],
                      "NA"=>$_SESSION['lostNA'],
                      "Fatal"=>$_SESSION['lostOTCFatal'],
                      "AAI"=>$_SESSION['lostAAI'],
                      "AAI2"=>$_SESSION['lostAAI2'],
                      "CDSig"=>$_SESSION['lostOTCCDSig'],
                      "Comments"=>$_SESSION['OTCcomments'],
                      "SeriousErrorType"=>$temp,
                      "GenericComment"=>$genericComment,
                      "CommentsAudit"=>$_SESSION['commentsAudit'],
                      "StudentRecordID"=>$_SESSION['studentrecordID']);

    if ($_SESSION['competent'] === "0" || $_SESSION['competent'] === "10"){
        // Add in the $_SESSION['competent'] value (for field Competent) if it equals either 0 or 10, but not otherwise ...
        $newline = array_merge($newline, array("Competent"=>$_SESSION['competent'])); 
        // ... prevents zero appearing in Competent field when demonstrator hasn't put anything into marking interface.
    };

    $condition = array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);
    switch ($_SESSION['categoryToMod']) {
        case 1: case 2: case 3:
        $db->delete("ScripOTCModerated", $condition);
        $db->insert("ScripOTCModerated", $newline);
        break;

        case 4: case 5: case 6:
        $db->delete("ScripOTCExamModerated", $condition);
        $db->insert("ScripOTCExamModerated", $newline);
        break;

        // 7 and 8 refer to student options - updates not available
    };

// Delete any existing entries in ScripProbesModerated
// for this student and this case
    $condition = array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);
            switch ($_SESSION['categoryToMod']) {
                case 1: case 2: case 3:
                $db->delete("ScripProbesModerated", $condition);
                break;

                case 4: case 5: case 6:
                $db->delete("ScripProbesExamModerated", $condition);
                break;

            // 7 and 8 refer to student options - updates not available
    };

// Now insert new entries in ScripProbesModerated
// for this student and this case 
    $condition = array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);

    // Probes section has slightly different format ...
    if (trim($_SESSION['textChanges']) != "") {
        $explode = explode ("\n", $_SESSION['textChanges']);
        foreach ($explode as $row) {
            if ($row) { // ... ignore blank rows
                list ($penalty, $remainder) = split ( '-', $row, 2);
                list ($tab, $comment) = split ( ':', $remainder, 2);
                $newline = array ("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID'],"Marks"=>$penalty, "mM"=>trim($tab), "Probe"=>$comment);

                switch ($_SESSION['categoryToMod']) {
                    case 1: case 2: case 3:
                    $db->insert("ScripProbesModerated", $newline);
                    break;

                    case 4: case 5: case 6:
                    $db->insert("ScripProbesExamModerated", $newline);
                    break;

                // 7 and 8 refer to student options - updates not available
                };
            };
        };
    };

// Write the marks (e.g. 9/10) and the communications marks
//    (e.g. 17/20) to the marks file 

    $condition = array("StudentID"=>$_SESSION['studentID'], "CaseID"=>$_SESSION['caseID']);



//    $newline = array ("StudentID"=>$_SESSION['studentID'],
//       "CaseID"=>$_SESSION['caseID'], "ModeratorID"=>$_SESSION['ID'], 
//       "Marks"=>$_SESSION['finalScore'],
//       "MarksMaximum"=>$_SESSION['maxscore'], "OTCYN"=>$OTCYN,
//       "CommsMarks"=>$_SESSION['communication'],
//       "CommsMarksMaximum"=>$_SESSION['maxCommTotal']);    


    $newline = array ("StudentID"=>$_SESSION['studentID'],
       "CaseID"=>$_SESSION['caseID'], "ModeratorID"=>$_SESSION['adminID'], 
       "Marks"=>$_SESSION['finalScore'],
       "MarksMaximum"=>$_SESSION['maxscore'], "OTCYN"=>$OTCYN,
       "CommsMarks"=>$_SESSION['communication'],
       "CommsMarksMaximum"=>$_SESSION['maxCommTotal']);    





    switch ($_SESSION['categoryToMod']) {
        case 1: case 2: case 3:
        $db->delete("ScripMarksModerated", $condition);
        $db->insert("ScripMarksModerated", $newline);
        break;

        case 4: case 5: case 6:
        $db->delete("ScripMarksExamModerated", $condition);
        $db->insert("ScripMarksExamModerated", $newline);
        break;

        // 7 and 8 refer to student options - updates not available
    };

//  Now unset all the variables ...

    print("<p><a href='ScripModerate.php?tab=10'>Back</a></p>");
};

function QA_buttons(){
    global $case;

    // Include button not for students ...
    print("<tr><td colspan='6' align='center'>");
    if ($_SESSION['fullAuthority']) {
        print("<input type='submit' value='Include data from this page and re-calculate'>");
    };
    print("</td></tr>");

    // Write to file link not for students ...
    print("<tr><td colspan='6' align='center'><font size='+1'>");
    if ($_SESSION['fullAuthority']) {
        print("<a href='ScripModerate.php?tab=52'>Write all included data to file</a>");
    };
    print ("</font></td></tr>");

//    print ("</TABLE></TD></TR></TABLE>");
//    print("</BODY></HTML>\n");
//
};


function ZA_byesAndCommsMax() {

    // Checks user and display options for program:
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Look up byes allowed ...
    $table=$db->select("ScripConfiguration");
    foreach ($table as $row) {
	$_SESSION['byes']=$row['Byes'];
    };

    // Look up maxima for communication scores ...
    $table=$db->select("ScripCommunicationMaxima");
    foreach ($table as $row) {
        $_SESSION['maxInitiate']=$row['Initiate'];
        $_SESSION['maxGather']=$row['Gather'];
        $_SESSION['maxExplain']=$row['Explain'];
        $_SESSION['maxConclude']=$row['Conclude'];
        $_SESSION['maxAnswer']=$row['Answer'];
        $_SESSION['maxCommunicate']=$row['Communicate'];
        $_SESSION['maxBonus']=$row['Bonus'];
    };
    $_SESSION['maxCommTotal'] = $_SESSION['maxInitiate'] + $_SESSION['maxGather'] + $_SESSION['maxExplain'] + $_SESSION['maxConclude'] + $_SESSION['maxAnswer'] + $_SESSION['maxCommunicate'] + $_SESSION['maxBonus'];
};

function ZB_MaxScore() {
    // Reads maximum score for case e.g. 10 or 20 
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripsScrips", array("Letter"=>$_SESSION['caseID']));
    foreach ($table as $row) {
        $_SESSION['formID']=$row['FormID'];

        $_SESSION['maxscore'] = 0;
        if (ZRE_CorrectRetainDrug(1)) {
            $_SESSION['maxscore'] += 10;
        };
        if (ZRE_CorrectRetainDrug(2)) {
            $_SESSION['maxscore'] += 10;
        };

/*
        if (ZRE_CorrectRetainDrug(2)) {
           $_SESSION['maxscore'] = 20;
        } else {
           $_SESSION['maxscore'] = 10;
        };
*/

// THIS IS A FUDGE AT PRESENT ...
// WOULD HAVE PROBLEMS IF THERE WERE TWO ITEMS ON ONE
// RECEIPT ... NOT EVEN SURE HOW WE WOULD TELL THE DATABASE!
        // Receipt of goods ...
        if ($_SESSION['formID'] == '10') {
            $_SESSION['maxscore'] = 10;
        };

    };
};

function ZD_subtotalsChanges(){

    // Calculate the sub-totals for Changes (within Main)
    global $case;
    $_SESSION['lostChanges'] = 0; $mTotal = 0;

    $explode = explode ("\n", $_SESSION['textChanges']);
    foreach ($explode as $row){
       list ($penalty, $remainder) = split ( '-', $row, 2);
       list ($tab, $comment) = split ( ':', $remainder, 2);
       if (trim($tab) == "M") {$_SESSION['lostChanges'] += $penalty;};
       if (trim($tab) == "m") {$mTotal += $penalty;};
    }
    $mTotal -= $_SESSION['byes']; // Allow for byes
    if ($mTotal < 0) $mTotal = 0; // Set to zero if not all byes used
    $_SESSION['lostChanges'] += $mTotal; // Both types
};

function ZE_subtotalsMain(){
    // Calculate the sub-totals for Main
    global $case;

    $_SESSION['lostMain'] = 0;
    $_SESSION['lostClassify'] = 0; $_SESSION['lostVerify'] = 0; $_SESSION['lostEndorse'] = 0;
    $_SESSION['lostLabel'] = 0; $_SESSION['lostAncillaries'] = 0; $_SESSION['lostFile'] = 0;
    $_SESSION['lostPMR'] = 0;

    $_SESSION['Fatal']=off;
    $explode = explode ("\n", $_SESSION['textMain']);
    foreach ($explode as $row){
        // SPLIT is now deprecated (February 2010) - change to EXPLODE instead ...
        // Break off the penalty (first item) ...
//        list ($penalty, $remainder) = split ( '-', $row, 2);
        // Break off the tab (second item) ...
//        list ($tab, $remainder1) = split ( ':', $remainder, 2);
        // Break off the generic code comment (third item) ...
//        list ($GC, $remainder2) = split ( '~', $remainder1, 2);

        if ($_SESSION['fullAuthority']){
            // This is the full version (for moderator): 
            // Break off the penalty (first item) ...
            $explodePenalty = explode ("-", $row);
            $penalty = trim($explodePenalty[0]);

            // Break off the tab (second item) ...
            $explodeTab = explode ("~", $explodePenalty[1]);
            $tab = trim($explodeTab[0]);

            // Break off the generic code comment (third item) ...
            $explodeGC = explode (":", $explodeTab[1]);
            $GC = trim($explodeGC[0]);

            // What's left is the comment ...
            $remainder = trim($explodeGC[1]);
        } else {
            // This is the partial version (for moderator):
            // Break off the penalty (first item) ...
            $explodePenalty = explode ("-", $row);
            $penalty = trim($explodePenalty[0]);

            // Break off the tab (second item) ...
            $explodeTab = explode (":", $explodePenalty[1]);
            $tab = trim($explodeTab[0]);

            // What's left is the comment ...
            $remainder = trim($explodeTab[1]);
        };

        // Look to see if the serious penalty has been awarded anywhere ...
        if ($penalty == $_SESSION['theSeriousPenalty']) { $_SESSION['Fatal']=on; };        

        if (trim(strtoupper($tab)) == strtoupper("Classify"))
          {$_SESSION['lostClassify'] += $penalty; $_SESSION['lostMain'] += $penalty;}
        if (trim(strtoupper($tab)) == strtoupper("Verify"))
          {$_SESSION['lostVerify'] += $penalty; $_SESSION['lostMain'] += $penalty;}
        if (trim(strtoupper($tab)) == strtoupper("Label"))
          {$_SESSION['lostLabel'] += $penalty; $_SESSION['lostMain'] += $penalty;}
        if (trim(strtoupper($tab)) == strtoupper("Ancillaries"))
          {$_SESSION['lostAncillaries'] += $penalty; $_SESSION['lostMain'] += $penalty;}
        if (trim(strtoupper($tab)) == strtoupper("Endorse"))
          {$_SESSION['lostEndorse'] += $penalty; $_SESSION['lostMain'] += $penalty;}
        if (trim(strtoupper($tab)) == strtoupper("File"))
          {$_SESSION['lostFile'] += $penalty; $_SESSION['lostMain'] += $penalty;}
        if (trim(strtoupper($tab)) == strtoupper("PMR"))
          {$_SESSION['lostPMR'] += $penalty; $_SESSION['lostMain'] += $penalty;}


        if ($_SESSION['NACheck']==on) { $_SESSION['lostMain'] +=
$_SESSION['lostNACheck']; };     };
};

function ZF_subtotalsCD(){
    // 

    // Calculate the sub-totals for CD page
    global $case;
    $_SESSION['lostCD'] = 0;
    $explode = explode ("\n", $_SESSION['textCD']);
    foreach ($explode as $row){

        // This uses the depreacted function LIST ...
//        list ($penalty, $remainder) = split ( '-', $row, 2);
//        list ($tab, $comment) = split ( ':', $remainder, 2);

        // Break off the penalty (first item) ...
//        $explodePenalty = explode ("-", $row);
//        $penalty = $explodePenalty[0];

        // Break off the tab (second item) ...
//        $explodeTab = explode ("~", $explodePenalty[1]);
//        $tab = $explodeTab[0];

        // Break off the generic code comment (third item) ...
//        $explodeGC = explode (":", $explodeTab[1]);
//        $GC = $explodeGC[0];

        // What's left is the comment ...
//        $comment = $explodeGC[1];


        if ($_SESSION['fullAuthority']){
            // This is the full version (for moderator): 
            // Break off the penalty (first item) ...
            $explodePenalty = explode ("-", $row);
            $penalty = trim($explodePenalty[0]);

            // Break off the tab (second item) ...
            $explodeTab = explode ("~", $explodePenalty[1]);
            $tab = trim($explodeTab[0]);

            // Break off the generic code comment (third item) ...
            $explodeGC = explode (":", $explodeTab[1]);
            $GC = trim($explodeGC[0]);

            // What's left is the comment ...
            $comment = trim($explodeGC[1]);
        } else {
            // This is the partial version (for moderator):
            // Break off the penalty (first item) ...
            $explodePenalty = explode ("-", $row);
            $penalty = trim($explodePenalty[0]);

            // Break off the tab (second item) ...
            $explodeTab = explode (":", $explodePenalty[1]);
            $tab = trim($explodeTab[0]);

            // What's left is the comment ...
            $comment = trim($explodeTab[1]);
        };


        // Add this POR penalty (from $row) into the total POR total ...
        if (trim(strtoupper($tab)) == strtoupper("CD")) {
            $_SESSION['lostCD'] += $penalty;
        };
    };
};

// Calculate the sub-totals for POR page
function ZG_subtotalsPOR(){
    global $case;
    $_SESSION['lostPOR'] = 0;
    $explode = explode ("\n", $_SESSION['textPOR']);
    foreach ($explode as $row){

        // This uses the depreacted function LIST ...
//        list ($penalty, $remainder) = split ( '-', $row, 2);
//        list ($tab, $comment) = split ( ':', $remainder, 2);

        // Break off the penalty (first item) ...
//        $explodePenalty = explode ("-", $row);
//        $penalty = $explodePenalty[0];

        // Break off the tab (second item) ...
//        $explodeTab = explode ("~", $explodePenalty[1]);
//        $tab = $explodeTab[0];

        // Break off the generic code comment (third item) ...
//        $explodeGC = explode (":", $explodeTab[1]);
//        $GC = $explodeGC[0];

        // What's left is the comment ...
//        $comment = $explodeGC[1];

        if ($_SESSION['fullAuthority']){
            // This is the full version (for moderator): 
            // Break off the penalty (first item) ...
            $explodePenalty = explode ("-", $row);
            $penalty = trim($explodePenalty[0]);

            // Break off the tab (second item) ...
            $explodeTab = explode ("~", $explodePenalty[1]);
            $tab = trim($explodeTab[0]);

            // Break off the generic code comment (third item) ...
            $explodeGC = explode (":", $explodeTab[1]);
            $GC = trim($explodeGC[0]);

            // What's left is the comment ...
            $comment = trim($explodeGC[1]);
        } else {
            // This is the partial version (for moderator):
            // Break off the penalty (first item) ...
            $explodePenalty = explode ("-", $row);
            $penalty = trim($explodePenalty[0]);

            // Break off the tab (second item) ...
            $explodeTab = explode (":", $explodePenalty[1]);
            $tab = trim($explodeTab[0]);

            // What's left is the comment ...
            $comment = trim($explodeTab[1]);
        };

        // Add this POR penalty (from $row) into the total POR total ...
        if (trim(strtoupper($tab)) == strtoupper("POR")) {
            $_SESSION['lostPOR'] += $penalty;
        };
    };
};

function ZH_subtotalsOTC(){

    // Calculate the sub-totals for OTC and Communications page

    global $case;
    $_SESSION['lostOTC'] = 0;
    $explode = explode ("\n", $_SESSION['textOTC']);

    // Communications total:
    if ($_SESSION['seriousErrorCounselling']){
        // Dangerous advice given therefore communication total is automatically zero ...
        $_SESSION['communication'] = 0;
    } else {
        // Otherwise add up the different types of communication to find total ...
        $_SESSION['communication'] = $_SESSION['initiate'] + $_SESSION['gather'] + $_SESSION['explain'] + $_SESSION['conclude'] + $_SESSION['answer'] + $_SESSION['communicate'] + $_SESSION['bonus'];
    };

    // OTC score ...
//    $_SESSION['OTCScore'] = $_SESSION['correctP'] + $_SESSION['itemsOfInfo'] + $_SESSION['OTCQ1'] + $_SESSION['OTCQ2'];
    $_SESSION['OTCScore'] = $_SESSION['competent'];

    $_SESSION['OTCTotal'] = 0;

    // Look at each box: if ticked, add appropriate penalty to total of penalties ...
    if ($_SESSION['OTCOODbox']==on) {
        $_SESSION['lostOTCOOD'] = 10;
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCOOD']; 
    } else {
        $_SESSION['lostOTCOOD'] = 0;
    };

    if ($_SESSION['OTCOODbox2']==on) {
        $_SESSION['lostOTCOOD2'] = 10; 
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCOOD2']; 
    } else {
        $_SESSION['lostOTCOOD2'] = 0;
    };

    if ($_SESSION['OTCNegative']==on) {
        $_SESSION['lostOTCNegative'] = 10;
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCNegative']; 
    } else {
        $_SESSION['lostOTCNegative'] = 0;
    };

    if ($_SESSION['OTCNegative2']==on) {
        $_SESSION['lostOTCNegative2'] = 10; 
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCNegative2']; 
    } else {
        $_SESSION['lostOTCNegative2'] = 0;
    };

    if ($_SESSION['OTCWrongQtyLegal']==on) {
        $_SESSION['lostOTCWrongQtyLegal'] = 10;
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCWrongQtyLegal']; 
    } else {
        $_SESSION['lostOTCWrongQtyLegal'] = 0;
    };

    if ($_SESSION['OTCWrongQtyLegal2']==on) {
        $_SESSION['lostOTCWrongQtyLegal2'] = 10; 
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCWrongQtyLegal2']; 
    } else {
        $_SESSION['lostOTCWrongQtyLegal2'] = 0;
    };

    // The check here used to be this
    //               if ($_SESSION['OTCFatal']==on) {
    // now, however, we don't have a specific fatal box, but lots of individual ones in the array $_SESSION['seriousErrorType'][$k] ...
    if (count($_SESSION['seriousErrorType'])) {
        $_SESSION['lostOTCFatal'] = $_SESSION['theSeriousPenalty']; // ... choose the correct value
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCFatal']; 
    } else {
        $_SESSION['lostOTCFatal'] = 0;
    };
    if ($_SESSION['failedNA']==on) {
        $_SESSION['lostNA'] = 5; 
        $_SESSION['OTCTotal'] += $_SESSION['lostNA']; 
    } else {
        $_SESSION['lostNA'] = 0;
    };

    if ($_SESSION['AAI']==on) {
        $_SESSION['lostAAI'] = 1; 
        $_SESSION['OTCTotal'] += $_SESSION['lostAAI']; 
    } else {
        $_SESSION['lostAAI'] = 0;
    };

    if ($_SESSION['AAI2']==on) {
        $_SESSION['lostAAI2'] = 1; 
        $_SESSION['OTCTotal'] += $_SESSION['lostAAI2']; 
    } else {
        $_SESSION['lostAAI2'] = 0;
    };

    if ($_SESSION['OTCCDSig']==on) {
        $_SESSION['lostOTCCDSig'] = 1; 
        $_SESSION['OTCTotal'] += $_SESSION['lostOTCCDSig']; 
    } else {
        $_SESSION['lostOTCCDSig'] = 0;
    };

};

function ZI_subtotalsCombine(){
    // Combine all the sub-totals:

    global $case;

    // If there are no serious errors then the array seriousErrorType will be set to one element with value 0 when read fom the database; if so, need to change
    // it to a zero-element array. Do this very formally [e.g. in a ten-element array, need to loop from 9 down to 0] ...
    for ($k = count($_SESSION['seriousErrorType']); $k > 0; $k--){
        $km1 = $k - 1;
        if ($_SESSION['seriousErrorType'][$km1] == "" || $_SESSION['seriousErrorType'][$km1] == 0){
            array_splice($_SESSION['seriousErrorType'], $km1, 1);
        }; // ... remove 1 element starting from element being considered (which means this element only)
    };

    $explode = explode ("\n", $_SESSION['textOTC']);
    if ($_SESSION['formID'] != 8){
        // All scripts other than OTCs ...

        // Lost on the registers ...
        $_SESSION['lostRegisters'] = $_SESSION['lostCD'] + $_SESSION['lostPOR'];

        // Lost on the probes, main tab and the two registers 
        $_SESSION['lostTabs'] = $_SESSION['lostChanges'] + $_SESSION['lostMain'] + $_SESSION['lostRegisters'];

        // The final score is the maximum minus the total minus the demonstrator input ...
        $_SESSION['finalScore'] = $_SESSION['maxscore'] - $_SESSION['lostTabs'] - $_SESSION['OTCTotal'];

        if ($_SESSION['finalScore'] < 0) {$_SESSION['finalScore'] = 0;}

        // I think this is no longer relevant ...
        if ($_SESSION['OODNoProd']==on) {
            $_SESSION['lostOODNoProd'] = 10; $_SESSION['finalScore'] = 0; 
        } else {
            $_SESSION['lostOODNoProd'] = 0;
        };

//        if ($_SESSION['Fatal']==on || $_SESSION['OTCFatal']==on) {

        // Is there a serious error, either through the Demonstrator input ($_SESSION['seriousErrorType']) or Main ($_SESSION['Fatal']) ...

        if (count($_SESSION['seriousErrorType']) || $_SESSION['Fatal']==on) {
            $_SESSION['lostFatal'] = $_SESSION['theSeriousPenalty'];
            // Until 2010: Final Score: -15 for single drug; -5 for double ...
            $_SESSION['finalScore'] = $_SESSION['maxscore'] - $_SESSION['lostFatal']; 
        } else {
            $_SESSION['lostFatal'] = 0;
        };

    } else { // OTC ...
        // Not possible to lose marks on the tabs for OTC ...
        $_SESSION['lostTabs'] = 0;
//        if ($_SESSION['OTCFatal']==on) {

        // Have any fatal errors occurred?
        if (count($_SESSION['seriousErrorType'])) {
            // If so (as usual, allow a negative score) ...
            $_SESSION['finalScore'] = 10 - $_SESSION['theSeriousPenalty'];
        } else {
            // Not a fatal error so final score is score from normal OTC parameters minus any out of date penalty ... 
            $_SESSION['finalScore'] = $_SESSION['OTCScore'] - $_SESSION['lostOTCOOD'];
            // ... if less than zero, set to zero ...
            if ($_SESSION['finalScore'] < 0) { $_SESSION['finalScore'] = 0; };
        };
    };
};

function ZJ_correctResultsTable(){
    // THIS SHOULD NO LONGER BE REQUIRED
    // Choose the correct results table from which to read:
 
    global $case;
    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
        $resultsTable = "ScripResults";
    } else {
        $resultsTable = "ScripResultsModerated";
    };
};


function ZRE_CorrectRetainDrug($i) {

    // Parameter is 1 (for first drug) or 2 (for second)
    // Is there a first or second drug ...
    global $case;

    $i--; // Revert to zero-based variable
    $retaindrug='1'; // Starting value is TRUE

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Take current latter and find where problems are stored
    $condition=array('Letter'=>$_SESSION['caseID']);

    $table=$db->select("ScripsScrips",$condition);    
    foreach ($table as $row) {
        if ($i=='0') {
            $drugprobid=$row['DrugProblemID'];
        } else {
            $drugprobid=$row['DrugProblemID2'];
        };
        $miscprobid=$row['MiscProblemID'];
    };

    if (!$drugprobid) { $retaindrug='0'; };

/* Old code (2005)
// When an interaction occurs, it is the second drug
// ($i=1) that is removed and not the first drug
// ($i=0) ... a ScripWare convention!

if ($i==1) {
//  Only non-drug specific reason for non-display is "Interaction"
    $condition=array('MiscProblemID'=>$miscprobid);
    $table=$db->select("ScripMiscProblem",$condition);
    foreach ($table as $row) {
       $interaction=$row['IntAction']; // "A", "R" or "D"
    };

    $retaindrug &= !($interaction=="D");
};
*/

    // Look for an interaction of this script ...
    $condition=array('CaseID'=>$_SESSION['caseID']);
    $table=$db->select("ScripInteractionMA",$condition);
    foreach ($table as $row) {
       $interaction=$row['Suggestion']; // e.g. "A", "R" or "D", "+"
       $withWhat = $row['WithWhat'];
       $deleteWhich = $row['DeleteWhich'];
    };

    // The only case where the drug is NOT retained is when
    // interaction is delete (others are advise, change dose
    // and advise) ...
    if ($interaction=="D") {

        $nonZeroNumber = $i + 1; // Back to working with non-zero based number

        // Either of two conditions can lead to deleting the drug: (a) The
        // drug under consideration ($i) reacts with a line in the PMR; or
        // (b) The d under c interacts with another item on the prescription
        // and it is the one to be deleted (as indicated by
        // field DeleteWhich) ... 
        if  (($nonZeroNumber == $withWhat)
                   || ($withWhat='12' && $deleteWhich == $nonZeroNumber)) {
            $retaindrug = 0;
        };
    };

//    $retaindrug &= !($interaction=="D");

// All others are drug-specific

    // $drugprobid should be non-zero ... just check!
    if ($drugprobid) {

        // Get problems
        $condition=array('DrugProblemID'=>$drugprobid);
        $table=$db->select("ScripDrugProblem",$condition);    
        foreach ($table as $row) {
            $drugmispelling=$row['DrugMispelling'];
            $druginappropriatechoice=$row['DrugInappropriateChoice'];
            $drugnonexist=$row['DrugNonExist'];
            $blacklisted=$row['Blacklisted'];
            $notformulary=$row['NotFormulary'];
            $wrongpractitioner=$row['WrongPractioner'];
            $wrongscrip=$row['WrongScrip'];
        };

    // Logic: Don't retain if the problem has been set up
    //        and there is no replacement i.e. retain if NOT (...)
    //        Now AND all these together since just one "Don't retain"
    //        is enough to not display drug



if ($druginappropriatechoice && !$drugmispelling) { $retaindrug = 0; };
if ($drugnonexist && !$drugmispelling) { $retaindrug = 0; };
if ($blacklisted && !$drugmispelling) { $retaindrug = 0; };
if ($notformulary && !$drugmispelling) { $retaindrug = 0; };
if ($wrongpractitioner && !$drugmispelling) { $retaindrug = 0; };
if ($wrongscrip && !$drugmispelling) { $retaindrug = 0; };

//        $retaindrug &= !($druginappropriatechoice && !$drugmispelling);
//        $retaindrug &= !($drugnonexist && !$drugmispelling);
//        $retaindrug &= !($blacklisted && !$drugmispelling);
//        $retaindrug &= !($notformulary && !$drugmispelling);
//        $retaindrug &= !($wrongpractitioner && !$drugmispelling);
//        $retaindrug &= !($wrongscrip && !$drugmispelling);
    };


    return $retaindrug;
};


function D_displayscrip() {
// Choose which prescription form to display
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition=array("FormID"=>$_SESSION['formID']);
    $table=$db->select("ScripForms",$condition);
    foreach ($table as $row) {
     	$formname2=$row['FormName2'];
     	$formname1=$row['FormName1'];
     	$_SESSION['formcolour']=$row['FormColour'];
     	$groupID=$row['GroupID'];
	$spectext=$row['SpecialText'];
    };
    $_SESSION['group']=$groupID;
    $db->close();

    print("<TR><tD rowspan=2 VALIGN=TOP>");

    switch($groupID) {
        case 1: // Requisition
        DA_displayrequisition();
        break;

        case 2: // Requisition-Midwife
        DG_displayrequisitionmw();
        break;

        case 3: // Private
        DB_displayprivate();
        break;

        case 4: // Standard 
        DC_displaystandardscrip($spectext);
        break;

        case 5:  // OTC
        DD_displayOTC();
        break;

        case 6:  // Receipt of goods
        DF_displayreceipt();
        break;

        case 7: // Emergency
        DB_displayprivate();
        break;
  
        case 8: // Private CD 
        DC_displaystandardscrip($spectext);
        break;

        case 9: // Hospital TTO 
            if ($_SESSION['PageToShow']){
                DZ_HospitalFormReverse();
            } else {
                DZ_HospitalForm();
            };

        print("<tr><td rowspan='2'><table border='1' align='left'>");
        print("<tr><td><font color='#FFFFFF'>");
        print("<h3>This prescription not available at present.</h3>");
        print("</font></td></tr></table>\n");
        break;

        case 10: // CD Requisition 
            if ($_SESSION['PageToShow']){
                DY_PrivateCDRequisitionReverse();
            } else {
                DY_PrivateCDRequisition();
            };
//        print("<tr><td rowspan='2'><table border='1' align='left'>");
//        print("<tr><td><font color='#FFFFFF'>");
//        print("<h3>This prescription not available at present.</h3>");
//        print("</font></td></tr></table>\n");
        break;

        default:
//        print("<tr><td rowspan='2'><table border='1' align='left'>");
//        print("<tr><td><font color='#FFFFFF'>");
//        print("<h3>This prescription not available at present.</h3>");
//        print("</font></td></tr></table>\n");
        break;
    };
    print("</td>\n");
};




function DY_PrivateCDRequisition() {
    global $case;
    print("<div class=tablewhite>");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array ('EndorseID'=>$_SESSION['EndorseID']);
    $table=$db->select("ScripEndorse", $condition);
    foreach ($table as $row) {
       $NottStamp=$row['NottStamp'];
    };

    print ("<table width='372' border='1'>\n");

/*
    print("<div style='background-color:#ffffff'>");

    // Row: Part A - Supplier Details
    print("<div style='width:372px; margin-bottom:10px'>");

    print("<div style='border-color:#000000; width: 62px; position:relative; float:left; background-color:#000000; color:#FFFFFF;'>");
    print("Part A");
    print("</div>");

    print("<div style='border-color:#000000; width:310px; position:relative; float:left; background-color:#CCCCCC; color:#000000'>");
    print("Supplier details");
    print("</div>");

    print("</div>");

    print("<br />");

    // Row A - The details ...
    print("<div style='width:372px; margin-bottom:10px'>");

    print("<div style='padding:2px; border:#000000 1px solid; height:120px; width:120px; position:relative; float:left; color:#000000'>");
    print("Supplier's Stamp:");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:60px; position:relative; float:left; color:#000000'>");
    print("Account Number:");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:180px; position:relative; float:left; color:#000000'>");
    print("ex AC");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:60px; position:relative; float:left; color:#000000'>");
    print("Name of business:");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:180px; position:relative; float:left; color:#000000'>");
    print("exNB");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:60px; position:relative; float:left; color:#000000'>");
    print("Address:");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:180px; position:relative; float:left; color:#000000'>");
    print("exA1");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:60px; position:relative; float:left; color:#000000'>");
    print("Address:");
    print("</div>");

    print("<div style='padding:2px; border:#000000 1px solid; height: 30px; width:180px; position:relative; float:left; color:#000000'>");
    print("exA2");
    print("</div>");

    print("</div>");

    print("<br />");

    // Row: Part B - Controlled Drugs Requisitioned
    print("<div style='width:372px; margin-bottom:10px'>");

    print("<div style='width: 62px; position:relative; float:left; background-color:#000000; color:#FFFFFF;'>");
    print("Part B");
    print("</div>");

    print("<div style='width:310px; position:relative; float:left; background-color:#CCCCCC; color:#000000'>");
    print("Controlled Drugs Requisitioned");
    print("</div>");

    print("</div>");

    print("<br />");

    // Row: Part B - The details ...
    print("<div style='width:372px; margin-bottom:10px'>");

    print("<div style='width: 372px; position:relative; float:left; color:#000000;'>");
    print("Drug name + Form");
    print("</div>");

    print("<div style='width:372px; position:relative; float:left; color:#000000'>");
    print("Example");
    print("</div>");

    print("<div style='width:248px; position:relative; float:left; color:#000000'>");
    print("Signature of customer");
    print("</div>");

    print("<div style='width:124px; position:relative; float:left; color:#000000'>");
    print("Date of order:");
    print("</div>");

    print("</div>");

    print("<br />");

*/

    // First get some details of what the student thinks ...
    $table=$db->select("ScripVerify", "VerifyID='".$_SESSION['VerifyID']."'");
    foreach ($table as $row) {
        $namemissing=$row['NameMissing'];
        $addressmissing=$row['AddressMissing'];
        $addressincomplete=$row['AddressIncomplete'];
        $qualsmissing=$row['QualsMissing'];
        $addressnonuk=$row['AddressNonUK'];
        $sigincorrect=$row['SigIncorrect'];
        $sigmissing=$row['SigMissing'];
        $sigauth=$row['SigAuth'];
        $qualsinappropriate=$row['QualsInappropriate'];
    };

    $condition=array("RecordID"=>$_SESSION['studentrecordID']);
    switch ($_SESSION['programMode']){
        case "U": $table=$db->select("ScripStudentRecordHome", $condition); break;
        case "P": $table=$db->select("ScripStudentRecord", $condition); break;
        case "E": $table=$db->select("ScripStudentRecordExam", $condition); break;
    };
    foreach ($table as $row) { $datedone=$row['DateDone']; };

    // Set the background colour for (most of) the cells (buff) ...
    $cellbgcolor =  " style='background-color:#ffe4c4'";
    $cellbordercolor =  " style='border-color:#ffe4c4'";

    // This row is single cells and exists purely to define the widths of
    // the columns - hence the outlines of the cells are hidden (0px
    // borders) ... 
    print("<tr>");
    for ($i = 0; $i < 6; $i++) {
        print("<td style='border:0px max-height:0px' width='62'>&nbsp;</td>");
    };
    print("</tr>");

    // PART A: Supplier Details - SUPPLIER'S STAMP ...
    print("<tr>");
    print("<td valign='top' style='background-color:#000000'>");
    print("<font color='#ffffff'><b>Part A</b></font>");
    print("</td>");
    // Code for use if background is to be bisque too ...
    //    print("<td $cellbgcolor valign='top' colspan='5'>");
    //    print("<font color='#000000'><b>Supplier Details</b></font>");
    print("<td valign='top' colspan='5' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><b>Supplier Details</b></font>");
    print("</td>");
    print("</tr>");

    // PART A: Supplier Details - The stamp itself ...
    print("<tr>");
    print("<td $cellbgcolor valign='top' rowspan='4' colspan='2' width='124'>");
    print("Supplier's Stamp:<br /><br />");
    if ($NottStamp) {
        print("<center><img src='images/pharmacy.gif'><br />");
        print("<font color='#00008C'>$datedone</font></center>");
    };
    print("</td>");

    // PART A: Supplier Details - ACCOUNT NUMBER ...
    print("<td $cellbgcolor valign='top'>Account Number:</td>");
    print("<td $cellbgcolor colspan='3'>");
    if ($NottStamp) { print("<font color='#00008C'>123 456 789</font>"); };
    print("&nbsp;");
    print("</td>");
    print("</tr>");

    // PART A: Supplier Details - NAME OF BUSINESS ...
    print("<tr>");
    print("<td $cellbgcolor valign='top'>Name of business:</td>");
    print("<td $cellbgcolor colspan='3'>");
    if ($NottStamp) { print("<font color='#00008C'>Blogg's the Chemist</font>"); };
    print("&nbsp;");
    print("</td>");
    print("</tr>");

    // PART A: Supplier Details - ADDRESS ...
    print("<tr>");
    print("<td $cellbgcolor valign='top'>Address:</td>");
    print("<td $cellbgcolor colspan='3'>");
    if ($NottStamp) { print("<font color='#00008C'>Blogg Street</font>"); };
    print("&nbsp;");
    print("</td>");
    print("</tr>");

    // PART A: Supplier Details - ADDRESS ...
    print("<tr>");
    print("<td $cellbgcolor valign='top'>Address:</td>");
    print("<td $cellbgcolor colspan='3'>");
    if ($NottStamp) { print("<font color='#00008C'>Bloggville</font>"); };
    print("&nbsp;");
    print("</td>");
    print("</tr>");

    // PART B starts here: TITLE ...
    print("<tr>");
    print("<td valign='top' style='background-color:#000000'>");
    print("<font color='#ffffff'><b>Part B</b></font>");
    print("</td>");
    print("<td valign='top' colspan='5' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><b>Controlled Drugs Requisitioned</b></font>");
    print("</td>");
    print("</tr>");

    print("<tr $cellbgcolor>");
    print("<td valign='top' colspan='6'>");

    // PART B: HEADING for details ...
    // Put an invisible table here to control lay-out ...
    print("<table width='100%' $cellbordercolor $cellbgcolor>");

    print("<tr>");
    print("<td $cellbgcolor style='border:0px' width='50%'><center>Drug name + Form</center></td>");
    print("<td $cellbgcolor style='border:0px' width='25%'><center>Strength</center></td>");
    print("<td $cellbgcolor style='border:0px' width='25%'><center>Quantity</center></td>");
    print("</tr>");
    print("</table>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td valign='top' colspan='6'>");

    print("<table width='100%' $cellbgcolor>");
    // Now loop over all the drugs (and use an invisible table as above) ...
    for ($kk = 0; $kk < count($_SESSION['drugproblemID']); $kk++) {
        $kkPlus1 = $kk + 1;
        print("<tr>");
        print("<td $cellbgcolor style='border:0px' width='50%'><center>");
        ZZC_DoMedicationPanel($kkPlus1, "name");
        print("&nbsp;");
        ZZC_DoMedicationPanel($kkPlus1, "form"); 
        print("</center></td>");

        print("<td $cellbgcolor style='border:0px' width='25%'><center>");
        ZZC_DoMedicationPanel($kkPlus1, "strength");
        print("</center></td>");

        print("<td $cellbgcolor style='border:0px' width='25%'><center>");
        ZZC_DoMedicationPanel($kkPlus1, "quantity");
        print("</center></td>");
        print("</tr>");
    };
    print("</table>");     

    print("</td>");
    print("</tr>");

    // PART B - Last row: SIGNATURE and DATE ...
    // Get the date from the appropriate record table ...
    $condition=array("RecordID"=>$_SESSION['studentrecordID']);
    switch ($_SESSION['programMode']){
        case "U": $table=$db->select("ScripStudentRecordHome", $condition); break;
        case "P": $table=$db->select("ScripStudentRecord", $condition); break;
        case "E": $table=$db->select("ScripStudentRecordExam", $condition); break;
    };
    foreach ($table as $row) { $datedone=$row['DateDone']; };

    print("<tr>");
    print("<td $cellbgcolor valign='top' colspan='4'>");
    print("Signature of customer:");

    // Show signature if either it is NOT not missing
    // (i.e. not a deliberate error) or
    // it is a deliberate error but the user has spotted it ...
    if (!$_SESSION['prescribersigmissing'] || $sigmissing) {
        if ($_SESSION['prescribersigincorrect'] && !$sigincorrect) {
     	    $sigpath="sigs/".$_SESSION['incorrectSigFile'].".gif";
        } else {
            $sigpath="sigs/".$_SESSION['sigfile'].".gif";
        };
	print("<center><IMG SRC=$sigpath></center>");
    };
    // ... else do nothing i.e. omit

    print("</td>");
    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("Date of order:<br />");

    // Change date done to normal format ...
    // (LONG TERM - Store $dateDone as normal? But bear in mind that the TTO
    // carries out this correction too.)
    $explode = explode("-", $datedone);
    $temp = $explode[0]; $explode[0] = $explode[2]; $explode[2] = $temp;
    $datedoneAmended = implode("-", $explode);

    print("<center><font color='#00008c'>$datedoneAmended</font></center>");
    print("</td>");
    print("</tr>");

    // PART C - Title ...
    print("<tr>");
    print("<td valign='top' style='background-color:#000000'>");
    print("<font color='#ffffff'><b>Part C</b></font>");
    print("</td>");
    print("<td valign='top' colspan='5' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><b>Customer Details</b></font>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td $cellbgcolor valign='top' rowspan='7'>");
    print("* See overleaf (Part E, point 1c) for guidance on completion");
    print("</td>");

    // PART C - The actual information CODE, NAME, QUALIFICATIONS and ADDRESS ...
    // We now need the details of the customer (prescriber in Scripware terms).

    switch ($_SESSION['practicePOrU']){
        case 0: // Practice
        $organisationCode = $_SESSION['prescriberPrivateCD']; // Prescriber's PrivateCD number 
        $pctCode = $_SESSION['trustCode'];
        $practiceCode = $_SESSION['practiceCode'];
        break;

        case 1: // Unit
        $organisationCode = $_SESSION['prescriberPrivateCD']; // Prescriber's PrivateCD number 
        $pctCode = $_SESSION['trustCode'];
        $practiceCode = $_SESSION['practiceCode'];
        break;

        case 2: // Pharmacy
        $organisationCode = $_SESSION['practiceCode']; // Pharmacy code
        $pctCode = $_SESSION['trustCode']; // PCT code
        $practiceCode = "&nbsp;";
        break;
    };

    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("*Organisation Code:");
    print("</td>");
    print("<td $cellbgcolor valign='top' colspan='3'>");
    print("<font color='#00008C'>$organisationCode</font>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("*PCT Code:");
    print("</td>");
    print("<td $cellbgcolor valign='top' colspan='3'>");
    print("<font color='#00008C'>$pctCode</font>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("*Practice Code:");
    print("</td>");
    print("<td $cellbgcolor valign='top' colspan='3'>");
    print("<font color='#00008C'>$practiceCode</font>");
    print("&nbsp;");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("Individual's Name (Printed):");
    print("</td>");
    print("<td $cellbgcolor colspan='3'>");

// Sloppy
    // This piece of code has been copied from ZZE - ultimately
    // it should be made into a function and both places then call it ...
    if ($_SESSION['prescribernamemissing']) { // Meant to be missing
        if ($namemissing) { // User spotted it - red ink ...
            print("<font color='#ee0000'>".$_SESSION['prescribername']."</font>");
        } else { // User not spotted it - omit ...
            print("&nbsp;");
        };	
    } else { // Not meant to be missing - blue ink
        print("<font color='#00008C'>".$_SESSION['prescribername']."</font>");
    };

    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("Professional Qualification:");
    print("</td>");
    print("<td $cellbgcolor colspan='3'>");

    if ($_SESSION['qual']=='0') { $_SESSION['qual']=""; };
    if ($_SESSION['prescriberqualsmissing']) { // Meant to be missing
        if ($qualsmissing) { // User spotted it - red ink ...
            print("<font color='#EE0000'>".$_SESSION['qual']." </font>");
        } else { // User not spotted it - omit ...
            print("&nbsp;");
        };
    } else { // Not meant to be missing - blue ink
        print("<font color=#00008C>".$_SESSION['qual']."</font>");
    };

    print("</td>");
    print("</tr>");

    // Assume this to be line 1 of the practice address ...
    print("<tr>");
    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("Address:");
    print("</td>");
    print("<td $cellbgcolor valign='top' colspan='3'>");
    if ($_SESSION['prescriberaddressmissing']) {
        if ($addressmissing) { // Print in RED ...
            print("<font color=#ff0000>".$_SESSION['practiceAddress1']."</font>");
        } else { // Print nothing ...
            print("&nbsp;");
        };
    } else if ($_SESSION['prescribeaddressincomplete']) {
        // At present, we shall always omit the second line for an incomplete address; ...
        if ($addressincomplete) {
            print("<font color=#EE0000>".$_SESSION['practiceAddress1']."</font>");
        } else {
            print("<font color=#00008C>".$_SESSION['practiceAddress1']."</font>");
        };
    } else if ($_SESSION['prescriberaddressnonuk']) { // Non-UK showing
        if ($addressnonuk) { // Spotted - RED
            print("<font color=#ff0000>".$_SESSION['prescriberukaddress1']."</font>");
        } else { // Not spotted BLUE
            print("<font color=#00008C>".$_SESSION['practiceAddress1']."</font>");
        };
    } else {
        print("<font color=#00008C>".$_SESSION['practiceAddress1']."</font>");
    }; 
    print("</td>");
    print("</tr>");

    // Assume this to be line 2 of the practice address ...
    print("<tr>");
    print("<td $cellbgcolor valign='top' colspan='2'>");
    print("Address:");
    print("</td>");
    print("<td $cellbgcolor valign='top' colspan='3'>");
    if ($_SESSION['prescriberaddressmissing']) {
        if ($addressmissing) { // Print in RED ...
            print("<font color=#ff0000>".$_SESSION['practiceAddress2']."</font>");
        } else { // Print nothing ...
            print("&nbsp;");
        };
    } else if ($_SESSION['prescribeaddressincomplete']) {
        // At present, we shall always omit the second line for an incomplete address; ...
        if ($addressincomplete) {
            print("<font color=#EE0000>".$_SESSION['practiceAddress2']."</font>");
        } else {
            print("&nbsp;");
        };
    } else if ($_SESSION['prescriberaddressnonuk']) { // Non-UK showing
        if ($addressnonuk) { // Spotted - RED
            print("<font color=#ff0000>".$_SESSION['prescriberukaddress2']."</font>");
        } else { // Not spotted BLUE
            print("<font color=#00008C>".$_SESSION['practiceAddress2']."</font>");
        };
    } else {
        print("<font color=#00008C>".$_SESSION['practiceAddress2']."</font>");
    }; 
    print("</td>");
    print("</tr>");

/*
    // Row 4 - Button to turn over
    print("<tr>");
    print("<td colspan='6' style='border:0px'>");
    print("<form method='post' action=$php_self>");
    print("<input type='hidden' name='tab' value=".$_SESSION['oldtab'].">");
//    print("<input type='hidden' name='returntotab' value=$_SESSION['tab']>");
    print("<center><input type='submit' name='TurnOverTTO' value='Turn over'></center>");
    print("</form>");
    print("</td>");
    print("</tr>");
*/

    print("</table>\n");

    print("</div>");
};

function DY_PrivateCDRequisitionReverse() {
    global $case;
    print("<div class='tablewhite'>");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // This code is taken from ZZC_DoMedication
    // [It is there for historical reasons: it is not a drug-dependent
    // property (as its location suggests); it is a form-dependent
    // property.]
    // Find the correct purpose and whether it should be shown ...
    $condition = array("MiscProblemID"=>$_SESSION['miscproblemID']);
    $table=$db->select("ScripMiscProblem",$condition);
    foreach ($table as $row) {
        $_SESSION['purposeCategory']=$row['PurposeCategory']; // Purpose category
        $_SESSION['purpose']=$row['Purpose']; // Purpose
        $_SESSION['purposemarks']=$row['PurposeMarks']; // Error intended?
    };

    // We also need to know whether the user has spotted a missing purpose ...
    $condition=array("VerifyID"=>$_SESSION['VerifyID']);
    $table=$db->select("ScripVerify", $condition);
    foreach ($table as $row) {
        $purposeMS=$row['Purpose']; // User's thoughts
    };

    // Find the various categories ...
    $table=$db->select("ScripPurposeText");
    foreach ($table as $row) {
        $purposeText[] = $row['PurposeText']; // Purpose text
    };

    // Set the background colour for (most of) the cells ...
    $cellbgcolor =  " style='background-color:#ffe4c4'";
    $cellbordercolor =  " style='border-color:#ffe4c4'";

    // Start form ...
    print("<form method='post' action=$php_self>");

    print ("<table width='372' border='1'>\n");
    print("<tr>");
    print("<td $cellbgcolor style='border:0px' width='62'>&nbsp;</td>");    
    print("<td $cellbgcolor style='border:0px' width='62'>&nbsp;</td>");
    print("<td $cellbgcolor style='border:0px' width='62'>&nbsp;</td>");
    print("<td $cellbgcolor style='border:0px' width='62'>&nbsp;</td>");
    print("<td $cellbgcolor style='border:0px' width='62'>&nbsp;</td>");
    print("<td $cellbgcolor style='border:0px' width='62'>&nbsp;</td>");
    print("</tr>");

    // PART D
    print("<tr>");
    print("<td valign='top' style='background-color:#000000'>");
    print("<font color='#ffffff'><b>Part D</b></font>");
    print("<td valign='top' colspan='5' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><b>Purpose for which drugs are required</b></font>");
    print("</tr>");

    // Loop over each purpose ...
    for ($i = 0; $i < count($purposeText); $i++) {
        $iPlus1 = $i + 1;
        print("<tr>");
        print("<td $cellbgcolor align='right'>$iPlus1 &nbsp; &nbsp;");

        if ($_SESSION['purposeCategory'] == $iPlus1){
            if ($_SESSION['purposemarks']){ // An error is intended ...
                if ($purposeMS){ // ... and it has been spotted ...
                    // ... therefore red tick
                    print("<img src='images/tickRed.gif'>");
                } else { // ... but it hasn't been spotted ...
                    // ... therefore empty box
                    print("<img src='images/empty.gif'>");
                };
            } else { // No error intended ...
                // ... therefore blue tick
                print("<img src='images/tick.gif'>");
            };
        } else { // Wrong category ...
            // ... therefore empty box
            print("<img src='images/empty.gif'>");
        };
        print("</td>");

        print("<td $cellbgcolor colspan='5'>");
        print($purposeText[$i]);
        print("</td>");
        print("</tr>");
    };

    print("<tr>");
    print("<td $cellbgcolor>&nbsp;</td>");
    print("<td $cellbgcolor colspan='5'><font color='#00008C'>");
    // Only show the purpose if text exists and category is 6 ...
    if ($_SESSION['purpose'] && $_SESSION['purposeCategory'] == 6) {
        // ... but even then we still use the red or blue or no text logic ...
        if ($_SESSION['purposemarks']){ // An error is intended ...
            if ($purposeMS){ // ... and it has been spotted ...
                // ... therefore red text
                print("<font color='#ff0000'>".$_SESSION['purpose']."</font>");
            } else { // ... but it hasn't been spotted ...
                // ... therefore empty box
                print("&nbsp;");
            };
        } else { // No error intended ...
            // ... therefore blue tick
            print("<font color='#00008C'>".$_SESSION['purpose']."</font>");
        };
    } else {
        print("&nbsp;");
    };
    print("</font>");

    // INCLUDE AMENDMENT (if applicable) ...
    $condition = array ('ID'=>$_SESSION['AmendmentsID']);
    $table=$db->select("ScripAmendments", $condition);
    foreach ($table as $row) {
        $amend_purpose = $row['Purpose'];
    };
    $amend_purpose = trim(str_replace("|", "", $amend_purpose));
    print(" <font color='#007f00'>$amend_purpose</font>");

    print("</td>");
    print("</tr>");

    // PART E - Title ...
    print("<tr>");

    print("<td valign='top' style='background-color:#000000'>");
    print("<font color='#ffffff'><b>Part E</b></font>");
    print("<td valign='top' colspan='5' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><b>Notes on using/obtaining FP10CDF forms</b></font>");

    print("</tr>");
    print("<tr>");
    print("<td $cellbgcolor colspan='6'>The person raising the requisition (customer) <b>must</b> �<br>");
    print("<br />");
    print("a. Write the controlled drugs to be requisitioned (including form, strength + quantity) in Part B overleaf");
    print("<br />");
    print("b. Sign their name at the bottom of Part B overleaf. Signature must be hand-written in ink");
    print("<br />");
    print("c. Write their name, individual/organisation code*, occupation/professional qualification (e.g. GP, pharmacist), and address of premises that they are working out of in Part C overleaf");
    print("<br />");
    print("d. Complete Part D above, indicating the purpose for which the drug(s) is/are required");
    print("<br />");
    print("* The organisation code can either be the individual prescriber`s code, or the account code of the pharmacy raising the requisition. If an individual prescriber code is used which is not affiliated to one practice/PCT (e.g. Nurse Independent Supplementary Prescriber), then the relevant practice code and PCT code must also be included.");
    print("<br />");
    print("2. The person/organisation supplying the controlled drugs (supplier) should either:");
    print("a. Write their account submission code, name of organisation, and address in Part A overleaf");
    print("<br />");
    print("<b>OR</b>");
    print("<br />");
    print("b. Include a legible stamp in the top left section of Part A, confirming their details");
    print("<br />");
    print("c. Ensure that the customer has completed their relevant sections with correct data");
    print("<br />");
    print("<br />");
    print("The supplier <b>must</b> then submit all CD requisitions that they have processed to the PPD, using the FP34PCD form which should be downloaded from �");
    print("<br />");
    print("<u>http://www.ppa.org.uk/pdfs/FP34PCD.pdf</u>");
    print("<br />");
    print("3. Supplies of the FP10CDF form can be obtained from your allocated Primary Care Trust.");
    print("</td>");
    print("</tr>");

    // Row F
    print("<tr>");

    print("<td valign='top' style='background-color:#000000'>");
    print("<font color='#ffffff'><b>Part F</b></font>");
    print("<td valign='top' colspan='5' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><b>Data Protection Statement</b></font>");

    print("</tr>");
    print("<tr>");
    print("<td $cellbgcolor colspan='6'>This requisition will be passed to the NHS Business Services Authority (NHSBSA), a Special Health Authority in the National Health Service (NHS), for the purposes of statistical analysis of what has been supplied. The information may also be used within the NHS to prevent incorrect usage of controlled drugs, and may be disclosed to organisations outside the NHS that have a lawful entitlement to receive it. This requisition will be confidentially destroyed 24 months after the month in which it was received by the NHSBSA, unless it has been disclosed to another organisation.</td>");
    print("</tr>");

/*
    // Row Last - Button to turn over
    print("<tr>");
    print("<td style='border:0px' colspan='6'>");
    print("<form method='post' action=$php_self>");
    print("<input type='hidden' name='tab' value=".$_SESSION['oldtab'].">");
    // Centred button ...
    print("<center><input type='submit' name='TurnOverTTO' value='Turn over'></center>");
    print("</form>");
    print("</td>");
    print("</tr>");
*/

    print("</table>");
    print("\n");
    print("</div>");
};

function DZ_HospitalForm() {
    global $case;
    print("<div class=tablewhite>");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    if ($_SESSION['entirelyhandwritten']) {
        $typeface="face='Lucida Handwriting Italic, serif'";
    } else {
        $typeface="";
    };

    $condition=array("ID"=>$_SESSION['HospitalTTOPatient']);
    $table=$db->select("ScripHospitalTTOPatient", $condition);
    foreach ($table as $row) {
        $HospitalNumber = $row['HospitalNumber'];
        $TelephoneNumber = $row['TelephoneNumber'];
        $DateOfAdmission = $row['DateOfAdmission'];
        $Ward = $row['Ward'];
        $TelExt = $row['TelExt'];
        $DateOfDischarge = $row['DateOfDischarge'];
        $TransferredTo = $row['TransferredTo'];
        $Consultant = $row['Consultant'];
        $ConsultantCode = $row['ConsultantCode'];
        $LetterToGPDr = $row['LetterToGPDr'];
        $DischargeAddressContactNo = $row['DischargeAddressContactNo'];
        $PrimaryDiagnosis = $row['PrimaryDiagnosis'];
        $Problems = $row['Problems'];
        $Confidential = $row['Confidential'];
        $NurseInformation = $row['NurseInformation'];
        $IsNotRequired = $row['IsNotRequired'];
        $WillBePosted = $row['WillBePosted'];
        $NextDate = $row['NextDate'];
        $NextTime = $row['NextTime'];
        $NextClinic = $row['NextClinic'];
        $Transport = $row['Transport'];
        $DistrictNursingService = $row['DistrictNursingService'];
        $SocialServices = $row['SocialServices'];
        $Psychiatric = $row['Psychiatric'];
        $OtherText = $row['OtherText'];
    };

    // Calculate dates ...
    $today = time();
    $DateOfAdmission = date("d M y", $today - $DateOfAdmission * 24 * 60 * 60);
    $DateOfDischarge = date("d M y", $today - $DateOfDischarge * 24 * 60 * 60);
    $NextDate = date("l, d F Y", $today + $NextDate * 24 * 60 * 60);


// Possible later option ...
    // Set the GP name to prescriber if blank ...
//    if (!$LetterToGPDr) { $LetterToGPDr = $_SESSION['prescribername']; };

    print ("<table width='370' border='4'>\n");

    // Row 0 - Name of hospital etc.
    $wholeTrustAddress = $_SESSION['trustAddress1'];
    if ($_SESSION['trustAddress2']) { $wholeTrustAddress.= ", ".$_SESSION['trustAddress2']; };
    if ($_SESSION['trustAddress3']) { $wholeTrustAddress.= ", ".$_SESSION['trustAddress3']; };
    if ($_SESSION['trustAddress4']) { $wholeTrustAddress.= ", ".$_SESSION['trustAddress4']; };
    if ($_SESSION['trustPostCode']) { $wholeTrustAddress.= " ".$_SESSION['trustPostCode']; };

    print("<tr>");
    print("<td width='370' valign='top' style='background-color:#e0e0ff'>");
    print("<font color='#000000' size='+1'><center><b>".$_SESSION['trustName']."</b></center></font>");
    print("<font color='#000000'><center><b>$wholeTrustAddress</b></center></font>");
    print("<font color='#000000'><center><b>".$_SESSION['trustTelNo']."</b></center></font>");
    print("</td>");
    print("</tr>");

    // Row 1 - General Information - Title
    print("<tr>");
    print("<td width='370' valign='top' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><center><b>GENERAL INFORMATION</b></center></font>");
    print("</td>");
    print("</tr>");

    // Row 1 - General Information - Details
    print("<tr>");
    print("<td>");
    print("Title/Patient Name: ");
    ZZB_DoAddressPanel("name");
    print("<br />");
    print("Address: ");
    ZZB_DoAddressPanel("address");
    print("<br />");
    print("D.O.B. ");
    ZZA_DoAgePanel("dob");
    print("<br />");

    print("Hospital No: ");
    print("<font color='#00008C' $typeface>$HospitalNumber</font><br />");
    print("<br />");
    print("Telephone Number: ");
    print("<font color='#00008C' $typeface>$TelephoneNumber</font><br />");
    print("Date of Admission: ");
    print("<font color='#00008C' $typeface>$DateOfAdmission</font><br />");
    print("Ward: ");
    print("<font color='#00008C' $typeface>$Ward</font><br />");
    print("Tel. Ext: ");
    print("<font color='#00008C' $typeface>$TelExt</font><br />");
    print("Date of Discharge/Transfer: ");
    print("<font color='#00008C' $typeface>$DateOfDischarge</font><br />");
    print("Transferred to:"); 
    print("<font color='#00008C' $typeface>$TransferredTo</font><br />");
    print("Consultant (code): ");
    print("<font color='#00008C' $typeface>$Consultant ($ConsultantCode)</font><br />");
    print("Letter to GP/Dr: ");
    print("<font color='#00008C' $typeface>$LetterToGPDr</font><br />");
    print("Discharge Address/Contact No. (if different): ");
    print("<font color='#00008C' $typeface>$DischargeAddressContactNo</font><br />");
    print("<br /><br />");
    print("</td></tr>");

    // Row 2 - Patient Information
    print("<tr><td width='370' valign='top' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><center><b>PATIENT INFORMATION</b></center></font></td></tr>");

    print("<tr>");
    print("<td valign='top'>");
    print("Primary Diagnosis/Operative Procedures: <br />");
    print("<font color='#00008C' $typeface>$PrimaryDiagnosis</font><br />");
    print("<br />");

    print("Problems/Comments: <br>");
    print("<font color='#00008C' $typeface>$Problems</font><br />");
    print("<br />");

    print("Carers Strategy Info (Delete as appropriate) 918A/918F<br />");
    print("</td>");
    print("</tr>");

    // ROW 3 - Confidential GP Specific Information - TITLE
    print("<tr>");
    print("<td width='370' valign='top' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><center><b>CONFIDENTIAL GP SPECIFIC INFORMATION</b></center></font>");
    print("</td>");
    print("</tr>");

    // ROW 3 - Confidential GP Specific Information - DETAILS
    print("<tr><td>");
    print("<center>Will <i><b>NOT</b></i> appear on the patient's copy</center><br />");
    print("<font color='#00008C' $typeface>$Confidential</font><br />");
    print("</td></tr>");

    // ROW 4 - Special Instructions - TITLE
    print("<tr>");
    print("<td width='370' valign='top' style='background-color:#e0e0ff'>");
    print("<font color='#000000'><center><b>FOLLOW UP CARE REQUIRED</b></center></font>");
    print("</td>");
    print("</tr>");

    // ROW 4 - Special Instructions - DETAILS
    print("<tr><td>");
    print("District/Practice Nurse Information:<br /><br /><br />");
    print("<font color='#00008C' $typeface>$NurseInformation</font><br />");
    print("Please tick as appropriate<br />");
    print("An outpatient appointment:<br />");

    print("&nbsp;&nbsp;&nbsp;");
    if ($IsNotRequired) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" Is not required<br>");

    print("&nbsp;&nbsp;&nbsp;");
    if ($WillBePosted) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" Will be posted to you<br>");

    print("&nbsp;&nbsp;&nbsp;");
    if ($NextDate || $NextTime || $NextClinic) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" Has been made<br />");

    for ($jj = 0; $jj < 13; $jj++) { print("&nbsp;"); };
    print ("Date: <font color='#00008C' $typeface>$NextDate</font><br />");
    for ($jj = 0; $jj < 13; $jj++) { print("&nbsp;"); };
    print ("Time: <font color='#00008C' $typeface>$NextTime</font><br />");
    for ($jj = 0; $jj < 13; $jj++) { print("&nbsp;"); };
    print ("Clinic: <font color='#00008C' $typeface>$NextClinic</font><br />");

    print("&nbsp;&nbsp;&nbsp;");
    if ($Transport) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" Your transport to the clinic has been arranged<br /><br />");

    print("<b>The following services have been asked to visit you:</b><br />");

    print("&nbsp;&nbsp;&nbsp;");
    if ($DistrictNursingService) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" District Nursing Service<br />");

    print("&nbsp;&nbsp;&nbsp;");
    if ($SocialServices) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" Social Services<br />");

    print("&nbsp;&nbsp;&nbsp;");
    if ($Psychiatric) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" Community Psychiatric Nursing Services<br />");

    print("&nbsp;&nbsp;&nbsp;");
    if ($OtherText) {
        print("<img src='images/tick.gif'>");
    } else {
        print("<img src='images/empty.gif'>");
    };
    print(" Other (Please specify) ");
    print("<font color='#00008C' $typeface>$OtherText</font><br />");

    print("</td>");
    print("</tr>");

/*
    // Row 4 - Button to turn over
    print("<tr><td style='border:0px'>");
    print("<form method='post' action=$php_self>");
    print("<input type='hidden' name='tab' value=".$_SESSION['oldtab'].">");
    print("<center><input type='submit' name='TurnOverTTO' value='Turn over'></center>");
    print("</form>");
    print("</td></tr>");
*/

    print("</table>\n");
    print("</div>");
};

function DZ_HospitalFormReverse() {
    global $case;
    print("<div class='tablewhite'>");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    if ($_SESSION['entirelyhandwritten']) {
        $typeface="face='Lucida Handwriting Italic, serif'";
    } else {
        $typeface="";
    };

    // Read the prescriber problem details ...
    ZE_GetPrescriberProblemDetails();
    ZF_GetPrescriberDetails();

    $condition=array ('EndorseID'=>$_SESSION['EndorseID']);
    $table=$db->select("ScripEndorse", $condition);
    foreach ($table as $row) {
        $NottStamp=$row['NottStamp'];
        $PORStamp[]=$row['PORStamp1'];
        $PORStamp[]=$row['PORStamp2'];
        $PackStamp[]=$row['PackStamp1'];
        $PackStamp[]=$row['PackStamp2'];
    };

    $condition=array ('StudentID'=>$_SESSION['studentID']);
    $table=$db->select("ScripStudent", $condition);
    foreach ($table as $row) {
        $initials=$row['initials'];
    };

    $condition=array("RecordID"=>$_SESSION['studentrecordID']);
    switch ($_SESSION['programMode']){
        case "U":
        $table=$db->select("ScripStudentRecordHome", $condition);
        break;

        case "P":
        $table=$db->select("ScripStudentRecord", $condition);
        break;

        case "E":
        $table=$db->select("ScripStudentRecordExam", $condition);
        break;
    };
    foreach ($table as $row) {
	$datedone=$row['DateDone'];
    };

    $nDrugsOnForm = 3;

    print("<table width='370' border='0'>");

    for ($k = 0; $k < $nDrugsOnForm; $k++){

        // If $k = 0, then we use DrugProblemID from ScripsScrips to find HospitalTTODetails
        // If $k = 1, then we use DrugProblemID2 from ScripsScrips to find HospitalTTODetails
        // If $k = 2, then we do nothing - there is no third drug

        if ($_SESSION['drugproblemID'][$k]) {
            $condition=array("DrugProblemID"=>$_SESSION['drugproblemID'][$k]);
            $table=$db->select("ScripDrugProblem", $condition);
            foreach ($table as $row) {
                $HospitalTTODetails=$row['HospitalTTODetails'];
            };

            $condition=array("ID"=>$HospitalTTODetails);
            $table=$db->select("ScripHospitalTTODetails", $condition);
            foreach ($table as $row) {
                $specialInstructions=$row['SpecialInstructions'];
                $time8=$row['Time8'];
                $time10=$row['Time10'];
                $time13=$row['Time13'];
                $time18=$row['Time18'];
                $time22=$row['Time22'];
                $obtainMore=$row['ObtainMore'];
                $stop=$row['Stop'];
                $indication=$row['Indication'];
            };
        } else {
            // Otherwise clear the variables so that the relevant form will be blank
            $specialInstructions = "";
            $time8 = "";
            $time10 = "";
            $time13 = "";
            $time18 = "";
            $time22 = "";
            $obtainMore = "";
            $stop = "";
            $indication = "";
        };

        // Row 0 - Drug name
        print("<tr><td colspan='6' style='background-color:#e0e0ff'>");
        print("<font color='#000000'><b>Item ".($k+1)."</b>");
        print("</td></tr>"); 

        // Row 1 - Name; Dose and Special Instructions
        print("<tr><td colspan='2' valign='top'>Name of Medication<br>");
        print("<font color='#000080'>");
        if ($_SESSION['drugproblemID'][$k]) {
            // N.B. The function ZZC expects 1, 2, ...
            ZZC_DoMedicationPanel($k+1, "name");
            print("&nbsp;");
            ZZC_DoMedicationPanel($k+1, "strength");
            print("&nbsp;");
            ZZC_DoMedicationPanel($k+1, "form");
        } else {
            print("&nbsp;");
        }; 
        print("</font></td>");

        print("<td colspan='2' valign='top'>Dose to be Taken<br>");
        print("<font color='#000080' $typeface>");
        if ($_SESSION['drugproblemID'][$k]) { ZZC_DoMedicationPanel($k+1, "dose"); }; // The function ZZC expects 1, 2, ...
        print("</font></td>");

        print("<td colspan='2' valign='top'>Special Instructions<br>");
        print("<font color='#00008C' $typeface>");
        print("$specialInstructions");
        print("</font>");
        print("</td></tr>");

        // Row 2 - When to take and Number of Days ...
        print("<tr>");
        print("<td colspan='5' valign='top' align='center'>When to take/use medication</td>");

        print("<td width='20%' rowspan='2' valign='top'>No of days supplied<br>");
        print("<font color='#000080' $typeface>");
        if ($_SESSION['nosdays'][$k] == 0) { $numberOfDays=""; } else { $numberOfDays=$_SESSION['nosdays'][$k]; };
        print($numberOfDays);
        print("</font></td>");
        print("</tr>");

        // Row 3 - The times ...
        print("<tr>");
        print("<td width='16%' valign='top'>8am");
        if ($time8) {
            print("<img src='images/tick.gif'>");
        } else {
            print("<img src='images/empty.gif'>");
        };
        print("</td>");

        print("<td width='16%' valign='top'>10am");
        if ($time10) {
            print("<img src='images/tick.gif'>");
        } else {
            print("<img src='images/empty.gif'>");
        };
        print("</td>");

        print("<td width='16%' valign='top'>1pm");
        if ($time13) {
            print("<img src='images/tick.gif'>");
        } else {
            print("<img src='images/empty.gif'>");
        };
        print("</td>");

        print("<td width='16%' valign='top'>6pm");

        if ($time18) {
            print("<img src='images/tick.gif'>");
        } else {
            print("<img src='images/empty.gif'>");
        };
        print("</td>");

        print("<td width='16%' valign='top'>10pm");
        if ($time22) {
            print("<img src='images/tick.gif'>");
        } else {
            print("<img src='images/empty.gif'>");
        };
        print("</td>");
        print("</tr>");

        // Row 4 - Obtain More, Reason for Medication and Pharmacy Use
        print("<tr>");
        print("<td colspan='2' valign='top'>Obtain more from GP before supply runs out");
        print("<input type='checkbox'");
        if ($obtainMore) { print(" checked"); };
        print("></td>");

        print("<td colspan='3' rowspan='2' valign='top'>Reason for Medication<br>");

        print("<font color='#00008C' $typeface>$indication</font></td>");

        print("<td valign='top' rowspan='2' >Pharmacy Use<br />");

        print("<table>");
        print("<tr><td style='border-color:#00008C'><font color='#00008C'>");

        // Adjust $datedone ...
        $explode = explode ("-", $datedone);
        $temp = $explode[0]; $explode[0] = $explode[2]; $explode[2] = $temp;#
        $explode[2] = substr($explode[2], 2);
        $amendedDate = implode("-", $explode);

        if ($_SESSION['drugproblemID'][$k] && $NottStamp) { print("$amendedDate $initials<br />"); };
        if ($PORStamp[$k]) { print("POR<br />"); };
        if ($PackStamp[$k]) { print("Pack"); };
        print("</font></td></tr>");
        print("</table>");

        print("</td></tr>");

        // Row 5 - Stop ...
        print("<tr>");
        print("<td colspan='2' valign='top'>Stop when supply runs out<input type='checkbox'");
        if ($stop) { print(" checked"); };
        print("></td></tr>");
    };

    // Signature and Date
    print("<tr>");
    print("<td colspan='4'>Signature of Doctor");

    ZZF_DoSignature(0);

//    print("<img src='sigs/$SignatureFile.gif'>");
    print("</td>");

    print("<td colspan='2' valign='top'>Date<br />");
    ZZD_DoDatePanel(0);
    print("</td>");
    print("</tr>");

    // Name of Doctor
    print("<tr>");
    print("<td colspan='6'>Name of Doctor: ");

    if ($_SESSION['prescribernamemissing']) { // Meant to be missing
        if ($namemissing) { // User spotted it - red ink ...
            print("<font color='#ee0000' $typeface>".$_SESSION['prescribername']."</font>");
        } else { // User not spotted it - omit ...
            print("&nbsp;");
        };	
    } else { // Not meant to be missing - blue ink
        print("<font color='#00008C' $typeface>".$_SESSION['prescribername']."</font>");
    };




//    print("<font color='#00008C' $typeface>$Doctor</font?");
    print("</td>");
    print("</tr>");

/*
    // Row Last - Button to turn over
    print("<tr>");
    print("<td colspan='6' style='border:0px'>");
    print("<form method='post' action=$php_self>");
    print("<input type='hidden' name='tab' value=".$_SESSION['oldtab'].">");
    print("<center><input type='submit' name='TurnOverTTO' value='Turn over'></center>");
    print("</form>");
    print("</td>");
    print("</tr>");
*/

    print("</table>");
    print("\n");
    print("</div>");
};






function DA_displayrequisition() {
    global $case;
    print("<div class = tablewhite>");
    print ("<TABLE ALIGN=LEFT BGCOLOR=#".$_SESSION['formcolour']);
    print(" WIDTH=370>\n");

    // If vet display top line
    if ($_SESSION['formname']=="Requisition-Vet") {
     	print("<TR><TD><IMG SRC=images/vetdog.gif></TD>\n");
	print("<TD><H3>Veterinary Surgeon</H3></TD></TR>\n");
    };

   // Row 1 of prescription
    print("<TR><TD WIDTH=150>");ZZG_DoPharmacyPanel();
    print("</TD><TD>");
    ZZE_DoPrescriberAddress(0);
    ZZD_DoDatePanel(0);
    print("</TD></TR><TR><TD colspan=2>");
    print("<P>Please supply</P>");
    ZZC_DoMedicationPanel(1, "");
    ZZC_DoMedicationPanel(2, "");
    ZZG_DoEndorsePanel();
    ZZF_DoSignature(0);ZZD_DoDatePanel(0);
    print("</TD></TR></TABLE>\n");
};

function DB_displayprivate() {
// Because of a programming infelicity, this section refers to BOTH
// private and emergency ... they have to be separated

    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition=array("FormID"=>$_SESSION['formID']);
    $table=$db->select("ScripForms",$condition);
    foreach ($table as $row) {
     	$formname2=strtolower($row['FormName2']);
    };

    // Private first (emergency after the ELSE)
//    if ($_SESSION['formID']=='9' || $_SESSION['formID']=='12' || $_SESSION['formID']=='13') {
    if ($formname2 == "private") {
        // Private ...
        print("<div class = tablewhite>");
        print ("<TABLE ALIGN=LEFT BGCOLOR=#".$_SESSION['formcolour']);
        print(" WIDTH=370>\n");

        // Row 1 of prescription
        print("<TR><TD WIDTH=200>");
        ZZG_DoPharmacyPanel();
        print("</TD><TD>");
        ZZE_DoPrescriberAddress(0);
        print("</TD></TR><TR><TD COLSPAN=2>\n");
        ZZB_DoAddressPanel(0);
        ZZC_DoMedicationPanel(1, "");
        ZZC_DoMedicationPanel(2, "");
        ZZG_DoEndorsePanel();
        ZZF_DoSignature(0);
        ZZD_DoDatePanel(0);
        print("</TD></TR></TABLE>\n");
//    } else if ($_SESSION['formID']=='1' || $_SESSION['formID']=='19') {
    } else if ($formname2 == "emergency supply") {
        // Both types of emergency ...
        print("<div class = tablepink>");
        print("<TABLE  ALIGN=LEFT BGCOLOR=#".$_SESSION['formcolour']);
        print(" WIDTH=350>\n");
        ZA_GetScripTable();

        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
        $condition=array('OTCID'=>$_SESSION['miscproblemID']);
        $table=$db->select("ScripOTCText",$condition);
        foreach ($table as $row) {
            $otctext=$row['OTCText'];
        };

        print ("<TR><TD height=305 pixels FONT color=#00008C ");
        // Choose the correct background ...
        print("background=images/");
        if ($_SESSION['formID']=='1') {
            print("emergencydoc");
        } else {
            print("emergencypatient");
        };
        print(".jpg>");    

        print("<p><br><br><br><br>$otctext<br><br><br><br></p>");
        print("\n</table></div>\n");
    } else {
        print("Error");
    };
};

function DC_displaystandardscrip($spectext) {
    global $case;
    global $_POST;
    global $_GET;
    print("<div class = standardform>");
    print ("\n\n<table border='1' align='left' ");

    if ($_SESSION['group'] != '8') {
        print("bgcolor=#".$_SESSION['formcolour']);
    } else {
        print("background='images/PrivateCD.gif'");
    };
    print(" width=390>\n");

    // This used to be controlled by $case->
    // reversescrip. For some reason (unexplained),
    // this has broken - therefore bring the variable
    // as a part of GET_VARS ...
    $_SESSION['reversescrip'] = $_GET['s'];

    if ($_SESSION['reversescrip']==1 && $_SESSION['oldtab']==30) {
        DCB_displayreverse();
    } else {
        DCA_displayfront($spectext);
    }; 
    print ("</table>\n\n");
};

function DCA_displayfront($spectext) {
    global $case;

    // Search for "Hospital" in $spectext
    $hospital = strtoupper("hospital");
    $location = strpos(strtoupper($spectext), $hospital);
    if ($location === false) {
        $hospitalForm = 0;
    } else {
        $hospitalForm = 1;
    };

// $hospitalForm = 1;
    $new2006Form = 1; // Use the 2006 form from now on ...

    if ($new2006Form) {
        // Row 1 of prescription; Cell: PHARMACY STAMP
        print ("<tr>");
        print("<td colspan='3' width='30%'");
        print("height='101' valign='top'>");
        print("Pharmacy Stamp<br>");
        ZZG_DoPharmacyPanel();
        print("</td>\n\n");

        // Row 1 of prescription (continued); Cell: AGE
        print ("<td colspan='2' height='101' ");
        print("valign='top' width='19%'>");

/* Magnifier OFF - this is HARD-WIRED ...
        print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=11\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
        print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/
        ZZA_DoAgePanel(0);
        print("</td>\n\n");

        // Row 1 of prescription (continued);
        //        NAME, ADDRESS AND DETAILS
        print ("<td colspan='4' rowspan='2' valign='top' width='51%'>");
        print ("Title, Forename, Surname &amp; Address");

/* Magnifier OFF - this is HARD-WIRED ...
        print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=12\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
        print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/

        ZZB_DoAddressPanel(""); 
        print("</td></tr>\n\n");

    } else {
        // Row 1 of prescription; Cell: Pharmacy stamp
        print ("<TR><TD colspan=3 width=116 pixels ");
        print("height=75 pixels VALIGN=TOP>");
        print("<FONT SIZE=0>Pharmacy Stamp</FONT><BR>");
        ZZG_DoPharmacyPanel();
        print("</td>");

        // Row 1 of prescription (continued); Cell: Age
        print ("<TD colspan=2 width=80 pixels>");

/* Magnifier OFF - this is HARD-WIRED ...
        print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=11\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
        print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/

        ZZA_DoAgePanel(0);
        print("</TD>\n");

        // Row 1 of prescription (continued); Cell: Title, etc.
        print ("<TD colspan=3 rowspan=2 VALIGN=TOP width=200 pixels>");
        print("<FONT SIZE=1>");
        print ("Title, Forename, Surname &amp; Address<FONT>");

/* Magnifier OFF - this is HARD-WIRED ...
        print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=12\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
        print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/

    ZZB_DoAddressPanel(""); 
    print("</TD></TR>\n");

    };

    if ($new2006Form) {
        // Row 2 of prescription; Cell: "NUMBER OF DAYS"
        print("<tr><td colspan='4' height='20' width='40%'>");
        print("Number of days' treatment<br>");
        print("N.B. Ensure dose stated");
        print("</td>\n");

        // Row 2 of prescription; Cell: THE NUMBER OF DAYS 
        print("<td width='9%' height='20' width='9%'>");
        $nosofdays=$_SESSION['nosdays'][0];
        if ($nosofdays < $_SESSION['nosdays'][1]) { $nosofdays=$_SESSION['nosdays'][1]; };
        if ($nosofdays>0) { print $nosofdays;} else { print "&nbsp;"; };
        print("</td>\n</tr>\n\n");
    } else {
        // Row 2 of prescription; Cell: No of days, etc.
        print ("<TR><TD colspan=4 width=160><FONT SIZE=1>");
        print ("No of days treatment<BR>N.B. Ensure dose stated");
        print("<FONT></TD>\n");

        // Row 2 of prescription; Cell: No of days, etc.
        print("<TD>");
        $nosofdays=$_SESSION['nosdays'][0];
        if ($nosofdays < $_SESSION['nosdays'][1]) { $nosofdays=$_SESSION['nosdays'][1]; };
        if ($nosofdays>0) { print $nosofdays;} else { print "&nbsp;"; };
        print("</TD>\n</TR>\n");
    };

    if ($hospitalForm) {
         // Hospital form AND new - no such thing as an old hospital form 
        // A hospital form has an extra line ...

        // NEW Row 3 of prescription; Cell: ENDORSEMENTS
        print("<tr>\n");
        print("<td width='20%' colspan='2' rowspan='2' valign='top'>");
        print("Endorsements<br>");
        ZZG_DoEndorsePanel();
        print("</td>\n\n");

        // NEW Row 3 of prescription (continued); Cell: MEDICATION
        print("<td height='228' colspan='5' valign='top' width='63%'>");
        if (strlen($spectext)>1) {
            print("<p align='right'><font color='#00008C'>");
            print("$spectext");
            print("</font></p>");
        };

/* Magnifier OFF - this is HARD=WIRED ...
        print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=1\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,width=500,height=350')\">");
        print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/

        ZZC_DoMedicationPanel(1, "");

        if ($_SESSION['drugproblemID'][1]) {
/* Magnifier OFF - this is HARD=WIRED ...
            print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=2\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
            print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/
            ZZC_DoMedicationPanel(2, "");
        };
        print("</td>\n\n");

// /* Magnifier OFF - this is HARD=WIRED ...
//    print("<A HREF=javascript:void(0)\n");
//print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=1\'');
//print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,width=500,height=350')\">");
//    print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
//*/

//    ZZC_DoMedicationPanel(1, "");

//    if ($_SESSION['drugproblemID'][1]) {
///* Magnifier OFF - this is HARD=WIRED ...
//        print("<A HREF=javascript:void(0)\n");
//print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=2\'');
//print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
//	print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
//*/
//	ZZC_DoMedicationPanel(2, "");
//    };
//    print("</TD>\n");

        // NEW Row 3 of prescription (continued); Cell: WHITE SPACE
        print("<td width='14%' rowspan='3' width='14%' ");
        print("bgcolor='#ffffff' valign='top'>");
        ZZM_DoTwoLetters();
        ZZM_DoBarCode();
        print("</td>\n\n");

        // NEW Row 3 of prescription (continued); Cell: The coloured bar
        print("<td width='3%' rowspan='3'>&nbsp;</td>\n\n");

        print("</tr>\n\n");

        // Row 3A of prescription (continued); Cell: HOSPITAL ONLY
        print("<tr>\n");
        print("<td height='28' colspan='5' width='63%'>");
        ZZL_DoHospitalSignature();
        print("</td>\n");
        print("</tr>\n\n");

    } else { // Non-hospital ...
        if ($new2006Form){ // New ...
            // A non-hospital form does not have an extra line ...

            // NEW Row 3 of prescription (non-hospital); Cell: ENDORSEMENTS
            print("<tr>");
            print("<td colspan='2' width='20%' ");
            print("valign=top>"); 
            print("Endorsements<br>");
            ZZG_DoEndorsePanel();
            print("</td>\n\n");

            // NEW Row 3 of prescription (non-hospital) (continued);
            //                      Cell: MEDICATION
            print("<td height='263' colspan='5' valign='top' width='63%'>");
            if (strlen($spectext)>1) {
                print("<p>$spectext</p>");
            };

/* Magnifier OFF - this is HARD=WIRED ...
            print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=1\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,width=500,height=350')\">");
            print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/

            ZZC_DoMedicationPanel(1, "");
            if ($_SESSION['drugproblemID'][1]) {

/* Magnifier OFF - this is HARD=WIRED ...
                print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=2\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
                print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/
               ZZC_DoMedicationPanel(2, "");
            };
            print("</td>\n");

            // NEW Row 3 of prescription (non-hospital) (continued);
            //        Cell: WHITE SPACE
            print("<td width='14%' ");
            print("rowspan='2' bgcolor='#FFFFFF' valign='top'>");
            ZZM_DoTwoLetters();
            ZZM_DoBarCode();
            print("</td>\n\n");

            // NEW Row 3 of prescription (non-hospital) (continued);
            //        Cell: COLOURED BAR
            print("<td width='3%' rowspan='2' width='3%'>");
            print("&nbsp;</td></tr>\n\n"); 

        } else { // Non-hospital : old
            // A non-hospital form does not have an extra line ...

            // OLD Row 3 of prescription; Cell: Endorsements
            print("<tr>");
            print("<TD colspan=2 height=247 width=80 ");
            print("valign=top><FONT SIZE=1>"); 
            print("<FONT SIZE=1>Endorsements</FONT><BR>");
            ZZG_DoEndorsePanel();
            print("</td>");

            // OLD Row 3 of prescription (continued); Cell: Medication
            print("<TD colspan=5 valign=top width=250>");
            if (strlen($spectext)>1) {
                print("<p>$spectext</p>");
            };

/* Magnifier OFF - this is HARD=WIRED ...
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=1\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,width=500,height=350')\">");
    print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/

        ZZC_DoMedicationPanel(1, "");
        if ($_SESSION['drugproblemID'][1]) {

/* Magnifier OFF - this is HARD=WIRED ...
            print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/ScripWare.php?tab=13&index=2\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=0,width=400,height=350')\">");
            print("<IMG SRC=images/mglass.jpg ALIGN=RIGHT></A>");
*/
            ZZC_DoMedicationPanel(2, "");
        };
        print("</td>");

    }; // End of $hospitalForm if-then-else block

    };

    if ($new2006Form) {
        // NEW Row 4 of prescription; Cell: SIGNATURE
        print ("<tr><td colspan='6' height='38' width='58%'>");
        ZZF_DoSignature(1);
        print("</td>\n\n");

        // NEW Row 4 of prescription (continued); Cell: DATE
        print("<td width='25%' height='38' ");
        print("valign='top' width='25%'>");
        ZZD_DoDatePanel(1);
        print("</td>\n");
        print("</tr>\n\n");
    } else {
        // OLD Row 4 of prescription; Cell: Signature
        print ("<TR><TD colspan=6 width=220>");
        ZZF_DoSignature(1);
        print("</TD>\n");

        // OLD Row 4 of prescription (continued); Cell: Date
        print("<TD>");
        ZZD_DoDatePanel(1);
        print("</TD></TR>\n");
    };

    if ($new2006Form) {
        // NEW Row 5 of Prescription; Cell: NO OF PRESCNS 
        print("<tr><td valign='top' width='15%' height='93' rowspan=2>");
//        print("<FONT SIZE=1>");
        print("For<br>dispenser<br>No.of<br>Prescns.<br>on form");
        print("<table><tr><td>");
        print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        print("&nbsp;&nbsp;<br>&nbsp;");
        print("</td></tr></table>");

        // This will require an IF statement to remove for private forms ...
        print("<table><tr><td bgcolor='#0000ff'>");
        if ($_SESSION['group'] != '8') { // NHS does not occur on Private CD forms
            print("<font color='#ffffff'><i><b>NHS</b></i></font>");
        };
        print("</td></tr></table>");
//        print("</FONT>");
        print("</td>\n\n");

        // NEW Row 5 of Prescription; Cell: PRESCRIBER DETAILS 
        print("<td colspan='6' height='108' ");
        print("bgcolor='#FFFFFF' width='68%'>\n");
        ZZE_DoPrescriberAddress(1);
        print("</td>\n\n");

        // Row 5 of Prescription (continued); Cell: "Reverse" 
        print("<td colspan='2' height='108' width='17%' ");
        print("valign='top' rowspan='2'>");
        ZZM_DoTwoLetters();
//        if ($_SESSION['addict']>0 && $_SESSION['oldtab']==30) {
//         	$_SESSION['reversescrip']=1;
//            print("<A HREF=ScripWare.php?tab=30&n=".$_SESSION['formname']);
//            print("&c=".$_SESSION['formcolour']."&i=".$_SESSION['i']."$i&a=".$_SESSION['addict'].">");
//            print("See reverse");
//            print("</A>");
//        };
        print("</td></td>\n\n");
    } else {
        // Row 5 of Prescription; Cell: No of Prescns 
        print("<TR><TD VALIGN=TOP WIDTH=30 pixels><FONT SIZE=1>");
        print("For<BR>dispenser.<BR>No.of<BR>Prescns.<BR>on form</TD>");
        print("</FONT>");

        // Row 5 of Prescription; Cell: Prescriber address 
        print("<TD colspan=6>\n");
        ZZE_DoPrescriberAddress(1);
        print("</TD>");

        // Row 5 of Prescription (continued); Cell: "Reverse" 
        print("<TD width=60>");
        if ($_SESSION['addict']>0 && $_SESSION['oldtab']==30) {
            $_SESSION['reversescrip']=1;
            print("<A HREF=ScripWare.php?tab=30&n=".$_SESSION['formname']);
	    print("&c=".$_SESSION['formcolour']."&i=".$_SESSION['i']."$i&a=".$_SESSION['addict'].">");
//	    print("See reverse");
            print("</a>");
        };
        print("</td></tr>\n");
    };

    if ($new2006Form) {
        print("<tr>");
        // NEW Row 5A of Prescription; Cell: PRESCRIBER DETAILS 
        print("<td colspan='6' height='15' ");
        print("bgcolor='#FFFFFF' width='68%'>\n");

        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
        $condition = array('FormID'=>$_SESSION['formID']);
        $table=$db->select("ScripForms", $condition);
        foreach($table as $row){
            $inTheBox = $row['FormName2'];
        };



        print("<p align='right'><font color='#00008C'>");
        print($inTheBox."0406");
        print("</font></p>");
        print("</td>\n\n");
        print("</tr>");
    };

    if ($new2006Form) {
        // Row 6 of prescription; Cell: Twelve digit number
        print("<tr>");
        print("<td height='22' colspan='9' width='100%' bgcolor='#ffffff'>");

        $printedSerialNumber = rand(100000,999999);
        print("<font color='#00008C'>");
        print($printedSerialNumber.$printedSerialNumber);
        print("</font>");
        print("</td>");
        print("</tr>\n\n");
    } else {
        // Row 6 of prescription; Cell: NHS 
        print("<TR><TD ALIGN=RIGHT BGCOLOR=#0000AA WIDTH=10>");
        print("<FONT COLOR=#FFFFFF><EM>NHS</EM></FONT></TD>");

        // Row 6 of prescription (continued); Cell: Please read ... 
        print("<TD ALIGN=RIGHT BGCOLOR=#0000AA colspan=7>");
        print("<FONT COLOR=#FFFFFF SIZE='2'>");
        print("PATIENTS - please read the notes overleaf</FONT></TD>");
    };


/*

This is the code generated by Dreamweaver for the new forms
- it includes an extra column
<table width="390" border="1">
  <tr>
    <td height="101" colspan="3">&nbsp;</td>
    <td height="101" colspan="2">&nbsp;</td>
    <td width="9%" colspan="4" rowspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td height="20" colspan="4">&nbsp;</td>
    <td width="9%" height="20">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" rowspan="2">&nbsp;</td>
    <td height="228" colspan="5">&nbsp;</td>
    <td width="14%" rowspan="3">&nbsp;</td>
    <td width="3%" rowspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td height="28" colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" colspan="6">&nbsp;</td>
    <td width="25%" height="38">&nbsp;</td>
  </tr>
  <tr>
    <td width="15%" height="108">&nbsp;</td>
    <td height="108" colspan="6">&nbsp;</td>
    <td height="108" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td height="22" colspan="9">&nbsp;</td>
  </tr>
</table>

*/



};

function DCB_displayreverse() {
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition=array('RegisterID'=>$_SESSION['RegisterID']);
    $table=$db->select("ScripRegister",$condition);
    foreach($table as $row) {
        $CDRegisterID=$row['CDRegisterID'];
    };

    // If doesn't exist ...
    if (!$CDRegisterID) {
        // Create an entry ...
	$db->insert("ScripCDRegister");
	$CDRegisterID=$db->getID();
	$text=array("CDRegisterID"=>$CDRegisterID);
        $condition=array('RegisterID'=>$_SESSION['RegisterID']);
        $db->update("ScripRegister",$text,$condition);
    };

    $condition=array('ID'=>$CDRegisterID);
    $table=$db->select("ScripCDRegister",$condition);
    foreach($table as $row) {
        $uDate=$row['ReverseDate'];
     	$item=$row['Item'];
    	$qty=$row['Qty'];
        $uInitials=$row['Initials'];
    };

    print("<FORM ACTION=ScripWare.php?tab=38 METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=cdregisterid VALUE=$CDRegisterID>");
    print("<TR>");
    print("<TH COLSPAN=4><font color=00008C>");
    print("Details of items supplied</font></TH>");
    print("</TR>\n");

    print("<TR>");
    print("<TH><font color=00008C>Date</font></TH>");
    print("<TH><font color=00008C>Item</font></TH>");
    print("<TH><font color=00008C>Quantity<BR>supplied</font></TH>");
    print("<TH><font color=00008C>Pharmacist's Initials</font></TH>");
    print("</TR>\n");
    $dofwk=date("w",time());
    if ($dofwk==0) $dofwk=6;
    // Start at 1 (Monday) not 0 (Sunday) ...
    for ($i=1;$i<10;$i++) {
        if ($i<$dofwk) {
	    print("<TR><TD BGCOLOR=#AAAAAA>&nbsp;</TD>");
 	    print("<TD BGCOLOR=#AAAAAA>&nbsp;</TD>");
	    print("<TD BGCOLOR=#AAAAAA>&nbsp;</TD>");   
	    print("<TD BGCOLOR=#AAAAAA>&nbsp;</TD></TR>");
  	}
	else if ($i==$dofwk) {
	    $today=date("d-m-y",time());
//	    print("<TR><TD>$today</TD>");
	    print("<TD><INPUT TYPE=TEXT NAME=uDate VALUE='$uDate'></TD>");
	    print("<TD><INPUT TYPE=TEXT NAME=item VALUE='$item'></TD>");
	    print("<TD><INPUT TYPE=TEXT NAME=qty VALUE='$qty'></TD>");
	    print("<TD><INPUT TYPE=TEXT NAME=uInitials VALUE='$uInitials'></TD>");
//	    print("<TD>".$_SESSION['initials']."</TD></TR>");
	}
	else 
	{   print('<TR><TD>&nbsp;</TD>');
	    print("<TD>&nbsp;</TD>");
	    print("<TD>&nbsp;</TD>");
	    print("<TD>&nbsp;</TD></TR>");
	}
    }
    print("<TR><TD COLSPAN=4 ALIGN=CENTER>");
    print("<INPUT TYPE=SUBMIT VALUE='Confirm entry'></TD></TR>");
    $_SESSION['reversescrip']=0;
    print("<TR><TD colspan=4><A HREF=ScripWare.php?tab=30&n=".$_SESSION['formname']);
    print("&c=".$_SESSION['formcolour']."&i=".$_SESSION['i']."$i&a=".$_SESSION['addict'].">");
//    print("See front");
    print("</A></TD></TR>");
};

function DD_displayOTC() {
    global $case;
    print("<div class = tablepink>");
    print("<TABLE  ALIGN=LEFT BGCOLOR=#".$_SESSION['formcolour']);
    print(" WIDTH=350>\n");
    ZA_GetScripTable();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

/* Get OTC details */
$table=$db->select("ScripOTCText","OTCID='".$_SESSION['miscproblemID']."'");
    foreach ($table as $row)
       $otctext=$row['OTCText'];
   /* row 1 of prescription */
//    print ("<TR><TD><P>$otctext</P><CENTER>");
//    print("<IMG SRC=images/otc.jpg></CENTER>\n</TABLE></div>\n");
    print ("<TR><TD height=305 pixels");
    print(" FONT color=#00008C background=images/otc.jpg>");    
    print("<P><br><br><br><br>$otctext<br><br><br><br></P>");
 //    print("<IMG SRC=images/otc.jpg>");
     print("\n</TABLE></div>\n");
} /* end of function Display OTC */



function DF_displayreceipt() {
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition=array ('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips",$condition);
    foreach ($table as $row) {
        $PrescriberProblemID=$row['PrescriberProblemID'];
        $MiscProblemID=$row['MiscProblemID'];
    };

    $condition=array ('OTCID'=>$MiscProblemID);
    $table=$db->select("ScripOTCText",$condition);
    foreach ($table as $row) {
        $ReceiptScenario=$row['OTCText'];
    	$FileDestination=$row['FileDestination'];
    	$FileDestinationLoss=$row['FileDestinationLoss'];
    };

    $condition=array ('PrescriberProblemID'=>$PrescriberProblemID);
    $table=$db->select("ScripPrescriberProblem",$condition);
    foreach ($table as $row) {
        $PrescriberID=$row['PrescriberID'];
    };

    $condition=array ('PrescriberID'=>$PrescriberID);
    $table=$db->select("ScripPrescriber",$condition);
    foreach ($table as $row) {
        $PrescriberName=$row['Name'];
        $PracticeID=$row['PracticeID'];
    };

    $condition=array ('PracticeID'=>$PracticeID);
    $table=$db->select("ScripPractice",$condition);
    foreach ($table as $row) {
        $address1=$row['Address1'];
        $address2=$row['Address2'];
        $address3=$row['Address3'];
        $address4=$row['Address4'];
        $postCode=$row['PostCode'];
    };

    $address = "";
    if ($address1) { $address.=$address1; };
    if ($address2) { $address.=",<br>".$address2; };
    if ($address3) { $address.=",<br>".$address3; };
    if ($address4) { $address.=",<br>".$address4; };
    if ($postCode) { $address.=",<br>".$postCode; };

//  Thus: $PrescriberName is the name of the supplier;
//        $address is the address of the supplier;
//        $ReceiptScenario is the text of the scenario.

/*
    $condition = array ('ReceiptID'=>$_SESSION['miscproblemID']);
    $table=$db->select("ScripReceipts", $condition);
    foreach($table as $row) {
     	$receiptid=$row['ReceiptID'];
     	$code=$row['Code'];
     	$quantity=$row['Quantity'];
     	$packsize=$row['PackSize'];
     	$item=$row['Item'];
     	$strength=$row['Strength'];
     	$disccode=$row['DiscCode'];
     	$trade=$row['Trade'];
     	$VAT=$row['VAT'];
     	$gross=$row['Gross'];
     	$credit=$row['Credit'];
    };
*/


    print("<div class = tablegrey>");
    print("<table align=left bgcolor=".$_SESSION['formcolour']." ");
    print("height=450 width=370 border=1>\n");
    print("<tr><td>$ReceiptScenario</td></tr>");
    print("</table>");


/*    print("<div class = tablegrey>");
    print("<table align=left bgcolor=".$_SESSION['formcolour']);
    print("width=370 border=1>\n");
    print("<tr><td height=50 colspan=9>");
    print("<h2 align=center>$PrescriberName</h2>");
    print("<h3 align=center>$address</h3>");
    print("</td></tr>\n");

    print("<tr><td colspan=9>\n&nbsp;</td></tr>");

    print("<tr>");
    print("<td><font size=1>code</font></td>\n");
    print("<td><font size=1>qnty</font></td>\n");
    print("<td><font size=1>pack size</font></td>\n");
    print("<td width=100 align=center><font size=1>item</font></td>\n");
    print("<td><font size=1>disc code</font></td>\n");
    print("<td><font size=1>trade</font></td>\n");
    print("<td><font size=1>vat</font></td>\n");
    print("<td><font size=1>gross</font></td>\n");
    print("<td><font size=1>credit</font></td>");
    print("</tr>\n");

    print("<tr>");
    print("<td height=100><font size=1>9820</font></td>\n");
    print("<td><font size=1>$quantity</font></td>\n");
    print("<td><font size=1>$packsize</font></td>\n");
    print("<td align=center><font size=1>");
    print("$item $strength</font></td>\n");
    print("<td><font size=1>$disccode</font></td>\n");
    print("<td><font size=1>$trade</font></td>\n");
    print("<td><font size=1>$VAT</font></td>\n");
    print("<td><font size=1>$gross</font></td>\n");
    print("<td><font size=1>$credit</font></td>\n");
    print("</tr>");

    print("<tr>");
    print("<td colspan=9><font size=1>Total</font>\n</td>");
    print("</tr>\n");

    print("<tr><td colspan=9>\n");
    print("<font size=1>Signature of<br>Authorised Recipient");
    print("____________________</font></td>\n");
    print("</tr></table>\n");*/
};

function DG_displayrequisitionmw() {
    global $case;
    print("<div class = tablewhite>");
    print("<TABLE ALIGN=LEFT BGCOLOR=#FFFFFF WIDTH=370>\n");

    print("<TR><TD>");
//    print("<p>".$_SESSION['practiceAddress1']."</p>");

    ZZE_DoPrescriberAddress(0);

    print("<p>Midwife's requisition for<br>");
    print("Prescription Only Drugs</p></TD>");

    print("<td>&nbsp;");
//    ZZD_DoDatePanel(0);
    ZZG_DoPharmacyPanel();
    print("</td></tr>");

    print("<tr><td><p>&nbsp;</p>");
    print("<p>Name of Midwife:&nbsp;&nbsp;&nbsp;&nbsp;");
//    print("<font face='Script' size=+2 color=#00008C>");
    print("<font color=#00008C>");
    print($_SESSION['prescribername']);
    print("</p>");
    print("<p>&nbsp;</p></td>");

    print("<td>");
    ZZG_DoEndorsePanel();
    print("</td>");

    print("</TR>");

    print("</FONT>");
    print("<TR><TD colspan='2'><P>&nbsp;</P>");
    print("<P>Please supply:</P>");
    ZZC_DoMedicationPanel(1, "");
    ZZC_DoMedicationPanel(2, "");
    print("</TD><TD></TD></TR>");

//    ZZG_DoEndorsePanel();

    print("<TR><TD COLSPAN=2><P>Signature of midwife ");
    ZZF_DoSignature(0);
//    print("</P>");
//    print("<P>Authorising Officer: </P>");
    print("</TD></TR></TABLE>\n");
};



function ZA_GetScripTable() {

    // IMPORANT: This is an AMENDED copy of the identically
    // named function in ScripWare

    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array('Letter'=>$_SESSION['currentscrip']);
    $table = $db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $_SESSION['formID'] = $row['FormID'];
        $_SESSION['patientproblemID'] = $row['PatientProblemID'];
        $_SESSION['prescriberproblemID'] = $row['PrescriberProblemID'];
        $_SESSION['drugproblemID'][0] = $row['DrugProblemID'];
        $_SESSION['drugproblemID'][1] = $row['DrugProblemID2'];
        $_SESSION['miscproblemID'] = $row['MiscProblemID'];
    };
};

function ZB_GetPatientProblemTable() {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the patient problem ...
    $condition = array('PatientProblemID'=>$_SESSION['patientproblemID']);
    $table=$db->select("ScripPatientProblem",$condition);
    foreach ($table as $row) {
        $_SESSION['patientID']=$row['PatientID'];
	$_SESSION['fnamemissing']=$row['PatientFNameMissing'];
	$_SESSION['lnamemissing']=$row['PatientSNameMissing'];
	$_SESSION['addressmissing']=$row['PatientAddressMissing'];
	$_SESSION['pataddressincomplete']=$row['PatientAddressIncomplete'];
	$_SESSION['agemissing']=$row['PatientAgeMissing'];
	$_SESSION['DOBmissing']=$row['PatientDOBMissing'];
	$_SESSION['ageDOBmismatch']=$row['PatientAgeDOBMismatch'];
	$_SESSION['nameandaddressmissing']=$row['NameAndAddress'];
	$_SESSION['specialtext']=$row['SpecialText'];
    };
};

function ZC_GetPatientDetails() {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the patient problem ...
    $condition = array ('PatientID'=>$_SESSION['patientID']);
    $table=$db->select("ScripPatient",$condition);
    foreach ($table as $row) {
        $_SESSION['patientfname']=$row['FirstName'];
        $_SESSION['patientlname']=$row['LastName'];
        $_SESSION['age']=$row['Age'];
        $_SESSION['address1']=$row['Address1'];
        $_SESSION['address2']=$row['Address2'];
        $_SESSION['address3']=$row['Address3'];
        $_SESSION['address4']=$row['Address4'];
        $_SESSION['postCode']=$row['PostCode'];
        $_SESSION['PMR']=$row['PMR'];
        $_SESSION['NHSNumber']=$row['NHSNumber'];
    };

    // If address = 0 change to space
    if ($_SESSION['address1']=="0") $_SESSION['address1']="";
    if ($_SESSION['address2']=="0") $_SESSION['address2']="";
    if ($_SESSION['address3']=="0") $_SESSION['address3']="";
    if ($_SESSION['address4']=="0") $_SESSION['address4']="";
    if ($_SESSION['postCode']=="0") $_SESSION['postCode']="";
};

function ZD_GetDrugProblemDetails($index) {
    global $case;
    $index--; // ... arrays are zero-based

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the drug problem
    $m=$_SESSION['drugproblemID'][$index];

    // Tells us what deliberate errors are
    if ($m) {
        $condition="DrugProblemID='".$_SESSION['drugproblemID'][$index]."'";
        $table=$db->select("ScripDrugProblem",$condition);

        foreach ($table as $row) {
            $_SESSION['drugname'][$index]=$row['DrugName'];
	    $_SESSION['drugmispelling'][$index] =$row['DrugMispelling'];
	    $_SESSION['drugnamemissing'][$index] =$row['DrugNameMissing'];
	    $_SESSION['drugnamemispelled'][$index] =$row['DrugNameMispelled'];
	    $_SESSION['druginappropriatechoice'][$index]=
                                    $row['DrugInappropriateChoice']; 
	    $_SESSION['drugnonexist'][$index]=$row['DrugNonExist'];  
            $_SESSION['strength'][$index] =$row['Strength'];
            $_SESSION['strengthcorrect'][$index]=$row['StrengthCorrect'];
 	    $_SESSION['strengthmissing'][$index]=$row['StrengthMissing'];
 	    $_SESSION['strengthunavailable'][$index]=$row['StrengthUnavailable'];
 	    $_SESSION['strengthPMR'][$index]=$row['StrengthPMR'];
 	    $_SESSION['formMR'][$index]=$row['FormPMR'];
 	    $_SESSION['PMRComment'][$index]=$row['PMRComment'];
 	    $_SESSION['deliveryform'][$index]=$row['DeliveryForm'];
            $_SESSION['formcorrect'][$index] =$row['FormCorrect'];
            $_SESSION['deliveryformmissing'][$index] =
                                       $row['DeliveryFormMissing'];
            $_SESSION['deliveryformunavailable'][$index]
                             =$row['DeliveryFormUnavailable']; 
            $_SESSION['deliveryforminappropriate'][$index] =
                           $row['DeliveryFormInappropriate'];
            $_SESSION['deliveryformnotallowed'][$index] =
                  $row['DeliveryFormNotAllowed'];
            $_SESSION['dose'][$index] =$row['Dose'];
 	    $_SESSION['dosecorrect'][$index]=$row['DoseCorrect'];

 	    $_SESSION['doseoverdose'][$index] =$row['DoseOverdose'];
	    $_SESSION['doseincomplete'][$index] =$row['DoseIncomplete'];
	    $_SESSION['dosenotrequired'][$index] =$row['DoseNotRequired'];
            $_SESSION['doseinappropriatetiming'][$index]=
                             $row['DoseInappropriateTiming'];      
            $_SESSION['quantity'][$index]=$row['Quantity'];            
            $_SESSION['quantitycorrect'][$index]=$row['QuantityCorrect'];            
            $_SESSION['quantitymissing'][$index]=$row['QuantityMissing'];            
            $_SESSION['quantityexcessivelegal'][$index]=                      
                             $row['QuantityExcessiveLegal'];  	   
            $_SESSION['quantityexcessivepo'][$index]=$row['QuantityExcessivePO']; 
	    $_SESSION['quantityinsufficient'][$index]=$row['QuantityInsufficient']; 
	    $_SESSION['intervalreq'][$index]=$row['Intervalreq'];                   
            $_SESSION['quantitynointerval'][$index]=$row['QuantityNoInterval'];    
            $_SESSION['quantitynowords'][$index]=$row['QuantityNoWords'];          
            $_SESSION['quantitynofigures'][$index]=$row['QuantityNoFigures'];      
            $_SESSION['blacklisted'][$index] =$row['Blacklisted'];                  
            $_SESSION['notformulary'][$index] =$row['NotFormulary'];            
            $_SESSION['wrongscrip'][$index]=$row['WrongScrip'];   	   
            $_SESSION['wrongpractioner'][$index]=$row['WrongPractioner'];  	   
            $_SESSION['nosdays'][$index] =$row['NosDays'];   	   
            $_SESSION['drughandwritten'][$index]=$row['DrugHandwritten'];    	   
            $_SESSION['drughandcorrect'][$index]=$row['DrugHandCorrect'];
        };
    } else {
        unset($_SESSION['drugname'][$index]);
	unset($_SESSION['drugmispelling'][$index]);
        unset($_SESSION['drugnamemissing'][$index]);
        unset($_SESSION['drugnamemispelled'][$index]);
        unset($_SESSION['druginappropriatechoice'][$index]);
        unset($_SESSION['drugnonexist'][$index]);  
        unset($_SESSION['strength'][$index]);
        unset($_SESSION['strengthcorrect'][$index]);
        unset($_SESSION['strengthmissing'][$index]);
        unset($_SESSION['strengthunavailable'][$index]);
        unset($_SESSION['deliveryform'][$index]);
        unset($_SESSION['formcorrect'][$index]);
        unset($_SESSION['deliveryformmissing'][$index]);
        unset($_SESSION['deliveryformunavailable'][$index]);
        unset($_SESSION['deliveryforminappropriate'][$index]);
        unset($_SESSION['deliveryformnotallowed'][$index]);
        unset($_SESSION['dose'][$index]);
        unset($_SESSION['dosecorrect'][$index]);
        unset($_SESSION['doseoverdose'][$index]);
        unset($_SESSION['doseincomplete'][$index]);
        unset($_SESSION['dosenotrequired'][$index]);
        unset($_SESSION['doseinappropriatetiming'][$index]);
        unset($_SESSION['quantity'][$index]);            
        unset($_SESSION['quantitycorrect'][$index]);            
        unset($_SESSION['quantitymissing'][$index]);            
        unset($_SESSION['quantityexcessivelegal'][$index]);                      
        unset($_SESSION['quantityexcessivepo'][$index]); 
        unset($_SESSION['quantityinsufficient'][$index]); 
        unset($_SESSION['intervalreq'][$index]);                   
        unset($_SESSION['quantitynointerval'][$index]);    
        unset($_SESSION['quantitynowords'][$index]);          
        unset($_SESSION['quantitynofigures'][$index]);      
        unset($_SESSION['blacklisted'][$index]);                  
        unset($_SESSION['notformulary'][$index]);            
        unset($_SESSION['wrongscrip'][$index]);   	   
        unset($_SESSION['wrongpractioner'][$index]);  	   
        unset($_SESSION['nosdays'][$index]);   	   
        unset($_SESSION['drughandwritten'][$index]);    	   
        unset($_SESSION['drughandcorrect'][$index]);
    }; 

};

function ZE_GetPrescriberProblemDetails() {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the prescriber problem ...
    $condition = array('PrescriberProblemID'=>$_SESSION['prescriberproblemID']);
    $table=$db->select("ScripPrescriberProblem",$condition);
    foreach ($table as $row) {
        $_SESSION['prescriberID']=$row['PrescriberID'];
	$_SESSION['prescribernamemissing']=$row['PrescriberNameMissing'];
	$_SESSION['prescribernhsnomissing']=$row['PrescriberNHSNoMissing'];
	$_SESSION['prescriberaddressmissing']=$row['PrescriberAddressMissing'];
	$_SESSION['prescribeaddressincomplete']=$row['PrescriberAddressIncomplete'];
 	$_SESSION['prescriberaddressnonuk']=$row['PrescriberAddressNonUK'];
	$_SESSION['prescriberukaddress1']=$row['UKAddress1'];
	$_SESSION['prescriberukaddress2']=$row['UKAddress2'];
	$_SESSION['prescriberukaddress3']=$row['UKAddress3'];
	$_SESSION['prescriberukaddress4']=$row['UKAddress4'];
	$_SESSION['prescriberukpostcode']=$row['UKPostCode'];
	$_SESSION['prescriberqualsmissing']=$row['PrescriberQualsMissing'];
	$_SESSION['prescriberqualsinappropriate']=$row['PrescriberQualsInappropriate'];
 	$_SESSION['prescribersigmissing']=$row['PrescriberSigMissing'];
 	$_SESSION['prescribersigincorrect']=$row['PrescriberSigIncorrect'];
 	$_SESSION['prescriberauthoriseneeded']=$row['PrescriberAuthoriseNeeded'];
 	$_SESSION['authorisingID']=$row['AuthorisingID'];
 	$_SESSION['incorrectSigFile']=$row['IncorrectSigFile'];
     };
};

function ZF_GetPrescriberDetails() {
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the prescriber ...
    $condition = array ('PrescriberID'=>$_SESSION['prescriberID']);
    $table=$db->select("ScripPrescriber",$condition);
    foreach ($table as $row) {
        $_SESSION['prescribername'] =$row['Name'];
	$_SESSION['qual']=$row['Qual'];
	$_SESSION['practiceID']=$row['PracticeID'];
 	$_SESSION['beepnumber']=$row['BeepNumber'];
 	$_SESSION['nhsnumber']=$row['NHSNumber'];
	$_SESSION['writingexemption']=$row['WritingExemption'];
	$_SESSION['sigfile']=$row['SigFileName'];
	$_SESSION['secondaryPrescriberID']=$row['SecondaryPrescriberID'];
	$_SESSION['prescriberPrivateCD']=$row['PrivateCD'];
     };

    // Get practice details ...
    $condition = array('PracticeID'=>$_SESSION['practiceID']);
    $table=$db->select("ScripPractice",$condition);
    foreach ($table as $row) {
        $_SESSION['practiceCode']=$row['Code'];
        $_SESSION['practicePOrU']=$row['PracticeOrUnit'];
        $_SESSION['practiceName']=$row['Name'];
        $_SESSION['practiceAddress1']=$row['Address1'];
        $_SESSION['practiceAddress2']=$row['Address2'];
        $_SESSION['practiceAddress3']=$row['Address3'];
        $_SESSION['practiceAddress4']=$row['Address4'];
        $_SESSION['practicePostCode']=$row['PostCode'];
        $_SESSION['practiceTelNo']=$row['TelephoneNumber'];
        $_SESSION['practiceTrustID']=$row['TrustID'];
     };

    // Get trust details ...
    $condition = array('ID'=>$_SESSION['practiceTrustID']);
    $table=$db->select("ScripTrust",$condition);
    foreach ($table as $row) {
        $_SESSION['trustCode']=$row['Code'];
        $_SESSION['trustTOrH']=$row['TrustOrHospital'];
        $_SESSION['trustName']=$row['Name'];
        $_SESSION['trustAddress1']=$row['Address1'];
        $_SESSION['trustAddress2']=$row['Address2'];
        $_SESSION['trustAddress3']=$row['Address3'];
        $_SESSION['trustAddress4']=$row['Address4'];
        $_SESSION['trustPostCode']=$row['PostCode'];
        $_SESSION['trustTelNo']=$row['TelephoneNumber'];
    };

    // Change address to space if set to 0 ...
    if ($_SESSION['practiceAddress1']=="0") $_SESSION['practiceAddress1']="";
    if ($_SESSION['practiceAddress2']=="0") $_SESSION['practiceAddress2']="";
    if ($_SESSION['practiceAddress3']=="0") $_SESSION['practiceAddress3']="";
    if ($_SESSION['practiceAddress4']=="0") $_SESSION['practiceAddress4']="";
    if ($_SESSION['practicePostCode']=="0") $_SESSION['practicePostCode']="";
};

function ZG_GetMiscProblemDetails() {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the misc problem ...
    $condition = array('MiscProblemID'=>$_SESSION['miscproblemID']);
    $table=$db->select("ScripMiscProblem", $condition);
    foreach ($table as $row) {
        $_SESSION['datemissing']=$row['DateMissing'];
	$_SESSION['datefuture']=$row['DateFuture'];
	$_SESSION['datelate']=$row['DateLate'];
	$_SESSION['purpose']=$row['Purpose'];
	$_SESSION['purposemarks']=$row['PurposeMarks'];
	$_SESSION['patienthandwritten']=$row['PatientHandwritten'];
	$_SESSION['interaction']=$row['Interaction'];
     };

    $condition=array("RecordID"=>$_SESSION['studentrecordID']);
    switch ($_SESSION['categoryToMod']){
        case 8:
        $table=$db->select("ScripStudentRecordHome",$condition);
        break;

        case 1: case 2: case 3:
        $table=$db->select("ScripStudentRecord",$condition);
        break;

        case 4: case 5: case 6:
        $table=$db->select("ScripStudentRecordExam",$condition);
        break;
    };
    foreach ($table as $row) {
        $thedate=$row['DateDone'];
        $thisyear=strtok($thedate,"-");
        $thismonth=strtok("-");
        $thisday=strtok("-");
        $_SESSION['timestamp']=mktime(0,0,0,$thismonth,$thisday,$thisyear);
    };
};

function ZK_GetVerify() {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the verify answer record
    $condition = array('VerifyID'=>$_SESSION['VerifyID']);
    $table=$db->select("ScripVerify",$condition);
    foreach ($table as $row) {
        $_SESSION['paddressmissing']=$row['AddressMissing'];
	 $_SESSION['addressincomplete']=$row['AddressIncomplete'];
	 $_SESSION['addressnonuk']=$row['AddressNonUK'];
	 $_SESSION['qualsmissing']=$row['QualsMissing'];
	 $_SESSION['qualsinappropriate']=$row['QualsInappropriate'];
	 $_SESSION['namemissing']=$row['NameMissing'];
	 $_SESSION['patfname']=$row['Patfname'];
	 $_SESSION['patsname']=$row['Patsname'];
	 $_SESSION['patage']=$row['PatAge'];
	 $_SESSION['pataddress']=$row['PatAddress'];
	 $_SESSION['pataddressincom']=$row['PatAddressIncomplete'];
	 $_SESSION['nameAndAddress']=$row['PatNameAddress'];
	 $_SESSION['patDOB']=$row['PatDOB'];
	 $_SESSION['vinteraction']=$row['Interaction'];
	 $_SESSION['pathand']=$row['PatHand'];
	 $_SESSION['vdatemissing']=$row['DateMissing'];
	 $_SESSION['vdpurpose']=$row['Purpose'];
	 $_SESSION['vdatefuture']=$row['DateFuture'];
	 $_SESSION['vdatelate']=$row['DateLate'];
	 $_SESSION['sigmissing']=$row['SigMissing'];
	 $_SESSION['sigincorrect']=$row['SigIncorrect'];
	 $_SESSION['sigauth']=$row['SigAuth'];
	 $_SESSION['sigcheck']=$row['SigCheck'];
    };
};

function ZL_GetVerifyDrug($index) {
    global $case;
    $index--;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the verify drug answer record
    $condition = array ('RecordID'=>$_SESSION['VerifyDrugID'][$index]);
    $table=$db->select("ScripVerifyDrug", $condition);
    foreach ($table as $row) {
     	 $_SESSION['vdnamemissing'][$index]=$row['NameMissing'];
	 $_SESSION['vdnamemispelled'][$index]=$row['NameMispelled'];
	 $_SESSION['vdinappropriate'][$index]=$row['Inappropriate'];
	 $_SESSION['vdnonexist'][$index]=$row['NonExist'];
	 $_SESSION['vdblacklisted'][$index]=$row['Blacklisted']; 
	 $_SESSION['vdnotformulary'][$index]=$row['NotFormulary']; 
	 $_SESSION['vdwrongscrip'][$index]=$row['WrongScrip']; 
	 $_SESSION['vdwrongpractioner'][$index]=$row['WrongPractioner']; 
	 $_SESSION['vdstrengthmissing'][$index]=$row['StrengthMissing'];
	 $_SESSION['vdstrengthunavailable'][$index]=$row['StrengthUnavailable'];
	 $_SESSION['vdformmissing'][$index]=$row['FormMissing'];
	 $_SESSION['vdformunavailable'][$index]=$row['FormUnavailable'];
	 $_SESSION['vdforminappropriate'][$index]=$row['FormInappropriate'];
	 $_SESSION['vdformnotallowed'][$index]=$row['FormNotAllowed'];
	 $_SESSION['vdoverdose'][$index]=$row['Overdose'];
	 $_SESSION['vdunderdose'][$index]=$row['DoseUnderdose'];
	 $_SESSION['vddoseincomplete'][$index]=$row['DoseIncomplete'];
	 $_SESSION['vddosenotrequired'][$index]=$row['DoseNotRequired'];
        $_SESSION['vddoseinappropriatetiming'][$index]= $row['DoseInappropriateTiming'];
	 $_SESSION['vdquantitymissing'][$index]=$row['QuantityMissing']; 
	 $_SESSION['vdquantityexcessivepo'][$index]=$row['QuantityExcessivePO']; 
	 $_SESSION['vdquantityexcessivelegal'][$index]=$row['QuantityExcessiveLegal'];
 	 $_SESSION['vdquantityinsufficient'][$index]=$row['QuantityInsufficient']; 
	 $_SESSION['vdquantitynointerval'][$index]=$row['QuantityNoInterval']; 
	 $_SESSION['vdquantitynowords'][$index]=$row['QuantityNoWords']; 
	 $_SESSION['vdquantitynofigures'][$index]=$row['QuantityNoFigures']; 
	 $_SESSION['drughand'][$index]=$row['DrugHand'];
	 $_SESSION['vdStrengthPMR'][$index]=$row['StrengthPMRMismatch'];
	 $_SESSION['vdFormPMR'][$index]=$row['FormPMRMismatch'];
    };
};

function ZZA_DoAgePanel($limited) {
    global $case;

/*
    // To find DOB take age from today
    // then another X random days so not his birthday
    // only do this if first time for this prescription and dob not set
    if ($_SESSION['dob']=="") {
        if ($_SESSION['ageDOBmismatch']==0) {
	    $_SESSION['dob']=date("d-m-Y",($_SESSION['timestamp'] -
		(($_SESSION['age']*60*60*24*365)+50*60*60*24)));
 	} else {
	    $_SESSION['dob']=date("d-m-Y",($_SESSION['timestamp'] -
		(($_SESSION['age']*60*60*24*365)+5000*60*60*24)));
        }; 
    };
*/

    // To find the date of birth take age from today and
    // then another X random days (so as not to choose birthday).
    // Only do this if first time for this prescription and dob not set.
    if ($_SESSION['dob'] == "") {
        if ($_SESSION['ageDOBmismatch']==0) {
            // To generate a likely date of birth, go back AGE years plus an extra 50 days ...
//            $_SESSION['dob']=date("d-m-Y",($_SESSION['timestamp'] - (($_SESSION['age']*60*60*24*365)+50*60*60*24)));
//            Above will not work under Windows ... use manual version below ...

            // Look at the date 315 (= 365 - 50) days from now ... 
            $Altdob = date("d-m-Y",($_SESSION['timestamp'] + (315*60*60*24)));
            // Now look (AGE + 1) years ago with string functions ...
            // Find the last four characters ...
            $tempYear = substr($Altdob, -4);
            // Keep the day and month ...
            $tempDayMonth = str_replace($tempYear, "", $Altdob);
            // Now go back (AGE + 1) years ...
            $tempYear = $tempYear - ($_SESSION['age'] + 1);
            $Altdob = $tempDayMonth.$tempYear;
 	} else {
            // To generate an UNlikely date of birth, go back AGE years plus an extra 5000 days (c. 13 years and 252 days) ...
//            $_SESSION['dob']=date("d-m-Y",($_SESSION['timestamp'] - (($_SESSION['age']*60*60*24*365)+5000*60*60*24)));

            // Look at the date 113 (= 365 - 252) days from now ... 
            $Altdob = date("d-m-Y",($_SESSION['timestamp'] + (113*60*24)));
            $tempYear = substr($Altdob, -4);
            // Keep the day and month ...
            $tempDayMonth = str_replace($tempYear, "", $Altdob);
            // Now go back (AGE + 14) years ...
            $tempYear = $tempYear - ($_SESSION['age'] + 14);
            $Altdob = $tempDayMonth.$tempYear;
        }; 

        $_SESSION['dob'] = $Altdob;

        // Check about leap day ... e.g. 29-02-1961 does not exist - change to 28-02-1961
        $tempDay = substr($Altdob, 0, 2);
        $tempMonth = substr($Altdob, 3, 2);

        if ($tempDay = "29" && $tempMonth = "02"){
            // Looking at a February 29th ...
            if ($tempYear/4 != floor($tempYear/4)) {
                $_SESSION['dob'] = "28-".$tempMonth."-".$tempYear;
            }; 
        };
    };

    // Has the user spotted this problem
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the patient problem
    $table=$db->select("ScripVerify","VerifyID='".$_SESSION['VerifyID']."'");
    foreach ($table as $row) {
        $patage=$row['PatAge'];
        $patDOB=$row['PatDOB'];
    };

    $condition = array ('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $entirelyhandwritten = $row['EntirelyHandwritten'];
    };

    if ($_SESSION['patienthandwritten'] || $entirelyhandwritten) {
        $tface="face='Lucida Handwriting Italic, serif'";
        $typeface="face='Lucida Handwriting Italic, serif'";
    } else {
        $tface="";
        $typeface="";
    };

    if (!$limited) {
        // Normal (i.e. non-hospital) forms only ...
        // Print age ...
        print("Age<br>");
        if ($_SESSION['agemissing']!="0") {
            if ($patage=="0") { // Deliberately missing ...
                print("&nbsp;");
            } else { // In RED ...
                print("<font color=#EE0000 size='1' $tface>");
                print("&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['age']."</font>");
            };
        } else { // In BLUE ...
            print("<font color=#00008C size='1' $tface>");
            print("&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['age']."</font>");
        };
        print("<br><br>");

        // Print date of birth ...
        print("DoB<br>");
    };

    if ($_SESSION['DOBmissing']!="0") {
     	if ($patDOB=="0") {
            print("&nbsp;");
        } else { // In RED
 	    print("<FONT color=#EE0000 size='1' $tface>".$_SESSION['dob']."</FONT>");
	};
    } else { // In BLUE
 	print("<FONT color=#00008C size='1' $tface>".$_SESSION['dob']."</FONT>");
    };
};

function ZZAA_Magnify_AgePanel()
{   global $case;
    print("<BODY BGCOLOR=".$_SESSION['formcolour'].">");
    print("<H1>Age</H1>");
/* to find DOB take age from today */
/* then another X random days so not their birthday */
/* only do this if first time for this prescription and dob not set */
    if ($_SESSION['dob']=="") {
        if ($_SESSION['ageDOBmismatch']==0) {
            $nDays = rand(1, 364);
	    $_SESSION['dob']=date("d-m-Y",($_SESSION['timestamp'] -
		(($_SESSION['age']*60*60*24*365)+$nDays*60*60*24)));
 	} else {
	    $_SESSION['dob']=date("d-m-Y",($_SESSION['timestamp'] -
		(($_SESSION['age']*60*60*24*365)+5000*60*60*24)));
        }; 
    };

    // Has the user spotted this problem
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    // Get details of the patient problem
$table=$db->select("ScripVerify","VerifyID='".$_SESSION['VerifyID']."'");
    foreach ($table as $row) {
        $patage=$row['PatAge'];
	$patDOB=$row['PatDOB'];
    };

    $condition = array ('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $entirelyhandwritten = $row['EntirelyHandwritten'];
    };

    if ($_SESSION['patienthandwritten'] || $entirelyhandwritten) {
        $tface="face='Lucida Handwriting Italic, serif'";
    } else {
        $tface="";
    };

    // Print age
    if ($_SESSION['agemissing']!="0")
    {	if ($patage=="0")
	{   print("&nbsp;");
        }
	else
	{   print("<BR><FONT color=#EE0000 $tface><H1>");
		print("&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['age']."</H1></FONT>");
    	}
    }
    else
    {   print("<BR><FONT  color=#00008C $tface><H1>");
	print("&nbsp;&nbsp;&nbsp;&nbsp;".$_SESSION['age']."</H1></FONT>");
    }
/* print DOB */
    if ($_SESSION['DOBmissing']!="0")
    {	if ($patDOB=="0")
	{   print("&nbsp;");
        }
	else
	{   print("</P><H1>DoB<BR></H1><FONT color=#EE0000 $tface>");
 	    print("<H1>".$_SESSION['dob']."</H1></FONT>");
 	}
    }
    else
    {   print("</P><H1>DoB<BR></H1><FONT color=#00008C $tface>");
 	print("<H1>".$_SESSION['dob']."</H1></FONT>");
    }
    print("<A HREF=javascript:void(0) \n");
    print('onClick="self.close()">');
    print("Close help window and return to course</A>");
};

function ZZB_DoAddressPanel($limited) {

    global $case;

//print("In ZZB");

    // Get details of the patient problem
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array ('VerifyID'=>$_SESSION['VerifyID']);
    $table=$db->select("ScripVerify",$condition);
    foreach ($table as $row) {
        $fname=$row['Patfname'];
        $sname=$row['Patsname'];
        $address=$row['PatAddress'];
        $pataddressincomplete=$row['PatAddressIncomplete'];
        $nameAndAddress=$row['PatNameAddress'];
    };

    // If NOT a hospital TTO ...
    if(!$limited) { print("<br><br>"); };

    // Decide on the FONT ...
    $condition = array ('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $entirelyhandwritten = $row['EntirelyHandwritten'];
    };

    if ($_SESSION['patienthandwritten'] || $entirelyhandwritten) {
        $tface="face='Lucida Handwriting Italic, serif'";
        $typeface="face='Lucida Handwriting Italic, serif'";
    } else {
        $tface="";
        $typeface="";
    };

    if ($_SESSION['patienthandwritten'] || $entirelyhandwritten) {
//        $tface="FACE='Script' SIZE=+2";
        $tface="face='Lucida Handwriting Italic, serif'";
    } else {
        $tface="";
    };

    // The NAME ...
    if(!$limited || $limited == "name"){

        print("<FONT COLOR=#00008C $tface>");

        // A new variable: if not set, set it now ...
        if ($_SESSION['fname']==0) $_SESSION['fname']==rand(1,2);
        if ($_SESSION['address']==0) $_SESSION['address']==rand(1,5);

        print("<font size='1'>");
        // Check if first name problem ...
        if ($_SESSION['fnamemissing'] || $_SESSION['nameandaddressmissing']) {
            if ($_SESSION['fnamemissing'] && $fname || 
              $_SESSION['nameandaddressmissing'] && $nameAndAddress) {
                // Show in red if user has spotted problem
                print("<br><font color=#EE0000 $tface>");
                print($_SESSION['patientfname']."</font>");
            } else {
                // Omit christian name
                if ($_SESSION['fname']==1 || $_SESSION['nameandaddressmissing']) {
                    print("<br>&nbsp;");  
                } else {
                    // Reduce name to initial ...
                    $initial=substr($_SESSION['patientfname'],0,1);
                    print("<BR>$initial");
	        };
            };
        } else { // If christian name not in problem, show in blue
            print("<BR>".$_SESSION['patientfname']);
        };

        // Amend NAME (either part)  - read and process ...
        $condition = array ('ID'=>$_SESSION['AmendmentsID']);
        $table=$db->select("ScripAmendments", $condition);
        foreach ($table as $row) {
            $amend_fname = $row['Patfname'];
            $amend_sname = $row['Patsname'];
        };
        $amend_fname = trim(str_replace("|", "", $amend_fname));
        $amend_sname = trim(str_replace("|", "", $amend_sname));

        print(" <font color='#007f00'>$amend_fname</font>");

        // Check if SURNAME problem ...
        if ($_SESSION['lnamemissing'] || $_SESSION['nameandaddressmissing']) { // Deliberate error
            if ($_SESSION['lnamemissing'] && $sname || $_SESSION['nameandaddressmissing'] && $nameAndAddress) {
            // User spotted it
                print("<FONT COLOR=#EE0000 $tface> ".$_SESSION['patientlname']."<BR></FONT>");
            } else {// Omit
                print("&nbsp;<BR>");
            };
        } else {
            print(" ".$_SESSION['patientlname']."<BR>");
        };

        print(" <font color='#007f00'>$amend_sname</font>");

        print("<br>");
    };

    // The ADDRESS ...
    if (!$limited || $limited == "address") {

        // Check if ADDRESS missing ...
        if ($_SESSION['addressmissing']>0
          || $_SESSION['nameandaddressmissing']) { // Address problem set ...
            if ($address && $_SESSION['addressmissing'] ||
               $_SESSION['nameandaddressmissing'] && $nameAndAddress) {
                // User spotted it - show in red ...

                print("<font color='#EE0000' $typeface>");
                $a1 = ($_SESSION['address1']) ? $_SESSION['address1'] : $nbsp;
                print("$a1");
                $a2 = ($_SESSION['address2']) ? $_SESSION['address2'] : $nbsp;
                if (!$limited || !$a2) { print("<br>"); } else { print(", "); };
                print("$a2");
                $a3 = ($_SESSION['address3']) ? $_SESSION['address3'] : $nbsp;
                if (!$limited || !$a3) { print("<br>"); } else { print(", "); };
                print("$a3");
                $a4 = ($_SESSION['address4']) ? $_SESSION['address4'] : $nbsp;
                if (!$limited || !$a4) { print("<br>"); } else { print(", "); };
                print("$a4");
                $pc = ($_SESSION['postCode']) ? $_SESSION['postCode'] : $nbsp;
                if (!$limited || !$pc) { print("<br>"); } else { print(", "); };
                print("$pc");
                print("</font>");
            } else {
/*	    if ($_SESSION['address']==4) print("<BR>&nbsp;");
	    elseif ($_SESSION['address']==1) print($_SESSION['address1']);
	    elseif ($_SESSION['address']==2) print("<BR>".$_SESSION['address2']);
	    elseif ($_SESSION['address']==3) print("<BR>".$_SESSION['address3']);
 	    else print("<BR>".$_SESSION['address4']); */
            };
        } else if ($_SESSION['pataddressincomplete']) {
            if ($pataddressincomplete) {
                print("<font color='#EE0000' $typeface>");
                $a1 = ($_SESSION['address1']) ? $_SESSION['address1'] : $nbsp;
                print("$a1");
                $a2 = ($_SESSION['address2']) ? $_SESSION['address2'] : $nbsp;
                if (!$limited || !$a2) { print("<br>"); } else { print(", "); };
                print("$a2");
                $a3 = ($_SESSION['address3']) ? $_SESSION['address3'] : $nbsp;
                if (!$limited || !$a3) { print("<br>"); } else { print(", "); };
                print("$a3");
                $a4 = ($_SESSION['address4']) ? $_SESSION['address4'] : $nbsp;
                if (!$limited || !$a4) { print("<br>"); } else { print(", "); };
                print("$a4");
                $pc = ($_SESSION['postCode']) ? $_SESSION['postCode'] : $nbsp;
                if (!$limited || !$pc) { print("<br>"); } else { print(", "); };
                print("$pc");
                print("</font>");
            } else {
                // Not detected by student; print LINE 2 ONLY in blue ...
                print("<font color='#00008C' $typeface>");
                print($_SESSION['address2']."<br>"); 
                print("</font>");
            };
            print("<p>&nbsp;</p>");

        } else { // If neither problem, show all (in blue) ...
            print("<font color='#00008C' $typeface>");
            $a1 = ($_SESSION['address1']) ? $_SESSION['address1'] : $nbsp;
            print("$a1");
            $a2 = ($_SESSION['address2']) ? $_SESSION['address2'] : $nbsp;
            if (!$limited || !$a2) { print("<br>"); } else { print(", "); };
            print("$a2");
            $a3 = ($_SESSION['address3']) ? $_SESSION['address3'] : $nbsp;
            if (!$limited || !$a3) { print("<br>"); } else { print(", "); };
            print("$a3");
            $a4 = ($_SESSION['address4']) ? $_SESSION['address4'] : $nbsp;
            if (!$limited || !$a4) { print("<br>"); } else { print(", "); };
            print("$a4");
            $pc = ($_SESSION['postCode']) ? $_SESSION['postCode'] : $nbsp;
            if (!$limited || !$pc) { print("<br>"); } else { print(", "); };
            print("$pc");
            print("</font>");
        };

        // SHOW AMENDMENT - ADDRESS - read and process ...
        $condition = array ('ID'=>$_SESSION['AmendmentsID']);
        $table=$db->select("ScripAmendments", $condition);
        foreach ($table as $row) {
            $amend_pataddress = $row['PatAddress']." ".$row['PatNameAddress']." ".$row['PatAddressIncomplete'];
        };
        $amend_pataddress = trim(str_replace("|", "", $amend_pataddress));

        print(" <font color='#007f00'>$amend_pataddress</font>");

        if ($_SESSION['specialtext']) { print($_SESSION['specialtext']."<BR>"); };

        print("</font></font>");

        if (!$limited) { print("<br />"); };
        if ($_SESSION['group'] == '4' || $_SESSION['group'] == '8') {
            print("NHS Number: ");
            if ($_SESSION['patienthandwritten'] || $entirelyhandwritten) {
                $tface="face='Lucida Handwriting Italic, serif'";
            } else {
                $tface="";
            };
            print("<font size='1' color='00008c' $tface>".$_SESSION['NHSNumber']."</font>");
        };
    };

};

function ZZAB_Magnify_AddressPanel() {
    global $case;
/* Get details of the patient problem */
    print("<BODY BGCOLOR=".$_SESSION['formcolour'].">");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripVerify","VerifyID='".$_SESSION['VerifyID']."'");
    foreach ($table as $row)
    {   $fname=$row['Patfname'];
	$sname=$row['patsname'];
	$address=$row['PatAddress'];
    }
    if ($_SESSION['patienthandwritten']) {
        $tface="face='Lucida Handwriting Italic, serif'";
    } else {
        $tface="";
    };
    print("<FONT COLOR=#00008C $tface><H1>");
    if ($_SESSION['fname']==0) $_SESSION['fname']==rand(1,2);
    if ($_SESSION['address']==0) $_SESSION['address']==rand(1,5);
/* check if first name problem */
    if ($_SESSION['fnamemissing']>0)
    {   if ($fname!="0")
	   print ($_SESSION['patientfname']." ");
	else
	   if ($_SESSION['fname']==1) print("<BR>&nbsp;");
	    else
	    {   $initial=substr($_SESSION['patientfname'],0,1);
		print("$initial ");
	    }
    }
    else print($_SESSION['patientfname']." ");
/* check if last name problem */
    if ($_SESSION['lnamemissing']>0) 
    {   if ($sname!="0")
	   print($_SESSION['patientlname']." "); 
 	else print("&nbsp; ");
    }
    else print(" ".$_SESSION['patientlname']."<BR>");
 /* check if address missing */
    if ($_SESSION['addressmissing']>0) {
        if ($address!="0") {
            print($_SESSION['address1']."<BR>");
            print($_SESSION['address2']."<BR>");
            print($_SESSION['address3']."<BR>");
            print($_SESSION['address4']);
        } else {
            if ($_SESSION['address']==4) print("<BR>&nbsp;");
            elseif ($_SESSION['address']==1) print($_SESSION['address1']);
            elseif ($_SESSION['address']==2) print("<BR>".$_SESSION['address2']);
            elseif ($_SESSION['address']==3) print("<BR>".$_SESSION['address3']);
            else print("<BR>".$_SESSION['address4']);
        };
    } else {
        print($_SESSION['address1']."<BR>");
        print($_SESSION['address2']."<BR>");
        print($_SESSION['address3']."<BR>");
    };
    if ($_SESSION['specialtext']) print($_SESSION['specialtext']."<BR>");
    print("</H1></FONT>");
    print("<A HREF=javascript:void(0) \n");
    print('onClick="self.close()">');
    print("Close help window and return to course</A>");
};

function ZZC_DoMedicationPanel($index, $limited) {
    // By default (i.e. $limited is nothing), this function shows
    // all atrributes (name, dose, etc.). If $limited is set to
    // "name", then only the name is shown ...

    global $case;

// print("<pre>");print_r($case);print("</pre>");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Should the script be entirely handwritten?
    $condition = array ('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $entirelyhandwritten = $row['EntirelyHandwritten'];
    };

    if ($entirelyhandwritten) {
  	$tface=" face='Lucida Handwriting Italic, serif'";
    };

    $index--; // Zero-based
    $thedrug=$_SESSION['VerifyDrugID'][$index];

    // Get the user's current thinking ...
    $table=$db->select("ScripVerifyDrug","RecordID='$thedrug'");
    foreach ($table as $row) {
        $namemissing=$row['NameMissing'];
	$namemispelled=$row['NameMispelled'];
        $inappropriate=$row['Inappropriate'];
        $nonexist=$row['NonExist'];
        $blcklisted=$row['Blacklisted'];
        $ntformulary=$row['NotFormulary'];
        $wrngscrip=$row['WrongScrip'];
        $wrngpractitioner=$row['WrongPractioner'];
 	$strengthmissing=$row['StrengthMissing'];
 	$strengthunav=$row['StrengthUnavailable'];

 	$formmissing=$row['FormMissing'];
 	$formunav=$row['FormUnavailable'];
 	$forminapp=$row['FormInapropriate'];
 	$formnotallowed=$row['FormNotAllowed'];

 	$overdose=$row['Overdose'];
 	$underdose=$row['DoseUnderdose'];
 	$doseincomplete=$row['DoseIncomplete'];
 	$dosenotrequired=$row['DoseNotRequired'];
 	$doseinappropriatetiming=$row['DoseInappropriateTiming'];

 	$QtyMissing=$row['QuantityMissing'];
 	$QtyExcessivePO=$row['QuantityExcessivePO'];
 	$QtyExcessiveLegal=$row['QuantityExcessiveLegal'];
 	$QtyInsufficient=$row['QuantityInsufficient'];
	$QtyNoInterval=$row['QuantityNoInterval'];
 	$QtyNoWords=$row['QuantityNoWords'];
 	$QtyNoFigures=$row['QuantityNoFigures'];

 	$drughandwritten=$row['DrugHand'];
    };

    // SHOW AMENDMENT - MEDICATION - read and process ...
    if ($index == 0) {
        $condition = array('ID'=>$_SESSION['AmendmentsDrugID']);
    } elseif ($index == 1) {
        $condition = array('ID'=>$_SESSION['Amendments2DrugID']);
    };

    $table=$db->select("ScripAmendmentsDrug", $condition);
    foreach ($table as $row) {
        $amend_namemissing=$row['NameMissing'];
        $amend_namemispelled=$row['NameMispelled'];
        $amend_inappropriate=$row['Inappropriate'];
        $amend_nonexist=$row['NonExist'];
        $amend_blcklisted=$row['Blacklisted'];
        $amend_ntformulary=$row['NotFormulary'];
        $amend_wrngscrip=$row['WrongScrip'];
        $amend_wrngpractitioner=$row['WrongPractioner'];
        $amend_strengthmissing=$row['StrengthMissing'];
        $amend_strengthunav=$row['StrengthUnavailable'];

        $amend_formmissing=$row['FormMissing'];
        $amend_formunav=$row['FormUnavailable'];
        $amend_forminapp=$row['FormInapropriate'];
        $amend_formnotallowed=$row['FormNotAllowed'];

        $amend_overdose=$row['Overdose'];
        $amend_underdose=$row['DoseUnderdose'];
        $amend_doseincomplete=$row['DoseIncomplete'];
        $amend_dosenotrequired=$row['DoseNotRequired'];
        $amend_doseinappropriatetiming=$row['DoseInappropriateTiming'];

        $amend_QtyMissing=$row['QuantityMissing'];
        $amend_QtyExcessivePO=$row['QuantityExcessivePO'];
        $amend_QtyExcessiveLegal=$row['QuantityExcessiveLegal'];
        $amend_QtyInsufficient=$row['QuantityInsufficient'];
        $amend_QtyNoInterval=$row['QuantityNoInterval'];
        $amend_QtyNoWords=$row['QuantityNoWords'];
        $amend_QtyNoFigures=$row['QuantityNoFigures'];

        $amend_drughandwritten=$row['DrugHand'];
    };

    // Get the student's thinking on interactions ...
    $condition = array ('ID'=>$_SESSION['InteractionID']);
    $table=$db->select("ScripInteraction",$condition);
    foreach ($table as $row) {
        $correctcombination = $row['CorrectCombination'];        
    };

    // ... and the model answer on interactions ...
    $condition = array ('CaseID'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripInteractionMA",$condition);
    foreach ($table as $row) {
        $suggestion = $row['Suggestion'];        
        $withwhat = $row['WithWhat'];        
    };
    if ($withwhat != '12') { $withwhat--; }; // Make zero-based

    // How the interaction part works: 
    // If the student has found the correct combination of interacting
    // drugs, then the correct action is taken on the script (e.g. 
    // replacement by another drug) even if the user didn't suggest
    // this course of action

    print("<font size=1>");

    // Don't do this being called from a hospital TTO (the
    // <p></p> upsets the formatting) ...
    // Even where set, only one purpose per form
    if (!$limited && $_SESSION['miscproblemID'] > 0 && !$index > 0) {
        // Should have been read already - no harm in making sure!

        $condition = array("MiscProblemID"=>$_SESSION['miscproblemID']);
        $table=$db->select("ScripMiscProblem",$condition);
        foreach ($table as $row) {
            $_SESSION['purpose']=$row['Purpose']; // Purpose
            $_SESSION['purposemarks']=$row['PurposeMarks']; // Error intended?
        };

        if ($_SESSION['VerifyID']) {
            $condition = array ('VerifyID'=>$_SESSION['VerifyID']);
            $table=$db->select("ScripVerify", $condition);
            foreach ($table as $row) {
                $purposeMS=$row['Purpose']; // User's thoughts
            };
        };

        $m=$_SESSION['purpose']; // For convenience ...
        if ($_SESSION['purposemarks']>0) { // Error intended ...
            if ($purposeMS != "0") { // Has been spotted ... red
                print("<P><FONT $tface color=#ee0000>$m</FONT></P>");
	    } else {
                print ("");
            };
        } else { // No error  ... blue
            print("<P><FONT $tface color=#00008C>$m</FONT></P>");
        };
    };

    if (!$limited && $index==1) { print("<p>"); };
    $m=$_SESSION['drugproblemID'][$index];
    if ($m!="0") {

        // The eight drug problems are deemed to be mutually exclusive. In some
        // cases this is obvious e.g. if missing then it cannot be misspelt; in
        // others it is less so e.g. an inappropriate choice could be misspelt.

        // If any ONE problem has been correctly spotted, therefore, the 
        // name of the drug is written in red (indicating success).
        // If not, check to see whether the deliberate error is "drug
        // missing"; if so, show no drug. Otherwise write it in blue.

        // Separate out the logic determining what to show and the display code
        // itself

        // Logic for determining what DRUG to show 
        $boolean1 = $_SESSION['drugnamemissing'][$index] && $namemissing;
        $boolean2 = $_SESSION['drugnamemispelled'][$index] && $namemispelled;
        $boolean3 = $_SESSION['druginappropriatechoice'][$index] && $inappropriate;
        $boolean4 = $_SESSION['drugnonexist'][$index] && $nonexist;
        $boolean5 = $_SESSION['blacklisted'][$index] && $blcklisted;
        $boolean6 = $_SESSION['notformulary'][$index] && $ntformulary;
        $boolean7 = $_SESSION['wrongscrip'][$index] && $wrngscrip;
        $boolean8 = $_SESSION['wrongpractioner'][$index] && $wrngpractitioner;
        $booleandrug = $boolean1 || $boolean2 || $boolean3 || $boolean4 ||
          $boolean5 || $boolean6 || $boolean7 || $boolean8;
        $nodrugchanges = !$_SESSION['drugnamemissing'][$index] &&
          !$_SESSION['drugnamemispelled'][$index] && 
          !$_SESSION['druginappropriatechoice'][$index] && 
          !$_SESSION['drugnonexist'][$index] && 
          !$_SESSION['blacklisted'][$index] &&
          !$_SESSION['notformulary'][$index] && 
          !$case->$_SESSION['wrongscrip'][$index] && 
          !$_SESSION['wrongpractioner'][$index]; 

        // Logic for determining what STRENGTH to show 
        $boolean1 = $_SESSION['strengthmissing'][$index] && $strengthmissing;
        $boolean2 = $_SESSION['strengthunavailable'][$index] && $strengthunav;
        $booleanstrength = $boolean1 || $boolean2;
        $nostrengthchanges = !$_SESSION['strengthmissing'][$index] &&
          !$_SESSION['strengthunavailable'][$index];

        // Logic for determining what FORM to show
        $boolean1 = $_SESSION['deliveryformmissing'][$index] && $formmissing;
        $boolean2 = $_SESSION['deliveryformunavailable'][$index] && $formunav;
        $boolean3 = $_SESSION['deliveryforminappropriate'][$index] && $forminapp;
        $boolean4 = $_SESSION['deliveryformnotallowed'][$index] && $formnotallowed;
        $booleanform = $boolean1 || $boolean2 || $boolean3 || $boolean4;
        $noformchanges = !$_SESSION['deliveryformmissing'][$index] &&
          !$_SESSION['deliveryformunavailable'][$index] &&
          !$_SESSION['deliveryforminappropriate'][$index] && 
          !$_SESSION['deliveryformnotallowed'][$index];

        // Logic for determining what DOSE to show
        $boolean1 = $_SESSION['doseoverdose'][$index] && $overdose;
        $boolean2 = $_SESSION['doseunderdose'][$index] && $underdose;
        $boolean3 = $_SESSION['doseincomplete'][$index] && $doseincomplete;
        $boolean4 = $_SESSION['dosenotrequired'][$index] && $dosenotrequired;
        $boolean5 = $_SESSION['doseinappropriatetiming'][$index] && $doseinappropriatetiming;
        $boolean6 = ($suggestion == '+' || $suggestion == '-')
                            && $correctcombination;
        // Boolean6 is change (i.e. increase or decrease) the dose in an 
        // interaction - only implemented if the student has spotted it
        $booleandose = $boolean1 || $boolean2 || $boolean3 || $boolean4 ||   
                             $boolean5 || $boolean6;
        $nodosechanges = !$_SESSION['doseoverdose'][$index] &&          
                         !$_SESSION['doseunderdose'][$index] &&      
                         !$_SESSION['doseincomplete'][$index] && 
                         !$_SESSION['dosenotrequired'][$index] && 
                         !$_SESSION['doseinappropriatetiming'][$index];

        // Logic for determining what QUANTITY to show
        $thequantity=$_SESSION['quantity'][$index];

        // Add in intervals if required
        $pint=0;
        if ($_SESSION['intervalreq'][$index]>0) { $pint+=4; };
        if ($_SESSION['quantitynointerval'][$index]>0) { $pint+=2; };
        if ($QtyNoInterval>0) { $pint+=1; };
        $daynos=date("w",$_SESSION['timestamp']);
        $sp="&nbsp;&nbsp;";
        if  ($daynos==0) { $daynos=7; $daynos--; };
            switch ($pint) {
	    case 0: case 1: case 2: case 3: case 6:
            $printquantity=$thequantity; break;

            case 4: case 5: case 7: 
            $current=strtok($thequantity,">");
            $printquantity=$current.">";
            $current=strtok(">");
            $i=0;
            while(strlen($current)>1) {
                if ($i<6) {
	            $_SESSION['theday']=date("D d-m-Y",
                       ($_SESSION['timestamp']-$daynos*60*60*24)); 
                    $printquantity.=$_SESSION['theday'].$sp.$current.">";
                    $daynos--;
                    $i++;
                } else {		    
                    $printquantity.=$current;
                };
                $current=strtok(">");
	    };
	    break;
        }; // End of Switch block

        // Now the normal checks
        $boolean1 = $_SESSION['quantitymissing'][$index] && $quantitymissing;
        $boolean2 = $_SESSION['quantitynointerval'][$index] && $QtyNoInterval;
        $boolean3 = $_SESSION['quantityexcessivepo'][$index] && $QtyExcessivePO;
        $boolean4 = $_SESSION['quantityexcessivelegal'][$index] && $QtyExcessiveLegal;
        $boolean5 = $_SESSION['quantityinsufficient'][$index] && $QtyInsufficient;
        $boolean6 = $_SESSION['quantitynowords'][$index] && $QtyNoWords;
        $boolean7 = $_SESSION['quantitynofigures'][$index] && $QtyNoFigures;
        $booleanquantity = $boolean1 || $boolean2 || $boolean3 || $boolean4 ||
          $boolean5 || $boolean6 || $boolean7;
        $noquantityhanges = !$_SESSION['quantitymissing'][$index] &&
          !$_SESSION['quantitynointerval'][$index] && 
          !$_SESSION['quantityexcessivepo'][$index] && 
          !$_SESSION['quantityexcessivelegal'][$index] &&
          !$_SESSION['quantityinsufficient'][$index] && 
          !$_SESSION['quantitynowords'][$index] && 
          !$_SESSION['quantitynofigures'][$index]; 

        $booleanchange = $booleandrug || $booleanstrength || $booleanform || 
          $booleandose || $booleanquantity;

        // Don't display the second drug if interaction correctly 
        // identified (M) and action is to remove it (D)

//        if ($intaction=="D" && $interaction == "M") {
        if ($suggestion=="D" && 
                           $correctcombination && ($withwhat == $index)) {
            $displaydrug="";
        } else {




            // Use determine DRUG, STRENGTH, FORM and QUANITITY
            // Code for determining what DRUG to show
            if ($booleandrug) { // RED
                $drugcolour = "#EE0000"; // RED
                if ($_SESSION['drugnamemissing'][$index]) { // Name as given
                    $displaydrug=$_SESSION['drugname'][$index];
                } else { // Corrected name
                    if ($_SESSION['drugmispelling'][$index]) { // Correct name if
                        $displaydrug=$_SESSION['drugmispelling'][$index];
                    };
                };
            } else {
                $drugcolour = "#00008C"; // BLUE
                if ($_SESSION['drugnamemissing'][$index]) { // Omit name
                    $displaydrug="";
                } else { // Name as given
                    $displaydrug=$_SESSION['drugname'][$index];
                };
            };

            // Code for determining what STRENGTH to show
            // If (for whatever reason) drug is not to be shown
            // then no strength can be shown either ...
            if (!$displaydrug) {
                $displaystrength = "";
            } else {
                // Default colour is BLUE; use RED for a change
                $strengthcolour = "#00008C";
                if ($booleanstrength) {
                    if ($_SESSION['strengthmissing'][$index]) { // Name as given
                        $strengthcolour = "#EE0000"; // RED
	                $displaystrength=$_SESSION['strength'][$index];
                    } else { // Corrected name
                        if ($_SESSION['strengthcorrect'][$index]) {
                            $strengthcolour = "#EE0000"; // RED
                            $displaystrength=$_SESSION['strengthcorrect'][$index];
                        } else {
                            $displaystrength=$_SESSION['strength'][$index];
                        };
                    };
                } else {
                    if ($_SESSION['strengthmissing'][$index]) { // Omit name
                        $displaystrength="";
                    } else { // Name as given
	                $displaystrength=$_SESSION['strength'][$index];
                    };
                };

                // Logic: If a change is being made in another section (e.g.
                // DRUG), make a change here (if there is one) provided that
                // this change is not meant to arise from this section (i.e.
                // STRENGTH) itself 

                if ($booleanchange && 
                 ($_SESSION['strengthcorrect'][$index]) &&
                               $nostrengthchanges) {
                    $strengthcolour = "#EE0000"; 
                    $displaystrength = $_SESSION['strengthcorrect'][$index];
                };
            };

            // Code for determining what FORM to show
            // Default colour is BLUE; use RED for a change
            // If no drug, then no form either!
            if (!$displaydrug) {
                $displayform = "";
            } else {
                $formcolour = "#00008C";
                if ($booleanform) {
                    if ($_SESSION['deliveryformmissing'][$index]) { // Name as given
                        $formcolour = "#EE0000"; // RED
	                $displayform=$_SESSION['deliveryform'][$index];
                    } else { // Corrected name if it exists
                        if ($_SESSION['formcorrect'][$index]) {
                            $formcolour = "#EE0000"; // RED
                            $displayform=$_SESSION['formcorrect'][$index]; // Correct name
                        } else {
                            $displayform=$_SESSION['form'][$index];
                        };
                    };
                } else {
                    if ($_SESSION['deliveryformmissing'][$index]) { // Omit name
                        $displayform="";
                    } else { // Name as given
	                $displayform=$_SESSION['deliveryform'][$index];
                    };
                };

                if ($booleanchange && $_SESSION['formcorrect'][$index] && $noformchanges) { 
                   $formcolour = "#EE0000"; 
                   $displayform = $_SESSION['formcorrect'][$index];
                };
            };

            // Code for determining what DOSE to show 
            // Code simpler than others because no dose missing option ...
            // If no drug, then no dose either!
            if (!$displaydrug) {
                $displaydose = "";
            } else {
                if ($booleandose) {
                    if ($_SESSION['dosecorrect'][$index]) {
                        $dosecolour = "#EE0000"; // RED
                        $displaydose=$_SESSION['dosecorrect'][$index]; // Correct name
                    } else {
                        $dosecolour = "#00008C"; // BLUE
                        $displaydose=$_SESSION['dose'][$index];
                    };
                } else {
                    $dosecolour = "#00008C"; // BLUE
                    $displaydose=$_SESSION['dose'][$index];
                };
            };

            if ($booleanchange && $_SESSION['dosecorrect'][$index] && $nodosechanges) { 
               $dosecolour = "#EE0000"; 
               $displaydose = $_SESSION['dosecorrect'][$index];
            };

            // Code for determining what QUANTITY to show
            // If no drug, then no quantity either!
            if (!$displaydrug) {
                $displaystrength = "";
            } else {
                $quantitycolour = "#00008C";
                if ($booleanquantity) { // RED
                    if ($_SESSION['quantitymissing'][$index]) { // ... the missing name
                        $quantitycolour = "#EE0000";
                        $displayquantity = $printquantity;
                    } else { // ... the correct name
                        if ($_SESSION['quantitynointerval'][$index]) {
                            $quantitycolour = "#EE0000";
                            $displayquantity = $printquantity;
                        } else {
                            if ($_SESSION['quantitycorrect'][$index]) {
                                $quantitycolour = "#EE0000";
                                $displayquantity = $_SESSION['quantitycorrect'][$index];
                            } else {
                                $displayquantity = $_SESSION['quantity'][$index];
                            };
                        };
                    };
                } else {
                    if ($_SESSION['quantitymissing'][$index]) {
                        $displayquantity = "";
                    } else {
                        $displayquantity = $printquantity;
                    };
                }
                if (!$displayquantity) {$displayquantity=""; };
            };

            if ($booleanchange && $_SESSION['quantitycorrect'][$index]
                          && $noquantityhanges) { 
               $quantitycolour = "#EE0000"; 
               $displayquantity = $_SESSION['quantitycorrect'][$index];
            };
        };

        // Argument is not zero-based ...
        $retainthisdrug = ZRD_RetainDrug($index+1); 
        // N.B. This function tells us whether we need to delete the drug
        // for any reason
        // e.g. the drug is black-listed AND the user has spotted this AND the
        // there is no replacement or the drug should be deleted because of
        // an interaction. 

        // Do not write drug if $retainthisdrug is 0. If it is 1, check to 
        // see whether drug should be replaced.

        if ($retainthisdrug) {
            if (($suggestion=="R" || $suggestion=="S") && 
                           $correctcombination && ($withwhat == $index)) {
                // For a replacement, everything is in RED regardless of why
                // the drug is having to be replaced ...    
                $drugcolour="#EE0000";
                $formcolour="#EE0000";
                $quantitycolour="#EE0000";
                $strengthcolour="#EE0000";
                $dosecolour="#EE0000";
            };

            // Code for displaying DRUG
            if (!$displaydrug) { $displaydrug=""; };
            // If the display is limited and limited to something that isn't
            // name, then don't display the name ...
            if ($limited && $limited != "name") { $displaydrug=""; };
            print("<FONT color=$drugcolour $tface>");
            print(" ".$displaydrug."</FONT>");

            // Code for displaying STRENGTH
            if (!$displaystrength) { $displaystrength=""; };
            // If the display is limited and limited to something that isn't
            // strength, then don't display the strength ...
            if ($limited && $limited != "strength") { $displaystrength=""; };
            // if (!$retaindrug) { $displaystrength=""; };
            print("<FONT color=$strengthcolour $tface>");
            print(" ".$displaystrength." </FONT>");

            // Code for displaying FORM
            if (!$displayform) { $displayform=""; };
            // If the display is limited and limited to something that isn't
            // form, then don't display the form ...
            if ($limited && $limited != "form") { $displayform=""; };
            // if (!$retaindrug) { $displayform=""; };
            print("<FONT color=$formcolour $tface>");
            print(" ".$displayform."</FONT>");

            // This line is required in a "normal" form ...
            if (!$limited){
                if ($displaydrug || $displaystrength || $displayform) {
                    print("<br>");
                };
            };

            // Code for displaying DOSE
            if (!$displaydose) { $displaydose=""; };
            // If the display is limited and limited to something that isn't
            // dose, then don't display the dose ...
            if ($limited && $limited != "dose") { $displaydose=""; };
            // if (!$retaindrug) {$displaydose="";};
            print("<FONT color=$dosecolour $tface>");
            print(" ".$displaydose."</FONT>");

            if ($displaydose) { print("<br>"); };

            // Code for displaying QUANTITY
            if (!$displayquantity) { $displayquantity=""; };
            // If the display is limited and limited to something that isn't
            // quantity, then don't display the quantity ...
            if ($limited && $limited != "quantity") { $displayquantity=""; };
            // if (!$retaindrug) { $displayquantity=""; };
            print("<FONT color=$quantitycolour $tface>");
            print(" ".$displayquantity."</FONT>");

            if ($displayquantity) { print("<br>"); };
        };
    };
    print("</font>");
};

function ZZAC_Magnify_MedicationPanel($index)
{   global $case;
/* Get details of the med verify problem */
    print("<BODY BGCOLOR=".$_SESSION['formcolour'].">");
    $index--;
    $thedrug=$_SESSION['VerifyDrugID'][$index];
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripVerifyDrug","RecordID='$thedrug'");
    foreach ($table as $row)
    {   $namemissing=$row['NameMissing'];
	$namemispelled=$row['NameMispelled'];
	$purpose=$row['Purpose'];
 	$strengthmissing=$row['StrengthMissing'];
 	$formmissing=$row['FormMissing'];
 	$overdose=$row['Overdose'];
 	$underdose=$row['DoseUnderdose'];
 	$doseincomplete=$row['DoseIncomplete'];
 	$dosenotrequired=$row['Dosenotrequired'];
 	$quantitymissing=$row['QuantityMissing'];
 	$drughandwritten=$row['DrugHand'];
	$QtyNoInterval=$row['QuantityNoInterval'];
    }
    $DHW=$_SESSION['drughandwritten'][$index];
    $DHC=$_SESSION['drughandcorrect'][$index];

    $condition = array ('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $entirelyhandwritten = $row['EntirelyHandwritten'];
    };

    if (($DHW!=0 && $DHC!=0 && $drughandwritten!=0) || $entirelyhandwritten) {
        $tface=" face='Lucida Handwriting Italic, serif'";
    };
    print("<h1>");
    if ($_SESSION['purposemarks']>0) { // Error intended ...
    	if ($_SESSION['purpose'] != "0") {
	    $m=$_SESSION['purpose'][$index];
	    print("<FONT $tface>$m</FONT><BR>");
	}
    }
    if ($index==1) print("<P>");
    $m=$_SESSION['drugproblemID'][$index];
    if ($m!="0")
    { 
/* Complex logic simplified by turning all 16 possible combinations */
/* into a number and using the bit positions to signal the action   */
	$thetest=0;
	$nm=$_SESSION['drugnamemissing'][$index];
	if ($nm) $thetest+=8;
	if ($namemissing) $thetest+=4;
	$nm1=$_SESSION['drugnamemispelled'][$index];
	if ($nm1) $thetest+=2;
	if ($namemispelled) $thetest++;
	switch ($thetest)
	{   case 0: case 1: case 2: case 4: case 5: case 6:
		print("<FONT color=#00008C $tface>");
	        $thedrug=$_SESSION['drugname'][$index];
		print(" $thedrug</FONT>"); break;
	    case 12: case 13: case 14:
		print("<FONT color=#EE0000 $tface>");
	        $thedrug=$_SESSION['drugname'][$index];
		print(" $thedrug</FONT>"); break;
	    case 3:  case 7: case 15:
		print("<FONT color=#EE0000 $tface>");
	        $thedrug=$_SESSION['drugmispelling'][$index];
	        print("$thedrug</FONT>");
	    default: print("&nbsp;"); break;
	}
	$thestrength=0;
	$sm=$_SESSION['strengthmissing'][$index];
	if ($sm) $thestrength+=8;
	if ($strengthmissing) $thestrength+=4;
	$su=$_SESSION['strengthunavailable'][$index];
	if ($su) $thestrength+=2;
	if ($strengthunavailable) $thestrength++;
	switch ($thestrength)
	{   case 0: case 1: case 2: case 4: case 5: case 6: 
		print("<FONT color=#00008C $tface>");
		$pstrength=$_SESSION['strength'][$index];
		print(" $pstrength</FONT>"); break;
	    case 12: case 13: case 14:
		print("<FONT color=#EE0000 $tface>");
		$pstrength=$_SESSION['strength'][$index];
		print(" $pstrength</FONT>"); break;
	    case 3: case 7: case 15:
		print("<FONT color=#EE0000 $tface>");
		$pstrength=$_SESSION['strengthcorrect'][$index];
 		print(" $pstrength</FONT>"); break;
	    default: print("&nbsp;"); break;
	}
	$theform=$_SESSION['deliveryform'][$index];
	if ($_SESSION['deliveryformmissing'][$index])
	{   if ($formmissing)
	    {	print("<FONT color=#EE0000 $tface>");
		print(" $theform</FONT>");
	    }
	    else print("&nbsp;");
	}
	else
	{   print("<FONT color=#00008C $tface>");
	    print(" $theform</FONT>");
	}

        /* First line break */
        print("<br>");

	if ($_SESSION['doseoverdose'][$index] || $_SESSION['doseunderdose'][$index] ||
		$_SESSION['doseincomplete'][$index] || $_SESSION['dosenotrequired'][$index])
	{   print("<FONT color=#EE0000 $tface>");
  	    $thedose=$_SESSION['dosecorrect'][$index];
	    print(" $thedose</FONT>");
	}
	else
	{   print("<FONT color=#00008C $tface>");
	    $thedose=$_SESSION['dose'][$index];
	    print(" $thedose</FONT>");
	}


	$thequantity=$_SESSION['quantity'][$index];
/* add in intervals if required */
	$pint=0;
	if ($_SESSION['intervalreq'][$index]>0) $pint+=4;
	if ($_SESSION['quantitynointerval']>0) $pint+=2;
	if ($QtyNoInterval>0) $pint+=1;
	$daynos=date("w",$_SESSION['timestamp']);
	$sp="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	if  ($daynos==0) $daynos=7; $daynos--;
	switch ($pint)
	{   case 0: case 1: case 2: case 3: case 6:
		$printquantity=$thequantity; break;
	    case 4: case 5: case 7: 
	    case 4: case 5: case 7: 
		$current=strtok($thequantity,">");
		$printquantity=$current.">";
		$current=strtok(">"); $i=0;
		while(strlen($current)>1)
		{   if ($i<6)
		   {   $_SESSION['theday']=date("D
d-m-Y",($_SESSION['timestamp']-$daynos*60*60*24));  	 		
$printquantity.=$_SESSION['theday'].$sp.$current.">";  	 	
$daynos--; $i++;   		   }
	    	   else 		    
		       $printquantity.=$current;
 		       $current=strtok(">");
	 	}
		break;
	}
	
        /* Second line break */
        print("<br>");

if ($_SESSION['quantitymissing'][$index])
	{   if ($quantitymissing)
	    {	print("<FONT color=#EE0000 $tface>");
		print(" $printquantity</FONT>");
	    }
	    else print("&nbsp;");
	}
	else
	{   print("<FONT color=#00008C $tface>");
	    print(" $printquantity</FONT>");
	}
    }
    print("</H1><A HREF=javascript:void(0) \n");
    print('onClick="self.close()">');
    print("</FONT></H1>Close help window and return to course</A>");
}  /* end of magnify medication panel */




function ZZD_DoDatePanel($printworddate) {
    global $case;

    if ($printworddate==1) { // ... print the word "Date"
     	print ("Date");
    };

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition = array ('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $entirelyhandwritten = $row['EntirelyHandwritten'];
    };

    if ($_SESSION['patienthandwritten'] || $entirelyhandwritten) {
//        $tface="FACE='Script'";
        $tface="face='Lucida Handwriting Italic, serif'";
    } else {
        $tface="";
    };

// Calculate the date ...
    $daynos=date("w",$_SESSION['timestamp']); 
    if ($daynos==0) { $daynos=7; };
    $daynos--;
    $_SESSION['theday']=date("d-m-Y",($_SESSION['timestamp']-$daynos * 60 * 60 * 24));

    // ... this means all prescriptions come out on a Monday

// See what student thinks at present ...
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripVerify","VerifyID='".$_SESSION['VerifyID']."'");
    foreach ($table as $row) {
        $datemissing=$row['DateMissing'];
        $datefuture=$row['DateFuture'];
        $datelate=$row['DateLate'];
    };

    $condition = array ('LetterCode'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripClassifyMA", $condition);
    foreach ($table as $row) {
        $l1 = $row['legal1'];
        $l2 = $row['legal2'];
    };

    $condition = array ('LegalID'=>$l1);
    $table=$db->select("ScripLegal", $condition);
    foreach ($table as $row) {
        $twentyeightdays = $row['TwentyEightDays'];
    };

    if ($l2) {
        $condition = array ('LegalID'=>$l2);
        $table=$db->select("ScripLegal", $condition);
        foreach ($table as $row) {
            $twentyeightdays2 = $row['TwentyEightDays'];
        };
        $twentyeightdays |= $twentyeightdays2; 
    };

    // This will be the MA for the POR ...
    $_SESSION['dateonprescription']=$_SESSION['theday'];

    if ($_SESSION['group']!= "1" && $_SESSION['group']!= "2") { // No date for requisitions

    // Decide how, and if, it is to be shown ...

// HERE IS THE FONT SOLUTION

print("<font size=1>");
        if ($_SESSION['datemissing']>0) { // Deliberate error: missing
            if ($datemissing) { // Spotted ... in RED
                print("<FONT color=#ff0000 $tface><br>".$_SESSION['theday']."</FONT>");
            } else { // Missed ... print nothing
                $_SESSION['theday']="";
            };
        } elseif ($_SESSION['datefuture']>0) { // Deliberate error: future
            if ($datefuture) { // Spotted ... in RED
                print("<FONT color=#ff0000 $tface><br>".$_SESSION['theday']."</FONT>");
            } else { // Missed ... in BLUE
                // All future-dated by a week ...
                $_SESSION['theday']=date("d-m-Y",($_SESSION['timestamp']+7*24*60*60));
                print("<FONT color=#00008C $tface><BR>".$_SESSION['theday']."</FONT>");
            };
        } elseif ($_SESSION['datelate']>0) { // Deliberate error: late
            if ($datelate) { // Spotted ... in RED
                print("<FONT color=#ff0000 $tface><br>".$_SESSION['theday']."</FONT>");
            } else { // Missed ... in BLUE
                // Out of date by either 35 days (for things that are nominally
                // 28 days) or 28 weeks (nominally six months) ...

                if ($twentyeightdays) {
                    $ood = 35;
                } else {
                    $ood = 28 * 7;
                };
                $_SESSION['theday']=date("d-m-Y",($_SESSION['timestamp']-$ood*24*60*60));
                print("<FONT color=#00008C $tface><BR>".$_SESSION['theday']."</FONT>");
            };
        } else { // No error - in BLUE
            print("<FONT color=#00008C $tface><BR>".$_SESSION['theday']."</FONT>");
        };
print("</font>");
    };
};

function ZZE_DoPrescriberAddress($printNHS) {
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $table=$db->select("ScripVerify","VerifyID='".$_SESSION['VerifyID']."'");
    foreach ($table as $row) {
        $namemissing=$row['NameMissing'];
	$addressmissing=$row['AddressMissing'];
	$addressincomplete=$row['AddressIncomplete'];
	$qualsmissing=$row['QualsMissing'];
	$addressnonuk=$row['AddressNonUK'];
	$qualsinappropriate=$row['QualsInappropriate'];
    };

    if ($_SESSION['group'] != 4 && $_SESSION['group'] != 8) { 
    // The "old" forms (so to speak) ...
    // Check for name problems (except midwives)
    if ($_SESSION['formID']!='18') {
        if ($_SESSION['prescribernamemissing']) { // Meant to be missing
            if ($namemissing) { // User spotted it - red ink ...
                print("<FONT color=#EE0000>");
                print($_SESSION['prescribername']." ");
            } else { // User not spotted it - omit ...
                print(" ");
            };
        } else { // Not meant to be missing - blue ink
            print("<FONT color=#00008C>");
            print($_SESSION['prescribername']." ");
        };
    };

    // Check for qualifications missing
    if ($_SESSION['formID']!='18') {
        if ($_SESSION['qual']=='0') { $_SESSION['qual']=""; };

        if ($_SESSION['prescriberqualsmissing']) { // Meant to be missing
            if ($qualsmissing) { // User spotted it - red ink ...
    	        print("<FONT color=#EE0000>".$_SESSION['qual']." </font>");
	    } else { // User not spotted it - omit ...
                print(" ");
            };
        } else { // Not meant to be missing - blue ink
            print("<FONT color=#00008C>".$_SESSION['qual']." </font>");
        };
    };


    // Address Problems ...
    $fulladdress = "<br>".$_SESSION['practiceName'];
    if ($_SESSION['practiceAddress1'] != "0") {
        $fulladdress .= "<br>".$_SESSION['practiceAddress1']; 
    } else {
        $fulladdress .= "<br>&nbsp;";
    };
    if ($_SESSION['practiceAddress2'] != "0") {
        $fulladdress.= "<br>".$_SESSION['practiceAddress2']; 
    } else {
        $fulladdress.= "<br>&nbsp;";
    };
    if ($_SESSION['practiceAddress3'] != "0") {
        $fulladdress.= "<br>".$_SESSION['practiceAddress3']; 
    } else {
        $fulladdress.= "<br>&nbsp;";
    };
    if ($_SESSION['practiceAddress4'] != "0") {
        $fulladdress.= "<br>".$_SESSION['practiceAddress4']; 
    } else {
        $fulladdress.= "<br>&nbsp;";
    };

    if ($_SESSION['prescriberaddressmissing']) {
        if ($addressmissing) { // Print in RED ...
            print("<font color=#ff0000>$fulladdress</font>");
        } else { // Print nothing ...
            print("<br><br><br><br>");
        }
    } else if ($_SESSION['prescribeaddressincomplete']) {
        if ($addressincomplete) {
            print("<font color=#ff0000>$fulladdress</font>");
        } else {
            $path=rand(0,1);
            $path=1;
            if ($path==0) {
	        print("<BR>".$_SESSION['practiceAddress1']); 
	    } else {
                print("<BR>".$_SESSION['practiceAddress2']); 
            }
            if ($_SESSION['practiceAddress3'] != "0") {
                print("<BR>".$_SESSION['practiceAddress3']);
            } else {
                print("<br>");
            } 
            if ($_SESSION['practiceAddress4'] !="0") {
                print("<BR>".$_SESSION['practiceAddress4']);
            } else {
                print("<br>");
            }
        };
    } elseif ($_SESSION['prescriberaddressnonuk']) { // Non-UK showing
        if ($addressnonuk) { // Spotted - RED
            if ($_SESSION['prescriberukaddress1'] != "0") {
                $fulladdress = "<br>".$_SESSION['prescriberukaddress1']; 
            } else {
                $fulladdress = "<br>&nbsp;";
            }
            if ($_SESSION['prescriberukaddress2'] != "0") {
                $fulladdress.= "<br>".$_SESSION['prescriberukaddress2']; 
            } else {
                $fulladdress = "<br>&nbsp;";
            }
            if ($_SESSION['prescriberukaddress3'] != "0") {
                $fulladdress.= "<br>".$_SESSION['prescriberukaddress3']; 
            } else {
                $fulladdress = "<br>&nbsp;";
            }
            if ($_SESSION['prescriberukaddress4'] != "0") {
                $fulladdress.= "<br>".$_SESSION['prescriberukaddress4']; 
            } else {
                $fulladdress = "<br>&nbsp;";
            }
            print("<font color=#ff0000>$fulladdress</font>");
        } else { // Not spotted BLUE
            print("<font color=#00008c>$fulladdress</font>");
        }
    } else {
        print("<font color=#00008c>$fulladdress</font>");
    }; 

        if ($printNHS==1) {
            print("<FONT color=#00008C SIZE=1>");
            print("<BR> NHS no: ".$_SESSION['nhsnumber']."</FONT>");
        };
    } else {
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
        $condition = array('FormID'=>$_SESSION['formID']);
        $table = $db->select("ScripForms", $condition);
        foreach ($table as $row){
            $PrivateCD = $row['PrivateCD'];
            $Supplementary = $row['Supplementary'];
            $Hospital = $row['Hospital'];
        };

        $condition = array('PrescriberID'=>$_SESSION['secondaryPrescriberID']);
        $table = $db->select("ScripPrescriber", $condition);
        foreach ($table as $row){
            $seniorGPName = $row['Name'];
            $seniorGPPracticeID = $row['PracticeID'];
        };

        $condition = array('PracticeID'=>$seniorGPPracticeID);
        $table = $db->select("ScripPractice", $condition);
        foreach ($table as $row){
            $seniorGPPracticeCode = $row['Code'];
            $seniorGPAddress1 = $row['Address1'];
            $seniorGPAddress2 = $row['Address2'];
            $seniorGPPostCode = $row['PostCode'];
            $seniorGPTelNo = $row['TelephoneNumber'];
            $seniorGPTrustID = $row['TrustID'];
        };

        $condition = array('ID'=>$seniorGPTrustID);
        $table = $db->select("ScripTrust", $condition);
        foreach ($table as $row){
            $seniorGPTrustName = $row['Name'];
            $seniorGPTrustCode = $row['Code'];
        };

        print("<font color='#00008C'>");
/*        if ($Hospital) {
            print($_SESSION['practiceName']);
            print($_SESSION['practiceCode']<br>);
            print($_SESSION['trustAddress1']."<br>");
            print($_SESSION['trustAddress2']."<br>");
            print($_SESSION['trustAddress3']);
            print($_SESSION['trustPostCode']."<br>");
            print($_SESSION['trustTelNo']."<br>");
            print($_SESSION['trustName']);
            print($_SESSION['trustCode']."<br>");
        } else {
            print($_SESSION['prescribername']);
            if ($PrivateCD) {
                print($_SESSION['prescriberPrivateCD']);
            } else {
                print($_SESSION['nhsnumber']);
            };
            print("<br>"); // End of line 
            print("<br>"); // Blank line
            if ($Supplementary){
                print("$seniorGPName");
                print("$seniorGPPracticeCode<br>");
                print("$seniorGPAddress1<br>");
                print("$seniorGPAddress2");
                print("$seniorGPPostCode<br>");
                print("$seniorGPTelNo<br>");
                print("$seniorGPTrustName");
                print("$seniorGPTrustCode<br>");
            } else {
                print($_SESSION['practiceAddress1']."<br>");
                print($_SESSION['practiceAddress2']."<br>");
                print($_SESSION['practiceAddress3']);
                print($_SESSION['practicePostCode']."<br>");
                print($_SESSION['practiceTelNo']."<br>");
                print($_SESSION['trustName']);
                print($_SESSION['trustCode']."<br>");
            }; 
        };
        print("</font>"); // End dark font
*/

    print("<div class='tableblank'>");
    print("<table border='0' bordercolor='#ffff00' width='100%'>");
    //print("bordercolorlight='#ffffff' bordercolordark='#ffffff'");
    print("<tr>");
    print("<td width='70%'>");
        if ($Hospital) {
            print($_SESSION['practiceName']."<br>");
            print($_SESSION['trustAddress1']."<br>");
            print($_SESSION['trustAddress2']."<br>");
            print($_SESSION['trustAddress3']);
            print($_SESSION['trustPostCode']."<br>");
            print($_SESSION['trustTelNo']."<br>");
            print($_SESSION['trustName']."<br>");
        } else {
            print($_SESSION['prescribername']);
            print("<br>"); // End of line 
            print("<br>"); // Blank line
            if ($Supplementary){
                print("$seniorGPName<br>");
                print("$seniorGPAddress1<br>");
                print("$seniorGPAddress2<br>");
                print("$seniorGPTelNo<br>");
                print("$seniorGPTrustName<br>");
            } else {
                print($_SESSION['practiceAddress1']."<br>");
                print($_SESSION['practiceAddress2']."<br>");
                print($_SESSION['practiceAddress3']."<br>");
                print($_SESSION['practiceTelNo']."<br>");
                print($_SESSION['trustName']."<br>");
            }; 
        };
        print("</font>"); // End dark font

print("</td>");
print("<td width='30%'>");
        if ($Hospital) {
            print($_SESSION['practiceCode']."<br>");
            print("<br>");
            print("<br>");
            print("<br>");
            print("<br>");
            print("<br>");
            print($_SESSION['trustCode']."<br>");
        } else {
            if ($PrivateCD) {
                print($_SESSION['prescriberPrivateCD']);
            } else {
                print($_SESSION['nhsnumber']);
            };
            print("<br>"); // End of line 
            print("<br>"); // Blank line
            if ($Supplementary){
                print("$seniorGPPracticeCode<br>");
                print("<br>");
                print("$seniorGPPostCode<br>");
                print("<br>");
                print("$seniorGPTrustCode<br>");
            } else {
                print("<br>");
                print("<br>");
                print($_SESSION['practicePostCode']."<br>");
                print("<br>");
                print($_SESSION['trustCode']."<br>");
            }; 
        };
        print("</font>"); // End dark font
print("</td>");
print("</tr>");
print("</table>");
print("</div>");

    };
};

function ZZF_DoSignature($printit) {
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    if ($printit==1) {
        print("Signature of ");
        if ($_SESSION['formID']=='7') {
            print("Dentist");
        } else {
            print("Prescriber");
        };
        print("<br>");
    };

    $table=$db->select("ScripVerify","VerifyID='".$_SESSION['VerifyID']."'");
    foreach ($table as $row) { // Has the user marked the signature as missing?
        $sigincorrect=$row['SigIncorrect'];
        $sigmissing=$row['SigMissing'];
        $sigauth=$row['SigAuth'];
        $qualsinappropriate=$row['QualsInappropriate'];
    };

    // Show signature if either it is NOT a deliberate error or
    // it is a deliberate error but the user has spotted it ...
    if ($_SESSION['prescribersigmissing']<1 || $sigmissing) {
        // Show the wrong signature is wrong signature is to be shown
        // and user hasn't spotted it (two causes) ...
        if ($_SESSION['prescriberqualsinappropriate'] && !$qualsinappropriate
            || $_SESSION['prescribersigincorrect'] && !$sigincorrect) {
     	    $sigpath="sigs/".$_SESSION['incorrectSigFile'].".gif";  
        } else {
            $sigpath="sigs/".$_SESSION['sigfile'].".gif";
        };
	print("<IMG SRC=$sigpath>");
    };

    // TWO signatures required for a midwife's requisition (18) ...
    if ($_SESSION['formID']=='18') {

        print("</p>");
        print("<P>Authorising Officer: ");

        // Find the .gif file for the authorising agent (if non-zero) ...

        if ($_SESSION['authorisingID']) {
            $condition = array("PrescriberID"=>$_SESSION['authorisingID']); 
            $table=$db->select("ScripPrescriber",$condition); 
            foreach ($table as $row) {
                $_SESSION['sigAOfile']=$row['SigFileName'];
            };

            // Show the signature if either it is not a deliberate error
            // or if it is but the user has spotted its omission ...
            if ($_SESSION['prescriberauthoriseneeded']<1 || $sigauth) {
                $sigAOpath="sigs/".$_SESSION['sigAOfile'].".gif";  
                print("<IMG SRC=$sigAOpath>");
            };
            print("</p>");
        };
    };

};  /* end of function do signature */

function ZZG_DoPharmacyPanel() {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition = array ('EndorseID'=>$_SESSION['EndorseID']);
    $table=$db->select("ScripEndorse",$condition);
    foreach ($table as $row) {
       $NottStamp=$row['NottStamp'];
    };

    $condition=array("RecordID"=>$_SESSION['studentrecordID']);

    switch ($_SESSION['categoryToMod']){
        case 8:
        $table=$db->select("ScripStudentRecordHome",$condition);
        break;

        case 1: case 2: case 3:
        $table=$db->select("ScripStudentRecord",$condition);
        break;

        case 4: case 5: case 6:
        $table=$db->select("ScripStudentRecordExam",$condition);
        break;
    };


    foreach ($table as $row) {
	$datedone=$row['DateDone'];
    };

    if ($NottStamp=="1") {
     	print("<IMG SRC='images/pharmacy.gif'>");
	print("<P>$datedone</P>");
    }
};

function ZZG_DoDrugAmendments($drug) {
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $index = $drug - 1; // Zero-based ...
    // Get correct line in table ...
    if ($index == 0) {
        $condition = array ('ID'=>$_SESSION['AmendmentsDrugID']);
    } elseif ($index == 1)  {
        $condition = array ('ID'=>$_SESSION['AmendmentsDrug2ID']);
    };

    // Read it ...
    $table = $db->select("ScripAmendmentsDrug", $condition);



    // Loop through most of ScripAmendmentsDrug (other than the
    // fields that are administrative) and DrugHand and, where not
    // nul, show them in the endorsements column ...
    foreach ($table as $row) {
        foreach ($row as $key=>$value) {
            if ($key != 'ID' && $key != 'CaseID' && $key != 'StudentID'
                         && $key != 'DrugNo' && $key != 'DrugHand') {
                if ($value) { // Take any pipe marks out before displaying
                    $valueNoPM = trim(str_replace("|", "", $value));
                    if ($valueNoPM) {
                        print("<font color='#007f00'>$valueNoPM</font><br>");
                     };
                 };
            };
        };
    };
};

function ZZG_DoEndorsePanel() {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition = array ('EndorseID'=>$_SESSION['EndorseID']);
    $table=$db->select("ScripEndorse", $condition);
    foreach ($table as $row) {
        $PORStamp1=$row['PORStamp1'];
        $PORStamp2=$row['PORStamp2'];
        $PackStamp1=$row['PackStamp1'];
        $PackStamp2=$row['PackStamp2'];
    };

/*
    $condition = array('EndorseID'=>$_SESSION['EndorseID']);
    $table = $db->select("ScripEndorse", $condition);
    foreach ($table as $row){
        $amendment = $row['Amendment'];
    };
*/

    print("<table>");

    print("<tr><td>\n");
    ZZG_DoDrugAmendments(1);
    print("</td></tr>");

    // Drug 1 endorsements ...
    print("<tr><td>\n");
    if ($PORStamp1=="1") { print("POR<BR>"); };
    if ($PackStamp1=="1") { print("Pack Size<BR>"); };
    print("</td></tr>");

    print("<tr><td>\n");
    ZZG_DoDrugAmendments(2);
    print("</td></tr>");

    // Drug 2 endorsements ...
    print("<tr>");
    print("<td align='middle'>\n");
    if ($PORStamp2=="1") { print("POR<BR>"); };
    if ($PackStamp2=="1") { print("Pack Size<BR>"); };
    print("</td></tr>");

    print("</table>\n");
};

function ZZL_DoHospitalSignature(){
    global $case;
    print("<font color='#00008C'>");
    print($_SESSION['prescribername']." [".$_SESSION['beepnumber']."]");
    print("</font>");
};

function ZZM_DoTwoLetters(){
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition = array ('FormID'=>$_SESSION['formID']);
    $table = $db->select("ScripForms", $condition);
    foreach ($table as $row){
        $twoLetters = $row['Letters'];
    };
    // Use a blank line if no letters ...
    if (!$twoLetters) { $twoLetters = "&nbsp;"; };
    print("<p align='center'><b><font color='#00008C' size='+1'>");
    print("$twoLetters");
    print("</font></b></p>");
};

function ZZM_DoBarCode(){
    // Inserts the bar code on the prescription ...
    print("<p align='center'>");
    print("<img src='images/rx_barcode.gif'>");
    print("</p>");
};

function ZX_GetID() {
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $condition = array('RecordID'=>$_SESSION['studentrecordID']);

    switch ($_SESSION['categoryToMod']){
        case 8:
        $table=$db->select("ScripStudentRecordHome",$condition);
        break;

        case 1: case 2: case 3: case 7:
        $table=$db->select("ScripStudentRecord",$condition);
        break;

        case 4: case 5: case 6:
        $table=$db->select("ScripStudentRecordExam",$condition);
        break;
    };

    foreach ($table as $row){
        $_SESSION['studentrecordID']=$row['RecordID']; 
        $_SESSION['FinishedYN']=$row['FinishedYN']; 
        $_SESSION['ClassifyID']=$row['ClassifyID']; 
        $_SESSION['VerifyID']=$row['VerifyID']; 
        $_SESSION['RegisterID']=$row['RegisterID']; 
        $_SESSION['LabelID']=$row['LabelID']; 
        $_SESSION['LabelID2']=$row['LabelID2']; 
        $_SESSION['EndorseID']=$row['EndorseID'];
        $_SESSION['ScoreID']=$row['ScoreID']; 
        $_SESSION['VerifyDrugID'][0]=$row['VerifyDrugID']; 
        $_SESSION['VerifyDrugID'][1]=$row['VerifyDrugID2'];
        $_SESSION['DateDone'] = $row['DateDone'];
        $_SESSION['ProbeID']=$row['ProbeID'];
        $_SESSION['PMRViewedID']=$row['PMRViewedID'];
        $_SESSION['InteractionID']=$row['InteractionID'];
        $_SESSION['AmendmentsID']=$row['AmendmentsID'];
        $_SESSION['AmendmentsDrugID']=$row['AmendmentsDrugID'];
        $_SESSION['AmendmentsDrug2ID']=$row['AmendmentsDrug2ID'];
    };
};

function ZRD_RetainDrug($i) {
    // Parameter is 1 (for first drug) or 2 (for second)

    // What this function does:
    // We need to know whether a certain drug should be shown. The function
    // accepts 1 or 2 as a parameter (first drug or second drug) and
    // returns either 0 (false) or 1 (true). The variable $retaindrug
    // starts as 1 (true) and then the code looks for reasons why it
    // should be set to 0 (false).

    global $case;

    $i--; // Revert to zero-based variable
    $retaindrug='1'; // Starting value is TRUE

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Take current latter and find where problems are stored
    $condition=array('Letter'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripsScrips",$condition);    
    foreach ($table as $row) {
        if ($i=='0') { // FIRST drug
            $drugprobid=$row['DrugProblemID'];
        } else { // SECOND drug
            $drugprobid=$row['DrugProblemID2'];
        };
        $miscprobid=$row['MiscProblemID'];
    };

    // Only non-drug specific reason for non-display is "Interaction"

    // Get the student's thinking on interactions ...
    $condition = array ('ID'=>$_SESSION['InteractionID']);
    $table=$db->select("ScripInteraction",$condition);
    foreach ($table as $row) {
        $correctcombination = $row['CorrectCombination'];        
    };

    // ... and the model answer on interactions ...
    $condition = array ('CaseID'=>$_SESSION['currentscrip']);
    $table=$db->select("ScripInteractionMA",$condition);
    foreach ($table as $row) {
        $suggestion = $row['Suggestion'];        
        $deletewhich = $row['DeleteWhich'];
        $withwhat = $row['WithWhat'];
    };
    $deletewhich--; // Make zero-based
    if ($withwhat != '12') { $withwhat--; }; // Make zero-based

    // Which drug is to be deleted? Answer: the one that is interacting
    // with the PMR. However, if the interaction is between the two drugs
    // on the script then we need to specify which is removed. This is
    // "DeleteWhich" in ScripInteractionMA.
    if ($withwhat == '12') {
        $tobedeleted = $deletewhich;
    } else {
        $tobedeleted = $withwhat;
    };

    // Delete drug if student has found correct combination
    // AND correct action is to delete AND the drug to be
    // deleted is the one we are currently considering ... 
    if ($correctcombination && $suggestion == "D"
                           && ($tobedeleted == $i)) {
        $retaindrug = '0';
    };

    // All other reasons for deletion are drug-specific

    // $drugprobid should be non-zero ...
    if ($drugprobid) {
        // Get problems
        $condition=array('DrugProblemID'=>$drugprobid);
        $table=$db->select("ScripDrugProblem",$condition);    
        foreach ($table as $row) {
            $drugmispelling=$row['DrugMispelling'];
            $druginappropriatechoice=$row['DrugInappropriateChoice'];
            $drugnonexist=$row['DrugNonExist'];
            $blacklisted=$row['Blacklisted'];
            $notformulary=$row['NotFormulary'];
            $wrongpractitioner=$row['WrongPractioner'];
        };

        // Get user's thoughts
        $condition=array('RecordID'=>$_SESSION['VerifyDrugID'][$i]);
        $table=$db->select("ScripVerifyDrug",$condition);    
        foreach ($table as $row) {
            $inappropriate=$row['Inappropriate'];
            $nxst=$row['NonExist'];
            $blcklstd=$row['Blacklisted'];
            $ntfrmlry=$row['NotFormulary'];
            $wrngprcttnr=$row['WrongPractioner'];
        };

        // Logic: Don't retain if the problem has been set up
        // and user has spotted it and there is no replacement
        // i.e. retain if NOT (...) Now AND all these together
        // since just one "Don't retain" is enough to not
        // display drug

        $retaindrug &= !($druginappropriatechoice && $inappropriate &&
                                                !$drugmispelling);
        $retaindrug &= !($drugnonexist && $nxst &&
                                                !$drugmispelling);
        $retaindrug &= !($blacklisted && $blcklstd &&
                                                !$drugmispelling);
        $retaindrug &= !($notformulary && $ntfrmlry &&
                                                !$drugmispelling);
        $retaindrug &= !($wrongpractitioner && $wrngprcttnr &&
                                                !$drugmispelling);  
    } else {
        // If there is no second drug, then obviously we don't keep it!
        $retaindrug='0';
    };
    return $retaindrug;
};



function HQ_updatelabel() {
//function HQ_updatelabel($qty, $theDrugNumber, $directions,
//	 $refno, $patient, $ancil, $freebox1, $freebox2, $labelid) {

    // This is an amended version of the function with the same 
    // name in ScripWare. The original writes the data away to file;
    // this version READS that data.


    global $case;

/*    $text=array("Quantity"=>$qty,
                "Formname"=>$formname, 
                "Directions"=>$directions,
                "Refno"=> $refno, 
                "Patient"=> $patient,
                "Ancillary"=>$ancil,
                "FreeBox1"=>$freebox1,
                "FreeBox2"=>$freebox2,
                "BrandID"=>$theDrugNumber);*/

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

/*    $condition = array('LabelID'=>$labelid);
    $db->update("ScripLabel", $text, $condition); 

    print("<TR><TD BGCOLOR=#003163 WIDTH=800 ALIGN=CENTER>");
    print("<h2>Label saved</h2>");*/

    // Lok at two parts of label ...
    print("<p>Note that any invalid codes are still shown as codes.</p>");

    $db->connect() or die ($db->getError());
    $condition=array("StudentID"=>$_SESSION['studentID'],
		"CaseID"=>$_SESSION['currentscrip']);

/*
    switch ($_SESSION['categoryToMod']){
        case 8:
        $table=$db->select("ScripStudentRecordHome",$condition);
        break;

        case 1: case 2: case 3:
        $table=$db->select("ScripStudentRecord",$condition);
        break;

        case 4: case 5: case 6:
        $table=$db->select("ScripStudentRecordExam",$condition);
        break;*/



/*
    switch ($_SESSION['programMode']) {
        case "U":
            $table=$db->select("ScripStudentRecordHome", $condition);
            break;
        case "P":
            $table=$db->select("ScripStudentRecord", $condition);
            break;
        case "E":
            $table=$db->select("ScripStudentRecordExam", $condition);
            break;
    };*/

/*    foreach ($table as $row) {
     	$labelID=$row['LabelID'];
    	$labelID2=$row['LabelID2'];
	$datedone=$row['DateDone'];
    };
*/



     	$labelID=$_SESSION['LabelID'];
    	$labelID2=$_SESSION['LabelID2'];
	$datedone=$_SESSION['DateDone'];



    // Draft for Label 1 ...
    if (ZRD_RetainDrug(1)) {
        JHZE_DrawLabel($labelID,$datedone);
    };

    // Draft for Label 2 (if it exists) ...
    if ($labelID2>0) {
     	print("<TR><TD COLSPAN=3><P>&nbsp;</P></TD></TR>\n");
	print("<TR><TD COLSPAN=3 ALIGN=CENTER>\n");
        if (ZRD_RetainDrug(2)) {
            JHZE_DrawLabel($labelID2,$datedone);
        };
    };
    print("</td></tr>\n");
    print("</TD></TR>");
};


function JHZE_DrawLabel($labelID,$datedone) {
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $caseID=$_SESSION['currentscrip'];
    $condition=array("LabelID"=>$labelID);
    $table=$db->select("ScripLabel",$condition);
    foreach ($table as $row) {
        $quantity=$row['Quantity'];
        $name=$row['Formname'];
        $directions=$row['Directions'];
        $refno=$row['Refno'];
        $patient=$row['Patient'];
        $ancil=$row['Ancillary'];
        $freebox1=$row['FreeBox1'];
        $freebox2=$row['FreeBox2'];
        $brandID = $row['BrandID'];
    };

    // If student has specified name, strength and form then
    // find out what this is from relevant table ...
    if ($brandID) {
        $condition=array("ID"=>$brandID);
        $table=$db->select("ScripCDPageTitleAndLabel",$condition);
        foreach ($table as $row) {
            $name = $row['Brand'];
            $name .= " ".$row['Strength'];
            $name .= " ".$row['Form'];
        };
    };

    // Anything read as zero should be nul string ...
    if ($quantity=="0") { $quantity=""; };
    if ($name=="0") { $name=""; };
    if ($directions=="0") { $directions=""; };
    if ($patient=="0") { $patient=""; };
    if ($ancil=="0") { $ancil=""; };
    if ($freebox1=="0") { $freebox1=""; };
    if ($freebox2=="0") { $freebox2=""; };

    $condition=array("Letter"=>$caseID);
    $table=$db->select("ScripsScrips",$condition);
    foreach ($table as $row) {
        $formid=$row['FormID'];
    };

    // All types of label are now produced from one function
    // only (there were previously several) ...
    JHD_AllTypes($quantity, $directions, 
		$datedone, $patient, $ancil,
                $freebox1, $freebox2, $refno, $name);

/*
    switch ($formid) {
  //      case 8: JHA_OTC(); break;

	case 1: case 19: JHB_Emergency($quantity, $directions, 
		$datedone, $patient, $ancil,$refno,$name); break;

	default: JHC_others($quantity, $directions, $datedone, $patient,
		$ancil,$refno,$name); break;
    };*/
};


function JHD_AllTypes($quantity, $directions, $datedone,
	$patient,$ancil,$freebox1, $freebox2, $refno, $name) {
    // N.B. $name is NSF ...
    global $case;
    print("<TABLE BORDER=2 WIDTH=80%><TR>\n");
    print("<TD ALIGN=CENTER><FONT>$quantity");
    print("&nbsp;&nbsp;&nbsp;&nbsp;$name");
    print("<BR>");
    JHZD_Print_Directions($directions);
    print("<br>");
    JHZE_PrintAncil($ancil);
    print("<BR>&nbsp; $refno");

    $year=strtok($datedone,"-");
    if (!$year) { $year="--"; };
    $month=strtok("-");
    $day=strtok("-");
  
    // Vet labels have more information ... and with a slightly
    // different layout ...
    if ($_SESSION['formID'] =='13') {
        print("<br>$freebox1  &nbsp;&nbsp;&nbsp;&nbsp; $patient");
        print("<BR>$day/$month/$year &nbsp;&nbsp;&nbsp;&nbsp;");
        print("$freebox2");
    } else {
        print("<BR>$day/$month/$year &nbsp;&nbsp;&nbsp;&nbsp;");
        print("$patient");
    };;

    print("</FONT>");
    print("<P ALIGN=CENTER><EM>The Dispensary, School of Pharmacy, ");
    print("Nottingham  NG7 2RD</EM></P>\n");
    print("</TD></TR></TABLE>");
    print("</TD></TR><TR><TD>");
};

function JHZD_Print_Directions($directions) {
    global $case;
    $item=strtok($directions," ");
    $db=new dbabstraction();
    $db->connect() or die($db->getError());
    while ($item) {
        $found='0';
     	$table=$db->select("ScripDoseCodes",array("Code"=>$item));
	foreach ($table as $row) {
	    $pitem=$row['Text'];
	    $pcode=$row['Code'];
            if (trim(strtoupper($pcode))==trim(strtoupper($item))) {
                $found='1';
	        print("$pitem ");
            };
        };
        if (!$found) { print("$item "); };
	$item=strtok(" ");
    };
};

function JHZE_PrintAncil($ancil) {
    global $case;
    $item=strtok($ancil," ");

    $db=new dbabstraction();
    $db->connect() or die($db->getError());
    while ($item) {
        $found='0';
     	$table=$db->select("ScripLabelCodes",array("Code"=>$item));
	foreach ($table as $row) {
	    $pitem=$row['Text'];
	    $pcode=$row['Code'];
            if (trim(strtoupper($pcode))==trim(strtoupper($item))) {
                $found='1';
	        print("$pitem ");
            };
        };
        if (!$found) { print("$item "); };
	$item=strtok(" ");
    }
};

function Y_Show($showWhat){
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // The user (student or moderator) wants to see the either the script or the label
    // for $_SESSION['caseID'] (e.g. CB4). If this has not been displayed before then all the data
    // relating to it must be read (if it has, though, there is no need to bother). 

//    if ($_SESSION['caseID'] != $_SESSION['currentscrip']) {
        $_SESSION['currentscrip'] = $_SESSION['caseID'];
        // Read the variables ...
        ZA_GetScripTable();
        ZB_GetPatientProblemTable();
        ZC_GetPatientDetails();
        ZD_GetDrugProblemDetails(1);
        ZD_GetDrugProblemDetails(2);
        ZE_GetPrescriberProblemDetails();
        ZF_GetPrescriberDetails();
        ZG_GetMiscProblemDetails();
        ZX_GetID();
        ZK_GetVerify();
        ZL_GetVerifyDrug(1);
        ZL_GetVerifyDrug(2);
//    };

/*
    switch ($_SESSION['categoryToMod']){
        case 8:
        $table=$db->select("ScripStudentRecordHome",$condition);
        break;

        case 1: case 2: case 3:
        $table=$db->select("ScripStudentRecord",$condition);
        break;

        case 4: case 5: case 6:
        $table=$db->select("ScripStudentRecordExam",$condition);
        break;
    };
*/
    print("<html>");
    print("<head>");
    print("<title>");
print("***");
    print($_SESSION['firstname']." ".$_SESSION['lastname']." - ".$_SESSION['caseID']);
    print("</title>");

    print('<link href="pharmacy_css.css" rel="stylesheet"');
    print(' type="text/css">');
    print('<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">');

    print("</head>");

    print("<body onBlur='window.close()'>");

//print("<pre>");print_r($_SESSION);

    if (strtolower($showWhat) == "script") {
        D_displayscrip();
    } else if (strtolower($showWhat) == "label") {
        HQ_updatelabel();
    };

    print("</body>");
    print("</html>");
};

function ZZ_DoAllMarkCalculations(){
    // 

    EA_MainResults();
    FA_CDResults();
    GA_PORResults();
    HA_CommsResults();

    // Calculate the sub-totals 
    ZH_subtotalsOTC();
    ZG_subtotalsPOR();
    ZF_subtotalsCD();
    ZD_subtotalsChanges();
    ZE_subtotalsMain();
    ZI_subtotalsCombine();

};


function Z_TimesAndPlaces(){
    // Show the available options:

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    global $_POST;

    print("<tr><td colspan='2' align='center'>");

    // Read the contents of the table ScripTimesAndPlaces ... 
    $table = $db->select("ScripTimesAndPlaces");
    foreach ($table as $row){
        $PK = $row['PK'];
        $time[] = $row['Time'];
        $place[] = $row['Place'];
        $available[] = $row['Available'];
    };

    if (!count($uniqueTime)){
        $uniqueTime = array();

        // Compile a list of times ...
        for ($i = 0; $i < count($time); $i++){
            $new = 1;
            for ($j = 0; $j < count($uniqueTime); $j++){
                if ($time[$i] == $uniqueTime[$j]){
                    $new = 0;
                };
            };
            if ($new){
                // Add to $uniqueTime
                array_push($uniqueTime, $time[$i]);
            };
        };
    };

    if (!count($uniquePlace)){
        $uniquePlace = array();

        // Compile a list of places ...
        for ($i = 0; $i < count($place); $i++){
            $new = 1;
            for ($j = 0; $j < count($uniquePlace); $j++){
                if ($place[$i] == $uniquePlace[$j]){
                    $new = 0;
                };
            };
            if ($new){
                // Add to $uniquePlace
                array_push($uniquePlace, $place[$i]);
            };
        };
    };

    // Build an example table (such as a moderator will see) ...
    print("<form action='ScripModerate.php' method='post'>");

    // Prompt
    print("<p>Specify the scripts that you wish to moderate and then click Submit.</p>");


    $counter = 0;
    print("<center><br><table border ='1'>");
    for ($i = 0; $i < count($uniquePlace) + 1; $i++){
        print("\n<tr>");
        for ($j = 0; $j < count($uniqueTime) + 1; $j++){
            print("<td>");
            if ($i == 0){ // Title row with time headers
                if ($j == 0){ // First cell
                    // Empty cell
                } else { // Later cell
                    print("<font color='#ffffff'>" . $uniqueTime[$j - 1] . "</font>"); // ... the headers
                };
            } else { // All later rows
                if ($j == 0){ // First cell in a later row: places
                    print("<font color='#ffffff'>" . $uniquePlace[$i - 1] . "</font>"); // ... column with headers for places
                } else { // Later cells in later rows: availability
                    $condition = array('Time'=>$uniqueTime[$j - 1], 'Place'=>$uniquePlace[$i - 1]);
                    $table = $db->select("ScripTimesAndPlaces", $condition);

                    $X = "-";
                    foreach ($table as $row){
                        $X = $row['Available'];
                    };

                    $chosenPlace[$counter] = $uniquePlace[$i - 1];
                    $chosenTime[$counter] = $uniqueTime[$j - 1];

                    if ($X == "-") {
                        print("&nbsp;");
                    } else {
                        print("<input type='radio' name='combination' value='" . $counter . "'");
                        if (!$X) { print(" disabled='disabled'"); };
                        print(">");
                    };
                    $counter++;
                };
            };
            print("</td>\n");
        };
        print("</tr>");
    };
    print("</table></center>");

    // Store the arrays ...
    print("\n\n<input type='hidden' name='chosentime' value='" . implode("|", $chosenTime) . "'>\n\n");
    print("\n\n<input type='hidden' name='chosenplace' value='" . implode("|", $chosenPlace) . "'>\n\n");

    // Submit button ...
    print("<input type='submit' value='Submit'>");

    print("</form>");



};



function Q_html_end() {
    print ("</table></td></tr></table>");
    print("</body></html>\n");
};

