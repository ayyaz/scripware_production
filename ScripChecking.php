<?php
session_start();
session_register("checking");
require 'dbabstraction.php';

class checkingclass {
    var $nQuestions, $nExercises, $weekNumber, $anyRecordsFound,
        $table, $ID, $authority, $letterThisWeek, $launcher,
        $username, $firstname, $lastname, $seriousError,
        $questionInUse, $category, $theCheckingPracticeCode, $setName;
}; 

// This is the Checking Program. It is based on the original (ScripCheckingAssessment).

// Set the value of the serious error (was 25; now 35) ...
$_SESSION['seriousError'] = 40;


$showQuestionsTab = 60;

// In PHP5 must explicitly set $tab ...
if (isset($_GET['tab'])){ $tab = $_GET['tab']; };
if (isset($_POST['tab'])){ $tab = $_POST['tab']; };

A_html_begin("Scripware - Checking", $tab);


if (!isset($tab)) {
    // New session variable ...
//    $checking=new checkingclass();

    // Clear the session variable ...
    $_SESSION = array(); 
    $_SESSION['seriousError'] = 40;

    // From the $_GET variable ...
    $_SESSION['table']=$_GET['t'];
    $_SESSION['ID']=$_GET['i'];
    $_SESSION['authority']=$_GET['a'];
    $_SESSION['inTheDispensary']=$_GET['d'];
    $_SESSION['dispensaryMode']=$_GET['m'];
    $_SESSION['checkingMode']=$_GET['c'];
    $_SESSION['checkingMachine']=$_GET['p'];

// JCH addition:
    $_SESSION['inTheDispensary']=1;
    $_SESSION['checkingMode']=1;
    $_SESSION['checkingMachine']=1;
    $_SESSION['dispensaryMode']='PC';

print("BBBB");

/* Commentary: 
    table: Which table does the user appear in?
    ID: What is the username of the user in that table (via ID)?
    authority: What authority does the user have? "A" is adminstrator; 
      "M" is moderator; "S" is student. Note that !users in table "Student"
      have "M" authority
    inTheDispensary is a number: If 0, false; if <> 0, true
    dispensaryMode is a letter: If "U" unsupervised; if "P" practice,
      if "E" examination
    mode: Checking (C) or not checking?
    machine: Is one allowed to run checking on this PC?
*/

    // In February 2007, this variable was fixed
    $_SESSION['nExercises'] = 4;
    // ... we now want to make it a variable


    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array('StudentID'=>$_SESSION['ID']);
    $table = $db->select("ScripStudent", $condition);
    foreach ($table as $row){
        $_SESSION['username'] = $row['UserName'];
        $_SESSION['firstname'] = $row['FirstName'];
        $_SESSION['lastname'] = $row['LastName'];
    };

    ZAS_AttendanceSheet("Checking Week");

    // The line below applied when there was no attendance sheet ...
//    C_ShowInstructions(0); // ... no warning
    // ... now the attendance sheet is shown first and it is this which calls C_ShowInstructions

} else {

//print("<br><pre>");
//print_r($HTTP_POST_VARS);
//echo $HTTP_POST_VARS;
//print("</pre>");

    switch ($tab) {
        case 20: // Read user's answers ...



        $db=new dbabstraction();
        $db->connect() or die ($db->getError());


        // Read the student's checking practice code ...
//        $condition = array('StudentID'=>$_SESSION['ID']);
//        $table = $db->select("ScripStudent", $condition);
//        foreach ($table as $row){
//            $_SESSION['theCheckingPracticeCode'] = $row['CheckingPracticeCode'];
//        };

//        $finishMode = $HTTP_POST_VARS['finishmode'];
        // The user has finished (of his own volition or of the timer's): the program has not crashed. Need to update (if an existing entry) or insert (if none) ...

        // Put 1 into the FinishMode field of ScripCheckingFinish for this user doing this set (not straightforweard since may already be an entry) ...
        $text = array("FinishMode"=>1);
        $condition = array("PracticeCode"=>$_SESSION['theCheckingPracticeCode'], "Student"=>$_SESSION['ID'], "SetName"=>$_SESSION['letterThisWeek']);
        $table = $db->select("ScripCheckingFinish", $condition);
        if ($table){ 
            $db->update("ScripCheckingFinish", $text, $condition);
        } else {
            $text = array_merge ($text, $condition);
            $db->insert("ScripCheckingFinish", $text);
        };

        $condition = array ('StudentID'=>$_SESSION['ID'], 'SetLetter'=>$_SESSION['letterThisWeek']);
        $text = array ('FinishMode'=>$finishMode);
        $db->update("ScripCheckingTimes", $text, $condition);

        // This was the original code i.e. when the answers were not revealed immediately ...
        EE_AfterSubmitting();

        // This is the new code (January 2009) when the answers are shown immediately ...
//        F_ShowMarksImmediately();

        break;

        case 25:
        C_ShowInstructions(0);
        break;

        case 50:
//        $username = $HTTP_POST_VARS['username'];
//        $password = $HTTP_POST_VARS['password'];
//        $db=new dbabstraction();
//        $db->connect() or die ($db->getError());
//        $condition = array('UserName'=>$username);
//        $table = $db->select("ScripModerator", $condition);
//        foreach ($table as $row){
//            $correctPassword = $row['Password'];
//        };
        // A correct password must have been found (i.e. it is not nul)
        // as well as the password entered being correct ...
//        if ($correctPassword && $password == $correctPassword) {
            $_SESSION['launcher'] = $username;
            D_Launch();
//        } else {
//            C_ShowInstructions(1); // ... with warning
//        };
        break;

        case 60:
        D_Launch();
        break;

        case 360:
        C_ShowInstructions(0);
        break;

        case 999:
        B_ShowQuestions($_SESSION['nExercises']);
        break;

    };
};

Q_html_end();

// FUNCTIONS BEGIN HERE ...

function A_html_begin($title, $tab) {
    // This is the beginining of the program ...

    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;
    print ("<html>\n");

    print ("<head>\n");
    if ($title) {
        print ("<title>$title</title>\n");
    };

    // Add the CSS link ...
    print('<link href="pharmacy_css.css" rel="stylesheet" type="text/css">');

    print("<script type='text/javascript'>");
    print("function autoSubmit() { alert('Help!') }");
    print("</script>");

    // Exemptions from running the stylesheet (e.g. so as to produce a white background
    // rather than the normal dark blue):
    // 20 - Marksheet
    // [Blank] - Attendance sheet
    if ($tab != 20 && $tab != "") { // Will print marksheet
        print('<link href="pharmacy_css.css" rel="stylesheet"');
        print(' type="text/css">');
    };

    print('<meta http-equiv="Content-Type"');
    print(' content="text/html; charset=iso-8859-1">');

    print("<script type='text/javascript'>");
    print("function timedMsg(to)");
    print("{");
    print("var t = setTimeout('document.forms[0].submit()', to);");
    print("}");
    print("</script>");

//        print("onLoad=javascript:setTimeout('document.forms[0].submit()', $delay)>\n");

    print("</head>\n");

    // Assign the variables in HTTP_GET_VARS from logon to $checking ...
    if (!isset($tab)) {

        $checking=new checkingclass();

        $_SESSION['table']=$_GET['t'];
        $_SESSION['ID']=$_GET['i'];
        $_SESSION['authority']=$_GET['a'];
        $_SESSION['inTheDispensary']=$_GET['d'];
        $_SESSION['dispensaryMode']=$_GET['m'];
        $_SESSION['checkingMode']=$_GET['c'];
        $_SESSION['checkingMachine']=$_GET['p'];

// JCH over-ride ...
$_SESSION['inTheDispensary']=1;
$_SESSION['checkingMode']=1;
$_SESSION['checkingMachine']=1;
$_SESSION['dispensaryMode']="PC";

    };

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Read the starting date ...
    $table = $db->select("ScripCheckingConfiguration");
    foreach ($table as $row){
        $theCheckingStartDate = $row['CheckingStartDate'];
    };

    // Read the student's checking practice code ...
    $condition = array('StudentID'=>$_SESSION['ID']);
    $table = $db->select("ScripStudent", $condition);
    foreach ($table as $row){
        $_SESSION['theCheckingPracticeCode'] = $row['CheckingPracticeCode'];
    };

    // Now we need to need to know which wek we are in (then we can consult the student's
    // pattern to see what exercise the student should be doiung) ...


/*
    $theCheckingStartDateYear=strtok($theCheckingStartDate,"-");
    $theCheckingStartDateMonth=strtok("-");
    $theCheckingStartDateDay=strtok("-");
    $theCheckingStart=mktime(0,0,0,$theCheckingStartDateMonth,$theCheckingStartDateDay,$theCheckingStartDateYear);

    $today=time();
    // Take difference, divide by number of seconds in a week, take
    // integer part, add one (since first week in table is called "1"
    // and not zero) ...
    $_SESSION['weekNumber'] = (int)(($today - $theCheckingStart) / (60 * 60 * 24 *7)) + 1; 

    // We read the week number from ScripDispensaryMode ...
    // Read the starting date ...
    $table = $db->select("ScripDispensaryMode");
    foreach ($table as $row){
        $_SESSION['weekNumber'] = $row['ThisWeek'];
    };
*/

    // This is the new technique for finding week number (now block number) and pattern ...

    // Put the C (i.e. C for Checking) contents of the table into two arrays ...
    $condition = array('Type'=>"C");
    $table=$db->select("ScripDates", $condition);
    foreach ($table as $row) {
        $theNumber[] = $row['Number'];
        $theDate[] = $row['Date'];
    };

    // Algorithm: We look for the date that is before to-day's date and closest to it ...
    $today=time();
    $todaysType = "";
    $todaysNumber = 0;

    // Loop over all the dates in the array ...
    for ($i = 0; $i < count($theDate); $i++){
        $thisyear = strtok($theDate[$i], "-");
        $thismonth = strtok("-");
        $thisday = strtok("-");
        $theStart = mktime(0, 0, 0, $thismonth, $thisday, $thisyear);

        // Time since the date we're looking at ...
        $timeSince = $today - $theStart;

//        unset($bestI);
        // If after the date then that date is at least a potential candidate ...
        if ($timeSince >= 0){
            // Is it the best candidate (either because there has been no previous candidate or because its $timeSince is smaller)?
            if (!isset($bestI) || $timeSince < $smallestTimeSince){
                $bestI = $i;
                $smallestTimeSince = $timeSince;
            };
         };

    };

    // Store the value for the best value of i ...
    $_SESSION['weekNumber'] = $theNumber[$bestI];

    // Now carry on as normal ...

    // To read the letters we need the practice code and the week number ...
    $condition = array("PracticeCode"=>$_SESSION['theCheckingPracticeCode'], "Week"=>$_SESSION['weekNumber']);

    // Search ScripCheckingAllPattern:
    $table = $db->select("ScripCheckingAllPattern", $condition);

    // Check if any records found; if so, get them and sort them; if not, show an error message ...
    if (count($table) == 0){
        $_SESSION['anyRecordsFound'] = 0;
        print("There are no records for this student (who has practice code ". $_SESSION['theCheckingPracticeCode'] . ") in this week (" . $_SESSION['weekNumber'] . ").");
    } else {

        $_SESSION['anyRecordsFound'] = 1;
        unset($_SESSION['setName']); // ... a permanent array: ensure it is empty before filling it ...
        foreach ($table as $row){
            $_SESSION['setName'][] = $row['SetName']; 
            $order[] = $row['Ordering']; // ... note $order but 'Ordering'
        };
    };


        // We now need to put the cases in $_SESSION['setName'] into order according to their values in $order ...
        if (count($order) == count($_SESSION['setName'])){
            array_multisort($order, SORT_ASC, SORT_NUMERIC, $_SESSION['setName']);
        };
        // ... the cases are now in numerical order

//    };

    // We now need to decide which of these cases we show to the user. Loop over them (in 
    // order) and see if it has been finished (either by the user or by the timer): if so
    // look at the next; if not, that's the one we want ...
    unset($_SESSION['letterThisWeek']);
    for ($i = 0; $i < count($_SESSION['setName']); $i++){
        // Look for relevant entry in ScripCheckingFinish ...
        $condition = array("PracticeCode"=>$_SESSION['theCheckingPracticeCode'], "Student"=>$_SESSION['ID'], "SetName"=>$_SESSION['setName'][$i]);
        $table = $db->select("ScripCheckingFinish", $condition);
        if ($table){
            // There IS an entry in ScripCheckingFinish ...
            foreach ($table as $row){
                if ($row['FinishMode'] != 1){
                    $_SESSION['letterThisWeek'] = $_SESSION['setName'][$i];
                    break 2; // ... break from both FOREACH and from FOR
                }; 
            };
        } else {
            // There is NOT an entry in ScripCheckingFinish ...
            $_SESSION['letterThisWeek'] = $_SESSION['setName'][$i];
            break; // ... break from for
        };
    };
    // ... if $_SESSION['letterThisWeek'] is still unset then all letters must have been finished
    // We now know which letter we should be doing (viz $_SESSION['letterThisWeek']). The rest of the code should now work ... 


    // Read the type - accuracy (A) or clinical (C) ...
    $condition = array("Letter"=>$_SESSION['letterThisWeek']);
    $table = $db->select("ScripCheckingCategory", $condition);
    foreach ($table as $row){
        $_SESSION['category'] = $row['Category'];
    };

    print ("<body ");
    if ($tab == 999) {
//        $db=new dbabstraction();
//        $db->connect() or die ($db->getError());
        $table = $db->select("ScripCheckingConfiguration");
        foreach ($table as $row){

            if ($_SESSION['category'] == "A") { $delay = 1000 * (60 * $row['DelayMinutesAccuracy'] + $row['DelaySecondsAccuracy']); };
            if ($_SESSION['category'] == "C") { $delay = 1000 * (60 * $row['DelayMinutesClinical'] + $row['DelaySecondsClinical']); };
            if ($_SESSION['category'] == "T") { $delay = 1000 * (60 * 90); };

        };

        // Run out of time (worked in IE6 - seems faulty in IE7 cf met refresh) ...
//        print("onLoad=javascript:setTimeout('document.forms[0].submit()', $delay)>\n");

        print("onLoad='timedMsg($delay)'");

//      Timeout not working ... this was the problem and solution in automated printing ...
//      print("<meta http-equiv=\"refresh\" content='0;URL=javascript:window.print()'>");
//	 print("<script type=\"text/javascript\" language=\"javascript\">window.print();</script>");


    } else {
        print(">");
    };
    print("\n");



//print("<br>tab is $tab");
//print("<br>_SESSION['category'] is $_SESSION['category']");
//print("<br>Delay is $delay");
//print("<br>DMA is $DMA");
//print_r($row);




};

function B_ShowQuestions($nExercises){
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

// REMOVE LATER 

// TEMPORARY OVER-RIDE
//$_SESSION['letterThisWeek'] = "C";


/*

    // This week is $_SESSION['letterThisWeek'] (e.g. C). What exercises
    // are there for this week?
    // Initally, we shall count them.
    $condition = array ('Letter'=>$_SESSION['letterThisWeek']);
    $table=$db->select("ScripCheckingMA", $condition);
    foreach ($table as $row){
        // If the exercise is available, add its number to the $exerciseAvailable array
        if ($row['Available']) { $exerciseAvailable[$row['NumberOfScript']] = 1; };
    };

print_r($exerciseAvailable); 

    
    $_SESSION['nExercises'] = count($exerciseAvailable);

*/

    // We know which letter we are using, we need to find what sort of exercise it
    // is: A (accuracy) or C (clinical)
    $condition = array ('Letter'=>$_SESSION['letterThisWeek']);
    $table=$db->select("ScripCheckingCategory", $condition);
    foreach($table as $row){
        $category = $row['Category'];
    };

    // We know which letter we are using, we need to see which questions from that
    // letter that are to be used: first we count them ...
    $condition = array ('Letter'=>$_SESSION['letterThisWeek'], 'Available'=>'1');
    $table=$db->select("ScripCheckingAvailability", $condition);

    // Clear the questionInUse array ...
    $_SESSION['questionInUse'] = array();
    foreach ($table as $row){
        $_SESSION['questionInUse'][] = $row['NumberOfScript'];
    };

    // Need to sort this array ...
    sort($_SESSION['questionInUse']); 

    // Have recorded the questions in use; how many are there?
    $_SESSION['nExercises'] = count($_SESSION['questionInUse']); 



    $condition = array ('StudentID'=>$_SESSION['ID'], 'SetLetter'=>$_SESSION['letterThisWeek']);
    $table=$db->select("ScripCheckingTimes", $condition);
    if (!count($table)) {
        $start = time();
        $data = array ('StartTime'=>$start,
                       'StudentID'=>$_SESSION['ID'],
                       'SetLetter'=>$_SESSION['letterThisWeek']);
        $db->insert("ScripCheckingTimes", $data);
    };

    $condition = array();
    $table = $db->select("ScripCheckingQuestions");
    foreach ($table as $row){
        $question[] = $row['Question'];
        $order[] = $row['QuestionNumber'];
        $categoryOfQuestion[] = $row['Category'];
    };
    $_SESSION['nQuestions'] = count($question);

    // Now use multisort, sort $question in the same way as $order:
    array_multisort($order, SORT_ASC, SORT_NUMERIC, $question);
    // Now use multisort, sort $categoryOfQuestion in the same way as $order:
    array_multisort($order, SORT_ASC, SORT_NUMERIC, $categoryOfQuestion);

    // We know which letter we are using, we need to find what sort of exercise it
    // is: A (accuracy) or C (clinical)
    $condition = array ('Letter'=>$_SESSION['letterThisWeek']);
    $table=$db->select("ScripCheckingCategory", $condition);
    foreach($table as $row){
        $category = $row['Category'];
    };

    // The rules are that if this is an A week then only A questions are
    // used; C questions are not. 

//print("Letter this week $_SESSION['letterThisWeek']<br><br>");

//print("Category $category");

    if ($category == "A") {
        for ($i = count($question) - 1; $i > -1; $i--){
            if ($categoryOfQuestion[$i] == "C") {
                // Don't ask this (one) question ($i), therefore remove it ... 
                array_splice($question, $i, 1);
                array_splice($categoryOfQuestion, $i, 1);
            };
        };
    };

    print("<h2 align='center'>");
    print("Press the [All Prescriptions Checked] button when you have finished.");
    print("</h2>");

    print("<table align='center' width='800' ");
    print("border='1' bgcolor='#003366'>\n");
    print("<tr>");
    print("<td>&nbsp;</td>");

    // Display the headings i.e. cells with the numbers of each exercise ...
    for ($i = 0; $i < $_SESSION['nExercises']; $i++) {
        $exerciseNo = $_SESSION['questionInUse'][$i];
        print("<td align='center'><b>$exerciseNo</b></td>");
    };
    print("</tr>");
    
    // We want different colours for the background of alternate rows ...
    $rowColour[0] = '#0058B0'; $rowColour[1] = '#003366';
    $kk = 1;
    print("<form method='post' name='Answers'>");
    print("<input type='hidden' name='tab' value='20'>");

    // Create rows ...
    // First note that the number of questions may have been reduced - re-set it ...
    $_SESSION['nQuestions'] = count($question);
    for ($j = 0; $j < $_SESSION['nQuestions']; $j++) {
        $jp1 = $j + 1;
        $kk = 1 - $kk; // To alternate between row colours 
        print("<tr bgcolor=$rowColour[$kk]>");

        // Create cells within the current row ...
        for ($i = 0; $i < $_SESSION['nExercises'] + 1; $i++) {
            if ($i == 0) { // The row's question cell ...
                print("<td><font size='+1'>".$question[$j]."</font></td>");
            } else { // ... and all the other cells in the row are answer cells ...
                print("<td align='center'>");
                print("<input type='checkbox' name='$jp1,$i'>");
                print("</td>");
            };
        };

        print("</tr>");
    };

    print("</table>");

    print("<p align='center'>");
    print("<input type='hidden' name='finishmode' value='1'>");
    print("<input type='submit' name='Submit' value='All Prescriptions Checked'>");
    print("</p>");
    print("</form>");
};

function C_ShowInstructions($warning){
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;

//    Recall: 
//    $_SESSION['table']; $_SESSION['ID']; $_SESSION['authority']

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Read the starting date ...
//    $table = $db->select("ScripCheckingConfiguration");
//    foreach ($table as $row){
//        $theCheckingStartDate = $row['CheckingStartDate'];
//    };

    // Read the student's checking practice code ...
//    $condition = array('StudentID'=>$_SESSION['ID']); 
//    $table = $db->select("ScripStudent", $condition);
//    foreach ($table as $row){
//        $_SESSION['theCheckingPracticeCode'] = $row['CheckingPracticeCode'];
//    };

//    $theCheckingStartDateYear=strtok($theCheckingStartDate,"-");
//    $theCheckingStartDateMonth=strtok("-");
//    $theCheckingStartDateDay=strtok("-");
//    $theCheckingStart=mktime(0,0,0,$theCheckingStartDateMonth,$theCheckingStartDateDay,$theCheckingStartDateYear);

//    $today=time();
    // Take difference, divide by number of seconds in a week, take
    // integer part, add one (since first week in table is called "1"
    // and not zero) ...
//    $_SESSION['weekNumber'] = (int)(($today - $theCheckingStart) / (60 * 60 * 24 *7)) + 1; 


    // Read the letter ...
//    $condition = array("PracticeCode"=>$_SESSION['theCheckingPracticeCode']);
//    $table = $db->select("ScripCheckingTermPattern", $condition);
//    foreach ($table as $row){
//        $_SESSION['letterThisWeek'] = $row['wk'.$_SESSION['weekNumber']];
//    };
//$_SESSION['letterThisWeek'] = "C";

    // Read the type - accuracy (A) or clinical (C) ...
    $condition = array("Letter"=>$_SESSION['letterThisWeek']);
    $table = $db->select("ScripCheckingCategory", $condition);
    foreach ($table as $row){
        $_SESSION['category'] = $row['Category'];
    };

    // Read the instructions depending on $_SESSION['category'] ...
    $condition = array('Category'=>$_SESSION['category']);
    $table = $db->select("ScripCheckingInstructions", $condition);
    foreach ($table as $row){
        $comment1 = $row['Comment1'];
        $comment2 = $row['Comment2'];

    };

    print("<table align='center' width='70%'><tr>");
    print("<td>");
//    print("<center>Checking Week Number: $_SESSION['weekNumber']</center>");
    if ($_SESSION['letterThisWeek'] != ""){
        print("$comment1"); // ... typically the initial instructions and including the title
    };

    // Rem out the letter ...
//    print("<h1 align='center'>$_SESSION['letterThisWeek']</h1>");

    if ($_SESSION['letterThisWeek'] != ""){
//        print("$comment2");
    };

    print("<font size=+2><center>" . $_SESSION['letterThisWeek'] . "</center></font>"); 

    if ($warning) { 
        print("<font color='#ff0000'>");
        print("Try again!");
        print("</font><br>");
    } else {
        print("&nbsp;<br>");
    };

    print("<form action='ScripChecking.php' method='post'>");
    print("<input type='hidden' name='tab' value='50'>");

//    print("<p align=center>");
//    print("Demonstrator's username: <input type='text' name='username'>");
//    print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
//    print("Demonstrator's password: <input type='password' name='password'>");
//    print("</p>");


    print("<p align='center'>");
    // Don't show the button if no letters left ...
    if ($_SESSION['letterThisWeek'] != "") {
        print("<input type='submit' name='Proceed' value='Proceed'>");
    } else {
        print("No exercises left!");
    };
    print("</p>");


    print("</tr></table>");

    print("</form>");
};

function D_Launch(){
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;
    
    print("<form action='ScripChecking.php' method='post'>");
    print("<input type='hidden' name='tab' value='999'>");
    print("<p align='center'><br><br><br>");
    print("<input type='submit' size='100' name='Launch' value='Launch'>");
    print("</p>");
    print("</form>");
};

function E_AfterSubmitting(){
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;

    // The letter is: $_SESSION['letterThisWeek']

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Record the stop time ...
    $stop = time();
    $condition = array ('StudentID'=>$_SESSION['ID'],
              'SetLetter'=>$_SESSION['letterThisWeek']);

    // ... and write it away ...
    $text = array ('StopTime'=>$stop);
    $db->update("ScripCheckingTimes", $text, $condition);
    $table = $db->select("ScripCheckingTimes", $condition);
    foreach ($table as $row) {
        $start = $row['StartTime'];
        $stop = $row['StopTime'];
    };

    // Non-zero stop time ...
    if ($stop) {
        $timetaken = $stop - $start;
        $timetakenminutes = floor($timetaken/60);
        $timetakenseconds = $timetaken - 60 * $timetakenminutes;

        $data = array ('StopTime'=>$stop,
                       'TimeTakenMinutes'=>$timetakenminutes,
                       'TimeTakenSeconds'=>$timetakenseconds);
        $db->update("ScripCheckingTimes", $data, $condition);
    };

    // Instruction ...
    $condition = array('Category'=>$_SESSION['category']);
    $table = $db->select("ScripCheckingInstructions", $condition);
    foreach ($table as $row){
        $comment3 = $row['Comment3'];
    };
    // This is comment 3 e.g.
    // "Your answers have been submitted for marking."
    // "Please replace the prescriptions back into the correct bags along with the correct products."
    // "Please return the prescriptions to one of the demonstrators along with your mark sheet. You may then continue with your normal ScripWare exercises."

//    print("$comment3");

    // Find the name of the student ...
    $condition = array ('StudentID'=>$_SESSION['ID']);
    $table = $db->select("ScripStudent", $condition);
    foreach ($table as $row) {
        $fname = $row['FirstName'];
        $lname = $row['LastName'];
        $username = $row['UserName'];
    };

    // Find the name of the moderator ...
    $condition = array ('UserName'=>$_SESSION['launcher']);
    $table = $db->select("ScripModerator", $condition);
    foreach ($table as $row) {
        $fnameM = $row['FirstName'];
        $lnameM = $row['LastName'];
    };

    $table = $db->select("ScripCheckingQuestions");
    foreach ($table as $row) {
        $qText[$row['QuestionNumber']] = $row['Question'];
    };

    if ($username[0] == "!") { $superuser = 1; } else { $superuser = 0; };

    if ($superuser) {
//        print("Debugging data:<br>");
//        print_r($HTTP_POST_VARS);
//        print("<br>");
//        print("Questions: $_SESSION['nQuestions']<br>");
//        print("Exercises: $_SESSION['nExercises']<br>");
    };

    // Loop over the exercises (e.g. B1, B3, B4) that ARE IN USE. The
    // array $questionInUse contains the numbers of the current week
    // that are in use.
    for ($i = 0; $i < count($_SESSION['questionInUse']); $i++) {
        $ip1 = $i + 1;

        // Loop over the questions in a given exercise ...
        $condition = array('Letter'=>$_SESSION['letterThisWeek'], 'NumberOfScript'=>$_SESSION['questionInUse'][$i]);
        $table = $db->select("ScripCheckingMA", $condition);

        // We now have all the information about the one checking question that 
        // we are marking. For simplicity, we're going to put the
        // information into three simple arrays ...
        foreach ($table as $row) {
            // Put the penalties into two arrays ...
            $qN = $row['QuestionNumber'];
            $penaltyTrue[$qN] = $row['PenaltyTrue'];
            $penaltyFalse[$qN] = $row['PenaltyFalse'];
        };

        $comment = "";
        $totalPenalty = 0; // ... initialise 
        $serious = 0; // ... initialise
        // Loop over the questions ...


        for ($j = 1; $j < $_SESSION['nQuestions']+1; $j++) {
            $thisPenalty = 0;

            // For questions for which the user has chosen true, add the true penalty (will be zero if true is the correct answer) ...

            if ($HTTP_POST_VARS["$j,$ip1"] == "on") {

                $thisPenalty = $penaltyTrue[$j];

                $totalPenalty += $thisPenalty;
                // Non-zero penalty?
                if ($thisPenalty) {
                    $comment.= "You should not have selected ";
                    $comment.=$qText[$j];
                    $comment.=". ";
                } else {
                    $comment.= "You selected ";
                    $comment.=$qText[$j];
                    $comment.=" - this was correct. ";
                }; 
                // Is this a serious error?
                if ($penaltyTrue[$j] == 25) { $serious = 1; };

            // For questions for which the user has chosen false, add the false penalty (will be zero if false is the correct answer) ...
            } else {
                $thisPenalty = $penaltyFalse[$j];

                // Non-zero penalty?
                $totalPenalty += $thisPenalty;
                if ($thisPenalty) {
                    $comment.= "You should have selected ";
                    $comment.=$qText[$j];
                    $comment.=". ";
                };
                // Is this a serious error?
                if ($penaltyFalse[$j] == 25) { $serious = 1; };
            };

            // Finished marking this question ...

            // $thisPenalty now tells us what penalty the student has incurred
            // on this question. Now is the time to write it away  if we want
            // to keep this information ...
            $condition = array ('StudentID'=>$_SESSION['ID'],
                            'Letter'=>$_SESSION['letterThisWeek'],
                            'NumberOfScript'=>$_SESSION['questionInUse'][$i],
                            'QuestionNumber'=>$j);
            // QuestionNumber is a column for storing the answers
            // from individual questions (e.g. 1, 2, ...).
            $text = array ('Answer' => $thisPenalty);
            $text = array_merge($text, $condition);
            if ($superuser) {
//                print_r($text);print("<br><br>");
            };
            $db->insert("ScripCheckingAnswers", $text);
        };



        // Finished marking this exercise (e.g. B3)
        
        // Write the total away ...
        if ($superuser) {
//            print("Total for Exercise No. $i (i.e. ".$questionInUse[$i].") is $totalPenalty<br>");
        };

        // If necessary, amend the penalty: maximum penalty is 10 ...
        if ($totalPenalty > 10) { $totalPenalty = 10; };
        // ... unless a serious error took place in which case it is 25 ...
        if ($serious) { $totalPenalty = 25; };

        $answer = 10 - $totalPenalty;

        $condition = array ('StudentID'=>$_SESSION['ID'],
                            'Letter'=>$_SESSION['letterThisWeek'],
                            'NumberOfScript'=>$_SESSION['questionInUse'][$i],
                            'QuestionNumber'=>0);
        // QuestionNumber is a column for storing the answers
        // from individual questions (e.g. 1, 2, ...).
        // If the value is set to zero, this indicates that the 
        // number in the field Answer is the TOTAL SCORE.
        $text = array ('Answer' => $answer);
        $text = array_merge($text, $condition);
        if ($superuser) {
//            print_r($text);print("<br><br>");
        };


        $db->insert("ScripCheckingAnswers", $text);

        $text = array ("StudentID"=>$_SESSION['ID'],
                       "CaseID"=>"",
                       "Penalty"=>$totalPenalty,
                       "Tab"=>$_SESSION['letterThisWeek'].$_SESSION['questionInUse'][$i],
                       "Comment"=>$comment);
        $db->insert("ScripCheckingResults", $text);

    }; // ... have now finished looping over all the questions that the student was offered



/*

    // This is the mark sheet when asnwers are released later ...
    // [Not required at present]
    $today=date("d-m-Y H:i",time());

    print("<br>");

    print("<table width='100%' border='1'>");
    print("<tr><td>");
    print("<br>Name of Student:<br>");
    print("<b><font size='+1'>$fname $lname</font></b>");
    print(" <font size='-1'>($username - $_SESSION['ID'])</font>");
    print("<br><br>");
    print("</td><td>");
    print("<br>Demonstrator (logging-on):<br>");
    print("<b>$fnameM $lnameM</b><br><br>");
    print("</td></tr>");

    print("<tr><td>");
    print("<br>Checking Set: <b>$_SESSION['letterThisWeek']</b><br><br>");
    print("</td><td>");
    print("<br>Demonstrator:<br>");
    print("(Receiving goods back)<br><br><br><br>");
    print("</td></tr>");

    print("<tr><td colspan='2'>");
    print("<br>Time and date: $today<br><br>");
    print("</td></tr>");

    print("<tr><td colspan='2' align='center'>");
    print("<h1>$_SESSION['letterThisWeek']: ______</h1>");
    print("</td></tr>");
    print("</table>");

    print("<br><br>");

    // The grid of user's answers ...
    print("<table border='1'>");
    for ($j = 1; $j < $_SESSION['nQuestions']+1; $j++) {
        $ip1 = $i + 1;
        print("<tr>");
        for ($i = 0; $i < count($_SESSION['questionInUse']); $i++) {
            $ip1 = $i + 1;
            if ($HTTP_POST_VARS["$j,$ip1"] == "on") { $symbol = "X"; } else { $symbol = "&nbsp; "; };
            print("<td width='35'><center>$symbol</center></td>");
        };
        print("</tr>");
    };
    print("</table>");

    // Printing ...
    print("<meta http-equiv=refresh ");
    print("content='0;URL=javascript:window.print()'>");
*/

    F_ShowMarksImmediately();

};

function EE_AfterSubmitting(){

    // This is a copy of the original E_AfterSubmitting() that has been amended for the January 2009 verion
    // version of this program i.e. the one that shows marks immediately and not waits until the marks have been 
    // released: 

    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;

    // The letter is: $_SESSION['letterThisWeek']

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Record the stop time ...
    $stop = time();
    $condition = array ('StudentID'=>$_SESSION['ID'],
              'SetLetter'=>$_SESSION['letterThisWeek']);

    // ... and write it away ...
    $text = array ('StopTime'=>$stop);
    $db->update("ScripCheckingTimes", $text, $condition);
    $table = $db->select("ScripCheckingTimes", $condition);
    foreach ($table as $row) {
        $start = $row['StartTime'];
        $stop = $row['StopTime'];
    };

    // Non-zero stop time ...
    if ($stop) {
        $timetaken = $stop - $start;
        $timetakenminutes = floor($timetaken/60);
        $timetakenseconds = $timetaken - 60 * $timetakenminutes;

        $data = array ('StopTime'=>$stop,
                       'TimeTakenMinutes'=>$timetakenminutes,
                       'TimeTakenSeconds'=>$timetakenseconds);
        $db->update("ScripCheckingTimes", $data, $condition);
    };

    // Instruction ...
    $condition = array('Category'=>$_SESSION['category']);
    $table = $db->select("ScripCheckingInstructions", $condition);
    foreach ($table as $row){
        $comment3 = $row['Comment3'];
    };
    // This is comment 3 e.g.
    // "Your answers have been submitted for marking."
    // "Please replace the prescriptions back into the correct bags along with the correct products."
    // "Please return the prescriptions to one of the demonstrators along with your mark sheet. You may then continue with your normal ScripWare exercises."

//    print("$comment3");

    // Find the name of the student ...
    $condition = array ('StudentID'=>$_SESSION['ID']);
    $table = $db->select("ScripStudent", $condition);
    foreach ($table as $row) {
        $fname = $row['FirstName'];
        $lname = $row['LastName'];
        $username = $row['UserName'];
    };

    // Find the name of the moderator ...
    $condition = array ('UserName'=>$_SESSION['launcher']);
    $table = $db->select("ScripModerator", $condition);
    foreach ($table as $row) {
        $fnameM = $row['FirstName'];
        $lnameM = $row['LastName'];
    };

    $table = $db->select("ScripCheckingQuestions");
    foreach ($table as $row) {
        $qText[$row['QuestionNumber']] = $row['Question'];
    };

    if ($username[0] == "!") { $superuser = 1; } else { $superuser = 0; };

    if ($superuser) {
//        print("Debugging data:<br>");
//        print_r($HTTP_POST_VARS);
//        print("<br>");
//        print("Questions: $_SESSION['nQuestions']<br>");
//        print("Exercises: $_SESSION['nExercises']<br>");
    };

    // Loop over the exercises (e.g. B1, B3, B4) that ARE IN USE. The
    // array $questionInUse contains the numbers of the current week
    // that are in use.
    for ($i = 0; $i < count($_SESSION['questionInUse']); $i++) {
        $ip1 = $i + 1;

        // Loop over the questions in a given exercise ...
        $condition = array('Letter'=>$_SESSION['letterThisWeek'], 'NumberOfScript'=>$_SESSION['questionInUse'][$i]);
        $table = $db->select("ScripCheckingMA", $condition);
        // ... model answers for the exercise we've been doing

        // We now have all the information about the one checking question that 
        // we are marking. For simplicity, we're going to put the
        // information into three simple arrays ...
        foreach ($table as $row) {
            // Put the penalties into two arrays ...
            $qN = $row['QuestionNumber'];
            $penaltyTrue[$qN] = $row['PenaltyTrue'];
            $penaltyFalse[$qN] = $row['PenaltyFalse'];
        };

        $comment = "";
        $totalPenalty = 0; // ... initialise 
        $serious = 0; // ... initialise
        // Loop over the questions ...


        for ($j = 1; $j < $_SESSION['nQuestions']+1; $j++) {
            $thisPenalty = 0;

            // For questions for which the user has chosen true, add the
            // true penalty (will be zero if true is the correct answer) ...

            if ($HTTP_POST_VARS["$j,$ip1"] == "on") {

                $thisPenalty = $penaltyTrue[$j];

                $totalPenalty += $thisPenalty;
                // Non-zero penalty?
                if ($thisPenalty) {
                    $comment.= "You should not have selected ";
                    $comment.=$qText[$j];
                    $comment.=". ";
                } else {
                    $comment.= "You selected ";
                    $comment.=$qText[$j];
                    $comment.=" - this was correct. ";
                }; 
                // Is this a serious error?
                if ($penaltyTrue[$j] == $_SESSION['seriousError']) { $serious = 1; };

            // For questions for which the user has chosen false, add the
            // false penalty (will be zero if false is the correct answer) ...
            } else {
                $thisPenalty = $penaltyFalse[$j];


                // Non-zero penalty?
                $totalPenalty += $thisPenalty;
                if ($thisPenalty) {
                    $comment.= "You should have selected ";
                    $comment.=$qText[$j];
                    $comment.=". ";
                };
                // Is this a serious error?
                if ($penaltyFalse[$j] == $_SESSION['seriousError']) { $serious = 1; };
            };

            // Finished marking this question ...

            // $thisPenalty now tells us what penalty the student has incurred
            // on this question. Now is the time to write it away  if we want
            // to keep this information ...
            $condition = array ('StudentID'=>$_SESSION['ID'],
                            'Letter'=>$_SESSION['letterThisWeek'],
                            'NumberOfScript'=>$_SESSION['questionInUse'][$i],
                            'QuestionNumber'=>$j);
            // QuestionNumber is a column for storing the answers
            // from individual questions (e.g. 1, 2, ...).
            $text = array ('Answer' => $thisPenalty);
            $text = array_merge($text, $condition);
            if ($superuser) {
//                print_r($text);print("<br><br>");
            };
            $db->insert("ScripCheckingAnswers", $text);
        };
        // ... finished marking this exercise (e.g. B3)
        
        // Write the total away ...
        if ($superuser) {
//            print("Total for Exercise No. $i (i.e. ".$questionInUse[$i].") is $totalPenalty<br>");
        };

        // If necessary, amend the penalty: maximum penalty is 10 ...
        if ($totalPenalty > 10) { $totalPenalty = 10; };
        // ... unless a serious error took place in which case it is $_SESSION['seriousError'] (e.g. 35) ...
        if ($serious) { $totalPenalty = $_SESSION['seriousError']; };

        $answer = 10 - $totalPenalty;

        $condition = array ('StudentID'=>$_SESSION['ID'],
                            'Letter'=>$_SESSION['letterThisWeek'],
                            'NumberOfScript'=>$_SESSION['questionInUse'][$i],
                            'QuestionNumber'=>0);
        // QuestionNumber is a column for storing the answers
        // from individual questions (e.g. 1, 2, ...).
        // If the value is set to zero, this indicates that the 
        // number in the field Answer is the TOTAL SCORE.
        $text = array ('Answer' => $answer);
        $text = array_merge($text, $condition);
        if ($superuser) {
//            print_r($text);print("<br><br>");
        };


        $db->insert("ScripCheckingAnswers", $text);

        $text = array ("StudentID"=>$_SESSION['ID'],
                       "CaseID"=>"",
                       "Penalty"=>$totalPenalty,
                       "Tab"=>$_SESSION['letterThisWeek'].$_SESSION['questionInUse'][$i],
                       "Comment"=>$comment);
        $db->insert("ScripCheckingResults", $text);

    }; // ... have now finished looping over all the questions that the student was offered

    // This is where the mark sheet occurs in E_AfterSubmitting - it has been removed from this function (EE_)

    // Now show the marks ...
    F_ShowMarksImmediately();

};


function F_ShowMarksImmediately(){
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    CH_ShowChecking();

    print("<form method='post' name='Marks'>");
    print("<input type='hidden' name='tab' value='25'>");

    print("<p align='center'><input type='submit' name='Submit' value='Next Checking Exercise'></p>");
    print("</form>");
};


function CH_ShowChecking(){
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;
  
    // Read ScripCheckingResults ...

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $checkingText = "";
    $cumulative = 0;
    $cumulativeMaximum = 0;

    // This code comes from the original function in ScripModerate: Adminisistraors and Moderators
    // can see marks whether released or not; all others only released marks ...
    if ($_SESSION['MorS'] == "A" || $_SESSION['MorS'] == "M") {
        $condition = array ('StudentID'=>$_SESSION['studentID']);
    } else {
        $condition = array ('StudentID'=>$_SESSION['studentID'], 'Release'=>1);
    };




    $condition = array ('StudentID'=>$_SESSION['ID']);
    $table = $db->select("ScripCheckingResults", $condition);
    foreach ($table as $row) {
        // We have selected entries real;ting to this student only; from thse select only those
        // rfelating to this week only - look for values of tab that begin with letterThisWeek
        if (substr($row['Tab'], 0, strlen($_SESSION['letterThisWeek'])) == $_SESSION['letterThisWeek']){
            $penalty = $row['Penalty'];
            $cumulative += (10 - $penalty);
            $cumulativeMaximum += 10;
            $checkingText.= $penalty." - ";
            $checkingText.= $row['Tab'].": ";
            $checkingText.= $row['Comment']."\n";

        };
    };    

    print("<div class=tablecream>");
    print("<table bgcolor='#0000EE' border=1 width=100% align='center'>");

    print("<tr><td colspan='4'>");
    print("NOT PART OF THE DISPENSING MARK - The numbers in the box "); 
    print("are the penalties incurred in each of the exercises. "); 
    print("The number to the side of the box is the total.<br>"); 
    print("</td></tr>");

    print("<tr><td align='center'>Total Marks<br>");
    print("(out of $cumulativeMaximum):<br>");
    print("<br><b>$cumulative</b>");
    print("</td>");
    print("<td align='center' colspan='3'>");
    print("<textarea name=marking rows=10
                  COLS=80");
    if (!$_SESSION['fullAuthority']) { print(" readonly"); };
    print(">");
    print("$checkingText");
    print("</textarea></td>");
    print("</tr>");

    print("</table></div>\n");
  
};

function ZAS_AttendanceSheet($occasion) {
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $table = $db->select("ScripDispensaryMode");
    foreach ($table as $row){
        $whichAttendanceMessage = $row['WhichAttendanceMessage'];
    };

    $table = $db->select("ScripAttendanceMessage");
    foreach ($table as $row){
        $message = $row['Message'];
    };

    print("<table width='100%' border='0' bgcolor='ffffff'>");

    print("<tr><td>");
    print("<font color=000000><h1>Attendance Sheet - $occasion</h1>");

    print("<p>Name: <b>" . $_SESSION['firstname'] . " " . $_SESSION['lastname']. "</b></p>");
    print("<p>Username: " . $_SESSION['username'] . "</p>");
    print("<p>Time and Date: ".date("d-m-Y H:i",time())."</p>");
//    print("<p>Set: <b>$_SESSION['thisWeeksLetter']</b></p>");
    print("<p>Number:</p>");
    print("<p>&nbsp;</p><p>Signature:</p></font>");
    print("</td></tr>");

    print("<tr><td>");
    print("<script type=\"text/javascript\" language=\"javascript\">window.print();</script>");
    print("</td></tr>");

    print("<tr><td><BR><BR><BR><BR><BR></tr></td>");

    print("<tr><td>$message</td></tr>");

    print("</table>");

    print("<table>");
    print("<tr><td><p align ='center'><font color=#ff0000>");
    print("<a href='ScripChecking.php?tab=360'>Proceed to exercise</a>");
    print("</font></p></td></tr>");

    print("</table>");

};

function Q_html_end() {
    global $HTTP_POST_VARS;
    global $_GET;
    global $checking;

    print("</body></html>\n");
};

?>

