<?php
class dbabstraction
{   var $link;
    var $errors = array();
    var $debug = false;
//    var $DBhost='mysql';
//    var $DBuser='cczwood_w3';
//    var $DBpass='Wp4MSQL!';
//    var $DBname='cczwood_calprograms';
    var $id; /* of item last inserted */


    var $DBhost='128.243.45.11:3309';
    var $DBuser='uni_of_nott';
    var $DBpass='n3w5T4r';
    var $DBname='scripware.production';



	/* constructor function */
    function dbabstraction()
    {
    }

    /* connect to database */
    function connect()
    {
    	$link=mysql_connect($this->DBhost, $this->DBuser, $this->DBpass);
		if (! $link)
		{
			$this->setError("Couldn't connect to database server");
	    	return false;
		} else {
			//            set key_buffer_size=536870912;
        }
		if (!mysql_select_db($this->DBname, $link))
		{
			$this->setError("Couldn't select database: $db");
			return false;
		}
		$this->link = $link;
		return true;
    }

    /* close connection */
    function close()
    {
    	mysql_close($this->link);
    }

    /* display error message */
    function getError()
    {
    	return $this->errors[count($this->errors)-1];
    }

	/* put error message into error array */
    function setError($str)
    {
    	array_push($this->errors, $str);
    }

    /* perform query on database */
    function _query($query)
    {
		if (!$this->link)
		{
			$this->setError("No active db connection");
			return false;
		}

		$result=mysql_query($query,$this->link);

		if (! $result)
		{
			$this->setError("error: ".mysql_error());
		}

	    $this->id=mysql_insert_id();
		return $result;
    }

    /* query key of item last inserted */
    function getID()
    {
    	return $this->id;
    }

    /* perform an action on the database */
    function setQuery($query)
    {
    	if (!$result=$this->_query($query))

    	{
    		return false;
		}
		return mysql_affected_rows($this->link);
    }

    /* get data from the database */
    function getQuery($query)
    {
		if (!$result=$this->_query($query))
		{
			return false;
		}

		$ret=array();

		while ($row=mysql_fetch_assoc($result))
		{
			$ret[]=$row;
		}
		return $ret;
    }

    /* build a simple select query and perform it */
    function select ($table, $condition="", $sort="")
    {
	   	$query="SELECT * FROM $table";
		$query.=$this->makeWhereList($condition);
		if ($sort !="")
		{
			$query .= " order by $sort";
		}
		$this->debug($query);
		return $this->getQuery($query, $error);
    }

    /* build a select query with a like option and perform it */
    function selectlike ($table, $condition="", $sort="")
    {
    	$query="SELECT * FROM $table";
		$query.=" WHERE ".$condition;
		if ($sort !="")
		{
			$query .= " order by $sort";
		}
		$this->debug($query);
		return $this->getQuery($query, $error);
    }

    /* build a count query and perform it */
    function getcount($table,$condition="")
    {
    	$query="SELECT * FROM $table";
		$query.=$this->makeWhereList($condition);
		$this->debug($query);
		$retval = mysql_query($query);
		return mysql_num_rows($retval);
    }

    /* build an insert query and perform it */
    function insert($table, $add_array)
    {
    	$add_array=$this->_quote_vals($add_array);
		$keys="(".implode(array_keys($add_array),", ").")";
		$values="values (".implode(array_values($add_array),", ").")";
		$query="INSERT INTO $table $keys $values";
		$this->debug($query);
		return $this->setQuery($query);
    }

    /* Build and perform a simple update query */
    function update($table,$update_array,$condition="")
    {
    	$update_pairs=array();
		foreach ($update_array as $field=>$val)
		{
			array_push($update_pairs, "$field=".$this->_quote_val($val));
		}
		$query="UPDATE $table set ";
		$query .= implode(", ",$update_pairs);
		$query .= $this->makeWhereList($condition);
		$this->debug($query);
		$nosrows= $this->setQuery($query);
		return $nosrows;
    }

    /* Build and perform a delete query */
    function delete($table, $condition="")
    {
    	$query="DELETE FROM $table";
		$query .= $this->makeWhereList($condition);
		$this->debug($query);
		return $this->setQuery($query);
    }

    /* debug function prints messages about steps performed */
    function debug($msg)
    {
    	if ($this->debug)
		{
			print "<P><FONT COLOR=BLUE>$msg<BR></FONT></P>";
		}
    }

    /* take condition and convert to a quoted list for WHERE clause */
    function makeWhereList($condition)
    {
    	if (empty($condition))
		{
			return "";
		}

		$retstr=" WHERE ";
		if (is_array($condition))
		{
			$cond_pairs=array();
			foreach ($condition as $field=>$val)
			{   array_push($cond_pairs, "$field=".$this->_quote_val($val));
			}
			$retstr .= implode(" and ", $cond_pairs);
		} elseif (is_string($condition))
		{
			$retstr .= $condition;
		}
		return $retstr;
    }

    /* put quote marks round a string */
    function _quote_val($val)
    {
    	if (is_numeric($val))
		{
			return $val;
		}
		return "'".addslashes($val)."'";
    }

	/* put quotes round all strings in an array */
    function _quote_vals($array)
    {
    	foreach ($array as $key=>$val)
		{
			$ret[$key]=$this->_quote_val($val);
		}
		return $ret;
    }
}
?>
