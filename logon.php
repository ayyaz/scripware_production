<?php
session_start();
require 'dbabstraction.php';

// session_register("case");

$case = null;

class scripclass {
    var $tab, $authority, $studentID, $username, $inTheDispensary, 
        $dispensaryMode, $checkingMode, $checkingMachine, $theComputerName, 
        $what, $extra, $spwchanged, $passwordLegitmate;
};
   
/*
The sole purpose of this program is to allow the user to log on and establish his authority. When the menu (next program) is set up,
it is set up according to the user's authority.
The variables carried across are:
    User table ("student"; "moderator" or "");
    User ID in that table;
    User authority ("S", "M" or "A").
*/

// In PHP5 must explicitly set $tab ...
if (isset($_GET['tab'])){ $tab = $_GET['tab']; };
if (isset($_POST['tab'])){ $tab = $_POST['tab']; };

A_html_begin("Scripware - Log-on");

if (!isset($_POST['tab']) && !isset($_GET['tab'])) {

    // Clear the session variable ...
    $_SESSION = array(); 

    // Force manual dispensary if necessary ...
    $_SESSION['wrongPassword'] = 1;

    // Now ask for the password ...
    X_AskForPassword();

} else {
    switch ($tab) {
        case 0: 
        $_SESSION['username']=$_POST['username'];
        $thePassword=$_POST['password'];
        Y_CheckPassword ($_SESSION['username'], $thePassword);
        break;

        case 10:
//print_r($_POST);
        Y_CheckPassword('0', '0');
        break;

        case 20: // Coming in from a sucessful change of password
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());

        // We know this is a successful log-in; don't ask the user
        // for password again - find it ourselves ...
        $condition = array ('Username'=>$_SESSION['username']);
        if ($_SESSION['authority'] == "S") {
            $table = $db->select("ScripStudent", $condition);
        };
        if ($_SESSION['authority'] == "M") {
            $table = $db->select("ScripModerator", $condition);
        };

//print("<pre>");print_r($table);

        foreach ($table as $row) {
            $password = $row['Password'];
//print($password);
        };
        Y_CheckPassword($_SESSION['username'], $password);
        break;


        case 40:
        // Have just asked student "Practising outside a formal class" or "In a supervised class" and student has pressed one button or other. If former
        // nothing further to do; if latter, collect passwords (and then process them) ... 

        if ($_POST['supervised']){
            V_ManualInDispensaryPasswords();
            // ... ask for passwords
        };

        if ($_POST['outside']){
            $_SESSION['wrongPassword'] = 0;
            Y_CheckPassword ($_SESSION['username'], $thePassword);
        };


//        V_ManualInDispensaryPasswords($shortTermPassword, $superUsername, $superPassword, $confirmPassword, $cancelPassword);
        break;


        case 41:
        if ($_POST['confirmpassword']){
            // Get passwords and analyse them (to see if any are correct) ...

            $_SESSION['shortTermPassword'] = (empty($_POST['shorttermpassword'])) ? null : $_POST['shorttermpassword'];
            $_SESSION['superUsername'] = (empty($_POST['superusername'])) ? null : $_POST['superusername'];
            $_SESSION['superPassword'] = (empty($_POST['superpassword'])) ? null : $_POST['superpassword'];
            $_SESSION['confirmPassword'] = (empty($_POST['confirmpassword'])) ? null : $_POST['confirmpassword'];
            $_SESSION['cancelPassword'] = (empty($_POST['cancelpassword'])) ? null : $_POST['cancelpassword'];

            V_ManualInDispensaryAnalysis($_SESSION['shortTermPassword'], $_SESSION['superUsername'], $_SESSION['superPassword'], $_SESSION['confirmPassword'], $_SESSION['cancelPassword']);
        } else if ($_POST['cancelpassword']) {
            // 
            V_ManualInDispensary(); // ... start again
        } else {
            // Should never get here ...
        };
        break;


        case 120:
        if (!$HTTP_GET_VARS['w']) { $warning = ""; };
        O_ChangePasswordForm($warning);
        break;

        case 121: 
        $pass1=$_POST['pass1'];
        $pass2=$_POST['pass2'];
        OA_ChangePasswordAction($pass1, $pass2);
        break;

        case 200:
        ZZZ_AskForNewPassword();
        break;

        case 202:
        $usernameForReset = $_POST['usernameforreset'];
        ZZZ_SetNewPassword($usernameForReset);
        break;

        case 210:
        X_AskForPassword();
        break;



    };
};
Q_html_end();


/* Functions begin here */

function X_AskForPassword() {
    global $case;

    print("<HTML>\n<HEAD>\n");
    print("<title>Scripware - Log-on</title>");
    print('<link href="pharmacy_css.css" rel="stylesheet"');
    print(' type="text/css">');
    print('<meta http-equiv="Content-Type"');
    print(' content="text/html; charset=iso-8859-1">');
    print("</HEAD>\n");

    print("<BODY>\n");
// Colour changed (slightly) from 003163 to 003366
    print("<TABLE ALIGN='CENTER' WIDTH='900' BGCOLOR='#003366'>\n");

    print("<tr><td>");
    print("<h1 align='center'>School of Pharmacy</h1>");

    $dBTime = filemtime("dbabstraction.php");
    $lTime = filemtime("logon.php");
    $SWTime = filemtime("ScripWare.php");
    $SMTime = filemtime("ScripModerate.php");

    // Now convert these (which are numbers of seconds) with date (to the required form) ...
    $dBTime = date("j-n-y G:i:s", $dBTime);
    $lTime = date("j-n-y G:i:s", $lTime);
    $SWTime = date("j-n-y G:i:s", $SWTime);
    $SMTime = date("j-n-y G:i:s", $SMTime);

    // This is the dot alert box ...
    print("<a href='javascript:onClick=alert(\"DB $dBTime -- L $lTime -- SW $SWTime -- SM $SMTime  \")'>.</a>");

    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'ScripWare.php?tab=70\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,width=700,height=400')\">");
    print("<div id='noimagebackground'>");
    print("<IMG SRC=images/help.gif width='30' height='30' border='0'");    
    print("align='middle'>");
    print("</div></A>");

//    print("<hr width='500' size='1'>");
    print("</TD></TR>\n");

    print("<TR><TD colspan=3>\n");

    // Start a second table ...
    print("<TABLE ALIGN='CENTER' WIDTH='900' BGCOLOR='#003366'>");
    print("<TR><TD ALIGN=CENTER WIDTH=800>");
/*
    print('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="650" height="470">');
    print('<param name="movie" value="scripware%20entry2_markedit_1.swf">');
    print('<param name=quality value=high>');
    print('<embed src="scripware%20entry2_markedit_1.swf" quality=high
pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="650" height="470">');
    print('</embed> </object>');
*/

    // New picture: no reference to module (name or code) ...
    print("\n\n<img src='ScripwareOnly.jpg'>\n\n");
    print('</TD></TR>');

    print("<TR><TD colspan=2>");

    print("<form method='post' action='".$_SERVER['PHP_SELF']."'>\n");
    print("<div align='center'>\n");
    print("<INPUT TYPE=HIDDEN name='tab' value='0'>\n");
    print("&nbsp;&nbsp;&nbsp;Please log on:\n"); 
    print("&nbsp;&nbsp;&nbsp;Username: \n");
    print("<INPUT TYPE=TEXT SIZE=10 MAXCHAR=10 NAME=username>\n");
    print("&nbsp;&nbsp;&nbsp;Password: \n");
    print("<INPUT TYPE=PASSWORD SIZE=10 MAXCHAR=10 NAME=password>\n");
    print("&nbsp;&nbsp;&nbsp;\n");
    print("<INPUT TYPE=SUBMIT value=Confirm></div></FORM>\n");
    print("</FONT></TD></TR>\n");

    print("<tr><td align='center'><a href=logon.php?tab=200>Forgotten your password? Ask for a new one.</a></td></tr>");

    print("</table>\n");
    // ... second table now closed

    print("</TD></TR>");
    print("</table>");
};

function Y_CheckPassword($username, $thepassword) {
//function Y_CheckPassword($username="", $thepassword="") {
    // This function may be called with $_SESSION['authority'] = false (e.g. on logging on) or = true

    global $case;

    // Assume password to be of legitimate type unless proved to the contrary  
    $_SESSION['passwordLegitmate'] = 1;

    $db=new dbabstraction(); $db->connect() or die ($db->getError());

    $forcePasswordChange = 0;

    //  If logged on but password not changed, change it ...
    if (isset($_SESSION['authority']) && isset($username) && isset($_SESSION['spwchanged'])){
        if ($_SESSION['authority']=="S" && $username[0] != "!" && !$_SESSION['spwchanged']) {
      	     print("<p>You have logged on successfully but must now change your password.</p>");
            $_SESSION['passwordLegitmate'] = 0;
            $warning = "You must change your password from its default setting.";
print("<br>A<br>");
            $forcePasswordChange = 1;
            O_ChangePasswordForm($warning);
        };
    };

    //  If superuser's password equals username, change it ...
    if ($_SESSION['authority']=="S" && $username[0]== "!" && strtoupper($username) == strtoupper($thepassword)) {
        print("<p>You have logged on successfully but must now change your password.</p>");
        $_SESSION['passwordLegitmate'] = 0;
        $warning = "Your password must not be the same as your username.";
print("<br>B<br>");
        $forcePasswordChange = 1;
        O_ChangePasswordForm($warning);
    };

    //  If moderator's  password equals username, change it ...
    if ($_SESSION['authority'] == "M" && strtoupper($username) == strtoupper($thepassword)) {
        print("<p>You have logged on successfully but must now change your password.</p>");
        $_SESSION['passwordLegitmate'] = 0;
        $warning = "Your password must not be the same as your username.";
print("<br>C<br>");
        $forcePasswordChange = 1;
        O_ChangePasswordForm($warning);
    };

    // Already logged on? If not, attempt to do so ...
    if (!$_SESSION['authority']) {


/*
    // Moved to after th eusername has been validated becuse of user-dependent dispensaries ...
        $_SESSION['theIPAddress'] = $_SERVER['REMOTE_ADDR'];

        // This is the part that applied when running from local Apache ...
        // Don't know whether it still exists in PHP5 - it may be in $_SERVER or $_ENV ...
        $_SESSION['theComputerName'] = $GLOBALS['COMPUTERNAME'];

        // Read examination proceeding table
        $table=$db->select("ScripDispensaryMode");
        foreach ($table as $row) {
            $_SESSION['dispensaryMode']=strtoupper($row['DispensaryMode']);

            // The checking mode is the second letter ...
            $_SESSION['checkingMode']=substr($_SESSION['dispensaryMode'], 1, 1);

            // The dispensary mode is the first letter ...
            $_SESSION['dispensaryMode']=substr($_SESSION['dispensaryMode'], 0, 1);
        };


        // In the Dispensary? By default not ...
        $_SESSION['inTheDispensary']='0';

        // $_SERVER['HTTP_HOST'] tells us whether we are on working locally (in which case it equals localhost) or on Granby ...


        // Read all the IP addresses in the Dispensary ...
        $table=$db->select("ScripIPAddresses");
        foreach ($table as $row){
            $anAddress=$row['IPAddress'];
            // To count as being in the Dispensary, candidate IP address must equal an IP address in the database which itself must not be 
            // nul. Matching a nul address will not let a machine be counted as being in the Dispensary!
            if ($anAddress==$_SESSION['theIPAddress'] && $_SESSION['theIPAddress']) {
                $_SESSION['inTheDispensary']='1';
            };
        };
*/



        // Read the tables that contain passwords
        // 1: Students
        $condition=array("UserName"=>$_SESSION['username']);
        $table=$db->select("ScripStudent",$condition);
        foreach ($table as $row) {
            $studentID=$row['StudentID'];
            $firstname=$row['FirstName'];
            $lastname=$row['LastName'];
//            $yearGroup=$row['YearGroup'];
            $spassword=$row['Password'];
            $spwchanged=$row['PWChanged'];
            $sDefaultPW=$row['DefaultPW'];
            $_SESSION['spwchanged']=$spwchanged;

            // Read other things for the location problem caused by Zengenti ...
            $sLocation=$row['Location']; // ... the student's normal location

            $condition=array("Location"=>$sLocation);
            $table=$db->select("ScripDispensaryMode",$condition);
            foreach ($table as $row) {
                $_SESSION['locationDetection'] = $row['LocationDetection'];
            };
        };

//print("Username: " . $_SESSION['username'] . "<br>");
//print("ID: " . $studentID . "<br>");
//print("The location is $sLocation and locationDetection is " . $_SESSION['locationDetection'] . "<br>");



        // 2: Moderators
        $condition=array("UserName"=>$_SESSION['username']);
        $table=$db->select("ScripModerator",$condition);
        foreach ($table as $row) {
            $moderatorID=$row['ModeratorID'];
            $mpassword=$row['Password'];
            $realModerator=$row['RealModerator']; 
        };
/*
        // 3: Project-workers (researchers)
        $condition=array("UserName"=>$_SESSION['username']);
        $table=$db->select("ScripProject", $condition);
        foreach ($table as $row) {
            $pID=$row['ID'];
            $ppassword=$row['Password'];
        };
*/


        // Has student changed his password?
        if ($_SESSION['spwchanged']) {
            // Yes - must match password to log on ...
            $sPasswordCorrect = ($thepassword && $thepassword == $spassword);

        } else {
            // No - must match default password to log on ...
            $sPasswordCorrect = ($thepassword && $thepassword == $sDefaultPW);

        };
        // ... note that the password supplied is not allowed to be nul




        // We now loop through all the possible types of user


        if ($studentID && $sPasswordCorrect) {
            $_SESSION['table']="Student";
            $_SESSION['what']="a student";
            $_SESSION['what']="Student";
            $_SESSION['ID']=$studentID;
            $_SESSION['authority'] = "S";

            // By default, current user is not an adminstrator ...
            $adminNumber = 0;

            // However, if the username begins with a !, we will check to see whether the user does have admin rights ...
            if ($_SESSION['username'][0] == "!"){
                // ScripAdmin records ID numbers (from ScripStudent) so we look for that ...
                $condition=array("SuperUserID"=>$_SESSION['ID']);
                $table=$db->select("ScripAdmin", $condition);
                foreach ($table as $row) {
                    $adminNumber = $row['ID'];
                    $adminLevel = $row['Level'];
                };
            };

            // If we have found an administrator, set some values ...
            if ($adminLevel > 0){
//                $_SESSION['table']="(NUL)";
                $_SESSION['what']="an administrator (No. $adminNumber with level $adminLevel)";
                $_SESSION['what']="Administrator (No. $adminNumber with level $adminLevel)";
//                $_SESSION['ID']='0';
                $_SESSION['authority']="A";
            };

            if (strtoupper($username) == strtoupper($thepassword)) {
                $_SESSION['passwordLegitmate'] = 0;
                $warning = "Password must not be the same as username.";
                $forcePasswordChange = 1;
                O_ChangePasswordForm($warning);
            };

            // Password still at default ...
            if (!$spwchanged) {
                $_SESSION['passwordLegitmate'] = 0;
                $warning = "Password must be changed from default value.";
                $forcePasswordChange = 1;
                O_ChangePasswordForm($warning);
            };

            // Special check on username beginning with ! ...
            if ($_SESSION['username'][0] == "!") {
                $unwithoutpling = strtoupper(substr($_SESSION['username'], 1));
                if ($unwithoutpling == strtoupper($thepassword)) {
                    $_SESSION['passwordLegitmate'] = 0;
                    $warning = "Password must not be the same as username even without exclamation mark!";
                    $forcePasswordChange = 1;
                    O_ChangePasswordForm($warning);
                };

                if (!$adminNumber){
                    $_SESSION['extra']="- but with <b>special</b> status -";
                };
            };
        } else if ($moderatorID && $thepassword == $mpassword && $realModerator) {
            // Only real moderators can moderate; others can start checking exercises ...
            $_SESSION['table']="Moderator";
            $_SESSION['what']="a moderator";
            $_SESSION['what']="Moderator";
            $_SESSION['ID']=$moderatorID;
            $_SESSION['authority']="M";
                if (strtoupper($username) == strtoupper($thepassword)) {
                    $warning = "Password must not be the same as username.";
                    $forcePasswordChange = 1;
                    O_ChangePasswordForm($warning);
                };
        } else if ($pID && $thepassword == $ppassword) {
            // This is someone working on a project ...
            $_SESSION['table']="Project";
            $_SESSION['what']="a researcher";
            $_SESSION['what']="Researcher";
            $_SESSION['ID']=$pID;
            $_SESSION['authority']="P";
                if (strtoupper($username) == strtoupper($thepassword)) {
                    $warning = "Password must not be the same as username.";
                    $forcePasswordChange = 1;
                    O_ChangePasswordForm($warning);
                };
//        } else if ($aUserName == $_SESSION['username'] && $thepassword == $apassword) {
//            $_SESSION['table']="(NUL)";
//            $_SESSION['what']="the administrator";
//            $_SESSION['ID']='0';
//            $_SESSION['authority']="A";
        } else {
            print("<html><body>\n");
            print("<table");
            print("align=center>\n<tr>\n<td>\n");
            print("<font color=red><h2>Incorrect password or username");
            print("</h2></font>");
            print("<p>Username and password are case sensitive. ");
            print("Check the caps lock and try again</p>\n");
            print("<p><a href='logon.php'>Try Again</a></p>\n");
            print("</td>\n</tr></table>");
     	     print("</body></html>");
        };
    };




    // 
    $nonUKUsername = 0;
    // Which usernames are in dispensaries where we're not detecting IP addresses? At present: those beginning H or K (i.e. Malaysia)
    // Now Zengenti have messed up the IP addresses, we now offer a rapid expiry access code to the UK too based on a setting in a
    // table (itself controlled from the administrative interface) ... 
//    if (substr(strtoupper($username), 0, 1) == "K" || substr(strtoupper($username), 0, 1) == "H"){
    if (substr(strtoupper($username), 0, 1) == "K" || substr(strtoupper($username), 0, 1) == "H" || !$_SESSION['locationDetection']){
        $nonUKUsername = 1;
    };


    // Candidate for manual entry into Dispensary ...
    if ($nonUKUsername && $_SESSION['wrongPassword']){
        // Offer option of manual log-in to the Dispensay if user is a K user and has not declined it: more specifically ask "Practising
        // outside a formal class" or "In a supervised class"

        if (!$forcePasswordChange){
            // But don't go into this if password is still to be changed ...
            V_ManualInDispensary();
        };

    } else {
        // Write logon details to ScripLogon if student ...

        if ($_SESSION['theComputerName']){
            // This is taken from $GLOBALS ... if it exists then we are using localhost and use the name for searching ...
            $condition=array("MachineName"=>$_SESSION['theComputerName']);
        } else {
            // if not then we are using Granby and search using the IP address ...
            $condition=array("IPAddress"=>$_SESSION['theIPAddress']);
        };

        $table=$db->select("ScripIPAddresses",$condition);
        foreach ($table as $row) {
            $location=$row['Location'];
            $_SESSION['checkingMachine'] = $row['Checking'];
        };

        if (!$location) { $location="Not known"; };

        if ($_SESSION['table']=="Student") {
            $ltext=array("StudentID"=>$studentID,"Username"=>$_SESSION['username'],
                     "FirstName"=>$firstname,"LastName"=>$lastname,
                     "IPAddress"=>$_SESSION['theIPAddress'],"Location"=>$location,
                     "InDispensary"=>$_SESSION['inTheDispensary'],
                     "DispensaryMode"=>$_SESSION['dispensaryMode'],
                     "Date"=>date("y-m-d"),"Time"=>date("H:i:s"));
            $db->insert("ScripLogon", $ltext);
        };


        // The only people who can go further are:
        //      1. Non-students (i.e. authority != "S");
        //      2. Students with ! users; and
        //      3. Students who have changed their passwords ...
//    if ($_SESSION['spwchanged'] || 
//              $_SESSION['authority'] != "S") {

        if ($_SESSION['authority'] && $_SESSION['passwordLegitmate']) {
            if ($_SESSION['authority']) {
                print("<font color='#ffffff'>");
                print("<p>Scripware dated April 5th, 2011.</p>");
                if ($_SERVER['HTTP_HOST'] != "localhost") {
                    // Running from GRANBY ...
                    print("<p>Scripware is running on ".$_SERVER['HTTP_HOST'].".</p>");
                } else {
                    // Running LOCALLY ...
                    $_SESSION['inTheDispensary'] = 1; // ... only possible to run locally in the Dispensary
                    $hostName = strtolower($_ENV['COMPUTERNAME']);
                    // Assume that the seat number follows the string "disp" ...
                    $pos = strpos($hostName, 'disp');
                    $hostName = strtoupper(substr($hostName, $pos + 4));
                    print("<p>Scripware is running on PC $hostName.</p>");
                };

                // Get all the details for this student ...
                $condition=array("StudentID"=>$_SESSION['ID']);
                $table=$db->select("ScripStudent",$condition);
                // Find location of student and then find mode of the student's dispensary ...
                foreach ($table as $row) {
                    $_SESSION['fN']=$row['FirstName'];
                    $_SESSION['lN']=$row['LastName'];
                    $_SESSION['locationOfStudent']=$row['Location'];
                    $_SESSION['yearGroupOfStudent']=$row['YearGroup'];
                    $_SESSION['studentSaturnNumber']=$row['StudentSaturnNumber'];
                    $_SESSION['newStudentNumber']=$row['NewStudentNumber'];
                    $_SESSION['seat']=$row['Seat'];
                };

                // Get all the details for this student's dispensary ...
                // N.B. Each dispensary should have a unique name and therefore occur in ScripDispensaryMode only ONCE - if necessary give different
                // dispensaries on same campus unique names ...  
                $condition=array("Location"=>$_SESSION['locationOfStudent']);

                $table=$db->select("ScripDispensaryMode", $condition);
                if (count($table)){
                    foreach ($table as $row){
                        $_SESSION['dispensaryMode']=strtoupper($row['DispensaryMode']);

                        // The checking mode is the second letter ...
                        $_SESSION['checkingMode']=substr($_SESSION['dispensaryMode'], 1, 1);

                        // The dispensary mode itself is the first letter ...
                        $_SESSION['dispensaryMode']=substr($_SESSION['dispensaryMode'], 0, 1);
                    };
                } else {
                    print("<p>Your location has no dispensary.</p>");
                };

                // Read the IP address of PC ...
                $_SESSION['theIPAddress'] = $_SERVER['REMOTE_ADDR'];



                // January 2017 - following CMS changes ...
                $getallheaders = getallheaders(); 
 
                $allIPAddresses = $getallheaders['X-Forwarded-For']; 
                // Check whether this element exists ... 
                if ($allIPAddresses){ 
                    // It does; explode it into an array on the comma � 
                    $eachIPAddress = explode(",", $allIPAddresses); 

                    // Now use one of these elements (the first i.e. No. 0, we think) as the IP address of the user's machine �

                    $_SESSION['theIPAddress'] = $eachIPAddress[0]; 

                    // Also: Show both $allIPAddresses and $theIPAddress on the logon page 
 
                } else { 
                    // Use the old route ... 
                }; 




                // This is the part that applied when running from local Apache ...
                // Don't know whether it still exists in PHP5 - it may be in $_SERVER or $_ENV ...
                $_SESSION['theComputerName'] = $GLOBALS['COMPUTERNAME'];


                if (!$nonUKUsername){
                    // In the Dispensary? By default not ...
                    $_SESSION['inTheDispensary']='0';

                    // Read all the IP addresses in the Dispensary ...
                    $table=$db->select("ScripIPAddresses");
                    foreach ($table as $row){
                        $anAddress=$row['IPAddress'];
                        // To count as being in the Dispensary, candidate IP address must equal an IP address in the database which itself must not be 
                        // nul. Matching a nul address will not let a machine be counted as being in the Dispensary!
                        if ($anAddress==$_SESSION['theIPAddress'] && $_SESSION['theIPAddress']) {
                            $_SESSION['inTheDispensary']='1';
                        };
                    };
                };

                // PERSONAL details ...
                print("<p>Your personal details:</p>");

                print("<ul>");
                print("<li>Name: " . $_SESSION['fN'] . " " . $_SESSION['lN'] . "</li>");
                print("<li>Username: " . $_SESSION['username'] . "</li>");
                print("<li>Saturn Number: " . $_SESSION['studentSaturnNumber'] . "</li>");
                print("<li>Campus Solutions Number: " . $_SESSION['newStudentNumber'] . "</li>");
                print("<li>Year group: " . $_SESSION['yearGroupOfStudent'] . "</li>");
                print("<li>Location: " . $_SESSION['locationOfStudent'] . "</li>");
                print("<li>Seat: " . $_SESSION['seat'] . "</li>");
                print("</ul>");

                // SCRIPWARE details ...
                print("<p>Your current Scripware details:</p>");
                print("<ul>");

                // STATUS of user ...
                print("<li>Status: " . $_SESSION['what'] . " (" . $_SESSION['authority'] . ")</li>");

                // IP ADDRESS (where applicable) ...
                if ($_SERVER['HTTP_HOST'] != "localhost") {
                    print("<li>IP Address: " . $_SESSION['theIPAddress'] . "</li>");
                };


                print("<li>Dispensary location method: <b>");

                switch ($_SESSION['locationDetection']){
                    case 1: print("IP address");
                    break;
                    case 0: print("REAC");
                    break;
                    default: print ("Cannot be found");
                }

                print("</b></li>");




                // LOCATION
                print("<li>Location: ");
                if ($_SESSION['inTheDispensary']){ print("You are <b>IN</b> the Dispensary"); } else { print("You are <b>NOT</b> in the Dispensary"); };
                print("</li>");



                // DISPENSARY MODE (if in Dispensary) or PROGRAM MODE (if not) ... 
                if ($_SESSION['inTheDispensary']) {
                    print("<li>Dispensary mode: ");
                    if ($_SESSION['dispensaryMode']=="U") { print("<b>Unsupervised</b> "); };
                    if ($_SESSION['dispensaryMode']=="P") { print("<b>Practice</b> "); };
                    if ($_SESSION['dispensaryMode']=="E") { print("<b>Examination</b> "); };
                    print("</li>");

                    print("<li>Checking mode: ");
                    if ($_SESSION['checkingMode']=='C'){
                        print("The Dispensary <b>IS</b> in checking mode.");
                    } else {
                        print("The Dispensary is <b>NOT</b> in checking mode.");
                    };
                    print("</li>");


                } else {
                    print("<li>Program mode: Unsupervised</li>");
                };
                print("</ul>");



//                print("<p>You are logged on as ".$_SESSION['what']." (authority code: ".$_SESSION['authority'].") ".$_SESSION['extra']." and your computer "); 
//                if ($_SERVER['HTTP_HOST'] != "localhost") { print("(".$_SESSION['theIPAddress'].")"); };
//                print(" is ");
//                if (!$_SESSION['inTheDispensary']) { print("not "); };
//                print("registered as being in the Dispensary.</p>");

/*
                if ($_SESSION['inTheDispensary']) {  
                    print("The Dispensary is currently in ");
                    if ($_SESSION['dispensaryMode']=="U") {
                        print("<b>unsupervised</b> ");
                    };
                    if ($_SESSION['dispensaryMode']=="P") {
                        print("<b>practice</b> ");
                    };
                    if ($_SESSION['dispensaryMode']=="E") {
                        print("<b>examination</b> ");
                    };
                    print("mode.");
                    print("<br><br>");

                    if ($_SESSION['checkingMode']=='C'){
                        print("The Dispensary is in checking mode.");
                    } else {
                        print("The Dispensary is not in checking mode.");
                    };
                    print("<br>");

                    if ($_SESSION['checkingMachine']){
                        print("This is a checking PC.");
                    } else {
                        print("The is not a checking PC.");
                    };
                } else {
                    print("The program is in unsupervised mode.");
                };
*/
                print("<p>These are your options:</p>");
                print("<ul>\n");

                // BUILD THE MENU:
                // Change Password
                print("<li>");
                print("<a href='logon.php?tab=120'>Change Password</a>");
                print("</li><br><br>");

                print("</font>");
            };
    

/*
if ($_SESSION['username'] == "cczjch"){
$_SESSION['authority']="S";
$_SESSION['checkingMode'] = "C";
$_SESSION['checkingMachine'] = 1;
$_SESSION['username'][0] = "!";
};
*/



    // ORDINARY - (i.e. unsupervised or home) - Use

    // Need an entry in table ScripStudents AND (NOT In Dispensary
    // OR (In Dispensary AND In Mode "U"))

//print("Compiling menu:<br>");

//print("Table is " . $_SESSION['table'] . "<br>");
//print("Table is " . $_SESSION['inTheDispensary'] . "<br>");
//print("Table is " . $_SESSION['dispensaryMode'] . "<br>");

        if ($_SESSION['table']=="Student" &&
          (!$_SESSION['inTheDispensary'] || $_SESSION['inTheDispensary'] &&
                              $_SESSION['dispensaryMode']=="U")) {
            print("<li>");

// Mixed case (as per filename) ...
            print("<a href=ScripWare.php?");
// All lower case (which is what the programs actually seems to want) ...
//            print("<a href=scripware.php?");
            print("t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&");  
            print("d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode'].">");
  
            print("Run Scripware Exercises");
            print("<br><br></a>");
            print("</li>");
        };


    // DISPENSARY Use
        if ($_SESSION['table']=="Student") {
            if ($_SESSION['inTheDispensary']) {
                if ($_SESSION['dispensaryMode']=="E" || $_SESSION['dispensaryMode']=="P") {
                    print("<li>");
                    print("<a ");
                    print("href=ScripWare.php?");
                    print("t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&");  
                    print("d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode'].">");  
                    print("Run Scripware exercises");
                    print("<br><br></a>");
                    print("</li>");
                };
            };
        };



        // Make this available to fourth years and ! users only ...
        if ($_SESSION['yearGroupOfStudent'] == 4 || $_SESSION['username'][0] == "!"){
            print("<li>");
            print("<a>");
            print("PMR");
            print("</a>");
            print("</li>");
            print("<br><br>");
        };


    // Look at Results of Exercise for ADMINISTRATORS and MODERATORS:
    // Condition: Be logged on and NOT (In Dispensary and Mode="E");
//        if ($_SESSION['authority'] && !($_SESSION['inTheDispensary'] && $_SESSION['dispensaryMode']=="E") && $_SESSION['authority'] != "P") {

/*
        if ($_SESSION['authority'] && !($_SESSION['inTheDispensary'] && $_SESSION['dispensaryMode']=="E") && ($_SESSION['authority'] == "A" || $_SESSION['authority'] == "M")) {
            $variety = 1; // ScripModerate - variety 1 => individual
            print("<li>");
            print("<a href=ScripModerate.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&v=$variety>");  
            print("Look at results of individual exercises");
            print("</a><br><br>");
            print("</li>");
        };
*/

    // Look at Results of Exercise for STUDENTS:
    // Condition: Be logged on and NOT (In Dispensary and Mode="E");
        if ($_SESSION['authority'] && !($_SESSION['inTheDispensary'] && $_SESSION['dispensaryMode']=="E") && $_SESSION['authority'] == "S") {
            $variety = 1; // ScripModerate - variety 1 => individual
            print("<li>");
            print("<a href=ScripModerate.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&v=$variety>");  
            print("Look at results of individual exercises");
            print("</a><br><br>");
            print("</li>");
        };

    // Look at GENERIC Results of Exercise for ADMINISTRATORS and MODERATORS:
    // Condition: Be logged on;
//        if ($_SESSION['authority'] && $_SESSION['authority'] != "P") {

    // This is the old moderating route for generic results:
/*
        if ($_SESSION['authority'] && ($_SESSION['authority'] == "A" || $_SESSION['authority'] == "M")) {
            $variety = 2; // ScripModerate - variety 2 => generic
            print("<li>");
            print("<a href=ScripModerate.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&v=$variety>");  
            print("Look at generic comments");
            print("</a>");
            print("<br><br>");
            print("</li>");
        };
*/

    // Look at GENERIC Results of Exercise for STUDENTS:
    // Condition: Be logged on;
//        if ($_SESSION['authority'] && $_SESSION['authority'] != "P") {

        if ($_SESSION['authority'] && $_SESSION['authority'] == "S") {
            $variety = 2; // ScripModerate - variety 2 => generic
            print("<li>");
            print("<a href=ScripModerate.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&v=$variety>");  
            print("Look at generic comments");
            print("</a>");
            print("<br><br>");
            print("</li>");
        };

    // Look at CALCULATION Results of Exercise for ADMINISTRATORS and MODERATORS:
    // Condition: Be logged on;
//        if ($_SESSION['authority'] && $_SESSION['authority'] != "P") {

    // This is the old moderating route for generic results:
/*
        if ($_SESSION['authority'] && ($_SESSION['authority'] == "A" || $_SESSION['authority'] == "M")) {
            $variety = 3; // ScripModerate - variety 3 => calculations
            print("<li>");
            print("<a href=ScripModerate.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&v=$variety>");  
            print("Look at calculations");
            print("</a><br><br>");
            print("</li>");
        };
*/

    // Look at CALCULATION Results of Exercise for STUDENTS:
    // Condition: Be logged on;
//        if ($_SESSION['authority'] && $_SESSION['authority'] != "P") {

        if ($_SESSION['authority'] && $_SESSION['authority'] == "S") {
            $variety = 3; // ScripModerate - variety 3 => calculations
            print("<li>");
            print("<a href=ScripModerate.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&v=$variety>");  
            print("Look at calculations");
            print("</a><br><br>");
            print("</li>");
        };



    // Look at project options:
    // Condition: Be logged on with authority P;
        if ($_SESSION['authority'] == "P") {
//            $variety = 2; // ScripModerate - variety 2 => generic
            print("<li>");
            print("<a href=ScripProject.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&v=$variety>");  
            print("Project options");
            print("</a><br><br>");
            print("</li>");
        };

    // Checking Mode:
    // Condition: Logged on AND ((In checking mode AND On a checking machine) OR Superuser)
        if ($_SESSION['authority'] && (($_SESSION['checkingMode'] == "C" && $_SESSION['checkingMachine']) || $_SESSION['username'][0] == "!")) {
            print("<li>");
            print("<a href=ScripChecking.php?");
            print("t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority']."&");  
            print("d=".$_SESSION['inTheDispensary']."&m=".$_SESSION['dispensaryMode']."&");
            print("c=".$_SESSION['checkingMode']."&p=".$_SESSION['checkingMachine']);
            print(">");  
            print("Checking exercises");
            print("</a><br><br>");
            print("</li>");
        };

// ADMINISTRATOR
// Must have logged on as Administrator
        if ($_SESSION['authority']=="A") {
            print("<li>");
//            print("<a href=scripadmin.php?t=".$_SESSION['table']."&i=".$_SESSION['ID']."&a=".$_SESSION['authority'].">Run administration software<br><br></a>");
//print_r($_SESSION);

            // Ought to check this properly - this is cheating. If an adminstator has tochange his password for some reason, then adminNumber and adminLevel are 
            // re-set to zero (probably because they neither session variables nor passed properly). The ID is kept so to make life simple, read them again ... 
            $condition = array("SuperUserID"=>$_SESSION['ID']);
            $table=$db->select("ScripAdmin", $condition);
            foreach ($table as $row){
                $adminNumber = $row['ID'];
                $adminLevel = $row['Level'];
            };


            print("<a href=scripadmin.php?n=".$adminNumber."&l=".$adminLevel."&a=".$_SESSION['authority']."><b>Run administration software</b><br><br></a>");
            print("</li>");

            print("<li>");
            print("<a href=../onlineuserguide/AdministrationUserGuide.html>View administration user guide<br><br></a>");
            print("</li>");
        };
    };

};


};

function O_ChangePasswordForm($warning="") {
    global $case;

   // Write form to ask for new password
    print("<TR>");

    print("<td><font color='#ffffff'>");
    print("You are " . $_SESSION['username'] . ". Change your password.");
    print(" ($warning)");
    print("</font></td>");

    print("<TD colspan=2 rowspan=3>");
//    print("<IMG SRC=images/Titlescreen.jpg>");
    print("</TD>");
    print("</TR>");

    // Password change form ...
    print("<tr><td>");
    print("<FORM METHOD='POST'>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE='121'>");
    print("<FONT COLOR='#ffffff'>");
    print("<p>Enter your new password in both fields and click on submit.</p>");
    print("<p>$warning</p>");
    print("</FONT>");

    print("<p><input type='PASSWORD' name='pass1'></p>");
    print("<p><input type='PASSWORD' name='pass2'></p>"); 
    print("<p><input type='SUBMIT' value='Confirm'></p>");   
    print("</form>");
    print("</td></tr>");

    print("<TR><TD>");
    print("<FONT COLOR='#FFFFFF'><P><A HREF='logon.php?tab=10'>Return to Menu</P></FONT>");
    print("</TD></TR>");
    print("</TABLE>");
};

function OA_ChangePasswordAction($pass1, $pass2) {
    global $case;

//print("<pre>"); print_r($_SESSION);

    $minchar = 7; $maxchar = 20;

    if ($_SESSION['username'][0] == "!") {
        $unwithoutpling = strtoupper(substr($_SESSION['username'], 1));
    };

    $punctuation = ".,:;!?+-*|<>()[]{}~#@";
    $posPunctuation = 0;
    for ($i = 0; $i < strlen($punctuation); $i++) {
        if (strpos($pass1, $punctuation[$i]) !== false) {
            $posPunctuation = 1;
        };
    };

    $digit = "0123456789";
    $posDigit = 0;
    for ($i = 0; $i < strlen($digit); $i++) {
        if (strpos($pass1, $digit[$i]) !== false) {
            $posDigit = 1;
        };
    };

    if ($pass1 != $pass2) {
        print("<FONT COLOR='#ffffff'>");
        print("<P>Error. The two passwords do not match.</P>");
        print("<P><A HREF=logon.php?tab=120&w=17>Try Again</P>");
        print("</FONT>");
    } elseif (!$pass1) {
        print("<FONT COLOR='#ffffff'>");
        print("<P>Error. Blank password not allowed.</P>");
        print("<P><A HREF=logon.php?tab=120&>Try Again</P>");
        print("</FONT>");
    } elseif (strtoupper($pass1) == strtoupper($_SESSION['username'])) {
        print("<font color='#ffffff'>");
        print("<p>Error. Password must not be the same as your username.</p>");
        print("<p><a href=logon.php?tab=120>Try Again</p>");
        print("</font>");
    } elseif ($unwithoutpling == strtoupper($pass1)) {
        print("<font color='#ffffff'>");
        print("<p>Error. Password must not be the same as your username even without the exclamation mark!</p>");
        print("<p><a href=logon.php?tab=120>Try Again</p>");
        print("</font>");
    } elseif ($_SESSION['authority'] == "S" && (strlen($pass1) < $minchar)) {
        print("<font color='#ffffff'>");  
        print("<p>Error. Password must not be shorter than $minchar characters long.</p>");
        print("<p><a href=logon.php?tab=120>Try Again</p>");
        print("</font>");
    } elseif ($_SESSION['authority'] == "S" && (strlen($pass1) > $maxchar)) {
        print("<font color='#ffffff'>");  
        print("<p>Error. Password must not be longer than $maxchar characters long.</p>");
        print("<p><a href=logon.php?tab=120>Try Again</p>");
        print("</font>");
    } elseif ($_SESSION['authority'] == "S" && ($posPunctuation == '0')) {
        print("<font color='#ffffff'>");  
        print("<p>Error. Password must contain at least one item of punctuation.</p>");
        print("<p><a href=logon.php?tab=120>Try Again</p>");
        print("</font>");
    } elseif ($_SESSION['authority'] == "S" && ($posDigit == '0')) {
        print("<font color='#ffffff'>");  
        print("<p>Error. Password must contain at least one digit.</p>");
        print("<p><a href=logon.php?tab=120>Try Again</p>");
        print("</font>");
    } else { // Change password
        $db=new dbabstraction();
    	 $db->connect() or die ($db->getError());

        $condition=array('UserName'=>$_SESSION['username']);
//print_r($condition);
        $_SESSION['spwchanged']='1';
        $data=array('Password'=>$pass1, 'PWChanged'=>$_SESSION['spwchanged']);


        // March 2013: Added (via an OR operator) the "A" option since admin is now run through the student table ...
        if ($_SESSION['authority'] == "S" || $_SESSION['authority'] == "A") {
            $_SESSION['spwchanged']='1';
            $data=array('Password'=>$pass1, 'PWChanged'=>$_SESSION['spwchanged']);
            $db->update("ScripStudent", $data, $condition);
        };

        if ($_SESSION['authority'] == "M") {
            $data=array('Password'=>$pass1);
            $db->update("ScripModerator", $data, $condition);
        };

        $_SESSION['passwordLegitmate'] = 1;
	print("<font color='#ffffff'>");
	print("<p>Password updated</p></font>");
    	print("<p><A HREF='logon.php?tab=20'>Return to Menu</p>");
    };
};

function A_html_begin($title) {
    global $tab;
    global $case;
    print ("<html>\n");

    print ("<head>\n");

    // Open window maximised ...
    print("<script language='JavaScript1.2'>\n");
    print("<!--\n");

    print("top.window.moveTo(0,0);\n");
    print("if (document.all) {\n");
    print("top.window.resizeTo(screen.availWidth,screen.availHeight);\n");
    print("}\n");
    print("else if (document.layers||document.getElementById) {\n");
    print("if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){\n");
    print("top.window.outerHeight = screen.availHeight;\n");
    print("top.window.outerWidth = screen.availWidth;\n");
    print("}\n");
    print("}\n");
    print("//-->\n");
    print("</script>\n\n");

    // Disable selection - useful to when changing passwords ...
    print("<script type='text/javascript'>\n");
    print("function disableSelection(target){\n");
    print("    if (typeof target.onselectstart!='undefined')\n");
    print("        target.onselectstart=function(){return false}\n");
    print("    else if (typeof target.style.MozUserSelect!='undefined')\n");
    print("        target.style.MozUserSelect='none'\n");
    print("    else\n");
    print("    target.onmousedown=function(){return false}\n");
    print("        target.style.cursor = 'default'\n");
    print("}\n\n");
    print("</script>");

    if ($title) {
        print ("<title>$title</title>\n");
    };
    print('<link href="pharmacy_css.css" rel="stylesheet"');
    print(' type="text/css">');
    print('<meta http-equiv="Content-Type"');
    print(' content="text/html; charset=iso-8859-1">');
    print("</head>\n");

    print ("<body>\n");
    print("<table align='center' width='800' bgcolor='#003366'>\n");
    print("<tr><td colspan=3><table>\n");
};

function Q_html_end() {
    print ("</table>");
    print("</td></tr></table>");
    print("<script type='text/javascript'>\n");
    print("disableSelection(document.body)\n");
    print("</script>\n\n");
    print("</body></html>\n");
};



function ZZZ_AskForNewPassword(){
    // 

    $newPassword = ZZZ_GenerateRandomPassword();

    print("A new password can be posted to your University e-mail account.<br><br>");
    print("What is your SCRIPWARE username?");

    print("<form method='post'>");
    print("<input type='hidden' name='tab' value='202'>");
    print("<font color='#ffffff'>");
    print("</font>");
    
    print("<p><input type='text' name='usernameforreset'></p>");   
    print("<p><input type='submit' value='Confirm'></p>");   
    print("</form></td></tr>");

};


function ZZZ_SetNewPassword($usernameInScripware){
    // 

    $db=new dbabstraction(); $db->connect() or die ($db->getError());

    // Need to analyse username:
    // Check a ! or ? hasn't been added to the beginning! (If so, remove it ...)
    $firstCharacter = strToUpper(substr($usernameInScripware, 0, 1));

    if ($firstCharacter == "!" || $firstCharacter == "?"){
        $usernameForReset = substr($usernameInScripware, 1);
    } else {
        $usernameForReset = $usernameInScripware;
    };

    // Now read contents of ScripDomain ...
    $table = $db->select("ScripDomain");
    foreach ($table as $row){
        $beginning[] = $row['Beginning'];
        $statusPosition[] = $row['StatusPosition'];
        $statusLetter[] = $row['StatusLetter'];
        $domain[] = $row['Domain'];
    };

    // Explicitly set this to null ...
    $correctDomain = "";
    $correctDefaultDomain = "";

    // Now loop over the contents of ScripDomain (as put into arrays - above) looking for two distinct matches ... 
    for ($i = 0; $i < count($domain); $i++){
        // Count the number of letters in $beginning[$i] ...
        $nLetters = strlen($beginning[$i]);

        if ($nLetters){
            // MATCH 1: If there ARE letters in $beginning[$i], compare these to the beginning of the beginning of the username ...
            if (strtolower(substr($usernameInScripware, 0, $nLetters)) == strtolower($beginning[$i])){
                // We have a match
                // MATCH 2: Now try to match the status letter in its CORRECT position ...
                // N.B. The function substr is zero based e.g. third letter is number 2 (and not 3).
                if (strtolower(substr($usernameInScripware, $statusPosition[$i] - 1, 1)) == strtolower($statusLetter[$i])){
                    // We also have a match on the status letter can therefore set the value of $correctDomain if not already set; if already set, then there is
                    // is more than one match in ScripDomain for this username - error ...
                    if ($correctDomain){
                        print("ERROR: More than once match for $usernameInScripware in ScripDomain. Report this to Head of Class.");
                    } else {
                        $correctDomain = $domain[$i];
                    };
                };
            };
        } else {
            // If there are NO letters in $beginning[$i], then we are dealing with default values. MATCH 1 has therefore been made by default
            // MATCH 2: Status letter - see notes above, including note on substr being zero-based ...
            if (strtolower(substr($usernameInScripware, $statusPosition[$i] - 1, 1)) == strtolower($statusLetter[$i])){
                // We  have a match on the status letter ...
                if ($correctDefaultDomain){
                    print("ERROR: More than once match for $usernameInScripware in ScripDomain. Report this to Head of Class.");
                } else {
                    $correctDefaultDomain = $domain[$i];
                };
            };
        };

    };


    // If no specific domain has been found, use the default domain ...
    if ($correctDomain == ""){ $correctDomain = $correctDefaultDomain; };

    // Provided this is a real address, build an e-mail address ...

    switch (strtolower($correctDomain)){
        case "unknown":
        // Entry found in ScripDomain but it says UNKNOWN:
        print("The type of your username ($usernameForReset) IS recognised by the table ScripDomain, but no domain is set for it there. Change the password manually.");
        $eMailAddress = "DOMAIN NOT KNOWN"; 
        break;

        case "":
        // Nothing at all found in ScripDomain:
        print("The type of your username ($usernameForReset) is NOT recognised by the table ScripDomain. Change the password manually.");
        $eMailAddress = "TYPE OF USERNAME NOT KNOWN"; 
        break;

        default:
        // Build the address and send the password:
        $eMailAddress = $usernameForReset . "@" . $correctDomain;

        print("Your new password will be e-mailed to $eMailAddress.<br>");

        // Create a new password ...
        $newPassword = ZZZ_GenerateRandomPassword();

        // Change the value of field Password in table ScripStudent to $newPassword for entry that has username $usernameInScripware ...
        $condition=array('UserName'=>$usernameInScripware);
        $data=array('DefaultPW'=>$newPassword, 'PWChanged'=>0);
        $db->update("ScripStudent", $data, $condition);

        // E-mail $newPassword to $eMailAddress ...
        $to = $eMailAddress;
        $subject = "Your New Password for Scripware";
        $body = "Your new password for Scripware is " . $newPassword;
        $headers = "From: Scripware";
        if (mail($to, $subject, $body, $headers)) {
            echo("<p>Message sent successfully.</p>");
        } else {
            echo("<p>Message delivery failed.</p>");
        };
        break;
    };

    // Record what has been done ...
//    $theIPAddress = $_SERVER['REMOTE_ADDR'];
    $now = time();
    $part = explode(" ", date("Y n j G i s", $now));
    $data = array ('Who'=>$usernameInScripware, 'WhoSans'=>$usernameForReset, 'SentTo'=>$eMailAddress, 'Year'=>$part[0], 'Month'=>$part[1], 'Day'=>$part[2], 'Hour'=>$part[3], 'Minute'=>$part[4], 'Second'=>$part[5], 'TimeStamp'=>$now, 'IP'=>$theIPAddress);
    $db->insert("ScripPasswordReset", $data);

    // The "Continue" button ...
    print("<form method='post'>");
    print("<input type='hidden' name='tab' value='210'>");
    print("<p><input type='submit' name='Continue' value='Continue'></p>");   
    print("</form></td></tr>");

};


/*
function ZZZ_SetNewPassword($usernameInScripware){
    // 

    $db=new dbabstraction(); $db->connect() or die ($db->getError());

    // Need to analyse username:
    // Check a ! or ? hasn't been added to the beginning! (If so, remove it ...)
    $firstCharacter = strToUpper(substr($usernameInScripware, 0, 1));

    if ($firstCharacter == "!" || $firstCharacter == "?"){
        $usernameForReset = substr($usernameInScripware, 1);
    } else {
        $usernameForReset = $usernameInScripware;
    };

    // Find first and third letters of username ...
    $firstLetter = strToUpper(substr($usernameForReset, 0, 1));
    $thirdLetter = strToUpper(substr($usernameForReset, 2, 1));

    // We now find the matching domain in ScripDomain. It depends on the first and third letters, but there is also a default set - look for this
    // default set first. Find the default for the current third letter ... 
    $condition = array ('FirstLetter'=>"", 'ThirdLetter'=>$thirdLetter);
    $table = $db->select("ScripDomain", $condition);
    foreach ($table as $row){
        $domain = $row['Domain'];
    };

    // Now look for the first letter in the FirstLetter field of the table. If this is found, this overwrites the default value ...
    $condition = array ('FirstLetter'=>$firstLetter, 'ThirdLetter'=>$thirdLetter);
    $table = $db->select("ScripDomain", $condition);
    if (count($table)){
        foreach ($table as $row){
            $domain = $row['Domain'];
        };
    };

    // Provided this is a real address, build an e-mail address ...
    if (strtolower($domain) != "unknown"){
        // Build the address ...
        $eMailAddress = $usernameForReset . "@" . $domain;

        print("Your new password will be e-mailed to $eMailAddress.<br>");

        // Create a new password ...
        $newPassword = ZZZ_GenerateRandomPassword();

        // Change the value of field Password in table ScripStudent to $newPassword for entry that has username $usernameInScripware ...
        $condition=array('UserName'=>$usernameInScripware);
        $data=array('DefaultPW'=>$newPassword, 'PWChanged'=>0);
        $db->update("ScripStudent", $data, $condition);

        // E-mail $newPassword to $eMailAddress ...
        $to = $eMailAddress;
        $subject = "Your New Password for Scripware";
        $body = "Your new password for Scripware is " . $newPassword;
        $headers = "From: Scripware";
        if (mail($to, $subject, $body, $headers)) {
            echo("<p>Message sent successfully.</p>");
        } else {
            echo("<p>Message delivery failed.</p>");
        };
    } else {
        print("No e-mail domain is known for $usernameForReset. Change the password manually.");
    };

    // Record what has been done ...
//    $theIPAddress = $_SERVER['REMOTE_ADDR'];
    $now = time();
    $part = explode(" ", date("Y n j G i s", $now));
    $data = array ('Who'=>$usernameInScripware, 'WhoSans'=>$usernameForReset, 'SentTo'=>$eMailAddress, 'Year'=>$part[0], 'Month'=>$part[1], 'Day'=>$part[2], 'Hour'=>$part[3], 'Minute'=>$part[4], 'Second'=>$part[5], 'TimeStamp'=>$now, 'IP'=>$theIPAddress);
    $db->insert("ScripPasswordReset", $data);

    // The "Continue" button ...
    print("<form method='post'>");
    print("<input type='hidden' name='tab' value='210'>");
    print("<p><input type='submit' name='Continue' value='Continue'></p>");   
    print("</form></td></tr>");

};
*/


function V_ManualInDispensary(){
    // Is user in dispensary? Can do this via short term password a superuser's username and password:

    // If not in the Dispensary but username begins with a "K" or a "Z", offer the user chance to enter further authorisation to validate presence in
    // Dispensary. Two options - user may try either

    print("<form method='post' action='".$_SERVER['PHP_SELF']."'>\n");
    print("<input type='hidden' name=tab value=40>");

    if ($confirmPassword){
        // The user got here by pressing the Confirm i.e. a password must have been entered wrongly
        print("<h3></h3>");
    };

    // Possible errors: Class access code ...
    if ($_SESSION['shortTermPassword']){
        if ($_SESSION['wrongClassAccess']){
            print("<p>This access code has not been recognised. Please try again or contact a member of academic staff for assistance.</p>");
        };

        if (!$_SESSION['wrongClassAccess'] && $_SESSION['wrongPassword']){
            print("<p>This access code is no longer valid � please contact a member of academic staff for assistance.</p>");
        };
    };

    // Possible errors: Wrong combination of username and password for a ! user ...
    if ($_SESSION['superUsername'] || $_SESSION['superpassword']){
        if ($_SESSION['wrongSupervisor']){
            print("<p>That is not a valid combination of supervisor's username and password.</p>");
        };
    };


    print("<h4><font face='Helvetica'>In what mode are you using Scripware - exam mode or non-exam mode?</font></h4>");
    print("<h4><font face='Verdana'>Click the appropriate button.</font></h4>");

    print("<p align='center'><input type='submit' name='supervised' value='Exam Mode' style='width: 300px'></p>");


    print("<br>");

    print("<p align='center'><input type='submit' name='outside' value='Non-exam Mode' style='width: 300px'></p>");

    print("</form>");

};


function V_ManualInDispensaryPasswords(){
    // Offer passsword options:

    print("<form method='post' action='".$_SERVER['PHP_SELF']."'>\n");
    print("<input type='hidden' name=tab value=41>");

    print("<table>");


    print("<tr>");

    print("<td align='center' colspan='2'>");

    print("<h4><font face='Arial'>If you are in the Dispensary and want to use Scripware in exam mode:</font></h4>");

    print("</td>");

    print("</tr>");

    print("<tr>");

    print("<td valign='top' width='50%'>");

    print("<h4><font face='Arial'>EITHER enter a class access code</font></h4>");

    print("Class access code: ");
    print("<input type='password' name='shorttermpassword'>");

    print("</td>");

    print("<td valign='top' width='50%'>");

    print("<h4><font face='Arial'>OR ask a supervisor to type in a username and password</font></h4>");

    print("Supervisor username: ");
    print("<input type='text' name='superusername'>");
    print("<br><br>");
    print(" Supervisor password: ");
    print("<input type='password' name='superpassword'>");

    print("</td>");

    print("<tr>");

    print("<td colspan='2' align='center'>");

    print("<br>");

    print("<h4><font face='Arial'>and in either case press the Confirm button:</font></h4>");

    print("<input type='submit' name='confirmpassword' value='Confirm'>");

    print("</td></tr>");

    print("<tr><td align='center' colspan='2'>");

    print("<br><br><hr width='50%'><br>");

    print("<h4><font face='Arial'>Otherwise:</font><br><br>");

    print("<input type='submit' name='cancelpassword' value='Cancel'>");

    print("</td>");

    print("</tr>");

    print("</table>");

    print("</form>");
};


function V_ManualInDispensaryAnalysis($shortTermPassword, $superUsername, $superPassword, $confirmPassword, $cancelPassword){
    // 

    $db=new dbabstraction(); $db->connect() or die ($db->getError());

    // By default, set these measures of doing things wringly to TRUE ...
    $_SESSION['wrongSupervisor'] = 1;
    $_SESSION['wrongClassAccess'] = 1;

    // User got here by pressing Confirm ...
    if ($confirmPassword){

        // Read current time ...
        $shortTerm = 10;
        $now = time(); // ... time in seconds from some arbitrary date

        // Read short term passwords ...
        $table=$db->select("ScripShortTermPasswords");

        // Check for three options: class access code entirely wrong, class access code was correct but is no longer valid or class access code is correct ... 
        // Save those that are less than ten minutes (or some other time) old ...
        foreach ($table as $row){

            if (strtolower($shortTermPassword) == strtolower($row['Password'])){
                // Password match ...
                $_SESSION['wrongClassAccess'] = 0;

                if ($now - $row['Created'] < $shortTerm * 60) {
                    // ... and new enough ...
                    $_SESSION['wrongPassword'] = 0;
                    $_SESSION['inTheDispensary'] = 1;
                };
            };
        };
        // ... thus wrong password entirely is $_SESSION['wrongClassAccess'] = 1, while out-of-date password is $_SESSION['wrongClassAccess'] = 0
        // && $_SESSION['wrongPassword'] = 1.

        // Check supervisor's username and password ...
        if ($superUsername && $superPassword){
            $condition = array ('Username'=>$superUsername,'Password'=>$superPassword);
            $table = $db->select("ScripStudent", $condition);
            $count = count($table);
            if ($count){
                // A correct combination has been entered ...
                $_SESSION['wrongPassword'] = 0;
                $_SESSION['inTheDispensary'] = 1;
                $_SESSION['wrongSupervisor'] = 0;
            };
        };
    };

    // User got here by pressing Cancel ...
    if ($cancelPassword){
        $_SESSION['wrongPassword'] = 0;
    };


    // Try again the Y_ function ...
    Y_CheckPassword ($_SESSION['username'], $thePassword);

};



function ZZZ_GenerateRandomPassword(){
    // Generate a new password:

    // Parameters for password: size ...
    $minChar = 7;
    $maxChar = 20;


// SIMPLICITY OVER-RIDE
    $minChar = 5;
    $maxChar = 6;


    // ... at least one each of these ...
    $punctuation = ".,:;!?+-*|<>()[]{}~#@";
    $digit = "0123456789";
    // ... and here are the letters ...
    $letter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMENOPQRSTUVWXYZ";

    // Generate a random size:
    $nChar = rand($minChar, $maxChar);

    // Put the integers 0, 1, 2, ... $nChar into an array called $randomIntegers ...
    for ($i = 0; $i < $nChar; $i++){
        $randomInteger[] = $i;
    };

    // Shuffle it ...
    for ($i = 0; $i < $nChar - 1; $i++){
        // Choose a random element beyond the current element ...
        $randomElement = rand($i + 1, $nChar - 1);
        // Swap ...
        $temp = $randomInteger[$i]; $randomInteger[$i] = $randomInteger[$randomElement]; $randomInteger[$randomElement] = $temp; 
    };

    // How many itemns of punctuation and digits? 
    $nPunctuation = rand(1, 3);
    $nDigit = rand(1, 3);

// SIMPLICITY OVER-RIDE
    $nPunctuation = 0;
    $nDigit = 0;


    // Loop over $randomInteger - successive elements consititute random positions - assign a character to these ...
    for ($i = 0; $i < $nChar; $i++){
        if ($i < $nPunctuation){
            $randomCharacter[$randomInteger[$i]] .= $punctuation[rand(0, strlen($punctuation)-1)];
        } else if ($i < $nPunctuation + $nDigit){
            $randomCharacter[$randomInteger[$i]] .= $digit[rand(0, strlen($digit)-1)];
        } else {
            $randomCharacter[$randomInteger[$i]] .= $letter[rand(0, strlen($letter)-1)];
        };
    };

    // ... now put them into a string ...
    $randomPassword = "";
    for ($i = 0; $i < $nChar; $i++){
        $randomPassword .= $randomCharacter[$i];
    };

    return $randomPassword;
};


?>
