<?php
/*********************************************************************/
/* Author Barry Wood and John Horton                                 */
/* Date   Jul 18 2005                                                */
/* Filename = scripadmin.php                                         */
/* This program runs the administration of the scripware system      */ 
/*********************************************************************/
session_set_cookie_params (60 * 60);
session_start();



// These were session variables before PHP5; now make them part of $_SESSION
//session_register("authority");
// session_register("whichSide"); // not in use
session_register("theusername");
session_register("thepassword");
//session_register("thecheck"); // not sure in une in any case
//session_register("thecheckIP");
session_register("case");

// Read in class to communicate with database
require 'dbabstraction.php';



// A nice text editor (CKEditor):
// Include the CKEditor class ...
include_once "ckeditor/ckeditor.php";

// Use this to create a class instance when required: $CKEditor = new CKEditor()

// Path to the CKEditor directory ...
$CKEditor->basePath = '/ckeditor/';

class scripclass {
    var $caseID, $letterCheckingWeek, $tableNumberOnly;
};

// Program starts here



print_r($GET);
print_r($POST);

// In PHP5 must explicitly set $tab ...
if (isset($_GET['tab'])){ $tab = $_GET['tab']; };
if (isset($_POST['tab'])){ $tab = $_POST['tab']; };

$maxResits=6;
A_html_begin($title, $header);

if (! isset($tab)) {

//    phpinfo();


    // Not used in PHP5 ...
//    $case=new scripclass();

    // Clear the session variable ...
    $_SESSION = array(); 

    // Collect information from logging in:
// What does the addition sign mean? Think it was an error!
//    $table=$_GET['t'];+


//print_r($_GET);

    $_SESSION['adminNumber']=$_GET['n'];
    $_SESSION['adminLevel']=$_GET['l'];
    $_SESSION['authority']=$_GET['a'];




    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Read the level number ...
    $condition = array("ID"=>$_SESSION['adminNumber']);
    $table=$db->select("ScripAdmin", $condition);

    foreach ($table as $row){
        $_SESSION['adminLevel'] = $row['Level'];
    };


    // Write the number and level away to logonadmin table

    ZZZ_WriteToLogonAdmin($_SESSION['adminNumber'], $_SESSION['adminLevel']);



//    $table=$_GET['t'];
//    $ID=$_GET['i'];
//    $_SESSION['authority']=$_GET['a'];

//    $_SESSION['theusername']="admin";
//    $tab=0;

//    B_Logon();

// Test code:
    if ($_SESSION['authority']=="A") {
        $tab=5;
        C_ShowMenu($tab);
    } else {
        B_Logon();
    };

} else {
    switch ($tab) {
        case 0: B_Logon();break;

        case 1: $password=$_POST['password'];
		BA_CheckPassword($password);break;

        case 5: C_ShowMenu($tab);break;


        case 8:
        V_CreateShortTermPassword();
        break;

        case 10: D_ChangePasswordForm(); break;

        case 11: DA_ChangePasswordAction($pass1, $pass2); break;

        // Administrator details:
        case 15:
        AAA_EditAdmin();
        break;

        case 16:
        // The ID of the line that we're editing ...
        $SID=$_POST['ID'];
        AAA_EditSingleAdmin($SID);
        break;

        case 17:
        // The ID of the line that we're editing ...
        $SID=$_POST['ID'];
        // The ID (StudentID i.e. as used in the ScripStudent table) ...
        $administrator=$_POST['administrator'];
        $level=$_POST['level'];
        if ($_POST['Update']) {
            // The button has been pressed: call the update function
            AAA_UpdateAdmin($SID, $administrator, $level);
        } else {
            // The select function has been changed (and has then submitted itself): call the edit-a-single-line function
            AAA_EditSingleAdmin($SID);
        };
        break;




        case 20: E_EditStudentData(); break;

        case 21: $SID=$_POST['ID'];
		EA_EditSingleStudent($SID); break;

        case 22: /*
            $SID=$_POST['SID'];
            $thefirstname=$_POST['firstname'];
            $thelastname=$_POST['lastname'];
            $_SESSION['theusername']=$_POST['username'];
            $thepassword=$_POST['password'];
            $thepracticecode=$_POST['practicecode'];
            $theexamcode=$_POST['examcode'];
            $theinitials=$_POST['initials'];
            $theRealStudent=$_POST['realstudent']; */

            $theStudentID=$_POST['SID'];
            $theStudentSaturnNumber=$_POST['studentsaturnnumber'];
            $theFirstName=$_POST['firstname'];
            $theLastName=$_POST['lastname'];
            $theUserName=$_POST['username'];
            $theDefaultPW=$_POST['defaultpw'];
            $thePracticeCode=$_POST['practicecode'];
            $theExamCode=$_POST['examcode'];
            $theCheckingPracticeCode=$_POST['checkingpracticecode'];
            $thePracticalGroup=$_POST['practicalgroup'];
            $theSeat=$_POST['seat'];
            $theRealStudent=$_POST['realstudent'];
            $thepassword=$_POST['password'];
            $theinitials=$_POST['initials'];
            $theNameForCDRegister=$_POST['nameforcdregister'];
            $thePWChanged=$_POST['pwchanged'];
            $theYearGroup=$_POST['yeargroup'];
            $theLocation=$_POST['location'];

            EB_UpdateStudent($theStudentID, $theStudentSaturnNumber,
                 $theFirstName, $theLastName, $theUserName, $theDefaultPW,
                 $thePracticeCode, $theExamCode,
                 $theCheckingPracticeCode, $thePracticalGroup,
                 $theSeat, $theRealStudent,
                 $thepassword, $theinitials, $theNameForCDRegister,
                 $thePWChanged, $theYearGroup, $theLocation);

//                 $theLastRefNo, $thePWChanged, $thePass,
//                 $theR1, $theR2, $theR3,
//                 $theR4, $theR5, $theR6);




//	    EB_UpdateStudent($SID, $thefirstname, $thelastname, $theusername, 
//		$thepassword, $thepracticecode, $theexamcode, 
//                $theinitials, $theRealStudent);
            break;
  	case 30: F_UploadStudentData($HTTP_POST_FILES); break;
  	case 35: F1_UploadIPData($HTTP_POST_FILES); break;

 	case 40: G_EditPatientData(); break;
 	case 41:  $SID=$_POST['ID'];
		GA_EditSinglePatient($SID); break;
 	case 42: $SID=$_POST['ID'];
	    $thefirstname=$_POST['firstname'];
	    $thelastname=$_POST['lastname'];
	    $theage=$_POST['age']; 	
	    $theaddress1=$_POST['address1']; 	
	    $theaddress2=$_POST['address2']; 	
	    $theaddress3=$_POST['address3']; 	
	    $theaddress4=$_POST['address4']; 	
            $thePMR=$_POST['pmr'];
            $thePatientNHSNumber=$_POST['nhsnumber'];
            $theDiabetes = $_POST['diabetes'];
            $theHypertension = $_POST['hypertension'];
            $theAsthma = $_POST['asthma'];
            $theHypothyroidism = $_POST['hypothyroidism'];
            $theEpilepsy = $_POST['epilepsy'];
            $thePregnancy = $_POST['pregnancy'];
            $theParkinsons = $_POST['parkinsons'];
            $theEczemaPsoriasis = $_POST['eczemapsoriasis'];
            $theAllergies = $_POST['allergies'];
            $thePMRNotes = $_POST['pmrnotes'];
            $theHospitalTTOPatient = $_POST['theHospitalTTOPatient'];
            GB_UpdatePatient($SID, $thefirstname, $thelastname,
                $theage, $theaddress1, $theaddress2, $theaddress3, 	
                $theaddress4, $thePMR, $thePatientNHSNumber,
                $theDiabetes, $theHypertension, $theAsthma, 
                $theHypothyroidism, $theEpilepsy, $thePregnancy,
                $theParkinsons, $theEczemaPsoriasis, 
                $theAllergies, $thePMRNotes, $theHospitalTTOPatient);
            break;
  	case 44: F3_UploadPatientData($HTTP_POST_FILES); break;

 	case 45: G_EditPMRData(); break;
 	case 46:  $SID=$_POST['ID'];
		GA_EditSinglePMR($SID); break;
 	case 47: $SID=$_POST['ID'];
	    $thePMRID=$_POST['pmrid'];
	    $theNameStrengthForm=$_POST['namestrengthform'];
	    $theQuantity=$_POST['quantity']; 	
	    $theDose=$_POST['dose']; 	
	    $theFrequency=$_POST['frequency']; 	
	    $theDaysBeforeToday=$_POST['daysbeforetoday']; 	
	    $theNumber=$_POST['number']; 	
            GB_UpdatePMR($SID, $thePMRID,
                       $theNameStrengthForm, $theQuantity, $theDose,
                       $theFrequency, $theDaysBeforeToday, $theNumber);
                       break;

 	case 50: H_EditPrescriberData(); break;
 	case 51:  $SID=$_POST['ID'];
		HA_EditSinglePrescriber($SID); break;
 	case 52: $SID=$_POST['ID'];
	    $thename=$_POST['name'];
	    $thequal=$_POST['qual'];
	    $thepracticeid=$_POST['practiceid'];
	    $theBeepNumber=$_POST['beepnumber'];
	    $thenhsnumber=$_POST['nhsnumber'];
	    $thewritingexemption=$_POST['writingexemption'];
	    $thesigfilename=$_POST['sigfilename'];
	    $theSecondaryPrescriberID=$_POST['secondaryprescriberid'];
            $thePrivateCD = $_POST['privatecd'];
	    HB_UpdatePrescriber($SID, $thename, 
		$thequal, $thepracticeid,  $theBeepNumber, $thenhsnumber,
		$thewritingexemption, $thesigfilename,
                $theSecondaryPrescriberID, $thePrivateCD); break;

        // Interaction model answers:
 	case 55: G_EditInteractionMA(); break;
 	case 56:  $SID=$_POST['ID'];
            unset ($_SESSION['caseID']);
            GA_EditSingleInteractionMA($SID, $theCaseID, 
                      $theWithWhat, $thePMRNumber, $theSeverity,
                      $theSuggestion, $theDeleteWhich,
                      $theCommentI,$theCommentS);
                break;

 	case 57: $SID=$_POST['ID'];
	    $theCaseID=$_POST['caseid'];
	    $theWithWhat=$_POST['withwhat'];
	    $thePMRNumber=$_POST['pmrnumber']; 	
	    $theSeverity=$_POST['severity']; 	
	    $theSuggestion=$_POST['suggestion'];
	    $theDeleteWhich=$_POST['deletewhich'];
	    $theCommentI=$_POST['commenti'];
	    $theCommentS=$_POST['comments'];

            if ($_POST['Update']) {
                GB_UpdateInteractionMA($SID, $theCaseID,
                       $theWithWhat, $thePMRNumber,
                       $theSeverity, $theSuggestion,
                       $theDeleteWhich,
                       $theCommentI, $theCommentS);
                break;
            } else {
                GA_EditSingleInteractionMA($SID, $theCaseID, 
                      $theWithWhat, $thePMRNumber, $theSeverity,
                      $theSuggestion, $theDeleteWhich,
                      $theCommentI, $theCommentS);
                break;
            };

  	case 60: I_EditPracticeData(); break;
 	case 61:  $SID=$_POST['ID'];
		IA_EditSinglePractice($SID); break;
 	case 62: $SID=$_POST['ID'];
		$theCode=$_POST['thecode'];
		$thePracticeOrUnit=$_POST['thepracticeorunit'];
		$theName=$_POST['thename'];
		$theAddress1=$_POST['theaddress1'];
		$theAddress2=$_POST['theaddress2'];
		$theAddress3=$_POST['theaddress3'];
		$theAddress4=$_POST['theaddress4'];
		$thePostCode=$_POST['thepostcode'];
		$theTelephoneNumber=$_POST['thetelephonenumber'];
		$theTrustID=$_POST['thetrustid'];
		IB_UpdatePractice($SID, $theCode, $thePracticeOrUnit, $theName,
                        $theAddress1, $theAddress2,
 			$theAddress3, $theAddress4, $thePostCode,
                        $theTelephoneNumber, $theTrustID); break;

  	case 65: I_EditTrustData(); break;
 	case 66: $SID=$_POST['ID'];
		 IA_EditSingleTrust($SID); break;
 	case 67: $SID=$_POST['ID'];
		 $theCode=$_POST['thecode'];
		 $theTrustOrHospital=$_POST['trustorhospital'];
		 $theName=$_POST['thename'];
		 $theAddress1=$_POST['theaddress1'];
		 $theAddress2=$_POST['theaddress2'];
		 $theAddress3=$_POST['theaddress3'];
		 $theAddress4=$_POST['theaddress4'];
		 $thePostCode=$_POST['thepostcode'];
		 $theTelNo=$_POST['thetelno'];
		 IB_UpdateTrust($SID, $theCode,$theTrustOrHospital,
                        $theName, $theAddress1, $theAddress2,
			$theAddress3, $theAddress4,
                        $thePostCode, $theTelNo); break;

 	case 70: J_EditFormData(); break;
 	case 71:  $theID=$_POST['ID'];
		JA_EditSingleForm($theID);
                break;
 	case 72: $SID=$_POST['ID'];
		$theName2=$_POST['theName2'];
		$theName1=$_POST['theName1'];
		$theName=$_POST['theName'];
		$theColour=$_POST['theColour'];
		$theColourName=$_POST['theColourName'];
		$theNameBox=$_POST['theNameBox'];
		$theText=$_POST['theText'];
		$theLetters=$_POST['theLetters'];
		$thegroup=$_POST['group'];
		$theDisplayOrder=$_POST['theDisplayOrder'];
		$theAddict=$_POST['theAddict'];
		$thePrivateCD=$_POST['privatecd'];
		$theSupplementary=$_POST['supplementary'];
		$theHospital=$_POST['hospital'];
		JB_UpdateForm($SID, $theName2, $theName1, $theName, 
                        $theColour, $theColourName, $theNameBox, 
 			$theText, $theLetters, $thegroup,
                        $theDisplayOrder,
                        $theAddict, $thePrivateCD, $theSupplementary,
                        $theHospital);
		break;

       case 75: I_EditHospitalTTO(); break;
 	case 76: $SID=$_POST['ID'];
		 IA_EditSingleHospitalTTO($SID); break;
 	case 77: $SID=$_POST['ID'];
		 $theSpecialInstructions=$_POST['thespecialinstructions'];
		 $the8AM=$_POST['the8am'];
		 $the10AM=$_POST['the10am'];
		 $the1PM=$_POST['the1pm'];
		 $the6PM=$_POST['the6pm'];
		 $the10PM=$_POST['the10pm'];
		 $theObtainMore=$_POST['theobtainmore'];
		 $theStop=$_POST['thestop'];
		 $theIndication=$_POST['theindication'];
               IB_UpdateHospitalTTO($SID, $theSpecialInstructions,
                  $the8AM,$the10AM,$the1PM,$the6PM,$the10PM,
                  $theObtainMore, $theStop,
                  $theIndication); break;


  	case 80: K_EditModeratorData(); break; 
 	case 81:  $theKey=$_POST['thekey'];
		KA_EditSingleModerator($theKey); break;
 	case 82: $firstname=$_POST['thefname'];
		$lastname=$_POST['thelname'];
		$username=$_POST['theusername'];
		$password=$_POST['thepassword'];
		$ID=$_POST['ID'];
		KB_UpdateModerator($firstname, $lastname, $username,
			$password, $ID); break;

       case 85: I_EditPatientTTO(); break;
 	case 86: $SID=$_POST['ID'];
		 IA_EditSinglePatientTTO($SID); break;
 	case 87: $SID=$_POST['ID'];
                $theHospitalNumber=$_POST['thehospitalnumber'];
                $theTelephoneNumber=$_POST['thetelephonenumber'];
                $theDateOfAdmission=$_POST['thedateofadmission'];
                $theWard=$_POST['theward'];
                $theTelExt=$_POST['thetelext'];
                $theDateOfDischarge=$_POST['thedateofdischarge'];
                $theTransferredTo=$_POST['thetransferredto'];
                $theConsultant=$_POST['theconsultant'];
                $theConsultantCode=$_POST['theconsultantcode'];
                $theLetterToGPDr=$_POST['thelettertogpdr'];
                $theDischargeAddressContactNo=$_POST['thedischargeaddresscontactno'];
                $thePrimaryDiagnosis=$_POST['theprimarydiagnosis'];
                $theProblems=$_POST['theproblems'];
                $theConfidential=$_POST['theconfidential'];
                $theNurseInformation=$_POST['thenurseinformation'];
                $theIsNotRequired=$_POST['theisnotrequired'];
                $theWillBePosted=$_POST['thewillbeposted'];
                $theNextDate=$_POST['thenextdate'];
                $theNextTime=$_POST['thenexttime'];
                $theNextClinic=$_POST['thenextclinic'];
                $theTransport=$_POST['thetransport'];
                $theDistrictNursingService=$_POST['thedistrictnursingservice'];
                $theSocialServices=$_POST['thesocialservices'];
                $thePsychiatric=$_POST['thepsychiatric'];
                $theOtherText=$_POST['theothertext'];
                $theSignatureFile=$_POST['thesignaturefile'];
                $theDoctor=$_POST['thedoctor'];
                IB_UpdatePatientTTO($SID,
                    $theHospitalNumber,
                    $theTelephoneNumber,
                    $theDateOfAdmission,
                    $theWard,
                    $theTelExt,
                    $theDateOfDischarge,
                    $theConsultant,
                    $theConsultantCode,
                    $theLetterToGPDr,
                    $theDischargeAddressContactNo,
                    $thePrimaryDiagnosis,
                    $theProblems,
                    $theConfidential,
                    $theNurseInformation,
                    $theIsNotRequired,
                    $theWillBePosted,
                    $theNextDate,
                    $theNextTime,
                    $theNextClinic,
                    $theTransport,
                    $theDistrictNursingService,
                    $theSocialServices,
                    $thePsychiatric,
                    $theOtherText,
                    $theSignatureFile,
                    $theDoctor);
               break;


   	case 90: L_EditScripsData(); break;
 	case 91: $theLetter=$_POST['theLetter'];
		LA_EditSingleScript($theLetter); break;
 	case 92: $theLetter=$_POST['theLetter'];
	    $theFormID=$_POST['theFormID'];
	    $thePatientProblem=$_POST['thePatientProblem'];
	    $thePrescriberProblem=$_POST['thePrescriberProblem'];
	    $theDrug1=$_POST['theDrug1'];
	    $theDrug2=$_POST['theDrug2'];
	    $theMisc=$_POST['theMisc'];
	    $theCounsel=$_POST['theCounsel'];
            $theEntirelyHandWritten=$_POST['entirelyhandwritten'];
            $InteractionMAID=$_POST['interactionmaid'];
	    LB_UpdateScrips($theLetter,$theFormID, $thePatientProblem,
		$thePrescriberProblem, $theDrug1, $theDrug2, $theMisc,
                $theCounsel, $theEntirelyHandWritten,
                $InteractionMAID);
            break;



        // Produce a list of number in ScripsScrips (e.g. patient number) ...
        case 97:
        ZW_ScripsscripsListAsk(); 
        break;

        case 98: 
        $theLetter=$_POST['ID'];
        ZW_ScripsscripsListProduce($theLetter); // ... argument is either one code e.g. CB3 or nothing (meaning all)
        break;


         // The TERM PATTERNS:
         case 100:
         $yearGroup = $_POST['yeargroup'];
         $location = $_POST['location'];
         $newlocation = $_POST['newlocation'];

         // If $location hasn't been set, then take contents of $newlocation ...
         if (!$location) { $location = $newlocation; };
         M_EditTermData($yearGroup, $location);
         break;

         case 101:
         $SID=$_POST['ID'];
         $yearGroup = $_POST['yeargroup'];
         $location = $_POST['location'];
         MA_EditSingleTermPattern($SID, $yearGroup, $location);
         break;

         case 102:
         $SID=$_POST['ID'];
         $yearGroup = $_POST['yeargroup'];
         $location = $_POST['location'];
         $theWk1=$_POST['thewk1'];
         $theWk2=$_POST['thewk2'];
         $theWk3=$_POST['thewk3'];
         $theWk4=$_POST['thewk4'];
         $theWk5=$_POST['thewk5'];
         $theWk6=$_POST['thewk6'];
         $theWk7=$_POST['thewk7'];
         MB_UpdateTermPattern($SID, $yearGroup, $location, $theWk1, $theWk2, $theWk3, $theWk4, $theWk5, $theWk6, $theWk7);
         break;

         case 103:
         M_EditTermDataYearGroup();
         break;


         // Edit CHECKING PATTERNS:
         case 105:
         $yearGroup = $_POST['yeargroup'];
         M_EditCheckingData($yearGroup);
         break;

         case 106:
         $SID=$_POST['ID'];
         $yearGroup = $_POST['yeargroup'];
         MA_EditSingleCheckingPattern($SID, $yearGroup);
         break;

         case 107:
         $SID=$_POST['ID'];
         $yearGroup = $_POST['yeargroup'];
         $thePracticeCode=$_POST['thepracticecode'];
         $theWeek=$_POST['theweek'];
         $theSetName=$_POST['thesetname'];
         $theOrder=$_POST['theorder'];
         MB_UpdateCheckingPattern($SID, $yearGroup, $thePracticeCode, $theWeek, $theSetName, $theOrder);
         break;

         case 108:
         M_EditCheckingDataChooseGroup();
         break;


        // Edit EXAM PATTERNS:
        case 110:
        $yearGroup = $_POST['yeargroup'];
        $location = $_POST['location'];
        $newlocation = $_POST['newlocation'];
        // If $location hasn't been set, then take contents of $newlocation ...
        if (!$location) { $location = $newlocation; };
        N_EditExamData($yearGroup, $location);
        break;

        case 111:
        $SID=$_POST['ID'];
        $yearGroup = $_POST['yeargroup'];
        $location = $_POST['location'];
        NA_EditSingleExamPattern($SID, $yearGroup, $location);
        break;

        case 112:
        $SID=$_POST['ID'];
        $yearGroup = $_POST['yeargroup'];
        $location = $_POST['location'];
        $theWk1=$_POST['thewk1'];
        $theWk2=$_POST['thewk2'];
        $theWk3=$_POST['thewk3'];
        $theWk4=$_POST['thewk4'];
        NB_UpdateExamPattern($SID, $yearGroup, $location, $theWk1, $theWk2, $theWk3, $theWk4);
        break;

        case 113:
        N_EditExamDataChooseGroup();
        break;

        // Copy one line of ScripsScrips and give it another name - also copy of the model answers and of everything in ScripsScrips (e.g. patientproblem):
        // Get the scrip to be copied e.g. CB3 ...
        case 115:
        ZV_ScripsscripsListAsk(); 
        break;

        // "You have asked to copy <scrip> - what do you want to call the new version?"
        case 116:
        $theLetter=$_POST['ID'];
        $newLetter="";
        ZV_ScripsscripsAskNewName($theLetter, $newLetter); // ... second argument will be null from here (but not from elsewhere) ...
        break;

        // Check new letter doesn't already exist. If it doesn't, create new entries (and model answers); if it does call ZV_ScripsscripsAskNewName again with new argument ...
        case 117:
        // The check is relatively trivial (check ScripsScrips for the existence of $newLetter) - do it here rather than in (yet) another function ...
        $theLetter=$_POST['ID'];
        $newLetter=strtoupper($_POST['newletter']);

        $db=new dbabstraction();
        $db->connect() or die ($db->getError()); 
        $condition = array('Letter'=>$newLetter);
        $table = $db->select("ScripsScrips", $condition);

        // Should have no rows or one row (and no more) ... 
        if (count($table)) {
            ZV_ScripsscripsAskNewName($theLetter, $newLetter); // ... one row - letter exists - ask for a new one
        } else {
            ZV_ScripsscripsCreateNewName($theLetter, $newLetter); // ... letter is new - write it
        };
        break;

        // 
        case 120: O_EditConfigurationData(); break;
        case 121:$thePracticeStart=$_POST['thepracticestartdate'];
		$theExamStart1=$_POST['theexamstartdate1'];
		$theExamStart2=$_POST['theexamstartdate2'];
		$theExamStart3=$_POST['theexamstartdate3'];
		$theresitdate=$_POST['theresitdate'];
		$theByes=$_POST['thebyes'];
		$theIPAddressLabs=$_POST['theipaddresslabs'];
		$theCheckingStartDate=$_POST['thecheckingstartdate'];
		$theDelayMinutes=$_POST['thedelayminutes'];
		$theDelaySeconds=$_POST['thedelayseconds'];
		OA_UpdateConfiguration($thePracticeStart, $theExamStart1,
			$theExamStart2, $theExamStart3, $theresitdate,
			$theByes, $theIPAddressLabs, $theCheckingStartDate,
                        $theDelayMinutes, $theDelaySeconds);
                break;

 	case 130: P_EditOTCData(); break;
 	case 131:  $SID=$_POST['ID'];
		PA_EditSingleOTC($SID); break;
 	case 132: $SID=$_GET['ID'];
		$theOTCText=$_GET['theotctext'];
		PB_UpdateOTC($SID, $theOTCText); break;

 	case 140: Q_EditLegalData(); break;
 	case 141:  $SID=$_POST['ID'];
		QA_EditSingleLegal($SID); break;
 	case 142: $SID=$_POST['ID'];
		$theLegalText=$_POST['thelegaltext'];
		$theDisplayOrder=$_POST['theDisplayOrder'];
		$theTwentyEightDays=$_POST['twentyeightdays'];
		QB_UpdateLegal($SID, $theLegalText, $theDisplayOrder,
                               $theTwentyEightDays);
                break;

  	case 150: R_EditDrugProblemData(); break;
 	case 151:  $SID=$_POST['ID'];
		RA_EditSingleDrug($SID); break;
 	case 152: $SID=$_POST['ID'];
	$theDrugName=$_POST['theDrugName'];
	$theDrugMispelling=$_POST['theDrugMispelling'];
	$theDrugNameMissing=$_POST['theDrugNameMissing'];
	$theDrugNameMispelled=$_POST['theDrugNameMispelled'];
	$theDrugInappropriateChoice=$_POST['theDrugInappropriateChoice'];
	$theDrugInappropriateChoiceComment=$_POST['theDrugInappropriateChoiceComment'];
	$theDrugNonExist=$_POST['theDrugNonExist'];
	$theStrength=$_POST['theStrength'];
	$theStrengthCorrect=$_POST['theStrengthCorrect'];
	$theStrengthMissing=$_POST['theStrengthMissing'];
	$theStrengthUnavailable=$_POST['theStrengthUnavailable'];
	$theDeliveryForm=$_POST['theDeliveryForm'];
	$theFormCorrect=$_POST['theFormCorrect'];
	$theDeliveryFormMissing=$_POST['theDeliveryFormMissing'];
	$theDeliveryFormUnavailable=$_POST['theDeliveryFormUnavailable'];
	$theDeliveryFormInappropriate=$_POST['theDeliveryFormInappropriate'];
	$theDeliveryFormNotAllowed=$_POST['theDeliveryFormNotAllowed'];
	$theDose=$_POST['theDose'];
	$theDoseCorrect=$_POST['theDoseCorrect'];
	$theDoseOverdose=$_POST['theDoseOverdose'];
	$theDoseUnderdose=$_POST['theDoseUnderdose'];
	$theDoseIncomplete=$_POST['theDoseIncomplete'];
	$theDoseNotRequired=$_POST['theDoseNotRequired'];
$theDoseInappropriateTiming=$_POST['theDoseInappropriateTiming'];
	$theQuantity=$_POST['theQuantity']; 
	$theQuantityCorrect=$_POST['theQuantityCorrect']; 
	$theQuantityMissing=$_POST['theQuantityMissing']; 
	$theQuantityExcessivePO=$_POST['theQuantityExcessivePO']; 
$theQuantityExcessiveLegal=$_POST['theQuantityExcessiveLegal']; 
	$theQuantityInsufficient=$_POST['theQuantityInsufficient']; 
	$theIntervalreq=$_POST['theIntervalreq']; 
	$theQuantityNoInterval=$_POST['theQuantityNoInterval']; 
	$theQuantityNoWords=$_POST['theQuantityNoWords']; 
	$theQuantityNoFigures=$_POST['theQuantityNoFigures']; 
	$thePurpose=$_POST['thePurpose'];  
	$thePurposeMarks=$_POST['thePurposeMarks'];  
	$theBlacklisted=$_POST['theBlacklisted'];
	$theNotFormulary=$_POST['theNotFormulary'];  
	$theWrongScrip=$_POST['theWrongScrip'];  
	$theWrongPractioner=$_POST['theWrongPractioner'];  
	$theNosDays=$_POST['theNosDays'];  
	$theHospitalTTODetails=$_POST['theHospitalTTODetails'];  
	$theDrugHandwritten=$_POST['theDrugHandwritten'];  
	$theDrugHandCorrect=$_POST['theDrugHandCorrect'];  
	$theStrengthPMR=$_POST['strengthpmr'];  
	$theFormPMR=$_POST['formpmr'];  
	$theDosePMR=$_POST['dosepmr'];  
	$thePMRComment=$_POST['thePMRComment'];  
	RB_UpdateDrugProblem($SID, $theDrugName, $theDrugMispelling,  	    
            $theDrugNameMissing,$theDrugNameMispelled, $theDrugInappropriateChoice,
            $theDrugInappropriateChoiceComment,
            $theDrugNonExist,$theStrength, $theStrengthCorrect,
            $theStrengthMissing, 		     
            $theStrengthUnavailable,$theDeliveryForm, $theFormCorrect,  
            $theDeliveryFormMissing,$theDeliveryFormUnavailable, 		     
            $theDeliveryFormInappropriate,$theDeliveryFormNotAllowed,   
	$theDose,$theDoseOverdose,$theDoseIncomplete, 		     
	$theDoseNotRequired,$theDoseInappropriate, 		     
	$theQuantity,$theQuantityMissing,$theQuantityExcessivePO,   
	$theQuantityInsufficient,$theQuantityNoInterval, $theQuantityNoWords,  
	$thePurpose, $theBlacklisted, $theNotFormulary, $theWrongScrip, 
	$theWrongPractioner, $theDoseUnderdose, $theQuantityExcessiveLegal,
	$theQuantityNoFigures, $thePurposeMarks, $theDoseCorrect, 
            $theNosDays, $theHospitalTTODetails, $theDrugHandwritten, $theDrugHandCorrect,
	$theDoseInappropriateTiming, $theQuantityCorrect, $theIntervalreq,
            $theStrengthPMR, $theFormPMR, $theDosePMR, $thePMRComment);
        break;

   	case 160: S_EditPatientProblemData();break;
  	case 161:  $SID=$_POST['ID'];
		SA_EditSinglePatientProblem($SID); break;
 	case 162: $SID=$_POST['ID'];
		$theFName=$_POST['theFName'];
		$theSName=$_POST['theSName'];
		$theAddress=$_POST['theAddress'];
		$theAddressIncom=$_POST['theAddressIncom'];
		$theNandA=$_POST['theNandA'];
		$theAge=$_POST['theAge'];
		$theDOB=$_POST['theDOB'];
		$theAgeDOB=$_POST['theAgeDOB'];
		$thePatientID=$_POST['thePatientID'];
		$theSText=$_POST['theSText'];
		SB_UpdatePatientProblem($SID, $thePatientID, $theFName,
                       $theSName, $theAddress, $theAddressIncom, $theNandA,
                       $theAge, $theDOB, $theAgeDOB, $theSText);
                break;   
	
        case 170: T_EditPrescriberProblemData(); break;
 	case 171:  $SID=$_POST['ID'];
		TA_EditSinglePrescriberProblem($SID); break;
 	case 172: $SID=$_POST['ID'];
		$thePrescriber=$_POST['prescriber'];
		$theName=$_POST['theName'];
		$theNHSNo=$_POST['theNHSNo'];
		$theAddress=$_POST['theAddress'];
		$theAddressIncomplete=$_POST['theAddressIncomplete'];
		$theAddressNonUK=$_POST['theAddressNonUK'];
		$theUKAddress1=$_POST['theUKAddress1'];
		$theUKAddress2=$_POST['theUKAddress2'];
		$theUKAddress3=$_POST['theUKAddress3'];
		$theUKAddress4=$_POST['theUKAddress4'];
		$theQuals=$_POST['theQuals'];
		$theQualsInappropriate=$_POST['theQualsInappropriate'];
		$theSig=$_POST['theSig'];
		$theSigIncorrect=$_POST['theSigIncorrect'];
		$theAuthorise=$_POST['theAuthorise'];
		$theAuthorisingID=$_POST['authoriser'];
		$$theIncorrectSigFile=$_POST['$theIncorrectSigFile'];
		TB_UpdatePrescriberProblem($SID, $thePrescriber, $theName,
			$theNHSNo, $theAddress, $theAddressIncomplete,
			$theAddressNonUK, $theUKAddress1, 
                        $theUKAddress2, $theUKAddress3, $theUKAddress4,
                        $theQuals, $theQualsInappropriate,
			$theSig, $theSigIncorrect, $theAuthorise,
                        $theAuthorisingID, $theIncorrectSigFile);
                break;

 	case 180: U_EditMiscProblemData(); break;
 	case 181:  $SID=$_POST['ID'];
		UA_EditSingleMiscProblem($SID); break;
 	case 182: $SID=$_POST['ID'];
		$DateMissing=$_POST['DateMissing'];
		$DateFuture=$_POST['DateFuture'];
		$DateLate=$_POST['DateLate'];
              $thePurposeCategory = $_POST['PurposeCategory'];
//              $thePurpose = $_POST['Purpose'];
              $thePurposeMarks = $_POST['PurposeMarks'];
		$PatientHandwritten=$_POST['PatientHandwritten'];
		$Interaction=$_POST['Interaction'];
		$Intaction=$_POST['Intaction'];
		$IntFB=$_POST['IntFB'];
		$FileDestination=$_POST['FileDestination'];
		$filedestinationloss=$_POST['filedestinationloss'];

              if ($thePurposeCategory) { $thePurpose = $_POST['thePurposeCD']; };
              if (!$thePurposeCategory) { $thePurpose = $_POST['thePurposeNonCD']; };

		UB_UpdateMiscProblem($SID, $DateMissing, $DateFuture, $DateLate,
                     $thePurposeCategory, $thePurpose, $thePurposeMarks, 
                     $PatientHandwritten, $Interaction,
                     $FileDestination, $filedestinationloss, $Intaction,
                     $IntFB);
 		break;

 	case 190: V_EditDoseCodeTable(); break;
	case 191: $SID=$_POST['ID']; VA_EditSingleDoseCode($SID);
		break;
	case 192:$SID=$_POST['ID'];
      		$code=$_POST['code'];
		$text=$_POST['text'];
 		VB_UpdateDoseCodeTable($SID, $code, $text); break;
 	case 200: V_ViewStudentPassword(); break;
 	case 201: $user=$_POST['susername'];
		VA_ViewPassword($user); break;
 	case 210: W_RemoveStudents(); break;
 	case 211: $confirm=$_POST['confirm'];
		WA_ActuallyRemoveStudents($confirm); break;

        case 220:
        X_AddStudent(); break;

        case 221:
        $thesfname=$_POST['sfname'];
        $theslname=$_POST['slname'];
        $thesuname=$_POST['suname'];
        $thespassword=$_POST['spassword'];
        $thespractice=$_POST['spractice'];
        $thesexam=$_POST['sexam'];
        XA_InsertStudent($thesfname, $theslname, $thesuname, $thespassword, $thespractice, $thesexam);
        break;

        case 227:
        XA_DeleteRealStudents();
        break;

 	case 230: Y_AddPatient(); break;
 	case 231:$thefname=$_POST['fname'];
		$thelname=$_POST['lname'];
		$theage=$_POST['age'];
		$theaddress1=$_POST['address1'];
		$theaddress2=$_POST['address2'];
		$theaddress3=$_POST['address3'];
		$theaddress4=$_POST['address4'];
		$themedication=$_POST['medication'];
		YA_InsertPatient($thefname, $thelname,$theage, $theaddress1,
			$theaddress2, $theaddress3, $theaddress4,
			$themedication); break;
 	case 240: Z_AddPrescriber(); break;
 	case 241:$thename=$_POST['name'];
		$thequal=$_POST['qual'];
		$thepracticeID=$_POST['practiceID'];
		$thenhsno=$_POST['nhsno'];
		$thewrite=$_POST['write'];
		$thesigfile=$_POST['sigfile'];
		ZA_InsertPrescriber($thename, $thequal,
			$thepracticeID, $thenhsno, $thewrite, $thesigfile);
       		break;
 	case 250: A1_AddPractice(); break;
 	case 251:$address1=$_POST['address1'];
		$address2=$_POST['address2'];
		$address3=$_POST['address3'];
		$address4=$_POST['address4'];
		A1A_InsertPractice($address1, $address2, $address3,
			$address4); break;
 	case 260: B1_AddOTC(); break;
 	case 261:$theOTCText=$_POST['otctext'];
		B1A_InsertOTC($theOTCText);  break;
 	case 270: C1_AddScrip(); break;
 	case 271: $theLetter=$_POST['theLetter'];
		$theFormID=$_POST['theFormID'];
		$thePatient=$_POST['thePatient'];
		$thePrescriber=$_POST['thePrescriber'];
		$theDrug1=$_POST['theDrug1'];
		$theDrug2=$_POST['theDrug2'];
		$theMisc=$_POST['theMisc'];
		$theCounsel=$_POST['counsel'];
		C1A_InsertScrip($theLetter,$theFormID, $thePatient,
			$thePrescriber, $theDrug1, $theDrug2,
			$theMisc,$theCounsel); break;
  	case 280: D1_AddDrugProblem(); break;
 	case 281: $theDrugName=$_POST['theDrugName'];
	$theDrugMispelling=$_POST['theDrugMispelling'];
	$theDrugNameMissing=$_POST['theDrugNameMissing'];
	$theDrugNameMispelled=$_POST['theDrugNameMispelled'];
	$theDrugInappropriateChoice=$_POST['theDrugInappropriateChoice'];
	$theDrugNonExist=$_POST['theDrugNonExist'];
	$theStrength=$_POST['theStrength'];
	$theStrengthCorrect=$_POST['theStrengthCorrect'];
	$theStrengthMissing=$_POST['theStrengthMissing'];
	$theStrengthUnavailable=$_POST['theStrengthUnavailable'];
	$theDeliveryForm=$_POST['theDeliveryForm'];
	$theFormCorrect=$_POST['theFormCorrect'];
	$theDeliveryFormMissing=$_POST['theDeliveryFormMissing'];
	$theDeliveryFormUnavailable=$_POST['theDeliveryFormUnavailable'];
	$theDeliveryFormInappropriate=$_POST['theDeliveryFormInappropriate'];
	$theDeliveryFormNotAllowed=$_POST['theDeliveryFormNotAllowed'];
	$theDose=$_POST['theDose'];
	$theDoseCorrect=$_POST['theDoseCorrect'];
	$theDoseOverdose=$_POST['theDoseOverdose'];
	$theDoseUnderdose=$_POST['theDoseUnderdose'];
	$theDoseIncomplete=$_POST['theDoseIncomplete'];
	$theDoseNotRequired=$_POST['theDoseNotRequired'];
$theDoseInappropriateTiming=$_POST['theDoseInappropriateTiming'];
	$theQuantity=$_POST['theQuantity'];
	$theQuantityCorrect=$_POST['theQuantityCorrect']; 
	$theQuantityMissing=$_POST['theQuantityMissing'];
	$theQuantityExcessivePO=$_POST['theQuantityExcessivePO']; 
$theQuantityExcessiveLegal=$_POST['theQuantityExcessiveLegal']; 
	$theQuantityInsufficient=$_POST['theQuantityInsufficient'];
	$theIntervalreq=$_POST['Intervalreq'];
	$theQuantityNoInterval=$_POST['theQuantityNoInterval'];
	$theQuantityNoWords=$_POST['theQuantityNoWords'];
	$theQuantityNoFigures=$_POST['theQuantityNoFigures']; 
	$thePurpose=$_POST['thePurpose']; 
	$thePurposeMarks=$_POST['thePurposeMarks'];  
	$theBlacklisted=$_POST['theBlacklisted']; 
	$theNotFormulary=$_POST['theNotFormulary']; 
	$theWrongScrip=$_POST['theWrongScrip']; 
	$theWrongPractioner=$_POST['theWrongPractioner']; 
	$theNosDays=$_POST['theNosDays'];  
	$theDrugHandwritten=$_POST['theDrugHandwritten'];  
	$theDrugHandCorrect=$_POST['theDrugHandCorrect'];  
	D1A_InsertDrugProblem($theDrugName, $theDrugMispelling,  	    
	$theDrugNameMissing,$theDrugNameMispelled, $theDrugInappropriateChoice,
	$theDrugNonExist,$theStrength, $theStrengthCorrect,
	$theStrengthMissing, 		     
	$theStrengthUnavailable,$theDeliveryForm, $theFormCorrect,  
	$theDeliveryFormMissing,$theDeliveryFormUnavailable, 		     
	$theDeliveryFormInappropriate,$theDeliveryFormNotAllowed,   
	$theDose,$theDoseOverdose,$theDoseIncomplete, 		     
	$theDoseNotRequired,$theDoseInappropriate, 		     
	$theQuantity,$theQuantityMissing,$theQuantityExcessivePO,   
	$theQuantityInsufficient,$theQuantityNoInterval, $theQuantityNoWords,  
	$thePurpose, $theBlacklisted, $theNotFormulary, $theWrongScrip, 
	$theWrongPractioner, $theDoseUnderdose, $theQuantityExcessiveLegal,
	$theQuantityNoFigures, $thePurposeMarks, $theDoseCorrect, 
	$theNosDays, $theDrugHandwritten, $theDrugHandCorrect,
	$theDoseInappropriateTiming, $theQuantityCorrect,$theIntervalreq ); break;
 	case 290: E1_AddPatientProblem(); break;
 	case 291: $$thePatient=$_POST['thePatient'];
		$theFName=$_POST['theFName'];
		$theSName=$_POST['theSName'];
		$theAddress=$_POST['theAddress'];
		$theAge=$_POST['theAge'];
		$theDOB=$_POST['theDOB'];
		$theAgeDOB=$_POST['theAgeDOB'];
		$theSText=$_POST['theSText'];
		E1A_AddPatientProblem($thePatient,
			$theFName, $theSName, $theAddress, $theAge, $theDOB,
			$theAgeDOB,$theSText); break;
 	case 300: F1_AddPrescriberProblem(); break;
 	case 301: $thePrescriber=$_POST['thePrescriber'];
		$theName=$_POST['theName'];
		$theNHSNo=$_POST['theNHSNo'];
		$theAddress=$_POST['theAddress'];
		$theAddressIncomplete=$_POST['theAddressIncomplete'];
		$theAddressNonUK=$_POST['theAddressNonUK'];
		$theQuals=$_POST['theQuals'];
		$theQualsInappropriate=$_POST['theQualsInappropriate'];
		$theSig=$_POST['theSig'];
		$theSigIncorrect=$_POST['theSigIncorrect'];
		$theAuthorise=$_POST['theAuthorise'];
		F1A_AddPrescriberProblem($thePrescriber, $theName,
			$theNHSNo, $theAddress, $theAddressIncomplete,
			$theAddressNonUK, $theQuals, $theQualsInappropriate,
			$theSig, $theSigIncorrect, $theAuthorise); break;
 	case 310: G1_AddMiscProblem(); break;
 	case 311: $DateMissing=$_POST['DateMissing'];
		$DateFuture=$_POST['DateFuture'];
		$DateLate=$_POST['DateLate'];
		$PatientHandwritten=$_POST['PatientHandwritten'];
		$Interaction=$_POST['Interaction'];
		$Intaction=$_POST['Intaction'];
		$IntFB=$_POST['IntFB'];
		$FileDestination=$_POST['FileDestination'];
	    G1A_InsertMiscProblem($DateMissing,
 		$DateFuture, $DateLate, $PatientHandwritten,
		$Interaction, $FileDestination, $Intaction, $IntFB);
		 break;


 	case 320: H1_AddTermPattern(); break;
 	case 321:$wk1=$_POST['wk1'];
		$wk2=$_POST['wk2'];
		$wk3=$_POST['wk3'];
		$wk4=$_POST['wk4'];
		$wk5=$_POST['wk5'];
		$wk6=$_POST['wk6'];
		$wk7=$_POST['wk7'];
		H1A_InsertTermPattern($wk1, $wk2, $wk3, $wk4,
			$wk5, $wk6, $wk7); break;


 	case 330: I1_AddExamPattern(); break;
 	case 331:$wk1=$_POST['wk1'];
		$wk2=$_POST['wk2'];
		$wk3=$_POST['wk3'];
		$wk4=$_POST['wk4'];
		I1A_InsertExamPattern($wk1, $wk2, $wk3, $wk4);
      		break;
 	case 340: J1_AddLegalText(); break;
 	case 341:$theLegalText=$_POST['legaltext'];
		J1A_InsertLegalText($theLegalText); break;
	case 350: K1_AddHelpText(); break;
 	case 351:$theHelpText=$_POST['helptext'];
		$theSituation=$_POST['situation'];
		$theTabNumber=$_POST['thetab'];
		K1A_InsertHelpText($theHelpText, $theSituation,$theTabNumber);
         		break;
	case 360: L1_EditHelpText(); break;
 	case 361:  $SID=$_POST['ID'];
		L1A_EditSingleHelp($SID); break;
 	case 362: $SID=$_POST['ID'];
		$theHelpText=$_POST['theHelpText'];
		$theSituation=$_POST['theSituation'];
		L1B_UpdateHelpText($SID, $theHelpText,
			$theSituation); break;
	case 370: V_AddDoseCode(); break;
 	case 371:  $code=$_POST['Code'];
		$text=$_POST['Text'];
		VA_InsertDoseCode($code, $text); break;

        // EDIT CLASSIFICATIONS:
        case 380:
        M1_EditClassifyMA();
        break;

        case 381:
        $code=$_POST['lettercode'];
        M1A_EditSingleClassify($code);
        break;

        case 382: 
/*
        if ($_POST['lettercode']){ $code=$_POST['lettercode']; };
        if ($_POST['formid']){ $formID=$_POST['formid']; };
        if ($_POST['thethera1']){ $thera1=$_POST['thethera1']; };
        if ($_POST['thethera2']){ $thera2=$_POST['thethera2']; };
        if ($_POST['legal1']){ $legal1=$_POST['legal1']; };
        if ($_POST['legal2']){ $legal2=$_POST['legal2']; };
        if ($_POST['formtype']){ $formtype=$_POST['formtype']; };
        if ($_POST['originator']){ $originator=$_POST['originator']; };
*/


        $code=$_POST['lettercode'];
        $formID=$_POST['formid'];
        $thera1=$_POST['thethera1'];
        $thera2=$_POST['thethera2'];
        $legal1=$_POST['legal1'];
        $legal2=$_POST['legal2'];
        $typeofform=$_POST['typeofform'];
        $originator=$_POST['originator'];

        if ($_POST['Update']){

            // New data posted: look at the originator and typeofform and find the matching serial number (field FormType) from table ScripForms
            $db=new dbabstraction();
            $db->connect() or die ($db->getError()); 
            $condition = array('FormName1'=>$originator, 'FormName2'=>$typeofform);
            $table = $db->select("ScripForms", $condition);
            foreach ($table as $row) {
                // Find the serial number ...
                $serialNumber = $row['FormID'];
            };

            M1B_UpdateClassifyMA($code, $formID, $thera1, $thera2, $legal1, $legal2, $serialNumber); 
        } else {
            M1A_EditSingleClassify($code);
        };
        break;

        // EDIT LABELS CODES:
	case 390: N1_EditLabelCode(); break;
	case 391: $labelID=$_POST['labelID'];
		N1A_EditSingleLabelCode($labelID); break;
	case 392: $labelID=$_POST['labelID'];
		$thetext=$_POST['thetext'];
		$thecode=$_POST['thecode'];
	N1B_UpdateLabelCode($labelID, $thecode, $thetext); break;
	case 400: O1_AddLabelCode(); break;
	case 401: $thecode=$_POST['thecode'];
		$thetext=$_POST['thetext'];
		O1A_InsertLabelCode($thecode,$thetext); break;
	case 410: P1_AddForm(); break;
 	case 411: $theName=$_POST['theName'];
		$theColour=$_POST['theColour'];
		$theColourName=$_POST['theColourName'];
		$theText=$_POST['theText'];
		$theNameBox=$_POST['theNameBox'];
		$theAddict=$_POST['theAddict'];
		$group=$_POST['group'];
		P1A_InsertForm($theName, $theColour, $theColourName,
 		   $theText, $theNameBox, $theAddict, $group);
	 break;

	case 420: Q1_EditLabelMA(); break;
	case 421: $labelID=$_POST['MAID'];
		Q1A_EditSingleLabelMA($labelID); break;
 	case 422:  $labelID=$_POST['MAID'];
		$drugid=$_POST['drugid'];
		$theAncil=$_POST['theAncil'];
		$theDose=$_POST['theDose'];
		$therefreq=$_POST['therefreq'];
		$labelQuantity=$_POST['labelQuantity'];
		$labelNSF=$_POST['labelNSF'];
		$theDrugNumber=$_POST['theDrugNumber'];
                if ($_POST['Update']) {
                    Q1B_UpdateLabelCodeMA($labelID,$drugid, $theAncil,$theDose,
                          $therefreq, $labelQuantity, $labelNSF,
                          $theDrugNumber);
                } else {
                    Q1A_EditSingleLabelMA($labelID);
                };
		break;

	case 425: R1ZA_Display_Label_Table(); break;
	case 426: R1ZB_Display_Dose_Table(); break;
	case 430: R1_AddLabelMA(); break;
	case 431: $theAncil=$_POST['theAncil'];
		$theDose=$_POST['theDose'];
		$drugid=$_POST['drugid'];
		$therefreq=$_POST['therefreq'];
	R1A_InsertLabelMA($drugid, $theAncil,$theDose,$therefreq); break;

// No longer in use? Superseded by 445, 446 and 447?
	case 440: S1_EditPORMA(); break;
	case 441: $theCaseID=$_POST['theCaseID'];
		S1A_EditSinglePORMA($theCaseID); break;
	case 442: $col2=$_POST['col2'];
		$col3=$_POST['col3'];
		$col4a=$_POST['col4a'];
		$col4b=$_POST['col4b'];
		$col5a=$_POST['col5a'];
		$col5b=$_POST['col5b'];
		$col6=$_POST['col6'];
		$thecase=$_POST['case'];
		S1B_UpdatePORMA($col2,$col3,$col4a,$col4b, $col5a, $col5b,
			$col6, $thecase); break;

	case 445: $theCaseID=$_POST['theCaseID'];
		S1A_NewPORDetails($theCaseID); break;
	case 446: $col2=$_POST['col2'];
		$col3=$_POST['col3'];
                $col4afull=$_POST['col4afull'];
		$col4a1=$_POST['col4a1'];
		$col4a2=$_POST['col4a2'];
                if ($col4a2 == "0") {
                    $col4a = $col4a1;
                    if (!$col4afull) { $col4afull = $col4a; }; 
                } else {
                    $col4a = $col4a2;
                    $col4afull = ""; 
                };
		$col4b=$_POST['col4b'];
		$col5a=$_POST['col5a'];
		$col5b=$_POST['col5b'];
		$col6=$_POST['col6'];
		$thecase=$_POST['case'];
		S1B_ImplementNewPOR($col2,$col3,$col4a,$col4afull,
                        $col4b, $col5a, $col5b,
			$col6, $thecase); break;

//	case 450: T1_AddPORMA(); break;
	case 451: $col2=$_POST['col2'];
		$col3=$_POST['col3'];
		$col4a1=$_POST['col4a1'];
		$col4a2=$_POST['col4a2'];
                if ($col4a2 == "0") {
                $col4a = $col4a1; 
                } else {
                $col4a = $col4a2; 
                }
		$col4b=$_POST['col4b'];
		$col5a=$_POST['col5a'];
		$col5b=$_POST['col5b'];
		$col6=$_POST['col6'];
		$thecase=$_POST['case'];
		T1A_InsertPORMA($col2,$col3,$col4a,$col4b, $col5a, $col5b, $col6, $thecase); break;

	case 460: U1_EditIP(); break;
	case 461: $IPID=$_POST['IPID'];
		U1A_EditSingleIP($IPID); break;
	case 462: $IPID=$_POST['IPID'];
		$location=$_POST['location']; 
		$ipaddress=$_POST['ipaddress'];
		$checking=$_POST['checking'];
		U1B_UpdateIP($IPID,$location, $ipaddress, $checking);
                break;


        // Define the IP location:
        case 465:
        UUU_EditIPLocation();
        break;

        case 466:
        $SID = $_POST['ID'];
        UUU_EditSingleIPLocation($SID);
        break;

        case 467:
        $SID = $_POST['ID'];
        $theIPBeginning = $_POST['ipbeginning'];
        $theLocation = $_POST['location'];
        UUU_UpdatePrescriber($SID, $theIPBeginning, $theLocation);
        break;







	case 470: V1_AddIP(); break;
	case 471: $location=$_POST['location'];
		$ipaddress=$_POST['ipaddress'];
		V1A_InsertIP($location, $ipaddress); break;
	case 480: W1_SelectCDToUpdate(); break;
        case 481: $theCaseID=$_POST['theCaseID'];
                  W1A_NewCDDetails($theCaseID); break;
        case 482: $drug=$_POST['drug'];
                  $particulars=$_POST['particulars'];
                  $amount=$_POST['amount'];
                  $form=$_POST['form'];
                  $theCaseID=$_POST['case'];
                  $reverseitem=$_POST['reverseitem'];
                  $reverseqty=$_POST['reverseqty'];
                  W1B_ImplementNewCD($theCaseID, $drug,
                    $particulars, $amount, $form, $reverseitem,
                    $reverseqty);
                  break;

        // Mass release:
        case 490:
        X1_ReleaseGet();
        break;

        case 492:
        // If the "Display Data" button has been pressed, pass year and location back to the function so that the data can be displayed, otherwise
        // Implement has been pressed and therefore implement the change required ...
        if ($_POST['displaydata']){
            $_SESSION['yearGroup'] = $_POST['yeargroup'];
            $_SESSION['location'] = $_POST['location'];
            X1_ReleaseGet();
        } else if (implementDispensing) {
            // If Implement pressed in dispensing section ...
            $makeavailable=$_POST['makeavailable'];
            if ($_POST['makeavailable'] == "on") { $makeavailable='1'; } else { $makeavailable='0'; };
            X1A_ReleaseImplement($_POST['RR'], $_POST['week'], $makeavailable); // ... variables are results to be released (1 or 0), week and whether to make week's scripts available in U mode 
        } else if (implementChecking) {
            // If Implement pressed in checking section ...
            X1C_ReleaseCheckingImplement($_POST['CR'], $_POST['weekchecking']);
        };
        break;

        // 493 now believed to be redundant
        case 493: $weekChecking=$_POST['weekchecking'];
                  $CR=$_POST['CR'];
                  X1C_ReleaseCheckingImplement($CR, $weekChecking);
                  break;

        // Individual release:
        case 495:
        X1B_ManualReleaseGet();
        break;

        case 497:
        $RR=$_POST['RR'];
        $WL=$_POST['WL'];
        $week=$_POST['week'];
        $letter=$_POST['letter'];
        $student=$_POST['student'];
        X1C_ManualReleaseImplement($RR, $WL, $week, $letter, $student);
        break; 	

        case 500: $index=$_GET['index'];
		  Z_Help($index); break;

  	case 510: W5_EditCDRegClass(); break;
 	case 511: $ID=$_POST['ID'];
		  W5_EditSingleCDRegClass($ID); break;
 	case 512: $ID=$_POST['ID'];
		  $theCDRegClass=$_POST['theCDRegClass'];
		  W5_UpdateCDRegClass($ID, $theCDRegClass);
                  break;

  	case 520: W6_EditCDRegBrand(); break;
 	case 521: $ID=$_POST['ID'];
		  W6_EditSingleCDRegBrand($ID); break;
 	case 522: $ID=$_POST['ID'];
		  $theCDRegBrand=$_POST['theCDRegBrand'];
		  W6_UpdateCDRegBrand($ID, $theCDRegBrand);
                  break;

  	case 530: W7_EditCDRegStrength(); break;
 	case 531: $ID=$_POST['ID'];
		  W7_EditSingleCDRegStrength($ID); break;
 	case 532: $ID=$_POST['ID'];
		  $theCDRegStrength=$_POST['theCDRegStrength'];
		  W7_UpdateCDRegStrength($ID, $theCDRegStrength);
                  break;

  	case 540: W8_EditCDRegForm(); break;
 	case 541: $ID=$_POST['ID'];
		  W8_EditSingleCDRegForm($ID); break;
 	case 542: $ID=$_POST['ID'];
		  $theCDRegForm=$_POST['theCDRegForm'];
		  W8_UpdateCDRegForm($ID, $theCDRegForm);
                  break;

  	case 550: W9_EditCDPageTitleAndLabel(); break;
 	case 551: $ID=$_POST['ID'];
		  W9_EditSingleCDPageTitleAndLabel($ID); break;
 	case 552: $ID=$_POST['ID'];
		  $theClass=$_POST['theClass'];
		  $theBrand=$_POST['theBrand'];
		  $theStrength=$_POST['theStrength'];
		  $theForm=$_POST['theForm'];
		  $theMinimumBalance=$_POST['theMinimumBalance'];
		  $theMaximumBalance=$_POST['theMaximumBalance'];
		  $theCDPage=$_POST['theCDPage'];
		  W9_UpdateCDPageTitleAndLabel($ID, $theClass,
                              $theBrand, $theStrength, $theForm,
                              $theMinimumBalance,
                              $theMaximumBalance, $theCDPage);
                  break;

  	case 555: F2_UploadCDPageData($HTTP_POST_FILES); break;

        case 560: W4_EditCDRegisterMA(); break;
        case 561: $ID=$_POST['ID'];
                  W4_EditSingleCDRegisterMA($ID); break;
        case 562: $ID=$_POST['id'];
		  $theAuthority=$_POST['theauthority'];
		  $theCaseID=$_POST['thecaseid'];
		  $theCollectorName=$_POST['thecollectorname'];
		  $theCollectorRelation=$_POST['thecollectorrelation'];
		  $theCollectorID=$_POST['thecollectorid'];
		  $theCollectorIDMA=$_POST['thecollectoridma'];
		  $theCollectorIDMAFull=$_POST['thecollectoridmafull'];
		  $theRealID=$_POST['therealid'];
		  $thePageID=$_POST['thepageid'];
		  $theItem=$_POST['item'];
//		  $theQty=$_POST['qty'];
                  W4_UpdateCDRegisterMA($ID, $theCaseID,
                      $theCollectorName, $theCollectorRelation,
                      $theCollectorID, $theRealID, $thePageID,
                      $theCollectorIDMA, $theCollectorIDMAFull,
//                      $theItem, $theQty); break; 
                      $theItem); break; 


        case 570:
        W5_EditFileDestination();
        break;

        case 571:
        $ID=$_POST['ID'];
        W5_EditSingleFileDestination($ID);
        break;

        case 572:
        $ID=$_POST['ID']; 
        $theFileDestination=$_POST['theFileDestination'];
        $theDisplayOrder=$_POST['theDisplayOrder'];
        W5_UpdateFileDestination($ID, $theFileDestination, $theDisplayOrder);
        break;





        // Take in a year and set the RealStudent field all real students to 99 (meaning inactive) ...
        // We may also need to flush those who have graduated
        case 590:
        AZA_GetComingYear();
        break;

        case 592:
        $yearComing = $_POST['yearComing'];
        AZA_SetYear();
        break;



        case 600: Y1_Marking (); break;
        case 602: $PER=$_POST['PER'];
                  if ($PER=="P") { $week=$_POST['weekP']; };
                  if ($PER=="E") { $week=$_POST['weekE']; };
                  Y1A_ExecuteMarks ($PER, $week); break;
        case 610: Z1_ChangeMaxima(); break;
        case 612: $style=$_POST['style'];
                  $information=$_POST['information'];
                  $structure=$_POST['structure'];
                  $response=$_POST['response'];
                  $understanding=$_POST['understanding'];
                  Z1A_ImplementChangeMaxima($style, $information, $structure,
                     $response, $understanding); break;
        case 620: X2A_ResetFMR(); break;
        case 622: $student=$_POST['student'];
                  $case=$_POST['case'];
                  $F=$_POST['F'];
                  $M=$_POST['M'];
                  $R=$_POST['R'];
                  X2B_ImplementResetFMR($student, $case, $F, $M, $R);
                  break;

        case 625: X3A_DeleteEntry(); break;
        case 627: $student=$_POST['student'];
                  $case=$_POST['case'];
                  X3B_ImplementDeleteEntry($student, $case); break;

	case 630: S2_EditSynAdd(); break;
	case 632: $theAddressSynonymID=$_POST['theAddressSynonymID'];
		S2A_NewSynAddDetails($theAddressSynonymID); break;
	case 634: $theAddressSynonymID=$_POST['theAddressSynonymID'];
		$theMainWord=$_POST['theMainWord'];
		$theSynonyms=$_POST['theSynonyms'];
		S2B_ImplementNewSynAdd($theAddressSynonymID,
                  $theMainWord, $theSynonyms); break;

	case 640: S2C_EditSynPharm(); break;
	case 642: $thePharmacySynonymID=$_POST['thePharmacySynonymID'];
		S2D_NewSynPharmDetails($thePharmacySynonymID); break;
	case 644:
           $thePharmacySynonymID=$_POST['thePharmacySynonymID'];  
	          $theMainWord=$_POST['theMainWord'];
                  $theSynonyms=$_POST['theSynonyms']; 	
                  S2E_ImplementNewSynPharm($thePharmacySynonymID,            
                          $theMainWord, $theSynonyms);
                  break;

	case 650: S2F_EditSynReg(); break;
	case 652: $theRegisterSynonymID=$_POST['theRegisterSynonymID'];
		S2G_NewSynRegDetails($theRegisterSynonymID); break;
	case 654: $theRegisterSynonymID=$_POST['theRegisterSynonymID'];
		  $theMainWord=$_POST['theMainWord'];
	  	  $theSynonyms=$_POST['theSynonyms'];
		  S2H_ImplementNewSynReg($theRegisterSynonymID,
                  $theMainWord, $theSynonyms); break;

        // Setting configuration ...
        case 660:
        X9B_ChooseDispensary();
        break;

        case 664:
        X9_SetExam($_POST['location']);
        break;

        case 662:
        $dispensaryMode=$_POST['dispensaryMode'];
        $threeKeyPoints=$_POST['threeKeyPoints'];
        $locationDetection=$_POST['locationDetection'];
        $theDispensary=$_POST['location'];
        X9A_ImplementSetExam($theDispensary, $dispensaryMode, $threeKeyPoints, $locationDetection);
        break;

        // Releasing generic comments for exam weeks ...

        case 665:
        V_EditExamWeeksReleasedGC();
        break;

        case 666:
        $ID=$_POST['ID'];
        V_EditSingleExamWeekReleasedGC($ID);
        break;

        case 667:
        $theID = $_POST['ID'];
        $theYearGroup = $_POST['theyeargroup'];
        $theLocation = $_POST['thelocation'];
        $theWeekType = $_POST['theweektype'];
        $theRR = $_POST['therr'];
        $theWR = $_POST['thewr'];
        V_UpdateSingleExamWeekReleasedGC($theID, $theYearGroup, $theLocation, $theWeekType, $theRR, $theWR);
        break;

        case 670: ZZ_ViewLogon(); break;
        case 680: ZSA_SpecialAccessSummary(); break;
        case 682: $theID=$_POST['ID'];
                  ZSA_SpecialAccessOne($theID); break;
        case 684: $ID=$_POST['ID'];
                  $studentID=$_POST['newStudentID'];
                  $programMode=$_POST['newMode'];
                  $letter=$_POST['newLetter'];
                  ZSA_SpecialImplement($ID, $studentID, $programMode, $letter);
                  break;
        case 700: Y2_MarkingWW (); break;

        // Information on type of student ...
        case 710:
        Y3_MarkingWWC_Pre ();
        break;

        case 711:
        // $_POST has a 'Year' element itself with elements 1, 2, 3 and 4 to represent years 1 to 4. These contain locations (in no particular order) that
        // are required e.g. $_POST['Year'][3] might have elements [0] => Nottingham and [1] => Bechuanaland
        $yearAndLocation = $_POST['Year'];

        // $year is a two dimensional array - we shall have to compare its contents with ScripStudent. Will be more effient if it is one-


        $forReportYear = array();
        $forReportLocation = array();
        foreach ($_POST['Year'] as $year => $yearOnlyArray){
            // We now have $year (e.g. 1, 2, 3, 4) and an array of locations based on that year e.g. year may be 2 and yearOnlyArray Nottingham and Malaysia
            // meaning we want marks for both Nottingham and Malaysia for Year 2 ...
            for ($k = 0; $k < count($yearOnlyArray); $k++){
                // Read into two separate 1-D arrays ...
                $forReportYear[] = $year;
                $forReportLocation[] = $yearOnlyArray[$k];
            };
        };
        // This rather contorted mechanism will therefore produce e.g.
        //               $forReportYear[0] = 1 and $forReportLocation[0] = "Nottingham"
        //               $forReportYear[1] = 1 and $forReportLocation[1] = "Bechuanaland"
        //               $forReportYear[2] = 1 and $forReportLocation[2] = "Bohemia"
        //               $forReportYear[3] = 2 and $forReportLocation[3] = "Nottingham"
        //               $forReportYear[4] = 2 and $forReportLocation[4] = "Latium"
        //               $forReportYear[5] = 3 and $forReportLocation[5] = "Nottingham"
        // i.e. two arrays the matching values of which are year number and location pairs.

        $academicYear = $_POST['academicyear'];
        $location = $_POST['location'];
        $typeOfMarks = $_POST['typeofmarks'];

// This is useful code for explaining what's what ...
//for ($jj = 0; $jj < count ($forReportYear); $jj++){
//print("Year $forReportYear[$jj] in $forReportYearLocation[$jj]<br>");
//};


        Y3_MarkingWWC ($academicYear, $forReportYear, $forReportLocation, $typeOfMarks);
        break;

        case 720:
            Y4_MarkingWWC (); // report for exams
            break;

        case 730: Y4_MarkingWWCR (); break;

        case 740: GG_EditModeratorData(); break;
        case 741:  $SID=$_POST['ID'];
		GGA_EditSingleModerator($SID); break;
 	case 742: $SID=$_POST['ID'];
	    $thefirstname=$_POST['firstname'];
	    $thelastname=$_POST['lastname'];
	    $theUserName=$_POST['username']; 	
	    $thePassword=$_POST['password']; 	
	    $theRealModerator=$_POST['realmoderator']; 	
 	    GGB_UpdateModerator($SID, $thefirstname, $thelastname,
 		$theUserName, $thePassword, $theRealModerator);
            break;

        case 777: 
        Q_WeeklySummary();
        break;

        // Edit the table ScripDates:
        case 780:
        $yearGroup = $_POST['yeargroup']; 
        I_EditDates($yearGroup);
        break;

        case 781:
        I_EditDatesYearGroup();
        break;

        case 782:
        $SID=$_POST['ID'];
        $yearGroup = $_POST['yeargroup']; 
        IA_EditSingleDate($SID, $yearGroup);
        break;

        case 784:
        $SID=$_POST['ID'];
        $yearGroup = $_POST['yeargroup']; 
	 $theType=$_POST['thetype'];
	 $theNumber=$_POST['thenumber'];
	 $theDate=$_POST['thedate'];
	 $theLocation=$_POST['thelocation'];
        IB_UpdateDates($SID, $yearGroup, $theType,$theNumber, $theDate, $theLocation);
        break;


        // 
        case 800: Y9_LogonData (); break;
        case 810: Y5_AllData (); break;
        case 820: Y6_AllModeratedExamResults (); break;

        case 830: XA_MassReleaseChooseOne();break; 
        case 831: $ID = $_POST['id']; 
                  XA_MassReleaseEditOne($ID);break; 
        case 832: $ID = $_POST['id'];
                  $YearGroup = $_POST['yeargroup'];
                  $Location = $_POST['location'];
                  $Category = $_POST['category'];
                  $ReleaseDate = $_POST['releasedate'];
                  XA_MassReleaseSaveOne($ID, $YearGroup, $Location, $Category, $ReleaseDate);
                  break; 

        case 865: Z_EditInitialLetterClassification(); break;
        case 866: $SID=$_POST['ID'];
	           ZA_EditSingleInitialLetterClassification($SID); break;
        case 867: $SID=$_POST['ID'];
                  $theInitialLetter=$_POST['theinitialletter'];
                  $theClassification=$_POST['theclassification'];
                  ZB_UpdateInitialLetterClassification($SID, $theInitialLetter,
                        $theClassification); break;


        case 870:
        Z_TimesAndPlaces();
        break;

        case 872:
        // Put the data in the table and re-display ...
        $condition = array('Time'=>$_POST['timeChosen'], 'Place'=>$_POST['placeChosen']);
        $data = array('Available'=>$_POST['settingChosen']);

        $db=new dbabstraction();
        $db->connect() or die ($db->getError()); 

        $table = $db->select("ScripTimesAndPlaces", $condition);

        if (count($table) > 0){
            // Update ...
            $table=$db->update("ScripTimesAndPlaces", $data, $condition);
        } else {
            // Insert ...
            $data = array_merge($data, $condition);
            $table=$db->insert("ScripTimesAndPlaces", $data);
        };

        Z_TimesAndPlaces($uniqueTime, $uniquePlace);

        break;

        case 874:

        // Explode the imploded arrays ...
        $uniqueTime = explode("|", $_POST['uniquetime']);
        $uniquePlace = explode("|", $_POST['uniqueplace']);

        // Add the new item to the correct array ...
        switch ($_POST['menu']){
            case "time":
            array_push($uniqueTime, $_POST['newmenuitem']);
            break;

            case "place":
            array_push($uniquePlace, $_POST['newmenuitem']);
            break;
        };
        // Call the code again ...
        Z_TimesAndPlaces($uniqueTime, $uniquePlace);
        break;

 	case 900: GZ_EditResitData(); break;
 	case 910: $EntryID=$_POST['ID'];
		  GZA_EditSingleResitStudent($EntryID); break;
 	case 920: $EntryID=$_POST['ID'];
            $adv[0]=$_POST['advance1'];
            $adv[1]=$_POST['advance2'];
            $adv[2]=$_POST['advance3'];
            $adv[3]=$_POST['advance4'];
            $adv[4]=$_POST['advance5'];
            $adv[5]=$_POST['advance6'];
            $RSt[0]=$_POST['RStart0'];
            $RSt[1]=$_POST['RStart1'];
            $RSt[2]=$_POST['RStart2'];
            $RSt[3]=$_POST['RStart3'];
            $RSt[4]=$_POST['RStart4'];
            $RSt[5]=$_POST['RStart5'];
            for ($i=0; $i<$maxResits; $i++) { // Add the advances ...
                $RSt[$i] += $adv[$i]*60;
            };
            $R1=$_POST['R1'];
            $R2=$_POST['R2'];
            $R3=$_POST['R3'];
            $R4=$_POST['R4'];
            $R5=$_POST['R5'];
            $R6=$_POST['R6'];
            $anID=$_POST['studentid'];
 	    GZB_UpdateResitStudent($EntryID, $anID,
 		$R1, $R2, $R3, $R4, $R5, $R6, $RSt); break;

        case 930:
        $anID=$_POST['studentid'];
        GZC_DeleteResitStudent($anID);
        break;



        // The serious error table:

        case 935:
        V_EditSeriousErrors();
        break;

        case 936:
        $ID=$_POST['ID'];
        V_EditSingleSeriousError($ID);
        break;

        case 937:
        $theID = $_POST['ID'];
        $theOrder = $_POST['theorder'];
        $theText = $_POST['thetext'];
        V_UpdateSeriousErrors($theID, $theOrder, $theText);
        break;


        // Delete discussions ... 

        case 940:
        X5A_DeleteDiscussion();
        break;

        case 941:
        $student=$_POST['student'];
        $case=$_POST['case'];
        X5B_ImplementDeleteDiscussion($student, $case);
        break;


        // 

        case 960: $newLetter = $_POST['newletter'];
                  CH_AddNewLetter($newLetter);
                  break;

        case 962: $newNumber = $_POST['newnumber'];
                  $newLetter = $_POST['newletter'];
                  CH_AddNewNumber($newLetter, $newNumber);
                  break;


        // The questions for the calculations ...
        case 970:
        CQ_EditCalculationQuestions();
        break;

        case 971:
        $SID=$_POST['ID'];
        CQ_EditSingleCalculationQuestion($SID);
        break;

        case 972:
        $SID=$_POST['ID'];
        $theExerciseSet=$_POST['theexerciseset'];
        $theExerciseNumber = $_POST['theexercisenumber'];
        $theQuestionNumber = $_POST['thequestionnumber'];
        $theQuestionText = $_POST['thequestiontext'];

        CQ_UpdateCalculationQuestion($SID, $theExerciseSet, $theExerciseNumber, $theQuestionNumber, $theQuestionText);
        break;


        // The answers for the calculations ...
        case 975:
        CA_EditCalculationAnswers();
        break;

        case 976:
        $SID=$_POST['ID'];
        CA_EditSingleCalculationAnswer($SID);
        break;

        case 977:
        $SID=$_POST['ID'];
        $theQuestionID = $_POST['thequestionID'];
        $theMA = $_POST['thema'];
        CA_UpdateCalculationAnswer($SID, $theQuestionID, $theMA);
        break;


        case 980:
        CH_EditMA("", "");
        break;

        case 981: if ($_POST['Submit']) {
                      // The button has been pressed ...
                      $theChosenLetter = $_POST['chosenletter'];
                      $theChosenNumber = $_POST['chosennumber'];

                      // No number - therefore must want to change availability ...
                      if ($theChosenNumber) {
                          // Have a number - go on to deal with model answers in it ...
                          CHA_SingleMA($theChosenLetter, $theChosenNumber);
                      };

                      for ($i = 0; $i < $_POST['maxExercise'] + 1; $i++) {
                          if ($_POST['available'.$i]) {
                              $available[$i] = $_POST['available'.$i]; 
                          };
                      };

                      $newCategory = $_POST['newcategory'];
                      CHG_UpdateAvailability($available, $newCategory);
//                      CHE_UpdateAvailability($available);
//                      CHF_UpdateCategory($newCategory);

                  } else { // Automatic jump ...
                      CH_EditMA("", "");
                  };
                  break;

        case 982: CHB_UpdateMA();
                  break;

        case 990: CHC_AmendInstructions();
                  break;

        case 991: $comment1 = $_POST['comment1'];
                  $comment2 = $_POST['comment2'];
                  $comment3 = $_POST['comment3'];
                  CHD_UpdateInstructions($comment1, $comment2, $comment3);
                  break;


        // Student answers ...
        case 995: Y3_1_AllAnswers();
                  break;

        case 997: $questionID=$_POST['questionid'];
                  Y3_2_AllAnswers($questionID);
                  break;




	 default: C_ShowMenu($tab);break;
     };
};




A_html_end();

// ******************************

function X1C_ReleaseCheckingImplement($CR, $weekChecking) {
    Z_DrawHeader();

    // Look in ScripCheckingTermPattern under the current week (e.g. Week 2), read the letters there (e.g. F, G and H)
    // and then set the Release field in ScripCheckingResults to either 1 or 0 ($CR). 


    // January 2009 - We are now using ScripCheckingAllPattern instead. We're still using ScripChecking Results 
    // (which is where the Release field is) so this is there the information is put, but it is now based on
    // what is found in ScripCheckingAllPattern instead.

    if ($CR == "Release") { $release = 1; } else { $release = 0; };

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 


    // Get all the two-letter codes associated with the week that the administrator has just selected: one week has many two-letter codes ... 
    $condition = array('Week'=>$weekChecking, 'YearGroup'=>$_SESSION['yearGroup'], 'Location'=>$_SESSION['location']);
    $table = $db->select("ScripCheckingAllPattern", $condition);
    foreach ($table as $row) {
        // Build up an array of released letters ...
        $releasedLetters[] = $row['SetName'];
    };

/*
    // This was the mechanism before January 2009 ...
    $table = $db->select("ScripCheckingTermPattern");
    foreach ($table as $row) {
        $letter = $row['wk'.$weekChecking];
        // Is it a letter (and not a blank)?
        if ($letter) { // Yes = store it ...
            $releasedLetters[] = $row['wk'.$weekChecking];
        };
    };
*/

    // Look for the two-letter codes that are in $releasedLetters and set the Release field to 1 for them in ScripCheckingResults:
    // Loop over each two-letter code ...
    for ($i = 0; $i < count($releasedLetters); $i++) {
        // Use a selectlike so that we can select all instances of a two-letter code regardless of the digit following e.g. can collect all ZC together 
        $condition="Tab LIKE '".$releasedLetters[$i]."%'";
        $table=$db->selectlike("ScripCheckingResults", $condition);

        // Look at each row found ...
        foreach ($table as $row) {
            // ... and apply the update only to results that match time and place ...
            if ($row['Location'] == $_SESSION['location'] && $row['YearGroup'] == $_SESSION['yearGroup']){
                // Read the primary key so that we canuse it in the update ...
                $resultID = $row['ResultID'];
                $condition = array('ResultID'=>$resultID);
                $text = array('Release'=>$release);
                $db->update("ScripCheckingResults", $text, $condition);
            };
        };

    };

    // Set the week and release condition just chosen by the administrator ...
    $condition = array('WeekNumber'=>$weekChecking, 'YearGroup'=>$_SESSION['yearGroup'], 'Location'=>$_SESSION['location']);
    $text = array('CR'=>$release);

    $db->update("ScripCheckingWeeksReleased", $text, $condition);

    print("<tr><td colspan='2'><font color='#ffffff'>");
    print("Week $weekChecking of Checking has these letters:<br>\n");
    for ($i = 0; $i < count($releasedLetters); $i++) {
        print($releasedLetters[$i]."<br>\n");
    };
    print("These have been set to $CR.<br>\n");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</font></td></tr>\n");
    print("</table>\n");
};



function XA_MassReleaseChooseOne() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all categories ...
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one category from the list and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=831>");

    print("<tr>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Year Group</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Location</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Category</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Release Date</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio type=text name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripReleaseDates");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        print("<td><input type='radio' name='id' value='$theID'></td>");
        print("<td><font color='#ffffff'>$theID</font></td>");

        $theYearGroup=$row['YearGroup'];
        print("<td><font color='#ffffff'>$theYearGroup</font></td>");

        $theLocation=$row['Location'];
        print("<td><font color='#ffffff'>$theLocation</font></td>");

        $theCategory=$row['Category'];
        print("<td><font color='#ffffff'>$theCategory</font></td>");

        $theReleaseDate=$row['ReleaseDate'];
        print("<td><font color='#ffffff'>$theReleaseDate</font></td>");
        print("</tr>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};
 
function XA_MassReleaseEditOne($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Get details of the category and display ...
    $condition = array('ID'=>$ID);   
    $table=$db->select("ScripReleaseDates", $condition);
    Z_DrawHeader();
    print("<tr><td colspan=2 align=center><TABLE BORDER=1>");
    print("<form ACTION=scripadmin.php METHOD=POST>");
    print("<input type='hidden' name='tab' value='832'>");
    print("<input type='hidden' name='id' value='$ID'>");

    print("<tr>");
    print("<th><font color='#FFFFFF'>ID</font></th>");
    print("<th><font color='#FFFFFF'>Year Group</font></th>");
    print("<th><font color='#FFFFFF'>Location</font></th>");
    print("<th><font color='#FFFFFF'>Category</font></th>");
    print("<th><font color='#FFFFFF'>Release Date</font></th>");
    print("</tr>");

    if ($ID) {
        foreach ($table as $row) {
            $theID=$row['ID'];
            $theYearGroup=$row['YearGroup'];
            $theLocation=$row['Location'];
            $theCategory=$row['Category'];
            $theReleaseDate=$row['ReleaseDate'];
        };
    };

    print("<tr>");
    print("<td><font color='#ffffff'>$ID</font></td>");
    print("<td><input type='text' name='yeargroup' value=$theYearGroup></td>");
    print("<td><input type='text' name='location' value=$theLocation></td>");
    print("<td><input type='text' name='category' value=$theCategory></td>");
    print("<td><input type='text' name='releasedate' value=$theReleaseDate></td>");
    print("</tr>");

    print("<tr><td colspan='3' align=center>");
    print("<input type='submit' value='Update'></td></tr>");

    print("</form></table></td></tr><tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>");
};
 
function XA_MassReleaseSaveOne($ID, $YearGroup, $Location, $Category, $ReleaseDate){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the category ...
    $data = array(
	"YearGroup" => $YearGroup,
	"Location" => $Location,
	"Category" => $Category,
	"ReleaseDate" => $ReleaseDate);

    if ($ID) {
        $condition=array('ID'=>$ID);
        $table=$db->update("ScripReleaseDates",$data,$condition);         
    } else {
        $table=$db->insert("ScripReleaseDates",$data);         
    };

    Z_DrawHeader();
    print("<tr><td colspan='2'><font color='#ffffff'>Category updated");
    print("</font></td></tr>");
    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a>");
    print("</td></tr></table>");
}; 


// Write the HTML for the head of the page
// Called from top level at start of program
function A_html_begin($title, $header) {
    global $tab;

    if ($tab!=500) {
     	print ("<HTML>\n");
	print ("<HEAD>\n");
	if ($title) print ("<TITLE>$title</TITLE>\n");
        print("<script type='text/javascript'>");
        print("function classifyX(variable) {");
        print("alert(variable)");
        print("}");
        print("</script>");
	print ("</HEAD>\n");
	print ("<BODY  LINK=aqua VLINK=aqua>\n");
	if ($header) print ("<H1>$header</H1>\n");
    };
};

// Write the HTML to close the page
// Called at end of program by top level
function A_html_end() {
    print ("</BODY></HTML>\n");
   // end of function A_html_end
};

// Display the title and logon screen
// Called if this is the first run of the program
function B_logon() {
    Z_DrawHeader();
    print("<TD colspan=2 ><FONT COLOR=#FFFFFF SIZE=4>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=1>");
    print("Please log on as Administrator:"); 
    print("&nbsp;&nbsp;&nbsp;password: ");
    print("<INPUT TYPE=PASSWORD SIZE=10 MAXCHAR=10 NAME=password>");
    print("&nbsp;&nbsp;&nbsp;");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR></table>");
}  /* end of function B_logon */

/* log on to system as administrator */

function BA_CheckPassword($password) {

    global $theusername, $thepassword;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $table=$db->select("ScripConfiguration");
    foreach ($table as $row) {
        $theusername=$row['AdminUserName'];
        $thepassword=$row['AdminPassword'];
    };

    Z_DrawHeader();
    print("<TR><TD>");
    if ($password == $thepassword) {
        C_ShowMenu($tab);
    } else { print("<FONT COLOR=#FFFFFF>");
        print("<H2>Incorrect password</H2>");
        print("<P><A HREF=scripadmin.php?tab=0>Start Again</P>");
        print("</FONT>");
    };
    print("</TD></TR></TABLE>");
};



function showNextTitle($permission){
    // Are there any items to be displayed in the next set? If so, show the title of that set:

    // Default is there are no such items ...
    $displayNext = false;
    // Loop through the permission object - is the admin level greater than any of the numbers in the object?
    foreach ($permission as $key => $value){
        if ($_SESSION['adminLevel'] >= $value){ $displayNext = true; };
    };
    return $displayNext;

};


function X_LevelInArray($allowedLevels){
    // Is $_SESSION['adminLevel'] in the array of allowed levels?

    // Function returns key number (if first argument is found in second) or (strictly) FALSE (if not found)
    if (array_search($_SESSION['adminLevel'], $allowedLevels) === false){
        return 0;
    } else {
        return 1;
    };
};


function C_ShowMenu($tab) {
//    global $thepassword, $authority;
    if ($tab>0) {
        Z_DrawHeader();
    };

    if (!$thepassword && $_SESSION['authority']!="A") {
     	print("<P>Error - aborting program</P>");
	die ("<P>Insecurity problem</P>");
    };

    print("<tr>");
    print("<td COLSPAN='2' ALIGN='CENTER'><FONT COLOR='#FFFFFF'>");
    print("<h1>Administrator Menu (No. ".$_SESSION['adminNumber']." @ level ".$_SESSION['adminLevel'].")</h1>"); 


    // Levels:
    // 99 Full administrator - can do everything - CAN moderate
    // 88 Combination of 77 and 33 - CAN moderate
    // 77 Academic viewer - CAN moderate
    // 55 External examiner (exam only) - can NOT moderate
    // 33 Demonstrator and marker and inputter - CAN moderate 
    // 22 Demonstrator and marker - CAN moderate
    // 12 Demonstrator (senior - CAN see exams)  - can NOT moderate
    // 11 Demonstrator (junior - can NOT see exams) - can NOT moderate


// Exercises
// http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=10&a=M&d=0&m=&v=1
//'http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=1'
// Generic
// http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=10&a=M&d=0&m=&v=2
// Calculations
// http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=10&a=M&d=0&m=&v=3


//'http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=1'
//'http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=2'
//'http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=3'



// The GET variables are used in ScripModerate under these names:
//        $_SESSION['table']=$_GET['t'];              YES
//        $_SESSION['ID']=$_GET['i'];                 YES ... and it's the moderator ID - will have to be ! ID

// Gets  $_SESSION['moderatorID']=$row['ModeratorID']; $_SESSION['mfirstname']=$row['FirstName']; $_SESSION['mlastname']=$row['LastName']; $_SESSION['musername']=$row['UserName']; $_SESSION['mpassword']=$row['Password'];

//        $_SESSION['authority']=$_GET['a'];          YES ... becomes $_SESSION['MorS']
//        $_SESSION['inTheDispensary']=$_GET['d'];    NO
//        $_SESSION['dispensaryMode']=$_GET['m'];     NO
//        $_SESSION['variety']=$_GET['v'];            YES
// and we need to see whether they are used further in ScripModerate




    $allowedLevels = array(99, 88, 77, 33, 22);
    if (X_LevelInArray($allowedLevels)) {
        print("<h2>Moderation: ");
//        print("<a href='http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=1'>Individual</a> � ");
        print("<a href='ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=1'>Individual</a> � ");
//        print("<a href='http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=2'>Generic</a> � ");
//        print("<a href='http://www.nottingham.ac.uk/pharmacy/scripware/ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=3'>Calculations</a></h2>");
        print("<a href='ScripModerate.php?t=Moderator&i=".$_SESSION['adminNumber']."&a=M&v=3'>Calculations</a></h2>");
    };
    print("</FONT></td>");
    print("</tr>"); 

    // Show menu options in COLUMN 1

    print("<tr>");
    print("<td colspan=4><table><tr width=33%>");
    print("<td valign=top><FONT COLOR=#FFFFFF>");

    print("<b>Configuration Details � Add or Edit</b></font>");

    print("<FONT COLOR=#FFFFFF>");

    print("<ol>");
//    print("<li><a href=scripadmin.php?tab=10><FONT COLOR=#FFFFFF>Change administrator password<FONT></a></li>");

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=15><font color='#ffffff'>Administrators</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=590><font color='#ffffff'>Inactivate</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=120><font color='#ffffff'>Configuration Details - Edit <i>only</i></font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=781><font color='#FFFFFF'>Dates</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=740><font color='#ffffff'>Moderators</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=103><font color='#ffffff'>Term Patterns</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=113><font color='#ffffff'>Exam Patterns</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=108><font color='#ffffff'>Checking Patterns</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=490><font color='#ffffff'>Release or Retract a Week</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=665><font color='#ffffff'>Release or Retract a Week (Generic Comments for Exams only)</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=495><font color='#ffffff'>Manual Release or Retract</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=830><font color='#ffffff'>Mass Release of Other Scrips</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=610><font color='#ffffff'>Change OTC Maxima</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=660><font color='#ffffff'>Set Availability</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=670><font color='#ffffff'>View Logon Data</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=865><font color='#ffffff'>Amend Initial Letter Classification</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=870><font color='#ffffff'>Times and Places</font></a></li>");
    };

    print("</ol>");


    // This array pertains to the "Student and PC Details" menu: one item in the object for each object in the menu. What level must an administrator have
    // in order to see the item?
    $permissionStudentPC['newStudent'] = 50;



    // Use the function array_search: $key = array_search($item, $array) returns the key where item occurs in array; === FALSE if no found


    // Only show this heading if there is at least one item to show (pass the permission array to the fucntion showNextTitle) ... 
    if (showNextTitle($permissionStudentPC)) { print("<font color='#ffffff'><b>Student and PC Details</b></font>"); };
    print("<ol>");

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=220><font color='#ffffff'>Add new student</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=227><font color='#ffffff'><i>Delete all students marked as real</i> (still set at select)</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=20><font color='#ffffff'>Edit Student Data</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=30><font color='#ffffff'>Upload Student Data</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=200><font color='#ffffff'>View Student Password</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=8><font color='#ffffff'>Create Short-term Password</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=680><font color='#ffffff'>Special Access</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=35><font color='#ffffff'>Upload IP Address Data</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=470><font color='#ffffff'>Add New IP Address</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=460><font color='#ffffff'>Edit IP Address</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=465><font color='#ffffff'>IP Address Locations</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=620><font color='#ffffff'>Edit FMR of Given Student</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=625><font color='#ffffff'>Delete Entry of Given Student</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=940><font color='#ffffff'>Delete a Discussion</font></a></li>");
    };

//    print("<LI><A HREF=scripadmin.php?tab=210><FONT COLOR='#FFFFFF'>Remove all students</FONT></A></LI>");

    print("</ol></font></td>");

    // Show menu options in Column 2
    print("<td width=33% valign=top><FONT COLOR=#FFFFFF><b>Main Scrip � Add or Edit</b></font><FONT COLOR=#FFFFFF><OL>");

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=40><FONT COLOR=#FFFFFF>Patients</FONT></A> - <A HREF=scripadmin.php?tab=44><FONT COLOR=#FFFFFF>Upload</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=45><FONT COLOR=#FFFFFF>Lines from PMR</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=65><FONT COLOR=#FFFFFF>Trusts</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=60><FONT COLOR=#FFFFFF>Prescribers' Practices</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=50><FONT COLOR=#FFFFFF>Prescribers</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=70><FONT COLOR=#FFFFFF>Forms</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=130><FONT COLOR=#FFFFFF>Text of OTCs</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=150><FONT COLOR=#FFFFFF>Drug Problems</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=160><FONT COLOR=#FFFFFF>Patient Problems</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=170><FONT COLOR=#FFFFFF>Prescriber Problems</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=180><FONT COLOR=#FFFFFF>Miscellaneous Problems (incl. OTC)</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI>Hospital TTO � <A HREF=scripadmin.php?tab=75><FONT COLOR=#FFFFFF>Prescription</FONT></A> and <A HREF=scripadmin.php?tab=85><FONT COLOR=#FFFFFF>Patient</FONT></A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=90><FONT COLOR=#FFFFFF>Scrips</FONT></A> and <A HREF=scripadmin.php?tab=97><FONT COLOR='#FFFFFF'>As CSV file TEST ONLY</A> and <A HREF=scripadmin.php?tab=115><FONT COLOR='#FFFFFF'>Copy one line TEST ONLY</A></LI>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><A HREF=scripadmin.php?tab=360><FONT COLOR=#FFFFFF>Help Text</FONT></A></LI>");
    };

    print("</ol></font>");

    print("</ol>");

    // Sub-section: MODEL ANSWERS - ADD OR EDIT ...
    print("<font color='#ffffff'><b>Model Answers � Add or Edit</b>");
    print("<ol>");

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><a href=scripadmin.php?tab=380><font color='#ffffff'>Classify</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><a href=scripadmin.php?tab=55><font color='#ffffff'>Interaction</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><a href=scripadmin.php?tab=420><font color='#ffffff'>Labels</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<LI><a href=scripadmin.php?tab=440><font color='#ffffff'>POR</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=560><font color='#ffffff'>CD Register</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=480><font color='#ffffff'><i>CD Register - Old</i></font></a></li>");
    };

    print("</ol>");

    // Sub-section: CHECKING EXERCISES - ADD OR EDIT ...
    print("<font color=#ffffff><b>Checking Exercises � Add or Edit</b>");
    print("<ol>");

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=980><font color='#ffffff'>Model Answers and Exercise Availability</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=990><font color='#ffffff'>Amend Instructions</font></a></li>");
    };

    print("</ol>");

    // Show menu options COLUMN 3 ...
    print("<TD width=33% VALIGN=TOP>");

    // Sub-section: TABLES - ADD OR EDIT ...
    print("<font color='#ffffff'><b>Tables � Add or Edit</b><ol>");

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=190'><font color='#ffffff'>Dose Codes</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=390'><font color='#ffffff'>Ancillary Codes</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=140'><font color='#ffffff'>Legal Texts</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=570'><font color='#ffffff'>File Destinations</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=935'><font color='#ffffff'>Serious Errors</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=630'><font color='#ffffff'>Synonyms</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=550'><FONT COLOR='#ffffff'>Drug Database</font></a> - <a href='scripadmin.php?tab=555'><FONT COLOR='#ffffff'>Upload</font></a></li>");
    };

    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href='scripadmin.php?tab=510'><font color='#ffffff'>CD Classes</font></a>");
    };

    print("</ol>");

    // Sub-section: REPORTING � TO CSV FILES ...
    print("<FONT COLOR='#FFFFFF'><b>Reporting � To CSV Files</b>");
    print("<ol>");

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=800><FONT COLOR=#FFFFFF>Log-on Data</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><font color='#ffffff'><a href=scripadmin.php?tab=810>Exercise by exercise (exam only)</a> Fields: Student and then each case (<i>e.g.</i> AA7)</font></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=820><FONT COLOR=#FFFFFF>All Moderated Exam Results - Showing where marks were lost</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=600><FONT COLOR=#FFFFFF>(Marks)</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li>");
        print("Total Marks Week by Week - ");
        print("<a href=scripadmin.php?tab=710><font color='#ffffff'>Practice</font></a>");
        print(", <a href=scripadmin.php?tab=720><font coor='#ffffff'>Exams</font></a>");
        print(" and <a href=scripadmin.php?tab=730><font color='#ffffff'>Resits</font></a>");
        print("</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=777><font color='#FFFFFF'>Weekly Summary</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><font color='#ffffff'>Calculations: <a href=scripadmin.php?tab=777>Practice</a> and <a href=scripadmin.php?tab=777>exams</a></font></li>");
    };

    $allowedLevels = array(99, 88, 77);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><font color='#ffffff'><a href=scripadmin.php?tab=995>List different types of answers to calculations</a></font></li>");
    };

    print("</ol>");

    // Sub-section: RE-SITS ...
    print("<FONT COLOR=#FFFFFF><b>Re-Sits</b>");
    print("<ol>");
    $allowedLevels = array(99);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=900><FONT COLOR=#FFFFFF>Set Weeks</font></a></li>");
    };
    print("</ol>");

    // Sub-section: CALCULATIONS ...
    print("<font color='#FFFFFF'><b>Calculations</b>");
    print("<ol>");

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=970><font color='#FFFFFF'>Set Questions</font></a></li>");
    };

    $allowedLevels = array(99, 88, 77, 33);
    if (X_LevelInArray($allowedLevels)) {
        print("<li><a href=scripadmin.php?tab=975><font color='#FFFFFF'>Set Answers</font></a></li>");
    };
    print("</ol>");

    print("</FONT></TD></TR>");

// Test
print("</tr></table></td>");


    print("<TR><TD COLSPAN=3 ALIGN=CENTER>");
    print("<A HREF=../index.html>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function DA_ChangePasswordAction($pass1, $pass2)
{   global $theusername;  Z_DrawHeader();
    print("<TR><TD><FONT COLOR=#FFFFFF>");
    if ($pass1 != $pass2)
    {   print("<P>Error. The two passwords do not match.</P>");
    	print("<P><A HREF=scripadmin.php?tab=10>Try Again</A></P>");
    }
    else
    {   $db=new dbabstraction();
    	$db->connect() or die ($db->getError());   
$db->update("ScripConfiguration",array('AdminPassword'=>$pass1),array('AdminUserName'=>$theusername));
	print("<P>Password updated</P>");
    	print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    }
    print("</FONT></TD></TR></TABLE>");
}  /* end of function NA_ChangePasswordAction */

function D_ChangePasswordForm()
{   /* Write form to ask for new password */
    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=#FFFFFF>");
    print("<H2>Change your password:</H2></FONT></TD></TR>");
/* password change form */
    print("<TR><TD colspan=2><FORM METHOD='POST'>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=11>");
    print("<FONT COLOR=#FFFFFF>");
    print("<P>Enter your new password in both fields and click on submit.</P>");
    print("</FONT><P><INPUT TYPE=PASSWORD NAME=pass1></P>");
    print("<P><INPUT TYPE=PASSWORD NAME=pass2></P>"); 
    print("<P><INPUT TYPE=SUBMIT VALUE='confirm'></P>");   
    print("</FORM></TD></TR><TR><TD>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR></TABLE>");
}  /* end of function Change password form */







function E_EditStudentData() {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all people in student table and display as a table
    $table=$db->select("ScripStudent");
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one student from the list and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=21>");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>First Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>User Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Password</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Practice Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Exam Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Real Student</FONT></TH>");
    print("</TR>");
    print("<td><FONT COLOR=FFFFFF>");
    print("<INPUT TYPE=RADIO NAME=ID VALUE='0'>New");
    print("</font></td>");

    foreach ($table as $row) {
        $theStudentID=$row['StudentID'];
        $theFirstName=$row['FirstName'];
        $theLastName=$row['LastName'];
        $theUserName=$row['UserName'];
        $thepassword=$row['Password'];
        $thePracticeCode=$row['PracticeCode'];
        $theExamCode=$row['ExamCode'];
        $theRealStudent=$row['RealStudent'];

        print("<TR><TD>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theStudentID></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$theStudentID</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$theFirstName</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$theLastName</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$theUserName</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$thepassword</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$thePracticeCode</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$theExamCode</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$theRealStudent</FONT></TD>");
        print("</TR>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function EA_EditSingleStudent($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the student and display in a form
    $table=$db->select("ScripStudent",array('StudentID'=>$SID));
    Z_DrawHeader();

    print("<TR><TD colspan='2' align='center'><TABLE BORDER='1'>");
    print("<FORM ACTION=scripadmin.php METHOD='POST'>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=22>");
    print("<INPUT TYPE=HIDDEN NAME=SID value=$SID>");

    foreach ($table as $row) {
        $theStudentID=$row['StudentID'];
        $theStudentSaturnNumber=$row['StudentSaturnNumber'];
        $theFirstName=$row['FirstName'];
        $theLastName=$row['LastName'];
        $theUserName=$row['UserName'];
        $theDefaultPW=$row['DefaultPW'];
        $thePracticeCode=$row['PracticeCode'];
        $theExamCode=$row['ExamCode'];
        $theCheckingPracticeCode=$row['CheckingPracticeCode'];
        $thePracticalGroup=$row['PracticalGroup'];
        $theSeat=$row['Seat'];
        $theRealStudent=$row['RealStudent'];
        $thepassword=$row['Password'];
        $theinitials=$row['initials'];
        $theNameForCDRegister=$row['NameForCDRegister'];
        $theLastRefNo=$row['LastRefNo'];
        $thePWChanged=$row['PWChanged'];
        $thePass=$row['Pass'];
        $theR1=$row['R1'];
        $theR2=$row['R2'];
        $theR3=$row['R3'];
        $theR4=$row['R4'];
        $theR5=$row['R5'];
        $theR6=$row['R6'];
        $theYearGroup=$row['YearGroup'];
        $theLocation=$row['Location'];
    };

    print("<th><font color='#ffffff'>First Name</font></th>");
    print("<th><font color='#ffffff'>Last Name</font></th>");
    print("<th><font color='#ffffff'>User Name</font></th>");
    print("<th><font color='#ffffff'>Student Number</font></th>");

    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=firstname VALUE='$theFirstName'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=lastname VALUE='$theLastName'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=username VALUE='$theUserName'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=studentsaturnnumber VALUE='$theStudentSaturnNumber'></TD>");
    print("</tr>");

    print("<TH><FONT COLOR=FFFFFF>Default Pw</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Password Changed</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Password</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>&nbsp;</FONT></TH>");

    print("<tr>");
    print("<td><INPUT TYPE=TEXT NAME=defaultpw VALUE='$theDefaultPW'></td>");
    print("<td><INPUT TYPE=TEXT NAME=pwchanged VALUE='$thePWChanged'></td>");
    print("<td><INPUT TYPE=TEXT NAME=password VALUE='$thepassword'></td>");
    print("</tr>");

    print("<tr>");
    print("<th><FONT COLOR=FFFFFF>Practical Group</FONT></TH>");
    print("<th><FONT COLOR=FFFFFF>Practice Code</FONT></TH>");
    print("<th><FONT COLOR=FFFFFF>Exam Code</FONT></TH>");
    print("<th><FONT COLOR=FFFFFF>Checking Practice Code</FONT></TH>");
    print("</tr>");

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=practicalgroup VALUE='$thePracticalGroup'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=practicecode VALUE='$thePracticeCode'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=examcode VALUE='$theExamCode'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=checkingpracticecode VALUE='$theCheckingPracticeCode'></TD>");
    print("</TR>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Real Student</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Initials</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name for CD Register</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Seat</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=realstudent VALUE='$theRealStudent'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=initials VALUE='$theinitials'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=nameforcdregister VALUE='$theNameForCDRegister'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=seat VALUE='$theSeat'></TD>");
    print("</TR>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Year Group</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Location</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=yeargroup VALUE='$theYearGroup'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=location VALUE='$theLocation'></TD>");
    print("</TR>");

    print("<TR><TD colspan=8 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

/*
function EB_UpdateStudent($SID, $thefirstname, $thelastname, $theusername, 
	$thepassword, $thepracticecode, $theexamcode,
        $theinitials, $theRealStudent) {*/





function EB_UpdateStudent($SID, $theStudentSaturnNumber,
                 $theFirstName, $theLastName, $theUserName, $theDefaultPW,
                 $thePracticeCode, $theExamCode,
                 $theCheckingPracticeCode, $thePracticalGroup,
                 $theSeat, $theRealStudent,
                 $thepassword, $theinitials, $theNameForCDRegister,
                 $thePWChanged, $theYearGroup, $theLocation) {


//                 $theR1, $theR2, $theR3,
//                 $theR4, $theR5, $theR6) {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

print("$thepassword");

    // Update details of the student ...
    $studentdata = array (
        'StudentSaturnNumber'=>$theStudentSaturnNumber,
	'FirstName'=>$theFirstName,
	'LastName'=>$theLastName,
	'UserName'=>$theUserName,
	'DefaultPW'=>$theDefaultPW,
	'PracticeCode'=>$thePracticeCode,
	'ExamCode'=>$theExamCode,
	'CheckingPracticeCode'=>$theCheckingPracticeCode,
	'PracticalGroup'=>$thePracticalGroup,
	'Seat'=>$theSeat,
	'RealStudent'=>$theRealStudent,
	'Password'=>$thepassword,
	'initials'=>$theinitials,
	'NameForCDRegister'=>$theNameForCDRegister,
	'LastRefNo'=>$theLastRefNo,
	'PWChanged'=>$thePWChanged,
	'Pass'=>$thePass,
//	'R1'=>$theR1,
//	'R2'=>$theR2,
//	'R3'=>$theR3,
//	'R4'=>$theR4,
//	'R5'=>$theR5,
//	'R6'=>$theR6,
	'YearGroup'=>$theYearGroup,
	'Location'=>$theLocation
    );

/*    $studentdata = array (
	"FirstName" => $thefirstname, "LastName" => $thelastname,
	"UserName" => $theusername, "Password" => $thepassword,
	"PracticeCode" => $thepracticecode, "ExamCode" => $theexamcode,
	"initials"=>$theinitials, "RealStudent"=>$theRealStudent); */

    if ($SID) {
        // An existing entry - update it ...
        $condition = array ('StudentID'=>$SID);  
        $table=$db->update("ScripStudent",$studentdata, $condition);
    } else {
        // A new entry - insert it ...
        $table=$db->insert("ScripStudent",$studentdata);
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Student updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};



function F_UploadStudentData($HTTP_POST_FILES) {
    global $thecheck;

    // Read the system array reading the key name and value of that key ...
    // $HTTP_POST_FILES may be $_FILES now
    foreach ($HTTP_POST_FILES as $file_name=>$file_array) {
        $fpath=$file_array['tmp_name'];
        $fname=$file_array['name']; // I don't think this is used anywhere!
    };

    Z_DrawHeader();
    if (strlen($fpath) == 0){ // Is there an uploaded file ready?

//    if (! isset($thecheck)) {
//    I think the above has been REMmed out because it's not a good check - works first time; not thereafter

        // No - inform the user how to upload ...

        $_SESSION['thecheck']=1;
        print("<tr><td><font color='#ffffff'>");
        print("<p>File must be comma separated in format:<br>");
        print("Student number in Saturn, First Name, Last Name, Username, Default Password, Practice Code, Exam Code, Checking Practice Code, Practical Group, Seat in the Dispensary, Real Student, Year Group, Location, Last Updated</p>");
        print("<p>Download a pre-formatted CSV file <a href='year_x.csv'>here</a>.</p>");
        print("<p>(Student number in Saturn: If the number already exists in the database, the student details will be updated only and a second entry not created UNLESS the student number is zero. If it is zero, a new entry for the student is always created.)</p>");
        print("<p>(Real student: 1 => a student; 0 => staff, <i>etc.</i>)</p>");
        print("<p>(Last updated: the first part of the name of the academic year to which the data apply <i>e.g.</i> if the data apply to the year 1837-1838, then enter 1837)</p>");
        print("</font>");
        print("<form enctype='multipart/form-data' method='post'>");
        print("<input type='file' name='fupload' size='80'>");
        print("<input type='submit' value='Confirm'>");
        print("</form></tr></td>");
    } else {
        // Yes - do the uploading ...
        $file_dir="/home/pazscrip/fileupload";
        if (is_uploaded_file($fpath)) {
            move_uploaded_file($fpath, $file_dir)
		or die("Couldn't copy");
        };

        $fpin=fopen($file_dir,"r");

        $db=new dbabstraction();
        $db->connect() or die ($db->getError()); 

        // Define array ...
        $student=array();

        // Get first line (all later lines are read within while loop) ...
        $chunk=fgets($fpin, 1024);

        while (! feof($fpin)) {
            unset ($student);
            // Loop over the data line by line. If an entry for a cell exists, copy it to the associative array $student. If not, don't set the element ... 

            // This is what identifies a student - depending on whether we insert or update the data, this will go into $student or be $condition ...

            // The function strtok doesn't seem to work with empty cells (i.e. null string between commas) - replace with explode function ... 
            list($StudentSaturnNumber, $FirstName, $LastName, $Username, $DefaultPW, $PracticeCode, $ExamCode, $CheckingPracticeCode, $PracticalGroup, $Seat, $RealStudent, $YearGroup, $Location, $LastUpdated) = explode(",", $chunk);

//            $StudentSaturnNumber=strtok($chunk,",");

            // Put all these variables into the associative array $student ...
//            $FirstName=strtok(",");
            if ($FirstName != ""){
                $student["FirstName"]=$FirstName;
            };

//            $LastName=strtok(",");
            if ($LastName){
                $student["LastName"]=$LastName;
            };

//            $Username=strtok(",");
            if ($Username != ""){
                $student["Username"]=$Username;
            };

//            $DefaultPW=strtok(",");
            if ($DefaultPW){
                $student["DefaultPW"]=$DefaultPW;
            };

//            $PracticeCode=strtok(",");
            if ($PracticeCode){
                $student["PracticeCode"]=$PracticeCode;
            };

//            $ExamCode=strtok(",");
            if ($ExamCode){
                $student["ExamCode"]=$ExamCode;
            };

//            $CheckingPracticeCode=strtok(",");
            if ($CheckingPracticeCode){
                $student["CheckingPracticeCode"]=$CheckingPracticeCode;
            };

//            $PracticalGroup=strtok(",");
            if ($PracticalGroup){
                $student["PracticalGroup"]=$PracticalGroup;
            };

//            $Seat=strtok(",");
            if ($Seat){
                $student["Seat"]=$Seat;
            };

//            $RealStudent=strtok(",");
            if ($RealStudent){
                $student["RealStudent"]=$RealStudent;
            };

            if ($Location){
                $student["Location"]=$Location;
            };

//            $YearGroup=strtok(",");
            if ($YearGroup){
                $student["YearGroup"]=$YearGroup;
            };

//            $LastUpdated=strtok(",");
            if ($LastUpdated){
                $student["LastUpdated"]=$LastUpdated;
            };
            // We need to know whether there is already an entry for this student number in table ScripStudent ...
            // October 2012:

            $condition = array ('StudentSaturnNumber'=>$StudentSaturnNumber);  
            $table=$db->select("ScripStudent", $condition);

            // Update if already exists and providing that the student number isn't zero, otherwise insert:
            if (count($table) > 0 && $StudentSaturnNumber != "0"){
                // Existing entry - update (elements not set will not replace existing values; set elements will) ...
                $condition = array ('StudentSaturnNumber'=>$StudentSaturnNumber);  
                $table=$db->update("ScripStudent", $student, $condition);
            } else {
                // New entry - insert ...
                $student["StudentSaturnNumber"]=$StudentSaturnNumber;
                $db->insert("ScripStudent", $student);
            };

            // Get next line ...
            $chunk=fgets($fpin, 1024);
        };

        fclose($fpin);
        print("<tr><td><font color='#ffffff'>File uploaded to student database</p></font></td></tr>");
    };

    print("<tr>");
    print("<td><font color='#ffffff'><p><a href=scripadmin.php?tab=5>Return to Menu</a></p></td>");
    print("</tr>");
    print("</table>");
};


/*
function F_UploadStudentData($HTTP_POST_FILES) {
    global $thecheck;

    // Read the system array ...
    foreach ($HTTP_POST_FILES as $file_name=>$file_array) {
        $fpath=$file_array['tmp_name'];
        $fname=$file_array['name']; // I don't think this is used anywhere!
    };

    Z_DrawHeader();
    if (strlen($fpath) == 0){ // Is there an uploaded file ready?

//    if (! isset($thecheck)) {

        // No - inform the user how to upload ...

     	$thecheck=1;
        print("<TR><TD><FONT COLOR=FFFFFF>");
        print("<p>File must be comma separated in format:<br>");
        print("Student number in Saturn, ");
        print("First Name, Last Name, Username, Default Password, ");
	print("Practice Code, Exam Code, Checking Practice Code, ");
	print("Practical Group, Seat in the Dispensary, ");
        print("Real Student</p>");
        print("<p>(Real student: 1 => a student; 0 => staff, <i>etc.</i>)</p>");
        print("</font>");
 	print("<FORM ENCTYPE='multipart/form-data' METHOD='POST'>");
	print("<INPUT TYPE=file NAME='fupload' size=80>");
	print("<INPUT TYPE=SUBMIT value=Confirm>");
	print("</FORM></TR></TD>");
    } else {
        $file_dir="/home/pazscrip/fileupload";
	if (is_uploaded_file($fpath)) {
            move_uploaded_file($fpath, $file_dir)
		or die("Couldn't copy");
	};
	$fpin=fopen($file_dir,"r");
	$db=new dbabstraction();
	$db->connect() or die ($db->getError()); 
	$student=array();
	$chunk=fgets($fpin, 1024);
	while (! feof($fpin)) {
            $student["StudentSaturnNumber"]=strtok($chunk,",");
            $student["FirstName"]=strtok(",");
	    $student["LastName"]=strtok(",");
	    $student["Username"]=strtok(",");
	    $student["DefaultPW"]=strtok(",");
	    $student["PracticeCode"]=strtok(",");
	    $student["ExamCode"]=strtok(",");
	    $student["CheckingPracticeCode"]=strtok(",");
	    $student["PracticalGroup"]=strtok(",");
	    $student["Seat"]=strtok(",");
	    $student["RealStudent"]=strtok(",");
	    $db->insert("ScripStudent", $student);
	    $chunk=fgets($fpin, 1024);
	};
	fclose($fpin);
        print("<TR><TD><FONT COLOR=FFFFFF>");
	print("File uploaded to student database</P></FONT></TD></TR>");
    }
    print("<TR><TD><FONT COLOR=FFFFFF>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR></TABLE>");
} 

*/


function F1_UploadIPData($HTTP_POST_FILES) {
    global $thecheckIP;

    $file_dir="/home/pazscrip/fileupload";
    Z_DrawHeader();

    if (!isset($_SESSION['thecheckIP'])) {
        $_SESSION['thecheckIP']=1;
        print("<tr><td><font COLOR=FFFFFF>");
        print("<p>File must be comma separated in format:<BR>");
        print("Description (<i>e.g.</i> Dispensary B03), IP Address, Checking (which must be 0 or 1), Host Name (as supplied by IS), Machine Name (<i>i.e.</i> MAC name), Seat Number (<i>e.g.</i> B03).</p></font>");

        print("<FORM ENCTYPE='multipart/form-data' METHOD='POST'>");
        print("<INPUT TYPE=file NAME='fupload' size=80>");
        print("<INPUT TYPE=SUBMIT value=Confirm>");
        print("</FORM></TR></TD>");
    } else {
        foreach ($HTTP_POST_FILES as $file_name=>$file_array){
            $fpath=$file_array['tmp_name'];
            $fname=$file_array['name'];
        };

        if (is_uploaded_file($fpath)) {
            move_uploaded_file($fpath, $file_dir)
            or die("Couldn't copy");
        };

        $fpin=fopen($file_dir,"r");

        $db=new dbabstraction();
        $db->connect() or die ($db->getError()); 

        $IPAddress=array();
        $chunk=fgets($fpin, 1024);
print("<pre>");
        while (! feof($fpin)) {
            $IPAddress["Location"]=strtok($chunk,",");
            $IPAddress["IPAddress"]=strtok(",");
            $IPAddress["Checking"]=strtok(",");
            $IPAddress["HostName"]=strtok(",");
            $IPAddress["MachineName"]=strtok(",");
            $IPAddress["SeatNumber"]=strtok(",");
//print_r($IPAddress);
            $db->insert("ScripIPAddresses", $IPAddress);
            $chunk=fgets($fpin, 1024);
        };

        fclose($fpin);
        print("<TR><TD><FONT COLOR=FFFFFF>");
        print("File uploaded to IP address database</P></FONT></TD></TR>");
    };

    print("<TR><TD><FONT COLOR=FFFFFF>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR></TABLE>");
};

function F2_UploadCDPageData($HTTP_POST_FILES) {
    global $thecheck;

    // $HTTP_POST_FILES is a two-dimensional array. It has one element
    // in the first dimension for each file uploaded.
    // This element has 5 elements of its own
    // (in the second dimension):
    // name e.g. XXX.csv
    // type e.g. application/vnd.ms-excel 
    // tmp_name e.g. /var/tmp/phpq6ayp1
    // error e.g. 0
    // size e.g. 284

    // Therefore, use foreach to read the element(s) in the
    // first dimension (which themselves are 1D arrays) and
    // use name and tmp_name

    foreach ($HTTP_POST_FILES as $file_name=>$file_array) {
        $fpath=$file_array['tmp_name'];
        $fname=$file_array['name']; // I don't think this is used anywhere!
    };

    // See http://www.htmlcodetutorial.com/forms/_INPUT_TYPE_FILE.html
    // for details of how type=file works ...

    Z_DrawHeader();
//    if (!isset($thecheck)) { // BW OLD CODE
    if (strlen($fpath) == 0){ // Is there an uploaded file ready?
//     	$thecheck=1; // BW OLD CODE
    // No uploaded file? We're doing the upload itself ...
        print("<tr><td><FONT COLOR=FFFFFF>");
        print("<p>File must be comma separated in format:<BR>");
        print("Class, Brand, Strength, Form, CD Page</p>");
        print("</FONT>");

        // N.B. the following <form> has no action - in spite of 
        // action being the one required parameter! Must assume
        // that it defauls to the file that is calling it.
 	print("<form ENCTYPE='multipart/form-data' METHOD='POST'>");
	print("<INPUT TYPE=file NAME='fupload' size=80>");
	print("<INPUT TYPE=SUBMIT value=Confirm>");
	print("</form>");
        print("</tr></td>");
    } else { // Second pass to process it ...
    // There is an uploaded file? We're dealing with it ...

        $file_dir="/home/pazscrip/fileupload";

        // Was file with name $fpath uploaded  via HTTP POST
        // If so, move it from where the upload mechanism has left
        // it to somewhere within PAZSCRIP's filespace
        // (as specified by $file_dir) ...
	if (is_uploaded_file($fpath)) {
	    move_uploaded_file($fpath, $file_dir)
		or die("Couldn't copy");
	};
	$fpin=fopen($file_dir,"r"); // Open the file for reading ...

	$db=new dbabstraction();
	$db->connect() or die ($db->getError()); 
	$CDPage=array();
	$chunk=fgets($fpin, 1024); // Read one line (up to 1024 max)
        $chunk = str_replace(",,", ", ,", $chunk);
	while (!feof($fpin)) { // Test for end of file ...
	    $CDPage["Class"]=strtok($chunk,",");
	    $CDPage["Brand"]=strtok(",");
	    $CDPage["Strength"]=strtok(",");
	    $CDPage["Form"]=strtok(",");
	    $CDPage["MinimumBalance"]=strtok(",");
	    $CDPage["MaximumBalance"]=strtok(",");
	    $CDPage["CDPage"]=strtok(",");
	    $db->insert("ScripCDPageTitleAndLabel", $CDPage);
	    $chunk=fgets($fpin, 1024);
            $chunk = str_replace(",,", ", ,", $chunk);
	};
	fclose($fpin);
        print("<TR><TD><FONT COLOR=FFFFFF>");
	print("File uploaded to CD Page Title and Label database");
        print("</p></font></td></tr>");
    };
    print("<TR><TD><FONT COLOR=FFFFFF>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR></TABLE>");
};

function F3_UploadPatientData($HTTP_POST_FILES) {
    global $thecheck;

    // $HTTP_POST_FILES is a two-dimensional array. It has one element
    // in the first dimension. This single element has 5 elements itself
    // (in the second dimension):
    // name e.g. XXX.csv
    // type e.g. application/vnd.ms-excel 
    // tmp_name e.g. /var/tmp/phpq6ayp1
    // error e.g. 0
    // size e.g. 284

    // Therefore, use foreach to read the element(s) in the
    // first dimension (which themselves are 1D arrays) and
    // use name and tmp_name

    foreach ($HTTP_POST_FILES as $file_name=>$file_array) {
        $fpath=$file_array['tmp_name'];
        $fname=$file_array['name']; // I don't think this is used anywhere!
    };

    // See http://www.htmlcodetutorial.com/forms/_INPUT_TYPE_FILE.html
    // for details of how type=file works ...

    Z_DrawHeader();
    if (strlen($fpath) == 0){ // Is there an uploaded file ready?
        print("<tr><td><font color=ffffff>");
        print("<p>File must be comma separated in format:<br>");
        print("First Name, Last Name, Age, ");
        print("Address 1, Address 2, Address 3, Address 4, ");
        print("PMR, NHS Number, Diabetes, Hypertension, Asthma, ");
        print("Hypothyroidism, Epilepsy, Pregnancy, Parkinson's Disease, ");
        print("Eczema or Psoriasis, Allergies, and PMR Notes</p>");
        print("</font>");

        // N.B. the following <form> has no action - in spite of 
        // action being the one required parameter! Must assume
        // that it defauls to the file that is calling it.
 	print("<form ENCTYPE='multipart/form-data' METHOD='POST'>");
	print("<INPUT TYPE=file NAME='fupload' size=80>");
	print("<INPUT TYPE=SUBMIT value='Confirm'>");
	print("</form>");
        print("</tr></td>");
    } else { // Second pass to process it ...

        $file_dir="/home/pazscrip/fileupload";

        // Was file with name $fpath uploaded  via HTTP POST
        // If so, move it from where the upload mechanism has left
        // it to somewhere within PAZSCRIP's filespace
        // (as specified by $file_dir) ...
	if (is_uploaded_file($fpath)) {
	    move_uploaded_file($fpath, $file_dir)
		or die("Couldn't copy");
	};
	$fpin=fopen($file_dir,"r"); // Open the file for reading ...

	$db=new dbabstraction();
	$db->connect() or die ($db->getError()); 
	$PatientPage=array();
	$chunk=fgets($fpin, 1024); // Read one line (up to 1024 max)
        $chunk = str_replace(",,", ", ,", $chunk);
	while (!feof($fpin)) { // Test for end of file ...
	    $PatientPage["FirstName"]=strtok($chunk,",");
	    $PatientPage["LastName"]=strtok(",");
	    $PatientPage["Age"]=strtok(",");
	    $PatientPage["Address1"]=strtok(",");
	    $PatientPage["Address2"]=strtok(",");
	    $PatientPage["Address3"]=strtok(",");
	    $PatientPage["Address4"]=strtok(",");
	    $PatientPage["PMR"]=strtok(",");
	    $PatientPage["NHSNumber"]=strtok(",");
	    $PatientPage["Allergies"]=strtok(",");
	    $PatientPage["PMRNotes"]=strtok(",");
	    $PatientPage["Diabetes"]=strtok(",");
	    $PatientPage["Hypertension"]=strtok(",");
	    $PatientPage["Asthma"]=strtok(",");
	    $PatientPage["Hypothyroidism"]=strtok(",");
	    $PatientPage["Epilepsy"]=strtok(",");
	    $PatientPage["Pregnancy"]=strtok(",");
	    $PatientPage["Parkinsons"]=strtok(",");
	    $PatientPage["EczemaPsoriasis"]=strtok(",");
	    $db->insert("ScripPatient", $PatientPage);
	    $chunk=fgets($fpin, 1024);
            $chunk = str_replace(",,", ", ,", $chunk);
	};
	fclose($fpin);
        print("<TR><TD><FONT COLOR=FFFFFF>");
	print("File uploaded to Patient database");
print('<br>PatientPage["Age"] is '.$PatientPage["Age"]);
        print("</p></font></td></tr>");
    };
    print("<TR><TD><FONT COLOR=FFFFFF>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR></TABLE>");
};

function G_EditPatientData() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all people in patient table and display as a table*/  
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one patient from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=41>");
    print("<tr>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>First Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Age</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Medication</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>NHS Number</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio type=text name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripPatient");
    foreach ($table as $row) {
        $thePatientID=$row['PatientID'];
        $theFirstName=$row['FirstName'];
        $theLastName=$row['LastName'];
        $theAge=$row['Age'];
        $theAddress1=$row['Address1'];
        $theAddress2=$row['Address2'];
        $theAddress3=$row['Address3'];
        $theAddress4=$row['Address4'];
        $theMedication=$row['Medication'];
        $thePatientNHSNumber=$row['NHSNumber'];
	print("<TR><TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$thePatientID></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$thePatientID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theFirstName</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theLastName</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theAge</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theAddress1</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theAddress2</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theAddress3</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theAddress4</FONT></TD>");
	print("<td><font color=FFFFFF>");
	print("$theMedication</font></td>");
	print("<td><font color=FFFFFF>");
	print("$thePatientNHSNumber</font></td>");
	print("</TR>");
    }
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function GA_EditSinglePatient($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Get details of the patient and display in a form ...
    $table=$db->select("ScripPatient",array('PatientID'=>$SID));
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=42>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>First Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Age</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>PMR ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>NHS Number</FONT></TH>");
    print("</TR>");

    if ($SID != '0') {
        foreach ($table as $row) {
            $thePatientID=$row['PatientID'];
            $theFirstName=$row['FirstName'];
            $theLastName=$row['LastName'];
            $theAge=$row['Age'];
            $theAddress1=$row['Address1'];
            $theAddress2=$row['Address2'];
            $theAddress3=$row['Address3'];
            $theAddress4=$row['Address4'];
            $thePMR=$row['PMR'];
            $thePatientNHSNumber=$row['NHSNumber'];
            $theDiabetes = $row['Diabetes'];
            $theHypertension = $row['Hypertension'];
            $theAsthma = $row['Asthma'];
            $theHypothyroidism = $row['Hypothyroidism'];
            $theEpilepsy = $row['Epilepsy'];
            $thePregnancy = $row['Pregnancy'];
            $theParkinsons = $row['Parkinsons'];
            $theEczemaPsoriasis = $row['EczemaPsoriasis'];
            $theAllergies = $row['Allergies'];
            $thePMRNotes = $row['PMRNotes'];
            $theHospitalTTOPatient=$row['HospitalTTOPatient'];
        };
    } else {
        $thePatientID="";
	$theFirstName="";
	$theLastName="";
	$theAge="";
	$theAddress1="";
	$theAddress2="";
	$theAddress3="";
	$theAddress4="";
	$thePMR="";
	$thePatientNHSNumber="";
        $theDiabetes = "";
        $theHypertension = "";
        $theAsthma = "";
        $theHypothyroidism = "";
        $theEpilepsy = "";
        $thePregnancy = "";
        $theParkinsons = "";
        $theEczemaPsoriasis = "";
        $theAllergies = "";
        $thePMRNotes = "";
        $theHospitalTTOPatient="";
    };

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=firstname VALUE=$theFirstName></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=lastname VALUE='$theLastName'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=age VALUE=$theAge></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=pmr VALUE='$thePMR'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=nhsnumber VALUE='$thePatientNHSNumber'></TD>");
    print("</TR>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("</TR>");
	
    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=address1 VALUE='$theAddress1'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=address2 VALUE='$theAddress2'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=address3 VALUE='$theAddress3'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=address4 VALUE='$theAddress4'></TD>");
    print("</tr>");

    print("<tr>");
    print("<TD><INPUT TYPE=checkbox NAME=diabetes ");
    if ($theDiabetes) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Diabetes</b></font></TD>");
    print("<TD><INPUT TYPE=checkbox NAME=hypertension ");
    if ($theHypertension) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Hypertension</b></font></TD>");
    print("<TD><INPUT TYPE=checkbox NAME=asthma ");
    if ($theAsthma) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Asthma</b></font></TD>");
    print("<TD><INPUT TYPE=checkbox NAME=hypothyroidism ");
    if ($theHypothyroidism) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Hypothyroidism</b></font></TD>");
    print("</tr>");

    print("<tr>");
    print("<TD><INPUT TYPE=checkbox NAME=epilepsy ");
    if ($theEpilepsy) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Epilepsy</b></font></TD>");
    print("<TD><INPUT TYPE=checkbox NAME=pregnancy ");
    if ($thePregnancy) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Pregnancy</b></font></TD>");
    print("<TD><INPUT TYPE=checkbox NAME=parkinsons ");
    if ($theParkinsons) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Parkinson's Disease</b></font></TD>");
    print("<TD><INPUT TYPE=checkbox NAME=eczemapsoriasis ");
    if ($theEczemaPsoriasis) { print("checked "); };
    print("VALUE='1'>");
    print("<FONT COLOR=FFFFFF><b> Eczema or Psoriasis</b></font></TD>");
    print("</tr>");

    print("<tr>");
    print("<td colspan=2><FONT COLOR=FFFFFF>");
    print("<b>Allergies</b></font><br>");
    print("<textarea name=allergies rows=5 cols=30>");
    print("$theAllergies</textarea>");
    print("</td>");
    print("<td colspan=2><FONT COLOR=FFFFFF>");
    print("<b>Additonal Notes</b></font><br>");
    print("<textarea name=pmrnotes rows=5 cols=30>");
    print("$thePMRNotes</textarea>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th>");
    print("<font color='#ffffff'>Hospital TTO Patient</font>");
    print("</th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input name='theHospitalTTOPatient' value='$theHospitalTTOPatient'>");
    print("</td>");
    print("</tr>");

    print("<TR><TD colspan=8 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");

    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function GB_UpdatePatient($SID, $thefirstname, $thelastname,
 		$theage, $theaddress1, $theaddress2, $theaddress3, 	
		$theaddress4, $thePMR, $thePatientNHSNumber,
                $theDiabetes, $theHypertension, $theAsthma, 
                $theHypothyroidism, $theEpilepsy, $thePregnancy,
                $theParkinsons, $theEczemaPsoriasis, 
                $theAllergies, $thePMRNotes, $theHospitalTTOPatient) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the patient ...
    $patientdata = array(
        "FirstName" => $thefirstname,
        "LastName" => $thelastname,
        "Age" => $theage,
        "Address1" => $theaddress1,
        "Address2" => $theaddress2,
        "Address3" => $theaddress3,
        "Address4" => $theaddress4,
        "PMR" => $thePMR,
        "NHSNumber" => $thePatientNHSNumber,
        "Diabetes" => $theDiabetes,
        "Hypertension" => $theHypertension,
        "Asthma" => $theAsthma, 
        "Hypothyroidism" => $theHypothyroidism,
        "Epilepsy" => $theEpilepsy,
        "Pregnancy" => $thePregnancy,
        "Parkinsons" => $theParkinsons,
        "EczemaPsoriasis" => $theEczemaPsoriasis, 
        "Allergies" => $theAllergies,
        "PMRNotes" => $thePMRNotes,
        "HospitalTTOPatient"=>$theHospitalTTOPatient
    );
    $condition=array('PatientID'=>$SID);

    if ($SID != '0') {
        $table=$db->update("ScripPatient",$patientdata,$condition);         
    } else {
        $table=$db->insert("ScripPatient",$patientdata);         
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Patient updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function G_EditPMRData() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all lines in PMR table and display ...
    Z_DrawHeader();
    print("<tr><td colspan=2>");
    print("<FONT COLOR=FFFFFF>");
    print("<h2>Select one PMR line from list");
    print(" and click the submit button to edit the details</h2>");
    print("</font>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=46>");
    print("<tr>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>PMR ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>NSF</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Quantity</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Dose</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Frequency</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Date from To-day</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Number</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio type=text name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripPMR");
    foreach ($table as $row) {
        $theID=$row['ID'];
        $thePMRID=$row['PMRID'];
        $theNameStrengthForm=$row['NameStrengthForm'];
        $theQuantity=$row['Quantity'];
        $theDose=$row['Dose'];
        $theFrequency=$row['Frequency'];
        $theDaysBeforeToday=$row['DaysBeforeToday'];
        $theNumber=$row['Number'];

	print("<tr>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$thePMRID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theNameStrengthForm</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theQuantity</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theDose</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theFrequency</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theDaysBeforeToday</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theNumber</FONT></TD>");
	print("</tr>");
    };
    print("<TR><TD colspan='9' align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function GA_EditSinglePMR($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Get details of the patient and display in a form
    $table=$db->select("ScripPMR",array('ID'=>$SID));
    Z_DrawHeader();
    print("<tr><td colspan=2>");
    print("<FONT COLOR=FFFFFF>");
    print("<br>Date from To-day - How long ago was the drug ");
    print("most recently dispensed?<br>");
    print("Number - How many times has the drug been dispensed? ");
    print("(Not required if Frequency is One-off.)<br><br>");
    print("</font>");
    print("</TD></TR>");
    print("<tr>");
    print("<td colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=47>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>PMR ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name, Strength and Form</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Quantity</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Dose</FONT></TH>");
    print("</tr>");

    if ($SID != '0') {
        foreach ($table as $row) {
            $theID=$row['ID'];
            $thePMRID=$row['PMRID'];
            $theNameStrengthForm=$row['NameStrengthForm'];
            $theQuantity=$row['Quantity'];
            $theDose=$row['Dose'];
            $theFrequency=$row['Frequency'];
            $theDaysBeforeToday=$row['DaysBeforeToday'];
            $theNumber=$row['Number'];
        };
    } else {
        $theID="";
        $thePMRID="";
        $theNameStrengthForm="";
        $theQuantity="";
        $theDose="";
        $theFrequency="";
        $theDaysBeforeToday="";
        $theNumber="";
    };

    print("<tr>");
    print("<td><FONT COLOR=FFFFFF>$theID</td>");
    print("<td><INPUT TYPE=TEXT NAME=pmrid VALUE=$thePMRID></TD>");
    print("<td><INPUT TYPE=TEXT NAME=namestrengthform ");
    print("VALUE='$theNameStrengthForm' size='40'></TD>");
    print("<td><INPUT TYPE=TEXT NAME=quantity VALUE=$theQuantity></TD>");
    print("<td><INPUT TYPE=TEXT NAME=dose VALUE='$theDose'></TD>");
    print("</tr>");

    print("<tr><td colspan='5'>&nbsp;</td>");
    print("</tr>");



    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Frequency</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Date from Today</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Number</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    $tableF=$db->select("ScripFrequencyCodes");
    print("<select name='frequency'>");
    foreach ($tableF as $rowF) {
        $theCode = $rowF['Code'];
        $theExplanation = $rowF['Explanation'];
        print("<option value='$theCode'");
        if ($theCode == $theFrequency) { print(" selected"); };
        print(">$theExplanation");
    };
    print("</select>");
    print("</td>");


/*
    print("<option value='1' ");
    if ($theFrequency == '1') { print("selected"); };
    print(">One off");
    print("<option value='w' ");
    if ($theFrequency == 'w') { print("selected"); };
    print(">Weekly (7 days)");
    print("<option value='m' ");
    if ($theFrequency == 'm') { print("selected"); };
    print(">Monthly (28 days)");
    print("</select>");
    print("</td>");
*/

    print("<td><input type='text' name=daysbeforetoday ");
    print("value='$theDaysBeforeToday'></td>");
    print("<td><input type='text' name=number ");
    print("value='$theNumber'></td>");
    print("</tr>");

//    print("<tr>");
//    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
//    print("</tr>");
	
//    print("<tr>");
//    print("<TD><INPUT TYPE=TEXT NAME=address1 VALUE='$theAddress1'></TD>");
//    print("<TD><INPUT TYPE=TEXT NAME=address2 VALUE='$theAddress2'></TD>");
//    print("<TD><INPUT TYPE=TEXT NAME=address3 VALUE='$theAddress3'></TD>");
//    print("<TD><INPUT TYPE=TEXT NAME=address4 VALUE='$theAddress4'></TD>");
//    print("</tr>");

    print("<TR><TD colspan=8 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");

    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function GB_UpdatePMR($SID, $thePMRID,
	$theNameStrengthForm, $theQuantity, $theDose,
	$theFrequency, $theDaysBeforeToday, $theNumber) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the PMR ...
    $data = array(
	"PMRID" => $thePMRID,
	"NameStrengthForm" => $theNameStrengthForm,
	"Quantity" => $theQuantity,
	"Dose" => $theDose,
	"Frequency" => $theFrequency,
	"DaysBeforeToday" => $theDaysBeforeToday,
	"Number" => $theNumber
	);

    if ($SID != '0') {
        $condition=array('ID'=>$SID);
        $table=$db->update("ScripPMR",$data,$condition);         
    } else {
        $table=$db->insert("ScripPMR",$data);         
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>PMR line updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}

function G_EditInteractionMA() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all lines in interaction MA table and display ...
    Z_DrawHeader();
    print("<tr><td colspan=2>");
    print("<FONT COLOR=FFFFFF>");
    print("<h2>Select one interaction model answer from list");
    print(" and click the submit button to edit the details</h2>");
    print("</font>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=56>");
    print("<tr>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Case ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>With what</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>PMR Number</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Severity</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Suggestion</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Delete Which</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio type=text name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripInteractionMA");
    foreach ($table as $row) {
        $theID=$row['ID'];
        $theCaseID=$row['CaseID'];
        $theWithWhat=$row['WithWhat'];
        $thePMRNumber=$row['PMRNumber'];
        $theSeverity=$row['Severity'];
        $theSuggestion=$row['Suggestion'];
        $theDeleteWhich=$row['DeleteWhich'];

	print("<tr>");
        print("<td><font COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>");
	print("<td><font COLOR=FFFFFF>");
	print("$theID</font></TD>");
	print("<TD><font COLOR=FFFFFF>");
	print("$theCaseID</font></TD>");
	print("<TD><font COLOR=FFFFFF>");
	print("$theWithWhat</font></TD>");
	print("<TD><font COLOR=FFFFFF>");
	print("$thePMRNumber</font></TD>");
	print("<TD><font COLOR=FFFFFF>");
	print("$theSeverity</font></TD>");
	print("<TD><font COLOR=FFFFFF>");
	print("$theSuggestion</font></TD>");
	print("<TD><font COLOR=FFFFFF>");
	print("$theDeleteWhich</font></TD>");
	print("</tr>");
    };
    print("<TR><TD colspan='9' align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function GA_EditSingleInteractionMA($SID, $theNewCaseID, 
                      $theNewWithWhat, $theNewPMRNumber, $theNewSeverity,
                      $theNewSuggestion, $theNewDeleteWhich,
                      $theNewCommentI, $theNewCommentS){
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Get details of the patient and display in a form
    $table=$db->select("ScripInteractionMA",array('ID'=>$SID));
    Z_DrawHeader();
    print("<tr><td colspan=2>");
    print("<font color=ffffff>");
    print("<br>With What - 1: Item 1 and PMR; 2: Item 2 and ");
    print("PMR; 1 and 2: Item 1 and Item 2<br>");
    print("</font>");
    print("</td></tr>");
    print("<tr>");
    print("<td colspan=2 align=center><table border='1'>");
    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name=tab value=57>");
    print("<input type='hidden' name=ID value=$SID>");

    foreach ($table as $row) {
        $theID=$row['ID'];
        $theCaseID=$row['CaseID'];
        $theWithWhat=$row['WithWhat'];
        $thePMRNumber=$row['PMRNumber'];
        $theSeverity=$row['Severity'];
        $theSuggestion=$row['Suggestion'];
        $theDeleteWhich=$row['DeleteWhich'];
        $theCommentI=$row['IntFB'];
    };

    // Logic: Read table and put case ID in $theCaseID; if $theNewCaseID
    // is different to the previous case ID (and is not nul), over-write
    // $theCaseID and exit; if the previous value is set, over-write
    // $theCaseID. (Otherwise $theCaseID is left at the d/b value.)

    if ($theNewCaseID && ($theNewCaseID != $_SESSION['caseID'])) {
        $theCaseID = $theNewCaseID;
    } else if ($_SESSION['caseID']) {
        $theCaseID = $_SESSION['caseID'];
    };

    // Nested ternary operators frowned upon ...
    // Case ID changed? 
    if ($theNewCaseID != $_SESSION['caseID']) { // Yes - cancel value of WithWhat
        $theWithWhat = "";
    } else if ($theNewWithWhat) { // No - Value of WithWhat changed?
        $theWithWhat = $theNewWithWhat; // Yes - take it
    };

    $_SESSION['caseID'] = $theCaseID; //Record for future use ...

    // Find case in ScripsScrips to locate patient problem ...
    $condition = array ('Letter'=>$theCaseID);
    $table = $db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $patientprob = $row['PatientProblemID'];
        $dpID2 = $row['DrugProblemID2'];
    };

    // Find patient problem in ScripPatientProblem 
    // to locate patient ...
    $condition = array ('PatientProblemID'=>$patientprob);
    $table = $db->select("ScripPatientProblem", $condition);
    foreach ($table as $row) {
        $patientID = $row['PatientID'];
    };

    // Find patient in ScripPatient 
    // to locate PMR ...
    $condition = array ('PatientID'=>$patientID);
    $table = $db->select("ScripPatient", $condition);
    foreach ($table as $row) {
        $pmr = $row['PMR'];
    };

    print("<tr>");
    print("<td colspan='2'><FONT COLOR=FFFFFF>For confirmation:</td>");
    print("</tr>");
    print("<tr>");
    print("<th><font color=FFFFFF>ID</FONT></th>");
    print("<td><FONT COLOR=FFFFFF>$theID</td>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'><FONT COLOR=FFFFFF>For alteration:</td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color=FFFFFF>Case ID</FONT></th>");
    print("<td>");
    print("<select name='caseid' ");
    print("onChange='javascript:document.forms[0].submit()'>");

    print("<option value=''>Select from ...");
    $table = $db->select("ScripsScrips");
    foreach ($table as $row){
        $aCaseID = $row['Letter'];
        print("<option value='$aCaseID'");
        if ($theCaseID == $aCaseID) { print(" selected"); };
        print(">$aCaseID");
    };

    print("</select>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color=FFFFFF>With What?</FONT></th>");
    print("<td>");
    print("<select name='withwhat' ");
    print("onChange='javascript:document.forms[0].submit()'>");
    print("<option value='n'");
    if ($theWithWhat == 'n') { print(" selected"); };
    print(">(None)");
    print("<option value='1'");
    if ($theWithWhat == '1') { print(" selected"); };
    print(">PMR (below) with Item 1");

    if ($dpID2) { // Options only for double-drug script ...
        print("<option value='2'");
        if ($theWithWhat == '2') { print(" selected"); };
        print(">PMR (below) with Item 2");
        print("<option value='12'");
        if ($theWithWhat == '12') { print(" selected"); };
        print(">Item 1 with Item 2");
    };
    print("</select></TD></tr>");

    print("<tr>");
    print("<th><font color=FFFFFF>PMR Number</FONT></th>");
    print("<td>");
    if ($theWithWhat == '1' || $theWithWhat == '2') {
        print("<select name='pmrnumber'>");
        print("<option value=''>Select from ...");
        // Find all PMR entries in ScripPMR ...
//        $condition = array ('PMRID'=>$pmr);
//        $table = $db->select("ScripPMR", $condition);
        $table = $db->select("ScripPMR");
        foreach ($table as $row) {
            $id = $row['ID'];
            $nsf = $row['NameStrengthForm'];
            print("<option value='$id'");
            if ($thePMRNumber == $id) { print(" selected"); };
            print(">$nsf ($id)\n");
        };
        print("</select>");
    } else {
        print("&nbsp;");
//        $thePMRNumber = 0;
    };
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color=FFFFFF>Severity</FONT></th>");
    print("<td><select name='severity'>");
    print("<option value=''>Select from ...");
    print("<option value='m'");
    if ($theSeverity == 'm') { print(" selected"); };
    print(">minor");
    print("<option value='M'");
    if ($theSeverity == 'M') { print(" selected"); };
    print(">MAJOR");
    print("</select></td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color=FFFFFF>Suggestion</FONT></th>");
    print("<td><select name='suggestion'>");
    print("<option value=''>Select from ...");
    $table = $db->select("ScripSuggestion");
    foreach ($table as $row){
        $code = $row['Code'];
        $suggestion = $row['Suggestion'];
        print("<option value='$code'");
        if ($theSuggestion == $code) { print(" selected"); };
        print(">$suggestion ($code)");
    };
    print("</select></td>");
    print("</tr>");

    print("<tr>");
    print("<td><font color='#ffffff'><b>Change which drug?</b></font>");
    print("</td>");
    print("<td>");
    print("<select name=deletewhich>");
    print("<option value=''>Select from ...");
    print("<option value='1'>1");
    print("<option value='2'>2");
    print("</td>");
    print("</tr>");

    print("<tr><td colspan='2'><font color='#ffffff'>Comments</font>");
    print("</td></tr>");

    print("<tr>");
    print("<th><font color=ffffff>On Interaction</font></th>");
    print("<td><textarea name='commenti'>");
    if ($theNewCommentI) {
        print("$theNewCommentI");
    } else {
        print("$theCommentI");
    };
    print("</textarea></td>");
    print("</tr>");

//    print("<tr>");
//    print("<th><font color=ffffff>On Suggested Change</font></th>");
//    print("<td><textarea name='comments'>");
//    print("$theNewCommentS</textarea></td>");
//    print("</tr>");

    print("<tr><td colspan='5'>&nbsp;</td></tr>");

    print("<tr><td colspan=8 align=center>");
    print("<input type='submit' name='Update' value='Update'>");
    print("</td></tr>");

    print("</form></table></td></tr>");
    print("<tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</A>");
};

function GB_UpdateInteractionMA($SID, $theCaseID,
	$theWithWhat, $thePMRNumber,
	$theSeverity, $theSuggestion,
        $theDeleteWhich,
        $theCommentI) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the PMR ...
    $data = array(
	"CaseID" => $theCaseID,
	"WithWhat" => $theWithWhat,
	"PMRNumber" => $thePMRNumber,
	"Severity" => $theSeverity,
	"Suggestion" => $theSuggestion,
        "DeleteWhich" => $theDeleteWhich,
        "IntFB" => $theCommentI
	);

    if ($SID != '0') {
        $condition=array('ID'=>$SID);
        $table=$db->update("ScripInteractionMA",$data,$condition);         
    } else {
        $table=$db->insert("ScripInteractionMA",$data);         
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Interaction MA updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}

function GZ_EditResitData() {
    global $maxResits;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all students in resit table and display */  
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one student from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=910>");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Student</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R4</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R5</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R6</FONT></TH>");
    print("</TR>");

    print("<tr>");
    print("<td><input type=radio type=text name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>New</font></td>");
    print("</tr>");


    $table=$db->select("ScripResit");
    foreach ($table as $row) {
        $theEntryID=$row['ID'];
        $theStudentID=$row['StudentID'];
        for ($i=0; $i<$maxResits; $i++) {
            $R[$i]=$row['R'.($i+1)]; // e.g. $R[1]=$row['R2']
            $RStart[$i]=$row['R'.($i+1).'Start'];
        };

	print("<TR><TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theEntryID></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theEntryID $x</FONT></TD>");

        // Use the student ID to read real name ...
        $scondition=array('StudentID'=>$theStudentID);
        $stable=$db->select("ScripStudent",$scondition);
        foreach ($stable as $srow) {
            $theFirstName=$srow['FirstName'];
            $theLastName=$srow['LastName'];
        };

	print("<TD><FONT COLOR=FFFFFF>");
	print("$theLastName, $theFirstName</FONT></TD>");

        for ($i=0; $i<$maxResits; $i++) {
            print("<td><FONT COLOR=FFFFFF>");
            if ($RStart[$i]) { print("<b>"); };
            print($R[$i]);
            if ($RStart[$i]) { print("</b>"); };
            print("</FONT></TD>");
        };
	print("</tr>");
    };
    print("<tr><td colspan=9 align=center>");
    print("<input type=submit value='Confirm'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a></td></tr></table>");
};

function GZA_EditSingleResitStudent($EntryID) {
    global $maxResits;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
//  Get details of the student and display in a form ...
    $table=$db->select("ScripResit", array('ID'=>$EntryID));
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=920>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$EntryID>");

    print("<tr>");
    // Column headings ...
    print("<TH><FONT COLOR=FFFFFF>Student</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R4</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R5</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>R6</FONT></TH>");
    print("</tr>");

    // Get the names of all the students ...
    $stable=$db->select("ScripStudent");
    foreach ($stable as $srow) {
        $allStudents[]=$srow['StudentID'];
    };

    // Get the names of the students taking re-sits ...
    $stable=$db->select("ScripResit");
    foreach ($stable as $srow) {
        $resitStudents[]=$srow['StudentID'];
    };


    // This is the ideal way to do it
    // Now take the difference i.e. students not yet with an entry
/*
    if (count($resitStudents)){
        $possibles = array_diff($allStudents, $resitStudents);
    } else {
        $possibles = $allStudents;
    };
*/

    // ... however, there is some suggestion that the array_diff function doesn't work ...

    // We are going to store the possibles i.e. those students not yet in resits in $possibles
    for ($i = 0; $i < count($allStudents); $i++){ // ... just a duplicate
        $possibles[$i] = $allStudents[$i];
    };

    for ($j = 0; $j < count($resitStudents); $j++){
        for ($i = 0; $i < count($possibles); $i++){
            if ($resitStudents[$j] == $possibles[$i]){
                array_splice($possibles, $i, 1); break;
            };
        };
    };

//print("There are ".count($allStudents)." ALL students<br>");
//print("There are ".count($resitStudents)." ALL students<br>");
//print("There are ".count($possibles)." ALL students<br>");

    // Students not yet in the re-sit table are in $possibles and form the drop-down list ...
    for ($k = 0; $k < count($possibles); $k++){
        $xcondition=array('StudentID'=>$possibles[$k]);
        $xtable=$db->select("ScripStudent", $xcondition);
        foreach ($xtable as $xrow) {
            $arrayFirstName[] = $xrow['FirstName']; 
            $arrayLastName[] = $xrow['LastName']; 
            $arrayID[] = $xrow['StudentID']; 
        };
    };

    // We would like to sort regardless of case, first on last name and then on first name ...

    $arrayLastName_lowercase = array_map('strtolower', $arrayLastName);
    $arrayFirstName_lowercase = array_map('strtolower', $arrayFirstName);

//    array_multisort($arrayLastName, $arrayFirstName, $arrayID); 
    array_multisort($arrayLastName_lowercase, $arrayFirstName_lowercase, $arrayLastName, $arrayFirstName, $arrayID); 





    print("<tr>");
    if ($EntryID != '0') {
        foreach ($table as $row) {
            $theEntryID=$row['ID'];
            $theStudentID=$row['StudentID'];

            // Use the student ID to elements of the name ...
            $scondition=array('StudentID'=>$theStudentID);
            $stable=$db->select("ScripStudent", $scondition);
            foreach ($stable as $srow) {
                $theFirstName=$srow['FirstName'];
                $theLastName=$srow['LastName'];
            };
            $thename=$theLastName.", ".$theFirstName;

            // Offer the names from $possibles only to administrator ...
            // Begin the drop-down list:
            print("<td><select name=studentid>");
            print("<option value='0'>"); // ... first, the null option
            print("<option value='$theStudentID' selected> $thename ($theStudentID)"); // ... next, the current name

            // ... then all the other names ... loop over the possibilities
            for ($k = 0; $k < count($possibles); $k++){
                print("<option value=$arrayID[$k]> $arrayLastName[$k], $arrayFirstName[$k] ($arrayID[$k])");
            }; 


/*
            for ($k=0; $k<count($allStudents); $k++) {
                // (Counting is based on parent array)
                if ($possibles[$k]) { // Only deal with non-null elements ...
                    $scondition=array('StudentID'=>$possibles[$k]);
                    $stable=$db->select("ScripStudent", $scondition);
                    foreach ($stable as $srow) {
                        $aFirstName=$srow['FirstName'];
                        $aLastName=$srow['LastName'];
                        $anID=$srow['StudentID'];
                    };
                    print("<option value=$anID> ");
                    print("$aLastName, $aFirstName ($anID)");
                };
            };
*/

            // Close the dropdown box ...
            print("</select></td>\n");


//            print("<TD><INPUT TYPE=TEXT NAME=student ");
//            print("VALUE='$thename' readonly></TD>");

                for ($i=0; $i<$maxResits; $i++) {
                    $R[$i]=$row['R'.($i+1)]; // e.g. $R[1]=$row['R2']
                    $RStart[$i]=$row['R'.($i+1).'Start'];
                };
            };
        } else { // Administrator wants a new student 
            $theEntryID="";
            $theStudentID="";




        // Offer the names from $possibles only to administrator ...
        print("<td><select name=studentid>");
        print("<option value='0' selected>"); // ... null option


        // All the other names ... loop over the possibilities
        for ($k = 0; $k < count($arrayID); $k++){
            print("<option value=$arrayID[$k]> $arrayLastName[$k], $arrayFirstName[$k] ($arrayID[$k])");
     
   };


/*
        for ($k=0; $k<count($allStudents); $k++) {
            // (Counting is based on parent array)
            if ($possibles[$k]) { // Only deal with non-null elements ...
                $scondition=array('StudentID'=>$possibles[$k]);
                $stable=$db->select("ScripStudent", $scondition);
                foreach ($stable as $srow) {
                    $aFirstName=$srow['FirstName'];
                    $aLastName=$srow['LastName'];
                    $anID=$srow['StudentID'];
                };
                print("<option value=$anID> ");
                print("$aLastName, $aFirstName ($anID)");
            };
        };
*/



        print("</select></td>\n");

/* Old code - offer all names regardless ...
        // Offer names to administrator ...
        print("<td><select name=studentid>");
        print("<option value='0' selected>");
        $stable=$db->select("ScripStudent");
        foreach ($stable as $srow) {
            $aFirstName=$srow['FirstName'];
            $aLastName=$srow['LastName'];
            $anID=$srow['StudentID'];
            print("<OPTION VALUE=$anID ");
            print("> $aLastName, $aFirstName ($anID)");
        };
        print("</select></td>\n");
*/

        for ($i=0; $i<$maxResits; $i++) {
            $R[$i]=""; 
            $RStart[$i]="";
        };
    };

    for ($i=0; $i<$maxResits; $i++) {
        print("<td><input type=text size='12'");
        $iplus1 = $i + 1;
        print(" name='R$iplus1' value=$R[$i]></td>");
        print("<input type=hidden ");
        print("name='RStart$i' value=$RStart[$i]>");
    };
    print("</tr>");

    print("<tr>");
    print("<td><font color=ffffff>Time</font></td>");
    for ($i=0; $i<$maxResits; $i++) {
        print("<td><font color=ffffff>");
        if ($RStart[$i]) { // Time ...
            print (date("H:i", $RStart[$i]));
        } else {
            print ("&nbsp;");
        };
        print("</font></td>");
    };
    print("</tr>");

    print("<tr>");
    print("<td><font color=ffffff>Date</font></td>");
    for ($i=0; $i<$maxResits; $i++) {
        print("<td><font color=ffffff>");
        if ($RStart[$i]) { // Date ...
            print (date("M j", $RStart[$i]));
        } else {
            print ("&nbsp;");
        };
        print("</font></td>");
    };
    print("</tr>");

    print("<tr>");
    print("<td><font color=ffffff>Advance by ... (minutes)</font></td>");
    for ($i=0; $i<$maxResits; $i++) {
        print("<td>");
        if ($RStart[$i]) {
            print("<input type=text size='12'");
            $iplus1 = $i+1;
            print(" name='advance$iplus1'");
        } else {
            print("&nbsp;");
        };
        print("</td>");
    };
    print("</tr>");


    print("<tr><td colspan=9 align=center>");
    print("<input type=submit value='Update'></td></tr>");
    print("<tr><td colspan=7>&nbsp;</td></tr>");

    print("</form>");

/*
// Begin the delete code ...
    if ($EntryID != '0') {
        print("<FORM ACTION=scripadmin.php METHOD=POST>");
        print("<INPUT TYPE=HIDDEN NAME=tab value=930>");
        print("<INPUT TYPE=HIDDEN NAME=ID value=$EntryID>");

        print("<tr>");
        print("<TH><FONT COLOR=FFFFFF>Student</FONT></TH>");
        print("<TH colspan=6><FONT COLOR=FFFFFF>&nbsp;</FONT></TH>");
        print("</tr>");

        print("<tr>");
        foreach ($table as $row) {
            $theEntryID=$row['ID'];
            $theStudentID=$row['StudentID'];
            print("<INPUT TYPE=HIDDEN NAME=studentid value=$theStudentID>");

            // Use the student ID to read and show name ...
            $scondition=array('StudentID'=>$theStudentID);
            $stable=$db->select("ScripStudent",$scondition);
            foreach ($stable as $srow) {
                $theFirstName=$srow['FirstName'];
                $theLastName=$srow['LastName'];
            };
            $name=$theLastName.", ".$theFirstName;
            print("<TD><INPUT TYPE=TEXT NAME=student ");
            print("VALUE='$name' readonly></TD>");

            for ($i=0; $i<$maxResits; $i++) {
                $R[$i]=$row['R'.($i+1)]; // e.g. $R[1]=$row['R2']
                $RStart[$i]=$row['R'.($i+1).'Start'];
            };
        };

        print("</tr>");

        print("<tr><td colspan=9 align=center>");
        print("<input type=submit value='Delete'></td></tr>");

        print("</form>");
    };
*/

    print("</table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function GZB_UpdateResitStudent($EntryID, $anID,
	$R1, $R2, $R3, $R4, $R5, $R6, $RStart) {
    global $maxResits;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the student */  
   $resitdata= array(
        "StudentID"=>$anID,
	"R1" => $R1, "R2" => $R2, "R3" => $R3, 
	"R4" => $R4, "R5" => $R5, "R6" => $R6,
	"R1Start" => $RStart[0], "R2Start" => $RStart[1],
        "R3Start" => $RStart[2], "R4Start" => $RStart[3],
        "R5Start" => $RStart[4], "R6Start" => $RStart[5]);
    $condition=array('ID'=>$EntryID);

    if ($EntryID) {
        if ($anID) { // Amend details of an existing entry ...
            $table=$db->update("ScripResit",$resitdata,$condition);         
        } else { // Delete an existing entry ...
            $condition=array('ID'=>$EntryID);
            $table=$db->delete("ScripResit",$condition);         
        };
    } else { // Insert a new entry ...
        $table=$db->insert("ScripResit",$resitdata);         
    };


/*
    if ($EntryID && $anID) {
        $table=$db->update("ScripResit",$resitdata,$condition);         
    } else {
        // For new entry, include the student's ID ...
//        $resitdata=array_merge($resitdata, array("StudentID"=>$anID));
        $table=$db->insert("ScripResit",$resitdata);         
    };

    if ($EntryID && !$anID) { // ... a substantive entry to become a null entry
        // ... means delete
        $condition=array('StudentID'=>$anID);
        $table=$db->delete("ScripResit",$condition);         
    };
*/

    Z_DrawHeader();
    print("<tr><td colspan=2><FONT COLOR=FFFFFF>Re-sit record updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</A>");
    print("</td></tr></table>");
};

function GZC_DeleteResitStudent($anID) {
    global $maxResits;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    if ($EntryID != '0') {
        $condition=array('StudentID'=>$anID);
        $table=$db->delete("ScripResit",$condition);         
    };
    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Entry deleted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};



function GG_EditModeratorData() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one moderator from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=741>");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>First Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Username</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Password</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Authority</FONT></TH>");
    print("</TR>");

    print("<tr>");
    print("<td><input type=radio type=text name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripModerator");
    foreach ($table as $row) {
        $theModeratorID=$row['ModeratorID'];
	$theFirstName=$row['FirstName'];
	$theLastName=$row['LastName'];
	$theUserName=$row['UserName'];
	$thePassword=$row['Password'];
	$theRealModerator=$row['RealModerator'];
	print("<TR><TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theModeratorID></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theModeratorID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theFirstName</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theLastName</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theUserName</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$thePassword</FONT></TD>");
	print("<TD>");
        print("<FONT COLOR=FFFFFF>");
	if ($theRealModerator == '0') { print("Starting only"); };
	if ($theRealModerator == '1') { print("Full"); };
        print("</FONT>");
        print("</TD>");
	print("</TR>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function GGA_EditSingleModerator($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    $table=$db->select("ScripModerator",array('ModeratorID'=>$SID));
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=742>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>First Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Username</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Password</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Authority</FONT></TH>");
    print("</TR>");

    if ($SID != '0') {
        foreach ($table as $row) {
            $theModeratorID=$row['ModeratorID'];
            $theFirstName=$row['FirstName'];
            $theLastName=$row['LastName'];
            $theUserName=$row['UserName'];
            $thePassword=$row['Password'];
            $theRealModerator = $row['RealModerator'];
        };
    } else {
        $theModeratorID="";
        $theFirstName="";
        $theLastName="";
        $theUserName="";
        $thePassword="";
        $theRealModerator = "";
    };

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=firstname VALUE=$theFirstName></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=lastname VALUE='$theLastName'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=username VALUE=$theUserName></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=password VALUE='$thePassword'></TD>");

    print("<td>");
    print("<select name='realmoderator'>");
    print("<option value=''>Select from ...");
    print("<option value='1' ");
    if ($theRealModerator == '1') { print("selected"); };
    print(">Full moderating authority");
    print("<option value='0' ");
    if ($theRealModerator == '0') { print("selected"); };
    print(">Can start exercises only");
    print("</select>");
    print("</td>");
    print("</tr>");

    print("<TR><TD colspan=8 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");

    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function GGB_UpdateModerator($SID, $thefirstname, $thelastname,
	$theUserName, $thePassword, $theRealModerator) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

   $data= array(
	"FirstName" => $thefirstname,
	"LastName" => $thelastname,
	"UserName" => $theUserName,
	"Password" => $thePassword,
        "RealModerator" => $theRealModerator);
    $condition=array('ModeratorID'=>$SID);

    if ($SID != '0') {
        $table=$db->update("ScripModerator",$data,$condition);         
    } else {
        $table=$db->insert("ScripModerator",$data);         
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Moderator updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function H_EditPrescriberData() {
    // Edit data for prescribers:

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all people in prescriber table and display as a table
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one prescriber from the list and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=51>");

    print("<TR>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Qualification</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Practice ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Beep Number</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>NHS Number</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Writing Exemption</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Signature File</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Secondary Prescriber ID</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<TD><FONT COLOR=FFFFFF>");
    print("<INPUT TYPE=RADIO NAME=ID VALUE='0' checked></TD>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripPrescriber");
    foreach ($table as $row) {
        $thePrescriberID=$row['PrescriberID'];
	$theName=$row['Name'];
	$theQual=$row['Qual'];
	$thePracticeID=$row['PracticeID'];
	$theBeepNumber=$row['BeepNumber'];
	$theNHSNumber=$row['NHSNumber'];
	$theWritingExemption=$row['WritingExemption'];
	$theSigFileName=$row['SigFileName'];
	$theSecondaryPrescriberID=$row['SecondaryPrescriberID'];
	print("<TR>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$thePrescriberID></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thePrescriberID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theName</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theQual</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thePracticeID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theBeepNumber</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theNHSNumber</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theWritingExemption</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theSigFileName</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theSecondaryPrescriberID</FONT></TD>");
	print("</TR>");
    }
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function HA_EditSinglePrescriber($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the prescriber and display in a form
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=52>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");
    print("<TH><FONT COLOR=FFFFFF>Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Qual</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Practice</FONT></TH>");
    print("<th><font color=FFFFFF>Beep Number</font></th>");
    print("</TR>");

    if ($SID!='0') {
        $table=$db->select("ScripPrescriber",array('PrescriberID'=>$SID));
        foreach ($table as $row) {
            $thePrescriberID=$row['PrescriberID'];
            $theName=$row['Name'];
            $theQual=$row['Qual'];
            $thePracticeID=$row['PracticeID'];
            $theBeepNumber=$row['BeepNumber'];
            $theNHSNumber=$row['NHSNumber'];
            $theWritingExemption=$row['WritingExemption'];
            $theSigFileName=$row['SigFileName'];
            $theSecondaryPrescriberID=$row['SecondaryPrescriberID'];
            $thePrivateCD=$row['PrivateCD'];
        };
    } else {
        $thePrescriberID="";
	$theName="";
	$theQual="";
	$thePracticeID="0";
	$theBeepNumber="";
	$theNHSNumber="";
	$theWritingExemption="";
	$theSigFileName="";
        $theSecondaryPrescriberID="";
        $thePrivateCD="";
    };

    print("<tr>");
    print("<td><input type=text name=name value='$theName'");
    print(" size='20'></td>");
    print("<td><input type=text name=qual value='$theQual'");
    print(" size='20'></td>");

    print("<td><select name=practiceid>");
    print("<option value='0'");
    if ($thePracticeID=="0") { print(" selected"); };
    print(">");
    $ptable=$db->select("ScripPractice");
    foreach ($ptable as $prow) {
        $showPracticeID=$prow['PracticeID'];
	$theAddress1=$prow['Address1'];
	$theAddress2=$prow['Address2'];
	print("<OPTION VALUE=$showPracticeID ");
	if ($showPracticeID==$thePracticeID) {
            print("selected ");
        };
        print("> $showPracticeID $theAddress1 $theAddress2");
    };
    print("</SELECT>\n");
    print("</td>");

    print("<td>");
    print("<input type='text' name='beepnumber' value='$theBeepNumber'>");
    print("</td></tr>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>NHSNumber</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Writing Exemption</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=50\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Signature File</A></FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Secondary Prescriber</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=nhsnumber VALUE='$theNHSNumber'");
    print(" SIZE=10></TD>");

    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=$writingexemption
      VALUE='$theWritingExemption'");
    print(" SIZE=10></TD>");

    print("<td>");
    print("<INPUT TYPE=TEXT NAME=sigfilename VALUE='$theSigFileName'");
    print(" SIZE=15></td>");

    print("<td><select name=secondaryprescriberid>");
    print("<option value='0'");
    if ($theSecondaryPrescriberID=="0") { print(" selected"); };
    print(">");
    $qtable=$db->select("ScripPrescriber");
    foreach ($qtable as $qrow) {
        $aPrescriber=$qrow['Name'];
	$aPrescriberID=$qrow['PrescriberID'];
	print("<option value=$aPrescriberID ");
	if ($aPrescriberID==$theSecondaryPrescriberID) {
            print("selected ");
        };
        print("> $aPrescriberID $aPrescriber");
    };
    print("</td></tr>");

    print("<tr>");
    print("<th><FONT COLOR=FFFFFF>Private CD Number</th>");
    print("</tr>");

    print("<tr>");
    print("<th><input type='text' name='privatecd' ");
    print("value=$thePrivateCD></th>");
    print("</tr>");

    print("<tr>");
    print("<th colspan=7><FONT COLOR=FFFFFF>");
    print("Edit the address in Practice Details");
    print("</FONT></TH></TR>");

    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function HB_UpdatePrescriber($SID, $thename, 
	$thequal, $thepracticeid, $theBeepNumber, $thenhsnumber,
	$thewritingexemption, $thesigfilename,
        $theSecondaryPrescriberID, $thePrivateCD) {
    $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 


    // Update details of the prescriber */  
    $prescriberdata= array(
	"Name" => $thename,
	"Qual" => $thequal,
	"PracticeID" => $thepracticeid,
	"BeepNumber" => $theBeepNumber,
	"NHSNumber" => $thenhsnumber,
	"WritingExemption" => $thewritingexemption,
	"SigFileName" => $thesigfilename,
	"SecondaryPrescriberID" => $theSecondaryPrescriberID,
        "PrivateCD" => $thePrivateCD
    );
    Z_DrawHeader();

    if ($SID != '0') {
        $condition=array('PrescriberID'=>$SID);
        $db->update("ScripPrescriber",$prescriberdata,$condition);    
    } else {
        $db->insert("ScripPrescriber",$prescriberdata);    
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Prescriber updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};


function UUU_EditIPLocation() {
    // Edit data for IP locations:

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of IP locations in their table and display as a table on screen:
    Z_DrawHeader();
    print("<tr>");
    print("<td colspan=2>");
    print("<font color='#FFFFFF'><H2>Select one location from the list and click the submit button to edit the details</H2></font>");
    print("</td>");
    print("</tr>");

    print("<tr><td colspan=2 align=center><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='466>");

    print("<tr>");
    print("<th>&nbsp;</TH>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>IP Beginning</font></th>");
    print("<th><font color='#ffffff'>Location</font></th>");
    print("</tr>");

    print("<tr>");
    print("<TD><FONT COLOR=FFFFFF>");
    print("<INPUT TYPE=RADIO NAME=ID VALUE='0' checked></TD>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripIPLocation");
    foreach ($table as $row) {
        $theID=$row['PK'];
        $theIPBeginning=$row['IPBeginning'];
        $theLocation=$row['Location'];
        print("<TR>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theID</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theIPBeginning</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theLocation</FONT></TD>");
        print("</TR>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function UUU_EditSingleIPLocation($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the IP location and display in a form
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");

    print("<INPUT TYPE=HIDDEN NAME=tab value=467>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<TH><FONT COLOR=FFFFFF>IP Beginning</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Location</FONT></TH>");
    print("</TR>");

    if ($SID!='0') {
        $table=$db->select("ScripIPLocation",array('PK'=>$SID));
        foreach ($table as $row) {
            $theID=$row['PK'];
            $theIPBeginning=$row['IPBeginning'];
            $theLocation=$row['Location'];
        };
    } else {
        $theID="";
        $theName="";
        $theQual="";
    };

    print("<tr>");
    print("<td><input type=text name=ipbeginning value='$theIPBeginning'");
    print(" size='20'></td>");
    print("<td><input type=text name=location value='$theLocation'");
    print(" size='20'></td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan=7><FONT COLOR=FFFFFF>");
    print("Edit the IP location");
    print("</FONT></TH></TR>");

    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</form></table></td></tr><tr><td>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function UUU_UpdatePrescriber($SID, $theIPBeginning, $theLocation) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the IP location ...
    $data= array("IPBeginning" => $theIPBeginning, "Location" => $theLocation);
    Z_DrawHeader();

    if ($SID != '0') {
        $condition=array('PK'=>$SID);
        $db->update("ScripIPLocation", $data, $condition);    
    } else {
        $db->insert("ScripIPLocation", $data);    
    };

    print("<tr><td colspan='2'><font color='#ffffff'>IP location updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};


function I_EditPracticeData() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the practices and display as a table
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one practice from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=61>");
    print("<TR><TH>&nbsp;</TH>");
    print("<th><font color=FFFFFF>ID</font></th>");
    print("<th><font color=FFFFFF>Code</font></th>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("<th><font color=FFFFFF>Post Code</font></th>");
    print("<th><font color=FFFFFF>Telephone Number</font></th>");
    print("<th><font color=FFFFFF>PCT</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><INPUT TYPE=RADIO NAME=ID VALUE='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripPractice");
    foreach ($table as $row) {
        print("<TR>");
        $thePracticeID=$row['PracticeID'];
	$theCode=$row['Code'];
	$theAddress1=$row['Address1'];
	$theAddress2=$row['Address2'];
	$theAddress3=$row['Address3'];
	$theAddress4=$row['Address4'];
	$thePostCode=$row['PostCode'];
	$theTelephoneNumber=$row['TelephoneNumber'];
	$thePCTID=$row['PCTID'];
        print("<TD><INPUT TYPE=RADIO NAME=ID VALUE=$thePracticeID></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thePracticeID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theCode</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress1&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress2&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress3&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress4&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thePostCode</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theTelephoneNumber</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thePCTID</FONT></TD>");
        print("</TR>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function IA_EditSinglePractice($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the practice and display in a form
    $table=$db->select("ScripPractice",array('PracticeID'=>$SID));
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theCode=$row['Code'];
            $thePracticeOrUnit=$row['PracticeOrUnit'];
            $theName=$row['Name'];
            $theAddress1=$row['Address1'];
            $theAddress2=$row['Address2'];
            $theAddress3=$row['Address3'];
            $theAddress4=$row['Address4'];
            $thePostCode=$row['PostCode'];
            $theTelephoneNumber=$row['TelephoneNumber'];
            $theTrustID=$row['TrustID'];
        };
    } else {
        $theCode="";
        $thePracticeOrUnit="";
        $theName="";
        $theAddress1="";
	$theAddress2="";
	$theAddress3="";
	$theAddress4="";
        $thePostCode="";
        $theTelephoneNumber="";
        $theTrustID="";
    };

    print("<tr><td colspan='2' align=center>");
    print("<table border='1'>");
    print("<form ACTION=scripadmin.php METHOD=POST>");
    print("<input type='hidden' name=tab value=62>");
    print("<input TYPE='hidden' name=ID value=$SID>");

    print("<tr>");
    print("<th><font COLOR=FFFFFF>Code</FONT></th>");
    print("<th><font COLOR=FFFFFF>Practice or Unit?</FONT></th>");
    print("<th><font COLOR=FFFFFF>Name</FONT></th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<INPUT TYPE=TEXT NAME=thecode VALUE='$theCode' SIZE=35>");
    print("</td>");

    print("<td><select name='thepracticeorunit'>");
    print("<option value=''>Select from ...");
    print("<option value='0'");
    if ($thePracticeOrUnit =='0') { print(" selected"); };
    print(">Practice");
    print("<option value='1'");
    if ($thePracticeOrUnit =='1') { print(" selected"); };
    print(">Unit");
    print("</td>");

    print("<td>");
    print("<INPUT TYPE=TEXT NAME=thename VALUE='$theName' SIZE=35>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("</tr>");
    print("<tr><TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress1 VALUE='$theAddress1' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress2 VALUE='$theAddress2' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress3 VALUE='$theAddress3' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress4 VALUE='$theAddress4' SIZE=35>");
    print("</TD></tr>");
    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Post Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Telephone Number</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Trust ID</FONT></TH>");
    print("</TR>");
    print("<tr>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=thepostcode VALUE='$thePostCode' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=thetelephonenumber ");
    print("VALUE='$theTelephoneNumber' SIZE=35>");
    print("</TD>");

    print("<td><SELECT NAME=thetrustid>");
    print("<option value='0'");
    if ($theTrustID=="0") { print(" selected"); };
    print(">Select from ...");
    $ptable=$db->select("ScripTrust");
    foreach ($ptable as $prow) {
        $showTrustID=$prow['ID'];
	$theCode=$prow['Code'];
	$theName=$prow['Name'];
	$theAddress1=$prow['Address1'];
	print("<OPTION VALUE=$showTrustID ");
	if ($showTrustID==$theTrustID) {
            print("selected ");
        };
        print(">$theCode -- $theName ($showTrustID)");
    };
    print("</SELECT>\n");
    print("</TD>");
    print("</tr>");

    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function IB_UpdatePractice($SID, $theCode, $thePracticeOrUnit,
         $theName, $theAddress1, $theAddress2,
         $theAddress3, $theAddress4, $thePostCode,
         $theTelephoneNumber, $theTrustID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 



/* Update details of the practice */  
    $practicedata= array(
        "Code" => $theCode,
        "PracticeOrUnit" => $thePracticeOrUnit,
        "Name" => $theName,
	"Address1" => $theAddress1,
	"Address2" => $theAddress2,
	"Address3" => $theAddress3,
	"Address4" => $theAddress4,
	"PostCode" => $thePostCode,
	"TelephoneNumber" => $theTelephoneNumber,
	"TrustID" => $theTrustID
	);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('PracticeID'=>$SID);
        $db->update("ScripPractice",$practicedata, $condition);
    } else {
        $db->insert("ScripPractice",$practicedata);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Practice updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function I_EditTrustData() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the trusts and display as a table
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one trust from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=66>");
    print("<TR><TH>&nbsp;</TH>");
    print("<th><font color=FFFFFF>ID</font></th>");
    print("<th><font color=FFFFFF>Code</font></th>");
    print("<th><font color=FFFFFF>T or H?</font></th>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><INPUT TYPE=RADIO NAME=ID VALUE='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripTrust");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
	$theCode=$row['Code'];
	$theTrustOrHospital=$row['TrustOrHospital'];
	$theAddress1=$row['Address1'];
	$theAddress2=$row['Address2'];
	$theAddress3=$row['Address3'];
	$theAddress4=$row['Address4'];
        print("<TD><INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theCode</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
        if ($theTrustOrHospital){
            print("Trust");
        } else {
            print("Hospital");
        };
	print("</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress1&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress2&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress3&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress4&nbsp;</FONT></TD>");
        print("</TR>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function IA_EditSingleTrust($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the trust and display in a form
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripTrust",$condition);
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theCode=$row['Code'];
            $theTrustOrHospital=$row['TrustOrHospital'];
            $theName=$row['Name'];
            $theAddress1=$row['Address1'];
            $theAddress2=$row['Address2'];
            $theAddress3=$row['Address3'];
            $theAddress4=$row['Address4'];
            $thePostCode=$row['PostCode'];
            $theTelNo=$row['TelephoneNumber'];
        };
    } else {
        $theCode="";
	$theTrustOrHospital="";
        $theName="";
        $theAddress1="";
	$theAddress2="";
	$theAddress3="";
	$theAddress4="";
        $thePostCode="";
        $theTelNo="";
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=67>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>T or H?</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input type=text name=thecode value='$theCode' size='35'>");
    print("</td>");
    print("<td>");
    print("<select name=trustorhospital>");
    print("<option value=''>Select from ...");
    print("<option value='1'");
    if ($theTrustOrHospital == '1') { print(" selected"); };
    print(">Trust");
    print("<option value='0'");
    if ($theTrustOrHospital == '0') { print(" selected"); };
    print(">Hospital");
    print("</td>");
    print("<td>");
    print("<input type=text name=thename value='$theName' size='35'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("</tr>");
    print("<tr><TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress1 VALUE='$theAddress1' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress2 VALUE='$theAddress2' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress3 VALUE='$theAddress3' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress4 VALUE='$theAddress4' SIZE=35>");
    print("</TD></tr>");


    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Post Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Telephone Number</FONT></TH>");
    print("</tr>");
    print("<tr><TD>");
    print("<INPUT TYPE=TEXT NAME=thepostcode VALUE='$thePostCode' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=thetelno VALUE='$theTelNo' SIZE=35>");
    print("</TD></tr>");



    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function IB_UpdateTrust($SID,$theCode,
         $theTrustOrHospital,$theName,
         $theAddress1, $theAddress2,
         $theAddress3, $theAddress4,
         $thePostCode, $theTelNo) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 


    // Update details of the trust ...
    $data= array(
        "Code" => $theCode,
        "TrustOrHospital" => $theTrustOrHospital,
	"Name" => $theName,
	"Address1" => $theAddress1,
	"Address2" => $theAddress2,
	"Address3" => $theAddress3,
	"Address4" => $theAddress4,
	"PostCode" => $thePostCode,
	"TelephoneNumber" => $theTelNo
	);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripTrust",$data, $condition);
    } else {
        $db->insert("ScripTrust",$data);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Trust updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};


function I_EditDatesYearGroup(){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select a year group and click the submit button to go on and edit the details for that year group.</h2></font>");
    print("</td></tr>");

    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='780'>");

    for ($i = 0; $i < 4; $i++){
        $yearGroup = $i + 1;
        print("<tr>");
        print("<td width='20%'><input type='radio' name='yeargroup' value='".$yearGroup."'>");
        print("<font color='#ffffff'>$yearGroup</td>");
        print("</tr>");
    };

    // Submit button:
    print("<tr><td colspan='8' align=center><input value='Choose Year-Group' type='submit'></TD></TR>");

    // End of the form ...
    print("</form>");

    // Hyperlink for return to main menu ...
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");

    // End of the table ...
    print("</table>");

    // End of the cell and row ...
    print("</td></tr>");

    // End of the table ...
    print("</table>");

};


function I_EditDates($yearGroup) {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the dates for yeargroup $yearGroup and display as a table:
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><h2>Select one dates from the list and click the submit button to edit the details</H2></FONT>");
    print("</TD></tr>");

    print("<tr><td><font color='#ffffff'>Year-group: <b>$yearGroup</b></font></td></tr>");

    print("<tr><td colspane='2'><font color='#ffffff'>P: Practice; C: Checking: D: Mock; E: Exam; F: Re-sit</font></td></tr>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<input type='hidden' name='tab' value='782'>");
    print("<input type='hidden' name='yeargroup' value='$yearGroup'>");

    print("<TR><TH>&nbsp;</TH>");
    print("<th><font color=FFFFFF>ID</font></th>");
    print("<th><font color=FFFFFF>Type</font></th>");
    print("<th><font color=FFFFFF>Number</font></th>");
    print("<TH><FONT COLOR=FFFFFF>Date</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Location</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><INPUT TYPE=RADIO NAME=ID VALUE='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $condition = array('YearGroup'=>$yearGroup);
    // ... this will allow us to use the old code

    $table=$db->select("ScripDates", $condition);
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        $theType=$row['Type'];
        $theNumber=$row['Number'];
        $theDate=$row['Date'];
        $theLocation=$row['Location'];
        print("<td><input type='radio' name=ID value='$theID'></td>");
        print("<td><font color='#ffffff'>$theID</font></td>");
        print("<td><font color='#ffffff'>$theType</font></td>");
        print("<td><font color='#ffffff'>$theNumber</font></td>");
        print("<td><font color='#ffffff'>$theDate</font></td>");
        print("<td><font color='#ffffff'>$theLocation</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan='8' align=center><INPUT TYPE='SUBMIT'></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function IA_EditSingleDate($SID, $yearGroup) {
    // Edit the dates table:

    $db=new dbabstraction(); $db->connect() or die ($db->getError()); 

    // Get details of the date and display in a form
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripDates", $condition);
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theType = $row['Type'];
            $theNumber = $row['Number'];
            $theDate = $row['Date'];
            $theLocation = $row['Location'];
        };
    } else {
        $theType = "";
        $theNumber = "";
        $theDate = "";
        $theLocation = "";
    };

    print("<tr><td colspan='2' align='center'>");

    print("<table><tr>");
    print("<td><font color='#ffffff'>This is for year-group <b>$yearGroup</b>.</font></td>");
    print("</tr></table>");

    print("<TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD='POST'>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=784>");
    print("<input type='hidden' name='ID' value='$SID'>");
    print("<input type='hidden' name='yeargroup' value='$yearGroup'>");

    print("<tr>");
    print("<th><font color='#ffffff'>Type</font></th>");
    print("<th><font color='#ffffff'>Number</font></th>");
    print("<th><font color='#ffffff'>Date</font></th>");
    print("<th><font color='#ffffff'>Location</font></th>");
    print("</tr>");

    print("<tr>");

    print("<td>");

    // Type can now (January 2014) be chosen from drop-down list only (and not free text as previously) ...
    // [Would be better if: Possible optipons (currently P, E and R) were in an array and then code looped over array to add each option - would make it easier to add further letters if necessary]
    print("<select name='thetype'>");

    print("<option value=''>Please select...</option>");

    print("<option value='P'");
    if ($theType == "P") { print(" selected='selected'"); };
    print(">Practice</option>");

    print("<option value='E'");
    if ($theType == "E") { print(" selected='selected'"); };
    print(">Examination</option>");

    print("<option value='R'");
    if ($theType == "R") { print(" selected='selected'"); };
    print(">Re-sit</option>");

    print("</select>");

    // Former code: Text
//    print("<input type='text' name='thetype' value='$theType' size='35'>");
    print("</td>");

    print("<td><input type='text' name='thenumber' value='$theNumber' size='35'></td>");
    print("<td><input type='text' name='thedate' value='$theDate' size='35'></td>");


    // This [entering the location] would have to be expanded if new locations were necessary - initial choice is to resort to using a free text line (as original) ...
    print("<td>");
    print("<select name='thelocation'>");

    print("<option value=''>Please select...</option>");

    print("<option value='Nottingham'");
    if ($theLocation == "Nottingham") { print(" selected='selected'"); };
    print(">Nottingham</option>");

    print("<option value='Malaysia'");
    if ($theLocation == "Malaysia") { print(" selected='selected'"); };
    print(">Malaysia</option>");

    print("</select>");

//    Original
//    print("<input type='text' name='thelocation' value='$theLocation' size='35'>");
    print("</td>");
    print("</tr>");

    print("<tr><td colspan='4' align='center'>");
    print("<input type='submit' value='Update'></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function IB_UpdateDates($SID, $yearGroup, $theType, $theNumber, $theDate, $theLocation) {
    // Write the data to the table in the database ...

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the dates ...
    $data= array("YearGroup" => $yearGroup, "Type" => $theType, "Number" => $theNumber, "Date" => $theDate, "Location" => $theLocation);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripDates", $data, $condition);
    } else {
        $db->insert("ScripDates", $data);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Date updated</FONT></TD></TR>");

    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");

    print("</td></tr></table>");
};

function V_EditExamWeeksReleasedGC() {
    // To make exam results available via the generic comments (GC) system:

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the weeks ...
    print("<tr><td colspan='2'><font color='#ffffff'>");
    print("<h2>Select one week from the list and click the submit button to edit the details</h2>");
    print("</font></td></tr>");
    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='666'>");

    // Headings ...
    print("<tr><th>&nbsp;</th>");
    print("<th><font color='#ffffff'>Year Group</font></th>");
    print("<th><font color='#ffffff'>Location</font></th>");
    print("<th><font color='#ffffff'>WeekNumberID</font></th>");
    print("<th><font color='#ffffff'>WeekType</font></th>");
    print("<th><font color='#ffffff'>RR</font></th>");
    print("</tr>");

    // Data - Option of a new item ...
    print("<tr>");
    print("<td><input type='radio' name='ID' value='0' checked></td>");
    print("<td><font color='#ffffff'>New</td>");
    print("</tr>");

    // Data - Existing data ...
    $table=$db->select("ScripWeeksGCExamReleased");
    foreach ($table as $row) {
        print("<tr>");
        $theWeekNumberID = $row['WeekNumberID'];
        $theWeekType = $row['WeekType'];
        $theYearGroup = $row['YearGroup'];
        $theLocation = $row['Location'];
        $theRR = $row['RR'];
        $theWR = $row['WR'];
        print("<td><input type='radio' name='ID' value='$theWeekNumberID'></td>");
        print("<td><font color='#ffffff'>$theYearGroup</fond></td>");
        print("<td><font color='#ffffff'>$theLocation</fond></td>");
        print("<td><font color='#ffffff'>$theWeekNumberID</font></td>");
        print("<td><font color='#ffffff'>$theWeekType</font></td>");
        print("<td><font color='#ffffff'>$theRR</fond></td>");
        print("</tr>");
    };

    // Submit button ...
    print("<tr><td colspan='8' align='center'><input type='submit'></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr></table>");
};

function V_EditSingleExamWeekReleasedGC($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the week and display in a form
    $condition = array('WeekNumberID'=>$ID);
    $table=$db->select("ScripWeeksGCExamReleased", $condition);
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theYearGroup = $row['YearGroup'];
            $theLocation = $row['Location'];
            $theWeekNumberID = $row['WeekNumberID'];
            $theWeekType = $row['WeekType'];
            $theRR = $row['RR'];
        };
    } else {
        $theYearGroup = "";
        $theLocation = "";
        $theWeekNumberID = "";
        $theWeekType = "";
        $theRR = "";
    };

    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");

    // Set the tab variable and re-set the ID ...
    print("<input type='hidden' name='tab' value='667'>");
    print("<input type='hidden' name='ID' value=$ID>");

    // Headings 1 ...
    print("<tr>");
    print("<th><font color='#ffffff'>Year Group</font></th>");
    print("<th><font color='#ffffff'>Location</font></th>");
    print("</tr>");

    // Data 1 ...
    print("<tr>");
    print("<td><input type=text name=theyeargroup value='$theYearGroup' size='35'></td>");
    print("<td><input type=text name=thelocation value='$theLocation' size='35'></td>");
    print("</tr>");

    // Headings 2 ...
    print("<tr>");
    print("<th><font color='#ffffff'>WeekNumberID</font></th>");
    print("<th><font color='#ffffff'>WeekType</font></th>");
    print("<th><font color='#ffffff'>RR</font></th>");
    print("</tr>");

    // Data 2 ...
    print("<tr>");
    print("<td><input type=text name=theweeknumberid value='$theWeekNumberID' size='35'></td>");
    print("<td><input type=text name=theweektype value='$theWeekType' size='35'></td>");
    print("<td><input type=text name=therr value='$theRR' size='35'></td>");
    print("</tr>");

    print("<tr><td colspan='4' align='center'>");
    print("<input type='submit' value='Update'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>");
};

//function V_UpdateSingleExamWeekReleasedGC($theID, $theWeekType, $theRR, $theWR) {
function V_UpdateSingleExamWeekReleasedGC($theID, $theYearGroup, $theLocation, $theWeekType, $theRR, $theWR) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the weeks ...
    $data= array("WeekType" => $theWeekType, "YearGroup" =>$theYearGroup, "Location" =>$theLocation, "RR" => $theRR);
    Z_DrawHeader();

    if ($theID != '0') { // ... edit
        $condition=array('WeekNumberID'=>$theID);
        $db->update("ScripWeeksGCExamReleased", $data, $condition);
    } else { // ... entirely new data 
        $db->insert("ScripWeeksGCExamReleased", $data);
    };

    print("<tr><td colspan='2'><font color='#ffffff'>Week updated</font></td></tr>");
    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr></table>");
};












function V_EditSeriousErrors() {
    // To make exam results available via the generic comments (GC) system:

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the weeks ...
    print("<tr><td colspan='2'><font color='#ffffff'>");
    print("<h2>Select one serious error from the list and click the submit button to edit the details</h2>");
    print("</font></td></tr>");
    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='936'>");

    // Headings ...
    print("<tr><th>&nbsp;</th>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Order</font></th>");
    print("<th><font color='#ffffff'>Text</font></th>");
    print("</tr>");

    // Data - Option of a new item ...
    print("<tr>");
    print("<td><input type='radio' name='ID' value='0' checked></td>");
    print("<td><font color='#ffffff'>New</td>");
    print("</tr>");

    // Data - Existing data ...
    $table=$db->select("ScripSeriousErrorTypes");
    foreach ($table as $row) {
        print("<tr>");
        $theID = $row['ID'];
        $theOrder = $row['Order'];
        $theText = $row['Text'];
        print("<td><input type='radio' name='ID' value='$theID'></td>");
        print("<td><font color='#ffffff'>$theID</font></td>");
        print("<td><font color='#ffffff'>$theOrder</font></td>");
        print("<td><font color='#ffffff'>$theText</fond></td>");
        print("</tr>");
    };

    // Submit button ...
    print("<tr><td colspan='8' align='center'><input type='submit'></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr></table>");
};

function V_EditSingleSeriousError($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the week and display in a form
    $condition = array('ID'=>$ID);
    $table=$db->select("ScripSeriousErrorTypes", $condition);
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theID = $row['ID'];
            $theOrder = $row['Order'];
            $theText = $row['Text'];
        };
    } else {
        $theID = "";
        $theOrder = "";
        $theText = "";
    };

    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");

    // Set the tab variable and re-set the ID ...
    print("<input type='hidden' name='tab' value='937'>");
    print("<input type='hidden' name='ID' value=$ID>");

    // Headings ...
    print("<tr>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Order</font></th>");
    print("<th><font color='#ffffff'>Text</font></th>");
    print("</tr>");

    // Data ...
    print("<tr>");
    print("<td><input type=text name=theid value='$theID' size='35' disabled='disabled'></td>");
    print("<td><input type=text name=theorder value='$theOrder' size='35'></td>");
    print("<td><input type=text name=thetext value='$theText' size='35'></td>");
    print("</tr>");

    print("<tr><td colspan='4' align='center'>");
    print("<input type='submit' value='Update'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>");
};

function V_UpdateSeriousErrors($theID, $theOrder, $theText) {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $data = array("O" => 61, "T" => "Test only");
//    $db->insert("ScripSeriousErrorTypes", $data);
    $db->insert("ScripSE", $data);


/*
    // Update details of the weeks ...
    $data = array("Order" => $theOrder, "Text" => $theText);
    Z_DrawHeader();

print("theID is $theID<br>");
print("theOrder is $theOrder<br>");
print("theText is $theText<br>");

    if ($theID != '0') { // ... edit
        $condition=array('ID'=>$theID);
print_r($condition);
        $db->update("ScripSeriousErrorTypes", $data, $condition);
    } else { // ... entirely new data 
//        $db->insert("ScripSeriousErrorTypes", $data);
        $db->insert("ScripSeriousErrorTypes", array("Order" => 34, "Text" => 23));
    };

print_r($data);


*/


    print("<tr><td colspan='2'><font color='#ffffff'>Serious error updated</font></td></tr>");
    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr></table>");
};







/*

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the weeks ...
    $data= array("WeekType" => $theWeekType, "RR" => $theRR, "WR" => $theWR);
    Z_DrawHeader();

    if ($theID != '0') { // ... edit
        $condition=array('WeekNumberID'=>$theID);
        $db->update("ScripWeeksGCExamReleased", $data, $condition);
    } else { // ... entirely new data 
        $db->insert("ScripWeeksGCExamReleased", $data);
    };

    print("<tr><td colspan='2'><font color='#ffffff'>Week updated</font></td></tr>");
    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr></table>");


*/












function I_EditHospitalTTO() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the trusts and display as a table
    print("<tr><td colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one trust from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<tr><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<form ACTION=scripadmin.php METHOD=POST>");
    print("<input TYPE='hidden' name='tab' value=76>");
    print("<TR><TH>&nbsp;</TH>");
    print("<th><font color=FFFFFF>ID</font></th>");
    print("<th><font color=FFFFFF>Special Instructions</font></th>");
    print("<th><font color=FFFFFF>8 am</font></th>");
    print("<TH><FONT COLOR=FFFFFF>10 am</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>1 pm</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>6 pm</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>10 pm</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Obtain More?</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Stop?</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Indication</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><INPUT TYPE=RADIO NAME=ID VALUE='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripHospitalTTODetails");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        $theSpecialInstructions=$row['SpecialInstructions'];
        $the8AM=$row['Time8'];
        $the10AM=$row['Time10'];
        $the1PM=$row['Time13'];
        $the6PM=$row['Time18'];
        $the10PM=$row['Time22'];
        $theObtainMore=$row['ObtainMore'];
        $theStop=$row['Stop'];
        $theIndication=$row['Indication'];
        print("<td><input type='radio' NAME=ID VALUE=$theID></TD>");
        print("<td><font color='#ffffff'>$theID</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$theSpecialInstructions</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$the8AM</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$the10AM</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$the1PM</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$the6PM</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$the10PM</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$theObtainMore</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$theStop</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$theIndication</FONT></TD>");
        print("</tr>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function IA_EditSingleHospitalTTO($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the trust and display in a form
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripHospitalTTODetails",$condition);
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theSpecialInstructions=$row['SpecialInstructions'];
            $the8AM=$row['Time8'];
            $the10AM=$row['Time10'];
            $the1PM=$row['Time13'];
            $the6PM=$row['Time18'];
            $the10PM=$row['Time22'];
            $theObtainMore=$row['ObtainMore'];
            $theStop=$row['Stop'];
            $theIndication=$row['Indication'];
        };
    } else {
        $theSpecialInstructions="";
        $the8AM="";
        $the10AM="";
        $the1PM="";
        $the6PM="";
        $the10PM="";
        $theObtainMore="";
        $theStop="";
        $theIndication="";
    };

    print("<TR>");
    print("<TD colspan='2' align='center'><TABLE BORDER='1'>");
    print("<FORM ACTIONscripadmin.php METHOD=post>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=77>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<tr>");
    print("<th colspan='7'><FONT COLOR=FFFFFF>Special Instructions</FONT></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='7'>");
    print("<input type=text name=thespecialinstructions value='$theSpecialInstructions' size='105'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color='#ffffff'>8 am</FONT></th>");
    print("<th><font color='#ffffff'>10 am</FONT></th>");
    print("<th><font color='#ffffff'>1 pm</FONT></th>");
    print("<th><font color='#ffffff'>6 pm</FONT></th>");
    print("<th><font color='#ffffff'>10 pm</FONT></th>");
    print("<th><font color='#ffffff'>Obtain More?</FONT></th>");
    print("<th><font color='#ffffff'>Stop?</FONT></th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<INPUT TYPE=checkbox NAME=the8am style='filter: invert'");
    if ($the8AM) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<INPUT TYPE=checkbox NAME=the10am");
    if ($the10AM) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<INPUT TYPE=checkbox NAME=the1pm");
    if ($the1PM) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<INPUT TYPE=checkbox NAME=the6am");
    if ($the6PM) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<INPUT TYPE=checkbox NAME=the10pm");
    if ($the10PM) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<INPUT TYPE=checkbox NAME=theobtainmore");
    if ($theObtainMore) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<INPUT TYPE=checkbox NAME=thestop");
    if ($theStop) { print(" checked"); };
    print(">");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<TH colspan='5'><FONT COLOR=FFFFFF>Indication</FONT></TH>");
    print("</tr>");
    print("<tr><TD colspan='5'>");
    print("<INPUT TYPE=TEXT NAME=theindication VALUE='$theIndication' SIZE=125>");
    print("</TD></tr>");

    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function IB_UpdateHospitalTTO($SID,$theSpecialInstructions,
         $the8AM,$the10AM,$the1PM,$the6PM,$the10PM,
         $theObtainMore, $theStop,
         $theIndication) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $the8AM = $the8AM == 'on' ? 1 : 0;
    $the10AM = $the10AM == 'on' ? 1 : 0;
    $the1PM = $the1PM == 'on' ? 1 : 0;
    $the6PM = $the6PM == 'on' ? 1 : 0;
    $the10PM = $the10PM == 'on' ? 1 : 0;

    // Update details of the TTO ...
    $data= array(
        "SpecialInstructions" => $theSpecialInstructions,
        "Time8" => $the8AM,
        "Time10" => $the10AM,
        "Time13" => $the1PM,
        "Time18" => $the6PM,
        "Time22" => $the10PM,
        "ObtainMore" => $theObtainMore,
        "Stop" => $theStop,
        "Indication" => $theIndication
	);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripHospitalTTODetails", $data, $condition);
    } else {
        $db->insert("ScripHospitalTTODetails", $data);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Hospital TTO details updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function I_EditPatientTTO() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the trusts and display as a table
    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>TTO details � Select one patient from the list");
    print(" and click the submit button to edit the details</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name='tab' value=86>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Name (Number)</font></th>");
    print("<th><font color='#ffffff'>Hospital Number</font></th>");
    print("<th><font color='#ffffff'>Telephone Number</font></th>");
    print("<th><font color='#ffffff'>Date of Admission</font></th>");
    print("<th><font color='#ffffff'>Date of Discharge</font></th>");
    print("<th><font color='#ffffff'>Consultant</font></th>");
    print("<th><font color='#ffffff'>Prinmary Diagnosis</font></th>");
    print("<th><font color='#ffffff'>Problems</font><th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type='radio' name=ID value='0' checked></td>");
    print("<td colspan='10'><font color='#ffffff'>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripHospitalTTOPatient");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        $theHospitalNumber=$row['HospitalNumber'];
        $theTelephoneNumber=$row['TelephoneNumber'];
        $theDateOfAdmission=$row['DateOfAdmission'];
        $theDateOfDischarge=$row['DateOfDischarge'];
        $theConsultant=$row['Consultant'];
        $thePrimaryDiagnosis=$row['PrimaryDiagnosis'];
        $theProblems=$row['Problems'];

        // Find the name of the patient ...
        $condition = array ('HospitalTTOPatient'=>$theID); 
        $table=$db->select("ScripPatient", $condition );
        foreach ($table as $row){
            $fName = $row['FirstName'];
            $lName = $row['LastName'];
            $patientID = $row['PatientID'];
        };

        print("<td><input type='radio' name=ID value=$theID></td>");
        print("<td><font color='#ffffff'>$theID</font></td>");
        print("<td colspan='2'><font color='#ffffff'>$fName $lName ($patientID)</font></td>");
        print("<td><font color='#ffffff'>$theHospitalNumber</font></td>");
        print("<td><font color='#ffffff'>$theTelephoneNumber</font></td>");
        print("<td><font color='#ffffff'>$theDateOfAdmission</font></td>");
        print("<td><font color='#ffffff'>$theDateOfDischarge</font></td>");
        print("<td><font color='#ffffff'>$theConsultant</font></td>");
        print("<td><font color='#ffffff'>$thePrimaryDiagnosis</font></td>");
        print("<td><font color='#ffffff'>$theProblems</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan='11' align='center'><input type='submit'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a></td></tr></table>");
};

function IA_EditSinglePatientTTO($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $condition = array('HospitalTTOPatient'=>$SID);
    $table=$db->select("ScripPatient", $condition);
    foreach ($table as $row){
        $fName = $row['FirstName'];
        $lName = $row['LastName'];
    };

    // Get details of the patient and display in a form
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripHospitalTTOPatient",$condition);
    Z_DrawHeader();

    if ($SID) {
        foreach ($table as $row) {
            $theHospitalNumber=$row['HospitalNumber'];
            $theTelephoneNumber=$row['TelephoneNumber'];
            $theDateOfAdmission=$row['DateOfAdmission'];
            $theWard=$row['Ward'];
            $theTelExt=$row['TelExt'];
            $theDateOfDischarge=$row['DateOfDischarge'];
            $theTransferredTo=$row['TransferredTo'];
            $theConsultant=$row['Consultant'];
            $theConsultantCode=$row['ConsultantCode'];
            $theLetterToGPDr=$row['LetterToGPDr'];
            $theDischargeAddressContactNo=$row['DischargeAddressContactNo'];
            $thePrimaryDiagnosis=$row['PrimaryDiagnosis'];
            $theProblems=$row['Problems'];
            $theConfidential=$row['Confidential'];
            $theNurseInformation=$row['NurseInformation'];
            $theIsNotRequired=$row['IsNotRequired'];
            $theWillBePosted=$row['WillBePosted'];
            $theNextDate=$row['NextDate'];
            $theNextTime=$row['NextTime'];
            $theNextClinic=$row['NextClinic'];
            $theTransport=$row['Transport'];
            $theDistrictNursingService=$row['DistrictNursingService'];
            $theSocialServices=$row['SocialServices'];
            $thePsychiatric=$row['Psychiatric'];
            $theOtherText=$row['OtherText'];
            $theSignatureFile=$row['SignatureFile'];
            $theDoctor=$row['Doctor'];
        };
    } else {
        $theHospitalNumber="";
        $theTelephoneNumber="";
        $theDateOfAdmission="";
        $theWard="";
        $theTelExt="";
        $theDateOfDischarge="";
        $theTransferredTo="";
        $theConsultant="";
        $theConsultantCode="";
        $theLetterToGPDr="";
        $theDischargeAddressContactNo="";
        $thePrimaryDiagnosis="";
        $theProblems="";
        $theConfidential="";
        $theNurseInformation="";
        $theIsNotRequired="";
        $theWillBePosted="";
        $theNextDate="";
        $theNextTime="";
        $theNextClinic="";
        $theTransport="";
        $theDistrictNursingService="";
        $theSocialServices="";
        $thePsychiatric="";
        $theOtherText="";
        $theSignatureFile="";
        $theDoctor="";
    };

    print("<tr>");
    print("<td colspan='2' align='center'><table border='1'>");
    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name=tab value=87>");
    print("<input type='hidden' name=ID value=$SID>");


    print("<tr>");
    print("<th colspan='6'><font color='#ffffff'>Patient: $fName $lName</font></th>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Hospital Number</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Telephone Number</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Date of Admission</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'>");
    print("<input type='text' name=thehospitalnumber value='$theHospitalNumber'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=thetelephonenumber value='$theTelephoneNumber'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' size=3 maxlength='5' name=thedateofadmission value='$theDateOfAdmission'>");
    print(" <font color='#ffffff'>days before to-day</font>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Ward</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Tel Ext</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Date of Discharge</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'>");
    print("<input type='text' name=theward value='$theWard'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=thetelext value='$theTelExt'>");
    print("</td>");
    print("<td colspan='2'>");
//    print("<input type='text' name=thedateofdischarge value='$theDateOfDischarge'>");

    print("<select name=thedateofdischarge >");
    print("<option value='1'");
    if ($theDateOfDischarge == 1) { print(" selected"); };
    print(">Yesterday");    
    print("<option value='0'");
    if ($theDateOfDischarge == 0) { print(" selected"); };
    print(">To-day");    
    print("<option value='-1'");
    if ($theDateOfDischarge == -1) { print(" selected"); };
    print(">Tomorrow");    
    print("</select>");



    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Transferred To</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Consultant</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Consultant Code</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'>");
    print("<input type='text' name=thetransferredto value='$theTransferredTo'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=theconsultant value='$theConsultant'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=theconsultantcode value='$theConsultantCode'>");
    print("</td>");
    print("<td colspan='2'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Letter to GP Dr</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Discharge Address Contact No</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Primary Diagnosis</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'>");
    print("<input type='text' name=thelettertogpdr value='$theLetterToGPDr'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=thedischargeaddresscontactno value='$theDischargeAddressContactNo'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=theprimarydiagnosis value='$thePrimaryDiagnosis'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Problems</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Confidential</font></th>");
    print("<th colspan='4'><font color='#ffffff'>Nursing Information</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'>");
    print("<input type='text' name=theproblems value='$theProblems'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=theconfidential value='$theConfidential'>");
    print("</td>");
    print("<td colspan='4'>");
    print("<input type='text' name=thenursinginformation value='$theNursingInformation'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='6'><font color='#ffffff'>&nbsp;</font></th>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='3'><font color='#ffffff'>An outpatient appointment ...</font></th>");
    print("<th colspan='3'><font color='#ffffff'>The following will visit ...</font></th>");
    print("</tr>");

    print("<tr>");
    print("<th><font color='#ffffff'>Is not required</font></th>");
    print("<th><font color='#ffffff'>Will be  posted</font></th>");
    print("<th><font color='#ffffff'>Transport</font></th>");
    print("<th><font color='#ffffff'>District nursing service</font></th>");
    print("<th><font color='#ffffff'>Social services</font></th>");
    print("<th><font color='#ffffff'>Psychiatric</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input type='checkbox' name='theisnotrequired' style='filter: invert'");
    if ($theIsNotRequired) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<input type='checkbox' name='thewillbeposted' style='filter: invert'");
    if ($theWillBePosted) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<input type='checkbox' name='thetransport' style='filter: invert'");
    if ($theTransport) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<input type='checkbox' name='thedistrictnursingservice' style='filter: invert'");
    if ($theDistrictNursingService) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<input type='checkbox' name='thesocialservices' style='filter: invert'");
    if ($theSocialServices) { print(" checked"); };
    print(">");
    print("</td>");
    print("<td>");
    print("<input type='checkbox' name='thepsychiatric' style='filter: invert'");
    if ($thePsychiatric) { print(" checked"); };
    print(">");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='3' align='left'><font color='#ffffff'>Time </font>");
    print("<input type='text' name='thenexttime' value=$theNextTime>");
    print("</th>");
    print("<th colspan='3'><font color='#ffffff'>Other</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='3' align='left'><font color='#ffffff'><b>Date</b> </font>");
    print("<input type='text' size='3' maxlength='5' name='thenextdate' value='$theNextDate'>");
    print("<font color='#ffffff'> days from to-day</font>");
    print("</td>");
    print("<td colspan='3'>");
    print("<input type='text' name=theothertext value='$theOtherText'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='3' align='left'><font color='#ffffff'>Clinic </font>");
    print("<input type='text' name='thenextclinic' value=$theNextClinic>");
    print("</th>");
    print("<td colspan='3'>&nbsp;");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Signature File</font></th>");
    print("<th colspan='2'><font color='#ffffff'>Doctor</font></th>");
    print("<th colspan='2'><font color='#ffffff'></font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'>");
    print("<input type='text' name=thesignaturefile value='$theSignatureFile'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name=thedoctor value='$theDoctor'>");
    print("</td>");
    print("<td colspan='2'>");
    print("</td>");
    print("</tr>");

    print("<tr><td colspan='6' align='center'>");
    print("<input type='submit' value=Update></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function IB_UpdatePatientTTO($SID,
                    $theHospitalNumber,
                    $theTelephoneNumber,
                    $theDateOfAdmission,
                    $theWard,
                    $theTelExt,
                    $theDateOfDischarge,
                    $theConsultant,
                    $theConsultantCode,
                    $theLetterToGPDr,
                    $theDischargeAddressContactNo,
                    $thePrimaryDiagnosis,
                    $theProblems,
                    $theConfidential,
                    $theNurseInformation,
                    $theIsNotRequired,
                    $theWillBePosted,
                    $theNextDate,
                    $theNextTime,
                    $theNextClinic,
                    $theTransport,
                    $theDistrictNursingService,
                    $theSocialServices,
                    $thePsychiatric,
                    $theOtherText,
                    $theSignatureFile,
                    $theDoctor) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $theIsNotRequired = $theIsNotRequired == 'on' ? 1 : 0;
    $theWillBePosted = $theWillBePosted == 'on' ? 1 : 0;
    $theTransport = $theTransport == 'on' ? 1 : 0;
    $theDistrictNursingService = $theDistrictNursingService == 'on' ? 1 : 0;
    $theSocialServices = $theSocialServices == 'on' ? 1 : 0;
    $thePsychiatric = $thePsychiatric == 'on' ? 1 : 0;

    // Update details of the TTO ...
    $data = array(
            'HospitalNumber'=>$theHospitalNumber,
            'TelephoneNumber'=>$theTelephoneNumber,
            'DateOfAdmission'=>$theDateOfAdmission,
            'Ward'=>$theWard,
            'TelExt'=>$theTelExt,
            'DateOfDischarge'=>$theDateOfDischarge,
            'Consultant'=>$theConsultant,
            'ConsultantCode'=>$theConsultantCode,
            'LetterToGPDr'=>$theLetterToGPDr,
            'DischargeAddressContactNo'=>$theDischargeAddressContactNo,
            'PrimaryDiagnosis'=>$thePrimaryDiagnosis,
            'Problems'=>$theProblems,
            'Confidential'=>$theConfidential,
            'NurseInformation'=>$theNurseInformation,
            'IsNotRequired'=>$theIsNotRequired,
            'WillBePosted'=>$theWillBePosted,
            'NextDate'=>$theNextDate,
            'NextTime'=>$theNextTime,
            'NextClinic'=>$theNextClinic,
            'Transport'=>$theTransport,
            'DistrictNursingService'=>$theDistrictNursingService,
            'SocialServices'=>$theSocialServices,
            'Psychiatric'=>$thePsychiatric,
            'OtherText'=>$theOtherText,
            'SignatureFile'=>$theSignatureFile,
            'Doctor'=>$theDoctor
    );

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripHospitalTTOPatient", $data, $condition);
    } else {
        $db->insert("ScripHospitalTTOPatient", $data);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Patient TTO details updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function J_EditFormData() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all form table and display as a table

    Z_DrawHeader();

    print("<TR>\n<TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one item from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>\n");
    print("<TR><TD colspan=2 align=center>\n<TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=71>\n");

    print("<tr>");
    print("<TH>&nbsp;</TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Name</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Hex Colour</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Colour</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Addict</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Special Text</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Group</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Order</FONT></TH>\n");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</FONT></TD>\n");
    print("</tr>");

    $table=$db->select("ScripForms");
    foreach ($table as $row) {
        $theName=$row['FormName'];
	$theColour=$row['FormColour'];
	$theColourName=$row['FormColourWords'];
	$theAddict=$row['Addict'];
	$theID=$row['FormID'];
	$theText=$row['SpecialText'];
	$theGroup=$row['GroupID'];
	$theOrder=$row['DisplayOrder'];
	print("<tr>");
        print("<TD><INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>\n");
 	print("<TD><FONT COLOR=FFFFFF>$theName</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$theColour</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$theColourName</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$theAddict</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>&nbsp;$theText</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$theGroup</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$theOrder</FONT></TD>\n");
	print("</tr>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>\n");    
    print("</FORM></TABLE>\n</TD></TR><TR><TD>\n");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>\n"); 
};

function JA_EditSingleForm($SID) {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the practice and display in a form
    if ($SID!='0') {  
        $table=$db->select("ScripForms",array('FormID'=>$SID));
        foreach ($table as $row) {
            $theName2=$row['FormName2'];
            $theName1=$row['FormName1'];
            $theName=$row['FormName'];
            $theColour=$row['FormColour'];
            $theColourName=$row['FormColourWords'];
            $theAddict=$row['Addict'];
            $theID=$row['FormID'];
            $theText=$row['SpecialText'];
            $theNameBox=$row['NameBox'];
            $theGroupID=$row['GroupID'];
            $theDisplayOrder=$row['DisplayOrder'];
            $theLetters=$row['Letters'];
            $thePrivateCD=$row['PrivateCD'];
            $theSupplementary=$row['Supplementary'];
            $theHospital=$row['Hospital'];
        };
    } else {
        $theName2="";
        $theName1="";
        $theName="";
	$theColour="";
	$theColourName="";
	$theAddict="";
	$theID="";
	$theText="";
	$theNameBox="";
	$theGroupID="";
        $theDisplayOrder="";
        $theLetters="";
        $thePrivateCD="";
        $theSupplementary="";
        $theHospital="";
    };

    print("<TR>");
    print("<TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=72>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<tr><td colspan='4'><font color='#ff9f9f'>");
    print("<b>Name</b></font></td></tr>");
    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Name 2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name 1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=theName2 VALUE='$theName2' ");
    print("SIZE=30></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theName1 VALUE='$theName1' ");
    print("SIZE=30></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theName VALUE='$theName' SIZE=30></TD>");
    print("</tr>");

    print("<tr><td colspan='4'><font color='#ff9f9f'>");
    print("<b>Colour</b></font></td></tr>");
    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=60\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Hex Colour</A></FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Colour</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=theColour VALUE='$theColour' SIZE=6></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theColourName VALUE='$theColourName' SIZE=15></TD>");
    print("</TR>");

    print("<tr><td colspan='4'><font color='#ff9f9f'>");
    print("<b>Miscellanous</b></font></td></tr>");
    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Order</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=63\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Group</A></FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=theDisplayOrder 
                                 VALUE='$theDisplayOrder' SIZE=5></TD>");
    print("<TD><SELECT NAME=group>");

    $newtable=$db->select("ScripFormGroups");
    foreach ($newtable as $newrow) {
        $theID=$newrow['GroupID'];
	$thename=$newrow['GroupName'];
	print("<OPTION VALUE=$theID ");
	if ($theID==$theGroupID) { print("SELECTED "); };
	print("> $thename");
    };
    print("</SELECT>\n</TD>");
    print("</TR>");

    print("<tr><td colspan='4'><font color='#ff9f9f'>");
    print("<b>Overprinting</b></font></td></tr>");
    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=61\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("NameBox?</A></FONT></TH>");
    print("<TH colspan=2 align=center><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=62\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Special Text</A></FONT></TH>");
    print("<th><font color='#ffffff'>Letters</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<select name='theNameBox'>");
    print("<option value=''>Select from ...");
    print("<option value='0'");
    if ($theNameBox == '0') { print(" selected"); };
    print(">No");
    print("<option value='1'");
    if ($theNameBox == '1') { print(" selected"); };
    print(">Yes");
    print("</select>");
    print("</td>");

    print("<TD COLSPAN=2>");
    print("<INPUT TYPE=TEXT NAME=theText VALUE='$theText' SIZE=50></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theLetters VALUE='$theLetters' ");
    print("SIZE=5></TD>");
    print("</TR>");

    print("<tr><td colspan='4'><font color='#ff9f9f'>");
    print("<b>Attributes of the form's layout</b></font></td></tr>");
    print("<tr>");
    print("<th><font color='#ffffff'>Addict</font>");
    print("</th>");
    print("<th><font color='#ffffff'>Private CD</font>");
    print("</th>");
    print("<th><font color='#ffffff'>Supplementary</font>");
    print("</th>");
    print("<th><font color='#ffffff'>Hospital</font>");
    print("</th>");
    print("</tr>");

    print("<tr>");
    print("<TD>");
    print("<select name='theAddict'>");
    print("<option value=''>Select from ...");
    print("<option value='0'");
    if ($theAddict == '0') { print(" selected"); };
    print(">Not addict - no back");
    print("<option value='1'");
    if ($theAddict == '1') { print(" selected"); };
    print(">ADDICT - WITH a back");
    print("</select>");
    print("</td>");

    print("<td>");
    print("<select name='privatecd'>");
    print("<option value=''>Select from ...");
    print("<option value='0'");
    if ($thePrivateCD == '0') { print(" selected"); };
    print(">NHS Number Used");
    print("<option value='1'");
    if ($thePrivateCD == '1') { print(" selected"); };
    print(">Private CD Number Used");
    print("</select>");
    print("</td>");

    print("<td>");
    print("<select name='supplementary'>");
    print("<option value=''>Select from ...");
    print("<option value='0'");
    if ($theSupplementary == '0') { print(" selected"); };
    print(">Main (e.g. G.P.)");
    print("<option value='1'");
    if ($theSupplementary == '1') { print(" selected"); };
    print(">Supplementary (e.g. nurse)");
    print("</select>");
    print("</td>");

    print("<td>");
    print("<select name='hospital'>");
    print("<option value=''>Select from ...");
    print("<option value='0'");
    if ($theHospital == '0') { print(" selected"); };
    print(">Not a hospital form");
    print("<option value='1'");
    if ($theHospital == '1') { print(" selected"); };
    print(">A hospital form");
    print("</select>");
    print("</td>");
    print("</tr>");

    print("<TR><TD colspan=5 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function JB_UpdateForm($SID, $theName2, $theName1, 
         $theName, $theColour, $theColourName,
	 $theNameBox, $theText, $theLetters, $thegroup, 
         $theDisplayOrder,
         $theAddict, $thePrivateCD, $theSupplementary,
         $theHospital) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
//006
    // Update details of the form 
    $formdata= array(
        "FormName2" => $theName2,
        "FormName1" => $theName1, 
	"FormName" => $theName,
        "FormColour" => $theColour,
        "FormColourWords" => $theColourName,
        "NameBox"=>$theNameBox,
        "SpecialText"=>$theText,
        "Letters"=>$theLetters,
        "GroupID"=>$thegroup,
        "DisplayOrder"=>$theDisplayOrder,
	"Addict" => $theAddict,
        "PrivateCD"=>$thePrivateCD,
        "Supplementary"=>$theSupplementary,
        "Hospital" => $theHospital
                        );
    Z_DrawHeader();

    if ($SID!='0') {
        $db->update("ScripForms",$formdata, array('FormID'=>$SID));
    } else {
        $db->insert("ScripForms",$formdata);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Form updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function K_EditModeratorData() {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all moderators display as a table
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one item from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=81>");

    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>First Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Username</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Password</FONT></TH>");

    print("<tr>");
    print("<td><input type=radio name=thekey value='0' checked></td>");
    print("<td colspan='4'><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripModerator");
    foreach ($table as $row) {
        $thefname=$row['FirstName'];
	$thelname=$row['LastName'];
	$theuname=$row['UserName'];
	$thepassword=$row['Password'];
        $thekey=$row['ModeratorID'];
	print("<TR><TD>");
        print("<INPUT TYPE=RADIO NAME=thekey VALUE=$thekey></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thefname</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thelname</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theuname</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$thepassword</FONT></TD>");
	print("</TR>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
}  

function KA_EditSingleModerator($theKey) {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the practice and display in a form */  
    if ($theKey!='0') {
        $table=$db->select("ScripModerator",array('ModeratorID'=>$theKey));
        foreach ($table as $row) {
            $FirstName=$row['FirstName'];
            $LastName=$row['LastName'];
            $UserName=$row['UserName'];
            $Password=$row['Password'];
        };
    } else {
        $FirstName="";
        $LastName="";
        $UserName="";
        $Password="";
    };
    print("<TR>");
    print("<TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=82>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$theKey>");

    print("<TR><TH><FONT COLOR=FFFFFF>First Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Username</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Password</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=thefname VALUE='$FirstName' SIZE=30></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=thelname VALUE='$LastName' SIZE=30></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theusername VALUE='$UserName' SIZE=15></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=thepassword VALUE='$Password'  SIZE=35></TD>");
    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");

};

function KB_UpdateModerator($firstname, $lastname, $username, $password, $ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* Update details of the form */  
    $formdata= array(
	"FirstName" => $firstname,
	"LastName" => $lastname,
	"UserName" => $username,
	"Password" => $password,
	);
    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('ModeratorID'=>$ID);
        $db->update("ScripModerator",$formdata, $condition);
    } else {
        $db->insert("ScripModerator",$formdata);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Moderator updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function L_EditScripsData() {
    $db=new dbabstraction();
/* get details of the scrip table and display as a table*/  
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one scrip from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=91>");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>Letter</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Form code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Patient code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Prescriber code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Drug 1 code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Drug 2 code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Misc code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Mark Sheet</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<td><INPUT TYPE=RADIO NAME=theLetter VALUE='0' checked>");
    print("</FONT></TD>");
    print("<td><font color=ffffff>New</td>");
    print("</TR>");

    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripsScrips","","Letter");

    foreach ($table as $row) {
        $Letter=$row['Letter'];
	$FormID=$row['FormID'];
	$PatientProblemID=$row['PatientProblemID'];
	$PrescriberProblemID=$row['PrescriberProblemID'];
	$DrugProblemID=$row['DrugProblemID'];
	$DrugProblemID2=$row['DrugProblemID2'];
	$MiscProblemID=$row['MiscProblemID'];
	$MarkSheet=$row['Counsel'];
	print("<TR>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=theLetter VALUE=$Letter></FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>$Letter");
	print("</FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>$FormID");
	print("</FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>");
	print("$PatientProblemID</FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>");
	print("$PrescriberProblemID</FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>");
	print("$DrugProblemID</FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>");
	print("$DrugProblemID2</FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>");
	print("$MiscProblemID</FONT></TD>");

	print("<TD><FONT COLOR=FFFFFF>");
	print("$MarkSheet</FONT></TD>");

	print("</TR>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function LA_EditSingleScript($theLetter) {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the scrip (if any) and display in a form
    if ($theLetter != '0') {
        $table=$db->select("ScripsScrips",array('Letter'=>$theLetter));
        foreach ($table as $row) {
            $FormID=$row['FormID'];
            $PatientProblemID=$row['PatientProblemID'];
            $PrescriberProblemID=$row['PrescriberProblemID'];
            $DrugProblemID=$row['DrugProblemID'];
            $DrugProblemID2=$row['DrugProblemID2'];
            $MiscProblemID=$row['MiscProblemID'];
            $Counsel=$row['Counsel'];
            $EntirelyHandwritten=$row['EntirelyHandwritten'];
            $InteractionMAID = $row['InteractionMAID'];
        };
    } else {
        $FormID="";
	$PatientProblemID="";
	$PrescriberProblemID="";
	$DrugProblemID="";
	$DrugProblemID2="";
	$MiscProblemID="";
	$Counsel="";
        $EntirelyHandwritten="";
        $InteractionMAID = "";
    };

    $condition = array('PatientProblemID'=>$PatientProblemID);
    $table=$db->select("ScripPatientProblem", $condition);
    foreach ($table as $row) {
        $PatientID=$row['PatientID'];
    };

    $table=$db->select("ScripPrescriberProblem",
       array('PrescriberProblemID'=>$PrescriberProblemID));
    foreach ($table as $row) {
        $PrescriberID=$row['PrescriberID'];
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=92>");
    print("<INPUT TYPE=HIDDEN NAME=theLetter value=$theLetter>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Letter</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Form</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Patient Problem</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Prescriber Problem</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<TD><input type=text name=theLetter");
    if ($theLetter!='0') { print(" value=$theLetter"); };
    print("></TD>");

    $table=$db->select("ScripForms");
    print("<td><select name=theFormID >");
    print("<option value='0'>Select from ..."); // ... show blank line at top
    foreach ($table as $row) {
        $theformID=$row['FormID'];
        $thefname=$row['FormName'];
        $thefname2=$row['FormName2'];
        $thefname1=$row['FormName1'];
        print("<OPTION VALUE=$theformID");
        if ($theformID==$FormID) { print (" SELECTED"); };
        print(">$thefname2");
        if ($thefname1) { print(" -- $thefname1"); };
        print("\n");
    };
    print("</SELECT></TD>");

    print("<TD><SELECT NAME=thePatientProblem>");
    print("<OPTION VALUE='0'>Select from ..."); // ... show blank line at top
    $ptable=$db->select("ScripPatientProblem");
    foreach ($ptable as $prow) {
        $thepatientproblemID=$prow['PatientProblemID'];
        $thepatientID=$prow['PatientID'];

        $condition=array("PatientID"=>$thepatientID);
        $pntable=$db->select("ScripPatient", $condition);
        foreach ($pntable as $pnrow) {
            $thefname=$pnrow['FirstName'];
            $thelname=$pnrow['LastName'];
        }
	print("<OPTION VALUE=$thepatientproblemID");
	if ($thepatientproblemID==$PatientProblemID) { print (" SELECTED"); };
	print(">$thefname $thelname ($thepatientproblemID)\n");
    }
    print("</SELECT></TD>");

    print("<TD><SELECT NAME=thePrescriberProblem>");
    print("<OPTION VALUE='0'>Select from ..."); // ... show blank line at top
    $ptable=$db->select("ScripPrescriberProblem");
    foreach ($ptable as $prow) {
        $theprescriberproblemID=$prow['PrescriberProblemID'];
        $theprescriberID=$prow['PrescriberID'];

        $condition=array("PrescriberID"=>$theprescriberID);
        $pntable=$db->select("ScripPrescriber", $condition);
        foreach ($pntable as $pnrow) {
            $thename=$pnrow['Name'];
        };
        print("<OPTION VALUE=$theprescriberproblemID");
        if ($theprescriberproblemID==$PrescriberProblemID) {
        print (" SELECTED"); };
        print(">$thename ($theprescriberproblemID)\n");
    };
    print("</SELECT></TD>");

    print("<tr>");
    print("<th><FONT COLOR=FFFFFF>Mark Sheet</FONT></TH>");
    print("<th><FONT COLOR=FFFFFF>Drug 1</FONT></TH>");
    print("<th><FONT COLOR=FFFFFF>Drug 2</FONT></TH>");
    print("<th><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=70\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Miscellaneous or OTC ID</A></FONT></TH>");
    print("</TR>");

// Note that Counsel field now contains mark sheet information! 
    print("<TR>");
    print("<TD><INPUT TYPE=TEXT");
    print(" NAME=theCounsel VALUE='$Counsel' SIZE=1>");
    print("</TD>");

    $table=$db->select("ScripDrugProblem","","DrugName");
    print("<TD><SELECT NAME=theDrug1 >");
    print("<OPTION VALUE='0'>Select from ...");
    foreach ($table as $row) {
        $theDrugProblemID=$row['DrugProblemID'];
        $thename=$row['DrugName'];
	print("<OPTION VALUE=$theDrugProblemID ");
	if ($theDrugProblemID==$DrugProblemID)
	    print("SELECTED ");
	print (">$thename ($theDrugProblemID)\n");
    }
    print("</SELECT></TD>");

    print("<TD>");
    print("<SELECT NAME=theDrug2 >");
    print("<OPTION VALUE='0'>Select from ...");
    $table=$db->select("ScripDrugProblem","","DrugName");
    foreach ($table as $row)
    {   $theDrugProblemID=$row['DrugProblemID'];
        $thename=$row['DrugName'];
	print("<OPTION VALUE=$theDrugProblemID ");
	if ($theDrugProblemID==$DrugProblemID2)
	    print("SELECTED ");
	print (">$thename ($theDrugProblemID)\n");
    }
    print("</SELECT>");
    print("</TD>");

    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theMisc SIZE=5 value=$MiscProblemID>");
    print("</TD>");
    print("</TR>");

    print("<tr>");
    print("<th><font color='#ffffff'>Entirely Handwritten</font></th>");
    print("<th colspan='2'><font color='#ffffff'>");
    print("InteractionMA ID</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input type='text' name='entirelyhandwritten' ");
    print("value='$EntirelyHandwritten'>");
    print("</td>");
    print("<td colspan='2'>");
    print("<input type='text' name='interactionmaid' ");
    print("value='$InteractionMAID'>");
    print("</td>");
    print("</tr>");

    print("<TR>");
    print("<TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD>");
    print("</TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function LB_UpdateScrips($theLetter,$theFormID, $thePatientProblem,
	$thePrescriberProblem, $theDrug1, $theDrug2, $theMisc, $theCounsel,
        $theEntirelyHandwritten, $InteractionMAID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

// Does the letter exist in table: Yes - Update; No - Insert 
    $condition=array("Letter"=>$theLetter);
    $table=$db->select("ScripsScrips", $condition);
    foreach ($table as $row) {
        $textexistence = $row['Letter'];
    };

/* Update details of the scrip*/  
    $scripdata= array(
	"FormID" => $theFormID,
	"PatientProblemID" => $thePatientProblem,
	"PrescriberProblemID" => $thePrescriberProblem,
	"DrugProblemID" => $theDrug1,
	"DrugProblemID2" => $theDrug2,
	"MiscProblemID" => $theMisc,
	"Counsel"=>$theCounsel,
	"EntirelyHandwritten"=>$theEntirelyHandwritten,
	"InteractionMAID"=>$InteractionMAID
                      );

    Z_DrawHeader();

    if ($theLetter == $textexistence) {
        $condition=array('Letter'=>$theLetter);
        $db->update("ScripsScrips",$scripdata, $condition);
    } else {
        $scripdata=array_merge($scripdata, array("Letter"=>$theLetter));
        $db->insert("ScripsScrips",$scripdata);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Scrip updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function update scrip */



function M_EditTermDataYearGroup(){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select (a) From A year group; and (b) <i>Either</i> an existing location from the drop-down box <i>or</i> type in a new location. ");
    print("<br>Finally click the submit button to go on and edit the details of the term pattern for that year group. ");
    print("<br>Note that if both types of location are entered, it will be the existing location that will be used.</h2></font>");
    print("</td></tr>");

    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='100'>");

    print("<tr>");
    print("<td>");
    print("<table>");

    // YEAR-GROUP ...
    print("<tr>");
    $nYears = 4;
    print("<font color='#ffffff'><b>Year Group:</b></font>");
    for ($i = 0; $i < $nYears; $i++){
        $yearGroup = $i + 1;
        print("<td width='20%'><input type='radio' name='yeargroup' value='".$yearGroup."'>");
        print("<font color='#ffffff'>$yearGroup</td>");
    };
    print("</tr>");

    // LOCATION ...
    $existingLocation = "";
    $table=$db->select("ScripTermPattern");
    foreach ($table as $row){
        // Read a location ...
        $tempLocation = $row['Location'];
        $notFound = true;
        for ($i = 0; $i < count($existingLocation); $i++){
            if ($tempLocation == $existingLocation[$i]){
                // Already in $existingLocation; no need to check further
                $notFound = false;
                break;
            } else {
            };
        };
        if ($notFound) { $existingLocation[] = $tempLocation; };
    };

    print("<tr><td>&nbsp;</td></tr>");
    print("<tr><td><font color='#ffffff'><b>Location</b></font></td></tr>");

    print("<tr>");
    // EXISTING LOCATION Drop-down text ...
    print("<td>");
    print("<font color='#ffffff'>Existing location: </font>");
    print("<select name='location'>");
    print("<option value=''>Select ...</option>");
    for ($k = 0; $k < count($existingLocation); $k++){
        print("<option value='$existingLocation[$k]'>" . $existingLocation[$k] . "</option>");
    };
    print("</select>");
    print("</td>");

    // NEW LOCATION Free text ...
    print("<td>");
    print("<font color='#ffffff'><i>or</i> New location: <input type='text' name='newlocation'></font>");
    print("</td>");

    print("</tr>");
    print("</table>");
    print("</td>");
    print("</tr>");

    // Submit button:
    print("<tr><td colspan='8' align=center><input value='Confirm Year-Group and Location' type='submit'></TD></TR>");

    // End of the form ...
    print("</form>");

    // Hyperlink for return to main menu ...
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");

    // End of the table ...
    print("</table>");

    // End of the cell and row ...
    print("</td></tr>");

    // End of the table ...
    print("</table>");

};


function M_EditTermData($yearGroup, $location) {
    $db=new dbabstraction();
    Z_DrawHeader();

    // Get details of the term table for the specified year-group and display as a table  

    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select one pattern (term) from the list (year $yearGroup in $location) and click the submit button to edit the details</h2></font>");
    print("</td></tr>");

    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name='tab' value='101'>");
    print("<input type='hidden' name='yeargroup' value='$yearGroup'>");
    print("<input type='hidden' name='location' value='$location'>");

    print("<tr>");
    print("<th>&nbsp;</th>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Week 1</font></th>");
    print("<th><font color='#ffffff'>Week 2</font></th>");
    print("<th><font color='#ffffff'>Week 3</font></th>");
    print("<th><font color='#ffffff'>Week 4</font></th>");
    print("<th><font color='#ffffff'>Week 5</font></th>");
    print("<th><font color='#ffffff'>Week 6</font></th>");
    print("<th><font color='#ffffff'>Week 7</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color='#FFFFFF'>New</td>");
    print("</tr>");

    $condition = array('YearGroup'=>$yearGroup, 'Location'=>$location);
    // ... this will allow us to use the old code

    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripTermPattern", $condition);
    foreach ($table as $row) {
        $thePracticeCode=$row['PracticeCode'];
        $theWk1=$row['wk1'];
        $theWk2=$row['wk2'];
        $theWk3=$row['wk3'];
        $theWk4=$row['wk4'];
        $theWk5=$row['wk5'];
        $theWk6=$row['wk6'];
        $theWk7=$row['wk7'];

        print("<TR>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$thePracticeCode></FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$thePracticeCode</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk1</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk2</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk3</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk4</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk5</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk6</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk7</FONT></TD>");
        print("</TR>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function MA_EditSingleTermPattern($SID, $yearGroup, $location){
    // Get details of the term pattern and display in a form

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripTermPattern",array('PracticeCode'=>$SID));
    Z_DrawHeader();
    if ($SID!='0') {
        foreach ($table as $row) {
            $theWk1=$row['wk1'];
            $theWk2=$row['wk2'];
            $theWk3=$row['wk3'];
            $theWk4=$row['wk4'];
            $theWk5=$row['wk5'];
            $theWk6=$row['wk6'];
            $theWk7=$row['wk7'];
        };
    } else {
        $theWk1="";
        $theWk2="";
        $theWk3="";
        $theWk4="";
        $theWk5="";
        $theWk6="";
        $theWk7="";
    };

    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='102'>");
    print("<input type='hidden' name='ID' value='$SID'>");
    print("<input type='hidden' name='yeargroup' value='$yearGroup'>");
    print("<input type='hidden' name='location' value='$location'>");

    print("<tr>");
    print("<th><font color='#ffffff'>wk1</font></th>");
    print("<th><font color='#ffffff'>wk2</font></th>");
    print("<th><font color='#ffffff'>wk3</font></th>");
    print("<th><font color='#ffffff'>wk4</font></th>");
    print("<th><font color='#ffffff'>wk5</font></th>");
    print("<th><font color='#ffffff'>wk6</font></th>");
    print("<th><font color='#ffffff'>wk7</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type='text' name='thewk1' value='$theWk1' size='5'></td>");
    print("<td><input type='text' name='thewk2' value='$theWk2' size='5'></td>");
    print("<td><input type='text' name='thewk3' value='$theWk3' size='5'></td>");
    print("<td><input type='text' name='thewk4' value='$theWk4' size='5'></td>");
    print("<td><input type='text' name='thewk5' value='$theWk5' size='5'></td>");
    print("<td><input type='text' name='thewk6' value='$theWk6' size='5'></td>");
    print("<td><input type='text' name='thewk7' value='$theWk7' size='5'></td>");
    print("<td></td>");

    print("<tr>");
    print("<td colspan='7' align='center'><input type='submit' value='Update'></td>");
    print("</tr>");

    print("</form></table>");
    print("</td></tr>");
    print("<tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>");
};

function MB_UpdateTermPattern($SID, $yearGroup, $location, $theWk1, $theWk2, $theWk3, $theWk4, $theWk5, $theWk6, $theWk7) {
    // Update details of the term pattern:

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $patterndata=array("YearGroup" => $yearGroup, "Location" => $location, 
       "wk1" => $theWk1, "wk2" => $theWk2, "wk3" => $theWk3, "wk4" => $theWk4, "wk5" => $theWk5, "wk6" => $theWk6, "wk7" => $theWk7);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('PracticeCode'=>$SID);
        $db->update("ScripTermPattern", $patterndata, $condition);
    } else {
        $db->insert("ScripTermPattern", $patterndata);
    };

    print("<TR><TD colspan=2>");
    print("<FONT COLOR='#FFFFFF'>Term Pattern updated for year $yearGroup in $location.</FONT>");
    print("</TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR>");
    print("</TABLE>");
};



function M_EditCheckingDataChooseGroup(){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select a year group and click the submit button to go on and edit the details of the checking pattern for that year group.</h2></font>");
    print("</td></tr>");

    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='105'>");

    for ($i = 0; $i < 4; $i++){
        $yearGroup = $i + 1;
        print("<tr>");
        print("<td width='20%'><input type='radio' name='yeargroup' value='".$yearGroup."'>");
        print("<font color='#ffffff'>$yearGroup</td>");
        print("</tr>");
    };

    // Submit button:
    print("<tr><td colspan='8' align=center><input value='Choose Year-Group' type='submit'></TD></TR>");

    // End of the form ...
    print("</form>");

    // Hyperlink for return to main menu ...
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");

    // End of the table ...
    print("</table>");

    // End of the cell and row ...
    print("</td></tr>");

    // End of the table ...
    print("</table>");

};

function M_EditCheckingData($yearGroup) {
    // Get details of the term table and display as a table:

    $db=new dbabstraction();
    Z_DrawHeader();

    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one pattern (checking) from the list and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION='scripadmin.php' METHOD='post'>");
    print("<INPUT TYPE=HIDDEN NAME='tab' value='106'>");

    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Practice Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Week</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Set Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Order</FONT></TH>");
    print("</TR>");

    print("<tr>");
    print("<td><input type='radio' name='ID' value='0' checked></td>");
    print("<td><font color='#ffffff'>New</td>");
    print("</tr>");

    $db->connect() or die ($db->getError()); 

    $condition = array('YearGroup'=>$yearGroup);
    // ... this will allow us to use the old code

    $table=$db->select("ScripCheckingAllPattern", $condition);
    foreach ($table as $row) {
        $theID=$row['ID'];
        $thePracticeCode=$row['PracticeCode'];
        $theWeek=$row['Week'];
        $theSetName=$row['SetName'];
        $theOrder=$row['Ordering'];
        print("<tr>");
        print("<td><font COLOR=FFFFFF>");
        print("<input type='radio' name='ID' value=$theID></font></td>");
        print("<td><font color='#ffffff'>$thePracticeCode</font></td>");
        print("<td><font color='#ffffff'>$theWeek</font></td>");
        print("<td><font color='#ffffff'>$theSetName</font></td>");
        print("<td><font color='#ffffff'>$theOrder</font></td>");
        print("</tr>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
} /* end of function edit term patterns */

function MA_EditSingleCheckingPattern($SID, $yearGroup){

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the checking pattern and display in a form   
    $table=$db->select("ScripCheckingAllPattern", array('ID'=>$SID));
    Z_DrawHeader();
    if ($SID!='0') {
        foreach ($table as $row) {
            $thePracticeCode=$row['PracticeCode'];
            $theWeek=$row['Week'];
            $theSetName=$row['SetName'];
            $theOrder=$row['Ordering'];
        };
    } else {
        $thePracticeCode="";
        $theWeek=="";
        $theSetName="";
        $theOrder="";
    };

    print("<tr>");
    print("<td colspan='2' align='center'><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='107'>");
    print("<input type='hidden' name='ID' value=$SID>");

    print("<tr>");
    print("<th><font color='#ffffff'>Practice Code</font></th>");
    print("<th><font color='#ffffff'>Week</font></th>");
    print("<th><font color='#ffffff'>Set Name</font></th>");
    print("<th><font color='#ffffff'>Order</font></th>");
    print("</tr>");

    print("<tr>\n");
    print("<td><input type='text' name=thepracticecode value='$thePracticeCode' size='5'></td>\n");
    print("<td><input type='text' name=theweek value='$theWeek' size='5'></td>\n");
    print("<td><input type='text' name=thesetname value='$theSetName' size='5'></td>\n");
    print("<td><input type='text' name=theorder value='$theOrder' size='5'></td>\n");
    print("</tr>\n");
    print("<tr><td colspan='4' align='center'><input type='submit' value='Update'></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
};

function MB_UpdateCheckingPattern($SID, $yearGroup, $thePracticeCode, $theWeek, $theSetName, $theOrder) {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Update details of the checking pattern ... 

    $data = array('YearGroup'=>$yearGroup, 'PracticeCode'=>$thePracticeCode, 'Week'=>$theWeek, 'SetName'=>$theSetName, 'Order'=>$theOrder);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripCheckingAllPattern", array('PracticeCode'=>$thePracticeCode, 'Week'=>$theWeek, 'SetName'=>$theSetName, 'Ordering'=>$theOrder), array('ID'=>$SID));
    } else {
        $db->insert("ScripCheckingAllPattern", $patterndata);
    };

    print("<tr><td colspan='2'><font color='#ffffff'>Checking Pattern updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};


function N_EditExamDataChooseGroup(){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select (a) From A year group; and (b) <i>Either</i> an existing location from the drop-down box <i>or</i> type in a new location. ");
    print("<br>Finally click the submit button to go on and edit the details of the exam pattern for that year group. ");
    print("<br>Note that if both types of location are entered, it will be the existing location that will be used.</h2></font>");
    print("</td></tr>");

    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='110'>");

    print("<tr>");
    print("<td>");
    print("<table>");

    // YEAR-GROUP ...
    print("<tr>");
    $nYears = 4;
    print("<font color='#ffffff'><b>Year Group:</b></font>");
    for ($i = 0; $i < $nYears; $i++){
        $yearGroup = $i + 1;
        print("<td width='20%'><input type='radio' name='yeargroup' value='".$yearGroup."'>");
        print("<font color='#ffffff'>$yearGroup</td>");
    };
    print("</tr>");

    // LOCATION ...
    $existingLocation = "";
    $table=$db->select("ScripTermPattern");
    foreach ($table as $row){
        // Read a location ...
        $tempLocation = $row['Location'];
        $notFound = true;
        for ($i = 0; $i < count($existingLocation); $i++){
            if ($tempLocation == $existingLocation[$i]){
                // Already in $existingLocation; no need to check further
                $notFound = false;
                break;
            } else {
            };
        };
        if ($notFound) { $existingLocation[] = $tempLocation; };
    };


    print("<tr><td>&nbsp;</td></tr>");
    print("<tr><td><font color='#ffffff'><b>Location</b></font></td></tr>");

    print("<tr>");
    // EXISTING LOCATION Drop-down text ...
    print("<td>");
    print("<font color='#ffffff'>Existing location: </font>");
    print("<select name='location'>");
    print("<option value=''>Select ...</option>");
    for ($k = 0; $k < count($existingLocation); $k++){
        print("<option value='$existingLocation[$k]'>" . $existingLocation[$k] . "</option>");
    };
    print("</select>");
    print("</td>");

    // NEW LOCATION Free text ...
    print("<td>");
    print("<font color='#ffffff'><i>or</i> New location: <input type='text' name='newlocation'></font>");
    print("</td>");

    print("</tr>");
    print("</table>");
    print("</td>");
    print("</tr>");

    // Submit button:
    print("<tr><td colspan='8' align=center><input value='Choose Year-Group' type='submit'></TD></TR>");

    // End of the form ...
    print("</form>");

    // Hyperlink for return to main menu ...
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");

    // End of the table ...
    print("</table>");

    // End of the cell and row ...
    print("</td></tr>");

    // End of the table ...
    print("</table>");
};






function N_EditExamData($yearGroup, $location) {
    // Get details of the exam table and display as a table:
    $db=new dbabstraction();

    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select one pattern (exam) from the list (year $yearGroup in $location) and click the submit button to edit the details</h2></font>");
    print("</td></tr>");

    print("<tr>");
    print("<td colspan='2' align='center'><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='111'>");
    print("<input type='hidden' name='yeargroup' value='$yearGroup'>");
    print("<input type='hidden' name='location' value='$location'>");

    print("<tr>");
    print("<th>&nbsp;</th>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Week 1</font></th>");
    print("<th><font color='#ffffff'>Week 2</font></th>");
    print("<th><font color='#ffffff'>Week 3</font></th>");
    print("<th><font color='#ffffff'>Week 4</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name='ID' value='0' checked></td>");
    print("<td><font color='#ffffff'>New</td>");
    print("</tr>");

    $condition = array('YearGroup'=>$yearGroup, 'Location'=>$location);
    // ... this will allow us to use the old code

    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripExamPatterns", $condition);
    foreach ($table as $row) {
        $theExamCode=$row['ExamCode'];
        $theWk1=$row['wk1'];
        $theWk2=$row['wk2'];
        $theWk3=$row['wk3'];
        $theWk4=$row['wk4'];
        print("<tr>");
        print("<td><FONT COLOR=FFFFFF><INPUT TYPE=RADIO NAME=ID VALUE=$theExamCode></FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theExamCode</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk1</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk2</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk3</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theWk4</FONT></TD>");
        print("</TR>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function NA_EditSingleExamPattern($SID, $yearGroup, $location) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the exam pattern and display in a form
    $table=$db->select("ScripExamPatterns",array('ExamCode'=>$SID));
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theWk1=$row['wk1'];
            $theWk2=$row['wk2'];
            $theWk3=$row['wk3'];
            $theWk4=$row['wk4'];
        };
    } else {
        $theWk1="";
        $theWk2="";
        $theWk3="";
        $theWk4="";
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=112>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");
    print("<input type='hidden' name='yeargroup' value='$yearGroup'>");
    print("<input type='hidden' name='location' value='$location'>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Week 1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Week 2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Week 3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Week 4</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<TD><INPUT TYPE=TEXT NAME=thewk1 VALUE='$theWk1' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=thewk2 VALUE='$theWk2' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=thewk3 VALUE='$theWk3' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=thewk4 VALUE='$theWk4' SIZE=5></TD>");
    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}  /* end of function edit single exam pattern */

function NB_UpdateExamPattern($SID, $yearGroup, $location, $theWk1, $theWk2, $theWk3, $theWk4) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Update details of the exam pattern:  
    $patterndata= array("YearGroup" => $yearGroup, "Location" => $location, "wk1" => $theWk1, "wk2" => $theWk2, "wk3" => $theWk3, "wk4" => $theWk4);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ExamCode'=>$SID);
        $db->update("ScripExamPatterns",$patterndata, $condition);
    } else {
        $db->insert("ScripExamPatterns",$patterndata);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Exam Pattern updated for year $yearGroup in $location.</FONT></TD></TR>");

    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function O_EditConfigurationData() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details from the config table and display in a form  
    $table=$db->select("ScripConfiguration");
    Z_DrawHeader();
    foreach ($table as $row) {
        $thePracticeStartDate=$row['PracticeStartDate'];
	$theExamStartDate1=$row['ExamStartDate1'];
	$theExamStartDate2=$row['ExamStartDate2'];
	$theExamStartDate3=$row['ExamStartDate3'];
	$theResitDate=$row['ResitDate'];
	$theByes=$row['Byes'];
	$theIPAddressLabs=$row['IPAddressLabs'];
	$theExamLength=$row['ExamLength'];
	$theDelayMinutes=$row['DelayMinutes'];
	$theDelaySeconds=$row['DelaySeconds'];
        $theCheckingStartDate=$row['CheckingStartDate'];
    };

    print("<tr><td colspan='2' align='center'>");

    print("<table border='1'>");

    print("<tr>");
    print("<td colspan='7'><font color='#ffffff'>");
    print("Practice and Examinations:</font>");
    print("</td>");
    print("</tr>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=121>");
    print("<TR><TH><FONT COLOR=FFFFFF>Practice Start<br>Y-M-D</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Exam Start 1<br>Y-M-D</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Exam Start 2<br>Y-M-D</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Exam Start 3<br>Y-M-D</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Lab IP</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Byes</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Resit Y-M-D</FONT></TH>");
    print("</TR>");

    print("<TR><TD>");
    print("<INPUT TYPE=TEXT NAME=thepracticestartdate ");
    print("SIZE=12 VALUE='$thePracticeStartDate'>\n");
    print("</TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theexamstartdate1 ");
    print("SIZE=12 VALUE='$theExamStartDate1'></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theexamstartdate2 ");
    print("SIZE=12 VALUE='$theExamStartDate2'></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theexamstartdate3 ");
    print("SIZE=12 VALUE='$theExamStartDate3'></TD>\n");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theipaddresslabs VALUE='$theIPAddressLabs'>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=thebyes VALUE='$theByes' SIZE=8>");
    print("</TD><TD><INPUT TYPE=TEXT NAME=theresitdate ");
    print("SIZE=12 VALUE='$theResitDate'>");
    print("</TD>");

    print("<tr>");
    print("<td colspan='7'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='7'><font color='#ffffff'>Checking:</font>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color='#ffffff'>Start Date</font></th>");
    print("<th><font color='#ffffff'>Delay Minutes</font></th>");
    print("<th><font color='#ffffff'>Delay Seconds</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type='text' name='thecheckingstartdate' ");
    print("value='$theCheckingStartDate'></td>");
    print("<td><input type='text' name='thedelayminutes' ");
    print("value='$theDelayMinutes'></td>");
    print("<td><input type='text' name='thedelayseconds' ");
    print("value='$theDelaySeconds'></td>");
    print("</tr>");

    print("<tr>");
    print("<td></td>");
    print("</tr>");

    print("<TR><TD colspan=7 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=7>Return to Menu</A>");
};

function OA_UpdateConfiguration($thePracticeStart, $theExamStart1,
	$theExamStart2, $theExamStart3, $theresitdate,
	$theByes, $theIPAddressLabs, $theCheckingStartDate,
        $theDelayMinutes, $theDelaySeconds) {

    $db=new dbabstraction();
    $db->connect()or die ($db->getError());
 
    // Update details of the configuration
    $patterndata= array(
	"PracticeStartDate" => $thePracticeStart,
	"ExamStartDate1" => $theExamStart1,
	"ExamStartDate2" => $theExamStart2,
	"ExamStartDate3" => $theExamStart3,
	"ResitDate" => $theresitdate,
	"Byes" => $theByes,
	"IPAddressLabs" => $theIPAddressLabs,
        "CheckingStartDate" => $theCheckingStartDate,
        "DelayMinutes" => $theDelayMinutes,
        "DelaySeconds" => $theDelaySeconds
	);
    Z_DrawHeader();
    $db->update("ScripConfiguration",$patterndata);
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Exam Pattern updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function P_EditOTCData() {
    // Edit text for an OTC [over the counter]:

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

//  Get details of the OTC text and display ...
    Z_DrawHeader();
    print("<tr>");
    print("<td colspan='2'>");
    print("<font color='#ffffff'><h2>Select one text from the list and click the submit button to edit the details.</h2></font>");
    print("</td>");
    print("</tr>");

    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name=tab value='131'>");

    print("<tr>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripOTCText");
    foreach ($table as $row) {
        $theOTCID=$row['OTCID'];
        $theOTCText=$row['OTCText'];
        print("<TR><TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theOTCID></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theOTCID");
        print("</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("$theOTCText</FONT></TD></TR>");
    };

    print("<TR><TD colspan=3><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function PA_EditSingleOTC($SID) {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

//  Get details of the OTC text and display in a form  
    $table=$db->select("ScripOTCText", array('OTCID'=>$SID));
    if ($SID!='0') {
        foreach ($table as $row) {
           $theOTCText=$row['OTCText'];
        };
    } else {
       $theOTCText="";
    };

    print("<tr><td colspan='2' align='center'><table border='1'>");

    print('<form method="get" action="scripadmin.php">');

    print("<input type='hidden' name='tab' value='132'>");
    print("<input type='hidden' name='ID' value='$SID'>");
    print("<tr><td><font color='#ffffff'></td></tr>");
    print("<tr><th><font color='#ffffff'>Text</font></th></tf>");
    print("<tr><td><font color='#ffffff'>");
    print("Do not use double quotation marks! Use either single quotation marks or, for extended sections of speech, italics.");
    print("</font></td></tr>");

    print("<tr><td>");

    print('<textarea cols="90" rows="25" name="theotctext" style="width:100%">');
    print($theOTCText.'</textarea>');

    print("</td></tr>");

    // Create a class instance.
    $CKEditor = new CKEditor();

    // Force a basic toolbar ...
//    $CKEditor->config['toolbar'] = 'Basic';

    // http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html



    // CKEDITOR.config.toolbar_Basic = [ [ 'Source', '-', 'Bold', 'Italic' ] ];
    // CKEDITOR.config.toolbar_Full = [
    // ['Source','-','Save','NewPage','Preview','-','Templates'],
    // ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    // '/',
    // ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    // ['Link','Unlink','Anchor'],
    // '/',
    // ['Styles','Format','Font','FontSize'],
    // ]; 


    // Defines a toolbar with only one strip containing the "Source" button, a separator graphic and the "Bold" and "Italic" buttons.
    $CKEditor->config['toolbar'] = array(array('Source','-', 'Cut', 'Copy', 'Paste', '-', 'Bold', 'Italic', 'Underline', '-', 'SpellChecker', 'Scayt', '-', 'Undo','Redo', '-', 'SpecialChar', '-', 'FontSize'));




//    $CKEditor->textareaAttributes = array("cols" => 8, "rows" => 10);

    // Replace a textarea element with an id (or name) of "textarea_id".
    $CKEditor->replace("theotctext");


// Details on config are at http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html


    print("<tr><td align='center'><input type='submit' value='Update'></td></tr>");

    print('</form>');

    print("</table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");

};

function PB_UpdateOTC($SID, $theOTCText) {
    $db=new dbabstraction();
    $db->connect()  or die ($db->getError());
 
/* Update details of the OTC text*/  
    $text= array( "OTCText" => $theOTCText );
   
    if ($SID!='0') {
        $condition = array('OTCID'=>$SID);
        $table=$db->update("ScripOTCText",$text, $condition);
    } else {
        $table=$db->insert("ScripOTCText",$text);
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>OTC text updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function Q_EditLegalData() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Get details of the legal table and display as a table
    $table=$db->select("ScripLegal");
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one text from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=141>");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Order</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td>&nbsp;</td>");
    print("<td><font color=ffffff>New Category</font></td>");
    print("</tr>");

    foreach ($table as $row) {
        $theLegalID=$row['LegalID'];
	$theLegalText=$row['LegalText'];
	$theDisplayOrder=$row['DisplayOrder'];
	print("<TR><TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theLegalID></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theLegalID");
	print("</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theLegalText</FONT></TD>");
	print("<TD align='right'><FONT COLOR=FFFFFF>");
	print("$theDisplayOrder</FONT></TD>");
	print("</TR>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function QA_EditSingleLegal($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Get details of the Legal text and display in a form
    $condition = array('LegalID'=>$SID);
    $table=$db->select("ScripLegal", $condition);
    Z_DrawHeader();

    if ($SID != '0') {
        foreach ($table as $row) {
           $theLegalText=$row['LegalText'];
           $theDisplayOrder=$row['DisplayOrder'];
           $theTwentyEightDays=$row['TwentyEightDays'];
        };
    } else {
        $theLegalText="";
        $theDisplayOrder="";
        $theTwentyEightDays="";
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=142>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Order</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Expires in ...</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<INPUT TYPE=TEXT NAME=thelegaltext VALUE='$theLegalText' SIZE=80>");
    print("</td>");
    print("<td><INPUT TYPE=TEXT NAME=theDisplayOrder ");
    print("VALUE='$theDisplayOrder' SIZE=10></td>");

    print("<td>");
    print("<select name='twentyeightdays'>");
    print("<option value=''>Select from ...");
    print("<option value='1'");
    if ($theTwentyEightDays == '1') { print(" selected"); };
    print(">28 days");
    print("<option value='0'");
    if ($theTwentyEightDays == '0') { print(" selected"); };
    print(">Six months");
    print("</td>");
    print("</tr>");


    print("<TR><TD align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");

    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function QB_Updatelegal($SID, $theLegalText, $theDisplayOrder,
                        $theTwentyEightDays) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the legal text ...
    $text= array("LegalText" => $theLegalText,
                 "DisplayOrder" => $theDisplayOrder,
                 "TwentyEightDays" => $theTwentyEightDays);

    if ($SID != '0') {
        $condition = array('LegalID'=>$SID);
        $table=$db->update("ScripLegal", $text, $condition);
    } else {
        $table=$db->insert("ScripLegal", $text);
    };

    Z_DrawHeader();

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Legal text updated");
    print("</FONT></TD></TR>");

    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function R_EditDrugProblemData() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the drug problem table and display as a table
    Z_DrawHeader();

    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one text from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR>");
    print("<TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=151>");

    print("<TR>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Drug</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Strength</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Dose</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Quantity</FONT></TH>");
    print("</TR>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripDrugProblem");
    foreach ($table as $row) {
        $theDrugProblemID=$row['DrugProblemID'];
	$theDrugName=$row['DrugName'];
	$theStrength=$row['Strength'];
	$theDose=$row['Dose'];
	$theQuantity=$row['Quantity'];
	print("<TR>");
        print("<TD><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theDrugProblemID></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theDrugProblemID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theDrugName&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theStrength&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theDose&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theQuantity&nbsp;</FONT></TD>");
	print("</TR>");
    }
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function RA_EditSingleDrug($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $table=$db->select("ScripDrugProblem",array('DrugProblemID'=>$SID));
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theDrugName=$row['DrugName'];
            $theDrugMispelling=$row['DrugMispelling'];
            $theDrugNameMissing=$row['DrugNameMissing'];
            $theDrugNameMispelled=$row['DrugNameMispelled'];
            $theDrugInappropriateChoice=$row['DrugInappropriateChoice'];
            $theDrugInappropriateChoiceComment=$row['DrugInappropriateChoiceComment'];
            $theDrugNonExist=$row['DrugNonExist'];
            $theStrength=$row['Strength'];
            $theStrengthCorrect=$row['StrengthCorrect'];
            $theStrengthMissing=$row['StrengthMissing'];
            $theStrengthUnavailable=$row['StrengthUnavailable'];
            $theDeliveryForm=$row['DeliveryForm'];
            $theFormCorrect=$row['FormCorrect'];
            $theDeliveryFormMissing=$row['DeliveryFormMissing'];
            $theDeliveryFormUnavailable=$row['DeliveryFormUnavailable'];
            $theDeliveryFormInappropriate=$row['DeliveryFormInappropriate'];
            $theDeliveryFormNotAllowed=$row['DeliveryFormNotAllowed'];
            $theDose=$row['Dose'];
            $theDoseCorrect=$row['DoseCorrect'];
            $theDoseOverdose=$row['DoseOverdose'];
            $theDoseUnderdose=$row['DoseUnderdose'];
            $theDoseIncomplete=$row['DoseIncomplete'];
            $theDoseNotRequired=$row['DoseNotRequired'];
            $theDoseInappropriateTiming=$row['DoseInappropriateTiming'];
            $theQuantity=$row['Quantity'];
            $theQuantityCorrect=$row['QuantityCorrect'];
            $theQuantityMissing=$row['QuantityMissing'];
            $theQuantityExcessivePO=$row['QuantityExcessivePO'];
            $theQuantityExcessiveLegal=$row['QuantityExcessiveLegal'];
            $theQuantityInsufficient=$row['QuantityInsufficient'];
            $theIntervalreq=$row['Intervalreq'];
            $theQuantityNoInterval=$row['QuantityNoInterval'];
            $theQuantityNoWords=$row['QuantityNoWords'];
            $theQuantityNoFigures=$row['QuantityNoFigures'];
            $thePurpose=$row['Purpose'];
            $thePurposeMarks=$row['PurposeMarks'];
            $theBlacklisted=$row['Blacklisted'];
            $theNotFormulary=$row['NotFormulary'];
            $theWrongScrip=$row['WrongScrip'];
            $theWrongPractioner=$row['WrongPractioner'];
            $theNosDays=$row['NosDays'];
            $theHospitalTTODetails=$row['HospitalTTODetails'];
            $theDrugHandwritten=$row['DrugHandwritten'];
            $theDrugHandCorrect=$row['DrugHandCorrect'];
            $theStrengthPMR=$row['StrengthPMR'];
            $theFormPMR=$row['FormPMR'];
            $theDosePMR=$row['DosePMR'];
            $thePMRComment=$row['PMRComment'];
        };
    } else {
        $theDrugName="";
	$theDrugMispelling="";
	$theDrugNameMissing="";
	$theDrugNameMispelled="";
	$theDrugInappropriateChoice="";
	$theDrugInappropriateChoiceComment="";
	$theDrugNonExist="";
	$theStrength="";
	$theStrengthCorrect="";
	$theStrengthMissing="";
	$theStrengthUnavailable="";
	$theDeliveryForm="";
	$theFormCorrect="";
	$theDeliveryFormMissing="";
	$theDeliveryFormUnavailable="";
	$theDeliveryFormInappropriate="";
	$theDeliveryFormNotAllowed="";
	$theDose="";
	$theDoseCorrect="";
	$theDoseOverdose="";
	$theDoseUnderdose="";
	$theDoseIncomplete="";
	$theDoseNotRequired="";
	$theDoseInappropriateTiming="";
	$theQuantity="";
	$theQuantityCorrect="";
	$theQuantityMissing="";
	$theQuantityExcessivePO="";
	$theQuantityExcessiveLegal="";
	$theQuantityInsufficient="";
	$theIntervalreq="";
	$theQuantityNoInterval="";
	$theQuantityNoWords="";
	$theQuantityNoFigures="";
	$thePurpose="";
	$thePurposeMarks="";
        $theBlacklisted="";
        $theNotFormulary="";
        $theWrongScrip="";
        $theWrongPractioner="";
        $theNosDays="";
        $theHospitalTTODetails="";
        $theDrugHandwritten="";
        $theDrugHandCorrect="";
        $theStrengthPMR="";
        $theFormPMR="";
        $theDosePMR="";
        $thePMRComment="";
    };
    print("<TR><TD colspan=2 align=center>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=152>\n");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>\n");

/*
    print("<TABLE BORDER=1>\n");
    print("<tr>");
    print("<th><FONT COLOR=FFFFFF>&nbsp;</th>");
    print("<th><FONT COLOR=FFFFFF>On Form</th>");
    print("<th><FONT COLOR=FFFFFF>Correction</th>");
    print("<th><FONT COLOR=FFFFFF>&nbsp;</th>");
    print("<th><FONT COLOR=FFFFFF>Problem</th>");
    print("<th><FONT COLOR=FFFFFF>Penalty</th>");
    print("</tr>");

    print("<tr>");
    print("<td><FONT COLOR=FFFFFF>Drug</td>");

    print("<td><INPUT TYPE=TEXT NAME=theDrugName ");
    print("VALUE='$theDrugName' SIZE=40></td>\n");
    print("<td><INPUT TYPE=TEXT NAME=theDrugMispelling ");
    print("VALUE='$theDrugMispelling' SIZE=30></td>\n");

    print("<td><FONT COLOR=FFFFFF>");
    print("<input type=radio name=drug value='$theDrugMispelling'>");
    print("Mis</td>");

    print("<td><FONT COLOR=FFFFFF>Problem</td>");
    print("<td><FONT COLOR=FFFFFF>Penalty</td>");
    print("</tr>");
    print("</table>");
*/

    print("<TABLE BORDER=1>\n");

    // LINE 1 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=1\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug on form</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=2\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug replace value</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=3\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug Name Missing</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=4\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug Name Mispelled</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=5\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug Inappropriate</A></FONT></TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theDrugName ");
    print("VALUE='$theDrugName' SIZE=40></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugMispelling ");
    print("VALUE='$theDrugMispelling' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugNameMissing ");
    print("VALUE='$theDrugNameMissing' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugNameMispelled ");
    print("VALUE='$theDrugNameMispelled' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugInappropriateChoice ");
    print("VALUE='$theDrugInappropriateChoice' SIZE=5></TD></TR>\n");

    // LINE 1A of form
    print("<tr><td colspan='5'><font color='#ffffff'>");
    print("Comment on why choice is inappropriate (but not PMR - see below for PMR)");
    print("</font></td></tr>");
    print("<tr><td colspan='5'>");
    print("<input type='text' name='theDrugInappropriateChoiceComment'");
    print(" value='$theDrugInappropriateChoiceComment' size='120'>");
    print("</td></tr>");

    // LINE 2 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=6\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Strength</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=7\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Correct Strength</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=8\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug Not Exist</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=9\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Strength Missing</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
//print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/scripadmin.php?tab=500&index=10\''); // old version
print('onClick="open(\'scripadmin.php?tab=500&index=10\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Strength Unavailable</A></FONT></TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theStrength ");
    print("VALUE='$theStrength' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theStrengthCorrect ");
    print("VALUE='$theStrengthCorrect' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugNonExist ");
    print("VALUE='$theDrugNonExist' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theStrengthMissing ");
    print("VALUE='$theStrengthMissing' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theStrengthUnavailable ");
    print("VALUE='$theStrengthUnavailable' SIZE=5></TD>\n");

    // LINE 3 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
// print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/scripadmin.php?tab=500&index=11\'');
print('onClick="open(\'scripadmin.php?tab=500&index=11\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Delivery form</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
// print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/scripadmin.php?tab=500&index=12\'');
print('onClick="open(\'scripadmin.php?tab=500&index=12\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Correct Form</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
// print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/scripadmin.php?tab=500&index=13\'');
print('onClick="open(\'scripadmin.php?tab=500&index=13\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Form Missing</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
//print('onClick="open(\'http://www.nottingham.ac.uk/~pazscrip/scripware/scripadmin.php?tab=500&index=14\'');
print('onClick="open(\'scripadmin.php?tab=500&index=14\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Form Unavailable</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=15\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Form Inappropriate</A></FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryForm ");
    print("VALUE='$theDeliveryForm' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theFormCorrect ");
    print("VALUE='$theFormCorrect' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormMissing ");
    print("VALUE='$theDeliveryFormMissing' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormUnavailable ");
    print("VALUE='$theDeliveryFormUnavailable' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormInappropriate ");
    print("VALUE='$theDeliveryFormInappropriate' SIZE=5></TD>\n");

    // LINE 4 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=16\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Dose</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=17\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Correct Dose</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=18\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Overdose</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=19\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Dose Incomplete</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=20\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Dose not req.</A></FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDose ");
    print("VALUE='$theDose' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseCorrect ");
    print("VALUE='$theDoseCorrect' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseOverdose ");
    print("VALUE='$theDoseOverdose' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseIncomplete ");
    print("VALUE='$theDoseIncomplete' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseNotRequired ");
    print("VALUE='$theDoseNotRequired' SIZE=5></TD>\n");

    // LINE 5 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=21\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Form Not Allowed</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=22\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=23\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Interval Required</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=24\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Underdose</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=25\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Inappropriate Timing</A></FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormNotAllowed ");
    print("VALUE='$theDeliveryFormNotAllowed' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantity ");
    print("VALUE='$theQuantity' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theIntervalreq ");
    print("VALUE='$theIntervalreq' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseUnderdose ");
    print("VALUE='$theDoseUnderdose' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseInappropriateTiming ");
    print("VALUE='$theDoseInappropriateTiming' SIZE=5></TD></TR>\n");

    // LINE 6 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=26\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Correct Quantity</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=27\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity Missing</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=28\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity XS (PO)</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=29\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity Insufficient</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=30\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity No interval</A></FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityCorrect ");
    print("VALUE='$theQuantityCorrect' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityMissing ");
    print("VALUE='$theQuantityMissing' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityExcessivePO ");
    print("VALUE='$theQuantityExcessivePO' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityInsufficient ");
    print("VALUE='$theQuantityInsufficient' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityNoInterval ");
    print("VALUE='$theQuantityNoInterval' SIZE=5></TD></TR>\n");

    // LINE 7 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=31\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity no words</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=32\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity No Figures</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=33\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Quantity XS (Legal)</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=34\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Wrong Script</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=35\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Practioner not allowed</A></FONT></TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theQuantityNoWords ");
    print("VALUE='$theQuantityNoWords' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityNoFigures ");
    print("VALUE='$theQuantityNoFigures' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityExcessiveLegal ");
    print("VALUE='$theQuantityExcessiveLegal' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theWrongScrip ");
    print("VALUE='$theWrongScrip' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theWrongPractioner ");
    print("VALUE='$theWrongPractioner' SIZE=5></TD></TR>\n");

    // LINE 8 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=36\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Number of Days</A></FONT></TH>\n");

print("<th><FONT COLOR=FFFFFF>Hospital TTO Details</font></th>");
print("<th>&nbsp;</th>");

/* Purpose doesn't belong to this table
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=37\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Purpose</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=38\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Marks for Purpose</A></FONT></TH>\n");
*/

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=39\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Blacklisted</A></FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=40\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Not in Formulary</A></FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theNosDays ");
    print("VALUE='$theNosDays' SIZE=5></TD>\n");

    print("<td><input name='theHospitalTTODetails' value='$theHospitalTTODetails' ></td>");
print("<td></td>");

/* Purpose doesn't belong in this table
    print("<TD><INPUT TYPE=TEXT NAME=thePurpose ");
    print("VALUE='$thePurpose' SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=thePurposeMarks ");
    print("VALUE='$thePurposeMarks' SIZE=5></TD>\n");
*/

    print("<TD><INPUT TYPE=TEXT NAME=theBlacklisted ");
    print("VALUE='$theBlacklisted' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theNotFormulary ");
    print("VALUE='$theNotFormulary' SIZE=5></TD></TR>\n");

    // LINE 9 of form
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=41\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug must be handwritten</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=42\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Does drug appear handwritten</A></FONT></TH>\n");
     print("<TH>&nbsp;</TH>\n");
    print("<TH>&nbsp;</TH>\n");
    print("<TH>&nbsp;</TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theDrugHandwritten ");
    print("VALUE='$theDrugHandwritten' SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugHandCorrect ");
    print("VALUE='$theDrugHandCorrect' SIZE=5></TD>\n");
    print("<TD>&nbsp;</TD>\n");
    print("<TD>&nbsp;</TD>\n");
    print("<TD>&nbsp;</TD></TR>\n");

// LINE 10 of form
    print("<tr>");
    print("<td colspan='5'>");
    print("<font color='#ffffff'><b>");
    print("Relating to the patient medication record (PMR) ...");
    print("</b></font>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color='#ffffff'>");
    print("StrengthPMR");
    print("</th>");
    print("<th><font color='#ffffff'>");
    print("FormPMR");
    print("</th>");
    print("<th colspan='3'><font color='#ffffff'>");
    print("DosePMR");
    print("</th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input type='text' name='strengthpmr' value='$theStrengthPMR'>");
    print("</td>");
    print("<td>");
    print("<input type='text' name='formpmr' value='$theFormPMR'>");
    print("</td>");
    print("<td colspan='3'>");
    print("<input type='text' name='dosepmr' value='$theDosePMR'>");
    print("</td>");
    print("</tr>");

// LINE 11 of form
    print("<tr><td colspan='5'><font color='#ffffff'>");
    print("Comment on PMR mismatch");
    print("</font></td></tr>");
    print("<tr><td colspan='5'>");
    print("<input type='text' name='thePMRComment'");
    print(" value='$thePMRComment' size='120'>");
    print("</td></tr>");

// LINE 12 of form - The button
    print("<TR><TD colspan=5 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>\n");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>\n");
    print("</TD></TR></TABLE>\n");
};

function RB_UpdateDrugProblem($SID, $theDrugName, $theDrugMispelling, 
	$theDrugNameMissing,$theDrugNameMispelled,
	$theDrugInappropriateChoice, $theDrugInappropriateChoiceComment,
        $theDrugNonExist,$theStrength,
	$theStrengthCorrect, $theStrengthMissing, 		    
	$theStrengthUnavailable,$theDeliveryForm, $theFormCorrect, 
	$theDeliveryFormMissing,$theDeliveryFormUnavailable, 		    
	$theDeliveryFormInappropriate,$theDeliveryFormNotAllowed,  
	$theDose,$theDoseOverdose,$theDoseIncomplete, 		    
	$theDoseNotRequired,$theDoseInappropriate, 	
	$theQuantity,$theQuantityMissing,$theQuantityExcessivePO,   
	$theQuantityInsufficient,$theQuantityNoInterval, $theQuantityNoWords,  
	$thePurpose, $theBlacklisted, $theNotFormulary, $theWrongScrip, 
	$theWrongPractioner, $theDoseUnderdose, $theQuantityExcessiveLegal,
	$theQuantityNoFigures, $thePurposeMarks, $theDoseCorrect,
	$theNosDays, $theHospitalTTODetails, $theDrugHandwritten, $theDrugHandCorrect,
	$theDoseInappropriateTiming, $theQuantityCorrect, $theIntervalreq,
        $theStrengthPMR, $theFormPMR, $theDosePMR, $thePMRComment) {

    // Update details of the drug problem
    $text= array("DrugName"=>$theDrugName,
     "DrugMispelling"=>$theDrugMispelling,
     "DrugNameMissing"=>$theDrugNameMissing,
     "DrugNameMispelled"=>$theDrugNameMispelled,
     "DrugInappropriateChoice"=>$theDrugInappropriateChoice,
     "DrugInappropriateChoiceComment"=>$theDrugInappropriateChoiceComment,
     "DrugNonExist"=>$theDrugNonExist,
     "Blacklisted"=>$theBlacklisted,
     "NotFormulary"=>$theNotFormulary,
     "WrongScrip"=>$theWrongScrip,
     "WrongPractioner"=>$theWrongPractioner,
     "Strength"=>$theStrength,
     "StrengthCorrect"=>$theStrengthCorrect,
     "StrengthMissing"=>$theStrengthMissing,
     "StrengthUnavailable"=>$theStrengthUnavailable,
     "DeliveryForm"=>$theDeliveryForm,
     "FormCorrect"=>$theFormCorrect,
     "DeliveryFormMissing"=>$theDeliveryFormMissing,
     "DeliveryFormUnavailable"=>$theDeliveryFormUnavailable,
     "DeliveryFormInappropriate"=>$theDeliveryFormInappropriate,
     "DeliveryFormNotAllowed"=>$theDeliveryFormNotAllowed,
     "Dose"=>$theDose,
     "DoseCorrect"=>$theDoseCorrect,
     "DoseOverdose"=>$theDoseOverdose,
     "DoseUnderdose"=>$theDoseUnderdose,
     "DoseIncomplete"=>$theDoseIncomplete,
     "DoseNotRequired"=>$theDoseNotRequired,
     "DoseInappropriateTiming"=>$theDoseInappropriateTiming,
     "Quantity"=>$theQuantity,
     "QuantityCorrect"=>$theQuantityCorrect,
     "QuantityMissing"=>$theQuantityMissing,
     "QuantityExcessivePO"=>$theQuantityExcessivePO,
     "QuantityExcessiveLegal"=>$theQuantityExcessiveLegal,
     "QuantityInsufficient"=>$theQuantityInsufficient,
     "Intervalreq"=>$theIntervalreq,
     "QuantityNoInterval"=>$theQuantityNoInterval,
     "QuantityNoWords"=>$theQuantityNoWords,
     "QuantityNoFigures"=>$theQuantityNoFigures,
     "StrengthPMR"=>$theStrengthPMR,
     "FormPMR"=>$theFormPMR,
     "DosePMR"=>$theDosePMR,
     "PMRComment"=>$thePMRComment,
     "Purpose"=>$thePurpose,
     "PurposeMarks"=>$thePurposeMarks,
     "NosDays"=>$theNosDays,
     "HospitalTTODetails"=>$theHospitalTTODetails,
     "DrugHandwritten"=>$theDrugHandwritten,
     "DrugHandCorrect"=>$theDrugHandCorrect
     );

    $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 
    
    if ($SID!='0') {
        $condition=array('DrugProblemID'=>$SID);
        $table=$db->update("ScripDrugProblem",$text,$condition);
    } else {
        $table=$db->insert("ScripDrugProblem",$text);
    };

    $db->close();
    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Drug Problem updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function S_EditPatientProblemData() {
    $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 

/* Get details from the patient problem table and display in a form */  
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=161>\n");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>&nbsp;</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Patient</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>ForeName Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Surname Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Address Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Address Incomplete</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Name and Address Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Age Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>DOB Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Age - DOB mismatch</FONT></TH>\n");
    print("</TR>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripPatientProblem");
    foreach ($table as $row) {
        $PatientProblemID=$row['PatientProblemID'];
	$PatientID=$row['PatientID'];
	$PatientFNameMissing=$row['PatientFNameMissing'];
	$PatientSNameMissing=$row['PatientSNameMissing'];
	$PatientAddressMissing=$row['PatientAddressMissing'];
	$PatientAddressIncomplete=$row['PatientAddressIncomplete'];
	$NandA=$row['NameAndAddress'];
	$PatientAgeMissing=$row['PatientAgeMissing'];
	$PatientDOBMissing=$row['PatientDOBMissing'];
	$PatientAgeDOBMismatch=$row['PatientAgeDOBMismatch'];

        $patient=$db->select("ScripPatient",array('PatientID'=>$PatientID));
	foreach ($patient as $person) {
	    $fname=$person['FirstName'];
	    $lname=$person['LastName'];
	};

	$thePatient=$fname." ".$lname." (".$PatientID.")";
 	print("<tr>");
	print("<td><input type=radio name=ID value=$PatientProblemID>");
        print("<font color=FFFFFF>$PatientProblemID</font></td>\n");
 	print("<td><font color=FFFFFF>$thePatient</font></td>\n");
 	print("<td><font color=FFFFFF>$PatientFNameMissing</font></td>\n");
 	print("<td><font color=FFFFFF>$PatientSNameMissing</font></td>\n");
 	print("<td><font color=FFFFFF>$PatientAddressMissing</font></td>\n");
 	print("<td><font
                color=FFFFFF>$PatientAddressIncomplete</font></td>\n");
        print("<td><font color=FFFFFF>$NandA</font></td>\n");
        print("<td><font color=FFFFFF>$PatientAgeMissing</font></td>\n");
        print("<td><font color=FFFFFF>$PatientDOBMissing</font></td>\n");
        print("<td><font color=FFFFFF>$PatientAgeDOBMismatch</font></td>\n");
        print("</tr>\n");
    };

    print("<tr>");
    print("<td colspan=8 align=center>");
    print("<input type=submit value=Update></td></tr>\n");
    print("</form></table>\n</td></tr>");
    print("<tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>\n");
};

function SA_EditSinglePatientProblem($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the Patient Problem and display in a form
    Z_DrawHeader();

    if ($SID!='0') {
    $table=$db->select("ScripPatientProblem",array('PatientProblemID'=>$SID)); 
        foreach ($table as $row) {
            $thePatientID=$row['PatientID'];
            $theFName=$row['PatientFNameMissing'];
            $theSName=$row['PatientSNameMissing'];
            $theAddress=$row['PatientAddressMissing'];
            $theAddressIncom=$row['PatientAddressIncomplete'];
            $theNandA=$row['NameAndAddress'];
            $theAge=$row['PatientAgeMissing'];
            $theDOB=$row['PatientDOBMissing'];
            $theAgeDOB=$row['PatientAgeDOBMismatch'];
            $theSText=$row['SpecialText'];
        };

        $table=$db->select("ScripPatient",array('PatientID'=>$thePatientID)); 
        foreach ($table as $row) {
            $thefname=$row['FirstName'];
            $thelname=$row['LastName'];
        };
        $thePatient=$thefname." ".$thelname;
    } else {
	$theFName="";
	$theSName="";
	$theAddress="";
	$theIncomAddress="";
	$theAge="";
	$theDOB="";
        $theNandA="";
	$theAgeDOB="";
	$theSText="";
        $thePatient="";
    };

    print("<tr>");
    print("<TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=162>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Patient</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>ForeName Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Surname Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address Incomplete</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name and Address Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Age missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>DOB Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=80\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Age DOB Mismatch</A></FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><select name=thePatientID>");
    if ($SID=='0') {
      print("<option value='0' selected>");
    };

    $ptable=$db->select("ScripPatient","","LastName");
    foreach ($ptable as $prow) {
        $patientID=$prow['PatientID'];
	$firstName=$prow['FirstName'];
	$lastName=$prow['LastName'];
        print("<option value='$patientID'");
        if ($patientID==$thePatientID) { print(" selected"); };
        print(">$firstName $lastName ($patientID)");
    };
    print("</select></td>");

    print("<TD><INPUT TYPE=TEXT NAME=theFName ");
    print("VALUE='$theFName' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theSName ");
    print("VALUE='$theSName' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theAddress ");
    print("VALUE='$theAddress' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theAddressIncom ");
    print("VALUE='$theAddressIncom' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theNandA ");
    print("VALUE='$theNandA' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theAge ");
    print("VALUE='$theAge' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theDOB ");
    print("VALUE='$theDOB' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theAgeDOB ");
    print("VALUE='$theAgeDOB' SIZE=5></TD></TR>");
    print("<TR><TH colspan=7><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=81\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Special Text</A></FONT></TH>");
    print("</TR><TR>");
    print("<TD colspan=7><INPUT TYPE=TEXT NAME=theSText ");
    print("VALUE='$theSText' SIZE=80></TD>");
    print("</TR><TR><TD align=center colspan=7>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function edit single patient problem */

function SB_UpdatePatientProblem($SID, $thePatientID, $theFName,
        $theSName, $theAddress, $theAddressIncom, $theNandA,
	$theAge, $theDOB, $theAgeDOB, $theSText) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* Update details of the patient problem */  

    $text= array(
	"PatientID" => $thePatientID,
	"PatientFNameMissing" => $theFName,
	"PatientSNameMissing" => $theSName,
	"PatientAddressMissing" => $theAddress,
	"PatientAddressIncomplete" => $theAddressIncom,
        "NameAndAddress" => $theNandA,
	"PatientAgeMissing" => $theAge,
	"PatientDOBMissing" => $theDOB,
	"PatientAgeDOBMismatch" => $theAgeDOB,
	"SpecialText"=>$theSText );

    if ($SID!='0') {
        $condition=array('PatientProblemID'=>$SID);
        $table=$db->update("ScripPatientProblem",$text,$condition);
    } else {
        $table=$db->insert("ScripPatientProblem",$text);
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Patient Problem updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function update patient problem */

function T_EditPrescriberProblemData() {
    $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 

/* Get details from the prescriber problem table and display in a form */  
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=171>\n");

    print("<tr>");
    print("<th><FONT COLOR=FFFFFF></FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Prescriber Problem</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Prescriber</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Name Missing</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>NHS No Missing</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Address Missing</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Address Incomplete</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Address Not UK</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Quals Missing</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Quals Inappropriate</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Sig Missing</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Sig Incorrect</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Authorise needed</FONT></TH>\n");
    print("<th><FONT COLOR=FFFFFF>Auth ID</FONT></TH>\n");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=FFFFFF>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripPrescriberProblem");
    foreach ($table as $row) {
        $PrescriberProblemID=$row['PrescriberProblemID'];
	$PrescriberID=$row['PrescriberID'];
	$PrescriberNameMissing=$row['PrescriberNameMissing'];
	$PrescriberNHSNoMissing=$row['PrescriberNHSNoMissing'];
	$PrescriberAddressMissing=$row['PrescriberAddressMissing'];
	$PrescriberAddressIncomplete=$row['PrescriberAddressIncomplete'];
	$PrescriberAddressNonUK=$row['PrescriberAddressNonUK'];
	$PrescriberQualsMissing=$row['PrescriberQualsMissing'];
	$PrescriberQualsInappropriate=$row['PrescriberQualsInappropriate'];
	$PrescriberSigMissing=$row['PrescriberSigMissing'];
	$PrescriberSigIncorrect=$row['PrescriberSigIncorrect'];
	$PrescriberAuthoriseNeeded=$row['PrescriberAuthoriseNeeded'];
	$AuthorisingID=$row['AuthorisingID'];
 	print("<TR>");
        print("<td><INPUT TYPE=RADIO NAME=ID VALUE=$PrescriberProblemID></td>");
	print("<td><FONT COLOR=FFFFFF>$PrescriberProblemID</FONT></td>\n");
	print("<td><FONT COLOR=FFFFFF>$PrescriberID</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberNameMissing</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberNHSNoMissing</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberAddressMissing</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberAddressIncomplete</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberAddressNonUK</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberQualsMissing</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberQualsInappropriate</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberSigMissing</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberSigIncorrect</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$PrescriberAuthoriseNeeded</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$AuthorisingID</FONT></TD>");
	print("</tr>\n");
    }

    print("<tr><TD colspan=12 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>\n");
    print("</FORM></TABLE>\n</TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>\n");
} /* end of function edit prescriber problem */

function TA_EditSinglePrescriberProblem($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the Prescriber Problem and display in a form */  
    $condition=array('PrescriberProblemID'=>$SID);
    $table=$db->select("ScripPrescriberProblem",$condition); 
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $thePrescriberID=$row['PrescriberID'];
            $theName=$row['PrescriberNameMissing'];
            $theNHSNo=$row['PrescriberNHSNoMissing'];
            $theAddress=$row['PrescriberAddressMissing'];
            $theAddressIncomplete=$row['PrescriberAddressIncomplete'];
            $theAddressNonUK=$row['PrescriberAddressNonUK'];
            $theUKAddress1=$row['UKAddress1'];
            $theUKAddress2=$row['UKAddress2'];
            $theUKAddress3=$row['UKAddress3'];
            $theUKAddress4=$row['UKAddress4'];
            $theQuals=$row['PrescriberQualsMissing'];
            $theQualsInappropriate=$row['PrescriberQualsInappropriate'];
            $theSig=$row['PrescriberSigMissing'];
            $theSigIncorrect=$row['PrescriberSigIncorrect'];
            $theAuthorise=$row['PrescriberAuthoriseNeeded'];
            $theAuthorisingID=$row['AuthorisingID'];
            $theIncorrectSigFile=$row['IncorrectSigFile'];
        };
        $condition=array('PrescriberID'=>$thePrescriberID);
        $table=$db->select("ScripPrescriber",$condition); 
        foreach ($table as $row) {
            $thePrescriber=$row['Name'];
        };
    } else {
	$theName="";
	$theNHSNo="";
	$theAddress="";
	$theAddressIncomplete="";
	$theAddressNonUK="";
	$theUKAddress1="";
	$theUKAddress2="";
	$theUKAddress3="";
	$theUKAddress4="";
	$theQuals="";
	$theQualsInappropriate="";
	$theSig="";
	$theSigIncorrect="";
	$theAuthorise="";
        $thePrescriber="";
        $theAuthorisingID="";
        $theIncorrectSigFile="";
    };
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=172>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<TR>");
    print("<TR><TH><FONT COLOR=FFFFFF>Prescriber Problem</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Prescriber</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Name Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>NHS No Missing</FONT></TH>\n");
    print("</tr>");

    print("<TR>");
    print("<TD><FONT COLOR=FFFFFF>$SID</FONT></TD>");

    print("<TD><FONT COLOR=FFFFFF>");
    print("<SELECT NAME='prescriber'>");
    if ($SID=='0') { print("<option value='0' selected>"); };
    $ptable=$db->select("ScripPrescriber");
    foreach ($ptable as $prow) {
        $name=$prow['Name'];
        $prescriberID=$prow['PrescriberID'];
        print("<OPTION VALUE=$prescriberID");
        if ($prescriberID==$thePrescriberID) { print(" SELECTED"); };
	print("> $name ($prescriberID)");
     };
    print("</select></td>\n");

    print("<TD><INPUT TYPE=TEXT NAME=theName ");
    print("VALUE='$theName' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theNHSNo ");
    print("VALUE='$theNHSNo' SIZE=5></TD>");
    print("</TR>");

    /* Address */
    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Address Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Address Incomplete</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Address Not UK</FONT></TH>\n");
    print("</tr>");
    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=theAddress ");
    print("VALUE='$theAddress' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theAddressIncomplete ");
    print("VALUE='$theAddressIncomplete' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theAddressNonUK ");
    print("VALUE='$theAddressNonUK' SIZE=5></TD>");
    print("</tr>");

    /* UK Address */
    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>UK Address 1</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>UK Address 2</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>UK Address 3</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>UK Address 4</FONT></TH>\n");
    print("</tr>");
    print("<tr>");
    print("<TD><INPUT TYPE=TEXT size='20' NAME=theUKAddress1 ");
    print("VALUE='$theUKAddress1' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT size='20' NAME=theUKAddress2 ");
    print("VALUE='$theUKAddress2' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT size='20' NAME=theUKAddress3 ");
    print("VALUE='$theUKAddress3' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT size='20' NAME=theUKAddress4 ");
    print("VALUE='$theUKAddress4' SIZE=5></TD>");
    print("</tr>");

    /* Qualifications */
    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Quals Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quals Inappropriate</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>&nbsp;</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Inappropriate Sig File</FONT></TH>\n");
    print("</tr>");
    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=theQuals ");
    print("VALUE='$theQuals' SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theQualsInappropriate ");
    print("VALUE='$theQualsInappropriate' SIZE=5></TD>");
    print("<td>&nbsp;</td>");
    print("<TD><INPUT TYPE=TEXT NAME=theIncorrectSigFile ");
    print("VALUE='$theIncorrectSigFile' SIZE=5></TD>");
    print("</tr>");

    /* Signature */
    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Sig Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Sig Incorrect</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Authorise needed</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Authorising Agent</FONT></TH>\n");
    print("</tr>");

    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=theSig ");
    print("VALUE='$theSig' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theSigIncorrect ");
    print("VALUE='$theSigIncorrect' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=theAuthorise ");
    print("VALUE='$theAuthorise' SIZE=5></TD>");

    print("<TD><FONT COLOR=FFFFFF>");
    print("<SELECT NAME='authoriser'>");
    print("<option value='0'");
    if ($theAuthorisingID == '0') { print(" selected"); };
    print(">");
    $ptable=$db->select("ScripPrescriber");
    foreach ($ptable as $prow) {
        $name=$prow['Name'];
        $prescriberID=$prow['PrescriberID'];
        print("<OPTION VALUE=$prescriberID");
        if ($prescriberID==$theAuthorisingID) { print(" SELECTED"); };
	print("> $name ($prescriberID)");
     };
    print("</select></td>\n");

    print("</tr>");

    print("<TR><TD align=center colspan=12>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR>");

    print("</TABLE>");
}  /* end of function edit single prescriber problem */

function TB_UpdatePrescriberProblem($SID, $thePrescriber, $theName,
        $theNHSNo, $theAddress,
	$theAddressIncomplete, $theAddressNonUK, $theUKAddress1,
        $theUKAddress2, $theUKAddress3, $theUKAddress4,  
        $theQuals,
	$theQualsInappropriate, $theSig, $theSigIncorrect,
        $theAuthorise, $theAuthorisingID, $theIncorrectSigFile) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* Update details of the prescriber problem */  

    $text = array( "PrescriberNameMissing" => $theName,
	"PrescriberID" => $thePrescriber,
	"PrescriberNHSNoMissing" => $theNHSNo,
	"PrescriberAddressMissing" => $theAddress,
	"PrescriberAddressIncomplete" => $theAddressIncomplete,
	"PrescriberAddressNonUK" => $theAddressNonUK,
        "UKAddress1" => $theUKAddress1,
        "UKAddress2" => $theUKAddress2,
        "UKAddress3" => $theUKAddress3,
        "UKAddress4" => $theUKAddress4,
	"PrescriberQualsMissing" => $theQuals,
	"PrescriberQualsInappropriate" => $theQualsInappropriate,
	"PrescriberSigMissing" => $theSig,
	"PrescriberSigIncorrect" => $theSigIncorrect,
	"PrescriberAuthoriseNeeded" => $theAuthorise,
        "AuthorisingID"=>$theAuthorisingID,
        "IncorrectSigFile"=>$theIncorrectSigFile);

    if ($SID!='0') {
        $condition=array('PrescriberProblemID'=>$SID);
        $table=$db->update("ScripPrescriberProblem",$text,$condition);
    } else {
        $table=$db->insert("ScripPrescriberProblem",$text);
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Prescriber Problem updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of update prescriber problem */

function U_EditMiscProblemData() {
    $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 
/* Get details from the MISC problem table and display in a form */  

    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=181>\n");
    print("<TR><TD colspan=8 align=center><FONT COLOR=FFFFFF>");
    print("Select one row to edit</FONT></TD></TR>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Misc Problem</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Date Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Date Future</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Date Late</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Patient Hand Written</FONT></TH>\n");
//    print("<TH><FONT COLOR=FFFFFF>Interaction</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>File Destination</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>File Destination Loss</FONT></TH>\n");
    print("</TR>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked>");
    print("<font color=#ffffff> New</font></td>");
    print("</tr>");

    $table=$db->select("ScripMiscProblem");
    foreach ($table as $row) {
        $MiscProblemID=$row['MiscProblemID'];
	$DateMissing=$row['DateMissing'];
	$DateFuture=$row['DateFuture'];
	$DateLate=$row['DateLate'];
	$PatientHandwritten=$row['PatientHandwritten'];
	$Interaction=$row['Interaction'];
	$filed=$row['FileDestination'];
	$filedestinationloss=$row['FileDestinationLoss'];

 	print("<tr>");
	print("<td><INPUT TYPE=RADIO NAME=ID VALUE=$MiscProblemID>");
	print("<font color=FFFFFF> $MiscProblemID</FONT></TD>\n");
	print("<td><FONT COLOR=FFFFFF>$DateMissing</FONT></TD>\n");
	print("<td><FONT COLOR=FFFFFF>$DateFuture</FONT></TD>\n");
	print("<td><FONT COLOR=FFFFFF>$DateLate</FONT></TD>\n");
	print("<td><FONT COLOR=FFFFFF>$PatientHandwritten</FONT></TD>\n");
//	print("<td><FONT COLOR=FFFFFF>$Interaction</FONT></TD>\n");
	print("<td><FONT COLOR=FFFFFF>$filed</FONT></td>\n");
	print("<td><FONT COLOR=FFFFFF>$filedestinationloss</FONT></td>\n");
 	print("</tr>");
    }
    print("<TR>\n");
    print("<TD colspan=8 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Select></TD></TR>\n");
    print("</FORM></TABLE>\n</TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>\n");
};

function UA_EditSingleMiscProblem($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Get details of the Misc Problem and display in a form
    Z_DrawHeader();
    if ($SID!='0') {
        $condition=array('MiscProblemID'=>$SID);
        $table=$db->select("ScripMiscProblem",$condition); 
        foreach ($table as $row) {
            $MiscProblemID=$row['MiscProblemID'];
            $DateMissing=$row['DateMissing'];
            $DateFuture=$row['DateFuture'];
            $DateLate=$row['DateLate'];
            $thePurposeCategory=$row['PurposeCategory'];
            $thePurpose=$row['Purpose'];
            $thePurposeMarks=$row['PurposeMarks'];
            $PatientHandwritten=$row['PatientHandwritten'];
            $DrugHandwritten=$row['DrugHandwritten'];
            $Interaction=$row['Interaction'];
            $IntAction=$row['IntAction'];
            $IntFB=$row['IntFB'];
            $FileDestination=$row['FileDestination'];
            $filedestinationloss=$row['FileDestinationLoss'];
        };
    } else {
        $MiscProblemID="";
        $DateMissing="";
        $DateFuture="";
        $DateLate="";
        $thePurposeCategory="";
        $thePurpose="";
        $thePurposeMarks="";
        $PatientHandwritten="";
        $DrugHandwritten="";
        $Interaction="";
        $IntAction="";
        $IntFB="";
        $FileDestination="";
        $filedestinationloss="";
    };

    print("<TR>");
    print("<TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=182>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    // ROW 1 (Title) ...
    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Misc Problem</FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=90\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Date Missing</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=91\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Date Future</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=92\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Date Late</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=93\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Patient Hand Written</A></FONT></TH>");
    print("</tr>");

    // ROW 1 (Contents) ...
    print("<TR>");
    print("<TD><FONT COLOR=FFFFFF>$SID</FONT></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=DateMissing ");
    print("VALUE='$DateMissing' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=DateFuture ");
    print("VALUE='$DateFuture' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=DateLate ");
    print("VALUE='$DateLate' SIZE=5></TD>");

    print("<TD><INPUT TYPE=TEXT NAME=PatientHandwritten ");
    print("VALUE='$PatientHandwritten' SIZE=5></TD>");
    print("</TR>");

    // BLANK ROW
    print("<tr>");
    print("<td colspan='5'>");
    print("<font color='#ffffff'>");
    print("For requisitions, you may choose non-FP10CDF or FP10CDF but not both; in either case, though, complete Purpose Marks.<br />");
    print("1. For non-FP10CDFs, complete Purpose in the non-FP10CDF column and make sure Purpose Category in the FP10CDF column is blank.<br />");
    print("2. For FP10CDFs, choose a Purpose Category and add a Purpose if instructed. ");
    print("</font>");
    print("</td>");
    print("</tr>");

    // ROW 2 (Master titles)
    print("<tr>");
    print("<td colspan='2'>");
    print("<font color='#ffffff'><b><center>1. Non-FP10CDF Requisitions</center></b></font>");
    print("</td>");

    print("<td colspan='3'>");
    print("<font color='#ffffff'><b><center>2. FP10CDF Requisitions</center></b></font>");
    print("</td>");
    print("</tr>");

    // ROW 2 - PURPOSE CATEGORY (Title) ...
    print("<tr>");
    print("<th colspan='2'>&nbsp;</th>");
    print("<th colspan='3'><font color='#ffffff'>Purpose Category</font></th>");
    print("</tr>");

    // ROW 2 - PURPOSE CATEGORY (Contents) ...
    print("<tr>");
    print("<td colspan='2'>&nbsp;</td>");
    print("<td colspan='3'>");
    $table=$db->select("ScripPurposeText"); 
    print("<select name='PurposeCategory'>");
    print("<option value='0'");
    if (!$thePurposeCategory) { print(" selected"); };
    print(">");
    foreach ($table as $row){
        $ID = $row['ID'];
        $PurposeText = $row['PurposeText'];
        print("<option value='$ID'");
        if ($thePurposeCategory == $ID) { print(" selected"); };
        print(">$PurposeText");
    };
    print("</select>");
    print("</td>");
    print("</tr>");

    // ROW 3 - PURPOSE - (Titles)
    print("<tr>");
    print("<TH colspan='2'><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
    print('onClick="open(\'scripadmin.php?tab=500&index=93\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Purpose</A></FONT></TH>");
    print("</th>");
    print("<TH colspan='3'><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
    print('onClick="open(\'scripadmin.php?tab=500&index=93\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Purpose</A></FONT></TH>");
    print("</th>");
    print("</tr>");

    // ROW - PURPOSE - (Contents)
    print("<tr>");
    print("<td colspan='2'>");
    print("<input type=text name=thePurposeNonCD size=80");
    if (!$thePurposeCategory) { print(" value='$thePurpose'"); };
    print(">");
    print("</td>");

    print("<td colspan='3'>");
    print("<input type=text name=thePurposeCD size=80");
    if ($thePurposeCategory) { print(" value='$thePurpose'"); };
    print(">");
    print("</td>");
    print("</tr>");

    // ROW 4 - PURPOSE MARKS (Titles)
    print("<tr>");
    print("<th colspan='5'><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
    print('onClick="open(\'scripadmin.php?tab=500&index=93\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Purpose Marks</A></FONT></TH>");
    print("</TR>\n");

    // ROW - PURPOSE MARKS (Contents)
    print("<tr>");
    print("<td colspan='5'><center>");
    print("<input type=text name=PurposeMarks value='$thePurposeMarks'>");
    print("</center></td>");
    print("</tr>");

    // Row - Blank
    print("<tr>");
    print("<td colspan='5'>&nbsp;");
    print("</td>");
    print("</tr>");

    // ROW - FILE DESTINATION (Titles)
    print("<tr>");
    print("<TH COLSPAN=4><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=94\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("File Destination</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=94\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("File Destination Marks</A></FONT></TH>\n");
    print("</tr>");

    // ROW - FILE DESTINATION (Contents)
    print("<tr>");
    $newtable=$db->select("ScripFileDestinations"); 
    print("<td colspan=4>");
    print("<select name=FileDestination>");
    if ($SID=='0') { print("<option value='0' selected>"); };
    foreach ($newtable as $row) {
        $FileDestinationID=$row['FileDestinationID'];
        $FileText=$row['Text'];
        print("<OPTION VALUE=$FileDestinationID ");
        if ($FileDestinationID==$FileDestination) {
	    print("SELECTED ");
        };
	print(">$FileText");
    };
    print(" </select></td>\n");

    print("<td>");
    print("<input type=text name=filedestinationloss "); 
    print("value=$filedestinationloss>");
    print("</td>");
    print("</tr>");

/*
    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=95\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Interaction</A></FONT></TH>\n");

    print("<TH COLSPAN=2><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=96\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Interaction ACTION</A>");
    print("</FONT></TH></tr>");

    print("<TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=Interaction ");
    print("VALUE='$Interaction' SIZE=5></TD>");

    print("<TD COLSPAN=2><INPUT TYPE=TEXT NAME=Intaction ");
    print("VALUE='$IntAction' SIZE=40></td></tr>");

    print("<TR>");
    print("<TH COLSPAN=5><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=97\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Feedback for interaction</A></FONT></TH>");
    print("</TR>\n");

    print("<TR>");
    print("<TD COLSPAN=5><INPUT TYPE=TEXT NAME=IntFB ");
    print("VALUE='$IntFB' SIZE=100></TD><TR>");
    print("</TR>");
*/

    print("<TR><TD align=center colspan=5>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");

    print("</form></table></td></tr><tr><td>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</td></tr></table>");
};

function UB_UpdateMiscProblem($SID, $DateMissing, $DateFuture, $DateLate,
        $thePurposeCategory, $thePurpose, $thePurposeMarks, 
        $PatientHandwritten, $Interaction, 
        $FileDestination, $filedestinationloss, $Intaction, $IntFB) {

    // Update details of the miscellaneous problem
    global $case;
    $text = array(
        "DateMissing" => $DateMissing,
        "DateFuture" => $DateFuture,
        "DateLate" => $DateLate,
        "PatientHandwritten" => $PatientHandwritten,
        "IntFB" => $IntFB, 
        "IntAction" => $Intaction,
        "PurposeCategory" => $thePurposeCategory, 
        "Purpose" => $thePurpose, 
        "PurposeMarks" => $thePurposeMarks, 
        "FileDestination" => $FileDestination,
        "FileDestinationLoss" => $filedestinationloss);

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    if ($SID!='0') {
        $condition=array('MiscProblemID'=>$SID);
        $db->update("ScripMiscProblem",$text,$condition);
    } else {
        $db->insert("ScripMiscProblem",$text);
    };

    Z_DrawHeader();
    print("<TR><TD colspan=8><FONT COLOR=FFFFFF>Misc Problem updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function V_AddDoseCode()
{   Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=371>");
    print("<TR><TH><FONT COLOR=FFFFFF>Code</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=Code ");
    print(" SIZE=15></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=Text ");
    print(" SIZE=100></TD>/<TR>\n");
    print("<TD colspan=8 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>\n");
    print("</FORM><TR><TD colspan=2 ALIGN=CENTER>\n");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>\n");
} /* end of add dose code */

function VA_InsertDoseCode($code, $text)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* insert details of the dose code */  
    $text= array( "Code" => $code,
	"Text" => $text,);
    $table=$db->insert("ScripDoseCodes",$text);
    Z_DrawHeader();
    print("<TR><TD colspan=8><FONT COLOR=FFFFFF>Dose Code added");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of insert dose code */

function V_EditDoseCodeTable() {
    Z_DrawHeader();
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=191>\n");
    print("<TR><TD ALIGN=CENTER><TABLE BORDER=1>");
    print("<TH colspan=3><FONT COLOR=FFFFFF><H2>");
    print("Select one row to edit</H2></FONT></TH></TR>");
    print("<TR><TH><FONT COLOR=FFFFFF>Dose Code</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Code</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH></TR>\n");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td colspan='2'><font color=ffffff>New Code</font></td>");
    print("</tr>");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripDoseCodes");
    foreach ($table as $row) {
        $DoseCodeID=$row['DoseCodeID'];
	$Code=$row['Code'];
	$Text=$row['Text'];
 	print("<TR><TD>");
	print("<INPUT TYPE=RADIO NAME=ID VALUE='$DoseCodeID'>");
	print("</TD>\n<TD><FONT COLOR=#FFFFFF>$Code</FONT>");
	print("</TD>\n<TD><FONT COLOR=#FFFFFF>$Text</FONT>");
	print("</TD>\n</TR>");
    }
    print("<TR><TD colspan=3 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>\n");
    print("</FORM><TR><TD colspan=3 ALIGN=CENTER>\n");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>\n");
} /* end of edit dose code table */

function VA_EditSingleDoseCode($SID){
    // 

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripDoseCodes",array('DoseCodeID'=>$SID)); 
    Z_DrawHeader();

    if ($SID != '0') {
        foreach ($table as $row) {
            $Code=$row['Code']; 
            $Text=$row['Text'];
        };
    } else {
        $Code="";
	$Text="";
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=192>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<TR><TH><FONT COLOR=FFFFFF>Code</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH></tr>\n");

    print("<TR><TD><FONT COLOR=FFFFFF>$SID</FONT></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=code ");
    print("VALUE='$Code' SIZE=15></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=text ");
    print("VALUE='$Text' SIZE=100></TD></tr>");

    print("<TR><TD align=center colspan=8>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function edit single dose code problem */

function VB_UpdateDoseCodeTable($SID, $code, $text) {
    Z_DrawHeader();
    $text= array( "Code" => $code,
	"Text" => $text,);
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    if ($SID!='0') {
        $table=$db->update("ScripDoseCodes",$text, array("DoseCodeID"=>$SID));
    } else {
        $table=$db->insert("ScripDoseCodes",$text);
    };

    Z_DrawHeader();
    print("<TR><TD><FONT COLOR=FFFFFF>Dose Code updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function V_CreateShortTermPassword(){
    // Create a short-term password:

    Z_DrawHeader();

    $db=new dbabstraction(); $db->connect() or die ($db->getError());   

    // The password ...
    $shortTermPassword = ZZZ_GenerateRandomPassword();

    // The time ...
    $now = time();

    $theDate = getdate($now);

    // Write to table in database ...
    $text = array("Password"=>$shortTermPassword, "Created"=>$now);
    $table=$db->insert("ScripShortTermPasswords", $text);

    print("<tr><td><font color='#ffffff'>Password <b>$shortTermPassword</b> was created at " . $theDate['hours'] . "." . $theDate['minutes'] . ":" . $theDate['seconds'] . ".</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");

    // Also - delete any codes that are older than 24 hours ...
    $tooOld = $now - 24 * 60 * 60; // ... 24 hours ago
    $table=$db->select("ScripShortTermPasswords");
    foreach ($table as $row){
        if ($row['Created'] < $tooOld){
            // Delete it: identify it uniquely by its primary key ...
            $condition = array("PK"=>$row['PK']);
            $db->delete("ScripShortTermPasswords", $condition);
        };
    };
};



function ZV_ScripsscripsCreateNewName($theLetter, $newLetter){
    // Write the copy ...

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    print("<b>To Copy $theLetter as $newLetter</b><br>");

    // Read the row for $theLetter from ScripsScrips and insert the details into ScripsScrips as a new row but with letter $newLetter ...

    // Checks will be made as the information is collected; only if all are passed will new information be written to the various tables ...

    $condition = array('Letter'=>$theLetter);
    $table = $db->select("ScripsScrips", $condition);
    $nRows = count($table);
    if ($nRows == 1){ // ... correct - there should indeed be one row only
        $correctScripsScrips = true;
        $newScripsScripsEntry = $table[0]; // ... new line
        $newScripsScripsEntry['Letter'] = $newLetter; // ... new line amended for change of letter
    } else { // ... something wrong ...
        $correctScripsScrips = false;
        print("Error while trying to read entry from ScripsScrips: Number of rows in this table with letter $theLetter is $nRows. (There should be one only.)<br><br>");
    };

    // Copy the model answers for these: (1) CLASSIFY - ScripClassifyMA: LetterCode [PK] ...
    print("<br>Copying from ScripClassifyMA - ");
    $condition = array('LetterCode'=>$theLetter);
    $table = $db->select("ScripClassifyMA", $condition);
    $nRows = count($table);
    if ($nRows < 2){ // ... correct
        $correctScripClassifyMA = true;
        print("Allowed: ");
        if ($nRows == 1) { // ... if 1, copy it; if zero, do nothing
            print("One row found to copy");
            $newScripClassifyMAEntry = $table[0]; // ... new line
            $newScripClassifyMAEntry['LetterCode'] = $newLetter; // ... new line amended for change of letter
        } else {
            print("No rows found to copy");
        };
    } else {
        // Something wrong ...
        $correctScripClassifyMA = false;
        print("Error: Number of rows in table with letter $theLetter is $nRows. (There should be no more than one.)<br><br>");
    };

    // Copy the model answers for these: (2) INTERACTION - ScripInteractionMA: CaseID [not PK] ...
    // [At present there appears to be one interation PER SCRIPT only. A mechanism allowing for more than one with the prsaent code would be dififcult to implement.
    // This code assumes there only interation and that it can be located with InteractionMAID from ScripsScrips.]

    // Find the Interaction model answer ID (it's in $newScripsScripsEntry) and then go to the ScripInteractionMA table ...
    print("<br>Copying from ScripInteractionMA - ");
    $InteractionMAID = $newScripsScripsEntry['InteractionMAID'];
    $correctScripInteractionMA = true;
    if ($InteractionMAID){ // ... an interaction exists
        $condition = array('ID'=>$InteractionMAID);
        $table = $db->select("ScripInteractionMA", $condition);
        $nRows = count($table);
        if ($nRows == 1){ // ... should be one only
            print("One row found to copy");
            $newScripInteractionMAEntry = $table[0]; // ... new line
            unset($newScripInteractionMAEntry['CaseID']); // ... strip out the ID - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)
            $newScripInteractionMAEntry['CaseID'] = $newLetter; // ... amend letter
        } else {
            // Something wrong ...
            $correctScripInteractionMA = false;
            print("Error: Number of rows in table with ID $InteractionMAID is $nRows. (There should not be more than one.)<br><br>");
        };
    } else {
        print("No Interaction for this script");
    };

    // Copy the model answers for these: (3) LABELS - ScripDoseCodesMA: runs from DrugProblemID [not PK] - may be TWO of them - see ScripsScrips ...
    print("<br>Copying from ScripDrugProblem and ScripDoseCodesMA - ");
    if ($correctScripsScrips){ // ... can't even attempt this is ScripsScips is wrong
        $drugProblemID = array();
        if ($newScripsScripsEntry['DrugProblemID'] > 0) {
            $drugProblemID[] = $newScripsScripsEntry['DrugProblemID'];
            if ($newScripsScripsEntry['DrugProblemID2'] > 0) {
                $drugProblemID[] = $newScripsScripsEntry['DrugProblemID2'];
            };
        };
        // ... $drugProblemID (which will have either one or two elements only) contains the ID numbers of the drug problem in ScripDrugProblem

        // Loop over $drugProblemID ... 
        for ($i = 0; $i < count($drugProblemID); $i++){
            $ip1 = $i + 1; // ... for convenience
            $dP = $drugProblemID[$i]; // ... for convenience
            $condition = array('DrugProblemID'=>$dP);
            $table = $db->select("ScripDrugProblem", $condition);
            $nRows = count($table);

            if ($nRows == 1){ // ... correct - there should indeed be one row only
                print("Drug problem $ip1 ($drugProblemID[$i]) found to copy; ");
                $correctScripDrugProblem[$i] = true;
                $newScripDrugProblemEntry[$i] = $table[0]; // ... new line
                unset($newScripDrugProblemEntry[$i]['DrugProblemID']); // ... strip out the primary key - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)

                // Now, go on the labels: each entry (i.e. row) in DrugProblemID has a label associated with it: find that label ...
                $condition = array('DrugProblemID'=>$dP);
                $tableLabel = $db->select("ScripDoseCodesMA", $condition);
                $nRows = count($tableLabel);

                if ($nRows){ // ... is there a label?
                    if ($nRows == 1){ // ... correct - there should indeed be one row only
                        print("Label $ip1 found to copy; ");
                        $correctScripDoseCodesMA[$i] = true;
                        $newScripDoseCodesMAEntry[$i] = $tableLabel[0]; // ... new line
                        unset($newScripDoseCodesMAEntry[$i]['MAID']); // ... strip out the primary key - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)
                    } else { // ... something wrong ...
                        $correctScripDoseCodesMA[$i] = false;
                        print("Error: Number of rows in table with DrugProblemID $dP is $nRows. (There should be no more than one label per item.)<br><br>");
                    };
                } else {
                    print("No label found for this item; ");
                };

            } else { // ... something wrong ...
                $correctScripDrugProblem[$i] = false;
                print("Error: Number of rows in table with DrugProblemID $dP is $nRows. (There should be one only.)<br><br>");
            };
        };
    };

    // Copy the model answers for these: (4) POR - ScripPORMA - CaseID [not PK] ...
    print("<br>Copying from ScripPORMA - ");
    $condition = array('CaseID'=>$theLetter);
    $table = $db->select("ScripPORMA", $condition);
    $nRows = count($table);
    if ($nRows){ // ... POR entry exists
        print("One row found to copy");
        $newScripPORMAEntry = $table[0]; // ... new line
        $newScripPORMAEntry['LetterCode'] = $newLetter; // ... new line amended for change of letter
        unset($newScripPORMAEntry[$i]['PORMAID']); // ... strip out the primary key - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)
    } else {
        print("Nothing for this script");
    };

    // Copy the model answers for these: (5) CD Register - ScripCDRegisterMA - CaseID [not PK] ...
    print("<br>Copying from ScripCDRegisterMA - ");
    $condition = array('CaseID'=>$theLetter);
    $table = $db->select("ScripCDRegisterMA", $condition);
    $nRows = count($table);
    if ($nRows){ // ... correct - there should indeed be one row only
        print("One row found to copy");
        $newScripCDRegisterMAEntry = $table[0]; // ... new line
        $newScripCDRegisterMAEntry['LetterCode'] = $newLetter; // ... new line amended for change of letter
        unset($newScripCDRegisterMAEntry[$i]['ID']); // ... strip out the primary key - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)
    } else {
        print("Nothing for this script");
    };

    // Copy the problem settings for these: (1) ScripPatientProblem
    // Find the PatientProblemID (it's in $newScripsScripsEntry) and then go to the ScripPatientProblem table ...
    print("<br>Copying from ScripPatientProblem - ");
    $PatientProblemID = $newScripsScripsEntry['PatientProblemID'];
    $correctScripPatientProblem = true;
    if ($PatientProblemID){ // ... a patient problem exists; if zero then value of zero is kept in $newScripsScripsEntry and nothing further need be done
        $condition = array('PatientProblemID'=>$PatientProblemID);
        $table = $db->select("ScripPatientProblem", $condition);
        $nRows = count($table);
        if ($nRows == 1){
            print("One row found to copy");
            $newScripPatientProblemEntry = $table[0]; // ... new line
            unset($newPatientProblemEntry['PatientProblemID']); // ... strip out the PatientProblemID - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)
        } else {
            // Something wrong ...
            $correctScripPatientProblem = false;
            print("Error: Number of rows in table with PatientProblemID $PatientProblemID is $nRows. (There should not be more than one.)<br><br>");
        };
    } else {
        print("Nothing for this script");
    };

    // Copy the problem settings for these: (2) ScripPrescriberProblem
    // Find the PrescriberProblemID (it's in $newScripsScripsEntry) and then go to the ScripPrescriberProblem table ...
    print("<br>Copying from ScripPrescriberProblem - ");
    $PrescriberProblemID = $newScripsScripsEntry['PrescriberProblemID'];
    $correctScripPrescriberProblem = true;
    if ($PrescriberProblemID){ // ... a prescriber problem exists; if zero then value of zero is kept in $newScripsScripsEntry and nothing further need be done
        $condition = array('PrescriberProblemID'=>$PrescriberProblemID);
        $table = $db->select("ScripPrescriberProblem", $condition);
        $nRows = count($table);
        if ($nRows == 1){
            print("One row found to copy");
            $newTest = $table[0]; // ... new line
            $newScripPrescriberProblemEntry = $table[0]; // ... new line
            unset($newPrescriberProblemEntry['PrescriberProblemID']); // ... strip out the PrescriberProblemID - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)
        } else {
            // Something wrong ...
            $correctScripPrescriberProblem = false;
            print("Error: Number of rows in table with PrescriberProblemID $PrescriberProblemID is $nRows. (There should not be more than one.)<br><br>");
        };
    } else {
        print("Nothing for this script");
    };


    // Copy the problem settings for these: (3) ScripDrugProblem - see Labels

    // Copy the problem settings for these: (4) ScripMiscProblem
    print("<br>Copying from MiscPrescriberProblem - ");
    $MiscProblemID = $newScripsScripsEntry['PatientMiscID'];
    $correctScripMiscProblem = true; // true by default unless wrong
    if ($MiscProblemID){ // ... a misc problem exists; if zero then value of zero is kept in $newScripsScripsEntry and nothing further need be done
        $condition = array('MiscProblemID'=>$MiscProblemID);
        $table = $db->select("ScripMiscProblem", $condition);
        $nRows = count($table);
        if ($nRows == 1){
            print("One row found to copy");
            $newScripMiscProblemEntry = $table[0]; // ... new line
            unset($newMiscProblem['MiscProblemID']); // ... strip out the PrescriberProblemID - this will be set automatically by the insert function (since ID is an auto-incrementing primary key)
        } else {
            // Something wrong ...
            $correctScripMiscProblem = false;
            print("Error: Number of rows in table with MiscProblemID $MiscProblemID is $nRows. (There should not be more than one.)<br><br>");
        };
    } else {
        print("Nothing for this script");
    };

    // Concatenate the two logicals that are array
    $correctSDP = true;
    for ($j = 0; $j < count($correctScripDrugProblem); $j++){
        $correctSDP &= $correctScripDrugProblem[$j];
    };

    $correctSDCMA = true;
    for ($j = 0; $j < count($correctScripDoseCodesMA); $j++){
        $correctSDCMA &= $correctScripDoseCodesMA[$j];
    };


//   NOTE NEED TO AND THE LABEL AND DRUG PROBLEM CONDITIONS 
    $correctAll = $correctScripsScrips && $correctScripClassifyMA && $correctScripInteractionMA && $correctScripPatientProblem && $correctScripPrescriberProblem && $correctScripMiscProblem && $correctSDP && $correctSDCMA;

    print("<br>");

    print("<br>All the following must be 1 for the program to proceed - REPORT ANY ZEROES OR BLANKS TO JCH");
    print("<br>correctScripsScrips = $correctScripsScrips");
    print("<br>correctScripClassifyMA = $correctScripClassifyMA");
    print("<br>correctScripInteractionMA = $correctScripInteractionMA");
    print("<br>correctScripPatientProblem = $correctScripPatientProblem");
    print("<br>correctScripPrescriberProblem = $correctScripPrescriberProblem");
    print("<br>correctScripMiscProblem = $correctScripMiscProblem");
    print("<br>correctSDP = $correctSDP");
    print("<br>correctSDCMA = $correctSDCMA");

    print("<br><br>");

    // THIS VARIABLE ($insert) CONTROLS WHETHER THE FUNCTION SENDS OUTPUT TO THE SCREEN (false) OR TO THE RESPECTIVE TABLES (true) ... 
    $insert = false;

    if ($correctAll){

        if ($insert){
            // Insert the new value into the appropriate table (provided the new value exists) ...
            if (isset($newScripClassifyMAEntry)){
                $table=$db->insert("ScripClassifyMA", $newScripClassifyMAEntry);
            };
        } else {
            print("To ScripClassifyMA, the code would add:");
            print("<pre>"); print_r($newScripClassifyMAEntry); print("</pre>"); 
        };

        if ($insert){
            if (isset($newScripInteractionMAEntry)){
                $db->insert("ScripInteractionMA", $newScripInteractionMAEntry);
                // ... find the primary key of the new line and put it into the new line for ScripsScrips ...
                $newID=$db->getID();
                $newScripsScripsEntry['InteractionMAID'] = $newID;
            };
        } else {
            print("To ScripInteractionMA, the code would add:");
            print("<pre>"); print_r($newScripInteractionMAEntry); print("</pre>"); 
        };

        if ($insert){
            if (isset($newScripPORMAEntry)){
                $db->insert("ScripPORMA", $newScripPORMAEntry);
            };
        } else {
            print("To ScripPORMA, the code would add:");
            print("<pre>"); print_r($newScripPORMAEntry); print("</pre>"); 
        };

        if ($insert){
            if (isset($newScripCDRegisterMAEntry)){
                $db->insert("ScripCDRegisterMA", $newScripCDRegisterMAEntry);
            };
        } else {
            print("To ScripCDRegisterMA, the code would add:");
            print("<pre>"); print_r($newScripCDRegisterMAEntry); print("</pre>"); 
        };

        if ($insert){
            if (isset($newScripPatientProblemEntry)){
                $db->insert("ScripPatientProblem", $newScripPatientProblemEntry);
                // ... find the primary key of the new line and put it into the new line for ScripsScrips ...
                $newID=$db->getID();
                $newScripsScripsEntry['PatientProblemID'] = $newID;
            };
        } else {
            print("To ScripPatientProblem, the code would add:");
            print("<pre>"); print_r($newScripPatientProblemEntry); print("</pre>"); 
        };

        if ($insert){
            if (isset($newScripPrescriberProblemEntry)){
                $db->insert("ScripPrescriberProblem", $newScripPrescriberProblemEntry);
                // ... find the primary key of the new line and put it into the new line for ScripsScrips ...
                $newID=$db->getID();
                $newScripsScripsEntry['PrescriberProblemID'] = $newID;
            };
        } else {
            print("To ScripPrescriberProblem, the code would add:");
            print("<pre>"); print_r($newScripPrescriberProblemEntry); print("</pre>"); 
        };

        if ($insert){
            if (isset($newScripMiscProblemEntry)){ // ... not necessary to have a 
                $db->insert("ScripMiscProblem", $newScripMiscProblemEntry);
                // ... find the primary key of the new line and put it into the new line for ScripsScrips ...
                $newID=$db->getID();
                $newScripsScripsEntry['MiscProblemID'] = $newID;
            };
        } else {
            print("To ScripMiscProblem, the code would add:");
            print("<pre>"); print_r($newScripMiscProblemEntry); print("</pre>"); 
        };

        // Loop over the number of the items on the script (1 or 2) ...
        for ($i = 0; $i < count($drugProblemID); $i++){
            if ($insert){
                if (isset($newScripDrugProblemEntry[$i])){
                    $db->insert("ScripDrugProblem", $newScripDrugProblemEntry[$i]);
                    // ... find the primary key of the new line and put it in TWO places ...
                    $newID=$db->getID();
                    // (1) Put new ID into the line that will go into ScripDoseCodesMA ...
                    $newScripDoseCodesMAEntry[$i]['DrugProblemID'] = $newID;

                    // ... and (2) put new ID into the line that will go into ScripsScrips ...
                    // [This has to be done manually - there are fields DrugProblemID and DrugProblemID2 in ScripsScrips.]
                    if ($i == 0){
                        $newScripsScripsEntry['DrugProblemID'] = $newID;
                    } else {
                        $newScripsScripsEntry['DrugProblemID2'] = $newID;
                    };
                };

                if(isset($newScripDoseCodesMAEntry[$i])){
                    $db->insert("ScripDoseCodesMA", $newScripDoseCodesMAEntry[$i]);
                };
            } else {
                print("To ScripDrugProblem, the code would add:");
                print("<pre>"); print_r($newScripDrugProblemEntry[$i]); print("</pre>"); 
                print("To ScripDoseCodesMA, the code would add:");
                print("<pre>"); print_r($newScripDoseCodesMAEntry[$i]); print("</pre>"); 
            };
        };

        // Finally, now that $newScripsScripsEntry has been amended, insert it into ScripsScrips ...
        if ($insert){
            if(isset($newScripsScripsEntry)){
                $db->insert("ScripsScrips", $newScripsScripsEntry);
            };
        } else {
            print("To ScripsScrips, the code would add:");
            print("<pre>"); print_r($newScripsScripsEntry); print("</pre>"); 
        };
    };

    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr>");

}; 


function ZV_ScripsscripsAskNewName($theLetter, $newLetter){

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    print("<form action=scripadmin.php method='post'>");

    print("<input type='hidden' name='tab' value='117'>");
    print("<input type='hidden' name='ID' value='$theLetter'>");

    if ($newLetter != ""){
        // A letter will only have been passed to this function if it is one that is already in use in ScripsScrips ...
        print("<tr><td><font color='#ffffff'>The code $newLetter is already taken.</font></td></tr>");
    };
    print("<tr><td><font color='#ffffff'>What do you want to call the new version of $theLetter? Two letters and one digit.</font></td></tr>");

    print("<tr><td><input type='text' name='newletter'></td></tr>");

    print("<tr><td><input type='submit' value='Confirm'></td></tr>");

    print("</form>");
};


function ZV_ScripsscripsListAsk(){
    // List all the letters and ask what is required (include All as an option) ...

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // A lookup table of form types ...
    $table=$db->select("ScripForms");
    foreach ($table as $row){
        $formID=$row['FormID'];
        $formname2[$formID]=$row['FormName2'];
        $formname1[$formID]=$row['FormName1'];
    };

    // Get details of all the scrips and display as a table ...
    print("<tr><td colspan='2'><font color='#ffffff'><h2>Select one scrip from the list or ALL and click the submit button to edit the details</h2></font></td></tr>");
    print("<tr><td colspan='2' align='center'><table border='1'>");

    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name=tab value='116'>");

    print("<tr>");
    print("<th>&nbsp;</th>");
    print("<th><font color='#ffffff'>Letter</font></th>");
    print("<th><font color='#ffffff'>Form</font></th>");
    print("<th><font color='#ffffff'>Form</font></th>");
    print("</tr>");

    $table=$db->select("ScripsScrips");
    foreach ($table as $row) {
        print("<tr>");

        // N.B. The primary key in ScripsScrips is the letter 

        // Read the letter and form but convert it to a name and display that ...
        $theLetter=$row['Letter'];
        $theFormID=$row['FormID'];

        print("<td><input type=radio name=ID value=$theLetter></td>");
        print("<td><FONT COLOR='#FFFFFF'>$theLetter</FONT></TD>");
        print("<td><FONT COLOR='#FFFFFF'>$formname2[$theFormID]</FONT></TD>");
        print("<td><FONT COLOR='#FFFFFF'>$formname1[$theFormID]</FONT></TD>");

        print("</tr>");
    };

    print("<tr>\n");
    print("<td colspan='8' align='center'><input type=submit value='Select'></td>");
    print("</tr>\n");

}; 


function ZW_ScripsscripsListAsk(){
    // List all the letters and ask what is required (include All as an option) ...

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // A lookup table of form types ...
    $table=$db->select("ScripForms");
    foreach ($table as $row){
        $formID=$row['FormID'];
        $formname2[$formID]=$row['FormName2'];
        $formname1[$formID]=$row['FormName1'];
    };

    // Get details of all the scrips and display as a table ...
    print("<tr><td colspan='2'><font color='#ffffff'><h2>Select one scrip from the list or ALL and click the submit button to edit the details</h2></font></td></tr>");
    print("<tr><td colspan='2' align='center'><table border='1'>");

    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name=tab value='98'>");

    print("<tr>");
    print("<th>&nbsp;</th>");
    print("<th><font color='#ffffff'>Letter</font></th>");
    print("<th><font color='#ffffff'>Form</font></th>");
    print("<th><font color='#ffffff'>Form</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>ALL</td>");
    print("</tr>");

    $table=$db->select("ScripsScrips");
    foreach ($table as $row) {
        print("<tr>");

        // N.B. The primary key in ScripsScrips is the letter 

        // Read the letter and form but convert it to a name and display that ...
        $theLetter=$row['Letter'];
        $theFormID=$row['FormID'];

        print("<td><input type=radio name=ID value=$theLetter></td>");
        print("<td><FONT COLOR='#FFFFFF'>$theLetter</FONT></TD>");
        print("<td><FONT COLOR='#FFFFFF'>$formname2[$theFormID]</FONT></TD>");
        print("<td><FONT COLOR='#FFFFFF'>$formname1[$theFormID]</FONT></TD>");

        print("</tr>");
    };

    print("<tr>\n");
    print("<td colspan='8' align='center'><input type=submit value='Select'></td>");
    print("</tr>\n");

}; 


function ZW_ScripsscripsListProduce($theLetter){
    // Produce a spreadsheet of required letter(s) ...

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Haedings ...
    $implode = array ("Letter", "FormID", "PatientProblemID", "PrescriberProblemID", "DrugProblemID", "DrugProblemID2", "MiscProblemID", "Counsel", "EntirelyHandwritten", "InteractionMAID", "HospitalTTODetails");
    $line = implode(",", $implode)."\n"; // ... add more text

    // Read the table ...
    if ($theLetter == ""){
        // Entire file ...
        $table=$db->select("ScripsScrips");
    } else {
        $condition = array("Letter"=>$theLetter);
        $table=$db->select("ScripsScrips", $condition);
    };

    foreach ($table as $row){
        $implode = array($row['Letter'], $row['FormID'], $row['PatientProblemID'], $row['PrescriberProblemID'], $row['DrugProblemID'], $row['DrugProblemID2'], $row['MiscProblemID'], $row['Counsel'], $row['EntirelyHandwritten'], $row['InteractionMAID'], $row['HospitalTTODetails']);
        $line .= implode(",", $implode)."\n"; // ... add more text
    };

    // Construct the file name ...
    $thefile="users/practice1/ScripsScrips1.csv"; 

    // If file already exists, delete it ...
    if (is_file($thefile)) {
        unlink($thefile);
    };

    $fpout=fopen($thefile, "w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    print("<tr><td bgcolor='#ffffff'>");
    print("<p><a href='$thefile'>Right click for details from ScripsScrips</a></p>");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");



/*
        print("<tr><td>");
        print("<p><font color=#ffffff>This may take a moment or two ... watch the cursor.</font></p>");
        print("<p><font color=#ffffff>Right click the links below to download the named files.");
        print(" From the context menus choose Save Target As ...</font></p>");

        print("<p><font color=#ffffff><ul>");
        print("<li><a href='$thefile'>Prescription marks</a></li>");
        print("<li><a href='$thefilec'>Communication marks</a></li>");
        print("</ul></font></p>");

        print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
        print("</td></tr>");
*/



};





function ZZZ_GenerateRandomPassword(){
    // Generate a new password:

    // Parameters for password: size ...
    $minChar = 7;
    $maxChar = 9;
    // ... at least one each of these ...
    $punctuation = ".,:;!?+-*|<>()[]{}~#@";
    $digit = "0123456789";
    // ... and here are the letters ...
//    $letter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMENOPQRSTUVWXYZ";
    $letter = "abcdefghijklmnopqrstuvwxyz";

    // Generate a random size:
    $nChar = rand($minChar, $maxChar);

    // Put the integers 0, 1, 2, ... $nChar into an array called $randomIntegers ...
    for ($i = 0; $i < $nChar; $i++){ $randomInteger[] = $i; };

    // Choose one random letter, six random digits and one random letter ...
    for ($i = 0; $i < 8; $i++){
        if ($i == 0 || $i == 7){
            $randomCharacter[$i] = $letter[rand(0, strlen($letter)-1)];
        } else {
            $randomCharacter[$i] = $digit[rand(0, strlen($digit)-1)];
        };
    };

/*
    // Generate a random size:
    $nChar = rand($minChar, $maxChar);

    // Put the integers 0, 1, 2, ... $nChar into an array called $randomIntegers ...
    for ($i = 0; $i < $nChar; $i++){
        $randomInteger[] = $i;
    };

    // Shuffle it ...
    for ($i = 0; $i < $nChar - 1; $i++){
        // Choose a random element beyond the current element ...
        $randomElement = rand($i + 1, $nChar - 1);
        // Swap ...
        $temp = $randomInteger[$i]; $randomInteger[$i] = $randomInteger[$randomElement]; $randomInteger[$randomElement] = $temp; 
    };

    // How many itemns of punctuation and digits? 
    $nPunctuation = rand(1, 3);
    $nDigit = rand(1, 3);

    // Loop over $randomInteger - successive elements consititute random positions - assign a character to these ...
    for ($i = 0; $i < $nChar; $i++){
        if ($i < $nPunctuation){
            $randomCharacter[$randomInteger[$i]] .= $punctuation[rand(0, strlen($punctuation)-1)];
        } else if ($i < $nPunctuation + $nDigit){
            $randomCharacter[$randomInteger[$i]] .= $digit[rand(0, strlen($digit)-1)];
        } else {
            $randomCharacter[$randomInteger[$i]] .= $letter[rand(0, strlen($letter)-1)];
        };
    };
*/


    // ... now put them into a string ...
    $randomPassword = "";
    for ($i = 0; $i < count($randomCharacter); $i++){
        $randomPassword .= $randomCharacter[$i];
    };

    return $randomPassword;
};



function V_ViewStudentPassword(){
    // 

    Z_DrawHeader();
    print("<TD colspan=2 ><FONT COLOR=#FFFFFF SIZE=4>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=201>");
    print("Please Enter Student username:&nbsp;&nbsp;&nbsp;"); 
    print("<INPUT TYPE=PASSWORD SIZE=10 MAXCHAR=10 NAME=susername>");
    print("&nbsp;&nbsp;&nbsp;");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD>");
    print("</TR>");
    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr>");
    print("</TABLE>");
};

function VA_ViewPassword($susername)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError());   
    $table=$db->select("ScripStudent",array('username'=>$susername));
    foreach ($table as $row)
	$thepassword=$row['Password'];
    Z_DrawHeader();
    print("<TR><TD>");
    print("<FONT COLOR=#FFFFFF>");
    if ($thepassword)
    {   print("<H2>Password for $susername</H2>");
	print("<P>$thepassword</P>");
    }
    else 
    {   print("<H2>Incorrect username $susername</H2>");
   	print("<P><A HREF=scripadmin.php?tab=0>Try Again</P>");
    }
    print("</TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of funtion view student password */

function W_RemoveStudents()
{   Z_DrawHeader();
    print("<TD colspan=2 ><FONT COLOR=#FFFFFF SIZE=4>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=211>");
    print("<P>This option removes all student data including ");
    print("all records of their work.</P>");
    print("Are you sure you want to remove all students:&nbsp;&nbsp;&nbsp;"); 
    print("Yes <INPUT TYPE=RADIO NAME=confirm VALUE='Y'>");
    print("No <INPUT TYPE=RADIO NAME=confirm  VALUE='N' CHECKED>");
    print("&nbsp;&nbsp;&nbsp;");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function remove students */
 
function WA_ActuallyRemoveStudents($confirm)
{   Z_DrawHeader();
    print("<TD colspan=2 >");
    if ($confirm=='Y')
    {   $db=new dbabstraction();
	$db->connect()  or die ($db->getError()); 
/* remove all students */  
	$db->delete("ScripStudent");
	$db->delete("ScripCDRegister");
	$db->delete("ScripClassify");
	$db->delete("ScripEndorse");
	$db->delete("ScripFile");
	$db->delete("ScripPOR");
	$db->delete("ScripProbes");
	$db->delete("ScripRegister");
	$db->delete("ScripScore");
	$db->delete("ScripStudentRecord");
	$db->delete("ScripVerify");
	$db->delete("ScripVerifyDrug");
	$db->delete("ScripLabel");
	print("<TR><TD><FONT COLOR=#FFFFFF><H2>All students deleted");
	print("</H2></FONT></TD></TR>");
    }
    else
    {   print("<TR><TD><FONT COLOR=#FFFFFF><H2>student deletion cancelled");
	print("</H2></FONT></TD></TR>");
    }
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function remove studnts */

function X_AddStudent()
{   Z_DrawHeader();
    print("<TD colspan=2 >");
    print("<FONT COLOR=#FFFFFF>");
    print("<H2>Entering new student data</H2>");
    print("<FONT><FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=221>");
    print("<TABLE BORDER=1><TR>");
    print("<TD><FONT COLOR=#FFFFFF>First Name"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Last Name"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Username"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Password"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Practice Code"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Exam Code"); 
    print("</FONT></TD></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=sfname></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=slname></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=suname></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=spassword></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 MAXCHAR=3 NAME=spractice></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 MAXCHAR=3 NAME=sexam></TD>");
    print("</TR><TR><TD colspan=6 align=center>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD colspan=6>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add student */

function XA_InsertStudent($thesfname, $theslname, $thesuname,
	$thespassword, $thespractice, $thesexam)
{   Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
   $studentdata= array( "FirstName" => $thesfname,
	"LastName" => $theslname, "Username" => $thesuname,
	"Password" => $thespassword, "PracticeCode" => $thespractice,
	"ExamCode" => $thesexam );
/* add the students */  
    $db->insert("ScripStudent", $studentdata);
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Students added</TD></TR>");
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert student */




function XA_DeleteRealStudents(){
    // Delete all students marked as real (i.e. 1 in field RealStudent) in ScripStudent:

    // Still set to select and not delete!!

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $condition = array('RealStudent'=>1);
    $table = $db->select("ScripStudent", $condition);

    foreach ($table as $row){
        print ($row['FirstName'] . " " . $row['LastName'] . "<br>");
    };
};



function Y_AddPatient()
{   Z_DrawHeader();
    print("<TD colspan=2 >");
    print("<FONT COLOR=#FFFFFF>");
    print("<H2>Entering new patient data</H2>");
    print("<FONT><FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=231>");
    print("<TABLE BORDER=1><TR>");
    print("<TH><FONT COLOR=#FFFFFF>First Name"); 
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Last Name"); 
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Age"); 
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Medication"); 
    print("</FONT></TH></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=fname></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=lname></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 MAXCHAR=3 NAME=age></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=medication></TD></TR>");
    print("<TR><TH><FONT COLOR=#FFFFFF>Address1");
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Address2");
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Address3");
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Address4</TH></TR><TR>"); 
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address1></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address2></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address3></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address4></TD>");
    print("</TR><TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD colspan=4>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add patient */

function YA_InsertPatient($thefname, $thelname, $theage, $theaddress1,
 $theaddress2, $theaddress3, $theaddress4, $themedication)
{   Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
   $patientdata= array( "FirstName" => $thefname,
	"LastName" => $thelname, "Age" => $theage,
	"Address1" => $theaddress1, "Address2" => $theaddress2,
	"Address3" => $theaddress3, "Address4" => $theaddress4,
	"Medication" => $themedication );
/* add the patient */  
    $db->insert("ScripPatient", $patientdata);
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Patient added</TD></TR>");
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert patient */

function Z_AddPrescriber()
{   Z_DrawHeader();
    print("<TD colspan=2 ><FONT COLOR=#FFFFFF>");
    print("<H2>Entering new prescriber data</H2>");
    print("<FONT><FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=241>");
    print("<TABLE BORDER=1><TR><TH><FONT COLOR=#FFFFFF>");
    print("Name</FONT></TH><TH><FONT COLOR=#FFFFFF>");
    print("Qual</FONT></TH><TH><FONT COLOR=#FFFFFF>");
    print("Practice</FONT></TH></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=50 NAME=name></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=13 MAXCHAR=13 NAME=qual></TD>");
    print("<TD><SELECT NAME=practiceID>");
   $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
    $ptable=$db->select("ScripPractice");
    foreach ($ptable as $prow)
    {   $showPracticeID=$prow['PracticeID'];
	$theAddress1=$prow['Address1'];
	$theAddress2=$prow['Address2'];
	print("<OPTION VALUE=$showPracticeID ");
	print("> $showPracticeID $theAddress1 $theAddress2");
   }
    print("</SELECT></TD></TR>\n");
    print("<TR><TH><FONT COLOR=#FFFFFF>NHS No."); 
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Writing Exempt"); 
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Signature File"); 
    print("</FONT></TH></TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=10 ");
    print("MAXCHAR=10 NAME=nhsno></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 MAXCHAR=3 NAME=write></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=15 MAXCHAR=15 NAME=sigfile></TD>");
    print("</TR><TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD colspan=7>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function add prescriber */

function ZA_InsertPrescriber($thename, $thequal,
	$thepracticeID, $thenhsno, $thewrite, $thesigfile)
{   Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 
   $prescriberdata= array("Name" => $thename, "Qual" => $thequal,
	"PracticeID" => $thepracticeID, "NHSNumber" => $thenhsno,
	"WritingExemption" => $thewrite, "SigFileName" => $thesigfile);
/* add the prescriber */  
    $db->insert("ScripPrescriber", $prescriberdata);
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Prescriber added</TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert prescriber */

/* Draw the header for the screens */
function Z_DrawHeader()
{   print("<TABLE ALIGN=CENTER WIDTH=800 BGCOLOR=#003163>\n");
    print("<TR><TD><IMG SRC=images/keep.jpg></TD>");
    print("<TD><IMG SRC=images/school.jpg></TD></TR>");
}   /* end of draw header function */

function A1_AddPractice()
{  Z_DrawHeader();
    print("<TD colspan=2 >");
    print("<FONT COLOR=#FFFFFF>");
    print("<H2>Entering new practice data</H2>");
    print("<FONT><FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=251>");
    print("<TABLE BORDER=1><TR>");
    print("<TH><FONT COLOR=#FFFFFF>Address1");
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Address2");
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Address3");
    print("</FONT></TH><TH><FONT COLOR=#FFFFFF>Address4</TH></TR><TR>"); 
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address1></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address2></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address3></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=30 MAXCHAR=30 NAME=address4></TD>");
    print("</TR><TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD colspan=4>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add practice */

function A1A_InsertPractice($address1, $address2, $address3,$address4)
{   Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 
    $practicedata= array("Address1" => $address1, "Address2" => $address2,
		"Address3" => $address3,"Address4" => $address4);
/* add the prescriber */  
    $db->insert("ScripPractice", $practicedata);
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Practice added</TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
}  /* end of function insert new practice */

function B1_AddOTC()
{   Z_DrawHeader();
    print("<TD colspan=2 ><FONT COLOR=#FFFFFF>");
    print("<H2>Entering new OTC data</H2><FONT>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=261>");
    print("<TABLE BORDER=1><TR>");
    print("<TD><FONT COLOR=#FFFFFF>OTC Text"); 
    print("</FONT></TD></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=500 MAXCHAR=500 NAME=otctext></TD>");
    print("</TR><TR><TD>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function add OTC */

function B1A_InsertOTC($theOTCText)
{   Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
   $OTCdata= array( "OTCText" => $theOTCText);
/* add the OTC text */  
    $db->insert("ScripOTCText", $OTCdata);
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>OTC added</TD></TR>");
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert OTC text */

function C1_AddScrip()
{   Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=271>");
    print("<TR><TH><FONT COLOR=FFFFFF>Letter</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Form</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Patient</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Prescriber</FONT></TH></TR>");
    print("<TR><TD><INPUT TYPE=TEXT ");
    print("NAME=theLetter SIZE=5></TD>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripForms");
    print("<TD><SELECT NAME=theFormID >");
    foreach ($table as $row)
    {   $theformID=$row['FormID'];
        $thefname=$row['FormName'];
	print("<OPTION VALUE=$theformID>$thefname\n");
    }
    print("</SELECT></TD>");
    $table=$db->select("ScripPatient");
    print("<TD><SELECT NAME=thePatient >");
    foreach ($table as $row)
    {   $thepatientID=$row['PatientID'];
        $thefname=$row['FirstName'];
        $thelname=$row['LastName'];
	print("<OPTION VALUE=$thepatientID>$thefname $thelname\n");
    }
    print("</SELECT></TD>");
    $table=$db->select("ScripPrescriber");
    print("<TD><SELECT NAME=thePrescriber >");
    foreach ($table as $row)
    {   $thePrescriberID=$row['PrescriberID'];
        $thename=$row['Name'];
	print("<OPTION VALUE=$thePrescriberID>$thename\n");
    }
    print("</SELECT></TD></TR><TR>");
    print("<TH><FONT COLOR=FFFFFF>Counsel (Y/N)</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Drug 1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Drug 2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Miscellaneous</FONT></TH>");
    print("</TR><TR><TD>");
    print("<TR><TD>");
    print("<INPUT TYPE=TEXT NAME=counsel SIZE=1></TD>");
    $table=$db->select("ScripDrugProblem","","DrugProblemID");
    print("<TD><SELECT NAME=theDrug1 >");
    foreach ($table as $row)
    {   $theDrugProblemID=$row['DrugProblemID'];
        $thename=$row['DrugName'];
	print("<OPTION VALUE=$theDrugProblemID>$thename
($theDrugProblemID)\n");
    }
    print("</SELECT></TD>");
    print("<TD><SELECT NAME=theDrug2 >");
    foreach ($table as $row)
    {   $theDrugProblemID=$row['DrugProblemID'];
        $thename=$row['DrugName'];
	print("<OPTION VALUE=$theDrugProblemID>$thename
($theDrugProblemID)\n");

    }
    print("</SELECT></TD><TD><INPUT TYPE=TEXT");
    print(" NAME=theMisc SIZE=5></TD>");
    print("<TR><TD colspan=7 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}  /* end of add scrip */

function C1A_InsertScrip($theLetter,$theFormID, $thePatient,
	$thePrescriber, $theDrug1, $theDrug2, $theMisc, $theCounsel)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* insert scrip*/  
   $scripdata= array( "Letter" => $theLetter,
	"FormID" => $theFormID, "PatientProblemID" => $thePatient,
	"PrescriberProblemID" => $thePrescriber,
	"DrugProblemID" => $theDrug1, "DrugProblemID2" => $theDrug2,
	"MiscProblemID" => $theMisc, "Counsel"=>$theCounsel);
    Z_DrawHeader();
    $db->insert("ScripsScrips",$scripdata);
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Scrip inserted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function insert scrip */

function D1_AddDrugProblem()
{   Z_DrawHeader();
    print("<TR><TD colspan=2 align=center>\n");
    print("<TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=281>\n");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>\n");
/* line 1 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Drug on form</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Drug replace value</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Drug Name Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Drug Name Mispelled</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Drug Inappropriate</FONT></TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theDrugName ");
    print(" SIZE=40></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugMispelling ");
    print(" SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugNameMissing ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugNameMispelled ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugInappropriateChoice ");
    print(" SIZE=5></TD></TR>\n");
/* line 2 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Strength</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Correct strength</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Drug Not Exist</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Strength Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Strength Unavailable</FONT></TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theStrength ");
    print(" SIZE=10></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theStrengthCorrect ");
    print("SIZE=10></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugNonExist ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theStrengthMissing ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theStrengthUnavailable ");
    print(" SIZE=5></TD>\n");
/* line 3 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Delivery form</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Correct Form</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Form Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Form Unavailable</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Form Inappropriate</FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryForm ");
    print(" SIZE=20></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theFormCorrect ");
    print(" SIZE=20></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormMissing ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormUnavailable ");
    print("SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormInappropriate ");
    print(" SIZE=5></TD>\n");
/* line 4 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Dose</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Correct dose</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Overdose</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Dose Incomplete</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Dose not req.</FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDose ");
    print("VALUE='$theDose' SIZE=40></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseCorrect ");
    print(" SIZE=40></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseOverdose ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseIncomplete ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseNotRequired ");
    print(" SIZE=5></TD>\n");
/* line 5 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Form Not Allowed</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quantity</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Interval required</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Underdose</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Inappropriate Timing</FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDeliveryFormNotAllowed ");
    print(" SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantity ");
    print(" SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=Intervalreq ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseUnderdose ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDoseInappropriateTiming ");
    print(" SIZE=5></TD></TR>\n");
/* line 6 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Correct Quantity</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quantity Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quantity XS (PO)</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quantity Insufficient</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quantity No interval</FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityCorrect ");
    print(" SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityMissing ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityExcessivePO ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityInsufficient ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityNoInterval ");
    print(" SIZE=5></TD></TR>\n");
/* line 7 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Quantity no words</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quantity no figures</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quantity XS (Legal)</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Wrong Script</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Practioner not allowed</FONT></TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theQuantityNoWords ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theQuantityNoFigures ");
    print(" SIZE=5></TD>\n");
    print("<<TD><INPUT TYPE=TEXT NAME=theQuantityExcessiveLegal ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theWrongScrip ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theWrongPractioner ");
    print(" SIZE=5></TD></TR>\n");
/* line 8 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Number of Days</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Purpose</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Marks for purpose</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Blacklisted</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Not in formulary</FONT></TH></TR>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theNosDays ");
    print("SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=thePurpose ");
    print(" SIZE=30></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=thePurposeMarks ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theBlacklisted ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theNotFormulary ");
    print("SIZE=5></TD></TR>\n");
/* line 9 of form */
    print("<TR><TH><FONT COLOR=FFFFFF>Drug must be handwritten</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Does drug appear handwritten</FONT></TH>\n");
    print("<TH>&nbsp;</TH>\n");
    print("<TH>&nbsp;</TH>\n");
    print("<TH>&nbsp;</TH></TR>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=theDrugHandwritten ");
    print(" SIZE=5></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=theDrugHandCorrect ");
    print(" SIZE=5></TD>\n");
    print("<TD>&nbsp;</TD>\n");
    print("<TD>&nbsp;</TD>\n");
    print("<TD>&nbsp;</TD></TR>\n");
    print("<TR><TD colspan=5 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>\n");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>\n");
    print("</TD></TR></TABLE>\n");
}   /* end of function add drug problem */

function D1A_InsertDrugProblem($theDrugName, $theDrugMispelling,  	    
	$theDrugNameMissing,$theDrugNameMispelled, $theDrugInappropriateChoice,
	$theDrugNonExist,$theStrength, $theStrengthCorrect,
	$theStrengthMissing, 		     
	$theStrengthUnavailable,$theDeliveryForm, $theFormCorrect,  
	$theDeliveryFormMissing,$theDeliveryFormUnavailable, 		     
	$theDeliveryFormInappropriate,$theDeliveryFormNotAllowed,   
	$theDose,$theDoseOverdose,$theDoseIncomplete, 		     
	$theDoseNotRequired,$theDoseInappropriate, 		     
	$theQuantity,$theQuantityMissing,$theQuantityExcessivePO,   
	$theQuantityInsufficient,$theQuantityNoInterval, $theQuantityNoWords,  
	$thePurpose, $theBlacklisted, $theNotFormulary, $theWrongScrip, 
	$theWrongPractioner, $theDoseUnderdose, $theQuantityExcessiveLegal,
	$theQuantityNoFigures, $thePurposeMarks, $theDoseCorrect, 
	$theNosDays, $theDrugHandwritten, $theDrugHandCorrect,
	$theDoseInappropriateTiming, $theQuantityCorrect, $theIntervalreq)
{/* insert details of the drug problem */  
   $text= array("DrugName"=>$theDrugName,
"DrugMispelling"=>$theDrugMispelling, "DrugNameMissing"=>$theDrugNameMissing,
"DrugNameMispelled"=>$theDrugNameMispelled,
"DrugInappropriateChoice"=>$theDrugInappropriateChoice,
"DrugNonExist"=>$theDrugNonExist,
"Strength"=>$theStrength, "StrengthCorrect"=>$theStrengthCorrect,
"StrengthMissing"=>$theStrengthMissing,
"StrengthUnavailable"=>$theStrengthUnavailable,
"DeliveryForm"=>$theDeliveryForm, "FormCorrect"=>$theFormCorrect,
"DeliveryFormMissing"=>$theDeliveryFormMissing,
"DeliveryFormUnavailable"=>$theDeliveryFormUnavailable,
"DeliveryFormInappropriate"=>$theDeliveryFormInappropriate,
"DeliveryFormNotAllowed"=>$theDeliveryFormNotAllowed,
"Dose"=>$theDose, "DoseCorrect"=>$theDoseCorrect,
"DoseOverdose"=>$theDoseOverdose, "DoseUnderdose"=>$theDoseUnderdose,
"DoseIncomplete"=>$theDoseIncomplete,
"DoseNotRequired"=>$theDoseNotRequired,
"DoseInappropriateTiming"=>$theDoseInappropriateTiming,
"Quantity"=>$theQuantity,
"QuantityMissing"=>$theQuantityMissing, 
"QuantityCorrect"=>$theQuantityCorrect,
"QuantityExcessivePO"=>$theQuantityExcessivePO,
"QuantityExcessiveLegal"=>$theQuantityExcessiveLegal,
"QuantityInsufficient"=>$theQuantityInsufficient,
"Intervalreq"=>$theIntervalreq,
"QuantityNoInterval"=>$theQuantityNoInterval,
"QuantityNoWords"=>$theQuantityNoWords,
"QuantityNoFigures"=>$theQuantityNoFigures,
"Purpose"=>$thePurpose, "PurposeMarks"=>$thePurposeMarks,
"Blacklisted"=>$theBlacklisted, "NotFormulary"=>$theNotFormulary,
"WrongScrip"=>$theWrongScrip, "WrongPractioner"=>$theWrongPractioner,
"NosDays"=>$theNosDays,
"DrugHandwritten"=>$theDrugHandwritten,
"DrugHandCorrect"=>$theDrugHandCorrect,);
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->insert("ScripDrugProblem",$text);
    $db->close();
    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Drug Problem inserted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function insert drug problem */

function E1_AddPatientProblem()
{   Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=291>");
    print("<TR><TH><FONT COLOR=FFFFFF>Patient</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>ForeName Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Surname Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Age missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>DOB Missing</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Age DOB Mismatch</FONT></TH>");
    print("<TR><TD>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripPatient");
    print("<SELECT NAME=thePatient >");
    foreach ($table as $row)
    {   $thepatientID=$row['PatientID'];
        $thefname=$row['FirstName'];
        $thelname=$row['LastName'];
	print("<OPTION VALUE=$thepatientID>$thefname $thelname\n");
    }
    print("</SELECT></TD><TD><INPUT TYPE=TEXT NAME=theFName ");
    print("SIZE=5></TD><TD><INPUT TYPE=TEXT NAME=theSName ");
    print("SIZE=5></TD><TD><INPUT TYPE=TEXT NAME=theAddress ");
    print("SIZE=5></TD><TD><INPUT TYPE=TEXT NAME=theAge ");
    print("SIZE=5></TD><TD><INPUT TYPE=TEXT NAME=theDOB ");
    print("SIZE=5></TD><TD><INPUT TYPE=TEXT NAME=theAgeDOB ");
    print("SIZE=5></TD></TR>");
    print("<TR><TH colspan=7><FONT COLOR=FFFFFF>Special Text</FONT></TH>");
    print("</TR><TR>");
    print("<TD colspan=7><INPUT TYPE=TEXT NAME=theSText ");
    print("VALUE='$theSText' SIZE=80></TD>");
    print("<TR><TD align=center colspan=7>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add patient problem */

function E1A_AddPatientProblem($thePatient, $theFName, $theSName, 
	$theAddress, $theAge, $theDOB, $theAgeDOB, $theSText)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* insert details of the patient problem */  
   $text= array( "PatientID" => $thePatient,
	"PatientFNameMissing" => $theFName,
	"PatientSNameMissing" => $theSName,
	"PatientAddressMissing" => $theAddress,
	"PatientAgeMissing" => $theAge,
	"PatientDOBMissing" => $theDOB,
	"PatientAgeDOBMismatch" => $theAgeDOB,
	"SpecialText"=>$theSText );
    $table=$db->insert("ScripPatientProblem",$text);
    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Patient Problem inserted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function insert patient problem */

function F1_AddPrescriberProblem()
{   Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=301>");
    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Prescriber</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Name Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>NHS No Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Address Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Address Incomplete</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Address Not UK</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quals Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Quals Inappropriate</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Sig Missing</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Sig Incorrect</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Authorise needed</FONT></TH>\n");
    print("<TR><TD>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripPrescriber");
    print("<SELECT NAME=thePrescriber >");
    foreach ($table as $row)
    {   $thePrescriberID=$row['PrescriberID'];
        $thename=$row['Name'];
	print("<OPTION VALUE=$thePrescriberID>$thename\n");
    }
    print("</SELECT></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theName SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theNHSNo SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theAddress ");
    print("SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theAddressIncomplete ");
    print("SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theAddressNonUK SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theQuals SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theQualsInappropriate ");
    print("SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theSig SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=theSigIncorrect ");
    print("SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=$theAuthorise SIZE=5></TD>");
    print("</TR><TR><TD align=center colspan=11>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add prescriber problem */

function F1A_AddPrescriberProblem($thePrescriber, $theName,
	$theNHSNo, $theAddress, $theAddressIncomplete,
	$theAddressNonUK, $theQuals, $theQualsInappropriate,
	$theSig, $theSigIncorrect, $theAuthorise)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* insert details of the prescriber problem */  
   $text= array( "PrescriberID" => $thePrescriber,
	"PrescriberNameMissing" => $theName,
	"PrescriberNHSNoMissing" => $theNHSNo,
	"PrescriberAddressMissing" => $theAddress,
	"PrescriberAddressIncomplete" => $theAddressIncomplete,
	"PrescriberAddressNonUK" => $theAddressNonUK,
	"PrescriberQualsMissing" => $theQuals,
	"PrescriberQualsInappropriate" => $theQualsInappropriate,
	"PrescriberSigMissing" => $theSig,
	"PrescriberSigIncorrect" => $theSigIncorrect,
	"PrescriberAuthoriseNeeded" => $theAuthorise, );
    $table=$db->insert("ScripPrescriberProblem",$text);
    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Prescriber Problem inserted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of insert prescriber problem */

function G1_AddMiscProblem() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the Misc Problem and display in a form */  
    Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=311>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=90\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Date Missing</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=91\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Date Future</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=92\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Date Late</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=93\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Patient Hand Written</A></FONT></TH></TR>\n");

    print("<tr>");
    print("<TD><INPUT TYPE=TEXT NAME=DateMissing SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=DateFuture SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=DateLate SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=PatientHandwritten ");
    print("SIZE=5></TD></TR>");

    print("<tr>");
    print("<TH COLSPAN=2><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=94\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("File Destination</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=95\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Interaction</A></FONT></TH>\n");

    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=96\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("ZInteraction ACTION</A>");
    print("</FONT></TH><TR>\n");

    print("<TD COLSPAN=2><SELECT NAME=FileDestination ");
    $newtable=$db->select("ScripFileDestinations"); 
    foreach ($newtable as $row) {
        $FileDestinationID=$row['FileDestinationID'];
	$FileText=$row['Text'];
	print("<OPTION VALUE=$FileDestinationID ");
	if ($FileDestinationID==$FileDestination)
	    print("SELECTED ");
	print(">$FileText");
    }
    print(" </SELECT></TD>\n");
    print("<TD><INPUT TYPE=TEXT NAME=Interaction SIZE=5></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=Intaction SIZE=1></TD><TR>");
    print("<TR><TH COLSPAN=4><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=97\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Feedback ");
    print("for interaction</A></FONT></TH></TR>\n");
    print("<TR><TD COLSPAN=4><INPUT TYPE=TEXT NAME=IntFB ");
    print("SIZE=100></TD><TR>");
    print("</TR><TR><TD align=center colspan=4>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add Misc problem */

function G1A_InsertMiscProblem($DateMissing, $DateFuture, $DateLate,
	$PatientHandwritten, $Interaction,$FileDestination, $Intaction, $IntFB)
{   $db=new dbabstraction();
    $db->connect()  or die ($db->getError()); 
/* update details of the misc problem */  
$DateMissing=($DateMissing=="")?0:$DateMissing;
$DateFuture=($DateFuture=="")?0:$DateFuture;
$DateLate=($DateLate=="")?0:$DateLate;
$PatientHandwritten=($PatientHandwritten=="")?0:$PatientHandwritten;
$Intaction=($Intaction=="")?0:$Intaction;
$IntFB=($IntFB=="")?" ":$IntFB;
$Interaction=($Interaction=="")?0:$Interaction;

print("DateMissing =>$DateMissing<BR>");
 	print("DateFuture => $DateFuture,<BR>");
	print("DateLate => $DateLate,<BR>");
	print("PatientHandwritten => $PatientHandwritten,<BR>");
	print("Interaction => $Interaction,<BR>");
	print("IntFB => $IntFB,<BR>");
	print("IntAction =>$Intaction,<BR>");
	print("FileDestination=> $FileDestination ");

   $text= array(
"DateMissing" => $DateMissing,
	"DateFuture" => $DateFuture,
	"DateLate" => $DateLate,
	"PatientHandwritten" => $PatientHandwritten,
	"Interaction" => $Interaction,
	"IntFB" => $IntFB,
	"IntAction" =>$Intaction,
	"FileDestination"=> $FileDestination );
    $table=$db->insert("ScripMiscProblem",$text);
    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Misc Problem inserted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of insert Misc problem */

function H1_AddTermPattern()
{   Z_DrawHeader();
    print("<TD colspan=2 align=center>");
    print("<FONT COLOR=#FFFFFF>");
    print("<H2>Entering new practice data</H2><FONT>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=321>");
    print("<TABLE BORDER=1><TR>");
    print("<TD><FONT COLOR=#FFFFFF>Week 1"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 2"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 3"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 4"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 5"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 6"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 7"); 
    print("</FONT></TD></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk1></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk2></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk3></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk4></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk5></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk6></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk7></TD>");
    print("</TR><TR><TD colspan=7 align=center>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD>");
    print("</TR><TR><TD colspan=7>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add term pattern */

function H1A_InsertTermPattern($wk1, $wk2, $wk3, $wk4, $wk5, $wk6, $wk7)
{   $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
/* add the pattern */  
   $patterndata= array("wk1" => $wk1,"wk2" => $wk2,
	"wk3" => $wk3,"wk4" => $wk4,"wk5" => $wk5,
	"wk6" => $wk6,"wk7" => $wk7);
    $db->insert("ScripTermPattern", $patterndata);
    Z_DrawHeader();
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Term Pattern added</TR>");
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert term pattern */

function I1_AddExamPattern()
{   Z_DrawHeader();
    print("<TD colspan=2 align=center>");
    print("<FONT COLOR=#FFFFFF>");
    print("<H2>Entering new exam data</H2>");
    print("<FONT>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=331>");
    print("<TABLE BORDER=1><TR>");
    print("<TD><FONT COLOR=#FFFFFF>Week 1"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 2"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 3"); 
    print("</FONT></TD><TD><FONT COLOR=#FFFFFF>Week 4"); 
    print("</FONT></TD></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk1></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk2></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk3></TD>");
    print("<TD><INPUT TYPE=TEXT SIZE=3 NAME=wk4></TD>");
    print("</TR><TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD colspan=4>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add exam pattern */

function I1A_InsertExamPattern($wk1, $wk2, $wk3, $wk4)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* add the pattern */  
   $practicedata= array( "wk1" => $wk1,
	"wk2" => $wk2, "wk3" => $wk3, "wk4" => $wk4, );
    $db->insert("ScripExamPatterns", $practicedata);
    Z_DrawHeader();
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Exam Pattern added</TR>");
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert term pattern */

function J1_AddLegalText()
{   Z_DrawHeader();
    print("<TD colspan=2 >");
    print("<FONT COLOR=#FFFFFF>");
    print("<H2>Entering new Legal Text</H2><FONT>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=341>");
    print("<TABLE BORDER=1><TR>");
    print("<TD><FONT COLOR=#FFFFFF>Legal Text"); 
    print("</FONT></TD></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=500 MAXCHAR=500 NAME=legaltext></TD>");
    print("</TR><TR><TD>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add legal text */

function J1A_InsertLegalText($theLegalText)
{   Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
   $Legaldata= array( "LegalText" => $theLegalText );
/* add the legal text */  
    $db->insert("ScripLegal", $Legaldata);
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Legal Text added</TD></TR>");
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert legal text */
function K1_AddHelpText()
{   Z_DrawHeader();
    print("<TD colspan=2 >");
    print("<FONT COLOR=#FFFFFF>");
    print("<H2>Entering new Help Text</H2>");
    HelpHTML();
    print("<FONT>");
    print("<FORM METHOD=POST ACTION=$PHP_SELF>");
    print("<INPUT TYPE=HIDDEN NAME=tab VALUE=351>");
    print("<TABLE BORDER=1><TR><TD>");
    print("<FONT COLOR=#FFFFFF>Tab Number </FONT>");
    print("<INPUT TYPE=TEXT SIZE=5 NAME=thetab></TD></TR>");
    print("<TR><TD><FONT COLOR=#FFFFFF>Help Text"); 
    print("</FONT></TD></TR><TR>");
    print("<TD><INPUT TYPE=TEXT SIZE=500 MAXCHAR=500 NAME=helptext></TD></TR>");
    print("<TR><TD><FONT COLOR=#FFFFFF>Situation"); 
    print("</FONT></TD></TR><TR>");
    print("<TR><TD><INPUT TYPE=TEXT SIZE=250 MAXCHAR=250 NAME=situation></TD>");
    print("</TR><TR><TD>");
    print("<INPUT TYPE=SUBMIT value=Confirm></FORM>");
    print("</FONT></TD></TR><TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function add help */

function K1A_InsertHelpText($theHelpText,$theSituation, $theTabNumber)
{   Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
    $Helpdata= array("HelpText" => $theHelpText,
	"Situation" => $theSituation,"tab" => $theTabNumber);
/* add the help text  */ 
    $db->insert("ScripHelpText", $Helpdata);
    print("<TD colspan=2 >");
    print ("<TR><TD><FONT COLOR=#FFFFFF SIZE=4>Help Text added</TD></TR>");
    print("<TR><TD>"); 
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</FONT></TD></TR></TABLE>");
} /* end of function insert Help text */

function HelpHTML()
{   print("<P>Type in the help text, the situation where it will be used ");
    print("and the tab value which will call it.</P>");
    print("<P>You can use HTML to control the layout of the help messages.<BR>");
     print("The most common you might need are &lt;P&gt; for a new paragraph.<BR>");
     print(" &lt;BR&gt; for a single line break and &lt;EM&gt; for ");
     print("emphasis.<BR>Remember to turn off the tags at the appropriate ");
     print("point by using the same tag name but preceding it with ");
     print("a / character e.g. &lt;/EM&gt; to turn off emphasis.");
}   /* end of function Help HTML */

function L1_EditHelpText() {
    Z_DrawHeader();
 
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

/* Get details of all the help table and display as a table*/  
    $table=$db->select("ScripHelpText");
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one help message from the list");
    print(" and click the submit button to edit the details</H2></FONT></TD>");
    print("</TR>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=361>");

    print("<tr>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Message</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Situation</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=ffffff>New</font></td>");
    print("</tr>");

    foreach ($table as $row) {
        $theHelpID=$row['HelpID'];
	$theTab=$row['tab'];
	$theHelpText=$row['HelpText'];
	$theSituation=$row['Situation'];
	print("<TR><TD>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theHelpID>");
        print("<INPUT TYPE=HIDDEN NAME=theTab value=$theTab></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theHelpID");
	print("</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theHelpText</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
	print("$theSituation</FONT></TD></TR>");
    }
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}  /* end of function edit help text */

function L1A_EditSingleHelp($SID) {
    $db=new dbabstraction();
    $db->connect() 
		or die ($db->getError()); 
/* Get details of the help text and display in a form */  
    $table=$db->select("ScripHelpText",array('HelpID'=>$SID));
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theHelpText=$row['HelpText'];
            $theSituation=$row['Situation'];
        };
    } else {
        $theHelpText="";
	$theSituation="";
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=362>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");
    print("<FONT color=#FFFFFF>");
    HelpHTML();
    print("</FONT>");
 
    print("<TR><TH><FONT COLOR=FFFFFF>HelpText</FONT></TH><TR>");
    print("<TR><TD><FONT COLOR=FFFFFF>");

    print("<textarea name=theHelpText rows='10' cols='100'>$theHelpText</textarea>");

//    print("<INPUT TYPE=TEXT NAME=theHelpText VALUE='$theHelpText'
//                    SIZE=255>");
    print("</FONT></TD></TR>");

    print("<TR><TH><FONT COLOR=FFFFFF>Situation</FONT></TH></TR>");
    print("<TR><TD>");
    print("<textarea name=theSituation rows='2' cols='100'>");
    print("$theSituation</textarea>");
//    print("<INPUT TYPE=TEXT NAME=theSituation VALUE='$theSituation'
//          SIZE=255>");
    print("</TD></TR>");

    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}  /* end of function edit single help */

function L1B_UpdateHelpText($SID, $theHelpText, $theSituation) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the practice */  
    $helpdata= array("HelpText" => $theHelpText,
	"Situation" => $theSituation);
    Z_DrawHeader();

    if ($SID!='0') {
        $db->update("ScripHelpText",$helpdata, array('HelpID'=>$SID));
    } else {
        $db->insert("ScripHelpText",$helpdata);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Help text updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function M1_EditClassifyMA() {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

    print("<tr><td colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one case from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</td></tr>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=381>");

    print("<tr>");
    print("<th rowspan=2></th>");
    print("<TH rowspan=2><FONT COLOR=FFFFFF>Letter Code</FONT></TH>");
    print("<TH colspan=2><FONT COLOR=FFFFFF>Form Name</FONT></TH>");
    print("<TH colspan=2><FONT COLOR=FFFFFF>Therapeutic</FONT></TH>");
    print("<TH colspan=2><FONT COLOR=FFFFFF>Legal</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>File Destination</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>FD Loss</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<th><font color='#ffffff'>Type</font></th>");
    print("<TH><FONT COLOR=FFFFFF>Originator</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>2</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>File Destination</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>FD Loss</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=lettercode value='0' Checked></td>");
    print("<td colspan='6'><FONT COLOR=FFFFFF>New</font></td>");
    print("</tr>");

    $table=$db->select("ScripClassifyMA","","LetterCode");
    foreach ($table as $row) {
        $theLetterCode=$row['LetterCode'];
	$theFormID=$row['FormType'];

        $condition = array('FormID'=>$theFormID);
        $tableFT = $db->select("ScripForms",$condition);
        foreach ($tableFT as $rowFT) {
            $theFormName2 = $rowFT['FormName2'];
            $theFormName1 = $rowFT['FormName1'];
        };

        $thethera1=$row['thera1'];
	$thethera2=$row['thera2'];
	$thelegal1=$row['legal1'];
	$thelegal2=$row['legal2'];

	print("<tr>");
        print("<TD><INPUT TYPE=RADIO NAME=lettercode VALUE=$theLetterCode>");
 	print("<TD><FONT COLOR=FFFFFF>$theLetterCode</FONT></TD>");
 	print("<TD><FONT COLOR=FFFFFF>$theFormName2</FONT></TD>");
 	print("<TD><FONT COLOR=FFFFFF>$theFormName1</FONT></TD>");
 	print("<TD><FONT COLOR=FFFFFF>$thethera1</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$thethera2</FONT></TD>");
 	print("<TD><FONT COLOR=FFFFFF>$thelegal1</FONT></TD>");
        if ($thelegal2 == '0') { // Show a blank instead of a zero
            print("<TD><FONT COLOR=FFFFFF></FONT></TD>");
         } else {
            print("<TD><FONT COLOR=FFFFFF>$thelegal2</FONT></TD>");
        };
     };

    print("<tr><TD colspan=7 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function M1A_EditSingleClassify($code) {
    global $_POST;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    // Get details of ClassifyMA and display in a form

    $condition=array('LetterCode'=>$code);
    $table=$db->select("ScripClassifyMA",$condition); 
    Z_DrawHeader();

    // N.B. The FormType field is now a number (no longer a string) ...
    if ($code!='0') {
        foreach ($table as $row) {
            $theformtype=$row['FormType'];
            $thethera1=$row['thera1'];
            $thethera2=$row['thera2'];
            $thelegal1=$row['legal1'];
            $thelegal2=$row['legal2'];
        };
    } else {
        $theformtype="";
        $thethera1="";
        $thethera2="";
        $thelegal1="";
        $thelegal2="";
    };

    if ($theformtype) {
        // Look in ScripForms to find what form this number represents - both name and originator ...
        $condition=array('FormID'=>$theformtype);
        $table=$db->select("ScripForms",$condition);
        foreach ($table as $row) {
            $thetypeDB = $row['FormName2']; 
            $theoriginatorDB = $row['FormName1']; 
        };
    };
    // ... these are the type and the originator of the present form (as in the database).

    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name='tab' value='382'>");
    print("<input type='hidden' name='lettercode' value='$code'>");

    print("<tr>");
    print("<td colspan=2 align=center>");
    print("<table border='1'>");
    print("<tr>");
    print("<th><font color='#ffffff'>Letter Code</font></th>\n");
    print("<th><font color='#ffffff'>Form Type</font></th>\n");
    print("<th><font color='#ffffff'>Originator</font></th>\n");
//    print("<TH><FONT COLOR=FFFFFF>File Destination</FONT></TH>\n");
//    print("<TH><FONT COLOR=FFFFFF>FD Loss</FONT></TH></TR>\n");

    // ROW 1 (applies to whole script):
    print("<tr>");

    // CELL 1 - Letter Code: Quote the new code if one exists; otherwise use that in d/b ...
    print("<td><input type=text name=lettercode value='");
    if ($_POST['lettercode']) { print($_POST['lettercode']); } else if ($code!='0') { print("$code"); };
    print("'></td>");

    // CELL 2 - Form type (field FormName2 in ScripForms): 
    $tableForms=$db->select("ScripForms");
    // List each type of form (e.g. Private) ONCE only ...
    for ($i = 0; $i < count($tableForms); $i++){
        $notFound = true;
        // Loop over forms only ...
        for ($j = 0; $j < count($tableFormtypesOnly); $j++){
            // If already in there, we don't want to add it and we don't even want to go any further down $tableFormtypesOnly (hence break) ...
            if ($tableForms[$i]['FormName2'] == $tableFormtypesOnly[$j]){
                $notFound = false; break;
            };
        };
        // Not in table so far - add it ...
        if ($notFound) {
            $tableFormtypesOnly[] = $tableForms[$i]['FormName2'];
        };
    };

    // We want the sort to be case insensitive: Produce an array that is entirely lower case:
    $lowerCase = array_map('strtolower', $tableFormtypesOnly);
    // Now use multisort, sort $tableFormtypesOnly in the same way as $lowerCase:
    array_multisort($lowerCase, SORT_ASC, SORT_STRING, $tableFormtypesOnly);
    // ... array in alphabetical order

    print("<td>");

    // SELECT typeofform ...
    // An automatically submitting drop down box for FORMTYPES (submits to $tab==382) ...
    print("<select name='typeofform' ");
    print("onchange='javascript:document.forms[0].submit()'>");
    print("<option value='0'>Select ...");
    foreach ($tableFormtypesOnly as $element) { // $element is the loop parameter and 
        print("<option value='$element' ");
        // If posted, make that SELECTED ...
//        if ($element == $_POST['formtype']) {
        if ($element == $_POST['typeofform']) {
            print(" selected");
        };

        // If nothing posted, make the content of the database (if any) SELECTED ...
//        if ($element == $thetypeDB && !$_POST['formtype']) {
        if ($element == $thetypeDB && !$_POST['typeofform']) {
            print(" selected");
        };
        print(">$element");
    };
    print("</select>");
    print("</td>");

    // CELL 3 - Originator (field FormName1 in ScripForms): 
    print("<td>");

    // SELECT originator ...
    // If a formtype has been posted (i.e. something just done), look for its originator; otherwise default to the originators of the form in the database ...
    if ($_POST['typeofform']){
        $condition = array ('FormName2'=>$_POST['typeofform']);
    } else {
        $condition = array ('FormName2'=>$thetypeDB);
    };

    // Look for all the possible originators for this type of form ...
    $table = $db->select("ScripForms", $condition);
    // ... $table now contains them (in field originator).
    if (count($table) > 1) {
        print("<select name=originator>");
        print("<option value='0'>Select from ...");
        foreach ($table as $row){
            $originator = $row['FormName1'];
            $formID = $row['FormID'];
//            print("<option value='$formID'");
            print("<option value='$originator'");
            if ($originator == $theoriginatorDB) { print(" selected"); };
            print(">$originator\n");
        };
    } else {
        foreach ($table as $row){
            $formID = $row['FormID'];
        };
        print("<input type='hidden' name='formid' value='$formID'>");
    };
    print("</select>");
    print("</td>");

    print ("</tr>");

    print ("<tr>");
    print("<td colspan='3'></td>");
    print ("</tr>");

    print ("<tr>");
    print("<th><font color='#ffffff'>Item</font></th>");
    print("<TH><FONT COLOR=FFFFFF>Therapeutic</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Legal</FONT></TH>\n");
    print ("</tr>");

    // DATA ROW 2 (first item on script):
    print ("<tr>");

    // CELL 1: 
    print("<td align='right'><font color='#ffffff'>1</font></td>");

    // CELL 2:
    print("<TD><INPUT TYPE=TEXT NAME=thethera1 VALUE='$thethera1' SIZE=10></TD>");

    // CELL 3:
    $ptable=$db->select("ScripLegal"); 
    print("<td><select name='legal1'>");
    print("<option value=''>Select from ...");
    foreach ($ptable as $prow) {
        $legaltext=$prow['LegalText'];
        $legalID=$prow['LegalID'];
        print("<OPTION VALUE=$legalID ");
        if ($thelegal1==$legalID) print("SELECTED ");
        print("> $legaltext");
    };
    print("</select>\n</td>");
    print("</tr>");

    // DATA ROW 3 (second item on script - assuming there is one):
    print ("<tr>");

    // CELL 1: 
    print("<td align='right'><font color='#ffffff'>2</font></td>");

    // CELL 2: 
    print("<td><input type='text' name=thethera2 value='$thethera2' size='10'></td>");

    // CELL 3: 
    print("<td><select name=legal2>");
    print("<option value=''>");
    $ptable=$db->select("ScripLegal"); 
    foreach ($ptable as $prow) {
        $legaltext=$prow['LegalText'];
        $legalID=$prow['LegalID'];
        print("<OPTION VALUE=$legalID ");
        if ($thelegal2==$legalID) print("selected ");
        print("> $legaltext");
    };
    print("</select>");
    print("\n");
    print("</td>");

    print("</tr>");

    // LAST ROW (button):
    print("<tr>");
    print("<td align=center colspan=12><input type='submit' name='Update' value='Update'></td>");
    print("</tr>");

    print("</form></table>");

    print("</TD></TR>");

    print("<tr>");
    print("<td><a href='scripadmin.php?tab=5'>Return to Menu</a></td>");
    print("</tr>");

    print("</table>");
};

function M1B_UpdateClassifyMA($code, $formID, $thera1, $thera2, $legal1, $legal2, $serialNumber) {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    // Update details of classify model answer ... 

    // Ought to only attempt to update anything passed to the function array_merge

    // Declare the array (must do this in PHP5 before attempting a merger ...
    $data = array ();

    // Now add non-null items ...
    if ($formID){ $data = array_merge($data, array("FormType" => $formID)); };
    if ($thera1){ $data = array_merge($data, array("thera1" => $thera1)); };
    if ($thera2){ $data = array_merge($data, array("thera2" => $thera2)); };
    if ($legal1){ $data = array_merge($data, array("legal1" => $legal1)); };
    if ($legal2){ $data = array_merge($data, array("legal2" => $legal2)); };
    if ($serialNumber){ $data = array_merge($data, array("FormType" => $serialNumber)); };

    // Old form ...
//    $data = array("FormType" => $formID, "thera1" => $thera1, "thera2" => $thera2, "legal1" =>$legal1 , "legal2" => $legal2);
    Z_DrawHeader();

    $condition=array("LetterCode"=>$code);
    $table=$db->select("ScripClassifyMA",$condition);
    foreach ($table as $row) {
        $testExistence=$row['LetterCode'];
    };

    if ($testExistence==$code) {
        $condition=array('LetterCode'=>$code);
        $db->update("ScripClassifyMA",$data, $condition);
    } else {
        $data=array_merge($data,array("LetterCode"=>$code));

        $db->insert("ScripClassifyMA",$data);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Classify model answer updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function N1_EditLabelCode() {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
/* get details of all the help table and display as a table*/  
    $table=$db->select("ScripLabelCodes");
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one answer from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=391>");
    print("<TR><TH>&nbsp;</TH><TH><FONT COLOR=FFFFFF>Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH></TR>\n");

    print("<tr>");
    print("<td><input type=radio name=labelID value='0' checked></td>");
    print("<td colspan='2'><font color=ffffff>New Case</font></td>");
    print("</tr>");

    foreach ($table as $row) {
     	$theLabelID=$row['LabelID'];
	$theCode=$row['Code'];
	$theText=$row['Text'];
	print("<TR><TD><INPUT TYPE=RADIO NAME=labelID VALUE=$theLabelID>\n");
	print("</TD><TD><FONT COLOR=FFFFFF>$theCode</FONT></TD>\n");
	print("<TD><FONT COLOR=FFFFFF>$theText</FONT></TD></TR>\n");
    }
    print("<TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
} /* end of function edit label codes */

function N1A_EditSingleLabelCode($labelID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* get details of label code and display in a form */  
$table=$db->select("ScripLabelCodes",array('LabelID'=>$labelID)); 
    Z_DrawHeader();

    if ($labelID != '0') {
        foreach ($table as $row) {
            $thecode=$row['Code'];
            $thetext=$row['Text']; 
        };
    } else {
        $thecode="";
	$thetext=""; 
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=392>");
    print("<INPUT TYPE=HIDDEN NAME=labelID value='$labelID'>");

    print("<TR><TH><FONT COLOR=FFFFFF>Code</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH></tr>\n");

//    print("<TR><TD><FONT COLOR=FFFFFF>$thecode</FONT></TD>");
    print("<TR>");
    print("<TD valign='top'>");
    print("<input type=text name=thecode value='$thecode'></TD>");
    print("<td><textarea name=thetext rows=5 cols=55>");
    print("$thetext</textarea></td>");
//    print("<TD><INPUT TYPE=TEXT NAME=thetext ");
//    print("VALUE='$thetext' SIZE=80></TD>");
    print("</TR>");

    print("<TR><TD align=center colspan=12>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function edit single label code */

function N1B_UpdateLabelCode($labelID, $thecode, $thetext) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* update details of the label code  */  
    $data= array("Text" => $thetext, "Code" => $thecode);
    $condition=array('LabelID'=>$labelID);
    Z_DrawHeader();

    if ($labelID != '0') {
        $db->update("ScripLabelCodes",$data, $condition);
    } else {
        $db->insert("ScripLabelCodes",$data);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Label Code updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function update label codes */

function O1_AddLabelCode()
{    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* get details of label code and display in a form */  
$table=$db->select("ScripLabelCodes",array('LabelID'=>$labelID)); 
    Z_DrawHeader();
    foreach ($table as $row)
    {   $thecode=$row['Code'];
	$thetext=$row['Text']; 
    }
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=401>");
    print("<INPUT TYPE=HIDDEN NAME=labelID value=>$labelID");
    print("<TR><TH><FONT COLOR=FFFFFF>Code</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Text</FONT></TH>\n");
    print("<TR><TD><INPUT TYPE=TEXT NAME=thecode SIZE=10></TD>");
    print("<TD><INPUT TYPE=TEXT NAME=thetext SIZE=80></TD></TR>");
    print("<TR><TD align=center colspan=12>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function add a label code */

function O1A_InsertLabelCode($thecode,$thetext)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* insert details of the label code  */  
    $data= array("Text" => $thetext, "Code"=>$thecode);
    Z_DrawHeader();
    $db->insert("ScripLabelCodes",$data);
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Label Code inserted");
     print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function insert label code */

function P1_AddForm()
{   Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=411>");
    print("<TR><TH><FONT COLOR=FFFFFF>Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Hex Colour</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Colour</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Addict</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>NameBox?</FONT></TH>");
    print("</TR><TR><TD>");
    print("<INPUT TYPE=TEXT NAME=theName SIZE=30>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=theColour SIZE=6>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=theColourName SIZE=15>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=theAddict SIZE=5>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=theNameBox SIZE=5>");
    print("</TD></TR>");
    print("<TR><TH colspan=4 align=center><FONT COLOR=FFFFFF>");
    print("Special Text</FONT>");
    print("</TH><TH><FONT COLOR=FFFFFF>Group</FONT></TH></TR>");
    print("TR><TD COLSPAN=4>");
    print("<INPUT TYPE=TEXT NAME=theText SIZE=50>");
    print("</TD><TD>");
    print("<SELECT NAME=group>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $newtable=$db->select("ScripFormGroups");
    foreach ($newtable as $newrow)
    {   $theID=$newrow['GroupID'];
	$thename=$newrow['GroupName'];
	print("<OPTION VALUE=$theID> $thename");
    }
    print("</SELECT>\n</TD>");
    print("</TR><TR><TD colspan=5 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}  /* end of function add single form */

function P1A_InsertForm($theName, $theColour,$theColourName, 
	$theText, $theNameBox, $theAddict, $group)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* update details of the form */  
   $formdata= array(
	"FormName" => $theName, "FormColour" => $theColour,
	"FormColourWords" => $theColourName,
	"Addict" => $theAddict, "SpecialText"=>$theText,
	"NameBox"=>$theNameBox, "GroupID"=>$group);
    Z_DrawHeader();
    $db->insert("ScripForms",$formdata);
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Form inserted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function Q1_EditLabelMA() {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

    // Must copy NSF into a local variable even if it's not being re-set (otherwise it won't persist) ...

    $table=$db->select("ScripDoseCodesMA","","DrugProblemID");
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one drug from the list and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=421>");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>Drug (Problem Number & Case)</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Dose Code</FONT></TH>\n");
    print("<TH><FONT COLOR=FFFFFF>Ancillary Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Ref Req</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Quantity</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>NSF</FONT></TH>");
    print("</TR>\n");

    print("<tr>");
    print("<td><input type='radio' name='maid' value='0' checked>\n");
    print("<td colspan='6'><font color=ffffff>New Case</font></td>");
    print("</tr>");

    foreach ($table as $row) {
        $theMAID=$row['MAID'];
        $theDoseLabel=$row['Doselabel'];
        $theAncLabel=$row['Ancillabel'];
        $thedrugid=$row['DrugProblemID'];
        $theRefReq=$row['RefReq'];
        $theLabelQuantity=$row['LabelQuantity'];
        $theLabelNSF=$row['LabelNSF'];
        $theNSFID=$row['NSFID'];

        print("<tr>");
        print("<td><INPUT TYPE=RADIO NAME=MAID VALUE=$theMAID>\n");
        print("</td>");

        $condition=array("DrugProblemID"=>$thedrugid);
        $newtable=$db->select("ScripDrugProblem",$condition);
        foreach ($newtable as $newrow) {
	     $theText=$newrow['DrugName'];
        };

        $condition=array("DrugProblemID"=>$thedrugid);
        $newtable=$db->select("ScripsScrips",$condition);
        foreach ($newtable as $newrow) {
	     $theCaseID=$newrow['Letter'];
        };

        $condition=array("DrugProblemID2"=>$thedrugid);
        $newtable=$db->select("ScripsScrips",$condition);
        foreach ($newtable as $newrow) {
	     $theCaseID=$newrow['Letter']."[2]";
        };

        print("<TD><FONT COLOR=FFFFFF>$theText ");
        print("($thedrugid");
        if ($theCaseID) { print(" & $theCaseID"); };
        print(")</FONT></TD>\n");

        print("<TD><FONT COLOR=FFFFFF>$theDoseLabel</FONT></TD>\n");
        print("<TD><FONT COLOR=FFFFFF>$theAncLabel</FONT></TD>\n");
        print("<TD><FONT COLOR=FFFFFF>$theRefReq</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theLabelQuantity</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theNSFID</FONT></TD>");
        print("</TR>\n");
    };
    print("<TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>");
    print("Return to Menu");
    print("</A></TD></TR></TABLE>");
};

function Q1A_EditSingleLabelMA($labelID) {
    global $_POST;
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Read any existing entry from the d/b ...
    if($labelID != '0') {
        $table=$db->select("ScripDoseCodesMA",array("MAID"=>$labelID));
        foreach ($table as $row) {
            $DrugProblemID=$row['DrugProblemID'];
            $Doselabel=$row['Doselabel'];
            $Ancillabel=$row['Ancillabel'];
            $refreq=$row['RefReq'];
            $labelQuantity=$row['LabelQuantity'];
            $labelNSF=$row['LabelNSF'];
            $NSFID=$row['NSFID'];
        }; 
    } else {
        $DrugProblemID="";
        $Doselabel="";
        $Ancillabel="";
        $refreq="";
        $labelQuantity="";
        $labelNSF="";
        $NSFID="";
    };

    // If drugif is set in the POST array, set it locally here ...
    if ($_POST['drugid']) {
        $DrugProblemID = $_POST['drugid'];
    };

    // ... and if it is set, find the case in ScripsScrips; it might be the first item on a script ...
    if ($DrugProblemID) {
        $condition=array("DrugProblemID"=>$DrugProblemID);
        $newtable=$db->select("ScripsScrips",$condition);
        foreach ($newtable as $newrow) {
            $theCaseID=$newrow['Letter'];
        };
    };

    // ... or it might be the second item ...
    if ($DrugProblemID) {
        $condition=array("DrugProblemID2"=>$DrugProblemID);
        $newtable=$db->select("ScripsScrips",$condition);
        foreach ($newtable as $newrow) {
            $theCaseID=$newrow['Letter']."[2]";
        };
    };

    // Turn drug number into name, strength and form (using the contents of the POST array if set or $NSFID if not): set the condition ...
    if ($_POST['theDrugNumber']) {
        $condition=array ('ID'=>$_POST['theDrugNumber']);
    } else if ($NSFID) {
        $condition=array ('ID'=>$NSFID);
    };

    // If the number in the condition is non-zero, look it up in ScripCDPageTitleAndLabel and read N, S and F ...
    if ($condition['ID'] > 0) {
        $table=$db->select("ScripCDPageTitleAndLabel", $condition);
        foreach ($table as $row) {
            $Formname=$row['Brand']." -- ";
            if ($row['Strength']) { $Formname.=$row['Strength']." -- "; }; 
            $Formname.=$row['Form'];
        };
    };

    // The DRUG search:
    if ($_POST['searchdrug']){ // Search Drug was pressed ...
        // Read all the drugs available  e.g. disprin ...
        $allDrug = $db->select("ScripCDPageTitleAndLabel");
        $firstLettersDrug = strtoupper($_POST['firstlettersdrug']);
        if ($firstLettersDrug) { // Stop if no letters specified ...
            for ($i = 0; $i < count($allDrug); $i++){ // Loop over all the drugs ...
                if ($allDrug[$i]['ID'] != $someDrug[0]['ID']) {
                    // If different to current selection, consider whether it satisfies user's search criterion ...
                    $location = strpos(strtoupper($allDrug[$i]['Brand']), $firstLettersDrug);
                    if ($location !== false && $location == 0){
                        $someDrug[] = $allDrug[$i]; // ... add it into the array
                    };
                };
            };
        };
    };
    // ... the array $someDrug now contains a sub-set of drugs: from the search criterion and the curent drug (if one).

    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>");

    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='422'>");
    print("<input type='hidden' name='MAID' value='$labelID'>");
    print("<input type='hidden' name='theDrugNumber' value=".$_POST['theDrugNumber'].">");

    // LINE 0 ...
    print("<tr><td colspan='2' align='center'>");
    print("<font color='#ffffff' size='+1'>");
    print("Case: ");
    if ($theCaseID) {
        print("$theCaseID");
    } else {
        print("[<i>Not assigned to a case at present</i>]");
    };
    print("</font></td></tr>");

    // LINE 1 NAME, STRENGTH AND FORM ...
    print("<tr>");
    print("<th><font color='#ffffff'>Name, Strength and Form</font></th>");
    print("<th><font color='#ffffff'>Search</font></th>");
    print("</tr>");

    print("<tr><td colspan='2'><font color='#ffffff'>");
    print("Type the beginning of the name of the drug into the pink box (e.g. asp for aspirin), press the button and make a selection ");
    print("from the drop-down box that appears. The selection will automatically appear in the white box.");
    print("</font></td></tr>");

    print("<tr>");
    // The box (pink background) for the first few letters of the DRUG name ...
    print("<td>");
    print("<input type='text' name='firstlettersdrug' style='color:000090;background:ffccff;'>");
    // The search button for the DRUG ...
    print("<input type='submit' name='searchdrug' value='Search Drugs List' style='width:120px;'>");

    print("<br>");

    // The drop-down list for the search results of DRUG (provided the list has some elements) ...
    if (count($someDrug) > 0) {
        print("<select name='theDrugNumber' onchange='javascript:document.forms[0].submit()' style='width:510px;'>");
        if (!$_POST['submitdrug']) {
            print("<option value=''>Select one of ...");
            foreach ($someDrug as $row) {
                print("<option value='".$row['ID']."'");
                // Show chosen drug (if any) as default ...
                if ($row['ID'] == $theDrugNumber) { print(" selected"); };
                print(">");
                print($row['Brand']." ".$row['Strength']." ".$row['Form']);
            };
        };
        print("</select>");
    } else {
        print("<br>");
    };
    print("</td>");

    // Box for NSF chosen by the user ...
    print("<td valign='top'>");
    print("<input type='text' name='formname' size=50 value='$Formname' readonly>");
    print("</td>\n");

    print("</tr>");

    // LINE 2 - DRUG ...
    print("<input type=hidden name='MAID' value=$labelID>");

    print("<tr>");
    print("<th><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=100\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug</A></FONT></TH>");
    print("</tr>\n");

    print("<tr>");
    print("<td><select name='drugid'>");
    $newtable=$db->select("ScripDrugProblem");
    foreach ($newtable as $newrow) {
        $thedrug=$newrow['DrugName'];
        $theid=$newrow['DrugProblemID'];
        print("<option value='$theid'");
        if ($DrugProblemID==$theid) print(" SELECTED");
        print("> $thedrug ($theid)");
    };
    print("</select>\n</td>");
    print("</tr>");

    // LINE 3 LABEL - QUANTITY ...
    print("<tr>");
    print("<th><font color=ffffff>Label - Quantity<br>");
    print("Must be included if quantity not on script ");
    print("(also used by CD)</font></th>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=103\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Ref required?</A></FONT></TH>");
    print("</tr>");

    print("<tr>");
    if ($_POST['labelQuantity']) {
        $labelQuantity = $_POST['labelQuantity'];
    };
    print("<td><input type=text name='labelQuantity'
                     value='$labelQuantity' size=70></td>");
//    print("<td><input type=text name='labelNSF'
//                     value='$labelNSF' size=50></td>");

    if ($_POST['therefreq']) {
        $refreq = $_POST['therefreq'];
    };
    print("<TD><INPUT TYPE=TEXT NAME=therefreq SIZE=1");
    print(" VALUE='$refreq'>");
    print("</TD>");
    print("</tr>");

    print("<tr><td colspan='2'>&nbsp;</td></tr>");

    // LINE 4 DOSE CODES ...
    print("<tr><td colspan='2'><font color='#ffffff'>");
    print("Enter any text from the dose code into the pink box ");
    print("(<i>e.g.</i> tongue) and type the appropriate dose code ");
    print("in the box. Add further codes as necessary.");
    print("</font></td></tr>");

    print("<tr><th><FONT COLOR=FFFFFF><A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=102\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Dose codes</A></FONT></TH></tr>");

    print("<tr>");
    // Search for text in a DOSE code ...
    print("<td>");
    print("<input type='text' name='searchtextdose' ");
    print("style='color:000090;background:ffccff;'>");

    // DOSE Search button ...
    print("<input type='submit' name='searchdose' ");
    print("value='Search' style='width:180px;'>");
    print("<br>");

    // The Search DOSE buttons searches ...
    if ($_POST['searchdose']){ // Read text ...
        // Read all the dose codes available e.g. for one patch
        $allDose = $db->select("ScripDoseCodes");
        $searchTextDose =
                  strtoupper($_POST['searchtextdose']);
        if ($searchTextDose) { // If not null ...
            for ($i = 0; $i < count($allDose); $i++){
            $location = strpos(strtoupper($allDose[$i]['Text']),
                           $searchTextDose);             
                if ($location !== false && $location >= 0){
                    $someDose[] = $allDose[$i];
                };
            };
        };
    };

    // The drop-down list for the DOSE results ...
    if (count($someDose) > 0) {
        print("<select name='theSelectDose' ");
        print("style='width:550px;'>");
        foreach ($someDose as $row) {
            print("<option value='".$row['Code']."'>");
            print($row['Code']." - ".$row['Text']);
        };
        print("</select>");
    } else {
        print("<br>");
    };
    print("</td>");

    print("<td valign='top'>");
    if ($_POST['theDose']) {
        $Doselabel=$_POST['theDose'];
    };
    print("<input type='text' name=theDose size='50'");
    print(" value='$Doselabel'>");
    print("</td>");
    print("</tr>");

    // LINE 5 ANCILLARY CODES ...
    print("<tr><td colspan='2'><font color='#ffffff'>");
    print("Enter any text from the ancillary code into the pink box ");
    print("(<i>e.g.</i> drowsiness for all the codes including ");
    print("the word drowsiness) and type the appropriate code ");
    print("in the box. Add further codes as necessary.");
    print("</font></td></tr>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=101\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Ancillary codes</A></FONT></TH>");
    print("</tr>");

    print("<tr>");

    // Search for text in an ANCILLARY code ...
    print("<td>");
    // If the Search ANCILLARY button has been pressed, then read
    // the phrase that is to be searched for and do the searching ...
    if ($_POST['searchancillary']){
        // Read the all the ancillary codes available e.g. for drowsiness
        $allAncillary = $db->select("ScripLabelCodes");
        $searchTextAncillary =
                 strtoupper($_POST['searchtextancillary']);
        if ($searchTextAncillary) {
            for ($i = 0; $i < count($allAncillary); $i++){
                $location = strpos(strtoupper($allAncillary[$i]['Text']),    
                                            $searchTextAncillary);
                if ($location !== false && $location >= 0){
                    $someAncillary[] = $allAncillary[$i];
                };
            };
        };
    };
    print("<input type='text' name='searchtextancillary' ");
    print("style='color:000090;background:ffccff;'>");

    // ANCILLARY Search button ...
    print("<input type='submit' name ='searchancillary' ");
    print("value='Search for Ancillary Code' style='width:180px;'>");
    print("<br>");

    // The drop-down list for the ANCILLARY results ...
    if (count($someAncillary) > 0){
        print("<select name='theSelectAncillary' ");
        print("style='width:550px;'>");
        foreach ($someAncillary as $row) {
            print("<option value='".$row['Code']."'>");
            print($row['Code']." - ".$row['Text']);
        };
        print("</select>");
    } else {
        print("<br>");
    };
    print("</td>");

    // The box for the ANCILLARY CODES ...
    print ("<td valign='top'>");
    if ($_POST['theAncil']) {
        $Ancillabel=$_POST['theAncil'];
    };
    print("<INPUT TYPE=TEXT NAME='theAncil' SIZE=50");
    print(" VALUE='$Ancillabel'>");
    print("</TD>");

    print("</tr>");

/*    print("<TR><TD>");
    print("<P><A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=426\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,");
print("width=900,height=400')\">");
    print("Dose codes look up table</A></P>\n"); 
    print("</TD></TR>");
*/

    print("<tr><td colspan='2'>&nbsp;</td></tr>");

    // BUTTON LINE ...
    print("<tr><td colspan=2 align=center>");
    print("<input type='submit' name='Update' value='Update'>");
    print("</td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function Q1B_UpdateLabelCodeMA($MAID,$drugid, $theAncil, $theDose,
        $therefreq, $labelQuantity, $labelNSF, $theDrugNumber) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the form ...
    $formdata = array(
        "DrugProblemID" => $drugid,
        "Doselabel" => $theDose,
        "Ancillabel"=>$theAncil,
        "RefReq"=>$therefreq,
        "LabelQuantity"=>$labelQuantity,
//        "NSFID"=>$theDrugNumber,
        "LabelNSF"=>$labelNSF
    );
    Z_DrawHeader();

    // This is a slight fudge - if the NSF is zero, don't write it; or, alternatively, write it if it is not zero ...
    if ($theDrugNumber){
        $formdata = array_merge ($formdata, array("NSFID"=>$theDrugNumber));
    };

    if ($MAID) { // If an ID exists, this must be an update ...
        $condition=array("MAID"=>$MAID);
        $db->update("ScripDoseCodesMA", $formdata, $condition);
    } else { // ... otherwise it's an insert ...
        $db->insert("ScripDoseCodesMA", $formdata);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Model answer updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
}  /* end of function update dose code MA */

function R1_AddLabelMA()
{   Z_DrawHeader(); 
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=431>");
    print("<TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=100\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Drug</A></FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=101\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Ancillary codes</A></FONT></TH>");
    print("</TR><TR><TD>");
    print("<SELECT NAME=drugid>");
    $newtable=$db->select("ScripDrugProblem");
    foreach ($newtable as $newrow)
    {   $thedrug=$newrow['DrugName'];
	$theid=$newrow['DrugProblemID'];
	print("<OPTION VALUE='$theid'> $thedrug");
    }
    print("</SELECT>\n</TD><TD>");
    print("<INPUT TYPE=TEXT NAME='theAncil' SIZE=50>");
    print("</TD></TR><TD COLSPAN=2>");
    print("<P><A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=425\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,");
print("width=900,height=400')\">");
    print("Ancillary codes look up table</A></P>\n"); 
    print("</TD></TR><TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=102\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Dose codes</A></FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>");
    print("<A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=500&index=103\'');
    print(",'miniwin','toolbar=0,menubar=0,width=400,height=350')\">");
    print("Ref required?</A></FONT></TH></TR>");
    print("TR><TD>");
    print("<INPUT TYPE=TEXT NAME=theDose SIZE=80>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=therefreq SIZE=1>");
    print("</TD></TR><TR><TD>");
    print("<P><A HREF=javascript:void(0)\n");
print('onClick="open(\'scripadmin.php?tab=426\'');
print(",'miniwin','toolbar=0,menubar=0,scrollbars=1,");
print("width=900,height=400')\">");
    print("Dose codes look up table</A></P>\n"); 
    print("</TD></TR><TR><TD colspan=2 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}   /* end of function add dose model answer */

function R1A_InsertLabelMA($drugid, $theAncil,$theDose, $refreq)
{   $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* update details of the form */  
   $formdata= array(
	"DrugProblemID" => $drugid, "Ancillabel" => $theAncil,
	"Doselabel"=>$theDose, "RefReq"=>$refreq);
    Z_DrawHeader();
    $db->insert("ScripDoseCodesMA",$formdata);
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Form inserted");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
} /* end of function insert form */

function R1ZA_Display_Label_Table()
{   print("<BODY onblur='window.close();'>");
print("<H2>The label table</H2>");
    print("<TABLE BORDER=1>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripLabelCodes");
    foreach ($table as $row)
    {	$Code=$row['Code']; $Text=$row['Text'];
        print("<TR><TD>$Code</TD>");
	print("<TD>$Text</TD></TR>");
    }
    print("</TABLE>\n");
    print("<P><A HREF=javascript:void(0) \n");
    print('onClick="self.close()">');
    print("Close this window</A></P>"); 
} /* end of function display dose tables */

function R1ZB_Display_Dose_Table()
{   print("<BODY onblur='window.close();'>");
    print("<TABLE BORDER=1>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripDoseCodes");
    foreach ($table as $row)
    {	$Code=$row['Code']; $Text=$row['Text'];
        print("<TR><TD>$Code</TD>");
	print("<TD>$Text</TD></TR>");
    }
    print("</TABLE>\n");
    print("<P><A HREF=javascript:void(0) \n");
    print('onClick="self.close()">');
    print("Close this window</A></P>"); 
} /* end of function display dose tables */

/* Shows all the model PORs and asks administrator to choose one */
function S1_EditPORMA() {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
/* Get details of all the POR MA and display as a table*/  
    print("<tr><td>");
    print("<FONT COLOR=#FFFFFF><H2>Select one answer from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</td></tr>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=445>");

    print("<TR><TD colspan=2 align=center>");
    print("<table border='1'>\n");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=#FFFFFF>CaseID</FONT></TH></tr>");

    print("<tr><td><input type=radio name=theCaseID value='0' checked></td>");
    print("<td><font color=#ffffff>New case</font></td></TR>\n");

    $table=$db->select("ScripPORMA", "", "CaseID");
    foreach ($table as $row) {
     	$theCaseID=$row['CaseID'];
	print("<TR><TD><INPUT TYPE=RADIO NAME=theCaseID VALUE=$theCaseID>\n");
	print("</TD><TD><FONT COLOR=#FFFFFF>$theCaseID</FONT></TD></TR>\n");
    }
    print("<tr><td align=center colspan='2'><input type=submit>");
    print("</td></tr></table>");
    print("<p><a href='scripadmin.php?tab=5'>Return to Menu</a></p>");

}  /* end of function edit POR model answer
*/


function S1A_NewPORDetails($theCaseID) {
    Z_DrawHeader();
    print("</TABLE>\n");

    $db=new dbabstraction();
    $db->connect() or die($db->getError());

    if ($theCaseID != '0') {
        $table=$db->select("ScripPORMA", array("CaseID"=>$theCaseID));
        foreach ($table as $row) {
            $col2 = $row['col2'];
            $col3 = $row['col3'];
            $col4a = $row['col4a'];
            $col4afull = $row['col4aFull'];
            $col4b = $row['col4b'];
            $col5a = $row['col5a'];
            $col5b = $row['col5b'];
            $col6 = $row['col6'];
        };
    } else {
        $col2 = "";
        $col3 = "";
        $col4a = "";
        $col4afull = "";
        $col4b = "";
        $col5a = "";
        $col5b = "";
        $col6 = "";
    };

    print("<form action=scripadmin.php?tab=446 method=post>");
    print("<h2 align=center>POR Model Answers</h2>");

    print("<p>Enter the letter-number for the case: ");
    print("<input TYPE=TEXT SIZE=2 NAME=case");
    if ($theCaseID != '0') { print (" value='$theCaseID'"); };
    print("></p></td>");

    print("<table align='center' border='1'>\n");

    print("<tr>");
    print("<th rowspan='2'>Ref No</th>\n");
    print("<th rowspan='2'>Col 2</th>\n");
    print("<th rowspan='2'>Col 3</th>\n");
    print("<th colspan='2'>Col 4A</th>\n");
    print("<th rowspan='2'>Col 5A</td>\n");
    print("<th rowspan='2'>Col 6</td>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<td align='center'><b><i>Either</i></b></td>\n");
    print("<td align='center'><b><i>Or</i></b></td>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<td>&nbsp;</td>\n");
    print("<td valign='top' rowspan='3'>\n");
    print("<select name='col2'>\n");
    print("<option value='P'");
    if ($col2 == "P" || $col2 == "") { print(" selected"); };
    print(">Patient data\n");
    print("<option value='N'");
    if ($col2 == "N") { print(" selected"); };
    print(">NFSQ\n");
    print("<option value='2'");
    if ($col2 == "2") { print(" selected"); };
    print(">2xNFSQ\n");
    print("</td>\n");

    print("<td valign='top' rowspan='3'>\n");
    print("<select name='col3'>\n");
    print("<option value='p'");
    if ($col3 == "p" || $col3 == "") { print(" selected"); };
    print(">Practitioner data\n");
    print("<option value='N'");
    if ($col3 == "N") { print(" selected"); };
    print(">NFSQ\n");
    print("<option value='2'");
    if ($col3 == "2") { print(" selected"); };
    print(">2xNFSQ\n");
    print("<option value='R'");
    if ($col3 == "R") { print(" selected"); };
    print(">Requester data\n");
    print("</td>\n");

    /* Note that 4A1 is text only */
    print("<td>");
    print("<input type=text name=col4a1");
    if ($col4a != "N" && $col4a != "2") { print(" value='$col4a'"); };
    print(">");
    print("</td>\n");

    print("<td valign='top'>\n");
    print("<select name='col4a2'>\n");
    print("<option value='0'");
    if ($col4a == '0' || $col4a == "") { print(" selected"); };
    print(">(Null)\n");
    print("<option value='N'");
    if ($col4a == 'N') { print(" selected"); };
    print(">NFSQ\n");
    print("<option value='2'");
    if ($col4a == '2') { print(" selected"); };
    print(">2xNFSQ\n");
    print("</td>");

    print("<td align='center' valign='top'>\n");
    print("<select name='col5a'>\n");
    print("<option value='W'");
    if ($col5a == "W" || $col5a == "") { print(" selected"); };
    print(">Price\n");
    print("<option value='D'");
    if ($col5a == "D") { print(" selected"); };
    print(">Date on script\n");
    print("</td>\n");

    print("<td valign='top' rowspan='3'>\n");
    print("<select name='col6'>\n");
    print("<option value='W'");
    if ($col6 == "W") { print(" selected"); };
    print(">Price\n");
    print("<option value='0'");
    if ($col6 == "0" || $col6 == "") { print(" selected"); };
    print(">(Null)\n");
    print("</td>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<th rowspan='2'>Date of<br>dispensing</th>\n");
    print("<th colspan='2'>Col 4B</th>\n");
    print("<th>Col 5B</th>\n");
    print("</tr>\n");

    print("<tr align='center'>\n");
    print("<td colspan='2'>\n");
    print("<select name='col4b'>\n");
    print("<option value='D'");
    if ($col4b == "D") { print(" selected"); };
    print(">Date on script\n");
    print("<option value='0'");
    if ($col4b == "0" || $col4b == "") { print(" selected"); };
    print(">(Null)\n");
    print("</td>\n");

    print("<td>\n");
    print("<select name='col5b'>\n");
    print("<option value='d'");
    if ($col5b == "d") { print(" selected"); };
    print(">Date script received\n");
    print("<option value='0'");
    if ($col5b == "0" || $col5b == "") { print(" selected"); };
    print(">(Null)\n");
    print("</td>\n");
    print("</tr>\n");

    print("<tr><td colspan='7'>&nbsp;<td></tr>");

    print("<tr>");
    print("<td colspan='2'><td>");
    print("<th colspan='2'>Col 4A Full<th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'><td>");
    print("<th colspan='2'>");
    print("<textarea name='col4afull' rows='5'>");
    print("$col4afull");
    print("</textarea>");
    print("<th>");
    print("</tr>");


    print("<tr><td colspan='7'>&nbsp;<td></tr>");

    print("<tr><td align='center' colspan='7'>\n");
    print("<input type=submit value='Add to table'>\n");
    print("</td></tr>\n");
    print ("</table>\n");

    print("<p>The data for most these fields is already on file. ");
    print("This table just directs the program where to look.<BR>");
    print("Use the drop dowm menus to choose which data should ");
    print("appear in the particular column according to the ");
    print("type of prescription.</p>");
    print("<p>Note 1: For col 4a you need to either type the ");
    print(" model answer or chose an answer - not both.<br>");
    print("Note 2: If you do type a model answer, you can (and ");
    print("probably should!) enter a full version of your answer ");
    print("in Col 4A Full.<br>");
    print("Note 3: NFSQ means Name, form, strength and quantity.</p>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
};


/*
function S1A_EditSinglePORMA($theCaseID) {
    Z_DrawHeader();print("</TABLE>");
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
// get details of all the POR MA and display as a table 
    $table=$db->select("ScripPORMA", array("CaseID"=>$theCaseID));
    foreach ($table as $row)
    {	$col2=$row['col2'];
    	$col3=$row['col3'];
    	$col4a=$row['col4a'];
     	$col4b=$row['col4b'];
     	$col5a=$row['col5a'];
     	$col5b=$row['col5b'];
     	$col6=$row['col6'];
     }
    print("<FORM ACTION=scripadmin.php?tab=442 METHOD=POST>");
    print("<H2 ALIGN=CENTER>POR Model Answers</H2>");
    print("<P>Enter the letter-number for the case: ");
    print("<INPUT TYPE=TEXT SIZE=2 NAME=case VALUE='$theCaseID'></P>");
    print("</TD><TR><TABLE BORDER=1 ALIGN=CENTER><TR>\n");
    print("<TH>Ref No.</TH>");
    print("<TH>Col 2</TH>");
    print("<TH>Col 3</TH>");
    print("<TH>Col 4a. Either:<BR>Emergency nature<BR>");
    print("or Purpose</TH>");
    print("<TH>Col 5a</TH>");
    print("<TH>Col 6</TH>");
    print("</TR><TR>\n");
    print("<TD>&nbsp;</TD>");   
    print("<TD VALIGN=TOP ROWSPAN=3><SELECT NAME=col2>");
    print("<OPTION VALUE='P'");
    if ($col2=="P") print(" SELECTED ");
    print(">Patient data");
    print("<OPTION VALUE='N'");
    if ($col2=="N") print(" SELECTED ");
    print(">Name,qty,strength &amp; form");
    print("</TD>");   
    print("<TD VALIGN=TOP ROWSPAN=3><SELECT NAME=col3>");
    print("<OPTION VALUE='p'");
    if ($col3=="P") print(" SELECTED ");
    print(">Practitioner data");
    print("<OPTION VALUE='N'");
    if ($col3=="N") print(" SELECTED ");
    print(">Name,qty,strength &amp; form");
    print("<OPTION VALUE='R'");
    if ($col3=="R") print(" SELECTED ");
    print(">Requester data");
    print("</TD>");   
    print("<TD><INPUT TYPE=TEXT NAME=col4a ");
    print("SIZE=30 MAXSIZE=100 VALUE='$col4a'>");
    print("</TD>");   
    print("<TD><SELECT NAME=col5a>");
    print("<OPTION VALUE='W'");
    if ($col5a=="P") print(" SELECTED ");
    print(">Price");
    print("<OPTION VALUE='D'");
    if ($col5a=="D") print(" SELECTED ");
    print(">Date on Prescription");
    print("</TD>");   
    print("<TD ROWSPAN=3  VALIGN=TOP><SELECT NAME=col6>");
    print("<OPTION VALUE='W'");
    if ($col6=="P") print(" SELECTED ");
    print(">Price");
    print("<OPTION VALUE='0'");
    if ($col6=="N") print(" SELECTED ");
    print(">No entry");
    print("</TD>");   
    print("</TR><TR>\n");
    print("<TH ROWSPAN=2>Date of<BR>Dispensing</TH>");
    print("<TH>Col 4b</TH>");
    print("<TH>Col 5b</TH>");
    print("</TR><TR>\n");
    print("<TD><SELECT NAME=col4b>");
    print("<OPTION VALUE='0'");
    if ($col4b=="N") print(" SELECTED ");
    print(">No Entry");
    print("<OPTION VALUE='D'");
    if ($col4b=="D") print(" SELECTED ");
    print(">Date on Prescription");
    print("</TD>");   
    print("<TD><SELECT NAME=col5b>");
    print("<OPTION VALUE='0'");
    if ($col5b=="N") print(" SELECTED ");
    print(">No Entry");
    print("<OPTION VALUE='d'");
    if ($col5b=="D") print(" SELECTED ");
    print(">Date Prescription recd");
    print("</TD></TR><TD COLSPAN=6 ALIGN=CENTER>");
    print("<INPUT TYPE=SUBMIT VALUE='Update table'>");
    print("</TD></TR></TABLE>\n");
    print("<P>The data for most these fields is already on file. ");
    print("This table just directs the program where to look.<BR>");
    print("Use the drop dowm menus to choose which data should ");
    print("appear in the particular column according to the ");
    print("type of prescription.</P>");
    print("<P>For col 4a you need to type the MA.</P>");  
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");

} */

function S1B_ImplementNewPOR($col2, $col3, $col4a, $col4afull,
           $col4b, $col5a, $col5b, $col6, $thecase) {
    global $case;
    Z_DrawHeader();

    $db = new dbabstraction();
    $db->connect() or die ($db->getError());

    $table=$db->select("ScripPORMA", array("CaseID"=>$thecase));
    foreach ($table as $row) {
        $testExistence = $row['CaseID'];
    };

    if ($thecase == $testExistence) { // Use UPDATE
        $text = array ("col2"=>$col2, "col3"=>$col3,
          "col4a"=>$col4a, "col4aFull"=>$col4afull,
          "col4b"=>$col4b, "col5a"=>$col5a,
          "col5b"=>$col5b, "col6"=>$col6);
        $db->update ("ScripPORMA", $text, array("CaseID"=>$thecase));
    } else { // Use INSERT
        $text = array ("CaseID"=>$thecase, "col2"=>$col2, "col3"=>$col3,
          "col4a"=>$col4a, "col4aFull"=>$col4afull,
          "col4b"=>$col4b, "col5a"=>$col5a,
          "col5b"=>$col5b, "col6"=>$col6);
        $db->insert("ScripPORMA", $text);
    };

    print("<tr><td bgcolor=#ffffff>");
    print("<p>Data added to database</p>");
    print("<p><a href='scripadmin.php?tab=5'>Return to menu</a></p>");
    print("</td></tr></table>");
} 

/*
function S1B_UpdatePORMA($col2,$col3,$col4a,$col4b, $col5a,
	$col5b,$col6, $thecase) 
{   global $case; Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
    $text=array( "col2"=>$col2, "col3"=>$col3, "col4a"=>$col4a,
	"col4b"=>$col4b, "col5a"=>$col5a, "col5b"=>$col5b, "col6"=>$col6);
    $db->update("ScripPORMA",$text, array("CaseID"=>$thecase));
    print("<TR><TD BGCOLOR=#FFFFFF>");
    print("Data updated in POR Model Answer table</P>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR>");
} */
/* end of function update POR MA */










/* Edit the synonyms table for addresses */
function S2_EditSynAdd() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

/* Get details of all the synonyms and display as a table*/  
    print("<tr><td>");
    print("<FONT COLOR=#FFFFFF><p>Select one answer from the list");
    print(" and click the submit button to edit the details.</p>");
    print("<p>If B is a synonym for A, do not make A a synonym for B!</p>");
    print("</FONT>");
    print("</td></tr>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=632>");

    print("<TR><TD colspan=2 align=center>");
    print("<table border='1'>\n");
    print("<TR>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=#FFFFFF>Word</FONT></TH>");
    print("<TH><FONT COLOR=#FFFFFF>Synonym</FONT></TH>");
    print("</tr>");

    print("<tr><td><input type=radio name=theAddressSynonymID value='0' checked></td>");
    print("<td><font color=#ffffff>New case</font></td></TR>\n");

    $table=$db->select("ScripSynonyms", "", "MainWord");
    foreach ($table as $row) {
     	$theAddressSynonymID=$row['SynID'];
     	$theMainWord=$row['MainWord'];
     	$theSynonyms=$row['Synonym'];
	print("<TR>");
        print("<TD><INPUT TYPE=RADIO NAME=theAddressSynonymID
          VALUE=$theAddressSynonymID></td>");
        print("<TD><FONT COLOR=#FFFFFF>$theMainWord</FONT></TD>");
        print("<TD><FONT COLOR=#FFFFFF>$theSynonyms</FONT></TD>");
        print("</TR>\n");
    }
    print("<tr><td align=center colspan='3'><input type=submit>");
    print("</td></tr></table>");
    print("<p><a href='scripadmin.php?tab=5'>Return to Menu</a></p>");

};


function S2A_NewSynAddDetails($theAddressSynonymID) {
    Z_DrawHeader();
    print("</table>\n");

    $db=new dbabstraction();
    $db->connect() or die($db->getError());

    print("<INPUT TYPE=HIDDEN NAME=theAddressSynonymID value=$theAddressSynonymID>");
    if ($theAddressSynonymID != '0') {
        $condition=array("SynID"=>$theAddressSynonymID);
        $table=$db->select("ScripSynonyms", $condition);
        foreach ($table as $row) {
            $theAddressSynonymID=$row['SynID'];
            $theMainWord=$row['MainWord'];
            $theSynonyms=$row['Synonym'];
        }
    } else {
        $theMainWord="";
        $theSynonyms="";
    };

    print("<form action=scripadmin.php?tab=634 method=post>");
    print("<h2 align=center>Synonyms</h2>");

    print("<table align='center' border='1'>\n");

    print("<tr>");
    print("<th>ID</th>\n");
    print("<th>Word(s)</th>\n");
    print("<th>Synonyms</th>\n");
    print("</tr>\n");

    print("<tr>");
    print("<td><input readonly name=theAddressSynonymID
      value='$theAddressSynonymID' size='10'></td>");
    print("<td><input type=text name=theMainWord size='50'
      value='$theMainWord'></td>\n");
    print("<td><input type=text name=theSynonyms size='50'
      value='$theSynonyms'></td>\n");
    print("</tr>\n");
    print("<tr>");
    print("<td align='center' colspan='3'>");
    print("<input type=submit value='Update table'></td>");
    print("</tr>");
    print("</table>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</A></P>");
}

function S2B_ImplementNewSynAdd($theAddressSynonymID, $theMainWord,
                           $theSynonyms) {
    global $case;
    Z_DrawHeader();

    $db = new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition=array("SynID"=>$theAddressSynonymID);

    $text = array ("MainWord"=>$theMainWord,
      "Synonym"=>$theSynonyms);

    if ($theAddressSynonymID != '0') { // Use UPDATE
        $db->update ("ScripSynonyms", $text, $condition);
    } else { // Use INSERT
        $db->insert("ScripSynonyms", $text);
    };

    print("<tr><td bgcolor=#ffffff>");
    print("<p>Data added to database</p>");
    print("<p><a href='scripadmin.php?tab=5'>Return to menu</a></p>");
    print("</td></tr></table>");
} 

/* Edit the synonyms table for pharmacy details */
function S2C_EditSynPharm() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

/* Get details of all the synonyms and display as a table*/  
    print("<tr><td>");
    print("<FONT COLOR=#FFFFFF><p>Select one answer from the list");
    print(" and click the submit button to edit the details.</p>");
    print("<p>If B is a synonym for A, do not make A a synonym for B!</p>");
    print("</FONT>");
    print("</td></tr>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=642>");

    print("<TR><TD colspan=2 align=center>");
    print("<table border='1'>\n");
    print("<TR>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=#FFFFFF>Word(s)</FONT></TH>");
    print("<TH><FONT COLOR=#FFFFFF>Synonyms</FONT></TH>");
    print("</tr>");

    print("<tr><td><input type=radio name=thePharmacySynonymID value='0'
                      checked></td>");
    print("<td><font color=#ffffff>New case</font></td></TR>\n");

    $table=$db->select("ScripSynonymsPharmacy", "", "MainWord");
    foreach ($table as $row) {
     	$thePharmacySynonymID=$row['PharmacySynonymID'];
     	$theMainWord=$row['MainWord'];
     	$theSynonyms=$row['Synonyms'];
	print("<TR>");
        print("<TD><INPUT TYPE=RADIO NAME=thePharmacySynonymID
          VALUE=$thePharmacySynonymID></td>");
        print("<TD><FONT COLOR=#FFFFFF>$theMainWord</FONT></TD>");
        print("<TD><FONT COLOR=#FFFFFF>$theSynonyms</FONT></TD>");
        print("</TR>\n");
    }
    print("<tr><td align=center colspan='3'><input type=submit>");
    print("</td></tr></table>");
    print("<p><a href='scripadmin.php?tab=5'>Return to Menu</a></p>");

};


function S2D_NewSynPharmDetails($thePharmacySynonymID) {
    Z_DrawHeader();
    print("</table>\n");

    $db=new dbabstraction();
    $db->connect() or die($db->getError());

    print("<INPUT TYPE=HIDDEN NAME=thePharmacySynonymID value=$thePharmacySynonymID>");

    if ($thePharmacySynonymID != '0') {
        $table=$db->select("ScripSynonymsPharmacy", "", "MainWord");
        foreach ($table as $row) {
            $thePharmacySynonymID=$row['PharmacySynonymID'];
            $theMainWord=$row['MainWord'];
            $theSynonyms=$row['Synonyms'];
        }
    } else {
        $theMainWord="";
        $theSynonyms="";
    };

    print("<form action=scripadmin.php?tab=644 method=post>");
    print("<h2 align=center>Pharmacy Synonyms</h2>");

    print("<table align='center' border='1'>\n");

    print("<tr>");
    print("<th>ID</th>\n");
    print("<th>Word(s)</th>\n");
    print("<th>Synonyms</th>\n");
    print("</tr>\n");

    print("<tr>");
    print("<td><input readonly name=thePharmacySynonymID
      value='$thePharmacySynonymID' size='10'></td>");
    print("<td><input type=text name=theMainWord size='50'
      value='$theMainWord'></td>\n");
    print("<td><input type=text name=theSynonyms size='50'
      value='$theSynonyms'></td>\n");
    print("</tr>\n");
    print("<tr>");
    print("<td align='center' colspan='3'>");
    print("<input type=submit value='Update table'></td>");
    print("</tr>");
    print("</table>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</A></P>");
}

function S2E_ImplementNewSynPharm($thePharmacySynonymID, $theMainWord,
                           $theSynonyms) {
    global $case;
    Z_DrawHeader();

    $db = new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition=array("PharmacySynonymID"=>$thePharmacySynonymID);

    $text = array ("MainWord"=>$theMainWord,
      "Synonyms"=>$theSynonyms);

    if ($thePharmacySynonymID != '0') { // Use UPDATE
        $db->update ("ScripSynonymsPharmacy", $text, $condition);
    } else { // Use INSERT
        $db->insert("ScripSynonymsPharmacy", $text);
    };

    print("<tr><td bgcolor=#ffffff>");
    print("<p>Data added to database</p>");
    print("<p><a href='scripadmin.php?tab=5'>Return to menu</a></p>");
    print("</td></tr></table>");
} 


/* Edit the synonyms table for register details */
function S2F_EditSynReg() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

/* Get details of all the synonyms and display as a table*/  
    print("<tr><td>");
    print("<FONT COLOR=#FFFFFF><p>Select one answer from the list");
    print(" and click the submit button to edit the details.</p>");
    print("<p>If B is a synonym for A, do not make A a synonym for B!</p>");
    print("</FONT>");
    print("</td></tr>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=652>");

    print("<TR><TD colspan=2 align=center>");
    print("<table border='1'>\n");
    print("<TR>");
    print("<TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=#FFFFFF>Word(s)</FONT></TH>");
    print("<TH><FONT COLOR=#FFFFFF>Synonyms</FONT></TH>");
    print("</tr>");

    print("<tr><td><input type=radio name=theRegisterSynonymID value='0'
                      checked></td>");
    print("<td><font color=#ffffff>New case</font></td></TR>\n");

    $table=$db->select("ScripSynonymsRegister", "", "MainWord");
    foreach ($table as $row) {
     	$theRegisterSynonymID=$row['RegisterSynonymID'];
     	$theMainWord=$row['MainWord'];
     	$theSynonyms=$row['Synonyms'];
	print("<TR>");
        print("<TD><INPUT TYPE=RADIO NAME=theRegisterSynonymID
          VALUE=$theRegisterSynonymID></td>");
        print("<TD><FONT COLOR=#FFFFFF>$theMainWord</FONT></TD>");
        print("<TD><FONT COLOR=#FFFFFF>$theSynonyms</FONT></TD>");
        print("</TR>\n");
    }
    print("<tr><td align=center colspan='3'><input type=submit>");
    print("</td></tr></table>");
    print("<p><a href='scripadmin.php?tab=5'>Return to Menu</a></p>");
};

function S2G_NewSynRegDetails($theRegisterSynonymID) {
    Z_DrawHeader();
    print("</table>\n");

    $db=new dbabstraction();
    $db->connect() or die($db->getError());

    print("<INPUT TYPE=HIDDEN NAME=theRegisterSynonymID value=$theRegisterSynonymID>");

    if ($theRegisterSynonymID != '0') {
        $table=$db->select("ScripSynonymsRegister", "", "MainWord");
        foreach ($table as $row) {
            $theRegisterSynonymID=$row['RegisterSynonymID'];
            $theMainWord=$row['MainWord'];
            $theSynonyms=$row['Synonyms'];
        }
    } else {
        $theMainWord="";
        $theSynonyms="";
    };

    print("<form action=scripadmin.php?tab=654 method=post>");
    print("<h2 align=center>Register Synonyms</h2>");

    print("<table align='center' border='1'>\n");

    print("<tr>");
    print("<th>ID</th>\n");
    print("<th>Word(s)</th>\n");
    print("<th>Synonyms</th>\n");
    print("</tr>\n");

    print("<tr>");
    print("<td><input readonly name=theRegisterSynonymID
      value='$theRegisterSynonymID' size='10'></td>");
    print("<td><input type=text name=theMainWord size='50'
      value='$theMainWord'></td>\n");
    print("<td><input type=text name=theSynonyms size='50'
      value='$theSynonyms'></td>\n");
    print("</tr>\n");
    print("<tr>");
    print("<td align='center' colspan='3'>");
    print("<input type=submit value='Update table'></td>");
    print("</tr>");
    print("</table>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</A></P>");
}

function S2H_ImplementNewSynReg($theRegisterSynonymID, $theMainWord,
                           $theSynonyms) {
    global $case;
    Z_DrawHeader();

    $db = new dbabstraction();
    $db->connect() or die ($db->getError());
    $condition=array("RegisterSynonymID"=>$theRegisterSynonymID);

    $text = array ("MainWord"=>$theMainWord,
      "Synonyms"=>$theSynonyms);

    if ($theRegisterSynonymID != '0') { // Use UPDATE
        $db->update ("ScripSynonymsRegister", $text, $condition);
    } else { // Use INSERT
        $db->insert("ScripSynonymsRegister", $text);
    };

    print("<tr><td bgcolor=#ffffff>");
    print("<p>Data added to database</p>");
    print("<p><a href='scripadmin.php?tab=5'>Return to menu</a></p>");
    print("</td></tr></table>");
} 



/*
function T1_AddPORMA()
{   Z_DrawHeader();print("</TABLE>");
    print("<FORM ACTION=scripadmin.php?tab=451 METHOD=POST>");
    print("<H2 ALIGN=CENTER>POR Model Answers</H2>");
    print("<P>Enter the letter-number for the case: ");
    print("<INPUT TYPE=TEXT SIZE=2 NAME=case></P>");
    print("</TD>");

// Beginning of old code
    print("<TR><TABLE BORDER=1 ALIGN=CENTER><TR>\n");
    print("<TH>Ref No.</TH>");
    print("<TH>Col 2</TH>");
    print("<TH>Col 3</TH>");
    print("<TH>Col 4a. Either:<BR>Emergency nature<BR>");
    print("or Purpose</TH>");
    print("<TH>Col 5a</TH>");
    print("<TH>Col 6</TH>");
    print("</TR><TR>\n");
    print("<TD>&nbsp;</TD>");   
    print("<TD VALIGN=TOP ROWSPAN=3><SELECT NAME=col2>");
    print("<OPTION VALUE='P'>Patient data");
    print("<OPTION VALUE='N'>Name,qty,strength &amp; form");
    print("</TD>");   
    print("<TD VALIGN=TOP ROWSPAN=3><SELECT NAME=col3>");
    print("<OPTION VALUE='P'>Practitioner data");
    print("<OPTION VALUE='N'>Name,qty,strength &amp; form");
    print("<OPTION VALUE='R'>Requester data");
    print("</TD>");   
    print("<TD><INPUT TYPE=TEXT NAME=col4a ");
    print("SIZE=30 MAXSIZE=100");
    print("</TD>");   
    print("<TD><SELECT NAME=col5a>");
    print("<OPTION VALUE='P'>Price");
    print("<OPTION VALUE='D'>Date on Prescription");
    print("</TD>");   
    print("<TD ROWSPAN=3  VALIGN=TOP><SELECT NAME=col6>");
    print("<OPTION VALUE='P'>Price");
    print("<OPTION VALUE='N'>No entry");
    print("</TD>");   
    print("</TR><TR>\n");
    print("<TH ROWSPAN=2>Date of<BR>Dispensing</TH>");
    print("<TH>Col 4b</TH>");
    print("<TH>Col 5b</TH>");
    print("</TR><TR>\n");
    print("<TD><SELECT NAME=col4b>");
    print("<OPTION VALUE='N'>No Entry");
    print("<OPTION VALUE='D'>Date on Prescription");
    print("</TD>");   
    print("<TD><SELECT NAME=col5b>");
    print("<OPTION VALUE='N'>No Entry");
    print("<OPTION VALUE='D'>Date Prescription recd");
    print("</TD></TR><TD COLSPAN=6 ALIGN=CENTER>");
    print("<INPUT TYPE=SUBMIT VALUE='Add to table'>");
    print("</TD></TR></TABLE>\n");

// End of old code

    print("<table align='center' border='1'>\n");

    print("<tr>");
    print("<th rowspan='2'>Ref No</th>\n");
    print("<th rowspan='2'>Col 2</th>\n");
    print("<th rowspan='2'>Col 3</th>\n");
    print("<th colspan='2'>Col 4A</th>\n");
    print("<th rowspan='2'>Col 5A</td>\n");
    print("<th rowspan='2'>Col 6</td>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<td align='center'><b><i>Either</i></b></td>\n");
    print("<td align='center'><b><i>Or</i></b></td>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<td>&nbsp;</td>\n");
    print("<td valign='top' rowspan='3'>\n");
    print("<select name='col2'>\n");
    print("<option value='P'>Patient data\n");
    print("<option value='N'>NFSQ\n");
    print("</td>\n");

    print("<td valign='top' rowspan='3'>\n");
    print("<select name='col3'>\n");
    print("<option value='p'>Practitioner data\n");
    print("<option value='N'>NFSQ\n");
    print("<option value='R'>Requester data\n");
    print("</td>\n");

    // Note that 4A1 is text only 
    print("<td><input type=text name=col4a1></td>\n");

    print("<td valign='top'>\n");
    print("<select name='col4a2'>\n");
    print("<option value='0'>(Null)\n");
    print("<option value='N'>NFSQ\n");
    print("</td>");

    print("<td align='center' valign='top'>\n");
    print("<select name='col5a'>\n");
    print("<option value='W'>Price\n");
    print("<option value='D'>Date on script\n");
    print("</td>\n");

    print("<td valign='top' rowspan='3'>\n");
    print("<select name='col6'>\n");
    print("<option value='W'>Price\n");
    print("<option value='0'>(Null)\n");
    print("</td>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<th rowspan='2'>Date of<br>dispensing</th>\n");
    print("<th colspan='2'>Col 4B</th>\n");
    print("<th>Col 5B</th>\n");
    print("</tr>\n");

    print("<tr align='center'>\n");
    print("<td colspan='2'>\n");
    print("<select name='col4b'>\n");
    print("<option value='D'>Date on script\n");
    print("<option value='0'>(Null)\n");
    print("</td>\n");

    print("<td>\n");
    print("<select name='col5b'>\n");
    print("<option value='d'>Date script received\n");
    print("<option value='0'>(Null)\n");
    print("</td>\n");
    print("</tr>\n");

    print("<tr><td align='center' colspan='7'>\n");
    print("<input type=submit value='Add to table'>\n");
    print("</td></tr>\n");
    print ("</table>\n");

    print("<P>The data for most these fields is already on file. ");
    print("This table just directs the program where to look.<BR>");
    print("Use the drop dowm menus to choose which data should ");
    print("appear in the particular column according to the ");
    print("type of prescription.</P>");
    print("<P>Note 1: For col 4a you need to either type the ");
    print(" model answer or chose an answer - not both.<br>");
    print("Note 2: NFSQ means Name, form, strength and quantity.<p>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
 } */

/*
function T1A_InsertPORMA($col2,$col3,$col4a,$col4b, $col5a, $col5b,$col6, $thecase)
{   global $case; Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
    $text=array( "col2"=>$col2, "col3"=>$col3, "col4a"=>$col4a,
	"col4b"=>$col4b, "col5a"=>$col5a, "col5b"=>$col5b, "col6"=>$col6,
	"CaseID"=>$thecase);
    $db->insert("ScripPORMA",$text);
    print("<TR><TD BGCOLOR=#FFFFFF>");
    print("Data added to POR Model Answer table</P>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR>");
} */
 /* end of function insert POR MA */

function U1_EditIP() {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

    // Get details of IP Addresses and display as a table  
    $table=$db->select("ScripIPAddresses");
    print("<TR><TD colspan=2>");
    print("<font color='#ffffff'><h2>Select one address from the list and click the submit button to edit the details</h2></font>");
    print("<font color='#FFFFFF'>Note: The contents of the \"At\" field are determined from the IP address. ");
    print("If you enter a new type of IP address, enter its beginning and the associated location in the table ScripIPLocation.</font>");
    print("</TD></TR>\n");

    print("<TR><TD colspan=2 align=center>");
    print("<TABLE BORDER=1>\n");
    print("<FORM ACTION=scripadmin.php METHOD='POST'>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=461>\n");

    print("<tr>");
    print("<th>&nbsp;</th>");
    print("<th><font color='#ffffff'>Location</font></th>");
    print("<th><font color='#ffffff'>At</font></th>");
    print("<th><font color='#ffffff'>IP Address</font></th>");
    print("<th><font color='#ffffff'>Checking</font></th>");
    print("</tr>\n");
    foreach ($table as $row) {
     	 $IPID=$row['IPID'];
	 $location=$row['Location'];
	 $ipaddress=$row['IPAddress'];

        // Find this type of IP address in ScripIPLocation: read the whole table ...
        $tableIP=$db->select("ScripIPLocation");
        foreach ($tableIP as $rowIP){
            $ipbeginning = $rowIP['IPBeginning'];
            
            $iplocation = "";
            // Find where $ipbeginning occurs in $location ... 
            $position = strpos($ipaddress, $ipbeginning);
            // We want instances where it occurs (i.e. is not equivalent to false) at hte beginning (= 0) ...
            if ($position == 0 && $position !== false){
                $iplocation = $rowIP['Location'];
            };
        }


	$checking=$row['Checking'];
	print("<tr>");
       print("<TD><INPUT TYPE=RADIO NAME=IPID VALUE=$IPID></TD>\n");
	print("<TD><FONT COLOR=#FFFFFF>$location</FONT></TD>\n");
       print("<td><font color='#ffffff'>$iplocation</font></td>");
	print("<TD><FONT COLOR=#FFFFFF>$ipaddress</FONT></TD>\n");
	print("<TD><FONT COLOR=#FFFFFF>$checking</FONT></TD>\n");
	print("</TR>\n");
    };
    print("<TR><TD ALIGN=CENTER colspan=3><INPUT TYPE=SUBMIT></TD></TR>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function U1A_EditSingleIP($IPID) {
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 

    // Get details of IP Addresses and display 
    $condition=array("IPID"=>$IPID);
    $table=$db->select("ScripIPAddresses", $condition);
    foreach ($table as $row) {
     	$location=$row['Location'];
	$ipaddress=$row['IPAddress'];
	$checking=$row['Checking'];
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=462>");
    print("<INPUT TYPE=HIDDEN NAME=IPID value=$IPID>");
    print("<TR><TH><FONT COLOR=FFFFFF>Location</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>IP Address</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Checking</FONT></TH>");
    print("</TR>");

    print("<TR><TD>");
    print("<INPUT TYPE=TEXT NAME=location SIZE=30 ");
    print("VALUE='$location'>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=ipaddress SIZE=15 ");
    print("VALUE=$ipaddress>");
    print("</TD>");
    print("<td>");
    print("<select name='checking'>");
    print("<option value=''>Select from ...");
    print("<option value='0'");
    if ($checking == '0') { print(" selected"); };
    print(">No");
    print("<option value='1'");
    if ($checking == '1') { print(" selected"); };
    print(">Yes");
    print("</td>");
    print("</TR>");
    print("</TR><TR><TD colspan=5 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Add to table></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function U1B_UpdateIP($IPID,$location, $ipaddress, $checking) {
    global $case; Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
    $text=array( "Location"=>$location,
                 "IPAddress"=>$ipaddress,
                 "Checking"=>$checking
                        );
    $condition=array("IPID"=>$IPID);
    $db->update("ScripIPAddresses",$text, $condition);
    print("<TR><TD BGCOLOR=#FFFFFF>");
    print("IP Address table updated</P>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR>");
};

function V1_AddIP()
{   Z_DrawHeader();
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=471>");
    print("<TR><TH><FONT COLOR=FFFFFF>Location</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>IP Address</FONT></TH>");
    print("</TR><TR><TD>");
    print("<INPUT TYPE=TEXT NAME=location SIZE=30>");
    print("</TD><TD>");
    print("<INPUT TYPE=TEXT NAME=ipaddress SIZE=15>");
    print("</TD></TR>");
    print("</TR><TR><TD colspan=5 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Add to table></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
} /* end of function add IP address */

function V1A_InsertIP($location, $ipaddress)
{   global $case; Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect()or die ($db->getError()); 
    $text=array( "Location"=>$location, "IPAddress"=>$ipaddress);
    $db->insert("ScripIPAddresses",$text);
    print("<TR><TD BGCOLOR=#FFFFFF>");
    print("Data added to IP Address table</P>");
    print("<P><A HREF=scripadmin.php?tab=5>Return to Menu</A></P>");
    print("</TD></TR>");
}  /* end of function insert ip address */

function W1_SelectCDToUpdate() {
// Choose an existing model answer or specify that a new one be created
    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff><h2>Select one answer from the list");
    print(" and click the submit button to edit the details</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>\n");
    print("<form action=scripadmin.php method=post>\n");
    print("<input type=hidden name=tab value=481>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color=#ffffff>Case ID</font></th></tr>\n");
    print("<tr><td><input type=radio name=theCaseID value='0'
             checked></td>");
    print("<td><font color=#ffffff>New Case</font></td></tr>\n");

    $db = new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripCDRegisterMA", "", "CaseID");
    foreach ($table as $row) {
        $theCaseID = $row['CaseID'];
        print ("<tr>");
        print("<td><input type=radio name=theCaseID value=$theCaseID></td>"); 
        print("<td><font color=#ffffff>$theCaseID</font></td>\n"); 
        print("</tr>");
    }
    print("<tr><td align=center colspan='2'><input type=submit></td></tr>");
    print ("</table>");
    print ("<p><a href=scripadmin.php?tab=5>Return to Menu</p>");
}

function W1A_NewCDDetails($theCaseID) {
// Obtain new details for model answer
    Z_DrawHeader();
    print ("</table>\n");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    if ($theCaseID != '0') { // Edit an exisiting CD register MA ...
        $table=$db->select("ScripCDRegisterMA", array("CaseID"=>$theCaseID));
        foreach ($table as $row) {
            $drug = $row['Drug'];
            $particulars = $row['Particulars'];
            $amount = $row['Amount'];
            $form = $row['Form'];
            $reverseitem = $row['Item'];
            $reverseqty = $row['Qty'];
        }
    } else { // Add a new CD register MA ...
        $drug = "";
        $particulars = "";
        $amount = "";
        $form = "";
    };

    print("<form action=scripadmin.php?tab=482 method=post>\n");
    print("<h2 align='center'>CD Model Answers</h2>\n");
    print("<p>Enter the letter-number for this case: ");
    print("<input type=text size=2 name=case");
    if ($theCaseID != '0') {print (" value='$theCaseID'");}
    print("></p>\n");

    print("<table border='1' align='center'>\n");
    print("<tr>\n");
    print("<td colspan=6>");
    print ("CONTROLLED DRUGS REGISTER (Misuse of Drugs Act) Record of ");
    print("<input text size='100' name=drug value='$drug'>");
    print(" sold or supplied</td>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<th rowspan=2>Date on which transaction was effected</th>\n");
    print("<th>Name</th>\n");
    print("<th>Address</th>\n");
    print("<th rowspan=2>Particulars as to licence of authority of
                 person or firm supplied to be in possession</th>\n");
    print("<th rowspan=2>Amount supplied</th>\n");
    print("<th rowspan=2>Form in which supplied</th>\n");
    print("</tr>\n");

    print("<tr>\n");
    print("<td colspan='2' align='center'>of Person or Firm Supplied</td>");
    print("</tr>\n");

    print("<tr>\n");
    print("<td><i>Date</i></td>");
    print("<td><i>Name</i></td>");
    print("<td><i>Address</i></td>");
    print("<td><textarea name=particulars cols=30
                           rows=5>$particulars</textarea></td>");
    print("<td><textarea name=amount cols=20 rows=5>$amount</textarea></td>");
    print("<td><textarea name=form cols=20 rows=5>$form</textarea></td>");   
    print("</tr>\n");

    // New Part
    print("<tr><th colspan='6'>&nbsp;</th></tr>");
    print("<tr><td colspan='6'>REVERSE OF FP10(MDA) FORM</td></tr>");

    // Special row for reverse of forms - headings
    print("<tr><th colspan='3'>Item</th>");
    print("<th>Quantity</th></tr>");

    // Special row for reverse of forms - details
    print("<tr>");
    print("<td colspan='3'><textarea name=reverseitem cols=50 rows=5>");
    print("$reverseitem</textarea></td>");
    print("<td><textarea name=reverseqty cols=50 rows=5>");
    print("$reverseqty</textarea></td>");
    print("</tr>");

    print("<tr>\n");
    print("<td align='center' colspan='6'><input type=submit
                  value='Write details to database'></td>");
    print("</tr></table>\n");

    print("<p>N.B. Terms in <i>italics</i> will be supplied
                 automatically.</p>"); 
    print ("<p><a href=scripadmin.php?tab=5>Return to Menu</p>");
}

function W1B_ImplementNewCD($theCaseID, $drug, $particulars, $amount, 
            $form, $reverseitem, $reverseqty) {
// Put the new information in the correct table using either update or insert
    global $case;
    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripCDRegisterMA", array("CaseID"=>$theCaseID));
    foreach ($table as $row) {
        $testExistence = $row['CaseID'];
    }

    if ($theCaseID == $testExistence) { // Update existing entry ...
        $text=array("Drug"=>$drug, "Particulars"=>$particulars,
             "Amount"=>$amount, "Form"=>$form,
             "Item"=>$reverseitem, "Qty"=>$reverseqty);
        $db->update("ScripCDRegisterMA", $text, array ("CaseID"=>$theCaseID));
    } else { // Insert new entry ...
        $text=array("CaseID"=>$theCaseID, "Drug"=>$drug,
             "Particulars"=>$particulars, "Amount"=>$amount, "Form"=>$form,
             "Item"=>$reverseitem, "Qty"=>$reverseqty);
        $db->insert("ScripCDRegisterMA", $text);
    };

    print ("<tr><td bgcolor=#ffffff>");
    print ("<p>Data added to database</p>");
    print ("<p><a href='scripadmin.php?tab=5'>Return to menu</a></p>");
    print ("</td></tr></table>");}



function W4_EditCDRegisterMA() {
// This is the NEW CD register (i.e. August 2006)
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the table and display
    print("<tr><td colspan='2'>");
    print("<FONT COLOR=FFFFFF><H2>Select one CD model answer from the list");
    print(" and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR><TD colspan=2 align=center>");
    print("<TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=561>");
    print("<TR><TH>&nbsp;</TH>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></th>");
    print("<TH><FONT COLOR=FFFFFF>Case ID</FONT></th>");
    print("<TH><FONT COLOR=FFFFFF>Collector's Name</FONT></th>");
    print("<TH><FONT COLOR=FFFFFF>Relationship to Patient</FONT></th>");
    print("<TH><FONT COLOR=FFFFFF>Collector's ID</FONT></th>");
    print("<TH><FONT COLOR=FFFFFF>Real ID</FONT></th>");
    print("<TH><FONT COLOR=FFFFFF>Page in CD Register</FONT></th>");

    print("<tr>");
    print("<td><input type='radio' name=ID VALUE='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripCDRegisterMA");
    foreach ($table as $row) {
        $ordering[] = $row['CaseID'];
    };

    // We want to sort $table on CaseID:
    // Produce an array that is CaseID.
    // Now use multisort, sort $table in the same way as $ordering:
    array_multisort($ordering, SORT_ASC, SORT_STRING, $table);

    foreach ($table as $row) {
        $theID=$row['ID'];
        $theCaseID=$row['CaseID'];
        $theAuthority=$row['Authority'];
        $theCollectorName=$row['CollectorName'];
        $theCollectorRelation=$row['CollectorRelation'];
        $theCollectorID=$row['CollectorID'];
        $theRealID = $row['RealID'];
        $thePageID=$row['PageID'];
        print("<tr>");
        print("<td><INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theID&nbsp;</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theCaseID&nbsp;</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theCollectorName&nbsp;</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theCollectorRelation&nbsp;</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theCollectorID&nbsp;</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$theRealID&nbsp;</FONT></TD>");
        print("<TD><FONT COLOR=FFFFFF>$thePageID&nbsp;</FONT></TD>");
        print("</tr>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function W4_EditSingleCDRegisterMA($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the line and display in a form  
    $table=$db->select("ScripCDRegisterMA",array('ID'=>$ID));
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theCaseID=$row['CaseID'];
            $theAuthority=$row['Authority'];  
            $theCollectorName=$row['CollectorName'];
            $theCollectorRelation=$row['CollectorRelation'];
            $theCollectorID=$row['CollectorID'];
            $theRealID=$row['RealID'];
            $thePageID=$row['PageID'];
            $theCollectorIDMA = $row['CollectorIDMA'];
            $theCollectorIDMAFull = $row['CollectorIDMAFull'];
            $theItem=$row['Item'];
            $theQty=$row['Qty'];
        };
    } else {
        $theCaseID="";
        $theAuthority="";  
        $theCollectorName="";
        $theCollectorRelation="";
        $theCollectorID="";
        $theRealID="";
        $thePageID="";
        $theCollectorIDMA="";
        $theCollectorIDMAFull="";
        $theItem="";
        $theQty="";
    };

    print("<tr><td><font color='#ffffff'>");
    print("Note: If the quantity is not on the script, it should ");
    print("be entered in the label model answer.");
    print("</font></td></tr>");

    print("<TR><TD colspan=2 align=center>");
    print("<TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=562>");
    print("<INPUT TYPE=HIDDEN NAME=id value=$ID>");

    print("<tr>");
    print("<th><font COLOR=FFFFFF>Case ID</font></th>");
    print("<th><font COLOR=FFFFFF>Collector's Name</font></th>");
    print("<th><font COLOR=FFFFFF>Relationship to Patient</font></th>");
    print("<th><font COLOR=FFFFFF>Collector's ID</font></th>");
    print("<th><font COLOR=FFFFFF>Real ID</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type='text' name='thecaseid' VALUE='$theCaseID' SIZE=35></TD>");
    print("<td><input type='text' name='thecollectorname' VALUE='$theCollectorName' SIZE=25></TD>");
    print("<td><input type='text' name='thecollectorrelation' VALUE='$theCollectorRelation' SIZE=20></TD>");
    print("<td><input type='text' name='thecollectorid' VALUE='$theCollectorID' SIZE=25></TD>");
    print("<td><input type='text' name='therealid' value='$theRealID'></td>");
    print("</tr>");

    print("<tr><th colspan=5><font COLOR=FFFFFF>");
    print("Page in CD Register</font></th></tr>");

    // The PAGEID drop-downlist ...
    print("<tr>");
    print("<td colspan=4><select name=thepageid>");

    print("<option value='0'");
    if ($thePageID=="0") { print(" selected"); };
    print(">");

    $idtable=$db->select("ScripCDPageTitleAndLabel");
    foreach ($idtable as $row) {
        $ordering[]=$row['Brand'];
    };

    // Now use multisort, sort $tableFormtypesOnly in the same way
    // as $ordering:
    array_multisort($ordering, SORT_ASC, SORT_STRING, $idtable);

    foreach ($idtable as $idrow) {
        $anID=$idrow['ID'];
        $aClass=$idrow['Class'];
        $aBrand=$idrow['Brand'];
        $aStrength=$idrow['Strength'];
        $aForm=$idrow['Form'];
        print("<option value=$anID ");
        if ($anID == $thePageID) { print("selected "); };
        print(">$anID: $aBrand -- $aStrength -- $aForm");
    };
    print("</select>\n");
    print("</td>");

//    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='5'>");
    print("<font color='#ffffff'>Collector ID MA</font>");
    print("</th>");
    print("</tr>");
    print("<tr>");
    print("<td colspan='5'>");
    print("<input type='text' name='thecollectoridma' ");
    print("size='120' value='$theCollectorIDMA'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<th colspan='5'>");
    print("<font color='#ffffff'>Collector ID MA Full</font>");
    print("</th>");
    print("</tr>");
    print("<tr>");
    print("<td colspan='5'>");
    print("<input type='text' name='thecollectoridmafull' ");
    print("size='120' value='$theCollectorIDMAFull'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='5'><font color='#ffffff'>");
    print("For MDA forms only, enter the item. ");
    print("As noted above, the quantity is ");
    print("set in the box marked \"Label - Quantity\" ");
    print("in the model answer for label.");
    print("</font></td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color='#ffffff'>MDA Item");
    print("</font></th>");
//    print("<th><font color='#ffffff'>MDA Quantity");
//    print("</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input type='text' name='item' value='$theItem'>");
    print("</td>");
//    print("<td>");
//    print("<input type='text' name='qty' value='$theQty'>");
//    print("</td>");
    print("</tr>");


    print("<tr><td colspan='5' align=center>");
    print("<input type='submit' value='Update'>");
    print("</td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function W4_UpdateCDRegisterMA($ID, $theCaseID,
               $theCollectorName, $theCollectorRelation,
               $theCollectorID, $theRealID, $thePageID,
               $theCollectorIDMA, $theCollectorIDMAFull,
//               $theItem, $theQty) {
               $theItem) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the CD Register model answer ...
    $CDMAdata= array(
	"CaseID" => $theCaseID,
	"CollectorName" => $theCollectorName,
	"CollectorRelation" => $theCollectorRelation,
	"CollectorID" => $theCollectorID,
	"RealID" => $theRealID,
	"PageID" => $thePageID,
        "CollectorIDMA" => $theCollectorIDMA,
        "CollectorIDMAFull" => $theCollectorIDMAFull,
        "Item" => $theItem);
//        "Qty" => $theQty);


// print_r($CDMAdata);


    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('ID'=>$ID);
        $db->update("ScripCDRegisterMA", $CDMAdata, $condition);
    } else {
        $db->insert("ScripCDRegisterMA", $CDMAdata);
    };

    print("<tr><td colspan=2><font COLOR=FFFFFF>CD Register MA updated</font></td></tr>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR>");
    print("</TABLE>");
};


function W5_EditFileDestination() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all the classes and display as a table */  
    print("<tr><td colspan=2>");
    print("<font color=ffffff><h2>Select one file destination from the list");
    print(" and click the submit button to edit the details</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=571>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color=ffffff>ID</font></th>");
    print("<th><font color=ffffff>File Destination</font></th>");
    print("<th><font color=ffffff>Display Order</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=ffffff>New</td>");
    print("</tr>");

    $table=$db->select("ScripFileDestinations");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['FileDestinationID'];
	$theFileDestination=$row['Text'];
	$theDisplayOrder=$row['DisplayOrder'];
        print("<td><input type=radio name=ID value=$theID></td>");
	print("<td><font color=FFFFFF>$theID</font></td>");
	print("<td><font color=FFFFFF>$theFileDestination</font></td>");
	print("<td><font color=FFFFFF>$theDisplayOrder</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan=8 align=center><input type=submit></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function W5_EditSingleFileDestination($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the class and display in a form */  
    $table=$db->select("ScripFileDestinations",
                      array('FileDestinationID'=>$ID));
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theFileDestination=$row['Text'];
            $theDisplayOrder=$row['DisplayOrder'];
        };
    } else {
        $theFileDestination="";
        $theDisplayOrder="";
    };

    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=572>");
    print("<input type=hidden name=ID value=$ID>");

    print("<tr><th><font color=ffffff>File Destination</font></th>");
    print("<th><font color=ffffff>DisplayOrder</font></th></tr>");

    print("<tr><td><input type=text name=theFileDestination ");
    print("value='$theFileDestination' size=35></td>");
    print("<td><input type=text name=theDisplayOrder ");
    print("value='$theDisplayOrder' size=35></td></tr>");

    print("<tr><td colspan=4 align=center>");
    print("<input type=submit value=Update></td></tr>");

    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function W5_UpdateFileDestination($ID, $theFileDestination,
                                   $theDisplayOrder) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the class */  
    $data= array("Text" => $theFileDestination,
                 "DisplayOrder" => $theDisplayOrder);

    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('FileDestinationID'=>$ID);
        $db->update("ScripFileDestinations",$data, $condition);
    } else {
        $db->insert("ScripFileDestinations",$data);
    };

    print("<tr><td colspan=2><font color=ffffff>File destination updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};




function W5_EditCDRegClass() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all the classes and display as a table */  
    print("<tr><td colspan=2>");
    print("<font color=ffffff><h2>Select one class from the list");
    print(" and click the submit button to edit the details</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=511>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color=ffffff>ID</font></th>");
    print("<th><font color=ffffff>Class</font></th>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=ffffff>New</td>");
    print("</tr>");

    $table=$db->select("ScripCDRegClass");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
	$theCDRegClass=$row['CDRegClass'];
        print("<td><input type=radio name=ID value=$theID></td>");
	print("<td><font color=FFFFFF>$theID</font></td>");
	print("<td><font color=FFFFFF>$theCDRegClass&nbsp;</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan=8 align=center><input type=submit></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function W5_EditSingleCDRegClass($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the class and display in a form */  
    $table=$db->select("ScripCDRegClass",array('ID'=>$ID));
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theCDRegClass=$row['CDRegClass'];
        };
    } else {
        $theCDRegClass="";
    };

    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=512>");
    print("<input type=hidden name=ID value=$ID>");

    print("<tr><th><font color=ffffff>Class</font></th></tr>");

    print("<tr><td><input type=text name=theCDRegClass ");
    print("value='$theCDRegClass' size=35></td></tr>");

    print("<tr><td colspan=4 align=center>");
    print("<input type=submit value=Update></td></tr>");

    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function W5_UpdateCDRegClass($ID, $theCDRegClass) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the class */  
    $data= array("CDRegClass" => $theCDRegClass);

    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('ID'=>$ID);
        $db->update("ScripCDRegClass",$data, $condition);
    } else {
        $db->insert("ScripCDRegClass",$data);
    };

    print("<tr><td colspan=2><font color=ffffff>Class updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};

function W6_EditCDRegBrand() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all the classes and display as a table */  
    print("<tr><td colspan=2>");
    print("<font color=ffffff><h2>Select one brand from the list");
    print(" and click the submit button to edit the details</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=521>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color=ffffff>ID</font></th>");
    print("<th><font color=ffffff>Brand</font></th>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=ffffff>New</td>");
    print("</tr>");

    $table=$db->select("ScripCDRegBrand");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
	$theCDRegBrand=$row['CDRegBrand'];
        print("<td><input type=radio name=ID value=$theID></td>");
	print("<td><font color=FFFFFF>$theID</font></td>");
	print("<td><font color=FFFFFF>$theCDRegBrand&nbsp;</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan=8 align=center><input type=submit></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function W6_EditSingleCDRegBrand($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the brand and display in a form */  
    $table=$db->select("ScripCDRegBrand",array('ID'=>$ID));
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theCDRegBrand=$row['CDRegBrand'];
        };
    } else {
        $theCDRegBrand="";
    };

    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=522>");
    print("<input type=hidden name=ID value=$ID>");

    print("<tr><th><font color=ffffff>Brand</font></th></tr>");

    print("<tr><td><input type=text name=theCDRegBrand ");
    print("value='$theCDRegBrand' size=35></td></tr>");

    print("<tr><td colspan=4 align=center>");
    print("<input type=submit value=Update></td></tr>");

    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function W6_UpdateCDRegBrand($ID, $theCDRegBrand) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the brand */  
    $data= array("CDRegBrand" => $theCDRegBrand);

    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('ID'=>$ID);
        $db->update("ScripCDRegBrand",$data, $condition);
    } else {
        $db->insert("ScripCDRegBrand",$data);
    };

    print("<tr><td colspan=2><font color=ffffff>Brand updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};

function W7_EditCDRegStrength() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all the strengths and display as a table */  
    print("<tr><td colspan=2>");
    print("<font color=ffffff><h2>Select one strength from the list");
    print(" and click the submit button to edit the details</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=531>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color=ffffff>ID</font></th>");
    print("<th><font color=ffffff>Strength</font></th>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=ffffff>New</td>");
    print("</tr>");

    $table=$db->select("ScripCDRegStrength");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
	$theCDRegStrength=$row['CDRegStrength'];
        print("<td><input type=radio name=ID value=$theID></td>");
	print("<td><font color=FFFFFF>$theID</font></td>");
	print("<td><font color=FFFFFF>$theCDRegStrength&nbsp;</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan=8 align=center><input type=submit></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function W7_EditSingleCDRegStrength($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the strength and display in a form */  
    $table=$db->select("ScripCDRegStrength",array('ID'=>$ID));
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theCDRegStrength=$row['CDRegStrength'];
        };
    } else {
        $theCDRegStrength="";
    };

    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=532>");
    print("<input type=hidden name=ID value=$ID>");

    print("<tr><th><font color=ffffff>Strength</font></th></tr>");

    print("<tr><td><input type=text name=theCDRegStrength ");
    print("value='$theCDRegStrength' size=35></td></tr>");

    print("<tr><td colspan=4 align=center>");
    print("<input type=submit value=Update></td></tr>");

    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function W7_UpdateCDRegStrength($ID, $theCDRegStrength) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the strength */  
    $data= array("CDRegStrength" => $theCDRegStrength);

    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('ID'=>$ID);
        $db->update("ScripCDRegStrength",$data, $condition);
    } else {
        $db->insert("ScripCDRegStrength",$data);
    };

    print("<tr><td colspan=2><font color=ffffff>Strength updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};

function W8_EditCDRegForm() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all the forms and display as a table */  
    print("<tr><td colspan=2>");
    print("<font color=ffffff><h2>Select one form from the list");
    print(" and click the submit button to edit the details</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=541>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color=ffffff>ID</font></th>");
    print("<th><font color=ffffff>Form</font></th>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=ffffff>New</td>");
    print("</tr>");

    $table=$db->select("ScripCDRegForm");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
	$theCDRegForm=$row['CDRegForm'];
        print("<td><input type=radio name=ID value=$theID></td>");
	print("<td><font color=FFFFFF>$theID</font></td>");
	print("<td><font color=FFFFFF>$theCDRegForm&nbsp;</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan=8 align=center><input type=submit></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function W8_EditSingleCDRegForm($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the form and display in a form (so to speak!) */  
    $table=$db->select("ScripCDRegForm", array('ID'=>$ID));
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theCDRegForm=$row['CDRegForm'];
        };
    } else {
        $theCDRegForm="";
    };

    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=542>");
    print("<input type=hidden name=ID value=$ID>");

    print("<tr><th><font color=ffffff>Form</font></th></tr>");

    print("<tr><td><input type=text name=theCDRegForm ");
    print("value='$theCDRegForm' size=35></td></tr>");

    print("<tr><td colspan=4 align=center>");
    print("<input type=submit value=Update></td></tr>");

    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function W8_UpdateCDRegForm($ID, $theCDRegForm) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the form */  
    $data= array("CDRegForm" => $theCDRegForm);

    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('ID'=>$ID);
        $db->update("ScripCDRegForm",$data, $condition);
    } else {
        $db->insert("ScripCDRegForm",$data);
    };

    print("<tr><td colspan=2><font color=ffffff>Form updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};




function W9_EditCDPageTitleAndLabel(){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of all the pages and display as a table */  
    print("<tr><td colspan=2>");
    print("<font color=ffffff><h2>Select one page from the list");
    print(" and click the Select button to edit the details</h2></font>");
    print("</td></tr>");
    print("<form action=scripadmin.php method=post>");
    print("<td colspan=8 align=center><input type=submit ");
    print("value='Select'></td>");
    print("<tr><td colspan=2 align=center><table border=1>");
    print("<input type=hidden name=tab value=551>");
    print("<tr><th>&nbsp;</th>");
    print("<th><font color=ffffff>ID</font></th>");
    print("<th><font color=ffffff>Class</font></th>");
    print("<th><font color=ffffff>Brand</font></th>");
    print("<th><font color=ffffff>Strength</font></th>");
    print("<th><font color=ffffff>Form</font></th>");
    print("<th><font color=ffffff>Balance Minimum</font></th>");
    print("<th><font color=ffffff>Balance Maximum</font></th>");
    print("<th><font color=ffffff>CD Page</font></th>");

    print("<tr>");
    print("<td><input type=radio name=ID value='0' checked></td>");
    print("<td><font color=ffffff>New</td>");
    print("</tr>");

    $table=$db->select("ScripCDPageTitleAndLabel");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        print("<td><input type=radio name=ID value=$theID></td>");
	print("<td><font color=FFFFFF>$theID</font></td>");

	$theClassID=$row['Class'];
        $condition = array('ID'=>$theClassID);
        $idtable=$db->select("ScripCDRegClass", $condition);
        $theClassName="";
        foreach ($idtable as $idrow) {
            $theClassName=$idrow['CDRegClass'];
        };
	print("<td><font color=FFFFFF>$theClassName&nbsp;</font></td>");

	$theBrand=$row['Brand'];
	print("<td><font color=FFFFFF>$theBrand</font></td>");

	$theStrength=$row['Strength'];
	print("<td><font color=FFFFFF>$theStrength</font></td>");

	$theForm=$row['Form'];
	print("<td><font color=FFFFFF>$theForm</font></td>");

	$theMinimumBalance=$row['MinimumBalance'];
	print("<td><font color=FFFFFF>$theMinimumBalance</font></td>");

	$theMaximumBalance=$row['MaximumBalance'];
	print("<td><font color=FFFFFF>$theMaximumBalance</font></td>");

	$theCDPage=$row['CDPage'];
	print("<td><font color=FFFFFF>$theCDPage</font></td>");

        print("</tr>");
    };

    print("<tr>");
    print("<td colspan=8 align=center><input type=submit ");
    print("value='Select'></td>");
    print("</tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function W9_EditSingleCDPageTitleAndLabel($ID){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Get details of the page and display in a form */  
    $table=$db->select("ScripCDPageTitleAndLabel", array('ID'=>$ID));
    Z_DrawHeader();

    if ($ID!='0') {
        foreach ($table as $row) {
            $theClass=$row['Class'];
            $theBrand=$row['Brand'];
            $theStrength=$row['Strength'];
            $theForm=$row['Form'];
            $theMinimumBalance=$row['MinimumBalance'];
            $theMaximumBalance=$row['MaximumBalance'];
            $theCDPage=$row['CDPage'];
        };
    } else {
        $theClass="";
        $theBrand="";
        $theStrength="";
        $theForm="";
        $theMinimumBalance="";
        $theMaximumBalance="";
        $theCDPage="";
    };

    print("<tr><td colspan=2 align=center><table border=1>");
    print("<form action=scripadmin.php method=post>");
    print("<input type=hidden name=tab value=552>");
    print("<input type=hidden name=ID value=$ID>");

    print("<tr>");
    print("<th><font color=ffffff>Class</font></th>");
    print("<th><font color=ffffff>Brand</font></th>");
    print("<th><font color=ffffff>Strength</font></th>");
    print("<th><font color=ffffff>Form</font></th>");
    print("</tr>");

    print("<tr>");

    // The CLASS drop-downlist ...
    print("<td><select name=theClass>");

    print("<option value='0'");
    if ($ID=="0") { print(" selected"); };
    print(">");

    $idtable=$db->select("ScripCDRegClass");
    foreach ($idtable as $idrow) {
        $anID=$idrow['ID'];
	$aClass=$idrow['CDRegClass'];
	print("<option value=$anID ");
	if ($anID == $theClass) { print("selected "); };
        print("> $aClass ($anID)");
    };
    print("</select>\n");
    print("</td>");

    // The BRAND text field ...
    print("<td>");
    print("<input type=text name=theBrand value='$theBrand' size=50>");
    print("</td>");

    // The STRENGTH text field ...
    print("<td>");
    print("<input type=text name=theStrength value='$theStrength' size=20>");
    print("</td>");

    // The FORM text field ...
    print("<td>");
    print("<input type=text name=theForm value='$theForm' size=30>");
    print("</td>");

    print("</tr>");

    print("<tr>");
    print("<th><font color=ffffff>Minimum Balance</font></th>");
    print("<th><font color=ffffff>Maximum Balance</font></th>");
    print("<th><font color=ffffff>CD Page</font></th>");
    print("</tr>");
    print("<tr>");

    // The MINIMUM BALANCE text field ...
    print("<td>");
    print("<input type=text name=theMinimumBalance ");
    print("value='$theMinimumBalance' size=30>");
    print("</td>");

    // The MAXIMUM BALANCE text field ...
    print("<td>");
    print("<input type=text name=theMaximumBalance ");
    print("value='$theMaximumBalance' size=30>");
    print("</td>");

    // The CD PAGE text field ...
    print("<td>");
    print("<input type=text name=theCDPage value='$theCDPage' size=10>");
    print("</td>");

    print("</tr>");

    print("<tr><td colspan=4 align=center>");
    print("<input type=submit value=Update></td></tr>");

    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function W9_UpdateCDPageTitleAndLabel($ID, $theClass,
                    $theBrand, $theStrength, $theForm,
                    $theMinimumBalance, $theMaximumBalance, 
                    $theCDPage){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Update details of the page */  
    $data= array("Class" => $theClass,"Brand" => $theBrand,
                 "Strength" => $theStrength,"Form" => $theForm,
                 "MinimumBalance" => $theMinimumBalance,
                 "MaximumBalance" => $theMaximumBalance,
                 "CDPage" => $theCDPage);

    Z_DrawHeader();

    if ($ID!='0') {
        $condition=array('ID'=>$ID);
        $db->update("ScripCDPageTitleAndLabel",$data, $condition);
    } else {
        $db->insert("ScripCDPageTitleAndLabel",$data);
    };

    print("<tr><td colspan=2><font color=ffffff>Page updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};

function X1_ReleaseGet() {
    // Choose the week (e.g. 3) that is to be released or retracted

    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>");
    print("<p>Important: The two halves of this screen are independent but have been put together for convenience.</p>");
    print("<p>Select a year group and a location, the click Display Data. The tables will then show you the relevant dispensing and checking data.</p>");
    print("<p>Bearing in mind the data you have been shown, select one week from the list, choose either Release or Retract and then click the Implement button.</p>");
    print("<p>In addition (for dispensing) ... do you want this week available as a practice week? (This may mean different sets for different students.)</p>");
    print("<p>The tables show the current CLASS settings. Note, however, that results for individual students may have been changed individually.</p><br>");
    print("</font>");
    print("</td></tr>\n");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array ("YearGroup"=>$_SESSION['yearGroup'], "Location"=>$_SESSION['location']);
    
    // Note on layout: One cell across the top (for year and location which applies to both dispensing and checking) then two cells below and
    // to the left (for dispensing) and two below and to the right (for checking) ...

    // YEAR and LOCATION ...
    print("<tr><td colspan='2' align='center' valign='top'>");

    // Begin form (returns to 492) ...
    print("<form action=scripadmin.php method='post'>\n");
    print("<input type='hidden' name='tab' value='492'>");

    // SELECT year group ...
    print("<font color=#ffffff>Year group: </font>");
    print("<select name='yeargroup'>");
    print("<option value=''>Select...</option>");
    $nYears = 4;
    for ($i = 1; $i < $nYears + 1; $i++) {
        print("<option value='$i'");
        if ($_SESSION['yearGroup'] == $i) { print(" selected"); };
        print(">$i");
    };
    print("</select>");

    print("&nbsp; &nbsp;");

    // Need a set of all the DIFFERENT LOCATIONS in ScripStudent ...
    $allLocations = array();
    $table=$db->select("ScripStudent");
    foreach ($table as $row){
        // Read an entry from the table ...
        $thisLocation = $row['Location'];
        $alreadyNoted = false;

        // Loop over contents of $allLocations ...
        for ($j = 0; $j < count($allLocations); $j++){
            if ($allLocations[$j] == $thisLocation){ $alreadyNoted = true; };
        };

        if (!$alreadyNoted) {
            // If not previously in $allLocations, add it now ...
            array_push($allLocations, $thisLocation);
        };
    };
    // ... all the locations are now in $allLocations

    // SELECT location ...
    print("<font color=#ffffff>Location: </font>");
    print("<select name='location'>");
    print("<option value=''>Select...</option>");
    for ($i = 0; $i < count($allLocations); $i++) {
        print("<option value='$allLocations[$i]'");
        if ($allLocations[$i] == $_SESSION['location']) { print(" selected"); };
        print(">$allLocations[$i]");
    };
    print("</select>");

    print("<br><br>");

    print("<input type='submit' value='Display Data' name='displaydata'>");

    print("<br><br>");

    print("</tr>");


    // 
    print("<tr><td colspan='1' align='center' valign='top'>");

    // If year and location set, draw the table ...
    // DISPENSING: 
    if ($_SESSION['yearGroup'] && $_SESSION['location']){
        // DISPENSING: Start the table ...
        print("<table border='1'>\n");

        // DISPENSING: Table title ...
        print("<tr>");
        print("<th colspan='3'>");
        print("<font color='#ffffff'>Dispensing for Year " . $_SESSION['yearGroup'] . " at " . $_SESSION['location'] . "</font></th>");
        print("</tr>");

        // DISPENSING: Column titles ...
        print("<tr>");
        print("<td><font color=ffffff>Week</font></td>");    
        print("<td><font color=ffffff>Released</font></td>");    
        print("<td><font color=ffffff>Available</font></td>");    
        print("</tr>");

        // DISPENSING: Column contents ...
        $table=$db->select("ScripWeeksReleased", $condition);
        foreach ($table as $row) {
            $weeknumber = $row['WeekNumber'];
            $rr = $row['RR']; // Results released
            $wr = $row['WR']; // Weeks released
            print("<tr>");
            print("<td><font color=ffffff>$weeknumber</font></td>");
            print("<td><font color=ffffff>$rr</font></td>");
            print("<td><font color=ffffff>$wr</font></td>");
            print("</tr>");
        };
        print("</table>");
    };

    print("</td>");

    // CHECKING: Start the table ...
    print("<td align='center' valign='top'>");

    // CHECKING: Table title ...
    if ($_SESSION['yearGroup'] && $_SESSION['location']){
        print("<table border='1'>");
        print("<tr>");
        print("<th colspan='2'>");
        print("<font color='#ffffff'>Checking for Year " . $_SESSION['yearGroup'] . " at " . $_SESSION['location'] . "</font></th>");
        print("</tr>");

        // CHECKING: Column headings ...
        print("<tr>");
        print("<td><font color='#ffffff'>Week</font></td>");
        print("<td><font color='#ffffff'>Released</font></td>");
        print("</tr>");

        // CHECKING: Column contents ...
        $table=$db->select("ScripCheckingWeeksReleased", $condition);
        foreach ($table as $row) {
            $weeknumber = $row['WeekNumber'];
            $cr = $row['CR']; // Results released
            print("<tr>");
            print("<td><font color='#ffffff'>$weeknumber</font></td>");
            print("<td><font color='#ffffff'>$cr</font></td>");
            print("</tr>");
        };

        print("</table>");
    };
    print("</td>");
    print("</tr>");

    // New row: The CONTROLS ...
    print("<tr>");

    // DISPENSING: Controls ...
    print("<td colspan='1' align='center'>");

    if ($_SESSION['yearGroup'] && $_SESSION['location']){
        print("<table border='1'>\n");

        print("<tr>");
        print("<td align=center>");

        // SELECT week
        print("<font color=#ffffff>Week: </font>");
        print("<select name='week'>");
        for ($i = 1; $i < 7+1; $i++) {
            print("<option value='$i'>$i");
        };
        print("</select>");
        print("</td></tr>");

        print("<tr><td><font color=#ffffff>Results: ");
        print("<input type=radio name=RR value=Release checked>Release");
        print("<input type=radio name=RR value=Retract>Retract");
        print("</font></td></tr>");

        print("<tr><td>");
        print("<font color=ffffff>Make available as a practice week</font>");
        print("<input type='checkbox' name='makeavailable' checked>");
        print("</td>");
        print("</tr>");

        print("<tr><td colspan='3' align='center'>");
        print("<input value='Implement' type='submit' name='implementDispensing'>");
        print("</td></tr>");

        print("</td>");
        print("</tr>");
        print("</td>");

        print("</tr>");

        print ("</table>");
//    print("</form>");
    };

    print("</td>");


    // CHECKING: Controls ...
    print("<td colspan='1' align='center' valign='top'>");

    // Only show the controls if both a year group and a loction have been chosen ..
    if ($_SESSION['yearGroup'] && $_SESSION['location']){
        print("<table border='1'>\n");

//    print("<form action=scripadmin.php method=post>\n");
//    print("<input type=hidden name=tab value=493>");

        print("<tr>");
        print("<td align='center' valign='top'>");
        print("<font color=#ffffff>Week: </font>");
        print("<select name=weekchecking>");
        for ($i = 1; $i < 4+1; $i++) {
            print("<option value='$i'>$i");
        };
        print("</select></td></tr>");

        print("<tr><td><font color=#ffffff>Results: ");
        print("<input type=radio name=CR value=Release checked>Release");
        print("<input type=radio name=CR value=Retract>Retract");
        print("</font></td></tr>");

        print("<tr><td colspan='3' align='center'>");
        print("<input value='Implement' type='submit' name='implement' name='implementChecking'>");
        print("</td></tr>");
        print("</td>");
        print("</tr>");
        print("</td>");

        print("</tr>");

        print ("</table>");
    };

    print("</td>");

    print("</form>");
    print("</tr>");

    // Blank line ...
    print("<tr><td align='center' colspan='2'>&nbsp;</td></tr>");

    // "Return to Menu" hyperlink ...
    print("<tr><td align='center' colspan='2'><p><a href=scripadmin.php?tab=5>Return to Menu</a></p></td></tr>");
};


function X1A_ReleaseImplement ($RR, $week, $makeavailable) {
    // Change the appropriate records in ScripStudentRecord:

    // Two things have be done. Say that (e.g. Week 4 has released) (stored in one table) and amend the FMR [finished moderated released] aspect of each student 
    // 

    // Administrator has chosen a week (e.g. 4), a release condition (i.e. release=1 or retract=0) and an availability condition (similar).
    // Now look in ScripTermPatterns to find what letters occur in this week. For each term pattern, see what students have that pattern ...
    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff></font>");
    print("</td></tr>\n");

    if ($RR == "Release") { $release = 1; } else { $release = 0; };
    $week1 = "wk".$week;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());  

    // Update (or insert into) the table of weeks released the (or a) line with release data (results released and week released [for use as a U-mode script]) ...
//QUESTION: Are the following variables all set?
    $condition=array('WeekNumber'=>$week, 'YearGroup'=>$_SESSION['yearGroup'], 'Location'=>$_SESSION['location']);
    $text=array('RR'=>$release, 'WR'=>$makeavailable);
    // Look for such a line in the table ...
    $table=$db->select("ScripWeeksReleased", $condition);

    // Does it have such a line?
    if (count($table)){
        // Yes - update it ...
        $db->update("ScripWeeksReleased", $text, $condition);
    } else {
        // No - insert one ...
        $text = array_merge($text, $condition);
        $db->insert("ScripWeeksReleased", $text);
    };
    // ... that's the easy bit!

// QUESTION: Has something been done - either a line added or a line inserted?

    // Now change all the records in ScripStudentRecord ...
    $condition=array('YearGroup'=>$_SESSION['yearGroup'], 'Location'=>$_SESSION['location']);
    $table=$db->select("ScripTermPattern", $condition);
    // ... this gives us that part of ScripTermPattern which applies to current time and place - we shall now loop over this ...

    // Loop over the practice codes - which means loops over the two-letter codes ...
    foreach ($table as $row) {
        // There may be only one row in this table (all class doing same thing) or more than one (students split into groups with each group doing a different two-letter 
        // code). For each row, read the practice code (primary key) and the matching letter for the week that has already been specified in the call to this function ...
        $practiceCode = $row['PracticeCode'];
        $thisWeeksLetter = $row[$week1];

        print("<br /><br /><b>Students with practice code $practiceCode use set $thisWeeksLetter in Week No. $week</b><br />");

        // ... this means $thisWeeksLetter contains a two-letter code for the current week and the matching practice code e.g. this might give KE and 1 on the first
        // loop, MQ and 2 on the second, etc. Successive loops are (in effect) different groups of students and the practice code is therefore a link back to the student.

        $condition = array("PracticeCode"=>$practiceCode);
        // Find students with current practice code ... (practice code is a primary key, therefore unique, therefore no need to specify time and place)
        $stable=$db->select("ScripStudent", $condition); // ... all students who have this particular practice code

        // Now loop over the students ...
        foreach ($stable as $srow) {
            $sid = $srow['StudentID']; // ... a student with current practice code
            print($srow['LastName'] . ", " . $srow['FirstName'] . " ($sid): ");

            // Look for CASES that have a similar two-letter code for current student ... (We can do all cases in one go and not need yet another loop.)
            $condition="CaseID LIKE '".$thisWeeksLetter."%' and StudentID=$sid"; // e.g. $thisWeeksLetter = GI means this $condition will pick up GI1, GI2, GI3, etc.              
            $text = array('ReleasedYN'=>$release);

            $db->update("ScripStudentRecord", $text, $condition);

            // While still testing would like to see what current settings are in ScripStudentRecord:
            $Xtable=$db->select("ScripStudentRecord", $condition); // ... all students who have this particular practice code
            foreach ($Xtable as $Xrow){
                if ($Xrow['ReleasedYN']) { print("<b>" . $Xrow['CaseID'] . "</b>"); } else { print("<i>" . $Xrow['CaseID'] . "</i>"); };
                print(" ");
            }; 
            print("<br />");

            // JANUARY 2012 - Now also need to amend ScripStudentRecordCalculations too
            $db->update("ScripStudentRecordCalculations", $text, $condition);
            // ... there will typically be ten rows

/* This is a very useful piece of diagnostic code - keep it for a while 
            $rcondition = array("StudentID"=>$sid);
            $rtable=$db->select("ScripStudentRecord", $rcondition); 
            foreach ($rtable as $rrow) {
                $CaseID = $rrow['CaseID'];
                if (substr($CaseID, 0, 1) == $thisWeeksLetter) {
                    print("<b>Student $sid did $thisWeeksLetter ($CaseID) in this week</b><br>");
                } else {
                    print("<i>Student $sid did $thisWeeksLetter ($CaseID) in this week</i><br>");
                };
            }; 
End of the useful diagnostic code */

        };
    };

    // Now the "make available" code ...
    // [Isn't this a repeat of what is done above?]
    $condition=array('WeekNumber'=>$week, 'YearGroup'=>$_SESSION['yearGroup'], 'Location'=>$_SESSION['location']);
    $text=array("RR"=>$release);
    $db->update("ScripWeeksReleased", $text, $condition);

    // Report what has been done ...
    print("<tr><td bgcolor='#ffffff'>");
    print("<p>All student records for week $week marked as ");
    if (!$release) { print("NOT "); };
    print("released.</p>");

    print("<p>Note: Records only available if they have also been moderated</p>");
    print("<p>Sets for week $week now ");
    if (!$makeavailable) { print("NOT "); };
    print("available as a practice week.</p>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
};

function X1B_ManualReleaseGet () {
    // Set a student's scrips manually

    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>Choose:<ul>");
    print("<li>Either Release or Retract;</li>");
    print("<li>A student;</li>");
    print("<li>Either Week or Letter; and</li>");
    print("<li>The number of the week or the letter of the case.</li>");
    print("</ul>");
    print("Finally click the submit button.</font>");
    print("</td></tr>\n");
    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>\n");

    print("<form action=scripadmin.php method=post>\n");
    print("<input type=hidden name=tab value=497>");

    print("<tr>\n");
    print("<td><font color=#ffffff>\n");
    print("<input type=radio name=RR value=Release checked>Release\n");
    print("<input type=radio name=RR value=Retract>Retract\n");
    print("<br>\n");

    print("<select name=student>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    // Sort by alphabetical order ...
    $sort = "LastName";
    $condition = ""; // Need to occupy space in list of arguments ...
    $table=$db->select("ScripStudent", $condition, $sort);
    foreach ($table as $row) {
        $sid = $row['StudentID'];
        $fn = $row['FirstName'];
        $ln = $row['LastName'];
        print("<option value='$sid'>$ln, $fn ($sid)\n");
    }
    print("</select>\n");
    print("</font></td>\n");

    print("<td><font color=#ffffff>\n");
    print("<input type='radio' name='WL' value='Week' checked>Week<br>");
    print("<select name='week'>\n");
    for ($i = 1; $i < 7 + 1; $i++) {
        print("<option value='$i'>$i\n");
    }
    print("</select>\n");
    print("</font></td>\n");

    print("<td><font color=#ffffff>\n");
    print("<input type=radio name='WL' value='Letter'>Letter<br>");

    // Compile a list from ScriptTermPattern of all two-letter codes (in fields wk1, ...,wk7) ...
    // (Should this be restricted by time and place?)

    $table=$db->select("ScripTermPattern");
    foreach ($table as $row){
        
    };



    print("<select name='letter'>\n");
    print("<option value='A'>A\n");
    print("<option value='B'>B\n");
    print("<option value='C'>C\n");
    print("<option value='D'>D\n");
    print("<option value='E'>E\n");
    print("<option value='F'>F\n");
    print("<option value='G'>G\n");
    print("</select>\n");
    print("</font></td>\n");

    print("<td rowspan=2><input type=submit></td>");
    print("</tr>\n");
    print("</form>\n");

    print ("</table>\n");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
};

function X1C_ManualReleaseImplement ($RR, $WL, $week, $letter, $student) {
// Set a student's scrips manually

    Z_DrawHeader();
    print("<tr><td colspan='2' align='center'>");
    print("<table>\n");

    if ($RR == "Release") {$release = 1;} else {$release = 0;};

print("<td>RR = $RR</td>");
print("<td>WL = $WL</td>");
print("<td>week = $week</td>");
print("<td>letter = $letter</td>");
print("<td>student = $student</td>");
print("<td>release = $release</td>");

/* Amend ScripStudent Record for student specified by id
   $student and scrips specified by either $week or
   $letter (which one is determined by $WL); nature of
   amendment is determine by $RR
*/

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    if ($WL=="Week") {
    // Find the appropriate letter for this student

        $table=$db->select("ScripStudent", array("StudentID"=>$student));
        foreach ($table as $row) {
            $pc = $row['PracticeCode'];
        }
        // This student uses practice code $pc and this is week ...
        $table=$db->select("ScripTermPattern", array("PracticeCode"=>$pc));
        foreach ($table as $row) {
            $letter = $row["wk".$week];
        }
    }

/* Now have the letter (one way or another) - update */

    $text = array ("ReleasedYN"=>$release);
    $condition = "CaseID LIKE '".$letter."%' AND StudentID=$student";
    $db->update("ScripStudentRecord", $text, $condition);

    print("<tr><td bgcolor=#FFFFFF>");
    print("<p>Student Record updated</p>");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
    print ("</table>");
}

function X2A_ResetFMR () {
    // Set a student's Finished-Moderated-Released characteristics (for a given case) manually:

    Z_DrawHeader();

    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>Choose:");

    print("<ul>");
    print("<li>Student;</li>");
    print("<li>Case; and</li>");
    print("<li>FMR details</li>");
    print("</ul>");

    print(" Finally click the submit button.</font>");
    print("</td></tr>\n");

    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>\n");

    print("<form action=scripadmin.php method=post>\n");
    print("<input type=hidden name=tab value=622>");

    print("<tr>\n");
    // Start cell for student ...
    print("<td valign='top'><font color=#ffffff>\n");

    print("Student<br />");
    print("<select name=student>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Sort by alphabetical order ...
    $sort = "LastName";
    $condition = ""; // Need to occupy space in list of arguments ...
    $table=$db->select("ScripStudent", $condition, $sort);
    foreach ($table as $row) {
        $sid = $row['StudentID'];
        $ssn = $row['StudentSaturnNumber'];
        $fn = $row['FirstName'];
        $ln = $row['LastName'];
        print("<option value='$sid'>$ln, $fn ($sid)\n");
    };
    print("</select>\n");
    print("</font></td>\n");

    // Start a cell for case ...
    print("<td valign='top'><font color=#ffffff>\n");
    print("Case name (e.g. CB4)<br /><input type=text name=case>");
    print("</font></td>\n");

    // Start a cell for FMR ...
    print("<td><font color=#ffffff>\n");
    print("Details<br />");
    print("<input type=radio name=F value=0 checked>0");
    print("<input type=radio name=F value=1>1");
    print("&nbsp;Finished<br>");

    print("<input type=radio name=M value=0 checked>0");
    print("<input type=radio name=M value=1>1");
    print("&nbsp;Moderated<br>");

    print("<input type=radio name=R value=0 checked>0");
    print("<input type=radio name=R value=1>1");
    print("&nbsp;Released<br>");
    print("</font></td>");

    print("</tr>\n");

    print("<tr>");
    print("<td colspan=4 rowspan=2 align=center>");
    print("<input type=submit value='Submit Change'></td>");
    print("</tr>\n");
    print("</form>\n");

    print ("</table>\n");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
};

function X2B_ImplementResetFMR ($student, $case, $F, $M, $R) {
    // Set a student's Finished-Moderated-Released characteristics (for a given case) manually:

    Z_DrawHeader();

    print("<tr><td colspan='2' align='center'>");
    print("<table>\n");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // First, find out whether this is a calculations icon ...
    $condition = array("Letter"=>$case);
    $table = $db->select("ScripsScrips", $condition);
    $formID = 0; // default
    foreach ($table as $row){
        $formID = $row['FormID'];
    };


    if ($formID){
        // This case does exist (we know this from the fact that it has a non-zero form ID i.e. itwa sfound in ScripsScrips) ...

        //
        $condition = array("StudentID"=>$student, "CaseID"=>$case);

        // Look in the ScripStudentRecord tables for this condition - it can only be found in one at most (since
        // a script is either a practice script or an exam script) ...
        if ($formID == 53){
            $description = "calculations";
            $table = $db->select("ScripStudentRecordCalculations", $condition);
            $tableE = $db->select("ScripStudentRecordCalculationsExam", $condition);
        } else if ($formID > 0) {
            $description = "not calculations";
            $table = $db->select("ScripStudentRecord", $condition);
            $tableE = $db->select("ScripStudentRecordExam", $condition);
        };

        print("<tr><td bgcolor=#FFFFFF>");

        if (count($table)){
            $text = array ("FinishedYN"=>$F, "ModeratedYN"=>$M, "ReleasedYN"=>$R);
            if ($formID == 53){
                $db->update("ScripStudentRecordCalculations", $text, $condition);        
            } else {
                $db->update("ScripStudentRecord", $text, $condition);        
            };
            print("<p>Student practice record for student $student taking case $case ($description) set to:<br><ul>");
            print("<li>"); if (!$F) { print("un");};print("finished</li><br>");
            print("<li>"); if (!$M) { print("un");};print("moderated</li><br>");
            print("<li>"); if (!$R) { print("not ");};print("released</li><br>");
        } else if (count($tableE)){
            $text = array ("FinishedYN"=>$F, "ModeratedYN"=>$M);
            if ($formID == 53){
                $db->update("ScripStudentRecordCalculationsExam", $text, $condition);
            } else {
                $db->update("ScripStudentRecordExam", $text, $condition);
            };        
            print("<p>Student EXAM record for student $student taking case $case ($description) set to:<br><ul>");
            print("<li>"); if (!$F) { print("un");};print("finished</li><br>");
            print("<li>"); if (!$M) { print("un");};print("moderated</li><br>");
            print("<li>"); if (!$R) { print("not ");};print("released</li><br>");
        } else {
            print("<p>No record found for case $case ($description) in the relevant table - no changes made.</p>");
        };
    } else {
        print("<p><font color='#ffffff'>Case $case does not exist.</font></p>");
    };

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
    print ("</table>");
};

function X3A_DeleteEntry () {
// Delete a student's results by deleting ScripStudentRecord
    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>");
    print("<p>This function allows a user to start a script again - ");
    print("no settings are kept from previous attempts</p>");
    print("Choose:<ul>");
    print("<li>Student; and</li>");
    print("<li>Case</li>");
    print("</ul>");
    print(" Finally click the submit button.</font>");
    print("</td></tr>\n");
    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>\n");

    print("<form action=scripadmin.php method=post>\n");
    print("<input type=hidden name=tab value=627>");

    print("<tr>\n");
    print("<td><font color=#ffffff>\n");

    print("<select name=student>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    // Sort by alphabetical order ...
    $sort = "LastName";
    $condition = ""; // Need to occupy space in list of arguments ...
    $table=$db->select("ScripStudent", $condition, $sort);
    foreach ($table as $row) {
        $sid = $row['StudentID'];
        $fn = $row['FirstName'];
        $ln = $row['LastName'];
        print("<option value='$sid'>$ln, $fn\n");
    }
    print("</select>\n");
    print("</font></td>\n");

    print("<td><font color=#ffffff>\n");
    print("<input type=text name=case>Case name (e.g. C4)");
    print("</font></td>\n");

    print("</tr>\n");

    print("<tr>");
    print("<td colspan=3 rowspan=2 align=center>");
    print("<input type=submit></td>");
    print("</tr>\n");
    print("</form>\n");

    print ("</table>\n");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
}

function X3B_ImplementDeleteEntry ($student, $case) {
// Set a student's scrips manually
    Z_DrawHeader();

    print("<tr><td colspan='2' align='center'>");
    print("<table>\n");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array("StudentID"=>$student, "CaseID"=>$case);
/*
    $table=$db->select("ScripStudentRecord", $condition);
    foreach ($table as $row) {
        $classifyid=$row['ClassifyID'];
    };
*/

//print("Classify = $classifyid<br>");
/*
    $db->delete("ScripCDRegister");
    $db->delete("ScripClassify");
    $db->delete("ScripEndorse");
    $db->delete("ScripFile");
    $db->delete("ScripPOR");
    $db->delete("ScripProbes");
    $db->delete("ScripRegister");
    $db->delete("ScripScore");
    $db->delete("ScripStudentRecord");
    $db->delete("ScripVerify");
    $db->delete("ScripVerifyDrug");
    $db->delete("ScripLabel");
*/
    $db->delete("ScripStudentRecord", $condition);


    print("<tr><td bgcolor=#FFFFFF>");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
    print ("</table>");
}

function X5A_DeleteDiscussion () {
// Delete a student's discussion with a prescriber
    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>");
    print("<p>This function allows a user to start ");
    print("discussing a script again - ");
    print("no discussions are kept</p>");
    print("Choose:<ul>");
    print("<li>Student; and</li>");
    print("<li>Case</li>");
    print("</ul>");
    print(" Finally click the submit button.</font>");
    print("</td></tr>\n");
    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>\n");

    print("<form action=scripadmin.php method=post>\n");
    print("<input type=hidden name=tab value=941>");

    print("<tr>\n");
    print("<td><font color=#ffffff>\n");

    print("<select name=student>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    // Sort by alphabetical order ...
    $sort = "LastName";
    $condition = ""; // Need to occupy space in list of arguments ...
    $table=$db->select("ScripStudent", $condition, $sort);
    foreach ($table as $row) {
        $sid = $row['StudentID'];
        $fn = $row['FirstName'];
        $ln = $row['LastName'];
        print("<option value='$sid'>$ln, $fn\n");
    }
    print("</select>\n");
    print("</font></td>\n");

    print("<td><font color=#ffffff>\n");
    print("<input type=text name=case>Case name (e.g. C4)");
    print("</font></td>\n");

    print("</tr>\n");

    print("<tr>");
    print("<td colspan=3 rowspan=2 align=center>");
    print("<input type=submit></td>");
    print("</tr>\n");
    print("</form>\n");

    print ("</table>\n");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
}

function X5B_ImplementDeleteDiscussion ($student, $case) {
// Set a student's scrips manually
    Z_DrawHeader();

    print("<tr><td colspan='2' align='center'>");
    print("<table>\n");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $condition = array("StudentID"=>$student, "CaseID"=>$case);

    $db->delete("ScripCombinationsDiscussed", $condition);

    print("<tr><td bgcolor=#FFFFFF>");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
    print ("</table>");
};

function Y1_Marking () {
// Choose the week (e.g. 3) for which marks are to be gathered
    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>Select one type of week,");
    print(" and then the week.</font>");
    print("</td></tr>\n");
    print("<tr><td colspan='2' align='center'>");
    print("<table border='1'>\n");

    print("<form action=scripadmin.php method=post>\n");
    print("<input type=hidden name=tab value=602>");

    print ("<tr><td colspan='3'><font color=#ffffff>");
    print("Choose one week from a drop-down list</td></tr>");


    print("<tr>");
    print("<td><input type=radio name=PER value=P checked>");
    print("<font color=#ffffff>Practice</font></td>");

    print("<td><input type=radio name=PER value=E>");
    print("<font color=#ffffff>Examination</font></td>");

    print("<td><input type=radio name=PER value=R>");
    print("<font color=#ffffff>Re-Sit</font></td>");
    print("</tr>");

    print("<tr><td align=center>");
    print("<select name=weekP>");
    for ($i = 0; $i < 7+1; $i++) {
        print("<option value='$i'>");
        if ($i > 0) print("$i");
    }
    print("</td>");

    print("<td align=center>");
    print("<select name=weekE>");
    for ($i = 0; $i < 3+1; $i++) {
        print("<option value='$i'>");
        if ($i > 0) print("$i");
    }
    print("</td>");

    print("<td>&nbsp;</td>");
    print("</tr>");

    print("<tr><td colspan='3'>&nbsp;</td></tr>");

    print("<tr><td colspan='3'><font color=#ffffff>");
    print("Choose a name or ALL STUDENTS</td></tr>");

    print("<tr><td colspan='3'>");
    print("<select name=student>");
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    print("<option value='0'>ALL STUDENTS\n");
    // Sort by alphabetical order ...
    $sort = "LastName";
    $condition = ""; // Need to occupy space in list of arguments ...
    $table=$db->select("ScripStudent", $condition, $sort);
    foreach ($table as $row) {
        $sid = $row['StudentID'];
        $fn = $row['FirstName'];
        $ln = $row['LastName'];
        print("<option value='$sid'>$ln, $fn\n");
    }
    print("</select>\n");
    print("</font></td>\n");

    print("<tr><td colspan='3'>&nbsp;</td></tr>");
    print("<tr><td colspan='3'>&nbsp;</td></tr>");
    print("<tr><td colspan='3'>&nbsp;</td></tr>");
    print("<tr><td colspan='3'>&nbsp;</td></tr>");

    print("<tr>");
    print("<td align=center colspan=3><input type=submit></td></tr>");

    print("</form>");
    print ("</table>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
}

function Y1A_ExecuteMarks ($PER, $week) {
// Gather the required marks
/* Administrator chooses a week (e.g. 4)
   Now look in ScripTermPatterns to find what letters occur in
   this week. For each term pattern, see what students have 
   that pattern ...
*/

/*
Syntax for writing:
    $name = "JCH";
    $thefile="Download_Results/".$name.".results"; 
    $fpout=fopen($thefile,"a");
    if (fpout) { fwrite($fpout, $message); } else { print("Error writing"); };
*/

//    $name = "JCH";
//    $thefile="Download_Results/".$name.".results"; 
    $thefile="users/practice1/JCH.csv"; 
    $fpout=fopen($thefile,"w");

    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>");
    print("</font>");
    print("</td></tr>\n");

    if ($RR == "Release") {$release = 1;} else {$release = 0;};

    // Unset some variable ...
    unset ($total, $letter);

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Readings of columns of CSV files ...
    $implode = array ("Name", "Case", "Marks", "Maximum Marks", "OTC?", "Communications", "Maximum Communication");
    $line = implode(",", $implode)."\n";

    $table=$db->select("ScripTermPattern");
    // Loop over each line in ScripTermPattern:
    // e.g. 3 A E F G B C D
    foreach ($table as $row) {
        $practiceCode = $row['PracticeCode'];
        // Read the the week field administrator has quoted ...
        $thisWeeksLetter = $row["wk".$week];
        // ... to retrieve the letter 

        // Now find students with this practice code
        // and by implication the week just deduced ...
        $condition = array("PracticeCode"=>$practiceCode);
        $stable=$db->select("ScripStudent", $condition); 
        foreach ($stable as $srow) {
            $sid = $srow['StudentID'];

            // Now look for every case with $thisWeeksLetter
            // from this student ...
            $condition="CaseID LIKE '".$thisWeeksLetter."%' and
                                  StudentID=$sid";
            $qtable=$db->select("ScripMarksModerated", $condition);

            // We now have a table with e.g. John Smith's C results ... 
            foreach ($qtable as $qrow) {
                $sid = $qrow['StudentID'];
                $wcondition = array("StudentID"=>$sid);

                $wtable=$db->select("ScripStudent", $wcondition);
                foreach ($wtable as $wrow) {
                    $wholename = $wrow['LastName']." - ".$wrow['FirstName'];
                }
                if (!isset($namefield[$sid])) {$namefield[$sid] = $wholename;};

                // Set this week's letter for student sid ... 
                $caseid = $qrow['CaseID'];
                if (!isset($letter[$sid])) {$letter[$sid] = $caseid[1];};

                // Read ScripsScrips for counselling - Yes or No ...

                // Increment marks for student sid ...
                $marks = $qrow['Marks'];
                if (!isset($total[$sid])) {
                    $total[$sid] = 0;
                    $grandtotal[$sid] = 0;
                };
                $total[$sid] += $marks;
                $grandtotal[$sid] += $marks;

                $marksmaximum = $qrow['MarksMaximum'];
                $OTCYN = $qrow['OTCYN'];
                if ($OTCYN == 1) { $OTCYN="Y"; } else { $OTCYN="N"; };
                $commsmarks = $qrow['CommsMarks'];

                // If counselling is set ...
                // Set $commsfield [$sid] = $commsmarks;
                // Set $grandtotal [$sid] += $commsmarks;

                $commsmarksmaximum = $qrow['CommsMarksMaximum'];
                $implode = array ($wholename, $caseid, $marks, $marksmaximum,
                      $OTCYN, $commsmarks, $commsmarksmaximum);
                $line .= implode(",", $implode)."\n"; // ... add more text
            };
        }
    }


    fwrite($fpout, $line); // ... write to file

    print("<tr><td BGCOLOR=#FFFFFF>");
    print("<p><a href='$thefile'>Right click for results</a></p>");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
}


function Y2_MarkingWW(){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $nPracticeWeeks=7;
    $nExamWeeks=4;

    // Read ScripStudent - put the data into several arrays with the ID as the counter ...
    $table=$db->select("ScripStudent");
    foreach ($table as $row) {
        $ID=$row['StudentID'];
        $firstName[$ID]=$row['FirstName'];
        $lastName[$ID]=$row['LastName'];
        $userName[$ID]=$row['UserName'];
        $practiceCode[$ID]=$row['PracticeCode'];
        $examCode[$ID]=$row['ExamCode'];
    };

    // Read ScripTermPattern - put into 2D array ...
    $table=$db->select("ScripTermPattern");
    foreach ($table as $row) {
        $pC = $row['PracticeCode'];
        for ($i=0; $i<$nPracticeWeeks; $i++) {
            $pLetter[$pC][$i]=$row['wk'.($i+1)];
            // ... this is the letter for a given practice code and a given week
        };
    };

    // Read ScripExamPatterns - put into 2D array ...
    $table=$db->select("ScripExamPatterns");
    foreach ($table as $row) {
        $examCode=$row['ExamCode'];
        for ($i=0; $i<$nExamWeeks; $i++) {
            $eLetter[$examCode][$i]=$row['wk'.($i+1)];
        };
    };

    // Look at the moderated marks [from practice weeks], pick the marks up ...
    $table=$db->select("ScripMarksModerated");
    foreach ($table as $row) {
        $SID = $row['StudentID'];
        $CaseID = $row['CaseID'];
        $Marks = $row['Marks'];
        // Find out in which week the case was done ...
        // Practice code is $practiceCode[$SID] ...
        // Exam code is $examCode[$SID] ...
        // Need to search relevant code for letter to find week ...
        for ($i=0; $i < $nPracticeWeeks; $i++) {
            if ($pLetter[$practiceCode[$SID]][$i] == $CaseID[0].$CaseID[1]){
                $week = $i + 1; // This is the WEEK (i.e. 1, ..., 7)
            };
        };
        $marksThisWeek[$SID][$week-1] += $Marks;
        if ($week=='7') { print("Week 7<br>"); };
    };

    // Headings of columns of CSV files ...
    $implode = array ("Name", "","1", "2", "3", "4", "5", "6", "7");
    $line = implode(",", $implode)."\n";

    foreach ($lastName as $id=>$theLastName) {
        $cName=$firstName[$id];
        $line.="$theLastName - $cName, ".$userName[$id];
        for ($i=0; $i<$nPracticeWeeks=7; $i++){

            // If there's a mark, write it; if not, write a space ...
            if (isset($marksThisWeek[$id][$i])) {
                $line.=",".$marksThisWeek[$id][$i];
            } else {
                $line.=",";
            };

        };
        $line.="\n"; // ... end of line marker
    };

    $thefile="users/practice1/WW.csv"; 
    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    print("<tr><td BGCOLOR=#FFFFFF>");
    print("<p><a href='$thefile'>Right click for results</a></p>");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
};


function Y3_MarkingWWC_Pre(){

    // Collect information prior to Y3_MarkingWWC() ...
    // Only read the students who are required (now that ScripStudent contains students from different years):
    // Bring in year number and year date assume current year September 1st, N to August 31st, N+1 will be taken as year N ...

    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    print("<tr>");

    print("<td valign='top'><font color='#ffffff'>For which students do you want a report? Choose year-group and location:</font></td>");

    print("</tr>");

    print("<tr>");

    print("<td>&nbsp;</td>");

    print("</tr>");



    // Try to work out the academic year (from September one year to August the next)
    $thisYear = date("Y");
    $thisMonth = date("m");
    if ($thisMonth < 9){ $thisYear--; }; // ... January to August belong to the previous year

    // The contents of the drop-down list ...
    $startYear = $thisYear - 2; $endYear = $thisYear + 3;

    print("<form action='scripadmin.php' method='post'>");

    print("<input type='hidden' name='tab' value='711'>");

    // Need a set of all the different locations in ScripStudent ...
    $allLocations = array();
    $table=$db->select("ScripStudent");
    foreach ($table as $row){
        // Read an entry from the table ...
        $thisLocation = $row['Location'];
        $alreadyNoted = false;

        // Loop over contents of $allLocations ...
        for ($j = 0; $j < count($allLocations); $j++){
            if ($allLocations[$j] == $thisLocation){ $alreadyNoted = true; };
        };

        if (!$alreadyNoted) {
            // If not previously in $allLocations, add it now ...
            array_push($allLocations, $thisLocation);
        };
    };
    // ... all the locations are now in $allLocations

    print("<tr>");
    print("<td>");
    print("<table border='1' align='center'>");

    print("<tr><td><font color='#ffffff'></font></td><td><font color='#ffffff'>1st</font></td><td><font color='#ffffff'>2nd</font></td><td><font color='#ffffff'>3rd</font></td><td><font color='#ffffff'>4th</font></td></tr>");

    for ($i = 0; $i < count($allLocations); $i++){

        print("\n<tr>");
        // Row headings are the locations ...
        print("\n<td><font color='#ffffff'>$allLocations[$i]</font></td>");

        // Now the body of each row (all tick boxes) ...
        for ($kk = 0; $kk < 4; $kk++){
            $kkp1 = $kk + 1;
            print("\n<td><input type='checkbox' name='Year[$kkp1][]' value='$allLocations[$i]'></td>");
        };

//        print("\n<td><input type='checkbox' name='Year[1][]' value='$allLocations[$i]'></td>");
//        print("\n<td><input type='checkbox' name='Year[2][]' value='$allLocations[$i]'></td>");
//        print("\n<td><input type='checkbox' name='Year[3][]' value='$allLocations[$i]'></td>");
//        print("\n<td><input type='checkbox' name='Year[4][]' value='$allLocations[$i]'></td>");

        print("\n</tr>");

    };

    print("</table>");
    print("</td>");
    print("</tr>");


/*
    print("<tr>");

    print("<td>");
    print("<font color='#ffffff'>Choose year of students (e.g. 1st, 2nd): </font>");

    print("<select name='year'>");
    print("<option value='1'>1</option>");
    print("<option value='2'>2</option>");
    print("<option value='3'>3</option>");
    print("<option value='4'>4</option>");
    print("</select>");
    print("<br><br>");
    print("</td>");
    print("</tr>");
*/

    print("<tr>");
    print("<td>");
    print("<br><br><font color='#ffffff'>Choose this academic year: </font>");

    print("<select name='academicyear'>");
    for ($x = $startYear; $x < $endYear; $x++){
        $xp1 = $x + 1;
        print( "<option value='" . $x . "'");
        if ($x == $thisYear) { print(" selected='selected'"); };
        print(">" . $x . "-" . $xp1 . "</option>");
    };

    print("</select>");
    print("<br><br>");
    print("<font color='#ffffff'>Choose type of mark (DO NOT USE YET): </font>");
    print("<font color='#ffffff'><input type='radio' name='typeofmarks' value='P' checked='checked'>Practice</font>");
    print("<font color='#ffffff'><input type='radio' name='typeofmarks' value='E'>Exam</font>");
    print("<br><br>");
    print("</td>");
    print("</tr>");

/*
    print("<tr>");

    print("<td>");
    print("<font color='#ffffff'>Choose location of students: </font>");

    print("<select name='location'>");

    for ($k = 0; $k < count($allLocations); $k++){
        print("<option value='" . $allLocations[$k] . "'>$allLocations[$k]</option>");
    };

    print("</select>");
    print("<br><br>");
    print("</td>");
    print("</tr>");
*/
    print("<tr><td align=center colspan=3><input type='submit' value='Compile Report'></td></tr>");

    print("</form>");

    print("</td></tr>");

};



//function Y3_MarkingWWC ($year, $academicYear, $location){

function Y3_MarkingWWC ($academicYear, $forReportYear, $forReportLocation, $typeOfMarks) {

// MANUAL OVER-RIDE

//    $typeOfMarks = "P";

    // Practice weeks (see Y4_MarkingWWC for exam weeks) ...

    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $nPracticeWeeks=7;
    $nExamWeeks=4;


    // Version with check-boxes (February 2013):
    // Read all students from ScripStudents

    // Loop through them all and check the YearGroup and Location of each against everything $year (also the academic year). Where there
    // is a match, write the details away to $firstName, etc. as below


    // Look for real students in the correct academic year ...
    $condition = array ('RealStudent'=>1, 'LastUpdated'=>$academicYear);
    $table=$db->select("ScripStudent", $condition);


    // Loop over students and only include those in yeargroup-location combinations that occur in the related 1-D arrays $forReportYear and $forReportLocation ...
    foreach ($table as $row) {

        // Loop over allowed combinations (each allowed combination is defined by a pair of entries in $forReportYear and $forReportLocation) ...
        for ($j = 0; $j < count($forReportLocation); $j++){
            if ($row['YearGroup'] == $forReportYear[$j] && $row['Location'] == $forReportLocation[$j]){
                // Match!!
                $ID=$row['StudentID'];
                $firstName[$ID]=$row['FirstName'];
                $lastName[$ID]=$row['LastName'];
                $userName[$ID]=$row['UserName'];
                $saturn[$ID]=$row['StudentSaturnNumber'];
                $practiceCode[$ID]=$row['PracticeCode'];
                $examCode[$ID]=$row['ExamCode'];
            };
        };
    };

//print("ONE ONE ONE ONE ONE ONE ONE ONE ");
//print("<pre>");print_r($examCode);print("</pre>");

    // ... now carry on as previously

/*
    $table=$db->select("ScripStudent");
    // Loop over students ...
    foreach ($table as $row) {

        // Loop over first element of $year (which is the yearnumber 1 to 4 inclusive) ...
        for ($yearNumber = 1; $yearNumber < 4 + 1; $yearNumber++){

            // Loop over the locations in the year            
            for ($tempLocation = 0; $tempLocation < $year[$yearNumber]; $tempLocation++){
                if ($yearNumber == $row['YearGroup'] && $year[$yearNumber][$tempLocation] == $row['Location'] && $row['LastUpdated'] == $academicYear){
                    // We have a match! Record the details
                    $ID=$row['StudentID'];
                    $firstName[$ID]=$row['FirstName'];
                    $lastName[$ID]=$row['LastName'];
                    $userName[$ID]=$row['UserName'];
                    $saturn[$ID]=$row['StudentSaturnNumber'];
                    $practiceCode[$ID]=$row['PracticeCode'];
                    $examCode[$ID]=$row['ExamCode'];
                };
            };    
        };
    };
*/





/*
    // Only read the students who are required (now that ScripStudent contains students from different years):
    // Bring in year number and year date assume current year September 1st, N to August 31st, N+1 will be taken as year N ...

    // Read ScripStudent for real student in correct year and academic year - put into 2D array ...
    $condition = array ('RealStudent'=>1, 'YearGroup'=>$year, 'LastUpdated'=>$academicYear, 'Location'=>$location);
    $table=$db->select("ScripStudent", $condition);

    foreach ($table as $row) {
//        if ($row['RealStudent'] && $row['YearGroup'] == $year && $row['LastUpdated'] == $academicYear) { // ... only real students
            $ID=$row['StudentID'];
            $firstName[$ID]=$row['FirstName'];
            $lastName[$ID]=$row['LastName'];
            $userName[$ID]=$row['UserName'];
            $saturn[$ID]=$row['StudentSaturnNumber'];
            $practiceCode[$ID]=$row['PracticeCode'];
            $examCode[$ID]=$row['ExamCode'];
//        };
    };

*/


    // Read ScripTermPattern - put into 2D array ...
    $table=$db->select("ScripTermPattern");
    foreach ($table as $row) {
        $pC=$row['PracticeCode'];
        for ($i=0; $i<$nPracticeWeeks; $i++) {
            $pLetter[$pC][$i]=$row['wk'.($i+1)];
        };
    };

/*
    // Old version - variable clash with $examCode - it's used for something else earlier on (and must be retained) so shouldn't be reused here  
    // Read ScripExamPatterns - put into 2D array ...
    $table=$db->select("ScripExamPatterns");
    foreach ($table as $row) {
        $examCode=$row['ExamCode'];
        for ($i=0; $i<$nExamWeeks; $i++) {
            $eLetter[$examCode][$i]=$row['wk'.($i+1)];
        };
    };
*/



    // Read ScripExamPatterns - put into a 2D array too ...
    $table=$db->select("ScripExamPatterns");
    foreach ($table as $row) {
        $eC=$row['ExamCode']; // ExamCode is the primary key for this table
        for ($i=0; $i<$nExamWeeks; $i++) {
            $eLetter[$eC][$i]=$row['wk'.($i+1)];
            $weekNumber[$eC][$row['wk'.($i+1)]] = $i;
        };
    };



    if ($typeOfMarks == "E"){
//        Y4_MarkingWWC_Reduced();


        $table=$db->select("ScripMarksExamModerated");


//print("<br>--------------------");
//print_r($examCode);
        foreach ($table as $row) {

            $SID = $row['StudentID'];
            if (isset($lastName[$SID])) {
//print("$SID ".$lastName[$SID]." ");
                $CaseID = $row['CaseID'];

                // Find the counselling type ...
                $Scondition=array("Letter"=>$CaseID);
                $Stable=$db->select("ScripsScrips", $Scondition);
                foreach ($Stable as $Srow) {
                    $Counsel=$Srow['Counsel'];
                };

                $Marks = $row['Marks'];
                $CommsMarks = $row['CommsMarks'];

//print("$Marks $CommsMarks<br />");

                // Find out in which week the case was done: practice code is $practiceCode[$SID] and exam code is $examCode[$SID] ...
                // Need to search relevant code for letter to find week ...
                for ($i=0; $i < $nExamWeeks; $i++) {

//print("User " . $SID . " -- ");
//print("who has code " . $examCode[$SID] . " -- ");
//print("in week " . $i . " -- ");
//print("does letter " . $eLetter[$examCode[$SID]][$i] . " -- ");
//print(" looks for match " . $CaseID[0].$CaseID[1]);
//print("<br>");
                    if ($eLetter[$examCode[$SID]][$i] == $CaseID[0].$CaseID[1]){
                        $week=$i+1; // This is the WEEK (i.e. 1, ..., 7)
                    };
                };


// AT A SAFE TIME, the following two parts of code ought to be put inside the above IF and tested 

//print ("Week " . $week . "<br>");

                // Scrip scores ...
                $marksThisWeek[$SID][$week-1] += $Marks;
//            if ($week=='7') { print("Student $SID has $Marks<br>"); };

                // Communications scores ...
                if ($Counsel==2 || $Counsel==3) {
                    $commsMarksThisWeek[$SID][$week-1] += $CommsMarks;
                };
            };
         }; 

        // Having looped over the moderated marks for ordinary prescriptions, we now loop over the moderated marks for the calculations ...
    
        $calculationTotal = array ();
        $table=$db->select("ScripCalculationsAnswersExamModerated");
        foreach ($table as $row){
            // Loop over the contents: start with the student ID ...
            $SID = $row['StudentID'];

            if (isset($lastName[$SID])) { // ... check it's set
                // Read the case ID ...
                $CaseID = $row['CaseID'];
            
                // Read the penalty ...
                $penalty = $row['Penalty'];

                // Add in the mark (which is one minus the penalty), we could do this as
                //               $calculationTotal[$SID][$CaseID] += 1 - $penalty
                // but we want the weekNumber (starting at zero i.e. weekNumber - 1) as the variable (and not the case ID) ...

                for ($i=0; $i < $nExamWeeks; $i++) {
                    if ($eLetter[$examCode[$SID]][$i] ==$CaseID[0].$CaseID[1]){
                        $week=$i+1; // This is the WEEK (i.e. 1, ..., 7)
                    };
                };
                $calculationTotal[$SID][$week-1] += 1 - $penalty;
            };        
        };


        // Headings of columns of CSV files ...
        $implode = array ("Name", "Username", "Saturn", "RX1", "x1", "C1", "RX2", "x2", "C2", "RX3", "x3", "C3");
        $line = implode(",", $implode)."\n";
        $linec = implode(",", $implode)."\n"; // "c" ending for communications ...

        foreach ($lastName as $id=>$theLastName) {
            $cName=$firstName[$id];
            $line.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
            $linec.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
            for ($i=0; $i<$nExamWeeks; $i++){

                // This is the joint version i.e. script and counselling together. If there's a mark, write it; if not, write a space ...
                if (isset($marksThisWeek[$id][$i])) {
                    $line.=",".$marksThisWeek[$id][$i];
                } else {
                    $line.=",";
                };

                if (isset($calculationTotal[$id][$i])) {
                    $line.=",".$calculationTotal[$id][$i];
                } else {
                    $line.=",";
                };

                if (isset($commsMarksThisWeek[$id][$i])) {
                    $line.=",".$commsMarksThisWeek[$id][$i];
                } else {
                    $line.=",";
                };

// THE FOLLOWING CODE BECOMES REDUNDANT WHEN RX AND COUNSELLING ARE MERGED INTO ONE FILE


            // If there's a mark, write it; if not, write a space ...
//            if (isset($marksThisWeek[$id][$i])) {
//                $line.=",".$marksThisWeek[$id][$i];
//            } else {
//                $line.=",";
//            };

            // If there's a mark, write it; if not, write a space ...
 //           if (isset($commsMarksThisWeek[$id][$i])) {
 //               $linec.=",".$commsMarksThisWeek[$id][$i];
//            } else {
//                $linec.=",";
//            };

            };

            $line.="\n"; // ... end of line marker
            $linec.="\n"; // ... end of line marker
        };


//print($line." </br>");

        $thefile="users/practice1/WW.csv"; 
        $fpout=fopen($thefile,"w"); // WRITE access

        fwrite($fpout, $line); // ... write to file

        $thefilec="users/practice1/WWC.csv"; 
        $fpoutc=fopen($thefilec,"w"); // WRITE access

        fwrite($fpoutc, $linec); // ... write to file

        print("<tr><td>");
        print("<p><font color=#ffffff>This may take a moment or two ... watch the cursor.</font></p>");
        print("<p><font color=#ffffff>Right click the links below to download the named files.");
        print(" From the context menus choose Save Target As ...</font></p>");

        print("<p><font color=#ffffff><ul>");
        print("<li><a href='$thefile'>Prescription marks</a></li>");
        print("<li><a href='$thefilec'>Communication marks</a></li>");
        print("</ul></font></p>");

        print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
        print("</td></tr>");
    };

    if ($typeOfMarks == "P"){

        $table=$db->select("ScripMarksModerated");
        foreach ($table as $row) {
            $SID = $row['StudentID'];
            if (isset($lastName[$SID])) {
                $CaseID = $row['CaseID'];

                $Scondition=array("Letter"=>$CaseID);
                $Stable=$db->select("ScripsScrips", $Scondition);
                foreach ($Stable as $Srow) {
                    $Counsel=$Srow['Counsel'];
                };

                $Marks = $row['Marks'];
                $CommsMarks = $row['CommsMarks'];
                // Find out in which week the case was done: Practice code is $practiceCode[$SID]; Exam code is $examCode[$SID] ...
                // Need to search relevant code for letter to find week ...
                for ($i=0; $i < $nPracticeWeeks; $i++) {
                    // The first two letters are the week ...
                    if ($pLetter[$practiceCode[$SID]][$i] == $CaseID[0].$CaseID[1]) {
                        $week=$i+1;
                        // This is the WEEK (i.e. 1, ..., 7)
                    };
                };

                // Scrip scores ...
                $marksThisWeek[$SID][$week-1] += $Marks;

                // Communications scores ...
                if ($Counsel==2 || $Counsel==3) {
                    $commsMarksThisWeek[$SID][$week-1] += $CommsMarks;
                };
            };
        };

       // Having looped over the moderated marks for ordinary prescriptions, we now loop over the moderated marks for the calculations ...
    
        $calculationTotal = array ();
        $table=$db->select("ScripCalculationsAnswersModerated");
        foreach ($table as $row){
            // Loop over the contents of ScripCalculationsAnswersModerated line by line: start with the student ID ...
            $SID = $row['StudentID'];

            if (isset($lastName[$SID])) { // ... check that it is set
                // Read the case ID ...
                $CaseID = $row['CaseID'];
            
                // Read the penalty ...
                $penalty = $row['Penalty'];

                // Add in the mark (which is one minus the penalty), we could do this as
                //               $calculationTotal[$SID][$CaseID] += 1 - $penalty
                // but we want the weekNumber (starting at zero i.e. weekNumber - 1) as the variable (and not the case ID) so we have to find the week number ...

                for ($i=0; $i < $nPracticeWeeks; $i++) {
                    // The first two letters are the week ...
                    if ($pLetter[$practiceCode[$SID]][$i] == $CaseID[0].$CaseID[1]) {
                        $week = $i + 1;
                        // This is the WEEK (i.e. 1, ..., 7)
                    };
                };
                $calculationTotal[$SID][$week-1] += 1 - $penalty;
            };        
        };

        // Headings of columns of CSV files ...
        $implode = array ("Name", "","Saturn", "E1", "x1", "C1", "E2", "x2", "C2", "E3", "x3", "C3", "E4", "x4", "C4", "E5", "x5", "C5", "E6", "x6", "C6", "E7", "x7", "C7");
        $line = implode(",", $implode)."\n";
        $linec = implode(",", $implode)."\n";

        foreach ($lastName as $id=>$theLastName) {
            $cName=$firstName[$id];
            $line.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
            $linec.="$theLastName - $cName, ".$userName[$id];
            for ($i=0; $i<$nPracticeWeeks=7; $i++){

                // This is the joint version i.e. script and counselling together
                // If there's a mark, write it; if not, write a space ...
                if (isset($marksThisWeek[$id][$i])) {
                    $line.=",".$marksThisWeek[$id][$i];
                } else {
                    $line.=",";
                };

                if (isset($calculationTotal[$id][$i])) {
                    $line.=",".$calculationTotal[$id][$i];
                } else {
                    $line.=",";
                };

                if (isset($commsMarksThisWeek[$id][$i])) {
                    $line.=",".$commsMarksThisWeek[$id][$i];
                } else {
                    $line.=",";
                };

// THE FOLLOWING CODE BECOMES REDUNDANT WHEN RX AND COUNSELLING ARE MERGED INTO ONE FILE


            // If there's a mark, write it; if not, write a space ...
//            if (isset($marksThisWeek[$id][$i])) {
//                $line.=",".$marksThisWeek[$id][$i];
//            } else {
//                $line.=",";
//            };

            // If there's a mark, write it; if not, write a space ...
//            if (isset($commsMarksThisWeek[$id][$i])) {
//                $linec.=",".$commsMarksThisWeek[$id][$i];
//            } else {
//                $linec.=",";
//            };

            };
            $line.="\n"; // ... end of line marker
            $linec.="\n"; // ... end of line marker
        };

    // Construct the file name ...
    $thefile="users/practice1/WeekByWeek-AY-" .$academicYear . ".csv"; 

    // If file already exists, delete it ...
    if (is_file($thefile)) { unlink($thefile); };

    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    $thefilec="users/practice1/WWC.csv"; 
    $fpoutc=fopen($thefilec,"w"); // WRITE access

    fwrite($fpoutc, $linec); // ... write to file

    $academicYearp1 = $academicYear + 1;
    print("<tr><td>");
    print("<p><font color=#ffffff>Students in academic year <b>" . $academicYear . "-" . $academicYearp1 . "</b>:</font></p>");

    print("<ul>");
    for ($i = 0; $i < count($forReportLocation); $i++){
        print("<font color='#ffffff'><li>Year " . $forReportYear[$i] . " at " . $forReportLocation[$i] . "</li></font>");
    };
    print("</ul>");

    print("<p><font color='#ffffff'>This may take a moment or two ... watch the cursor.</font></p>");
    print("<p><font color=#ffffff>Right click the links below to download the named files.");
    print(" From the context menus choose Save Target As ...</font></p>");

    print("<p><font color=#ffffff><ul>");
    print("<li><a href='$thefile'>Prescription marks</a></li>");
    print("<li><a href='$thefilec'>Communication marks</a></li>");
    print("</ul></font></p>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
};
};


function Y4_MarkingWWC_Reduced(){
    // Exam weeks (see Y3_MarkingWWC for practice weeks) ...


    // This is the reduced form of the function (to be called from Y3)


    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $nPracticeWeeks=7;
    $nExamWeeks=3;


    // Read ScripStudent - put the contents into a 2D array ...
    $table=$db->select("ScripStudent");
    foreach ($table as $row) {
        if ($row['RealStudent']) {
            $ID=$row['StudentID'];
            $firstName[$ID]=$row['FirstName'];
            $lastName[$ID]=$row['LastName'];
            $userName[$ID]=$row['UserName'];
            $saturn[$ID]=$row['StudentSaturnNumber'];
            $practiceCode[$ID]=$row['PracticeCode'];
            $examCode[$ID]=$row['ExamCode'];
        };
    };

    // Read ScripTermPattern - also put into a 2D array ...
    $table=$db->select("ScripTermPattern");
    foreach ($table as $row) {
        $pC=$row['PracticeCode'];
        for ($i=0; $i<$nPracticeWeeks; $i++) {
            $pLetter[$pC][$i]=$row['wk'.($i+1)];
        };
    };

    // Read ScripExamPatterns - put into a 2D array too ...
    $table=$db->select("ScripExamPatterns");
    foreach ($table as $row) {
        $eC=$row['ExamCode']; // ExamCode is the primary key for this table
        for ($i=0; $i<$nExamWeeks; $i++) {
            $eLetter[$eC][$i]=$row['wk'.($i+1)];
            $weekNumber[$eC][$row['wk'.($i+1)]] = $i;
        };
    };


    $table=$db->select("ScripMarksExamModerated");

    foreach ($table as $row) {

        $SID = $row['StudentID'];
        if (isset($lastName[$SID])) {
//print("$SID ".$lastName[$SID]." ");
            $CaseID = $row['CaseID'];

            // Find the counselling type ...
            $Scondition=array("Letter"=>$CaseID);
            $Stable=$db->select("ScripsScrips", $Scondition);
            foreach ($Stable as $Srow) {
                $Counsel=$Srow['Counsel'];
            };

            $Marks = $row['Marks'];
            $CommsMarks = $row['CommsMarks'];

//print("$Marks $CommsMarks<br />");

            // Find out in which week the case was done ...
            // Practice code is $practiceCode[$SID] ...
            // Exam code is $examCode[$SID] ...
            // Need to search relevant code for letter to find week ...
            for ($i=0; $i < $nExamWeeks; $i++) {
                if ($eLetter[$examCode[$SID]][$i] ==$CaseID[0].$CaseID[1]){
                    $week=$i+1; // This is the WEEK (i.e. 1, ..., 7)
                };
            };


// AT A SAFE TIME, the following two parts of code ought to be put inside the above IF and tested 


            // Scrip scores ...
            $marksThisWeek[$SID][$week-1] += $Marks;
//            if ($week=='7') { print("Student $SID has $Marks<br>"); };

            // Communications scores ...
            if ($Counsel==2 || $Counsel==3) {
                $commsMarksThisWeek[$SID][$week-1] += $CommsMarks;
            };
        };
    };
    // Having looped over the moderated marks for ordinary prescriptions, we now loop over the moderated marks 
    // for the calculations ...
    
    $calculationTotal = array ();
    $table=$db->select("ScripCalculationsAnswersExamModerated");
    foreach ($table as $row){
        // Loop over the contents: start with the student ID ...
        $SID = $row['StudentID'];

        if (isset($lastName[$SID])) { // ... check it's set
            // Read the case ID ...
            $CaseID = $row['CaseID'];
            
            // Read the penalty ...
            $penalty = $row['Penalty'];

            // Add in the mark (which is one minus the penalty), we could do this as
            //               $calculationTotal[$SID][$CaseID] += 1 - $penalty
            // but we want the weekNumber (starting at zero i.e. weekNumber - 1) as the variable (and not the case ID) ...

            for ($i=0; $i < $nExamWeeks; $i++) {
                if ($eLetter[$examCode[$SID]][$i] ==$CaseID[0].$CaseID[1]){
                    $week=$i+1; // This is the WEEK (i.e. 1, ..., 7)
                };
            };
            $calculationTotal[$SID][$week-1] += 1 - $penalty;
        };        
    };


    // Headings of columns of CSV files ...
    $implode = array ("Name", "Username", "Saturn", "RX1", "x1", "C1", "RX2", "x2", "C2", "RX3", "x3", "C3");
    $line = implode(",", $implode)."\n";
    $linec = implode(",", $implode)."\n"; // "c" ending for communications ...

    foreach ($lastName as $id=>$theLastName) {
        $cName=$firstName[$id];
        $line.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
        $linec.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
        for ($i=0; $i<$nExamWeeks; $i++){


            // This is the joint version i.e. script and counselling together
            // If there's a mark, write it; if not, write a space ...
            if (isset($marksThisWeek[$id][$i])) {
                $line.=",".$marksThisWeek[$id][$i];
            } else {
                $line.=",";
            };

            if (isset($calculationTotal[$id][$i])) {
                $line.=",".$calculationTotal[$id][$i];
            } else {
                $line.=",";
            };

            if (isset($commsMarksThisWeek[$id][$i])) {
                $line.=",".$commsMarksThisWeek[$id][$i];
            } else {
                $line.=",";
            };

// THE FOLLOWING CODE BECOMES REDUNDANT WHEN RX AND COUNSELLING ARE MERGED INTO ONE FILE


            // If there's a mark, write it; if not, write a space ...
//            if (isset($marksThisWeek[$id][$i])) {
//                $line.=",".$marksThisWeek[$id][$i];
//            } else {
//                $line.=",";
//            };

            // If there's a mark, write it; if not, write a space ...
 //           if (isset($commsMarksThisWeek[$id][$i])) {
 //               $linec.=",".$commsMarksThisWeek[$id][$i];
//            } else {
//                $linec.=",";
//            };

        };

        $line.="\n"; // ... end of line marker
        $linec.="\n"; // ... end of line marker
    };


//print($line." </br>");

    $thefile="users/practice1/WW.csv"; 
    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    $thefilec="users/practice1/WWC.csv"; 
    $fpoutc=fopen($thefilec,"w"); // WRITE access

    fwrite($fpoutc, $linec); // ... write to file

    print("<tr><td>");
    print("<p><font color=#ffffff>This may take a moment or two ...");
    print(" watch the cursor.</font></p>");
    print("<p><font color=#ffffff>Right click the links below to");
    print(" download the named files.");
    print(" From the");
    print(" context menus choose Save Target As ...</font></p>");

    print("<p><font color=#ffffff><ul>");
    print("<li><a href='$thefile'>Prescription marks</a></li>");
    print("<li><a href='$thefilec'>Communication marks</a></li>");
    print("</ul></font></p>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
};





function Y3_1_AllAnswers(){

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    Z_DrawHeader();


    // Read all the questions and offer them to the user
    $table=$db->select("ScripCalculationsQuestions");
    foreach ($table as $row){
        $exerciseSet[] = $row['ExerciseSet'];
        $exerciseNumber[] = $row['ExerciseNumber'];
        $questionNumber[] = $row['QuestionNumber'];
        $questionID[] = $row['ID'];
        $questionText[] = $row['QuestionText'];
    };

    // Sort on ExerciseSet and then QuestionNumber

    // We would like to sort regardless of case, first on last name and then on first name ...

    $twoLetterCode_uppercase = array_map('strtoupper', $exerciseSet);
    array_multisort($twoLetterCode_uppercase, SORT_STRING, SORT_ASC, $exerciseNumber, SORT_NUMERIC, $questionNumber, SORT_NUMERIC, $questionText, $questionID); 

    // Then show ExerciseSet, QuestionNumber, QuestionID (in brackets) and the question text with a radio button (QuestionID is the parameter)
    // Choosing a question leads to Y3_2_AllAnswers()

    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='997'>");

    print("<tr><td colspan='5'><font color='#ffffff'>Headings: Exercise � The two-letter code; Number � Always 1 at present; QN � Question number within exercise (from 1 to 10); Serial � Internal serial  number only</font></td></tr>");

    print("<tr><td>");
    print("<table>");

    print("<tr>");
    print("<td><font color='#ffffff'>&nbsp;</font></td>");
    print("<td><font color='#ffffff'>Exercise</font></td>");
    print("<td><font color='#ffffff'>Number</font></td>");
    print("<td><font color='#ffffff'>QN</font></td>");
    print("<td><font color='#ffffff'>Serial</font></td>");
    print("<td><font color='#ffffff'>Text</font></td>");
    print("<tr>");

    for ($i = 0; $i < count($exerciseSet); $i++){
        print("<tr>");
//        print("<td valign='top'><input type='radio' name='questionid' value='$questionID[$i]'></td>");
        print("<td valign='top'><input type='checkbox' name='questionid[]' value='$questionID[$i]'></td>");
        print("<td valign='top'><font color='#ffffff'>$exerciseSet[$i]</font></td>");
        print("<td valign='top'><font color='#ffffff'>$exerciseNumber[$i]</font></td>");
        print("<td valign='top'><font color='#ffffff'>$questionNumber[$i]</font></td>");
        print("<td valign='top'><font color='#ffffff'>[$questionID[$i]]</font></td>");
        print("<td valign='top'><font color='#ffffff'>$questionText[$i]</font></td>");
        print("</tr>");
    };
    print("</table>");

    print("<input type='submit' value='Confirm'>");

    print("</form>");

    print("</td></tr>");

};

function Y3_2_AllAnswers($questionID){

    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    for ($k = 0; $k < count($questionID); $k++){

        // For answers that are CORRECT ...
        $correctAnswerText = array();
//        $condition=array("QuestionID"=>$questionID, "Penalty"=>0);
        $condition=array("QuestionID"=>$questionID[$k], "Penalty"=>0);
        $table=$db->select("ScripCalculationsAnswersModerated", $condition);
        foreach ($table as $row) {
            // Look for the student's username ...
            $studentID = $row['StudentID'];
            $condition=array("StudentID"=>$studentID);
            $tableStudent=$db->select("ScripStudent", $condition);
            foreach ($tableStudent as $rowStudent){
                if ($rowStudent['RealStudent']){
                    if (!isset($correctAnswerText[$row['StudentAnswer']])){
                        $correctAnswerText[$row['StudentAnswer']] = 0;
                    };
                    $correctAnswerText[$row['StudentAnswer']] += 1;
                };
            };
        };

    // For answers that are INCORRECT ...
    $wrongAnswerText = array();
    $condition=array("QuestionID"=>$questionID[$k], "Penalty"=>1);
    $table=$db->select("ScripCalculationsAnswersModerated", $condition);
    foreach ($table as $row) {
        // Look for the student's username ...
        $studentID = $row['StudentID'];
        $condition=array("StudentID"=>$studentID);
        $tableStudent=$db->select("ScripStudent", $condition);
        foreach ($tableStudent as $rowStudent){
            if ($rowStudent['RealStudent']){
                if (!isset($wrongAnswerText[$row['StudentAnswer']])){
                    $wrongAnswerText[$row['StudentAnswer']] = 0;
                };
                $wrongAnswerText[$row['StudentAnswer']] += 1;
            };
        }; 
    };

    // Blank line between different questions (if question is no the very first) ...
    if ($k != 0){ // ... blank row for separation
        print("<tr><td>&nbsp;</td></tr>");        
    };

    // Open a row and begin a table ...
    print("<tr><td>");
    print("<table border='1'>");

    // Question text ...
    $condition=array("ID"=>$questionID[$k]);
    $table=$db->select("ScripCalculationsQuestions", $condition);
    foreach ($table as $row){
        print("<tr><td colspan='2'><font color='#ffffff'><i>Question Set:</i> <b>" . $row['ExerciseSet'] . "</b> <i>Exercise Number:</i> " . $row['ExerciseNumber'] . " <i>Question Number:</i> <b>" . $row['QuestionNumber'] . "</b> [<i>Internal ID:</i> " . $questionID[$k] . "]</font></td></tr>");
        print("<tr><td colspan='2'><font color='#ffffff'><i>Question:</i> " . $row['QuestionText'] . "</font></td></tr>");
    };

    // Model answers ...
    $firstAnswer = true;
    print("<tr><td colspan='2'><font color='#ffffff'>");
    $condition=array("QuestionID"=>$questionID[$k]);
    $table=$db->select("ScripCalculationsMA", $condition);
    foreach ($table as $row){
        $anMA = trim($row['MA']);
        if ($anMA != ""){ // ... don't show any MA that are blank
            if ($firstAnswer){
                print("<i>Model Answer(s):</i> " . $anMA);
            } else {
                print(" -- " . $anMA);
            };
            $firstAnswer = false;
        };
    };
    print("</font></td></tr>");


    print("<tr><td colspan='2'>&nbsp;</td></tr>");

    print("<tr>");
    print("<td><font color='#ffffff'><b>Answer Offered</b></font></td>");
    print("<td><font color='#ffffff'><b>Number of Occurrences</b></font></td>");
    print("</tr>");

    // Different forms of CORRECT answer ...
    print("<tr>");
    print("<td colspan='2'><font color='#ffffff'><i>CORRECT</i></font></td>");
    print("</tr>");
    foreach ($correctAnswerText as $key => $value){
        print("<tr>");
        print("<td><font color='#ffffff'>$key</font></td>");
        print("<td><font color='#ffffff'>$value</font></td>");
        print("</tr>");
    };

    // Blank line ...
    print("<tr><td colspan='2'>&nbsp;</td></tr>");

    print("<tr>");
    print("<td colspan='2'><font color='#ffffff'><i>INCORRECT</i></font></td>");

    // Different forms of INCORRECT answer ...
    foreach ($wrongAnswerText as $key => $value){
        print("<tr>");
        print("<td><font color='#ffffff'>$key</font></td>");
        print("<td><font color='#ffffff'>$value</font></td>");
        print("</tr>");
    };
    print("</table>");

    // End the row that contains the table ...
    print("</td></tr>");

    };

   // Link back to menu ...
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
};


function Y4_MarkingWWC(){
    // Exam weeks (see Y3_MarkingWWC for practice weeks) ...

    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $nPracticeWeeks=7;
    $nExamWeeks=3;

    // Read ScripStudent - put the contents into a 2D array ...
    $table=$db->select("ScripStudent");
    foreach ($table as $row) {
        if ($row['RealStudent']) {
            $ID=$row['StudentID'];
            $firstName[$ID]=$row['FirstName'];
            $lastName[$ID]=$row['LastName'];
            $userName[$ID]=$row['UserName'];
            $saturn[$ID]=$row['StudentSaturnNumber'];
            $practiceCode[$ID]=$row['PracticeCode'];
            $examCode[$ID]=$row['ExamCode'];
        };
    };

//print("<pre>");print_r($examCode);



    // Read ScripTermPattern - also put into a 2D array ...
    $table=$db->select("ScripTermPattern");
    foreach ($table as $row) {
        $pC=$row['PracticeCode'];
        for ($i=0; $i<$nPracticeWeeks; $i++) {
            $pLetter[$pC][$i]=$row['wk'.($i+1)];
        };
    };

    // Read ScripExamPatterns - put into a 2D array too ...
    $table=$db->select("ScripExamPatterns");
    foreach ($table as $row) {
        $eC=$row['ExamCode']; // ExamCode is the primary key for this table
        for ($i=0; $i<$nExamWeeks; $i++) {
            $eLetter[$eC][$i]=$row['wk'.($i+1)];
            $weekNumber[$eC][$row['wk'.($i+1)]] = $i;
        };
    };

    $table=$db->select("ScripMarksExamModerated");

    foreach ($table as $row) {

        $SID = $row['StudentID'];
        $CaseID = $row['CaseID'];

        // For convenience, identify this ...
        $twoLetterCode = $CaseID[0] . $CaseID[1];

        // We want to add a student's scores together. However, we first check the two-letter code of a score is included in that student's exam pattern ...

// NOTE: THIS CODE NEEDS TO BE REPRODUCED (EXACTLY) IN THE Y4_MarkingWWCR() VERSION OF THIS FUNCTION
// ALSO IT OUGHT TO BE IN THE CALCULATIONS PART OF THIS FUNCTION AND THE Y4_MarkingWWCR() FUNCTION ... EXCEPT WE AREN'T DOING CACLATIONS ANY LONGER!


// For some useful debugging, switch on Debugging 1, Debugging 2 and Debugging 3:  
// Debugging 1:
//print("<br>Looking for $twoLetterCode in");

        // Unset flag ...
        $toBeIncluded = 0;

        // The exam pattern of student with ID $SID is $examCode[$SID]; the two-letter codes in this pattern are the elements of array $eLetter[$examCode[$SID]]. Loop over
        // these elements to see if $twoLetterCode is one of them ... 
        for ($k=0; $k < count($eLetter[$examCode[$SID]]); $k++) {
// Debugging 2:
//print(" " . $eLetter[$examCode[$SID]][$k]);
            if ($eLetter[$examCode[$SID]][$k] == $twoLetterCode){
                // It IS one of them! Record this ...
                $toBeIncluded = 1;
            };
        };

// Debugging 3:
//if ($toBeIncluded){
//print(" -- IN");
//} else {
//print(" -- EX");
//};


        // Big IF check ... only includes marks from current row if row passes this test: $lastName[$SID] is set and the logical variable $toBeIncluded is set:
        if (isset($lastName[$SID]) && $toBeIncluded) {

//print("$SID ".$lastName[$SID]." ");

            // Find the counselling type ...
            $Scondition=array("Letter"=>$CaseID);
            $Stable=$db->select("ScripsScrips", $Scondition);
            foreach ($Stable as $Srow) {
                $Counsel=$Srow['Counsel'];
            };

            $Marks = $row['Marks'];
            $CommsMarks = $row['CommsMarks'];

//print("$Marks $CommsMarks<br />");

            // Find out in which week the case was done ...
            // Practice code is $practiceCode[$SID] ...
            // Exam code is $examCode[$SID] ...
            // Need to search relevant code for letter to find week ...
            for ($i=0; $i < $nExamWeeks; $i++) {
                if ($eLetter[$examCode[$SID]][$i] == $twoLetterCode){
                    $week=$i+1; // This is the WEEK (i.e. 1, ..., 7)
                };
            };


// AT A SAFE TIME, the following two parts of code ought to be put inside the above IF and tested 


            // Scrip scores ...
            $marksThisWeek[$SID][$week-1] += $Marks;
//            if ($week=='7') { print("Student $SID has $Marks<br>"); };

            // Communications scores ...
            if ($Counsel==2 || $Counsel==3) {
                $commsMarksThisWeek[$SID][$week-1] += $CommsMarks;
            };
        };
    };

    // Having looped over the moderated marks for ordinary prescriptions, we now loop over the moderated marks 
    // for the calculations ...
    
    $calculationTotal = array ();
    $table=$db->select("ScripCalculationsAnswersExamModerated");
    foreach ($table as $row){
        // Loop over the contents: start with the student ID ...
        $SID = $row['StudentID'];

        if (isset($lastName[$SID])) { // ... check it's set
            // Read the case ID ...
            $CaseID = $row['CaseID'];
            
            // Read the penalty ...
            $penalty = $row['Penalty'];

            // Add in the mark (which is one minus the penalty), we could do this as
            //               $calculationTotal[$SID][$CaseID] += 1 - $penalty
            // but we want the weekNumber (starting at zero i.e. weekNumber - 1) as the variable (and not the case ID) ...

            for ($i=0; $i < $nExamWeeks; $i++) {
                if ($eLetter[$examCode[$SID]][$i] ==$CaseID[0].$CaseID[1]){
                    $week=$i+1; // This is the WEEK (i.e. 1, ..., 7)
                };
            };
            $calculationTotal[$SID][$week-1] += 1 - $penalty;
        };        
    };


    // Headings of columns of CSV files ...
    $implode = array ("Name", "Username", "Saturn", "RX1", "x1", "C1", "RX2", "x2", "C2", "RX3", "x3", "C3");
    $line = implode(",", $implode)."\n";
    $linec = implode(",", $implode)."\n"; // "c" ending for communications ...

    foreach ($lastName as $id=>$theLastName) {
        $cName=$firstName[$id];
        $line.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
        $linec.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
        for ($i=0; $i<$nExamWeeks; $i++){


            // This is the joint version i.e. script and counselling together
            // If there's a mark, write it; if not, write a space ...
            if (isset($marksThisWeek[$id][$i])) {
                $line.=",".$marksThisWeek[$id][$i];
            } else {
                $line.=",";
            };

            if (isset($calculationTotal[$id][$i])) {
                $line.=",".$calculationTotal[$id][$i];
            } else {
                $line.=",";
            };

            if (isset($commsMarksThisWeek[$id][$i])) {
                $line.=",".$commsMarksThisWeek[$id][$i];
            } else {
                $line.=",";
            };

// THE FOLLOWING CODE BECOMES REDUNDANT WHEN RX AND COUNSELLING ARE MERGED INTO ONE FILE


            // If there's a mark, write it; if not, write a space ...
//            if (isset($marksThisWeek[$id][$i])) {
//                $line.=",".$marksThisWeek[$id][$i];
//            } else {
//                $line.=",";
//            };

            // If there's a mark, write it; if not, write a space ...
 //           if (isset($commsMarksThisWeek[$id][$i])) {
 //               $linec.=",".$commsMarksThisWeek[$id][$i];
//            } else {
//                $linec.=",";
//            };

        };

        $line.="\n"; // ... end of line marker
        $linec.="\n"; // ... end of line marker
    };


//print($line." </br>");

    $thefile="users/practice1/WW.csv"; 
    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    $thefilec="users/practice1/WWC.csv"; 
    $fpoutc=fopen($thefilec,"w"); // WRITE access

    fwrite($fpoutc, $linec); // ... write to file

    print("<tr><td>");
    print("<p><font color=#ffffff>This may take a moment or two ...");
    print(" watch the cursor.</font></p>");
    print("<p><font color=#ffffff>Right click the links below to");
    print(" download the named files.");
    print(" From the");
    print(" context menus choose Save Target As ...</font></p>");

    print("<p><font color=#ffffff><ul>");
    print("<li><a href='$thefile'>Prescription marks</a></li>");
    print("<li><a href='$thefilec'>Communication marks</a></li>");
    print("</ul></font></p>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
};


function Y4_MarkingWWCR(){
    // This "R" version (cf Y4_MarkingWWC) is to include re-sits ...
    // The normal version includes practice and exams. This versions reads the resit table and adds letters there to the exams ...


    Z_DrawHeader();
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $nPracticeWeeks=7;
    $nExamWeeks=3;
    $nResits = 6;

    // Read ScripStudent - put the all students into a many 1D arrays - this will act as look-up table ...
    $table=$db->select("ScripStudent");
    foreach ($table as $row) {
        if ($row['RealStudent']) {
            $luID=$row['StudentID'];
            $lufirstName[$luID]=$row['FirstName'];
            $lulastName[$luID]=$row['LastName'];
            $luuserName[$luID]=$row['UserName'];
            $lupracticeCode[$luID]=$row['PracticeCode'];
            $luexamCode[$luID]=$row['ExamCode'];
        };
    };

    // Read all the usernames in ScripResit and using the look-up table from ScripStudents build tables for this more limited set of students ...
    $table=$db->select("ScripResit");
    foreach ($table as $row) {
        $ID=$row['StudentID'];
        $firstName[$ID]=$lufirstName[$ID];
        $lastName[$ID]=$lulastName[$ID];
        $userName[$ID]=$luuserName[$ID];
        $practiceCode[$ID]=$lupracticeCode[$ID];
        $examCode[$ID]=$lexamCode[$ID];

        // ... and read the two-letter codes
        $R[$ID][1] = $row['R1'];
        $R[$ID][2] = $row['R2'];
        $R[$ID][3] = $row['R3'];
        $R[$ID][4] = $row['R4'];
        $R[$ID][5] = $row['R5'];
        $R[$ID][6] = $row['R6'];
        // ... e.g. $R[1234][2] = "AZ" means that student 1234 in week 2 is doing the AZ cases 
    };


//print("N = " . count($lastName) . "<br />");

    // Read Scripresit - put into a 2D array too ...

//    $table=$db->select("ScripResit");
//    foreach ($table as $row) {
//        $ID=$row['StudentID'];
//        $R[$ID][1] = $row['R1'];
//        $R[$ID][2] = $row['R2'];
//        $R[$ID][3] = $row['R3'];
//        $R[$ID][4] = $row['R4'];
//        $R[$ID][5] = $row['R5'];
//        $R[$ID][6] = $row['R6'];
        // ... e.g. $R[1234][2] = "AZ" means that student 1234 in week 2 is doing the AZ cases 
//    };


    // Read all entries in ScripMarksExamModerated ...
    $table=$db->select("ScripMarksExamModerated");

    // ... and loop over them ...
    foreach ($table as $row) {
        // Work with the student's ID number ...
        $SID = $row['StudentID'];




// Checking needs to be against the $R table ...




        // Check whether the ID has a surname (really just a check whether it is real) ...

        if (isset($lastName[$SID])) {
            $CaseID = $row['CaseID'];

            // Find the counselling type for this case (e.g. 0, 1, 2) ...
            $Scondition=array("Letter"=>$CaseID);
            $Stable=$db->select("ScripsScrips", $Scondition);
            foreach ($Stable as $Srow) {
                $Counsel=$Srow['Counsel'];
            };

            // Read the marks (both types) ...
            $Marks = $row['Marks'];
            $CommsMarks = $row['CommsMarks'];

            // Find out whether this is a resit mark ...
            // Find out in which week the case was done: Practice code is $practiceCode[$SID] and exam code is $examCode[$SID] ...
            // Need to search relevant code for letter to find week ...
            for ($i=0; $i < $nResits; $i++) {
                $week = $i + 1;
                if ($R[$SID][$week] == $CaseID[0].$CaseID[1]){
                    // This ($CaseID) is a resit week (number $week) for student $SID - record the data in two arrays $marksThisWeek and $commsMarksThisWeek ... 
                    // Scrip scores ...
                   $marksThisWeek[$SID][$week-1] += $Marks;

                    // ... and communications scores where appropriate ...
                    if ($Counsel==2 || $Counsel==3) {
                        $commsMarksThisWeek[$SID][$week-1] += $CommsMarks;
                    };
                };
            };
        };
    };



    // Having looped over the moderated marks for ordinary prescriptions, we now loop over the moderated marks for the calculations ...
    
    $calculationTotal = array ();
    $table=$db->select("ScripCalculationsAnswersExamModerated");
    foreach ($table as $row){
        // Loop over the contents: start with the student ID ...
        $SID = $row['StudentID'];

        if (isset($lastName[$SID])) { // ... check it's set
            // Read the case ID ...
            $CaseID = $row['CaseID'];
            
            // Read the penalty ...
            $penalty = $row['Penalty'];

            // Add in the mark (which is one minus the penalty), we could do this as
            //               $calculationTotal[$SID][$CaseID] += 1 - $penalty
            // but we want the weekNumber (starting at zero i.e. weekNumber - 1) as the variable (and not the case ID) ...

            // We go into ScripResits and look for the current student ...
            $condition = array ("StudentID"=>$SID);
            $table=$db->select("ScripResit", $condition);
            // ... and read all the resit two-letter codes for that student ...
            foreach ($table as $row){
                $localR[1] = $row['R1'];
                $localR[2] = $row['R2'];
                $localR[3] = $row['R3'];
                $localR[4] = $row['R4'];
                $localR[5] = $row['R5'];
                $localR[6] = $row['R6'];
            };

            // Now loop over these two-letter codes and see which one (as characterised by its week number) matches the cae Id we'e read from ScripCalculationsAnswersExamModerated ...
            for ($i=0; $i < $nResits; $i++) {
                $week = $i + 1;
                if ($localR[$week] == $CaseID[0].$CaseID[1]){
                    $calculationTotal[$SID][$week-1] += 1 - $penalty;
                };
            };
        };        
    };


    // Everything should now run as before i.e. in Y4_MarkingWWC() ...

    // Process the arrays $marksThisWeek and $commsMarksThisWeek:
    // Headings of columns of CSV files ...
    $implode = array ("Name", "","Rx", "Calc", "Couns");
    $line = implode(",", $implode)."\n";
    $linec = implode(",", $implode)."\n"; // "c" ending for communications ...


    foreach ($lastName as $id=>$theLastName) {
//print("Build line for $theLastName<br />");
        $cName=$firstName[$id];
        $line.="$theLastName - $cName, ".$userName[$id];
        $linec.="$theLastName - $cName, ".$userName[$id];

        for ($i=0; $i<$nResits; $i++){

//    foreach ($lastName as $id=>$theLastName) {
//        $cName=$firstName[$id];
//        $line.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
//        $linec.="$theLastName - $cName, ".$userName[$id].",".$saturn[$id];
//        for ($i=0; $i<6; $i++){


            // This is the joint version i.e. script and counselling together
            // If there's a mark, write it; if not, write a space ...
            if (isset($marksThisWeek[$id][$i])) {
                $line.=",".$marksThisWeek[$id][$i];
            } else {
                $line.=",";
            };

            if (isset($calculationTotal[$id][$i])) {
                $line.=",".$calculationTotal[$id][$i];
            } else {
                $line.=",";
            };

            if (isset($commsMarksThisWeek[$id][$i])) {
                $line.=",".$commsMarksThisWeek[$id][$i];
            } else {
                $line.=",";
            };



            // If there's a mark, write it; if not, write a space ...
//if ($i == 0) { print("For student $id and Resit $i mark is " . $marksThisWeek[$id][$i] . " and comms is " . $commsMarksThisWeek[$id][$i] . "<br>"); };
//            if (isset($marksThisWeek[$id][$i])) {
//                $line.=",".$marksThisWeek[$id][$i];
//            } else {
//                $line.=",";
//            };

            // If there's a mark, write it; if not, write a space ...
//            if (isset($commsMarksThisWeek[$id][$i])) {
//                $linec.=",".$commsMarksThisWeek[$id][$i];
//            } else {
//                $linec.=",";
//            };

        };
        $line.="\n"; // ... end of line marker
        $linec.="\n"; // ... end of line marker
    };


    // Now output to a CSV file called ...
    $thefile="users/practice1/WW.csv"; 

    // If the file already exists, delete it ...
    if (is_file($thefile)) {
        unlink($thefile);
    };

    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    $thefilec="users/practice1/WWC.csv"; 
    $fpoutc=fopen($thefilec,"w"); // WRITE access

    fwrite($fpoutc, $linec); // ... write to file

    print("<tr><td>");
    print("<p><font color=#ffffff>This may take a moment or two ... watch the cursor.</font></p>");
    print("<p><font color=#ffffff>Right click the links below to download the named files.");
    print(" From the context menus choose Save Target As ...</font></p>");

    print("<p><font color=#ffffff><ul>");
    print("<li><a href='$thefile'>Prescription marks</a></li>");
    print("<li><a href='$thefilec'>Communication marks</a></li>");
    print("</ul></font></p>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
};





function Y5_AllData(){
    // This gathers all data $Marks is a two-dimensional array with fields: (1) Student details (e.g. 99); (2) Case ID (e.g. AF3):

    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>");
    print("</font>");
    print("</td></tr>\n");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $table=$db->select("ScripsScrips");
    foreach ($table as $row) {
        $Letter[]=$row['Letter'];
    }; // e.g. IS1, IS2, IS3, ...



    // This code was originally for exams only. Now [September 2013] expanded to include practice weeks too
    $nExamWeeks=3;
    $table=$db->select("ScripExamPatterns");
    foreach ($table as $row) {
        for ($i=0; $i<$nExamWeeks; $i++) {
            $iplus1=$i+1;
            $thisLetter=$row['wk'.($i+1)];
            $weekIncluded[$thisLetter]=1;
        };
    };
    // ... $weekIncluded is thus an array marking whether or not each letter occurs in the relevant type of pattern 
    // (here ScripExamPatterns). If a letter doesn't, cases from that set aren't included in the following code.

    $nPracticeWeeks=7;
    $tableP=$db->select("ScripTermPattern");
    foreach ($tableP as $rowP) {
        for ($i=0; $i<$nPracticeWeeks; $i++) {
            $iplus1=$i+1;
            $thisLetterP=$rowP['wk'.($i+1)];
            $weekIncludedP[$thisLetterP]=1;
        };
    };
    // ... same thing for practice weeks. 

    sort($Letter);
    // ... $Letter is therefore the entire contents of ScripsScrips; whilst $weekIncluded tells us whether a given entry is a part of exams e.g. (for exams)
    // ZA and OB both occur in ScripsScrips but $weekIncluded['ZA'] is false (because it is NOT an exam) while $weekIncluded['OB'] is true because
    // it IS an exam. Similarly, $weekIncludedP is a filter for practice weeks. 

    // Get the details of each student ...
    $table=$db->select("ScripStudent");
    foreach ($table as $row) {
        if ($row['RealStudent']) {
            $ID=$row['StudentID'];
            $firstName[$ID]=$row['FirstName'];
            $lastName[$ID]=$row['LastName'];
            $userName[$ID]=$row['UserName'];
            $practiceCode[$ID]=$row['PracticeCode'];
            $examCode[$ID]=$row['ExamCode'];
        };
    };

    // Read the marks into a 2-D array (with parameters student ID and case ID) ...
    $table=$db->select("ScripMarksExamModerated");
    foreach ($table as $row) {
        $StudentID=$row['StudentID'];
        $CaseID=$row['CaseID'];
        $Marks[$StudentID][$CaseID]=$row['Marks'];
    };
    // ... all exam marks now in array $Marks.

    $tableP=$db->select("ScripMarksModerated");
    foreach ($tableP as $rowP) {
        $StudentID=$rowP['StudentID'];
        $CaseID=$rowP['CaseID'];
        $MarksP[$StudentID][$CaseID]=$rowP['Marks'];
    };
    // ... all practice marks now in array $MarksP.

    // Add in the names i.e. $Marks['FName'] and $Marks['LName'] to the $Marks array ...
    foreach ($Marks as $ID => $v){
        // $ID is the student ID and $v is the rest of it ...
        $Marks[$ID]['FName']=$firstName[$ID];
        $Marks[$ID]['LName']=$lastName[$ID];
    };

    // Construct the column headings: Name, [blank], ID ...
    $line = "Name,,ID";

    // ... and the indvidual cases from ScripsScrips IF they are in ScripPatternExams ...
    for ($i=0; $i<count($Letter); $i++){
        if ($weekIncluded[substr($Letter[$i], 0, 2)]) {
            $line.=",".$Letter[$i];
        };
    };

    $line.=","; // blank column to separate practice from exams

    // ... and the indvidual cases from ScripsScrips IF they are in ScripTermPattern ...
    for ($i=0; $i<count($Letter); $i++){
        if ($weekIncludedP[substr($Letter[$i], 0, 2)]) {
            $line.=",".$Letter[$i];
        };
    };

    $line.="\n"; // ... end of the column headings (so add a return)

    foreach ($Marks as $ID => $v){
        if ($Marks[$ID]['LName']) { // Loop over existing entries only 
            // Beginning of the line (names and ID) ...
            $line.=$Marks[$ID]['LName'].",".$Marks[$ID]['FName'].",".$ID;

            // ... and now the marks for the exams ...
            for ($i=0; $i<count($Letter); $i++){
                if ($weekIncluded[substr($Letter[$i], 0, 2)]) {
                    $line.=",".$Marks[$ID][$Letter[$i]];
                };
            };

            $line.=","; // blank column to separate practice from exams

            // ... and now the marks for practice ...
            for ($i=0; $i<count($Letter); $i++){
                if ($weekIncludedP[substr($Letter[$i], 0, 2)]) {
                    $line.=",".$MarksP[$ID][$Letter[$i]];
                };
            };

            $line.="\n"; // ... end of line
        };
    };

    $thefile="users/practice1/all.csv"; 
    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    print("<tr><td>");
    print("<p><font color='#ffffff'>This may take a moment or two ... watch the cursor.</font></p>");

    print("<p><font color='#ffffff'>Right click the links below to download the named files. From the context menus choose Save Target As ...</font></p>");

    print("<p><font color=#ffffff><ul>");
    print("<li><a href='$thefile'>All marks</a></li>");
    print("</ul></font></p>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");

};

function Y6_AllModeratedExamResults (){
    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>");
    print("</font>");
    print("</td></tr>\n");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $output = "StudentID,Name,Case,Penalty,Tab,Comment";

    $table=$db->select("ScripResultsExamModerated");
    foreach ($table as $row) {
        $CaseID = $row['CaseID'];
        $Penalty = $row['Penalty'];
        $Tab = $row['Tab'];
        $Comment = $row['Comment'];
        $StudentID = $row['StudentID'];
        $condition = array ('StudentID'=>$StudentID);
        $table=$db->select("ScripStudent", $condition);
        foreach ($table as $row) {
            $FirstName = $row['FirstName'];
            $LastName = $row['LastName'];
        };

        $output .= "\n".$StudentID.",".
                 $LastName." - ".$FirstName.",".
                 $CaseID.",".$Penalty.",".$Tab.",".$Comment;
     };

    $thefile="users/practice1/allexamresults.csv"; 
    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $output); // ... write to file


    print("<tr><td>");
    print("<p><font color=#ffffff>This may take a moment or two ...");
    print(" watch the cursor.</font></p>");
    print("<p><font color=#ffffff>Right click the links below to");
    print(" download the named files.");
    print(" From the");
    print(" context menus choose Save Target As ...</font></p>");

    print("<p><font color=#ffffff><ul>");
    print("<li><a href='$thefile'>All Exam Results</a></li>");
    print("</ul></font></p>");

    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");



    
};


function Y9_LogonData () {
// Download all the log-on data ... probably for Excel use

    Z_DrawHeader();
    print("<tr><td colspan='2'>");
    print("<font color=#ffffff>");
    print("</font>");
    print("</td></tr>\n");

    $line="ID,StudentID,UserName,FirstName,";
    $line.="LastName,IPAddress,Location,InDispensary,";
    $line.="DispensaryMode,Date,Time\n";
    $i = 0;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $table=$db->select("ScripLogon");
    foreach ($table as $row) {
        $ID = $row['ID'];
        $StudentID = $row['StudentID'];
        $UserName = $row['UserName'];
        $FirstName = $row['FirstName'];
        $LastName = $row['LastName'];
        $IPAddress = $row['IPAddress'];
        $Location = $row['Location'];
        $InDispensary = $row['InDispensary'];
        $DispensaryMode = $row['DispensaryMode'];
        $Date = $row['Date'];
        // Need to take the hyphen out since Excel will object (trying to
        // carry out SYLK conversion) ... best option at present seems
        // to be dot (still objects but doesn't destroy or mark the 
        // data as some other delimiters do) ...
        $Date = str_replace("-", ".", $Date);
        $Time = $row['Time'];
        $line.=$ID.",".$StudentID.",".$UserName.",".$FirstName.",";
        $line.=$LastName.",".$IPAddress.",".$Location.",".$InDispensary.",";
        $line.=$DispensaryMode.",".$Date.",".$Time."\n";
    };

    $thefile="users/practice1/logon.csv"; 
    $fpout=fopen($thefile,"w"); // WRITE access

    fwrite($fpout, $line); // ... write to file

    print("<tr><td BGCOLOR=#FFFFFF>");
    print("<p><a href='$thefile'>Right click for results</a></p>");
    print("<p><a href=scripadmin.php?tab=5>Return to Menu</a></p>");
    print("</td></tr>");
};

function X9B_ChooseDispensary(){
    // List the dispensaries shown in ScripDispensaryMode ...

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Read the existing values
    Z_DrawHeader();

    print("<TR><TD colspan=2 align=center>");
    print("<TABLE BORDER=1>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='664'>");

    print("<p><font color='#ffffff'>Choose a dispensary from the drop-down menu:</font></p>");

    $table=$db->select("ScripDispensaryMode");

    print("<select name='location'>");
    foreach ($table as $row) {
        $location = $row['Location'];
        print("<option value='$location'>$location</option>");
    };
    print("</select>");

    print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

    print("<input type='submit' value='Update'>");

    print("</form>");

//    print("<tr><td colspan='7' align='center'>");
    print("</table>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>");

};


function X9_SetExam($theDispensary) {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Read the existing values
    Z_DrawHeader();

    $condition = array('Location'=>$theDispensary);
    $table=$db->select("ScripDispensaryMode", $condition);
    foreach ($table as $row) {
        $dispensaryMode=$row['DispensaryMode'];
        $threeKeyPoints=$row['ThreeKeyPoints'];
        $locationDetection=$row['LocationDetection'];
    };


//    $table=$db->select("ScripDispensaryMode", $condition);
//    foreach ($table as $row) {
//        $threeKeyPoints=$row['ThreeKeyPoints'];
//    };

    print("<TR><TD colspan=2 align=center>");
    print("<TABLE BORDER=1>");

    print("<tr><td colspan='3'><font color=ffffff>");
    print("<p>This setting determines what students see in the Dispensary called <b><font color=#FFFF00 size=+2>$theDispensary</font></b>. </p>");
    print("<p>The current setting is <b><font color=#FFFF00 size=+2>");
    if ($dispensaryMode == "U") { print("Unsupervised"); };
    if ($dispensaryMode == "P") { print("Practice"); };
    if ($dispensaryMode == "E") { print("Examination"); };
    if ($dispensaryMode == "PC") { print("Checking (Practice)"); };
    if ($dispensaryMode == "EC") { print("Checking (Examination)"); };
    print("</font></b>.</p>");
    print("<p>Note that default choice below is always <b>Unsupervised</b> "); 
    print("regardless of the current setting.</p>");
    print("</font></td></tr>");

    print("<form action=scripadmin.php method=post>");
    print("<input type='hidden' name='tab' value='662'>");
    print("<input type='hidden' name='location' value='$theDispensary'>");

    print("<tr>");
    print("<th><font color='#ffffff'>Unsupervised</font></th>");
    print("<th><font color='#ffffff'>Practice</font></th>");
    print("<th><font color='#ffffff'><b>Examination</b></font></th>");
    print("</tr>");

    print("<tr>");
    print("<td align=center><input type='radio' name='dispensaryMode' ");
    print(" value='U' size='5' checked></td>"); // Safety! - Always the default
    print("<td align=center><input type='radio' name='dispensaryMode' ");
    print(" value='P' size='5'></td>");
    print("<td align=center><input type='radio' name='dispensaryMode' ");
    print(" value='E' size='5'></td>");
    print("</tr>");

    print("<tr>");
    print("<th><font color='#ffffff'></font></th>");
    print("<th><font color='#ffffff'>Checking (Practice)</font></th>");
    print("<th><font color='#ffffff'><b>Checking (Examination)</b></font></th>");
    print("</tr>");

    print("<tr>");
    print("<td align=center>");
    print("</td>");
    print("<td align=center><input type='radio' name='dispensaryMode' ");
    print(" value='PC' size='5'></td>");
    print("<td align=center><input type='radio' name='dispensaryMode' ");
    print(" value='EC' size='5'></td>");
    print("</tr>");

    print("<tr><td colspan ='3'><br><font color='#ffffff'>");
    print("<p>This setting determines whether the students are asked for the three key points (where appropriate). ");
    print("Note that this only applies to the practice weeeks; three key points are never asked for in the exams - the settings below will not affect the exams.</p>");
    print("<p>The current setting is <b><font color=#FFFF00 size=+2>");
    if ($threeKeyPoints == "1") { print("ASK"); };
    if ($threeKeyPoints == "0") { print("DO NOT ask"); };
    print("</font></b>.</p>");
    print("<p>Note that default choice below is always <b>ASK</b> "); 
    print("regardless of the current setting.</p>");
    print("</font><br><br></td></tr>");

    print("<tr>");
    print("<th></th>");
    print("<th><font color='#ffffff'>ASK</font></th>");
    print("<th><font color='#ffffff'>DO NOT ask</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td align=center>");
    print("</td>");
    print("<td align=center><input type='radio' name='threeKeyPoints' ");
    print(" value='1' size='5' checked></td>");
    print("<td align=center><input type='radio' name='threeKeyPoints' ");
    print(" value='0' size='5'></td>");
    print("</tr>");

    // Changes to cope with Zengenti problem ...
    print("<tr><td colspan ='3'><br><font color='#ffffff'>");
    print("<p>This setting determines how Scripware checks whether a PC is in the Dispensary. ");
    print("<p>The current setting is <b><b><font color=#FFFF00 size=+2>");
    if ($locationDetection == "1") { print("IP address"); };
    if ($locationDetection == "0") { print("Rapid expiry access code (REAC)"); };
    print("</font></b>.</p>");
    print("<p>Note that default choice below is always <b>REAC</b> regardless of the current setting.</p>");
    print("</font><br><br></td></tr>");

    print("<tr>");
    print("<th></th>");
    print("<th><font color='#ffffff'>Rapid expiry access code (REAC)</font></th>");
    print("<th><font color='#ffffff'>IP address</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td align=center>");
    print("</td>");
    print("<td align=center><input type='radio' name='locationDetection' ");
    print(" value='0' size='5' checked></td>");
    print("<td align=center><input type='radio' name='locationDetection' ");
    print(" value='1' size='5'></td>");
    print("</tr>");


    // Button at the end
    print("<tr><td colspan='7' align='center'>");
    print("<input type='submit' value='Update'></td></tr>");
    print("</form></table>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>");
};

function X9A_ImplementSetExam($theDispensary, $dispensaryMode, $threeKeyPoints, $locationDetection) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

    $condition = array('Location'=>$theDispensary);

    // Set new value of variable ...
    $data=array("DispensaryMode" => $dispensaryMode);
    $db->update("ScripDispensaryMode", $data, $condition);

    $data=array("ThreeKeyPoints" => $threeKeyPoints);
    $db->update("ScripDispensaryMode", $data, $condition);

    $data=array("LocationDetection" => $locationDetection);
    $db->update("ScripDispensaryMode", $data, $condition);

    print("<tr><td colspan='2'><font color='#ffffff'>Availability updated"); 
    print("</font></td></tr>");
    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a>");    
    print("</td></tr></table>"); 
};

function Z_EditInitialLetterClassification() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the trusts and display as a table
    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select one letter from the list");
    print(" and click the submit button to edit the classification</h2></font>");
    print("</td></tr>");
    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='866'>");

    print("<tr>");
    print("<th>&nbsp;</th>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Initial Letter</font></th>");
    print("<th><font color='#ffffff'>Classification</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type='radio' name='ID' value='0' checked></td>");
    print("<td><font color='#ffffff'>New</td>");
    print("</tr>");

    $table=$db->select("ScripInitialLetterClassification");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        $theInitialLetter=$row['InitialLetter'];
        $theClassification=$row['Classification'];
        print("<td><input type='radio' name='ID' value=$theID></td>");
        print("<td><font color='#ffffff'>$theID</font></TD>");
        print("<td><font color='#ffffff'>$theInitialLetter</font></TD>");
        print("<td><font color='#ffffff'>$theClassification</font></TD>");
        print("</tr>");
    };

    print("<tr><td colspan='8' align='center'><input type='submit'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a></td></tr></table>");
};

function ZA_EditSingleInitialLetterClassification($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the trust and display in a form
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripInitialLetterClassification",$condition);
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theInitialLetter=$row['InitialLetter'];
            $theClassification=$row['Classification'];
        };
    } else {
        $theInitialLetter="";
        $theClassification="";
    };


    print("<tr>");
    print("<td colspan='2' align='center'>");
    print("<table border='1'>");
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value='867'>");
    print("<input type='hidden' name='ID' value=$SID>");

    print("<tr>");
    print("<th><font color='#ffffff'>Initial Letter</font></th>");
    print("<th><font color='#ffffff'>Classification</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input type='text' name='theinitialletter' value='$theInitialLetter' size='35'>");
    print("</td>");

    print("<td>");
    print("<select name=theclassification>");
    print("<option value=''>Select from ...");
    print("<option value='practice'");
    if ($theClassification == 'practice') { print(" selected"); };
    print(">Practice");
    print("<option value='exam'");
    if ($theClassification == 'exam') { print(" selected"); };
    print(">Examination");
    print("<option value='unsupervised'");
    if ($theClassification == 'unsupervised') { print(" selected"); };
    print(">Unsupervised");
    print("</td>");
    print("</tr>");

    print("<tr><td colspan='4' align='center'>");
    print("<input type='submit' value='Update'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>");
};


function ZB_UpdateInitialLetterClassification($SID, $theInitialLetter,
                        $theClassification) {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 


    // Update details of the trust ...
    $data= array(
        "ID" => $SID,
        "InitialLetter" => $theInitialLetter,
        "Classification" => $theClassification);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripInitialLetterClassification", $data, $condition);
    } else {
        $db->insert("ScripInitialLetterClassification", $data);
    };

    print("<tr><td colspan='2'><font color='#ffffff'>Initial letter classification updated");
    print("</font></td></tr>");
    print("<tr><td><a href='scripadmin.php?tab=5'>Return to Menu</a>");
    print("</td></tr></table>");
};

function Z_Help($index) {
    print("<HTML><HEAD>\n<TITLE>Help</TITLE>\n");
    print("</HEAD><BODY)\n");
    switch ($index)
    {   /* CASES 1- 50 relate to Drug Problem screen */
	case 1: print("<P>This field contains the name of the drug as it ");
	    print("should appear on the prescription</P>"); break;
    	case 2: print("<P>This field contains the name of the ");
	    print("replacement drug.</P>");
	    print("<P>This is used either for a drug name mispelling ");
	    print("or for a new drug.<P>");
	    print("<P>This field can be left blank.</P>");
		break;
    	case 3: print("<P>This field contains the value zero ");
	    print("if the drug name is to be shown.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 4: print("<P>This field contains the value zero ");
	    print("if the drug name is mispelled in field 1.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 5: print("<P>This field contains the value zero ");
	    print("if the drug is appropriate.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 6: print("<P>This field contains the drug strength ");
	    print("as it appears on the form.</P>");
		break;
    	case 7: print("<P>This field contains the ");
	    print("replacement strength</P>");
	    print("<P>This is used if the strength is incorrect</P>");
	    print("<P>This field can be left blank.</P>");
		break;
    	case 8: print("<P>This field contains the value zero ");
	    print("if the drug exists.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
     	case 9: print("<P>This field contains the value zero ");
	    print("if the strength is shown.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
     	case 10: print("<P>This field contains the value zero ");
	    print("if the strength is available.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 11: print("<P>This field contains the delivery form ");
	    print("as it appears on the form.</P>");
		break;
    	case 12: print("<P>This field contains the ");
	    print("replacement delivery form</P>");
	    print("<P>This is used if the form is incorrect</P>");
	    print("<P>This field can be left blank.</P>");
		break;
    	case 13: print("<P>This field contains the value zero ");
	    print("if the form is OK.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 14: print("<P>This field contains the value zero ");
	    print("if the form is available.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 15: print("<P>This field contains the value zero ");
	    print("if the form is appropriate.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 16: print("<P>This field contains the dose ");
	    print("as it appears on the form.</P>");
		break;
    	case 17: print("<P>This field contains the ");
	    print("replacement dose</P>");
	    print("<P>This is used if the dose is incorrect</P>");
	    print("<P>This field can be left blank.</P>");
		break;
    	case 18: print("<P>This field contains the value zero ");
	    print("if the dose is not an overdose.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 19: print("<P>This field contains the value zero ");
	    print("if the dose is complete.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 20: print("<P>This field contains the value zero ");
	    print("if the dose is required.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 21: print("<P>This field contains the value zero ");
	    print("if the form is allowed.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 22: print("<P>This field contains the quantity ");
	    print("as it appears on the form.</P>");
		break;
    	case 23: print("<P>This field contains the value zero ");
	    print("if the form does not require intervals to be shown.</P>");
	    print("<P>Enter a 1 (one) - and no other number - ");
            print("if the interval <b>is</b> to be shown</P>");
		break;
    	case 24: print("<P>This field contains the value zero ");
	    print("if the dose is not an under dose.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 25: print("<P>This field contains the value zero ");
	    print("if the dose timing is OK.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 26: print("<P>This field contains the correct quantity ");
	    print("if that field is incorrect.</P>");
	    print("<P>This field can be left blank</P>");
		break;
    	case 27: print("<P>This field contains the value zero ");
	    print("if the quantity is missing.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 28: print("<P>This field contains the value zero ");
	    print("if the quantity is excessive in professional opinion.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 29: print("<P>This field contains the value zero ");
	    print("if the quantity is sufficient.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 30: print("<P>This field contains the value zero ");
	    print("if the interval is correct.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 31: print("<P>This field contains the value zero ");
	    print("if the quantity is given in words.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 32: print("<P>This field contains the value zero ");
	    print("if the quantity is given in figures.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 33: print("<P>This field contains the value zero ");
	    print("if the quantity is legally OK.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 34: print("<P>This field contains the value zero ");
	    print("if the drug can be issued on this prescription type.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 35: print("<P>This field contains the value zero ");
	    print("if the drug can be issued by this practitioner.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 36: print("<P>This field contains the number of ");
	    print("days that appears on the prescription.</P>");
		break;
    	case 37: print("<P>This field contains the purpose.");
	    print("<P>This field can be left blank is the purpose ");
	    print("is not necessary for this prescription type</P>");
 		break;
    	case 38: print("<P>This field contains the value zero ");
	    print("if the purpose is shown.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 39: print("<P>This field contains the value zero ");
	    print("if the drug is not blacklisted.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 40: print("<P>This field contains the value zero ");
	    print("if the drug is in the Formulary.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
    	case 41: print("<P>This field contains the value zero ");
	    print("if the drug can be printed.</P>");
	    print("<P>Enter the number 1 if the drug must be handwritten</P>");
		break;
    	case 42: print("<P>This field contains the value zero ");
	    print("if the drug writing matches the previous field.</P>");
	    print("<P>Otherwise enter the number of marks to be lost ");
	    print("if the student fails to spot the mistake<P>");
		break;
/* CASES 50 - 59 relate to Prescriber */
   	case 50: print("<P>This field contains name of the signature ");
	    print("graphic file (without the extension).</P>");
	    print("<P>The file must be a transparent GIF file</P>");
	    print("<P>Use ftp to put the file in to the correct directory</P>");
 		break;
/* CASES 60 - 69 relate to the form */
   	case 60: print("<P>This field contains background colour of the ");
 	    print("form as a 6 character hexadecimal number.</P>");
	    print("<P>This is used by HTML to show the form colour</P>");
	    print("<P>You will need to use a paint program to find ");
	    print("this RGB colour.</P>");
  		break;
   	case 61: print("<P>Some forms have the doctor's name printed ");
 	    print("in a separate box just below the medication.</P>");
	    print("<P>Set this value to 1 to force this.</P>");
	    print("<P>Set this value to zero to have the doctor's ");
	    print("name with the address</P>");
   		break;
   	case 62: print("<P>Some forms have extra text printed ");
 	    print("just above the medication. e.g. Hospital Prescriber</P>");
	    print("<P>Enter the text here to force this.</P>");
	    print("<P>Leave blank if there is nothing to print</P>");
   		break;
   	case 63: print("<P>The prescriptions are divided in to similar ");
 	    print("groups by layout.</P>");
	    print("<P>Choose the appropriate layout for this form.</P>");
	    print("<P>The other fields customise the form within ");
	    print("this overall layout ");
   		break;
/* CASES 70 - 79 relate to the scrip */
   	case 70: print("<P>This field contains the id code ");
 	    print("for the miscellaneous problem data.</P>");
	    print("<P>You will need to choose 'Edit misc ");
	    print("problem details' to find this number.</P>");
  		break;
/* CASES 80 - 89 relate to the patient problem */
   	case 80: print("<P>This item is not used in this version</P>");
	    print("<P>Leave it set to zero.</P>");
  		break;
   	case 81: print("<P>Some cases need extra text that doesn't ");
	    print("fit other categories e.g. the weight of the dog ");
 	    print("or the age of a child.</P>");
 	    print("<P>Otherwise leave the field blank.</P>");
 		break;
/* CASES 90 - 99 relate to the misc problem */
   	case 90: print("<P>This is set to number of marks lost "); 
	    print("if the date is missing.</P>");
	    print("<P>Otherwise leave it set to zero.</P>");
  		break;
   	case 91: print("<P>This is set to number of marks lost "); 
	    print("if the date is in the future.</P>");
	    print("<P>Otherwise leave it set to zero.</P>");
  		break;
    	case 92: print("<P>This is set to number of marks lost "); 
	    print("if the date is overdue.</P>");
	    print("<P>Otherwise leave it set to zero.</P>");
  		break;
    	case 93: print("<P>This is set to number of marks lost "); 
	    print("if the patient name should be handwritten.</P>");
	    print("<P>Otherwise leave it set to zero.</P>");
  		break;
    	case 94: print("<P>This is set to the file destination "); 
	    print("code by choosing from the list</P>");
  		break;
    	case 95: print("<P>This is set to number of marks lost "); 
	    print("if there is an interaction.</P>");
	    print("<P>Otherwise leave it set to zero.</P>");
  		break;
    	case 96: print("<P>This is set to the action "); 
	    print("if there is an interaction.</P>");
	    print("<P>Set it to (A)dvise, (R)eplace or (D)elete</P>");
            print("<H2>Any actions take place on the second drug shown</H2>");
	    print("<P>Edit the scrip details to make sure this drug ");
	    print("is in the Drug 2 position!</P>");
  		break;
    	case 97: print("<P>This is set to the feedback given "); 
	    print("if the interaction is spotted.</P>");
  		break;
/* CASES 100 - 109 relate to the label model answers problem */
   	case 100: print("<P>This is set to the drug ID "); 
	    print("using the drop down menu.</P>");
  		break;
   	case 101: print("<P>This is set to the ancillary codes "); 
	    print("You can use the lookup table to check the codes.</P>");
  		break;
   	case 102: print("<P>This is set to the dose codes "); 
	    print("You can use the lookup table to check the codes.</P>");
  		break;
   	case 103: print("<P>This is a flag to show that a ref no is "); 
	    print("required. Leave at zero if no ref number is required.</P>");
  		break;
       default: print("<P>No error message for this item</P>");
     }
    print("<P><A HREF=javascript:void(0)\n");
    print('onClick="self.close()">');
    print("Close help window and return to admin screen</A>");
    print("</P></BODY></HTML>");
} /* end of function help */

function Z1_ChangeMaxima() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* Read the existing values */  
    $table=$db->select("ScripCommunicationMaxima");
    Z_DrawHeader();

    foreach ($table as $row) {
        $style = $row['Style'];
	$information = $row['Information'];
	$structure = $row['Structure'];
	$response = $row['Response'];
	$understanding = $row['Understanding'];
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");

    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=612>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Style</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Information</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Structure</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Response</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Understanding</FONT></TH>");
    print("</TR>");

    print("<TR>");
    print("<td align=center><INPUT TYPE=TEXT NAME=style ");
    print(" VALUE='$style' SIZE=5></td>");

    print("<td align=center><INPUT TYPE=TEXT NAME=information ");
    print("VALUE='$information' SIZE=5></td>");

    print("<td align=center><INPUT TYPE=TEXT NAME=structure ");
    print("VALUE='$structure' SIZE=5></td>");

    print("<td align=center><INPUT TYPE=TEXT NAME=response ");
    print("VALUE='$response' SIZE=5></td>");

    print("<td align=center><INPUT TYPE=TEXT NAME=understanding ");
    print("VALUE='$understanding' SIZE=5></td>");
    print("</TR>");

    print("<TR><TD colspan=7 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}  /* end of function edit single term pattern */

function Z1A_ImplementChangeMaxima($style, $information, $structure,
     $response, $understanding) {

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

/* update details of the term pattern */  
   $maximadata= array( "Style" => $style,
	"Information" => $information,
        "Structure" => $structure,
	"Response" => $response,
        "Understanding" => $understanding);
    Z_DrawHeader();
    $db->update("ScripCommunicationMaxima",$maximadata);
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Maxima updated");    
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");    
    print("</TD></TR></TABLE>"); 
} /* End of function update maxima */

function ZZ_ViewLogon(){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
    $table=$db->select("ScripLogon");

    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Here is the data ");
    print("(D means Dispensary)</H2></FONT>");
    print("</TD></TR>");
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<TH><FONT COLOR=FFFFFF>ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Student ID</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Username</FONT></TH>");
    print("<TH colspan='2'><FONT COLOR=FFFFFF>Name</FONT></TH>");
//    print("<TH><FONT COLOR=FFFFFF>Last Name</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>IP Address</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Location</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>In D?</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>D Mode</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Date</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Time</FONT></TH></TR>");


    foreach ($table as $row) {

        $ID = $row['ID'];
        $StudentID = $row['StudentID'];
        $UserName = $row['UserName'];
        $FirstName = $row['FirstName'];
        $LastName = $row['LastName'];
        $IPAddress = $row['IPAddress'];
        $Location = $row['Location'];
        $InDispensary = $row['InDispensary'];
        $DispensaryMode = $row['DispensaryMode'];
        $Date = $row['Date'];
        $Time = $row['Time'];

        print("<tr>");
        print("<td><font color='#ffffff'>$ID</FONT></TD>");
        print("<td><font color='#ffffff'>$StudentID</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$UserName</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$FirstName</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$LastName</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$IPAddress</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$Location</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$InDispensary</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$DispensaryMode</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$Date</FONT></TD>");
        print("<td><FONT COLOR=FFFFFF>$Time</FONT></TD>");
        print("</tr>");
    };
    print("</TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");

    
};

function ZSA_SpecialAccessSummary() {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details and display as a table  
    Z_DrawHeader();
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one item from the list");
    print(" and click the submit button to edit the details.<br></H2>");
    print("Be careful with this option - remember to switch it off ");
    print(" when no longer required. The settings for ");
    print("program mode and letter will over-ride everything else!<br>");
    print("</FONT>");
    print("</TD></TR>");

    // Column headings ...
    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=682>");
    print("<tr><th>&nbsp;</th>");
    print("<th><FONT COLOR=FFFFFF>ID</FONT></th>");
    print("<th><FONT COLOR=FFFFFF>Student</FONT></th>");
    print("<th><FONT COLOR=FFFFFF>Program Mode</FONT></th>");
    print("<th><FONT COLOR=FFFFFF>Letter</FONT></th>");
    print("</tr>");

    // First row - New ...
    print("<tr>");
    print("<td><input type=radio type=text name=ID value='0' checked></td>");
    print("<td><font color='#ffffff'>New</font></td>");
    print("</tr>");

    // Later rows - existing ...
    $table=$db->select("ScripSpecialAccess");
    foreach ($table as $row) {
        $theID=$row['ID'];
        $theStudentID=$row['StudentID'];
        $theProgramMode=$row['ProgramMode'];
        $theLetter=$row['Letter'];

        print("<tr>");

        print("<td><FONT COLOR=FFFFFF>");
        print("<INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>");

        print("<td><FONT COLOR=FFFFFF>$theID</FONT></TD>");

        if ($theStudentID) {
            $db=new dbabstraction();
            $db->connect() or die ($db->getError());
            $condition=array("StudentID"=>$theStudentID);
            $table=$db->select("ScripStudent",$condition);
            foreach ($table as $row) {
                $name=$row['LastName'].", ".$row['FirstName'];
            };
        } else {
            $name="";
        };

	print("<td><FONT COLOR=FFFFFF>$name</FONT></td>");
	print("<td><FONT COLOR=FFFFFF>$theProgramMode</FONT></td>");
	print("<TD><FONT COLOR=FFFFFF>$theLetter</FONT></TD>");

	print("</TR>");
    };
    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};


function ZZ_FindAllLetters(){
    // This function looks down ScripsScrips and finds all
    // the letters that are currently in use e.g. if there were
    // five entries A1, ZB1, ZB2, ZB3 and VT8, it would respond
    // with a three-element array containing A, ZB and VT

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    $table = $db->select("ScripsScrips");
    foreach($table as $row){
        $aName = $row['Letter'];

        // Strip out any digits ...
        for ($i = 0; $i < strlen($aName); $i++) {
            if (is_numeric(substr($aName, $i, 1))){
                $gotthelettersonly = substr($aName, 0, $i);
                // Now compare to existing letters in $theLettersOnly ...
                $notfound = true;
                for ($j = 0; $j < count($theLettersOnly); $j++){
                    if ($gotthelettersonly == $theLettersOnly[$j]) {
                        // Aleady there - don't record ...
                        $notfound = false;
                        break;
                    };
                };

                // Is $notfound still true ...
                if ($notfound){ // If so, record the letter ...
                    $theLettersOnly[] = $gotthelettersonly;
                };

            };
        };
    };
    sort($theLettersOnly);
    return $theLettersOnly;
};



function ZSA_SpecialAccessOne($ID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
 
    // Get details of the patient and display in a form
    $table=$db->select("ScripSpecialAccess",array('ID'=>$ID));
    Z_DrawHeader();
    print("<tr><td colspan=2 align=center><table border='1'>");
    print("<form action=scripadmin.php METHOD=POST>");
    print("<input type=hidden name=tab value=684>");
    print("<input type=hidden name=ID value=$ID>");

    print("<tr>");
    print("<th><font color=FFFFFF>ID</FONT></th>");
    print("<th><font color=FFFFFF>Student</FONT></th>");
    print("<th><font color=FFFFFF>Program Mode</FONT></th>");
    print("<th><font color=FFFFFF>Letter</FONT></th>");
    print("</tr>");

    if ($ID != '0') {
        $db=new dbabstraction();
        $db->connect() or die ($db->getError());
        $condition=array("ID"=>$ID);
        $table=$db->select("ScripSpecialAccess",$condition);
        foreach ($table as $row) {
            $theID=$row['ID'];
            $theStudentID=$row['StudentID'];
            $theProgramMode=$row['ProgramMode'];
            $theLetter=$row['Letter'];
       };
    } else {
        $theID="";
	$theStudentID="";
	$theProgramMode="";
	$theLetter="";
    };

    print("<tr>");
    print("<td><font color=FFFFFF>$ID</font></td>");

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());
    $table=$db->select("ScripStudent");

    // Need to sort the table of student data on surname ...

    foreach ($table as $row) {
        $ordering[]=$row['LastName'];
    };
    array_multisort($ordering, SORT_ASC, SORT_STRING, $table);

    print("<td><select name=newStudentID>");
    print("<option value='0'>");
    foreach ($table as $row) {
        $name=$row['LastName'].", ".$row['FirstName'];
        $SID=$row['StudentID'];
        print("<option value='$SID'");
        if ($SID == $theStudentID) { print(" selected"); };
        print(">$name");        
    };
    print("</select></td>");

    $modeOptions=array("Unsupervised", "Practice", "Examination");
    print("<td><select name=newMode>");
    foreach ($modeOptions as $anOption) {
        print("<option value='$anOption[0]'");
        if ($theProgramMode == $anOption[0]) { print(" selected"); };
        print(">$anOption");
    };
    print("</select></td>");

    // Find all the possibilities (e.g. CB, RR, ...) and store in array ...


    $letterOptions = ZZ_FindAllLetters();

//print_r($letterOptions);

//    $letterOptions="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    print("<td><select name=newLetter>");
    for ($i=0; $i < count($letterOptions); $i++) {
        print("<option value='$letterOptions[$i]'");
        if ($theLetter == $letterOptions[$i]) { print(" selected"); };
        print(">$letterOptions[$i]");
    };
    print("</select></td>");
    print("</tr>");

    print("<TR><TD colspan=8 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");

    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
}  /* end of function edit single patient */

function ZSA_SpecialImplement($ID, $studentID, $programMode, $letter) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 
/* Update details of the student */  
   $accessdata= array("StudentID" => $studentID,
	"ProgramMode" => $programMode,"Letter" => $letter);
    $condition=array('ID'=>$ID);

    if ($ID != '0') {
        $table=$db->update("ScripSpecialAccess",$accessdata, $condition);   
    } else {
        $table=$db->insert("ScripSpecialAccess",$accessdata);         
    };

    Z_DrawHeader();
    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Special access updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};

function CH_AddNewLetter($newLetter){
    // Add the letter specified by the user to the table

    // THIS CAN PROBABLY BE REPLACED BY CH_AddNewNumber($newLetter, "")
    global $_POST;
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

//    $data = array ('Letter'=>$newLetter, 'NumberOfScript'=>1, 'QuestionNumber'=>1, 'PenaltyTrue'=>'', 'PenaltyFalse'=>'');
    $data = array ('Letter'=>$newLetter, 'NumberOfScript'=>1);

    $table=$db->insert("ScripCheckingMA", $data);

//    print("<input type='hidden' name='newletter' value='981'>\n");
    CH_EditMA($newLetter, "");

};

function CH_AddNewNumber($newLetter, $newNumber){
    // Add the number specified by the user to the table

    global $_POST;
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

//    $data = array ('Letter'=>$newLetter, 'NumberOfScript'=>$newNumber, 'QuestionNumber'=>1, 'PenaltyTrue'=>'', 'PenaltyFalse'=>'');
    $data = array ('Letter'=>$newLetter, 'NumberOfScript'=>$newNumber);

    $table=$db->insert("ScripCheckingMA", $data);

//    print("<input type='hidden' name='newletter' value='981'>\n");
//    print("<input type='hidden' name='newnumber' value='981'>\n");
    CH_EditMA($newLetter, $newNumber);

};

function CH_EditMA($showLetter, $showNumber){

    // What this should do. User selects a new or existing letter. Based on the letter, user selects a new or existing number.
    // With letter-number connection (e.g. AA7), user can change type (A, C or T) and availability (true or false) and then
    // goes on to amend (set up for a new case) the model answers:


    global $_POST;
    global $case;

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all form table and display as a table

    Z_DrawHeader();

    print("<tr>\n<td colspan='2'>");
    print("<font color='#ffffff'>");
    print("<h2>Select one item from the list and click the submit button to edit the details</h2>");
    print("You can do one of two different things on this screen:<br><br>");
    print("1. Choose a letter, choose a number and press Submit to go on to amend model answers;<br><br>");
    print("2. Choose a letter, tick or untick boxes next to numbers of exercises and press Submit to change availability.<br><br>");
    print("</font>");
    print("</td></tr>\n");

    print("<tr><td colspan='2' align='center'>\n");
    print("<table border='1'>\n");
    print("<form action='scripadmin.php' method='post'>\n");
    print("<input type='hidden' name='tab' value='981'>\n");

    // Headings ...
    print("<tr>");
    print("<th><font color='#ffffff'>Letter</font></th>");
    print("<th><font color='#ffffff'>Number</font></th>");
    print("</tr>");

    // Prepare data for the drop-down box for the LETTER ...
    $table=$db->select("ScripCheckingMA");
    // List each letter (e.g. C) ONCE only ...
    for ($i = 0; $i < count($table); $i++){
        $notFound = true;
        // Loop over letters only ...
        for ($j = 0; $j < count($tableLetterOnly); $j++){
            // If already in there, we don't want to add it and we don't even want to go any further down $tableLetterOnly (hence break) ...
            if ($table[$i]['Letter'] == $tableLetterOnly[$j]){
                $notFound = false; break;
            };
        };
        // Not in table so far - add it ...
        if ($notFound) {
            $tableLetterOnly[] = $table[$i]['Letter'];
        };
    };

    // Sort the letters. We want the sort to be case insensitive: Produce an array that is entirely lower case ...
    $lowerCase = array_map('strtolower', $tableLetterOnly);
    // ... now use multisort, sort $tableFormtypesOnly in the same way as $lowerCase ...
    array_multisort($lowerCase, SORT_ASC, SORT_STRING, $tableLetterOnly);

    // Drop down box for CHOSENLETTER with $_POST['chosenletter'] showing EXCEPT that if $_POST['chosenletter'] is zero
    // we accept a letter from the user ... (eventually, we want a check that the letter doesn't already exist)
    print("<tr>");
    print("<td>");
    if ($_POST['chosenletter'] != '0'){
        print("<select name=chosenletter ");
        // When a letter is chosen, automatically submit the form ...
        print("onchange='javascript:document.forms[0].submit()'>");
        print("<option value=''>Select from ...");
        print("<option value='0'>New");
        foreach ($tableLetterOnly as $element) {
            print("<option");
            // Set the letter if one exists in $_POST or if $showLetter is not null ...
            if ($element == $_POST['chosenletter'] || $element == $showLetter && $showLetter != "") {
                print(" selected");
            };
            print(">$element");
        };
        print("</select>");
    } else {
        // Input box for the new letter ...
        // Must clear the number too (since this is new letter) hence this is used - see below:
        // if (($element == $_POST['chosennumber'] || $element == $showNumber && $showNumber != "") && $_POST['chosenletter'] != '0') {

        print("<input type='hidden' name='tab' value='960'>\n");
        print("<input type='text' name='newletter' size='10'>\n");
        print("<br><input type='submit' name='Submit' value='Confirm'>\n");
    };
    print("</td>");

    // NOW GET THE NUMBER (i.e. letter is fixed) ...

    // If a letter has been chosen (one way or the other, v.s.), this part will execute. If not, it will fail by default ...
    if ($_POST['chosenletter']) {
        $_SESSION['letterCheckingWeek'] = $_POST['chosenletter'];
    };

    // If a letter has just been chosen and set, set the variable ...
    if ($showLetter != "") {
        $_SESSION['letterCheckingWeek'] = $showLetter;
    };

    $condition = array ('Letter'=>$_SESSION['letterCheckingWeek']);
    $table = $db->select("ScripCheckingMA", $condition);
    // List each number (e.g. 2) ONCE only ...
    for ($i = 0; $i < count($table); $i++){
        $notFound = true;
        // Loop over numbers only ...
        for ($j = 0; $j < count($tableNumberOnly); $j++){
            // If already in there, we don't want to add it and we don't even want to go any further down $tableNumberOnly (hence break) ...
            if ($table[$i]['NumberOfScript'] == $tableNumberOnly[$j]){
                $notFound = false; break;
            };
        };
        // Not in table so far - add it ...
        if ($notFound) {
            $tableNumberOnly[] = $table[$i]['NumberOfScript'];
        };
    };
    // Put into a permanent array - we need it later ...
    $_SESSION['tableNumberOnly'] = $tableNumberOnly;

    // Note that $tableNumberOnly contains the numbers of all the exercises belonging to the current letter. They have been derived from the 
    // $ScripCheckingMA table (which list all the model answers). Once a letter has been chosen, we offer the administrator a table showing the
    // exercises (each labelled by its number) and which of them is available and which not.
 
    // Drop down box for CHOSENNUMBER ...
    print("<td>");
    if ($_POST['chosennumber'] != '0'){
        if (count($tableNumberOnly)) {
            print("<select name=chosennumber ");
            // When a letter is chosen, automatically submit the form ...
            print("onchange='javascript:document.forms[0].submit()'>");
            print("<option value=''>Select from ...");
            print("<option value='0'>New");
            foreach ($tableNumberOnly as $element) {
                print("<option");
                if (($element == $_POST['chosennumber'] || $element == $showNumber && $showNumber != "") && $_POST['chosenletter'] != '0') {
                    print(" selected");
                };
                print(">$element");
            };
            print("</select>");
        } else {
            print("&nbsp;");
        };
    } else {
        // Input box for the new letter ...
        print("<input type='hidden' name='newletter' value='".$_SESSION['letterCheckingWeek']."' size='10'>");
        print("<input type='hidden' name='tab' value='962'>");
        print("<input type='text' name='newnumber' size='10'><br>");
        print("<input type='submit' name='Submit' value='Submit'>");
    };
    print("<td>");
    print("</tr>");

// Original position of Confirm button ...
//    print("<tr>");
//    print("<td colspan='8' align='center'>");
//    print("<input type='submit' name='Confirm' value='XConfirm'>");
//    print("</td>");
//    print("</tr>\n");

    print("</table>\n");

    print("<br><br>");

    if ($_POST['chosenletter'] || $showLetter != ""){
        if ($showLetter != ""){
            $condition = array ('Letter'=>$showLetter);
        } else {
            $condition = array ('Letter'=>$_POST['chosenletter']);
        };
        $table = $db->select("ScripCheckingCategory", $condition);
        foreach ($table as $row){
            $categoryOfQuestion = $row['Category'];
        };

        print("<table align='center' border='1'>");
        print("<tr>");
        // Headings ...
        print("<th><font color='#ffffff'>Exercise:</font>");
        print("</th>");
        print("<th><font color='#ffffff'>Category:</font>");
        print("</th>");
        print("</tr>");

        print("<tr>");
        print("<td>");
        if ($showLetter != ""){
            print("<font color='#ffffff'>".$showLetter."</font>");
        } else {
            print("<font color='#ffffff'>".$_POST['chosenletter']."</font>");
        };
        print("</td>");

        print("<td>");
        // Begin the <SELECT> for A, C and T ...
        print("<select name='newcategory'>");
        print("<option value='A'");
        if ($categoryOfQuestion == "A") { print(" selected"); };
        print(">Accuracy");
        print("<option value='C'");
        if ($categoryOfQuestion == "C") { print(" selected"); };
        print(">Clinical");
        print("<option value='T'");
        if ($categoryOfQuestion == "T") { print(" selected"); };
        print(">Tutorial");
        print("</select>");

        print("</td>");
        print("</tr>");
        print("</table>");
    };

    print("<br><br>");

    if ($_POST['chosenletter'] || $showLetter != ""){
        // Go to ScripCheckingAvailablility and see which exercises from the letter are available ...
        if ($showLetter != ""){
            $condition = array ('Letter'=>$showLetter);
        } else {
            $condition = array ('Letter'=>$_POST['chosenletter']);
        };

        $table=$db->select("ScripCheckingAvailability", $condition);
        foreach ($table as $row){
            $availableThisExercise[$row['NumberOfScript']] = $row['Available'];
        };

        print("<table align='center' border='1'>");
        // Headings ...
        print("<tr>");
        print("<td><font color='#ffffff'><b>Exercise</b></font></td>");
        print("<td><font color='#ffffff'><b>Available</b></font></td>");
        print("</tr>");

        // This variable is the largest exercise number e.g. if the only
        // C exercises in the ScripCheckingMA are C2, c4 and C7, then
        // $maxExercise is 7 ...
        $maxExercise = 0; 
        for ($k = 0; $k < count($tableNumberOnly); $k++) {
            if($tableNumberOnly[$k] > $maxExercise) { $maxExercise = $tableNumberOnly[$k]; };

            print("<tr>");
            print("<td><font color='#ffffff'>".$tableNumberOnly[$k]."</font></td>");
            print("<td align='center'><input type='checkbox' name='available$tableNumberOnly[$k]' value='1'");
            if ($availableThisExercise[$tableNumberOnly[$k]] || $showLetter != "") { print(" checked"); };
            print("></td>");
            print("</tr>");
        };
        print("<input type='hidden' name='maxExercise' value=$maxExercise>\n");
        print("</table>");
    } else {
        // No letter chosen yet ... do nothing
    };

    print("<br><input type='submit' name='Submit' value='Confirm'>\n");
 
    print("</form>");

    print("</td></tr><tr><td>\n");

    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>\n"); 
};

function CHA_SingleMA($theChosenLetter, $theChosenNumber){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

    $table=$db->select("ScripCheckingProbe");
    foreach ($table as $row){
        $probe = $row['Probe'];
    };

    $letterExists = $theChosenLetter && $theChosenNumber;

    print("<tr>\n<td colspan='2'>");
    print("<font color='#ffffff'>");
    print("<h2>How to complete this table:</h2>");
    if (!$letterExists) {
        print("<p>Enter a letter and a number for ");
        print("the set next to Checking Script.</p>");
    };

    print("<p>If a statement is true, but the user believes it is ");
    print("false, a variable penalty is extracted. Enter the penalty ");
    print("in the box.</p>");

    print("<p>If a statement is false, leave the box at zero.");
    print("(If the user believes it is ");
    print("true, then a standard penalty (probe) of ");
    print("$probe will be extracted.)</p>");

    print("</font>");
    print("</td></tr>\n");
    print("<tr><td colspan='2' align='center'>\n");
    print("<table border='1'>\n");
    print("<form action='scripadmin.php' method='post'>\n");
    print("<input type='hidden' name='tab' value='982'>\n");
    print("<input type='hidden' name='chosenletter' ");
    print("value='$theChosenLetter'>"); 
    print("<input type='hidden' name='chosennumber' ");
    print("value='$theChosenNumber'>"); 

    // Main heading ...
    print("<tr>");
    print("<th colspan='4'>");
    print("<font color='#ffffff'>");
    print("Checking Script: ");
    if ($theChosenLetter && $theChosenNumber) {
        print($theChosenLetter.$theChosenNumber); // ... existing name
    } else { // ... new
        print("<br>Letter: <input type='text' name='chosenletter' ");
        print("maxsize='10'>");
        print(" Number: <input type='text' name='chosennumber' ");
        print(">");
    };
    print("</font></td>");
    print("</tr>\n");

    // Headings ...
    print("<tr>");
    print("<th><font color='#ffffff'>No.</font></td>");
    print("<th><font color='#ffffff'>Problem</font></td>");
//    print("<th><font color='#ffffff'>TRUE</font></td>");
    print("<th><font color='#ffffff'>Penalty</font></td>");
    print("</tr>\n");

    $condition = array('Letter'=>$theChosenLetter,
                       'NumberOfScript'=>$theChosenNumber);
    $tableMA = $db->select("ScripCheckingMA", $condition);
    foreach ($tableMA as $rowMA) {
        $qN = $rowMA['QuestionNumber'];
        $penaltyTrue[$qN] = $rowMA['PenaltyTrue'];
        $penaltyFalse[$qN] = $rowMA['PenaltyFalse'];
    };

    $tableQ = $db->select("ScripCheckingQuestions");
    foreach ($tableQ as $rowQ) {
        $questionNumber[] = $rowQ['QuestionNumber'];
    };

    // Sort into increasing (ASC) numerical (NUMERIC) order
    // of question number ...
    array_multisort($questionNumber, SORT_ASC, SORT_NUMERIC, $tableQ);

    foreach ($tableQ as $rowQ) {
        $qN = $rowQ['QuestionNumber'];
        print("<tr>");
        print("<td align='right'><font color='#ffffff'>");
        print($qN.".");
        print("</font></td>");

        print("<td><font color='#ffffff'>");
        print($rowQ['Question']);
        print("</font></td>");

//        print("<td align='center'>");
//        print("<input type='text' name='T$qN' value='$penaltyTrue[$qN]'");
//        print(">\n");
//        print("</td>\n");

        print("<td align='center'>");
        print("<input type='text' name='F$qN' value='$penaltyFalse[$qN]'");
        print(">\n");
        print("</td>\n");

        print("</tr>");
    };

    print("<tr><td align='center' colspan='4'>");
    print("<input type='submit' name='Confirm' value='Confirm'>");
    print("</td></tr>");

    print("</form></table>\n</td></tr>");
    print("<tr><td>\n");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>\n"); 
};

function CHB_UpdateMA(){
    global $_POST;

    $theChosenLetter = $_POST['chosenletter'];
    $theChosenLetter = strtoupper($theChosenLetter); // ... upper case
    $theChosenNumber = $_POST['chosennumber'];

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Is there a blank entry (actin as a place holder)? Since we are now about to enter real data,
    // we can remove the placeholder ...

    $null = "";
    $conditionPHExist = array('Letter'=>$theChosenLetter);
    $tablePHExist=$db->select("ScripCheckingMA", $conditionPHExist);

    foreach ($tablePHExist as $row) {
        if ($row['QuestionNumber'] == ''){
            // Delete row from table ...

            print("  NULL!!<br> Delete row ID ".$row['ID']);

            $conditionDelete = array('ID'=>$row['ID']);
            $table=$db->delete("ScripCheckingMA", $conditionDelete);
            // Don't attept another one!
            break;
        };
    };

    Z_DrawHeader();
    $table=$db->select("ScripCheckingQuestions");
    foreach ($table as $row) {
        // Here is a question number ...
        $qN = $row['QuestionNumber'];

print("<br>qN is $qN"."<-----------");
        if ($qN > 0) {
            // This is what the administrator wants the model answer to be ...
            $penaltyT = $_POST['T'.$qN];
            $penaltyF = $_POST['F'.$qN];

            // If there is no penaly for not choosing the item, there IS a penaly for choosing that item
            // (this is what the probe is) ...
            if (!$penaltyF) { $penaltyT = 10; };

            // Do we need to use "Update" or "Insert"?
            // Try reading this (Letter, Number, Question) combination
            $conditionExist = array('Letter'=>$theChosenLetter,
                                    'NumberOfScript'=>$theChosenNumber,
                                    'QuestionNumber'=>$qN);
            $tableExist=$db->select("ScripCheckingMA", $conditionExist);

            $entry = array ('PenaltyTrue'=>$penaltyT, 'PenaltyFalse'=>$penaltyF);
            $condition = array(
                            'Letter'=>$theChosenLetter,
                            'NumberOfScript'=>$theChosenNumber,
                            'QuestionNumber'=>$qN);

            if (count($tableExist) > 0) { // Use "Update"
print("Updating with $qN"."<-----------");
                $db->update("ScripCheckingMA", $entry, $condition);
            } else { // Use "Insert"
print("Inserting with $qN"."<-----------");
                $entry = array_merge($entry, $condition);
                $db->insert("ScripCheckingMA", $entry);
            };
        };
        
    };

    print("<tr>\n<td colspan='2'>");
    print("<font color='#ffffff'>Written to file.");
    print("</h2></font>");
    print("</td></tr>\n");
    print("<tr><td colspan='2' align='center'>\n");

//    print("<table border='1'>\n");
//    print("<tr><td></td></tr>");
//    print("</table>");

    print("</td>");
    print("</tr>");

    print("<tr><td>");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>\n"); 
    print("</td></tr>");

    print("</table >");
};

function CHC_AmendInstructions(){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

    $table = $db->select("ScripCheckingInstructions");
    foreach ($table as $row){
        $comment1 = $row['Comment1'];
        $comment2 = $row['Comment2'];
        $comment3 = $row['Comment3'];
    };

    print("<form action='scripadmin.php' method='post'>\n");
    print("<INPUT TYPE=HIDDEN NAME=tab value=991>");

    print("<tr><td>");
    print("<br><font color='#ffffff'>Comment 1:</font><br>");
    print("<textarea name='comment1' cols='80' rows='8'>");
    print($comment1);
    print("</textarea>");
    print("</td></tr>");

    print("<tr><td>");
    print("<br><font color='#ffffff'>Comment 2:</font><br>");
    print("<textarea name='comment2' cols='80' rows='8'>");
    print($comment2);
    print("</textarea>");
    print("</td></tr>");

    print("<tr><td>");
    print("<br><font color='#ffffff'>Comment 3:</font><br>");
    print("<textarea name='comment3' cols='80' rows='8'>");
    print($comment3);
    print("</textarea>");
    print("</td></tr>");

    print("<tr><td align='center' colspan='4'>");
    print("<input type='submit' name='Confirm' value='Confirm'>");
    print("</td></tr>");

    print("</form></table>\n</td></tr>");
    print("<tr><td>\n");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>\n"); 
    print("</td></tr>");

};

function CHD_UpdateInstructions($comment1, $comment2, $comment3){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

    $text = array('Comment1'=>$comment1,
                  'Comment2'=>$comment2,
                  'Comment3'=>$comment3);

    $db->update ("ScripCheckingInstructions", $text);

    print("<tr><td>");
    print("<font color='#ffffff'>Checking instructions updated</font>");
    print("</td></tr>");
    print("<tr><td>\n");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>\n"); 
    print("</td></tr>");
};

function CHE_UpdateAvailability($availability){
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

    // We have three sets of information:
    // 1. $_SESSION['tableNumberOnly'] (from ScripCheckingMA) tells us which
    //    exercises exist 
    // 2. $availability (from the user's checkboxes) tells us which
    //    exercises are to be availble
    // 3. The table ScripAvailability tell us which are currently
    //    available. 

    // We loop through $_SESSION['tableNumberOnly']; if the current element
    // exists in $availability then we write 1 to ScripAvailability;
    // if not, we write 0. 

    // There is a complication depending on whether we updating
    // or inserting.

    // Read the availability for this set ...
    $condition = array('Letter'=>$_SESSION['letterCheckingWeek']);
    $table = $db->select("ScripCheckingAvailability", $condition);

    print("<tr><td>");
    print("<font color='#ffffff'>");

    for ($i = 0; $i < count($_SESSION['tableNumberOnly']); $i++){
        if ($availability[$_SESSION['tableNumberOnly'][$i]]){
            $value = 1;
        } else {
            $value = 0;
        };

        $alreadyThere = 0;
        foreach ($table as $row){
            if ($_SESSION['tableNumberOnly'][$i] == $row['NumberOfScript']){
                $alreadyThere = 1;
                break;
            };
        };

        if ($alreadyThere){
            print("Will update ".$_SESSION['letterCheckingWeek'].$_SESSION['tableNumberOnly'][$i]." with $value<br>"); 
            // Update ...
            $condition = array('Letter'=>$_SESSION['letterCheckingWeek'],
                               'NumberOfScript'=>$_SESSION['tableNumberOnly'][$i]);
            $data = array('Available'=>$value);
            $db->update("ScripCheckingAvailability", $data, $condition);
        } else {
            print("Will insert ".$_SESSION['letterCheckingWeek'].$_SESSION['tableNumberOnly'][$i]." with $value<br>"); 
            // Insert ...
            $data = array('Available'=>$value,
                          'Letter'=>$_SESSION['letterCheckingWeek'],
                          'NumberOfScript'=>$_SESSION['tableNumberOnly'][$i]);
            $db->insert("ScripCheckingAvailability", $data);
        };
    };

//    print("Checking instructions updated");
    print("</font>");
    print("</td></tr>");
    print("<tr><td>\n");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>\n"); 
    print("</td></tr>");
};

function CHF_UpdateCategory($newCategory){


print("<br><br><br>CHF_");


    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

    // Write the category away ...

    // Read the availability for this set ...
    $condition = array('Letter'=>$_SESSION['letterCheckingWeek']);
    $table = $db->select("ScripCheckingCategory", $condition);

    print("<tr><td>");
    print("<font color='#ffffff'>");



    if (count($table) > 0){
        // Update ...
            $condition = array('Letter'=>$_SESSION['letterCheckingWeek']);
            $data = array('Category'=>$newCategory);
            $db->update("ScripCheckingCategory", $data, $condition);
    } else {
        // Insert ...
            $data = array('Letter'=>$_SESSION['letterCheckingWeek'], 'Category'=>$newCategory);
            $db->insert("ScripCheckingCategory", $data);
    };

//    print("Checking instructions updated");
    print("</font>");
    print("</td></tr>");
    print("<tr><td>\n");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>\n"); 
    print("</td></tr>");
};



function CHG_UpdateAvailability($availability, $newCategory){
    global $case;
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

    // We have three sets of information:
    // 1. $_SESSION['tableNumberOnly'] (from ScripCheckingMA) tells us which
    //    exercises exist 
    // 2. $availability (from the user's checkboxes) tells us which
    //    exercises are to be availble
    // 3. The table ScripAvailability tell us which are currently
    //    available. 

    // We loop through $_SESSION['tableNumberOnly']; if the current element
    // exists in $availability then we write 1 to ScripAvailability;
    // if not, we write 0. 

    // There is a complication depending on whether we updating
    // or inserting.

    // Read the availability for this set ...
    $condition = array('Letter'=>$_SESSION['letterCheckingWeek']);
    $table = $db->select("ScripCheckingAvailability", $condition);

    print("<tr><td>");
    print("<font color='#ffffff'>");

    for ($i = 0; $i < count($_SESSION['tableNumberOnly']); $i++){
        if ($availability[$_SESSION['tableNumberOnly'][$i]]){
            $value = 1;
        } else {
            $value = 0;
        };

        $alreadyThere = 0;
        foreach ($table as $row){
            if ($_SESSION['tableNumberOnly'][$i] == $row['NumberOfScript']){
                $alreadyThere = 1;
                break;
            };
        };

        if ($alreadyThere){
            print("Will update ".$_SESSION['letterCheckingWeek'].$_SESSION['tableNumberOnly'][$i]." with $value<br>"); 
            // Update ...
            $condition = array('Letter'=>$_SESSION['letterCheckingWeek'],
                               'NumberOfScript'=>$_SESSION['tableNumberOnly'][$i]);
            $data = array('Available'=>$value);
            $db->update("ScripCheckingAvailability", $data, $condition);
        } else {
            print("Will insert ".$_SESSION['letterCheckingWeek'].$_SESSION['tableNumberOnly'][$i]." with $value<br>"); 
            // Insert ...
            $data = array('Available'=>$value,
                          'Letter'=>$_SESSION['letterCheckingWeek'],
                          'NumberOfScript'=>$_SESSION['tableNumberOnly'][$i]);
            $db->insert("ScripCheckingAvailability", $data);
        };
    };

    // Read the availability for this set ...
    $condition = array('Letter'=>$_SESSION['letterCheckingWeek']);
    $table = $db->select("ScripCheckingCategory", $condition);

    print("<tr><td>");
    print("<font color='#ffffff'>");



    if (count($table) > 0){
        // Update ...
            $condition = array('Letter'=>$_SESSION['letterCheckingWeek']);
            $data = array('Category'=>$newCategory);
            $db->update("ScripCheckingCategory", $data, $condition);
    } else {
        // Insert ...
            $data = array('Letter'=>$_SESSION['letterCheckingWeek'], 'Category'=>$newCategory);
            $db->insert("ScripCheckingCategory", $data);
    };



//    print("Checking instructions updated");
    print("</font>");
    print("</td></tr>");
    print("<tr><td>\n");
    print("<a href='scripadmin.php?tab=5'>Return to Menu</a>\n"); 
    print("</td></tr>");
};



function Z_TimesAndPlaces($uniqueTime, $uniquePlace){
    // Change availability of all time (e.g. third year) and place (e.g. Malaysia) combinations:

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    global $_POST;

    Z_DrawHeader();

    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>This is the table that the moderator will see: some options are enabled, some disabled and some entirely absent.</h2></font>");
    print("</td></tr>");

    print("<tr><td colspan='2' align='center'>");

    // Can choose existing times and places from two sources - Source 1: ScripTimesAndPlaces ...
    $table = $db->select("ScripTimesAndPlaces");
    foreach ($table as $row){
        $PK = $row['PK'];
        $time[] = $row['Time'];
        $place[] = $row['Place'];
        $available[] = $row['Available'];
    };

    // Can choose existing times and places from two sources - Source 2: ScripStudent ...
    $table = $db->select("ScripStudent");
    foreach ($table as $row){
        $YG[] = $row['YearGroup'];
        $L[] = $row['Location'];
    };

    // Now construct lists of times and places (but only do it once) ...
    if (!count($uniqueTime)){
        $uniqueTime = array();

        // Compile a list of times from ScripTimesAndPlaces ...
        for ($i = 0; $i < count($time); $i++){
            $new = 1;
            for ($j = 0; $j < count($uniqueTime); $j++){
                if ($time[$i] == $uniqueTime[$j]){
                    $new = 0;
                };
            };
            if ($new){
                // Add to $uniqueTime
                array_push($uniqueTime, $time[$i]);
            };
        };

        // Compile a list of times from ScripStudent ...
        for ($i = 0; $i < count($YG); $i++){
            $new = 1;
            for ($j = 0; $j < count($uniqueTime); $j++){
                if ($YG[$i] == $uniqueTime[$j]){
                    $new = 0;
                };
            };
            if ($new){
                // Add to $uniqueTime
                array_push($uniqueTime, $YG[$i]);
            };
        };
    };

    if (!count($uniquePlace)){
        $uniquePlace = array();

        // Compile a list of places from ScripTimesAndPlaces ...
        for ($i = 0; $i < count($place); $i++){
            $new = 1;
            for ($j = 0; $j < count($uniquePlace); $j++){
                if ($place[$i] == $uniquePlace[$j]){
                    $new = 0;
                };
            };
            if ($new){
                // Add to $uniquePlace
                array_push($uniquePlace, $place[$i]);
            };
        };

        // Compile a list of places from ScripStudent ...
        for ($i = 0; $i < count($L); $i++){
            $new = 1;
            for ($j = 0; $j < count($uniquePlace); $j++){
                if ($L[$i] == $uniquePlace[$j]){
                    $new = 0;
                };
            };
            if ($new){
                // Add to $uniquePlace
                array_push($uniquePlace, $L[$i]);
            };
        };
    };


    // Build an example table (such as a moderator will see) ...
    print("<form>");
    $counter = 0;
    print("<center><br><table border ='1'>");
    for ($i = 0; $i < count($uniquePlace) + 1; $i++){
        print("<tr>");
        for ($j = 0; $j < count($uniqueTime) + 1; $j++){
            print("<td>");
            if ($i == 0){ // Title row with time headers
                if ($j == 0){ // First cell
                    // Empty cell
                } else { // Later cell
                    print("<font color='#ffffff'>" . $uniqueTime[$j - 1] . "</font>"); // ... the headers
                };
            } else { // All later rows
                if ($j == 0){ // First cell in a later row: places
                    print("<font color='#ffffff'>" . $uniquePlace[$i - 1] . "</font>"); // ... column with headers for places
                } else { // Later cells in later rows: availability
                    $condition = array('Time'=>$uniqueTime[$j - 1], 'Place'=>$uniquePlace[$i - 1]);
                    $table = $db->select("ScripTimesAndPlaces", $condition);

                    $X = "-";
                    foreach ($table as $row){
                        $X = $row['Available'];
                    };

                    if ($X == 0 || $X == 1){
                        $chosenPlace[$counter] = $uniquePlace[$i - 1];
                        $chosenTime[$counter] = $uniqueTime[$j - 1];
                    };

                    if ($X == "-") {
                        print("&nbsp;");
                    } else {
                        print("<input type='radio' name='combination' value'" . $counter . "'");
                        if (!$X) { print(" disabled='disabled'"); };
                        print(">");
                    };
                };
            };
            print("</td>");
            if ($X) { $counter++; };
        };
        print("</tr>");
    };
    print("</table></center>");
    print("</form>");

    print("<font color='#ffffff'><p>You can control the state of each cell with these drop-down menus: choose a time and a place and then choose the availablility.</p></font>");

    // Now show drop-down lists for changing the table. This form takes the data and sends it to the table ...
    print("<form action='scripadmin.php' method='post'>");
    print("<input type='hidden' name='tab' value=872>");

    print("<input type='hidden' name='uniquetime' value=$uniqueTime>");
    print("<input type='hidden' name='uniqueplace' value=$uniquePlace>");

    print("<select name='timeChosen'>");
    for ($i = 0; $i < count($uniqueTime); $i++){
        print("<option value='$uniqueTime[$i]'>$uniqueTime[$i]</option>");
    };
    print("</select>");

    print("&nbsp;");

    print("<select name='placeChosen'>");
    for ($j = 0; $j < count($uniquePlace); $j++){
        print("<option value='$uniquePlace[$j]'>$uniquePlace[$j]</option>");
    };
    print("</select>");

    print("&nbsp;");

    // We also want the drop-down list for Availabilty to show the correct setting for the first option in the first two columns ...
    $settingFirstItem = "";
    $condition = array ('Time'=>$uniqueTime[0], 'Place'=>$uniquePlace[0]);
    $table = $db->select("ScripTimesAndPlaces", $condition);
    foreach ($table as $row){
        $settingFirstItem = $row['Available'];
    };

    print("<select name='settingChosen'>");

    print("<option value='-'");
    if ($settingFirstItem == "" || $settingFirstItem == "-") { print(" selected='selected'"); };
    print(">N/A</option>");
    // ... the null string and "-" options cover no entry or an entry set at "-"

    print("<option value='0'");
    if ($settingFirstItem == "0") { print(" selected='selected'"); };
    print(">Disabled</option>");

    print("<option value='1'");
    if ($settingFirstItem == "1") { print(" selected='selected'"); };
    print(">Enabled</option>");

    print("</select>");

    print("&nbsp;");

    print("<input type='submit' value='Submit'>");

    print("</form>");

/*
    // Form 2: Ability to add new values ...
    print("<font color='#ffffff'><p>You can also add new options into the time and place menus. ");
    print("Specify the menu with the radio buttons below and then enter the name of the new item in the text box.</p>");
    print("<p>Once a new item has been added, use the menus above to assign the availability of the new cells that will have been added to the table. ");
    print("If you don't assign any of the new cells, the new value will not be kept.</p></font>\n\n");

    print("<form action='scripadmin.php' method='post'>\n\n");
    print("<input type='hidden' name='tab' value=874>\n\n");

    print("<input type='hidden' name='uniqueplace' value='" . implode("|", $uniquePlace) . "'>\n\n");
    print("<input type='hidden' name='uniquetime' value='" . implode("|", $uniqueTime) . "'>\n\n");

    print("<input type='radio' name='menu' value='time'><font color='#ffffff'>Time</font>");
    print("&nbsp;");
    print("<input type='radio' name='menu' value='place' checked='checked'><font color='#ffffff'>Place</font>");
    print("<br>");
    print("<font color='#ffffff'>New item: </font><input type='text' name='newmenuitem'>");
    print("&nbsp;");
    print("<input type='submit' value='Submit'>");

    print("</form>");
*/
};




function Q_WeeklySummary(){

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    global $_POST;
    $week = $_POST['week'];

    if (!$week){
        // Don't have a week number; get one ...
        print("<tr><td colspan='2'>");
        print("<font color=#'ffffff'><h2>Choose a week</h2></font>");
        print("</td></tr>");

        print("<tr><td colspan='2' align='center'><table border='1'>");
        print("<form action=scripadmin.php method='post'>");
        print("<input type='hidden' name='tab' value=777>");
        print("<tr><th>&nbsp;</th>");
        print("<th><font color='#ffffff'>Week</font></th>");
        print("</tr>");

        $nWeeks = 8;
        for ($i = 1; $i < $nWeeks + 1; $i++){
            $week = $i;
            print("<tr>");
            print("<td><input type='radio' name='week' value='$i'></td>");
            print("<td><font color='#ffffff'>$week</font></td>");
            print("</tr>");
        };

        print("<tr><td colspan='8' align=center><input type='submit'></td></tr>");
        print("</form></table></td></tr>");

    } else {

        $patternTable = $db->select("ScripTermPattern");
        foreach ($patternTable as $row){
            $patterns[$row['PracticeCode']] = $row['wk'.$week];
        };

        $kk= 0;
        $condition = array('RealStudent'=>'1');
        $table = $db->select("ScripStudent", $condition);
        foreach ($table as $row){
            $summaryData[$kk]['student'] = $row['StudentID'];
            $summaryData[$kk]['letterCode'] = $patterns[$row['PracticeCode']];
            $requiredCombination[$row['StudentID']] = $patterns[$row['PracticeCode']];
            // ... e.g. if student 314 is doing TY this week, then $requiredCombination[314] = TY
            $kk++;
        };

        for ($i = 0; $i < count($summaryData); $i++) {
            $guide[$i] = $summaryData[$i]['student'];
        };
        array_multisort($guide, SORT_ASC, SORT_NUMERIC, $summaryData);
        // ... we now have a list of students and two-letter codes in increasing numerical order of code - all results that have these are to be counted.

        // Look at the data in ScripResultsModerated ...
        $table = $db->select("ScripResultsModerated");
        // Want to sort in student order ...
        foreach ($table as $row){
            $guide1[] = $row['StudentID'];
        };
        array_multisort($guide1, SORT_ASC, SORT_NUMERIC, $table);
        // ... also now in increasing numerical order of student

        // Now look through the newly ordered ScripResultsModerated ... 
        foreach ($table as $row){
            $studentID = $row['StudentID'];
            $caseID = $row['CaseID'];
            $twoLetters = substr($caseID, 0, 2);
            if ($requiredCombination[$studentID] == $twoLetters){
               // Include this result in the summary...
               $genericComment = $row['GenericComment'];
               $tab = $row['Tab'];
               $gC[$genericComment]++;
               $t[$tab]++;
             };       
        };


        // Now look at the place where the other generic comments are stored () ...
        $table = $db->select("ScripOTCModerated");
        // Want to sort in student order ...
        foreach ($table as $row){
            $guide2[] = $row['StudentID'];
        };
        array_multisort($guide2, SORT_ASC, SORT_NUMERIC, $table);
        // ... also now in increasing numerical order of student

        // Now look through the newly ordered ScripOTCModerated ... 
        foreach ($table as $row){
            $studentID = $row['StudentID'];
            $caseID = $row['CaseID'];
            $twoLetters = substr($caseID, 0, 2);
            if ($requiredCombination[$studentID] == $twoLetters){
                // Include this result in the summary...
                $explodeGC = trim($row['GenericComment']);

                // If we have something, we break it up and increment each total ...
                // (Remember that GenericComment in ScripOTCModerated can be a sequence of numbers separated by a space.)
                if ($explodeGC){
                    $explode = explode(" ", $explodeGC);
                    // Add the elements of $explode to $genericComment ...
                    for ($ik = 0; $ik < count($explode); $ik++){
                        $genericComment = $explode[$ik];
                        // Other table has its own tabs; invent "DI" [Demonstrator Input for this one] ...
                        $tab = "DI";
                        $gC[$genericComment]++;
                        $t[$tab]++;
                    };
                };
            };       
        };

        // What are the tabs (and in what order), including DI?
        $theTabs = array("Classify", "PMR", "Verify", "CD", "POR", "Label", "Ancillaries", "Endorse", "File", "DI"); 

        $practiceWeek = $week -1;
        print("<tr><td><font color='#ffffff'><b>This is what the whole class did in Week $week (Practice Week $practiceWeek)</b></font></td></tr>");

        for ($i = 0; $i < count($theTabs); $i++){
            print("<tr>");
            print("<td>");
            print("<font color='#ffffff'>".$theTabs[$i]."</font>");
            print("</td>");
            print("<td>");
            print("<font color='#ffffff'>".$t[$theTabs[$i]]."</font>");
            print("</td>");
            print("</tr>");
        };

        // A blank line ...
        print("<tr><td>&nbsp;</td></tr>");

        // Read the generic comments table ...
        $table = $db->select("ScripGenericComments");

       $highestID = 0;
        // Find the highest ID ...
        foreach ($table as $row){
            $ID = $row['ID'];
            if ($highestID < $ID){ $highestID = $ID; };
        };


        for ($i = 1; $i < $highestID + 1; $i++){
            // If an entry with this number exists, show it ...
            if ($table[$i - 1][ID]){
                print("<tr><td>");
                print("<font color='#ffffff'>".$table[$i-1]['GenericComment']."</font>");
                print("</td>");

                print("<td>");
                if ($gC[$i]){ $number = $gC[$i]; } else { $number = 0; };
                print("<font color='#ffffff'>".$number."</font>");
                print("</td></tr>");
            };
        };

    };

    print("<tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a></td></tr></table>");


};


/*
function I_EditTrustData() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the trusts and display as a table
    print("<TR><TD colspan=2>");
    print("<FONT COLOR=FFFFFF><H2>Select one trust from the list and click the submit button to edit the details</H2></FONT>");
    print("</TD></TR>");

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=66>");
    print("<TR><TH>&nbsp;</TH>");
    print("<th><font color=FFFFFF>ID</font></th>");
    print("<th><font color=FFFFFF>Code</font></th>");
    print("<th><font color=FFFFFF>T or H?</font></th>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td><INPUT TYPE=RADIO NAME=ID VALUE='0' checked></td>");
    print("<td><FONT COLOR=FFFFFF>New</td>");
    print("</tr>");

    $table=$db->select("ScripTrust");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
	$theCode=$row['Code'];
	$theTrustOrHospital=$row['TrustOrHospital'];
	$theAddress1=$row['Address1'];
	$theAddress2=$row['Address2'];
	$theAddress3=$row['Address3'];
	$theAddress4=$row['Address4'];
        print("<TD><INPUT TYPE=RADIO NAME=ID VALUE=$theID></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theID</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theCode</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>");
        if ($theTrustOrHospital){
            print("Trust");
        } else {
            print("Hospital");
        };
	print("</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress1&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress2&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress3&nbsp;</FONT></TD>");
	print("<TD><FONT COLOR=FFFFFF>$theAddress4&nbsp;</FONT></TD>");
        print("</TR>");
    };

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};

function IA_EditSingleTrust($SID) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the trust and display in a form
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripTrust",$condition);
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theCode=$row['Code'];
            $theTrustOrHospital=$row['TrustOrHospital'];
            $theName=$row['Name'];
            $theAddress1=$row['Address1'];
            $theAddress2=$row['Address2'];
            $theAddress3=$row['Address3'];
            $theAddress4=$row['Address4'];
            $thePostCode=$row['PostCode'];
            $theTelNo=$row['TelephoneNumber'];
        };
    } else {
        $theCode="";
	$theTrustOrHospital="";
        $theName="";
        $theAddress1="";
	$theAddress2="";
	$theAddress3="";
	$theAddress4="";
        $thePostCode="";
        $theTelNo="";
    };

    print("<TR><TD colspan=2 align=center><TABLE BORDER=1>");
    print("<FORM ACTION=scripadmin.php METHOD=POST>");
    print("<INPUT TYPE=HIDDEN NAME=tab value=67>");
    print("<INPUT TYPE=HIDDEN NAME=ID value=$SID>");

    print("<TR>");
    print("<TH><FONT COLOR=FFFFFF>Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>T or H?</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Name</FONT></TH>");
    print("</tr>");

    print("<tr>");
    print("<td>");
    print("<input type=text name=thecode value='$theCode' size='35'>");
    print("</td>");
    print("<td>");
    print("<select name=trustorhospital>");
    print("<option value=''>Select from ...");
    print("<option value='1'");
    if ($theTrustOrHospital == '1') { print(" selected"); };
    print(">Trust");
    print("<option value='0'");
    if ($theTrustOrHospital == '0') { print(" selected"); };
    print(">Hospital");
    print("</td>");
    print("<td>");
    print("<input type=text name=thename value='$theName' size='35'>");
    print("</td>");
    print("</tr>");

    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Address1</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address2</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address3</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Address4</FONT></TH>");
    print("</tr>");
    print("<tr><TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress1 VALUE='$theAddress1' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress2 VALUE='$theAddress2' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress3 VALUE='$theAddress3' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=theaddress4 VALUE='$theAddress4' SIZE=35>");
    print("</TD></tr>");


    print("<tr>");
    print("<TH><FONT COLOR=FFFFFF>Post Code</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Telephone Number</FONT></TH>");
    print("</tr>");
    print("<tr><TD>");
    print("<INPUT TYPE=TEXT NAME=thepostcode VALUE='$thePostCode' SIZE=35>");
    print("</TD>");
    print("<TD>");
    print("<INPUT TYPE=TEXT NAME=thetelno VALUE='$theTelNo' SIZE=35>");
    print("</TD></tr>");



    print("<TR><TD colspan=4 align=center>");
    print("<INPUT TYPE=SUBMIT VALUE=Update></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A>");
};

function IB_UpdateTrust($SID,$theCode,
         $theTrustOrHospital,$theName,
         $theAddress1, $theAddress2,
         $theAddress3, $theAddress4,
         $thePostCode, $theTelNo) {
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 


    // Update details of the trust ...
    $data= array(
        "Code" => $theCode,
        "TrustOrHospital" => $theTrustOrHospital,
	"Name" => $theName,
	"Address1" => $theAddress1,
	"Address2" => $theAddress2,
	"Address3" => $theAddress3,
	"Address4" => $theAddress4,
	"PostCode" => $thePostCode,
	"TelephoneNumber" => $theTelNo
	);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripTrust",$data, $condition);
    } else {
        $db->insert("ScripTrust",$data);
    };

    print("<TR><TD colspan=2><FONT COLOR=FFFFFF>Trust updated");
    print("</FONT></TD></TR>");
    print("<TR><TD><A HREF=scripadmin.php?tab=5>Return to Menu</A>");
    print("</TD></TR></TABLE>");
};
*/

function CQ_EditCalculationQuestions(){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the trusts and display as a table
    print("<tr>");
    print("<td colspan='2'><font color='#ffffff'>");
    print("<h2>Select one question from the list and click the submit button to edit the details.</h2>");
    print("The ID is a unique internal identifier; the question number is the number that the student sees. The exercise number is not in use at present � set it to 1.");
    print("</font></td>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2' align='center'><table border='1'>");

    print("<form action=scripadmin.php method='post'>");
    print("<input type=hidden name=tab value=971>");

    print("<tr>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Exercise Set</font></th>");
    print("<th><font color='#ffffff'>Exercise Number</font></th>");
    print("<th><font color='#ffffff'>Question Number</font></th>");
    print("<th><font color='#ffffff'>Question Text</font></th>");
    print("</tr>");

    print("<tr><td colspan='8' align='center'><input type='submit'></td></tr>");

    print("<tr>");
    print("<td><input type='radio' name='ID' value='0' checked><font color='#FFFFFF'>New</td>");
    print("</tr>");

    $table=$db->select("ScripCalculationsQuestions");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        $theExerciseSet=$row['ExerciseSet'];
        $theExerciseNumber=$row['ExerciseNumber'];
        $theQuestionNumber=$row['QuestionNumber'];
        $theQuestionText=$row['QuestionText'];
        print("<td><input type='radio' name='ID' value=$theID> <font color='#ffffff'>$theID</font></td>");
        print("<td align='center'><font color='#ffffff'>$theExerciseSet &nbsp;</font></td>");
        print("<td align='center'><font color='#ffffff'>$theExerciseNumber &nbsp;</font></td>");
        print("<td align='center'><font color='#ffffff'><b>$theQuestionNumber &nbsp;</b></font></td>");
        print("<td><font color='#ffffff'>$theQuestionText &nbsp;</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan='8' align='center'><input type='submit'></td></tr>");
    print("</form>");
    print("</table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function CQ_EditSingleCalculationQuestion($SID){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the question and display in a form
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripCalculationsQuestions", $condition);
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theID=$row['ID'];
            $theExerciseSet=$row['ExerciseSet'];
            $theExerciseNumber=$row['ExerciseNumber'];
            $theQuestionNumber=$row['QuestionNumber'];
            $theQuestionText=$row['QuestionText'];
        };
    } else {
        $theID="";
        $theExerciseSet="";
        $theExerciseNumber="";
        $theQuestionNumber="";
        $theQuestionText="";
    };

    print("<tr><td colspan='2' align='center'><font color='#ffffff'>");
    print("Set the Exercise Number to 1. The Exercise Set is the two-letter code, the Question Number is between 1 and 10.");
    print("</font>");
    print("<table border='1'>");
    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name='tab' value=972>");
    print("<input type='hidden' name='ID' value=$SID>");

    // First row ...
    print("<tr>");
    print("<th><font color='#ffffff'>Exercise Set</font></th>");
    print("<th><font color='#ffffff'>Exercise Number</font></th>");
    print("<th><font color='#ffffff'>Question Number</font></th>");
    print("</tr>");
    print("<tr>");
    print("<td><input type='text' name=theexerciseset value='$theExerciseSet'></td>");
    print("<td><input type='text' name=theexercisenumber value='$theExerciseNumber'></td>");
    print("<td><input type='text' name=thequestionnumber value='$theQuestionNumber'></td>");
    print("</tr>");

    // Second row ...
    print("<tr><th colspan='3'><font color='#ffffff'>Question Text</font></th></tr>");
    print("<tr><td colspan='3'><textarea rows='4' cols='100' name=thequestiontext>$theQuestionText</textarea></td></tr>");

    print("<tr><td colspan='4' align='center'>");
    print("<input type='submit' value='Update'></td></tr>");

    print("</form>");
    print("</table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function CQ_UpdateCalculationQuestion($SID, $theExerciseSet, $theExerciseNumber, $theQuestionNumber, $theQuestionText){

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the trust ...
    $data= array(
        "ExerciseSet" => $theExerciseSet,
        "ExerciseNumber" => $theExerciseNumber,
        "QuestionNumber" => $theQuestionNumber,
        "QuestionText" => $theQuestionText
    );

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripCalculationsQuestions", $data, $condition);
    } else {
        $db->insert("ScripCalculationsQuestions", $data);
    };

    print("<tr><td colspan='2'><font color='#ffffff'>Question updated</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};



function CA_EditCalculationAnswers(){
    // Show all the questions that exist and all answers for any given question in just one box; user
    // chooses one question; all answers for that question are then shown 

    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the answers and display as a table ...
    print("<tr>");
    print("<td colspan='2'><font color='#ffffff'><h2>Select one question from the list and click the submit button to edit its answers.</h2></font></td>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2' align='center'><table border='1'>");

    print("<form action=scripadmin.php method='post'>");
    print("<input type=hidden name=tab value=976>");

    // Headings ...
    print("<tr>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Exercise Set and Number</font></th>");
    print("<th><font color='#ffffff'>Question (with number)</font></th>");
    print("<th><font color='#ffffff'>Model Answers</font></th>");
    print("</tr>");

//    print("<tr>");
//    print("<td><input type='radio' name='ID' value='0' checked><font color='#ffffff'>New</td>");
//    print("</tr>");

    $table=$db->select("ScripCalculationsQuestions");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        $theExerciseSet=$row['ExerciseSet'];
        $theExerciseNumber=$row['ExerciseNumber'];
        $theQuestionNumber=$row['QuestionNumber'];
        $theQuestionText=$row['QuestionText'];

        // Use the ID (as QuestionID in ScripCalculationsMA) to find all the model answers for each question ...
        $allMA = "";
        $condition = array('QuestionID'=>$theID);
        $MAtable=$db->select("ScripCalculationsMA", $condition);
        // Show non-null model answers ...
        foreach ($MAtable as $MArow){
            $currentMA = $MArow['MA'];
            if ($currentMA != ""){
                if ($allMA == ""){
                    $allMA .= $currentMA;
                } else {
                    $allMA .= " -- " . $currentMA;
                };
            };
        };

        print("<td><input type='radio' name=ID value=$theID> <font color='#ffffff'>$theID</font></td>");
        print("<td><font color='#ffffff'>$theExerciseSet - $theExerciseNumber&nbsp;</font></td>");
        print("<td><font color='#ffffff'>($theQuestionNumber) $theQuestionText &nbsp;</font></td>");
        print("<td><font color='#ffffff'>$allMA &nbsp;</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan='8' align='center'><input type='submit'></td></tr>");
    print("</form>");
    print("</table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};

function CA_EditSingleCalculationAnswer($SID){


    // This function is rather different to the other functions that appear to run in the samne way ...
    // The parameter $SID is the ID of the question (ID in the Questions table) and QuestionID in the MA table 

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    Z_DrawHeader();

/*
    // Get details of the question and display in a form ...
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripCalculationsMA", $condition);
    if ($SID!='0') {
        foreach ($table as $row) {
            $theID=$row['ID'];
            $theQuestionID=$row['QuestionID'];
            $theMA=$row['MA'];
        };
    } else {
        $theID="";
        $theQuestionID="";
        $theMA="";
    };
*/
    // Find the text of the question ...
    $condition=array('ID'=>$SID);
    $qtable=$db->select("ScripCalculationsQuestions", $condition);
    foreach ($qtable as $qrow){
        $qText = $qrow['QuestionText'];
        $qNumber = $qrow['QuestionNumber'];
    };

    // Write details of the question ...
    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Question ID (internal only), Question Number (shown to student) and Question Text</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td colspan='2'><font color='#ffffff'><i>$SID</i> �� <b>($qNumber)</b> $qText</font></td>");
    print("</tr>");

    // Instructions ...
    print("<tr>");
    print("<th colspan='2'><font color='#ffffff'>Amend existing answers and/or add a new one.</font></th>");
    print("</tr>");

    // Begin the form ...
    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action=scripadmin.php method=post>");
    print("<input type='hidden' name='tab' value=977>");
    print("<input type='hidden' name='thequestionID' value=$SID>");

    // Headings ...
    print("<tr>");
    print("<th><font color='#ffffff'>Model Answer(s)</font></th>");
    print("</tr>");

    // Read all the model answers ...
    $condition = array('QuestionID'=>$SID);
    $MAtable=$db->select("ScripCalculationsMA", $condition);
    $k = 0;
    foreach ($MAtable as $MArow){
        // Read each ID and write it into an array
        $theID = $MArow['ID'];
        $theMA = $MArow['MA'];
        if ($theMA != "") {
            print("<input type='hidden' name=ID[".$k."] value=$theID>");    
            print("<tr>");
            print("<td><input type='text' name=thema[".$k."] value=$theMA></td>");
            print("</tr>");
            $k++;
        };
    };

    // ... plus three further boxes (blank) for new answers (only written to table if not blank) ...
    print("<tr>");
    print("<input type='hidden' name='ID[".$k."]' value='0'>");
    print("<td><input type='text' name=thema[".$k."] value=''></td>");
    print("</tr>");
    $k++;
    print("<tr>");
    print("<input type='hidden' name='ID[".$k."]' value='0'>");
    print("<td><input type='text' name=thema[".$k."] value=''></td>");
    print("</tr>");
    $k++;
    print("<tr>");
    print("<input type='hidden' name='ID[".$k."]' value='0'>");
    print("<td><input type='text' name=thema[".$k."] value=''></td>");
    print("</tr>");

    print("<tr><td colspan='4' align='center'>");
    print("<input type='submit' value='Update'></td></tr>");

    print("</form>");
    print("</table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function CA_UpdateCalculationAnswer($ID, $theQuestionID, $theMA){

    // Write details of the answer to the database ...
    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    Z_DrawHeader();

    // If a model answer is blank and its ID is zero, expunge it from $theMA and the its matching ID from $ID (because it is a new
    // answer that isn't (so to speak)) ...
    for ($i = 0; $i < count($theMA); $i++){
        if (trim($theMA[$i]) == "" && ($theID[$i] == "0" || $theID[$i] == "")){
            array_splice($theMA, $i, $i + 1);
            array_splice($ID, $i, $i + 1);
        };
    };
    // ... if it is blank and has a non-zero ID then it was once a non-null answer.

    // Write whatever is left over to the database. We may have more than one model answer - this is an array ...
    for ($i = 0; $i < count($ID); $i++){
        $data= array("QuestionID" => $theQuestionID, "MA" => $theMA[$i]);

        if ($ID{$i} != '0') {
            $condition=array('ID'=>$ID[$i]);
            $db->update("ScripCalculationsMA", $data, $condition);
        } else {
            $db->insert("ScripCalculationsMA", $data);
        };
    };

    print("<tr><td colspan='2'><font color='#ffffff'>Answer updated</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr>");
    print("</table>");
};




function AZA_GetComingYear() {
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Ask for current year ...
    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>What is the coming academic year (i.e. the one about to start)?</h2></font>");
    print("</td></tr>");

    // Clarify the question ...
    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h3>If the question doesn't make sense e.g. because it's currently Christmas, then you probably shouldn't be using this function at this time! Seek help.</h3></font>");
    print("</td></tr>");

    // Ask PHP about current year ...
    $yearCurrent = date(Y); 
    print("<tr><td colspan='2' align='center'><table border='1'>");
    print("<form action=scripadmin.php method=post>");
    print("<input type='hidden' name=tab value='592'>");

    print("<tr><th><font color='#ffffff'>Coming Year</font></th></tr>");

    print("<tr>");
    print("<td>");
    print("<select>");
    for ($i = 0; $i < 6; $i++){
        $y = $yearCurrent - 2 + $i;
        $yp1 = $y + 1;
        print("<option>" . $y . "-" . $yp1 . "</option>");
    };
    print("</select>");
    print("</td>");
    print("</tr>");

    print("<TR><TD colspan=8 align=center><INPUT TYPE=SUBMIT value='Submit Year'></TD></TR>");
    print("</FORM></TABLE></TD></TR><TR><TD>");
    print("<A HREF=scripadmin.php?tab=5>Return to Menu</A></TD></TR></TABLE>");
};



function AZA_SetYear($yearComing){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    $yCp1 = $yearComing + 1;

    // Confirm what is happening ...
    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h3>All real students with Last Updated NOT set to " . $yearComing . "-" . $yCp1 . " will have Real Student set to 99.</h3></font>");
    print("</td></tr>");

    $table=$db->select("ScripStudent");         
    foreach ($table as $row){
        // Look for real students (!=0) whose LastUpdated is not equal to the argument presented to this function ...
        if ($row['RealStudent'] != 0 && $row['LastUpdated'] != $yearComing){
            // In these cases, set Real Student to 99 (regardless of its current value) ...
            $data = array ('RealStudent'=>'99');
            $condition = array ('StudentID'=>$row['StudentID']);
            $db->update("ScripStudent", $data, $condition);
        };
    };

    print("<tr><td colspan='8' align='center'><input type='submit' value='Continue'></td></tr>");
    print("</form></table></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a></td></tr></table>");
};




function AAA_EditAdmin(){
    Z_DrawHeader();

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of all the administrators and display as a table ...
    print("<tr><td colspan='2'>");
    print("<font color='#ffffff'><h2>Select one administrator from the list and click the submit button to edit the details</h2></font>");
    print("</td></tr>");

    print("<tr><td colspan='2' align=center><table border='1'>");
    print("<form action=scripadmin.php method='post'>");
    print("<input type='hidden' name='tab' value='16'>");

    print("<tr>");
    print("<th>&nbsp;</th>");
    print("<th><font color='#ffffff'>ID</font></th>");
    print("<th><font color='#ffffff'>Usual ID</font></th>");
    print("<th><font color='#ffffff'>Level</font></th>");
    print("<th><font color='#ffffff'>Name</font></th>");
    print("</tr>");

    print("<tr>");
    print("<td><input type='radio' name='ID' value='0' checked></td>");
    print("<td><font color='#ffffff'>New</td>");
    print("</tr>");

    $table=$db->select("ScripAdmin");
    foreach ($table as $row) {
        print("<tr>");
        $theID=$row['ID'];
        $theSuperUserID=$row['SuperUserID'];
        $theLevel=$row['Level'];

        // Find the name from the ScripStudent table ...
        $condition = array('StudentID'=>$theSuperUserID);
        $sTable=$db->select("ScripStudent", $condition);
        foreach ($sTable as $sRow){
            $firstName = $sRow['FirstName'];
            $lastName = $sRow['LastName'];
        };

        print("<td><input type='radio' name='ID' value='$theID'></td>");
        print("<td><font color='#ffffff'>$theID</font></td>");
        print("<td><font color='#ffffff'>$theSuperUserID</font></td>");
        print("<td><font color='#ffffff'>$theLevel</font></td>");
        print("<td><font color='#ffffff'>$firstName $lastName</font></td>");
        print("</tr>");
    };

    print("<tr><td colspan='8' align='center'><input type='submit' value='Choose'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a></td></tr></table>");
};

function AAA_EditSingleAdmin($SID){
    // 

    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Get details of the administrator and display in a form ...
    $condition = array('ID'=>$SID);
    $table=$db->select("ScripAdmin", $condition);
    Z_DrawHeader();

    if ($SID!='0') {
        foreach ($table as $row) {
            $theID=$row['ID'];
            $theSuperUserID=$row['SuperUserID'];
            $theLevel=$row['Level'];
        };
    } else {
        $theID="";
        $theSuperUserID="";
        $theLevel="";
    };

    print("<tr><td colspan='2' align='center'><font color='#ffffff'>These are what different types of adminstrator can do:</font></td></tr>");

    print("<TR><TD colspan=2 align=center>");

    print("<table border='1'>");
    print("<tr><th><font color='#ffffff'>Level</font></th><th><font color='#ffffff'>Can do</font></th></tr>");
    print("<tr><td><font color='#ffffff'>99</font></td><td><font color='#ffffff'>Full administrator - can do everything</font></td></tr>");
    print("<tr><td><font color='#ffffff'>88</font></td><td><font color='#ffffff'>Combination of 77 and 33</font></td></tr>");
    print("<tr><td><font color='#ffffff'>77</font></td><td><font color='#ffffff'>Academic viewer</font></td></tr>");
    print("<tr><td><font color='#ffffff'>55</font></td><td><font color='#ffffff'>External examiner (exam only)</font></td></tr>");
    print("<tr><td><font color='#ffffff'>33</font></td><td><font color='#ffffff'>Demonstrator and marker and inputter</font></td></tr>");
    print("<tr><td><font color='#ffffff'>22</font></td><td><font color='#ffffff'>Demonstrator and marker</font></td></tr>");
    print("<tr><td><font color='#ffffff'>11</font></td><td><font color='#ffffff'>Demonstrator</font></td></tr>");
    print("</table>");

    print("</td></tr>");

    print("<tr><td colspan='2' align='center'>&nbsp;</td></tr>");

    print("<tr><td colspan='2' align='center'>");

    print("<table border='1'>");
    print("<FORM ACTION=scripadmin.php method='post'>");

    print("<input type='hidden' name='tab' value='17'>");
    print("<input type='hidden' name='ID' value='$SID'>");

    // Headings ...
    print("<tr>");
    print("<th><font color='#FFFFFF'>ID</font></th>");
    print("<TH><FONT COLOR=FFFFFF>ID as Student</FONT></TH>");
    print("<TH><FONT COLOR=FFFFFF>Level</FONT></TH>");
    print("</tr>");

    // Data: ID - fixed text ...
    print("<tr>");
    print("<td><font color='#ffffff'>$theID</font></td>");

    // Data: A ! username - choose from a drop down list ...
    // This box requires a default ID (a StudentID from ScripStudent). If the user has chosen a new ID, then it is
    // that; if we're editing an existing ID then it is that, otherwise it is zero
    $defaultLevel = 0;
    if ($_POST['administrator'] && $_POST['administrator'] != $theSuperUserID){ // ... a NEW administrator
        $defaultID = $_POST['administrator'];
    } else if ($theSuperUserID){ // ... otherwise use the old version (if one exists)
        $defaultID = $theSuperUserID;
        $defaultLevel = $theLevel;
    } else { // ... otherwise nothing
        $defaultID = 0;
    };
    print("<td>");
    print("<select name='administrator' onChange='javascript:document.forms[0].submit()'>");
    print("<option value='0'>Select ...");
    $table=$db->select("ScripStudent");
    foreach ($table as $row){
        $userName = $row['UserName'];
        $ID = $row['StudentID'];
        $firstName = $row['FirstName'];
        $lastName = $row['LastName'];
        if ($userName[0] == "!"){
            print("<option value=$ID");
            if ($ID == $defaultID) { print(" selected"); };
            print(">$userName - $firstName $lastName ($ID)");
        };
    };
    print("</select>");
    print("</td>");



/*
    print("<select name='caseid' ");
    print("onChange='javascript:document.forms[0].submit()'>");

// The use this in the big case statement at the top
            if ($_POST['Update']) {
                // The button has been pressed: call the update function
            } else {
                // The select function has been changed (and submitted itself): call the edit-a-single-line function
            };


*/



    // Data: Level - choose from a drop-down box ...
    print("<td>");
    print("<select name='level'>");
    for ($i = 0; $i < 100; $i++){
        print("<option value='$i'");
        if ($i == $defaultLevel) { print(" selected"); };
        print(">$i");
    };
    print("</select>");
    print("</td>");
   
    print("</tr>");

    print("<tr><td colspan='4' align='center'>");
    print("<input type='submit' value='Update' name='Update'></td></tr>");
    print("</form></table></td></tr><tr><td>");
    print("<a href=scripadmin.php?tab=5>Return to Menu</a>");
};

function AAA_UpdateAdmin($SID, $administrator, $level){
    $db=new dbabstraction();
    $db->connect() or die ($db->getError()); 

    // Update details of the trust ...
    $data= array(
        "SuperUserID" => $administrator,
        "Level" => $level
	);

    Z_DrawHeader();

    if ($SID!='0') {
        $condition=array('ID'=>$SID);
        $db->update("ScripAdmin",$data, $condition);
    } else {
        $db->insert("ScripAdmin",$data);
    };

    print("<tr><td colspan='2'><font color='#ffffff'>Administrator updated");
    print("</font></td></tr>");
    print("<tr><td><a href=scripadmin.php?tab=5>Return to Menu</a>");
    print("</td></tr></table>");
};


function ZZZ_WriteToLogonAdmin($adminNumber, $adminLevel){
    // Write details of the user to the database:

    $db=new dbabstraction();
    $db->connect() or die ($db->getError());

    // Items still to be added: IPAddress, Location, InDispensary, Dispendary Mode, Date, Time

    // Find out more information about user:
    // Begin with ScripAdmin ...
    $condition = array('ID'=>$adminNumber);
    $table=$db->select("ScripAdmin", $condition);
    foreach ($table as $row){
        $studentID = $row['SuperUserID'];
    };

    // Now use ScripStudent ...
    $condition = array('StudentID'=>$studentID);
    $table=$db->select("ScripStudent", $condition);
    foreach ($table as $row){
        $firstName = $row['FirstName'];
        $lastName = $row['LastName'];
        $userName = $row['UserName'];
    };



/*
    if ($_SESSION['theComputerName']){
        // This is taken from $GLOBALS ... if it exists then we are using localhost and use the name for searching ...
        $condition=array("MachineName"=>$_SESSION['theComputerName']);
    } else {
        // if not then we are using Granby and search using the IP address ...
        $condition=array("IPAddress"=>$_SESSION['theIPAddress']);
    };

    $table=$db->select("ScripIPAddresses",$condition);
    foreach ($table as $row) {
        $location=$row['Location'];
        $_SESSION['checkingMachine'] = $row['Checking'];
    };

    if (!$location) { $location="Not known"; };

    if ($_SESSION['table']=="Student") {
        $ltext=array("StudentID"=>$studentID,"Username"=>$_SESSION['username'],
                     "FirstName"=>$firstname,"LastName"=>$lastname,
                     "IPAddress"=>$_SESSION['theIPAddress'],"Location"=>$location,
                     "InDispensary"=>$_SESSION['inTheDispensary'],
                     "DispensaryMode"=>$_SESSION['dispensaryMode'],
                     "Date"=>date("y-m-d"),"Time"=>date("H:i:s"));
        $db->insert("ScripLogon", $ltext);
    };
*/



    $data= array("AdminID" => $adminNumber, "AdminLevel" => $adminLevel, "StudentID" => $studentID, "UserName" => $userName, "FirstName" => $firstName, "LastName" => $lastName, "Date"=>date("y-m-d"),"Time"=>date("H:i:s"));

    $db->insert("ScripLogonAdmin", $data);

};


?>


